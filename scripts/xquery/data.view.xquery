xquery version "3.1";

declare namespace entity = "https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace foaf = "http://xmlns.com/foaf/0.1/";
declare namespace gndo = "https://d-nb.info/standards/elementset/gnd#";
declare namespace owl = "http://www.w3.org/2002/07/owl#";
declare namespace skos = "http://www.w3.org/2004/02/skos/core#";


import module namespace stats = "https://sub.uni-goettingen.de/met/apps/entity-xml/statistics" at "stats.xqm";

declare variable $doc := .;

declare function local:data-view( $nodes ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node
            case element(entity:entityXML) return 
                <html>
                    <header>
                        <link rel="stylesheet" href="css/records.css" type="text/css"></link>
                    </header>
                    <body>
                        { 
                            for $elem in $node/entity:data/element()
                            return
                            <div class="list {$elem/name()}">
                                { local:data-view($elem/node()) }
                            </div>
                        }      
                    </body>
                </html>
            
            case element(entity:person) return 
                local:entity( $node )
            
            default return local:data-view($node/node())
};


declare function local:entity( $entity ){
    let $eval := stats:person-level($entity)
    return 
    <div class="entity {$entity/name()}">
        <div class="header">
            <h1>
                {
                    if ( $entity[gndo:preferredName] ) 
                    then ( data($entity/gndo:preferredName) ) 
                    else if ( $entity[gndo:alternativeName] ) 
                    then ( data($entity/gndo:alternativeName) ) 
                    else if ( $entity[@xml:id] ) 
                    then ( data($entity/@xml:id) ) 
                    else ("Unknown")
                }
            </h1>
            {            
                if ( $entity[@gnd-uri] ) 
                then ( <h3><a href="{data($entity/@gnd-uri)}">{data($entity/@gnd-uri)}</a></h3> ) 
                else () 
            }
            <div class="level">
                <i>Informationsdichte</i>: { $eval/@stat => data() }%
            </div>
        
        </div>
        <div class="data">
            { 
                let $known-properties := ( $entity/gndo:preferredName, $entity/gndo:alternativeName, $entity/foaf:page, $entity/skos:note )
                return 
                    local:structured-data-properties( $entity/element() ) 
            }
        </div>
        <div class="text-box">
            { local:text-data-view( $entity/node() ) }
        </div>
    </div>
};


declare function local:structured-data-properties( $nodes ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node
            case element( gndo:preferredName ) return 
                <div class="property">
                    <div class="label">{ $node/name() }</div>
                    <div class="value element">
                        { local:structured-data-properties( $node/element() )  }
                    </div>
                </div>            
            default return
                <div class="property">
                    <div class="label">{ $node/name() }</div>
                    <div class="value text">
                        { local:structured-data-properties( $node/node() )  }
                    </div>
                </div>
};

declare function local:text-data-view( $nodes ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case comment() return $node          
            default return
                <span class="property {$node/name()}" title="{$node/name()}" data-tooltip="{$node/name()}">
                    { local:text-data-view( $node/node() ) }
                </span>
};





local:data-view($doc)