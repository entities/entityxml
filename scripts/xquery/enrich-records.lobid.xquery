xquery version "3.1";

import module namespace lobid="http://sub.uni-goettingen.de/met/xquery/lobid" at "modules/lobid.xqm";

declare namespace app="http://sub.uni-goettingen.de/met/standards/application-profiles/app-info#";
declare namespace a="http://relaxng.org/ns/compatibility/annotations/1.0";
declare namespace entityxml="https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace gndo="https://d-nb.info/standards/elementset/gnd#" ;
declare namespace html="http://www.w3.org/1999/xhtml" ;
declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace rng="http://relaxng.org/ns/structure/1.0";
declare namespace sch="http://purl.oclc.org/dsdl/schematron";
declare namespace schema="http://schema.org/";
declare namespace owl="http://www.w3.org/2002/07/owl#";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";

declare variable $local:doc := .;

declare function local:order($nodes){
    for $node in $nodes
    let $name := $node/name()
    order by $name
    return $node
};


declare function local:convert( $nodes ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return $node
            case processing-instruction() return $node
            case comment() return $node
            
            case element(entityxml:entityXML) return 
                element {QName( fn:namespace-uri( $node ), $node/name() )}{
                    (
                        for $prefix in in-scope-prefixes($node)
                        let $namespace := namespace-uri-for-prefix($prefix, $node)
                        return 
                            namespace {$prefix} {$namespace}
                    ),
                    $node/@*,
                    local:convert($node/node())
                        
                }
            
            case element(entityxml:list) return (
                element {QName( fn:namespace-uri( $node ), $node/name() )}{
                    $node/@*,
                    for $item in $node/node()
                    return
                        
                        (: Update enrichment via tag @enriched :)
                        if ( $item[@enrich='true'] )
                        then (
                            let $uri := $item/@gndo:uri => string()
                            let $doc := lobid:doc-from-lobid( $uri )
                            let $enrichment := lobid:convert($doc/node()/node()/node())
                            return 
                                element {QName( fn:namespace-uri( $item ), $item/name() )}{
                                    $item/@*,
                                    $item/node()[not(@enriched = "http://lobid.org/gnd/")],
                                    $enrichment
                                }
                        ) 
                        
                        else (
                            local:convert( $item )
                        )
                } 
            )
            default return local:default( $node )
};

declare function local:default( $nodes ){
    for $node in $nodes
    let $namespace-uri := fn:namespace-uri( $node )
    return 
        element {QName( $namespace-uri, $node/name() )}{
            $node/@*,
            local:convert( $node/node() )
        } 
};


let $uri := "https://d-nb.info/gnd/4115792-8"
let $uri := "https://d-nb.info/gnd/118602802"
let $doc := lobid:doc-from-lobid( $uri )
let $convert := lobid:convert($doc/node()/node()/node())
return local:convert($local:doc/node())