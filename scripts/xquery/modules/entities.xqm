xquery version "3.1";

module namespace entities="http://sub.uni-goettingen.de/met/apps/entitystore/entities";

declare namespace entityxml="https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace gndo="https://d-nb.info/standards/elementset/gnd#" ;
declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace dc="http://purl.org/dc/elements/1.1/";



declare function entities:entities( $deliveries as node()* ){
    for $resource in $deliveries
    let $data := $resource//entityxml:data
    return 
        entities:entities-by-section($data/*)
};

declare function entities:entities-by-section( $sections as node()* ){
    
    for $section in $sections
    return $section/element()[
        self::entityxml:person or self::entityxml:place
    ]
};

declare function entities:entities-by-id( $deliveries as node()*, $id ){
    for $entity in entities:entities( $deliveries )
    return 
        $entity[@xml:id = $id]     
}; 

declare function entities:entity-by-index( $deliveries as node()*, $index ){
    entities:entities( $deliveries )[ $index ]
};


declare function entities:entity-type( $entities as node()* ){
    let $ignore := ("entityxml:title", "entityxml:desc")
    for $entity in $entities
    return
        if ( $entity/name() != $ignore )
        then ( $entity/name() )
        else ()
};


declare function entities:entity-title( $entity as node() ){
    if ( $entity[gndo:preferredName] ) 
    then (
        entities:entity-name($entity/gndo:preferredName)
    ) 
    else if ( $entity[gndo:variantName] ) 
    then (
        entities:entity-name($entity/gndo:variantName)
    ) 
    else if ( $entity[dc:title] ) 
    then (
        entities:entity-name($entity/dc:title)
    ) 
    else if ( $entity[@gndo:uri] ) 
    then ( $entity/@gndo:uri => data() ) 
    else if ( $entity[@xml:id] ) 
    then ( $entity/@xml:id => data() ) 
    else ( "Unbenannte Enitität" )
};


declare function entities:entity-name( $name as node()* ){
    let $name-string := 
        if ( $name[gndo:personalName] ) 
        then (
            normalize-space( data($name/gndo:personalName) )
        ) 
        else if ( $name[gndo:surname] ) 
        then (
            <span class="name">{normalize-space( data($name/gndo:surname) )}</span>,
            if ( $name[gndo:forename] ) then (<span class="name-component">{normalize-space( data($name/gndo:forename) )}</span>) else (),
            if ( $name[gndo:prefix] ) then (<span class="name-prefix">{normalize-space( data($name/gndo:prefix) )}</span>) else ()
        ) 
        else ( normalize-space( data($name) ) )
    return $name-string 
};

declare function entities:group-by-name( $entities as node()* ){
    for $entity in $entities
    let $name := $entity/name()
    group by $name
    return 
        <entities name="{$name}" count="{count($entity)}">
            {$entity}
        </entities>
};

declare function entities:lobid-query( $entity ){
    let $preferred-name := 
        if ( $entity/gndo:preferredName[gndo:personalName] ) 
        then (
            normalize-space($entity/gndo:preferredName/gndo:personalName/text())
        ) 
        
        else if ( $entity[self::entityxml:person] ) 
        then (
            if ($entity/gndo:preferredName[gndo:forename]) 
            then (normalize-space($entity/gndo:preferredName/gndo:forename/text()))
            else (),
            
            if ($entity/gndo:preferredName[gndo:prefix]) 
            then (normalize-space($entity/gndo:preferredName/gndo:prefix/text())) 
            else (),
            
            normalize-space($entity/gndo:preferredName/gndo:surname/text())
        ) 
        
        else (normalize-space($entity/gndo:preferredName/text()))
    let $date-of-birth-and-death := 
        if ( $entity/gndo:dateOfBirth or $entity/gndo:dateOfDeath ) 
        then (
            if ($entity/gndo:dateOfBirth[@iso-date]) then (tokenize($entity/gndo:dateOfBirth/@iso-date, '-')[1]) else (),
            "-",
            if ($entity/gndo:dateOfDeath[@iso-date]) then (tokenize($entity/gndo:dateOfDeath/@iso-date, '-')[1]) else ()
        ) 
        else ()
    
    let $query := normalize-space(string-join(($entity/dc:title/text(), $preferred-name, $date-of-birth-and-death),' '))
    return $query

};