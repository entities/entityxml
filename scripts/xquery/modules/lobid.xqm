xquery version "3.1";

module namespace lobid="http://sub.uni-goettingen.de/met/xquery/lobid";

declare namespace app="http://sub.uni-goettingen.de/met/standards/application-profiles/app-info#";
declare namespace a="http://relaxng.org/ns/compatibility/annotations/1.0";
declare namespace entityxml="https://sub.uni-goettingen.de/met/standards/entity-xml#";
declare namespace gndo="https://d-nb.info/standards/elementset/gnd#" ;
declare namespace html="http://www.w3.org/1999/xhtml" ;
declare namespace foaf="http://xmlns.com/foaf/0.1/";
declare namespace rng="http://relaxng.org/ns/structure/1.0";
declare namespace sch="http://purl.oclc.org/dsdl/schematron";
declare namespace schema="http://schema.org/";
declare namespace wdrs="http://www.w3.org/2007/05/powder-s#";
declare namespace owl="http://www.w3.org/2002/07/owl#";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";


declare function lobid:get-gnd-id($gnd-uri){
    tokenize($gnd-uri, "/")[last()]
};

declare function lobid:doc-from-lobid( $gnd-uri ){
    let $gnd-id := lobid:get-gnd-id( $gnd-uri )
    return doc("http://lobid.org/gnd/"||$gnd-id)
};



declare function lobid:source(){
    attribute enriched {"http://lobid.org/gnd/"}
    (:attribute enriched {"http://lobid.org/gnd/"}:)
};



(: CONVERSION Routines for LOBID RDF-XML DATA :)


declare function lobid:convert( $nodes ){
    for $node in $nodes
    return 
        typeswitch($node)
            case text() return 
                if ( not(normalize-space($node) = '') ) 
                then ( $node ) 
                else ()
            
            case comment() return $node
            
            case element(gndo:firstAuthor) return 
                element gndo:firstAuthor {
                    lobid:source(),
                    attribute gndo:ref {$node/*:Description/@*:about => string()},
                    normalize-space(data($node))
                }
            
            case element(gndo:forename) return 
                element gndo:forename {
                    (:lobid:source(),:)
                    normalize-space(data($node))
                }
                
            case element(gndo:gndSubjectCategory) return 
                element gndo:gndSubjectCategory {
                    lobid:source(),
                    attribute gndo:term {$node/*:Description/@*:about => string()},
                    normalize-space(data($node)),
                    fn:namespace-uri($node)
                    (:prefix-from-QName():)
                }
                
            case element(gndo:geographicAreaCode) return 
                element gndo:geographicAreaCode {
                    lobid:source(),
                    attribute gndo:term {$node/*:Description/@*:about => string()},
                    normalize-space(data($node))
                }
            
            case element(gndo:languageCode) return 
                element gndo:languageCode {
                    lobid:source(),
                    normalize-space(data($node))
                }
                
            case element(gndo:personalName) return 
                element gndo:personalName {
                    (:lobid:source(),:)
                    normalize-space(data($node))
                }
                
            case element(gndo:preferredName) return 
                element gndo:preferredName {
                    lobid:source(),
                    normalize-space(data($node))
                }
            
            case element(gndo:professionOrOccupation) return 
                lobid:gndo-uri-element( $node )
                
            case element(rdf:RDF) return 
                lobid:convert( $node/node() )
                
            case element(gndo:surname) return 
                element gndo:surname {
                    (:lobid:source(),:)
                    normalize-space(data($node))
                }
            
            case element(gndo:variantName) return 
                element gndo:variantName {
                    lobid:source(),
                    lobid:convert($node/node())
                }
            
            case element(gndo:variantNameEntityForThePerson) return 
                element gndo:variantName {
                    lobid:source(),
                    lobid:convert($node/node())
                }
                
            case element(gndo:Work) return 
                lobid:convert( $node/node() )
            
            case element(wdrs:describedby) return ()
                
            case element(schema:sameAs) return 
                element owl:sameAs {
                    lobid:source(),
                    $node/*:Description/@*:about => string()
                }
                
            default return lobid:copy( $node )
};



declare function lobid:copy( $nodes ){
    for $node in $nodes
    let $namespace-uri := fn:namespace-uri( $node )
    return 
        element {QName( $namespace-uri, $node/name() )}{
            lobid:source(),
            (:attribute default {"true"},:)
            $node/@*,
            normalize-space(data( $node ))
        } 
};

declare function lobid:gndo-uri-element( $nodes ){
    for $node in $nodes
    let $namespace-uri := fn:namespace-uri( $node )
    let $gndo-uri := 
        if ($node/rdf:Description/@rdf:about) 
        then (
            data($node/rdf:Description/@rdf:about)
        ) 
        else if ($node/@rdf:resource) 
        then (
            data($node/@rdf:resource)
        ) 
        else ()
    return 
        element {QName( $namespace-uri, $node/name() )}{
            lobid:source(),
            if ( exists($gndo-uri) ) 
            then (
                attribute gndo:ref {$gndo-uri}
            )
            else (),
            normalize-space( string-join(data( $node/node() )) )
        } 
};

