<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:dnb="https://www.dnb.de" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:ex="https://gitlab.gwdg.de/usikora/entityxml/tests" 
    xmlns:es="https://sub.uni-goettingen.de/met/standards/entity-xml-staging#"
    xmlns:cst="https://sub.uni-goettingen.de/met/standards/cst"
    xmlns:staba="https://www.gda.bayern.de/bamberg"
    xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output indent="yes" method="xml"/>
    
    <!-- 
    ##########################################################################
            Konkordanzen aus GND-Nr. Listen mit entityXML
            *********************************************
            
            Dieses Script erweitert entityXML Dateien mit GND-IDs, die aus 
            einer GND-Einspielungskonkordanz (locale IDs -> GND-Nr.) geliefert werden.
            
            Der Skriptworkflow muss in der entityXML Datei in 
            'entityXML/metadata/applications' konfiguriert werden:
            
            <applications>
                <application name="gnd-ids" href="koko_sample.txt" completed="false" who="UwS" info="true"/>
            </applications>
            
            @name: Immer "gnd-ids", sonst wird das script nicht ausgeführt!
            @href: relativer Link zur GND-Konkordanz
            @completed: Wird vom Script ausgefüllt, wenn schon einmal über diesen Weg angereichert
            @info: Wenn 'true', dann werden einzelne Informationen in applications angelegt
    
    ##########################################################################
    -->
    
    
    <!-- 
        FUNCTIONS
        #########
    -->
    
    <!-- parsing a text file consiting of lines with two values separated by whitespace and templating it to XML -->
    <xsl:function name="gndo:parse-konkordanz">
        <xsl:param name="content"/>
        <xsl:variable name="lines" select="tokenize($content, '\n')"/>
        <konkordanz>
            <xsl:for-each select="$lines">
                <xsl:if test="not(normalize-space(.) = '')">
                    <xsl:variable name="tokens" select="tokenize(., '\s')"/>
                    <id gnd="{$tokens[1]}" local="{$tokens[2]}">
                        <xsl:text>https://d-nb.info/gnd/</xsl:text>
                        <xsl:value-of select="$tokens[1]"/>
                    </id>
                </xsl:if>
            </xsl:for-each>
        </konkordanz>
    </xsl:function>
    
    
    <!-- 
        TRANSFORMATION
        ##############
    -->
    <xsl:variable name="date" select="format-date(current-date(),'[Y]-[M,2]-[D,2]')"/>
    <xsl:variable name="application-info" select="//applications/application[@name='gnd-ids'][@completed='false']"/>
    <xsl:variable name="konkordanz-href" select="$application-info/@href => data()"/>
    <xsl:variable name="base-uri" select="base-uri(.)"/>
    <xsl:variable name="konkordanz-path" select="resolve-uri($konkordanz-href, $base-uri)"/>
    <xsl:variable name="konkordanz" select="if (not($konkordanz-href = '')) then (gndo:parse-konkordanz(unparsed-text($konkordanz-path))) else(false())"/>
    
    <!-- IF Konkordanz is found and loaded transformation is fired -->
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="not($konkordanz = 'false')">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- writes the application info -->
    <xsl:template match="applications/application[@name='gnd-ids']">
        <xsl:choose>
            <xsl:when test="@info = 'true'">
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                    <xsl:attribute name="when" select="$date"/>
                    <xsl:attribute name="href" select="$konkordanz-path"/>
                    <xsl:apply-templates select="$konkordanz/id"/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>
    
    <!-- marks application as completed -->
    <xsl:template match="applications/application[@name='gnd-ids']/@completed">
        <xsl:attribute name="comleted">true</xsl:attribute>
    </xsl:template>
    
    <!-- templates a change entry that the data delivery was published -->
    <xsl:template match="metadata/revision">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <change when="{$date}" who="{$application-info/@who}" status="published">Neu angelegte Records auf GND-URIs gemappt.</change>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Transforms every entry IF it has no gndo:uri yet. IF its local URI is found in the Konkordanz it is processes. Otherwise it is just copied. -->
    <xsl:template match="data/list/element()[@xml:id][not(@gndo:uri)]">
        <xsl:variable name="local-id" select="@xml:id"/>
        <xsl:choose>
            <xsl:when test="$local-id = ($konkordanz//id/@local => data())">
                <xsl:copy>
                    <xsl:apply-templates select="@*[not(name()='gndo:uri')]"/>
                    <xsl:attribute name="gndo:uri"><xsl:value-of select="$konkordanz//id[@local = $local-id]/text()"/></xsl:attribute>
                    <xsl:apply-templates select="node()"/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Every transformed entry is set to be ignored by an further agency action -->
    <xsl:template match="data/list/element()[@xml:id]/@agency">
        <xsl:attribute name="agency">ignore</xsl:attribute>
    </xsl:template>
    
    <!-- Every tranformed entry gets a change entry stating that it is published -->
    <xsl:template match="data/list/element()/revision">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <change when="{$date}" who="{$application-info/@who}" status="published">Eintrag in die GND eingespielt.</change>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Every tranformed entry gets marked to be closed -->
    <xsl:template match="data/list/element()/revision/@status">
        <xsl:attribute name="status">closed</xsl:attribute>
    </xsl:template>
    
    <!-- Every entry which actually has a gndo:uri is just copied without transformation -->
    <xsl:template match="data/list/element()[@xml:id][@gndo:uri]">
        <xsl:copy-of select="."/>            
    </xsl:template>
    
    <!-- Everything else is copied and run through transformation -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>