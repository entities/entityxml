<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rng="http://relaxng.org/ns/structure/1.0"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron"
    xmlns:utils="https://sub.uni-goettingen.de/met/xslt/functions/entity-xml#"
    exclude-result-prefixes="xs rng" 
    xpath-default-namespace="http://relaxng.org/ns/structure/1.0"
    version="3.0">
    
    <xsl:function name="utils:dir">
        <xsl:param name="path" />
        <xsl:value-of select="string-join( tokenize($path, '/')[not(position()=last())], '/' )"/>
    </xsl:function>
    
    <xsl:function name="utils:schema-element-names" visibility="public">
        <xsl:param name="schema" />
        <xsl:for-each select="($schema//rng:element, $schema//rng:attribute)">
            <xsl:value-of select="name()"/>
        </xsl:for-each>
    </xsl:function>
    
</xsl:stylesheet>