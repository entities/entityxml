<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs geo gndo exml marc foaf" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
   <xsl:function name="gndo:date">
       <xsl:param name="from"/>
       <xsl:param name="to"/>
       <xsl:choose>
           <xsl:when test="$from and $to">
               <xsl:value-of select="gndo:iso-date2dotted-date( $from )"/><xsl:text>-</xsl:text><xsl:value-of select="gndo:iso-date2dotted-date( $to )"/>
           </xsl:when>
           <xsl:when test="$from">
               <xsl:value-of select="gndo:iso-date2dotted-date( $from )"/><xsl:text>-</xsl:text>
           </xsl:when>
           <xsl:when test="$to">
               <xsl:text>-</xsl:text><xsl:value-of select="gndo:iso-date2dotted-date( $to )"/>
           </xsl:when>
           <xsl:otherwise>UWE</xsl:otherwise>
       </xsl:choose>
   </xsl:function>
    
    
    <!-- 
        INCLUDE Module (not standalone): Event Records 2 Marc-XML 
        **********************************************************
        - uses external named templated from /utils/templs.xsl
    -->
    
    <xsl:template match="exml:event">
        <xsl:call-template name="default-record"/>
    </xsl:template>
    
    <!-- {{ 111: Bevorzugter Name, gndo:preferredName }} -->
    <xsl:template match="exml:event/gndo:preferredName">
        <datafield tag="111" ind1="2" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="d">
                <xsl:value-of select="gndo:date(parent::exml:event/gndo:dateOfConferenceOrEvent/@iso-from, parent::exml:event/gndo:dateOfConferenceOrEvent/@iso-to)"/>
                <!--<xsl:value-of select="gndo:date('2015', '2016')"/>-->
            </subfield>
            <subfield code="c"><xsl:value-of select="normalize-space(parent::exml:event/gndo:place/text())"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 411: Namensvariante, gndo:variantName }} -->
    <xsl:template match="exml:event/gndo:variantName">
        <datafield tag="411" ind1="2" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="d">
                <xsl:value-of select="gndo:date(parent::exml:event/gndo:dateOfConferenceOrEvent/@iso-from, parent::exml:event/gndo:dateOfConferenceOrEvent/@iso-to)"/>
                <!--<xsl:value-of select="gndo:date('2015', '2016')"/>-->
            </subfield>
            <subfield code="c"><xsl:value-of select="normalize-space(parent::exml:event/gndo:place/text())"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 510: Veranstalter, gndo:organizerOrHost }} -->
    <xsl:template match="exml:event/gndo:organizerOrHost">
        <xsl:variable name="label" select="if (label) then (label/text()) else (text())"/>
        <datafield tag="510" ind1="2" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space($label)"/></subfield>
            <subfield code="4">vera</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#organizerOrHost</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Veranstalter</subfield>
            <subfield code="e">Veranstalter</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 550: Thema, gndo:topic }} -->
    <xsl:template match="exml:event/gndo:topic">
        <xsl:variable name="label" select="if (label) then (label/text()) else (text())"/>
        <datafield tag="550" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space($label)"/></subfield>
            <subfield code="4">them</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#topic</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Thema</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 551: Ort des events, gndo:date }} -->
    <xsl:template match="exml:event/gndo:place">
        <datafield tag="551" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">ortv</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfConferenceOrEvent</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Veranstaltungsort</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 548: Datum des Events }} -->
    <xsl:template match="exml:event/gndo:dateOfConferenceOrEvent">
        <xsl:variable name="from" select="normalize-space(data(@iso-from))"/>
        <xsl:variable name="to" select="normalize-space(data(@iso-to))"/>
        <datafield tag="548" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a">
                <xsl:choose>
                    <xsl:when test="$from and $to">
                        <xsl:value-of select="gndo:iso-date2dotted-date( $from )"/><xsl:text>-</xsl:text><xsl:value-of select="gndo:iso-date2dotted-date( $to )"/>
                    </xsl:when>
                    <xsl:when test="$from">
                        <xsl:value-of select="gndo:iso-date2dotted-date( $from )"/><xsl:text>-</xsl:text>
                    </xsl:when>
                    <xsl:when test="$to">
                        <xsl:text>-</xsl:text><xsl:value-of select="gndo:iso-date2dotted-date( $to )"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="normalize-space(text())"/></xsl:otherwise>
                </xsl:choose>
            </subfield>
            <subfield code="4">datv</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfConferenceOrEvent</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Veranstaltungsdaten</subfield>
        </datafield>
    </xsl:template>

    
</xsl:stylesheet>