<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs geo gndo exml marc foaf" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <!-- 
        INCLUDE Module (not standalone): Corporate Body Records 2 Marc-XML 
        **********************************************************
        - uses external named templated from /utils/templs.xsl
    -->
    
    <!--
        Templates
        ########################################
    -->
    <!--<xsl:template match="exml:corporateBody/@gndo:uri">
        <xsl:variable name="id" select="tokenize(., '/')[last()]"/>
        <datafield tag="024" ind1="7" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="$id"/></subfield>
            <subfield code="0"><xsl:value-of select="."/></subfield>
            <subfield code="2">gnd</subfield>
        </datafield>
    </xsl:template>-->
    
    <xsl:template match="exml:corporateBody">
        <xsl:call-template name="default-record"/>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:abbreviatedName">
        <datafield tag="410" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">abku</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:dateOfEstablishment">
        <xsl:variable name="date" select="normalize-space(data(@iso-date))"/>
        <datafield tag="548" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="$date"/><xsl:text>-</xsl:text></subfield>
            <subfield code="4">datb</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfEstablishment</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Zeitraum</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:dateOfEstablishmentAndTermination">
        <xsl:variable name="from" select="normalize-space(data(@iso-from))"/>
        <xsl:variable name="to" select="normalize-space(data(@iso-to))"/>
        <datafield tag="548" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="$from"/><xsl:text>-</xsl:text><xsl:value-of select="$to"/></subfield>
            <subfield code="4">datb</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfEstablishmentAndTermination</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Zeitraum</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:dateOfTermination">
        <xsl:variable name="date" select="normalize-space(data(@iso-date))"/>
        <datafield tag="548" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:text>-</xsl:text><xsl:value-of select="$date"/></subfield>
            <subfield code="4">datb</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfTermination</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Zeitraum</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 550: gndo:functionOrRole }} DEPRECATED in GND since 2017 -->
    <!--
    <xsl:template match="exml:corporateBody/gndo:functionOrRole[@gndo:term]">
        <datafield tag="550" ind1=" " ind2=" ">
            <subfield code="a"><xsl:value-of select="normalize-space(data(@gndo:term))"/></subfield>
            <subfield code="4">funk</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#functionOrRole</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Funktion oder Rolle</subfield>
        </datafield>
    </xsl:template>
    -->
    
    <xsl:template match="exml:corporateBody/gndo:placeOfBusiness">
        <datafield tag="551" ind1="2" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">orta</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfBusiness</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Ort</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:precedingCorporateBody">
        <datafield tag="510" ind1="2" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">vorg</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#precedingCorporateBody</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Vorgaenger</subfield>
            <subfield code="e">Vorgaenger</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:preferredName">
        <datafield tag="110" ind1="2" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:variable name="place" select="normalize-space(parent::element()/gndo:placeOfBusiness/text())"/>
            <xsl:if test="not(empty($place)) and not($place = '')">
                <subfield code="g"><xsl:value-of select="$place"/></subfield>
            </xsl:if>
            <xsl:if test="@script">
                <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
            </xsl:if>
            <xsl:if test="@xml:lang">
                <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:succeedingCorporateBody">
        <datafield tag="510" ind1="2" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">nach</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#succeedingCorporateBody</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Nachfolger</subfield>
            <subfield code="e">Nachfolger</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:temporaryName">
        <datafield tag="510" ind1="2" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">nazw</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#temporaryNameOfTheCorporateBody</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Zeitweiser Name</subfield>
            <subfield code="e">Zeitweiser Name</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:corporateBody/gndo:variantName">
        <datafield tag="410" ind1="2" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:if test="@script">
                <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
            </xsl:if>
            <xsl:if test="@xml:lang">
                <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
</xsl:stylesheet>