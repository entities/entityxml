<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs geo gndo exml marc foaf wgs84" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">  
    
    <!-- 
        INCLUDE Module (not standalone): Place Records 2 Marc-XML 
        *********************************************************
        - uses external named templated from /utils/templs.xsl
        - uses external functions from /utils/functs.xsl
    -->
    
    
    <!--
        Templates
        ########################################
    -->
    <!--<xsl:template match="exml:place/@gndo:uri">
        <xsl:variable name="id" select="tokenize(., '/')[last()]"/>
        <datafield tag="024" ind1="7" ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <subfield code="a"><xsl:value-of select="$id"/></subfield>
            <subfield code="0"><xsl:value-of select="."/></subfield>
            <subfield code="2">gnd</subfield>
        </datafield>
    </xsl:template>-->
    
    <xsl:template match="exml:place">
        <xsl:call-template name="default-record"/>
    </xsl:template>
    
    <xsl:template match="geo:hasGeometry">
        <xsl:variable name="dgx_lat" select="xs:string(format-number(xs:float(normalize-space(wgs84:lat/text())), '000.#####'))"/>
        <xsl:variable name="dgx_long" select="xs:string(format-number(xs:float(normalize-space(wgs84:long/text())), '000.#####'))"/>
        <xsl:variable name="lat_dir">
            <xsl:choose>
                <xsl:when test="starts-with($dgx_lat, '-')">S</xsl:when>
                <xsl:otherwise>N</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="long_dir">
            <xsl:choose>
                <xsl:when test="starts-with($dgx_long, '-')">W</xsl:when>
                <xsl:otherwise>E</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="source" select="data(@geo:source)"/>
        <datafield tag="034" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="d"><xsl:value-of select="$long_dir"/><xsl:text> </xsl:text><xsl:value-of select="replace(geo:dec2gms($dgx_long), '[\-\+]', '')"/></subfield>
            <subfield code="e"><xsl:value-of select="$long_dir"/><xsl:text> </xsl:text><xsl:value-of select="replace(geo:dec2gms($dgx_long), '[\-\+]', '')"/></subfield>
            <subfield code="f"><xsl:value-of select="$lat_dir"/><xsl:text> </xsl:text><xsl:value-of select="replace(geo:dec2gms($dgx_lat), '[\-\+]', '')"/></subfield>
            <subfield code="g"><xsl:value-of select="$lat_dir"/><xsl:text> </xsl:text><xsl:value-of select="replace(geo:dec2gms($dgx_lat), '[\-\+]', '')"/></subfield>
            <xsl:if test="@geo:source">
                <!--<subfield code="2">geonames</subfield>-->
                <subfield code="0"><xsl:value-of select="$source"/></subfield>
            </xsl:if>
            <subfield code="9">A:agx</subfield>
        </datafield>
        <datafield tag="034" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="d"><xsl:value-of select="$long_dir"/><xsl:value-of select="replace($dgx_long, '[\-\+]', '')"/></subfield>
            <subfield code="e"><xsl:value-of select="$long_dir"/><xsl:value-of select="replace($dgx_long, '[\-\+]', '')"/></subfield>
            <subfield code="f"><xsl:value-of select="$lat_dir"/><xsl:value-of select="replace($dgx_lat, '[\-\+]', '')"/></subfield>
            <subfield code="g"><xsl:value-of select="$lat_dir"/><xsl:value-of select="replace($dgx_lat, '[\-\+]', '')"/></subfield>
            <xsl:if test="@geo:source">
                <!--<subfield code="2">geonames</subfield>-->
                <subfield code="0"><xsl:value-of select="$source"/></subfield>
            </xsl:if>
            <subfield code="9">A:dgx</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:place/gndo:place">
        <datafield tag="551" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">orta</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#place</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Ort</subfield>
            <subfield code="9">X:1</subfield>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:place/gndo:preferredName">
        <datafield tag="151" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:if test="@script">
                <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
            </xsl:if>
            <xsl:if test="@xml:lang">
                <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    <xsl:template match="exml:place/gndo:variantName">
        <datafield tag="451" ind1=" " ind2=" " xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <!-- Zusätzliche Relation Information, z.B. wer Name vergebn hat
            <subfield code="i"></subfield>
            <subfield code="4"></subfield>
            -->
            <xsl:if test="@script">
                <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
            </xsl:if>
            <xsl:if test="@xml:lang">
                <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
            
</xsl:stylesheet>