<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    exclude-result-prefixes="xs exml geo gndo marc"
    version="3.0">
    
    <xsl:function name="exml:get-entity-by-id">
        <xsl:param name="context"/>
        <xsl:param name="id"/>
        <xsl:copy-of select="$context/root()//element()[@xml:id = $id]"/>
    </xsl:function>
    
    <xsl:function name="exml:get-local-entity">
        <xsl:param name="node"/>
        <xsl:variable name="id" select="substring-after(data($node/@ref), '#')"/>
        <xsl:copy-of select="exml:get-entity-by-id($node, $id)"/>
    </xsl:function>
    
    <xsl:function name="geo:dec2gms">
        <xsl:param name="dec"/>
        <xsl:variable name="tokens" select="tokenize($dec, '\.')" />
        <xsl:variable name="grad" select="xs:int($tokens[1])" />
        <xsl:variable name="nachkomma" select="xs:float('0.'||$tokens[2])" />
        <xsl:variable name="x" select="$nachkomma*60"/>
        <xsl:variable name="minuten" select="xs:int(tokenize(xs:string($x), '\.')[1])"/>
        <xsl:variable name="sekunden" select="format-number(($x - $minuten)*60, '00.#')"/>
        <xsl:variable name="result">
            <xsl:value-of select="format-number($grad, '000')"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="format-number($minuten, '00')"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$sekunden"/>
        </xsl:variable>
        <xsl:value-of select="string-join($result)"/>
    </xsl:function>
    
    <xsl:function name="gndo:base-uri-2-label">
        <xsl:param name="uri"/>
        <xsl:param name="default"/>
        <xsl:choose>
            <xsl:when test="matches($uri, '\.wikipedia.org')">Wikipedia</xsl:when>
            <xsl:otherwise><xsl:value-of select="$default"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="gndo:get-entityfacts">
        <xsl:param name="uri" />
        <xsl:variable name="id" select="tokenize($uri, '/')[last()]"/>
        <xsl:variable name="data" select="unparsed-text('https://hub.culturegraph.org/entityfacts/'||$id)"/>
        <xsl:copy-of select="parse-json($data)"/>
    </xsl:function>
    
    <xsl:function name="gndo:types-from-mapping">
        <xsl:param name="mapping" />
        <xsl:param name="record-type" />
        <xsl:variable name="mapping-term" select="$mapping//*:term[@type = $record-type]"/>
        <xsl:variable name="tokens" select="tokenize($mapping-term/text(), ' ')"/>
        <xsl:for-each select="$tokens">
            <xsl:value-of select="."/>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:function name="gndo:isil">
        <xsl:param name="isil"/>
        <xsl:choose>
            <xsl:when test="$isil = true()">DE-588</xsl:when>
            <xsl:otherwise>gnd</xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="gndo:get-agency-isil">
        <xsl:param name="node"/>
        <xsl:value-of select="$node/root()//*:metadata/*:agency/@isil"/>
    </xsl:function>
    
    <xsl:function name="gndo:iso-date2dotted-date">
        <xsl:param name="iso-date" />
        <xsl:variable name="date-components" select="tokenize($iso-date, '-')"/>
        <xsl:variable name="year" select="$date-components[1]"/>
        <xsl:variable name="month">
            <xsl:choose>
                <xsl:when test="$date-components[2]"><xsl:value-of select="$date-components[2]"/></xsl:when>
                <xsl:otherwise>XX</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="day">
            <xsl:choose>
                <xsl:when test="$date-components[3]"><xsl:value-of select="$date-components[3]"/></xsl:when>
                <xsl:otherwise>XX</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="dotted-date" select="string-join(reverse(($year, $month, $day)), '.')"/>
        <xsl:value-of select="$dotted-date"/>
    </xsl:function>
    
    <!--<xsl:function name="gndo:obin">
        <xsl:param name="terms" />
        <xsl:param name="mapping" />
        <xsl:for-each select="$terms">
            <xsl:variable name="term" select="."/>
            <xsl:variable name="mapping-term" select="$mapping//*:term[@type = $term]"/>
            <!-\-<xsl:variable name="obin" select="$mapping-term/@obin"/>
            <xsl:if test="$obin"><xsl:value-of select="$obin"/><xsl:text>:</xsl:text></xsl:if>
            <xsl:value-of select="$mapping-term"/>-\->
            <xsl:copy-of select="$mapping-term"/>
        </xsl:for-each>
    </xsl:function>-->
    
    <xsl:function name="gndo:uri2id">
        <xsl:param name="uri"/>
        <xsl:value-of select="substring-after($uri, '/gnd/')"/>
    </xsl:function>
    
    
    
    <!-- MARC FUNCTIONS -->
    <!-- ************** -->
    
    <xsl:function name="marc:get-marc">
        <xsl:param name="uri"/>
        <xsl:variable name="id" select="tokenize($uri, '/')[last()]"/>
        <xsl:variable name="marc" select="doc($uri||'/about/marcxml')"/>
        <xsl:copy-of select="$marc"/>
    </xsl:function>
    
    <xsl:function name="marc:get-datafields-from-marc">
        <xsl:param name="marc"/>
        <xsl:param name="tags"/>
        <xsl:copy-of select="$marc//marc:datafield[@tag = $tags]"/>
    </xsl:function>
    
    <xsl:function name="marc:get-subfields-from-datafield">
        <xsl:param name="datafield"/>
        <xsl:param name="codes"/>
        <xsl:copy-of select="$datafield/marc:subfield[@code = $codes]"/>
    </xsl:function>
    
    <xsl:function name="marc:get-preferred-name">
        <xsl:param name="marc"/>
        <xsl:copy-of select="marc:get-datafields-from-marc($marc, ('100', '103'))"/>
    </xsl:function>
    
    <xsl:function name="marc:synopsis">
        <xsl:param name="marc"/>
        <xsl:param name="tags"/>
        <xsl:copy-of select="marc:get-datafields-from-marc($marc, $tags)"/>
    </xsl:function>
    
    <xsl:function name="marc:records">
        <xsl:param name="uris"/>
        <xsl:param name="tags"/>
        <xsl:variable name="distinct" select="distinct-values($uris)"/>
        <records count="{count($distinct)}" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:for-each select="$distinct">
                <xsl:copy-of select="marc:record(., $tags)"></xsl:copy-of>
            </xsl:for-each>
        </records>
    </xsl:function>
    
    <xsl:function name="marc:record">
        <xsl:param name="uri"/>
        <xsl:param name="tags"/>
        <record uri="{$uri}" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:variable name="marc" select="marc:get-marc($uri)"/>
            <xsl:for-each select="$marc/marc:record/@*">
                <xsl:attribute name="{./name()}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:choose>
                <xsl:when test="count($tags)>0">
                    <xsl:copy-of select="marc:get-datafields-from-marc($marc, $tags)"></xsl:copy-of>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="$marc/marc:record/element()"/>
                </xsl:otherwise>
            </xsl:choose>
        </record>
    </xsl:function>
    
    <xsl:function name="marc:retrieve-local-entity">
        <xsl:param name="node"/>
        <xsl:variable name="local-entity" select="exml:get-local-entity($node)"/>
        <xsl:if test="$local-entity[@gndo:uri]">
            <xsl:variable name="uri" select="data($local-entity/@gndo:uri)"/>
            <xsl:variable name="record" select="marc:record($uri, ('100', '103', '024'))"/>
            <!--<xsl:copy-of select="$record"/>-->
            <xsl:if test="$record">
                <record xmlns="http://www.loc.gov/MARC21/slim">
                    <xsl:copy-of select="$record/@*"></xsl:copy-of>
                    <xsl:attribute name="id" select="substring-after($node/@ref, '#')"></xsl:attribute>
                    <xsl:copy-of select="$record/element()"></xsl:copy-of>
                </record>
            </xsl:if>
        </xsl:if>
    </xsl:function>
    
</xsl:stylesheet>