<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:variable name="gndo:record-types">
        <mappings>
            <mapping name="corporateBody">
                <term default="true" type="https://d-nb.info/standards/elementset/gnd#CorporateBody">gndgen:b gndspec:kiz</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Company">gndgen:b gndspec:kif</term>
                <term type="https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody">gndgen:b gndspec:kxz</term>
                <term type="https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody">gndgen:b gndspec:kim</term>
                <term type="https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody">gndgen:b gndspec:kio</term>
                <term type="https://d-nb.info/standards/elementset/gnd#ProjectOrProgram">gndgen:b gndspec:kip</term>
                <term type="https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit">gndgen:b gndspec:kiv</term>
                <term type="https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody">gndgen:b gndspec:kir</term>
            </mapping>
            <mapping name="event">
                <term default="true" type="https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent">gndgen:f gndspec:vie</term>
                <term type="https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent">gndgen:f gndspec:vif</term>
            </mapping>
            <mapping name="expression">
                <term default="true" type="https://d-nb.info/standards/elementset/gnd#Expression">gndgen:u gndspec:wie</term>
            </mapping>
            <mapping name="person">
                <term default="true" type="https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson">gndgen:p gndspec:piz</term>
                <term type="https://d-nb.info/standards/elementset/gnd#CollectivePseudonym">gndgen:p gndspec:pis</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Gods">gndgen:p gndspec:pxg</term>
                <term type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">gndgen:p gndspec:pxl</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Pseudonym">gndgen:p gndspec:pip</term>
                <term type="https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse">gndgen:p gndspec:pik</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Spirits">gndgen:p gndspec:pxs</term>
            </mapping>
            <mapping name="place">
                <term default="true" type="https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName">gndgen:g gndspec:giz</term>
                <term type="https://d-nb.info/standards/elementset/gnd#AdministrativeUnit">gndgen:g gndspec:giv</term>
                <term type="https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial">gndgen:g gndspec:gib</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Country">gndgen:g gndspec:gil</term>
                <term type="https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory">gndgen:g gndspec:gix</term>
                <term type="https://d-nb.info/standards/elementset/gnd#FictivePlace">gndgen:g gndspec:gxz</term>
                <term type="https://d-nb.info/standards/elementset/gnd#MemberState">gndgen:g gndspec:gif</term>
                <term type="https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit">gndgen:g gndspec:gio</term>
                <term type="https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit">gndgen:g gndspec:gin</term>
                <term type="https://d-nb.info/standards/elementset/gnd#ReligiousTerritory">gndgen:g gndspec:gir</term>
                <term type="https://d-nb.info/standards/elementset/gnd#WayBorderOrLine">gndgen:g gndspec:gik</term>
                <term type="https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit">gndgen:g gndspec:giw</term>
            </mapping>
            <mapping name="work">
                <term default="true" type="https://d-nb.info/standards/elementset/gnd#Work">gndgen:u gndspec:wit</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Collection">gndgen:u gndspec:win</term>
                <term type="https://d-nb.info/standards/elementset/gnd#CollectiveManuscript">gndgen:u gndspec:wil</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Expression">gndgen:u gndspec:wie</term>
                <term type="https://d-nb.info/standards/elementset/gnd#Manuscript">gndgen:u gndspec:wis</term>
                <term type="https://d-nb.info/standards/elementset/gnd#MusicalWork">gndgen:u gndspec:wim</term>
                <term type="https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic">gndgen:u gndspec:wip</term>
                <term type="https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork">gndgen:u gndspec:wif</term>
            </mapping>
        </mappings>
    </xsl:variable>
    
    <xsl:function name="gndo:record-types">
        <xsl:param name="mappings" />
        <xsl:param name="record-name" />
        <xsl:param name="record-type" />
        <xsl:choose>
            <xsl:when test="not(empty($record-type)) or not(normalize-space($record-type) = '')">
                <xsl:variable name="mapping" select="$mappings//*:mapping[@name = $record-name]"/>
                <xsl:variable name="term" select="$mapping//*:term[@type = $record-type]"/>
                <xsl:variable name="tokens" select="tokenize($term/text(), ' ')"/>
                <xsl:for-each select="$tokens">
                    <xsl:value-of select="."/>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="gndo:default-type($mappings, $record-name)"></xsl:copy-of>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="gndo:default-type">
        <xsl:param name="mappings" />
        <xsl:param name="record-name" />
        <xsl:variable name="mapping" select="$mappings//*:mapping[@name = $record-name]"/>
        <xsl:variable name="tokens" select="tokenize($mapping//*:term[@default = 'true']/text(), ' ')"/>
        <xsl:for-each select="$tokens">
            <xsl:value-of select="."/>
        </xsl:for-each>
    </xsl:function>
    
</xsl:stylesheet>