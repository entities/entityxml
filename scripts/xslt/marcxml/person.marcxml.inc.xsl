<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs geo gndo exml marc foaf" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <!--<xsl:include href="utils/functs.xsl"/>
    <xsl:include href="utils/templs.xsl"/>
    <xsl:include href="utils/mappings.xsl"/>-->
    
    
    
    <!-- 
        INCLUDE Module (not standalone): Person Records 2 Marc-XML 
        **********************************************************
        - uses external named templated from /utils/templs.xsl
    -->
   
    <xsl:import href="utils/mappings.xsl"/>
    <xsl:import href="defaults.marcxml.inc.xsl"/>
    
    
    <xsl:variable name="fictive-persons">
        <mapping name="person">
            <term type="https://d-nb.info/standards/elementset/gnd#Gods" obin="Gott">Göttergestalt</term>
            <term type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter" obin="Fiktive Gestalt">Fiktive Gestalt</term>
            <term type="https://d-nb.info/standards/elementset/gnd#Spirits" obin="Geist">Geist</term>
            <term type="https://d-nb.info/gnd/4306746-3" obin="Fiktive Gestalt">Fiktive Gestalt</term>
            <term type="https://d-nb.info/gnd/4140220-0" obin="Fiktive Gestalt">Literarische Gestalt</term>
            <term type="https://d-nb.info/gnd/4474147-9" obin="Fiktive Gestalt">Filmgestalt</term>
            <term type="https://d-nb.info/gnd/4221861-5" obin="Sagengestalt">Sagengestalt</term>
        </mapping>
    </xsl:variable>
    
    <!-- 
        FUNCTIONS
        *************************************************************
    -->
    
    <xsl:function name="exml:fictive-uris">
        <xsl:param name="terms" />
        <xsl:param name="mapping" />
        <xsl:for-each select="$terms">
            <xsl:variable name="term" select="."/>
            <xsl:variable name="mapping-term" select="$mapping//*:term[@type = $term]"/>
            <xsl:copy-of select="$mapping-term"/>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:function name="exml:is-fictive">
        <xsl:param name="terms" />
        <xsl:param name="mapping" />
        <xsl:copy-of select="if (count( exml:fictive-uris($terms, $mapping) ) &gt; 0 ) then ( true() ) else ( false() )"/>
    </xsl:function>
    
    <xsl:function name="exml:years-of-living">
        <xsl:param name="date-of-birth"/>
        <xsl:param name="date-of-death"/>
        <xsl:if test="$date-of-birth">
            <xsl:value-of select="tokenize($date-of-birth, '-')[1]"/>    
        </xsl:if>
        <xsl:text>-</xsl:text>
        <xsl:if test="$date-of-death">
            <xsl:value-of select="tokenize($date-of-death, '-')[1]"/>
        </xsl:if>
    </xsl:function>
    
    <!--
        TEMPLATES
        ########################################
    -->
    
    <xsl:template match="exml:person">               
        <record type="Authority" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:call-template name="record-controll-attributes"/>
            
            <xsl:call-template name="leader-controlfields">
                <xsl:with-param name="agency-isil" select="$agency-isil"/>
            </xsl:call-template>
            <xsl:apply-templates select="@*"/>
            
            <xsl:call-template name="df_040">
                <xsl:with-param name="agency-isil" select="$agency-isil"/>
            </xsl:call-template>
            <xsl:call-template name="df_042">
                <xsl:with-param name="level" select="$cataloging-level"/>
            </xsl:call-template>
            <xsl:call-template name="df_079"/>
            <xsl:call-template name="df_667_PK">
                <xsl:with-param name="agency-isil" select="$agency-isil"/>
            </xsl:call-template>
            
            <!-- Create 075 (Entitytype) -->
            <xsl:call-template name="person-075">
                <xsl:with-param name="type" select="@gndo:type" />
                <xsl:with-param name="profession-or-occupation" select="gndo:professionOrOccupation/@gndo:ref" />
                <xsl:with-param name="record-types" select="$gndo:record-types"></xsl:with-param>
            </xsl:call-template>
            
            <xsl:apply-templates select="node()[not(self::gndo:dateOfBirth or self::gndo:dateOfDeath)]"/>
            <xsl:if test="gndo:dateOfBirth[@iso-date] or gndo:dateOfDeath[@iso-date]">
                <xsl:call-template name="dateOfBirthAndDeath.standard" />
                <xsl:call-template name="dateOfBirthAndDeath.exact" />
            </xsl:if>
            <!-- Tp Datensätze erfordern eine Ländercodeangabe. Wenn keine vorhanden ist, dann wird der Code "ZZ" als Platzhalter gesetzt. -->
            <xsl:if test="not(gndo:geographicAreaCode)">
                <datafield tag="043" ind1=" " ind2=" ">
                    <subfield code="c">ZZ</subfield>
                </datafield>
            </xsl:if>
        </record>
    </xsl:template>
    
    
    <!-- {{ 075: Person Subclass }} -->
    <xsl:template name="person-075">
        <xsl:param name="type" />
        <xsl:param name="profession-or-occupation" />
        <xsl:param name="record-types" />
         
        <xsl:choose>
            <xsl:when test="not($type) and exml:is-fictive($profession-or-occupation, $fictive-persons)">
                <datafield tag="075" ind1=" " ind2=" ">
                    <subfield code="b">p</subfield>
                    <subfield code="2">gndgen</subfield>
                </datafield>
                <datafield tag="075" ind1=" " ind2=" ">
                    <subfield code="b">pxl</subfield>
                    <subfield code="2">gndspec</subfield>
                </datafield>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="types" select="gndo:record-types($record-types, 'person', $type)"/>
                <xsl:for-each select="$types">
                    <xsl:variable name="tokens" select="tokenize(., ':')"/>
                    <datafield tag="075" ind1=" " ind2=" ">
                        <subfield code="b"><xsl:value-of select="$tokens[2]"/></subfield>
                        <subfield code="2"><xsl:value-of select="$tokens[1]"/></subfield>
                    </datafield>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <!-- {{ 100 und 700: gndo:preferredName }} -->
    <xsl:template match="exml:person/gndo:preferredName">
        <xsl:variable name="type" select="parent::exml:person/@gndo:type"/>
        <xsl:variable name="occupation-gnd-uris" select="parent::exml:person/gndo:professionOrOccupation/@gndo:ref"/>
        <xsl:variable name="add-names-titles-terretory" select="(gndo:nameAddition, gndo:epithetGenericNameTitleOrTerritory)"/>
        <xsl:variable name="is-original" select="@type = 'original'"/>
        <xsl:variable name="tag" select="if ($is-original) then ('700') else ('100')"/>
        <xsl:variable name="ind2" select="if ($is-original) then ('4') else (' ')"/>
        
        <datafield tag="{$tag}" ind1="1" ind2="{$ind2}">
            <xsl:call-template name="property-mode"/>
            <subfield code="a">
                <xsl:call-template name="person-name-structure"/>
            </subfield>
            <!-- Hack, damit alle elemente, wenn sie im GND Namespace sitzen genommen werden. Das hier sollte ich nochmal überdenken -->
            <xsl:apply-templates select="gndo:*[not(local-name() = ('personalName', 'forename', 'surname', 'prefix'))]" />
            <xsl:call-template name="person-name-parts-sub_c">
                <xsl:with-param name="type" select="$type"/>
                <xsl:with-param name="occupation-gnd-uris" select="$occupation-gnd-uris"/>
                <xsl:with-param name="add-names" select="$add-names-titles-terretory"/>
            </xsl:call-template>
            <xsl:if test="parent::node()/gndo:dateOfBirth[@iso-date] or parent::node()/gndo:dateOfDeath[@iso-date]">
                <subfield code="d"><xsl:value-of select="exml:years-of-living(parent::node()/gndo:dateOfBirth/@iso-date, parent::node()/gndo:dateOfDeath/@iso-date)"/></subfield>
            </xsl:if>
            <xsl:if test="@script">
                <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
            </xsl:if>
            <xsl:if test="@xml:lang">
                <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
            </xsl:if>
            <xsl:if test="$is-original">
                <subfield code="9">v:Original</subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!--{* 100, 400 *}: Name Parts-->    
    <xsl:template name="person-name-structure">
        <xsl:variable name="rtl" select="('Adlam', 'Arab', 'Armi', 'Avst', 'Cprt', 'Elym', 'Hatr', 'Hebr', 'Hung', 'Khar', 'Lydi', 'Mand', 'Mani', 'Mend', 'Merc', 'Mero', 'Narb', 'Nbat', 'Nkoo', 'Orkh', 'Palm', 'Phli', 'Phlp', 'Phnx', 'Prti', 'Rohg', 'Sarb', 'Sogo', 'Syrc', 'Thaa', 'Yezi')"/>
        <xsl:choose>
            <xsl:when test="gndo:personalName">
                <xsl:apply-templates select="gndo:personalName" />
            </xsl:when>
            <xsl:when test="gndo:surname and gndo:forename">
                <xsl:choose>
                    <xsl:when test="@script = $rtl">
                        <xsl:variable name="name" select="data(gndo:surname)||', '||data(gndo:forename)"/>
                        <!--<xsl:text>VOR</xsl:text>-->
                        <!--<xsl:apply-templates select="gndo:forename" mode="rtl"/>-->
                        <!--<xsl:text>NACH</xsl:text>-->
                        <!--<xsl:apply-templates select="gndo:surname" mode="rtl"/>-->
                        <xsl:value-of select="$name"/>
                        <!--<xsl:text>PREF</xsl:text>-->
                        <xsl:apply-templates select="gndo:prefix" mode="rtl"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="gndo:surname" />
                        <xsl:apply-templates select="gndo:forename" />
                        <xsl:apply-templates select="gndo:prefix" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="exml:person//gndo:surname | exml:person//gndo:personalName">
        <xsl:value-of select="normalize-space(./text())"/>
    </xsl:template>
    
    <xsl:template match="exml:person//gndo:forename">
        <xsl:text>, </xsl:text>
        <xsl:value-of select="normalize-space(./text())"/>
    </xsl:template>
    
    <xsl:template match="exml:person//gndo:prefix">
        <xsl:text> &#152;</xsl:text>
        <xsl:value-of select="normalize-space(./text())"/>
        <xsl:text>&#156;</xsl:text>
    </xsl:template>
    
    <!--<xsl:template match="exml:person//gndo:surname" mode="rtl">
        <xsl:text> ,</xsl:text>
        <xsl:value-of select="normalize-space(./text())"/>
    </xsl:template>
    
    <xsl:template match="exml:person//gndo:forename | exml:person//gndo:personalName" mode="rtl">
        <xsl:value-of select="normalize-space(./text())"/>
    </xsl:template>-->
    
    <xsl:template match="exml:person//gndo:prefix" mode="rtl">
        <xsl:text>&#152;</xsl:text>
        <xsl:value-of select="normalize-space(./text())"/>
        <xsl:text>&#156;</xsl:text>
    </xsl:template>
    
    <!-- {* 100, 400 $b *}: gndo:counting as part of the name -->
    <xsl:template match="exml:person//gndo:counting">
        <xsl:variable name="value" select="normalize-space(./text())"/>
        <subfield code="b" xmlns="http://www.loc.gov/MARC21/slim">
            <xsl:value-of select="$value"/>
            <xsl:choose>
                <xsl:when test="matches($value, '[IVXLCDMA0-9]+?') and not(substring($value, string-length($value), 1)='.')">
                    <xsl:text>.</xsl:text>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
        </subfield>
    </xsl:template>
    
    <!--{* 100, 400 $l *}: Typing and additional roles -->
    <xsl:template name="person-name-parts-sub_c">
        <xsl:param name="type"/>
        <xsl:param name="occupation-gnd-uris"/>
        <xsl:param name="add-names"/>
        
        <xsl:variable name="fictive-uris" select="exml:fictive-uris(($type, $occupation-gnd-uris), $fictive-persons)"/>
        <xsl:variable name="is-fictive" select="count($fictive-uris) &gt; 0"/>
        <xsl:variable name="adds" select="string-join( for $item in $add-names return normalize-space($item), ', ' )"/>
        
        <xsl:variable name="value">
            <xsl:value-of select="$adds"/>
            <xsl:if test="$is-fictive and not($adds = '')"><xsl:text>, </xsl:text></xsl:if>
            <xsl:if test="$is-fictive">Fiktive Gestalt</xsl:if>
        </xsl:variable>
        
        <xsl:if test="not(normalize-space($value)='')">
            <!--
            <subfield is-fictive="{$is-fictive}" code="c" xmlns="http://www.loc.gov/MARC21/slim">
                <xsl:value-of select="$value"/>
                <type><xsl:copy-of select="$type"></xsl:copy-of></type>
                <occ><xsl:copy-of select="$occupation-gnd-uris"></xsl:copy-of></occ>
                <fic>
                    <xsl:copy-of select="$fictive-uris"></xsl:copy-of>
                </fic>
            </subfield>
            -->
            <subfield code="c"><xsl:value-of select="$value"/></subfield>
        </xsl:if>
    </xsl:template>
    
    
    <!-- {{ 375: gndo:gender }} -->
    <xsl:template match="exml:person/gndo:gender[@gndo:term]">
        <xsl:variable name="gender" select="tokenize(data(@gndo:term), ' ')"/>
        <xsl:variable name="codes">
            <gender>
                <xsl:for-each select="$gender">
                    <code>
                        <xsl:choose>
                            <xsl:when test=". = 'https://d-nb.info/standards/vocab/gnd/gender#female'">2</xsl:when>
                            <xsl:when test=". = 'https://d-nb.info/standards/vocab/gnd/gender#male'">1</xsl:when>
                            <xsl:when test=". = 'https://d-nb.info/standards/vocab/gnd/gender#notKnown'">0</xsl:when>
                            <xsl:otherwise>9</xsl:otherwise>
                        </xsl:choose>
                    </code>
                </xsl:for-each>
            </gender>
        </xsl:variable>
        
        <datafield tag="375" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="string-join($codes//*:code/text(), ';')"/></subfield>
            <subfield code="2">iso5218</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 400: gndo:variantName }} -->
    <xsl:template match="exml:person/gndo:variantName">
        <xsl:variable name="type" select="parent::exml:person/@gndo:type"/>
        <xsl:variable name="occupation-gnd-uris" select="parent::exml:person/gndo:professionOrOccupation/@gndo:ref"/>
        <xsl:variable name="add-names-titles-terretory" select="(gndo:nameAddition, gndo:epithetGenericNameTitleOrTerritory)"/>
        
        <datafield tag="400" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a">
                <xsl:call-template name="person-name-structure"/>
            </subfield>
            <!-- Hack, damit alle elemente, wenn sie im GND Namespace sitzen genommen werden. Das hier sollte ich nochmal überdenken -->
            <xsl:apply-templates select="gndo:*[not(local-name() = ('personalName', 'forename', 'surname', 'prefix'))]" />
            <!--<xsl:apply-templates select="gndo:*[not(name() = ('gndo:personalName', 'gndo:forename', 'gndo:surname', 'gndo:prefix', 'gndo:nameAddition', 'gndo:epithetGenericNameTitleOrTerritory'))]" />-->
            <xsl:call-template name="person-name-parts-sub_c">
                <xsl:with-param name="type" select="$type"/>
                <xsl:with-param name="occupation-gnd-uris" select="$occupation-gnd-uris"/>
                <xsl:with-param name="add-names" select="$add-names-titles-terretory"/>
            </xsl:call-template>
            <xsl:if test="parent::node()/gndo:dateOfBirth[@iso-date] or parent::node()/gndo:dateOfDeath[@iso-date]">
                <subfield code="d"><xsl:value-of select="exml:years-of-living(parent::node()/gndo:dateOfBirth/@iso-date, parent::node()/gndo:dateOfDeath/@iso-date)"/></subfield>
            </xsl:if>
            <xsl:if test="@script">
                <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
            </xsl:if>
            <xsl:if test="@xml:lang">
                <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 500: gndo:pseudonym }} -->    
    <xsl:template match="exml:person/gndo:pseudonym">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">pseu</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#pseudonym</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Pseudonym</subfield>
            <subfield code="e">Pseudonym</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 500: gndo:acquaintanceshipOrFriendship }} -->    
    <xsl:template match="exml:person/gndo:acquaintanceshipOrFriendship">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref">
                <!--<xsl:with-param name="isil-over-sisc" select="true()"/>-->
                <xsl:with-param name="ignore-url" select="true()"/>
            </xsl:call-template>
            <subfield code="a">
                <xsl:call-template name="person-name-structure"/>
            </subfield>
            <subfield code="4">beza</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#acquaintanceshipOrFriendship</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Bekanntschaft</subfield>
            <subfield code="e">Bekanntschaft</subfield>
            <xsl:if test="note">
                <subfield code="9">v:<xsl:value-of select="note/text()"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 500: gndo:familialRelationship }} -->    
    <xsl:template match="exml:person/gndo:familialRelationship">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref">
                <!--<xsl:with-param name="isil-over-sisc" select="true()"/>-->
                <xsl:with-param name="ignore-url" select="true()"/>
            </xsl:call-template>
            <subfield code="a">
                <xsl:call-template name="person-name-structure"/>
            </subfield>
            <subfield code="4">bezf</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#familialRelationship</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Beziehung familiaer</subfield>
            <subfield code="e">Beziehung familiaer</subfield>
            <xsl:if test="note">
                <subfield code="9">v:<xsl:value-of select="note/text()"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 510: gndo:affiliation }} -->
    <xsl:template match="exml:person/gndo:affiliation">
        <datafield tag="510" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref">
                <!--<xsl:with-param name="isil-over-sisc" select="true()"/>-->
            </xsl:call-template>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">affi</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#affiliation</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Affiliation</subfield>
            <subfield code="e">Affiliation</subfield>
            <!--<subfield code="9"></subfield>-->
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 548: dateOfBirthAndDeath "Standard (datl)" }} -->
    <xsl:template name="dateOfBirthAndDeath.standard">
        <datafield tag="548" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode">
                <xsl:with-param name="nodes" select="(gndo:dateOfBirth, gndo:dateOfDeath)"></xsl:with-param>
            </xsl:call-template>
            <subfield code="a">
                <xsl:value-of select="tokenize(gndo:dateOfBirth/@iso-date, '-')[1]"/><xsl:text>-</xsl:text><xsl:value-of select="tokenize(gndo:dateOfDeath/@iso-date, '-')[1]"/>
            </subfield>
            <subfield code="4">datl</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfBirthAndDeath</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Lebensdaten</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 548: dateOfBirthAndDeath "EXACT (datx)" }} -->
    <xsl:template name="dateOfBirthAndDeath.exact">
        <xsl:variable name="dateOfBirth-iso" select="gndo:dateOfBirth/@iso-date" />
        <xsl:variable name="dateOfDeath-iso" select="gndo:dateOfDeath/@iso-date" />
        <xsl:if test="count(tokenize($dateOfBirth-iso, '-')) &gt; 1 and count(tokenize($dateOfBirth-iso, '-')) &gt; 1">
            <xsl:variable name="dateOfBirth" select="if($dateOfBirth-iso) then(gndo:iso-date2dotted-date( $dateOfBirth-iso )) else()"/>
            <xsl:variable name="dateOfDeath" select="if($dateOfDeath-iso) then (gndo:iso-date2dotted-date( $dateOfDeath-iso )) else ()"/>
            <datafield tag="548" ind1=" " ind2=" ">
                <xsl:call-template name="property-mode">
                    <xsl:with-param name="nodes" select="(gndo:dateOfBirth, gndo:dateOfDeath)"></xsl:with-param>
                </xsl:call-template>
                <subfield code="a">
                    <xsl:value-of select="$dateOfBirth"/><xsl:text>-</xsl:text><xsl:value-of select="$dateOfDeath"/>
                </subfield>
                <subfield code="4">datx</subfield>
                <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfBirthAndDeath</subfield>
                <subfield code="w">r</subfield>
                <subfield code="i">Exakte Lebensdaten</subfield>
            </datafield>
        </xsl:if>
    </xsl:template>
    
    
    <!-- {{ 548: gndo:periodOfActivity }} -->
    <xsl:template match="exml:person/gndo:periodOfActivity">
        <xsl:variable name="from" select="normalize-space(data(@iso-from))"/>
        <xsl:variable name="to" select="normalize-space(data(@iso-to))"/>
        <xsl:variable name="is-datz">
            <xsl:choose>
                <xsl:when test="($from = $to) and matches($from, '\d{4}-\d{2}-\d{2}')">true</xsl:when>
                <xsl:otherwise>false</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <datafield tag="548" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a">
                <xsl:choose>
                    <!-- When both years are identical and a full date -->
                    <xsl:when test="$is-datz = 'true'">
                        <xsl:variable name="tokens" select="tokenize($from, '-')"/>
                        <xsl:value-of select="$tokens[3]"/><xsl:text>.</xsl:text><xsl:value-of select="$tokens[2]"/><xsl:text>.</xsl:text><xsl:value-of select="$tokens[1]"/>
                    </xsl:when>
                    <!-- When both years are identical -->
                    <xsl:when test="$from = $to">
                        <xsl:value-of select="$from"/>
                    </xsl:when>
                    <xsl:otherwise><xsl:value-of select="$from"/><xsl:text>-</xsl:text><xsl:value-of select="$to"/></xsl:otherwise>
                </xsl:choose>
            </subfield>
            <subfield code="4">
            <xsl:choose>
                <xsl:when test="$is-datz = 'true'">datz</xsl:when>
                <xsl:otherwise>datw</xsl:otherwise>
            </xsl:choose>
            </subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#periodOfActivity</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Wirkungszeitraum</subfield>
            <xsl:if test="note">
                <subfield code="9">v:<xsl:value-of select="note/text()"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {* 550: RELATIONS *} -->
    <!-- {{ 550: gndo:academicDegree }} -->
    <xsl:template match="exml:person/gndo:academicDegree">
        <datafield tag="550" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">akad</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#academicDegree</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Akademischer Grad</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 550: gndo:fieldOfStudy }} -->
    <xsl:template match="exml:person/gndo:fieldOfStudy">
        <datafield tag="550" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">stud</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#fieldOfStudy</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Studienfach</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 550: gndo:functionOrRole }} DEPRECATED in GND since 2017 -->
    <!--
    <xsl:template match="exml:person/gndo:functionOrRole[@gndo:term]">
        <datafield tag="550" ind1=" " ind2=" ">
            <!-/-<xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>-/->
            <subfield code="a"><xsl:value-of select="normalize-space(data(@gndo:term))"/></subfield>
            <subfield code="4">funk</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#functionOrRole</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Funktion oder Rolle</subfield>
        </datafield>
    </xsl:template>
    -->
    
    <!-- {{ 550: gndo:playedInstrument }} -->
    <xsl:template match="exml:person/gndo:playedInstrument">
        <xsl:variable name="text" select="if (label) then (normalize-space(label/text())) else (normalize-space(text()))"/>
        <datafield tag="550" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="$text"/></subfield>
            <xsl:if test="note">
                <subfield code="g"><xsl:value-of select="normalize-space(note/text())"/></subfield>
            </xsl:if>
            <subfield code="4">istr</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#playedInstrument</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Instrument</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 550: gndo:professionOrOccupation }} -->
    <xsl:template match="exml:person/gndo:professionOrOccupation">
        <xsl:variable name="is-fictive" select="exml:is-fictive(@gndo:ref, $fictive-persons)"/>
        <datafield tag="550" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:choose>
                <xsl:when test="$is-fictive">
                    <subfield code="4">obin</subfield>
                    <subfield code="4">http://d-nb.info/standards/elementset/gnd#broaderTermInstantial</subfield>
                    <subfield code="w">r</subfield>
                    <subfield code="i">Oberbegriff instantiell</subfield>
                </xsl:when>
                <xsl:otherwise>
                    <subfield code="4">
                        <xsl:choose>
                            <xsl:when test="@gndo:type='significant'">berc</xsl:when>
                            <xsl:otherwise>beru</xsl:otherwise>
                        </xsl:choose>
                    </subfield>
                    <subfield code="4">https://d-nb.info/standards/elementset/gnd#professionOrOccupation</subfield>
                    <subfield code="w">r</subfield>
                    <subfield code="i">
                        <xsl:choose>
                            <xsl:when test="@gndo:type='significant'">Charakteristischer Beruf</xsl:when>
                            <xsl:otherwise>Beruf</xsl:otherwise>
                        </xsl:choose>
                    </subfield>
                </xsl:otherwise>
            </xsl:choose>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 550: gndo:titleOfNobility }} -->
    <xsl:template match="exml:person/gndo:titleOfNobility">
        <datafield tag="550" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">adel</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#titleOfNobility</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Adelstitel</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {* 551: PLACES *} -->
    <!-- {{ 551: gndo:placeOfActivity }} -->
    <xsl:template match="exml:person/gndo:placeOfActivity">
        <datafield tag="551" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">ortw</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfActivity</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Wirkungsort</subfield>
            <xsl:variable name="iso-date" select="@iso-date" />
            <xsl:variable name="iso-from" select="@iso-from" />
            <xsl:variable name="iso-to" select="@iso-to" />
            <xsl:if test="count(($iso-date, $iso-from, $iso-to)) > 0">
                <subfield code="9">
                    <xsl:text>Z:</xsl:text>
                    <xsl:call-template name="datable-field">
                        <xsl:with-param name="node" select="."/>
                    </xsl:call-template>
                </subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 551: gndo:placeOfBirth }} -->
    <xsl:template match="exml:person/gndo:placeOfBirth">
        <datafield tag="551" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">ortg</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfBirth</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Geburtsort</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 551: gndo:placeOfDeath }} -->
    <xsl:template match="exml:person/gndo:placeOfDeath">
        <datafield tag="551" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">orts</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfDeath</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Sterbeort</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 551: gndo:placeOfExile }} -->
    <xsl:template match="exml:person/gndo:placeOfExile">
        <datafield tag="551" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4">ortx</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#placeOfExile</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Exil</subfield>
        </datafield>
    </xsl:template>
    
    
    
    <!-- {* 672: PUBLICATIONS *} -->
    <!-- ########### ACHTUNG!!!! `gndo:publication` ist hochgradig experimentell. Mal schauen, ob man das nicht einfacher lösen kann ########### -->
    
    <!-- {{ 672: gndo:publication DEFAULT "AUTHOR" }} -->
    <xsl:template match="exml:person/gndo:publication">
        <datafield tag="672" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref">
                <xsl:with-param name="ignore-url" select="true()"/>
            </xsl:call-template>
            <!-- Transform the title -->
            <xsl:call-template name="publication-title"/>
            <!-- transform all other elements -->
            <xsl:apply-templates select="exml:add" mode="publication-elements" />
            <xsl:apply-templates select="exml:date" mode="publication-elements" />
            <xsl:apply-templates select="exml:ref" mode="publication-elements" />
            <xsl:apply-templates select="exml:idno" mode="publication-elements" />
        </datafield>
    </xsl:template>
    
    <xsl:template name="publication-title">
        <xsl:variable name="title">
            <xsl:choose>
                <xsl:when test="exml:title">
                    <xsl:apply-templates select="exml:title" mode="publication-title-elements"></xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(string-join(text(), ' '))"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <subfield code="a">
            <xsl:choose>
                <!-- {{ 672: gndo:publication "EDITOR" }} -->
                <xsl:when test="@role ='editor'">
                    <xsl:text>Hrsg. von: </xsl:text><xsl:value-of select="$title"/>
                </xsl:when>
                <!-- {{ 672: gndo:publication "ABOUT" }} -->
                <xsl:when test="@role ='about'">
                    <xsl:apply-templates select="exml:author|gndo:firstAuthor" mode="publication-title-elements"/>
                    <xsl:value-of select="$title"/>
                </xsl:when>
                <!-- {{ 672: gndo:publication DEFAULT "AUTHOR" }} -->
                <xsl:otherwise>
                    <xsl:value-of select="$title"/>
                </xsl:otherwise>
            </xsl:choose>
        </subfield>
    </xsl:template>
    
    <xsl:template match="exml:title" mode="publication-title-elements">
        <xsl:value-of select="normalize-space(string-join(text(), ' '))"/>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication/exml:author | exml:person/gndo:publication/gndo:firstAuthor" mode="publication-title-elements">
        <xsl:value-of select="normalize-space(text())"/>
        <xsl:text>: </xsl:text>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication/exml:add" mode="publication-elements">
        <subfield code="b"><xsl:value-of select="normalize-space(text())"/></subfield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication/exml:date" mode="publication-elements">
        <subfield code="f"><xsl:value-of select="normalize-space(text())"/></subfield>
    </xsl:template>
    
    <xsl:template match="exml:person/gndo:publication/exml:idno[@type]" mode="publication-elements">
        <subfield code="1"><xsl:value-of select="normalize-space(text())"/></subfield>
    </xsl:template>
    
    <!--<xsl:template match="exml:person/gndo:publication/exml:ref[@target]" mode="publication-elements">
        <subfield code="1"><xsl:value-of select="data(@target)"/></subfield>
    </xsl:template>-->
    
    <xsl:template match="element()" mode="publication-elements"/>
    
    <!-- ########### ACHTUNG!!!! ENDE ###########-->
    
</xsl:stylesheet>