<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs geo gndo marc" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="3.0">
    
    <!-- parameter -->
    <xsl:param name="agency-isil">DE-000</xsl:param> <!-- Pseudo default ISIL -->
    <xsl:param name="cataloging-level">gnd4</xsl:param>
    <xsl:param name="records"/> <!-- record agency modes separated by whitespace: 'create', 'update', 'merge' -->
    <xsl:param name="ignore-records" select="true()"/> <!-- records with @agency='ignore' will be ignored when converting without a specified record agency mode -->
    <xsl:param name="base-uri">http://sub.uni-goettingen.de/gnd-agentur/entity/</xsl:param>
    <xsl:param name="requests" select="true()"/>
    <xsl:param name="marc-schematron-url">https://gitlab.gwdg.de/entities/gnd-marc-xml/-/raw/master/schemas/schematron/marc-authority-gnd.sch</xsl:param>
    
    <!-- Import named template module, global functions module and mapping module -->
    <xsl:import href="utils/functs.xsl"/>
    <xsl:import href="utils/mappings.xsl"/>
    
    <!-- Include globals conversion modules -->
    <xsl:include href="defaults.marcxml.inc.xsl"/>
    
    <!-- Include all Class conversion modules -->
    <xsl:include href="event.marcxml.inc.xsl"/>
    <xsl:include href="corporateBody.marcxml.inc.xsl"/>
    <xsl:include href="person.marcxml.inc.xsl"/>
    <xsl:include href="place.marcxml.inc.xsl"/>
    <xsl:include href="work.marcxml.inc.xsl"/>
    
    
    
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    
    
    <!-- 
        INITAL STEP: Convert all collections to MARC-XML 
        #############################################################################
    -->
    <xsl:template match="/">
        <xsl:processing-instruction name="xml-model">href="https://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
        <xsl:text>&#xa;</xsl:text>
        <xsl:variable name="conversion">
            <xsl:apply-templates />
        </xsl:variable>
        <xsl:apply-templates select="$conversion" mode="sort-props" />
    </xsl:template>
    
    <xsl:template match="collection">
        <collection xmlns="http://www.loc.gov/MARC21/slim" 
            xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.loc.gov/MARC21/slim https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/marc21/MARC21entityxml.xsd?ref_type=heads">
            <xsl:call-template name="dataset-id"/>
            <xsl:variable name="all-records" select="data/list/element()[not(name() = ('title', 'abstract'))][@xml:id]"/>
            <xsl:variable name="modes" select="distinct-values((sort(distinct-values(for $mode in $all-records/@agency return tokenize($mode, ' '))), 'ignore'))"/>
            <xsl:variable name="target-records">
                <xsl:choose>
                    <xsl:when test="empty($records) or not(normalize-space($records) = '')">
                        <xsl:copy-of select="$all-records[@agency = tokenize($records)]"/>
                    </xsl:when>
                    <xsl:when test="$ignore-records = true()">
                        <xsl:copy-of select="$all-records[not(@agency = 'ignore')]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="$all-records"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:apply-templates select="$target-records/element()[@agency]"/>
            <xsl:apply-templates select="$target-records/element()[not(@agency)]"/>
        </collection>
    </xsl:template>
    
    <!-- Creates the dataset ID incl. Timestamp -->
    <xsl:template name="dataset-id">
        <xsl:variable name="current-dateTime" select="current-dateTime()"/>
        <xsl:variable name="time-stamp" select="format-dateTime($current-dateTime,'[Y0001][M01][D01]T[H01][m02][s02]')"/>
        <xsl:choose>
            <xsl:when test="@id">
                <xsl:attribute name="id" select="data(@id)||'_'||$time-stamp" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="id" select="data(metadata/provider/@id)||'_'||$time-stamp" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="node()|@*">
        <xsl:apply-templates select="node()|@*"/>
    </xsl:template>
    


    
    <!-- 
        END-STEP: Sort all datafields by tag 
        #############################################################################
    -->
    <xsl:template match="/" mode="sort-props">
        <xsl:apply-templates mode="sort-props"/>
    </xsl:template>
    
    <xsl:template match="marc:record" mode="sort-props">
        <xsl:copy>
            <xsl:apply-templates select="@*" mode="sort-props" />
            <xsl:copy-of select="marc:leader" />
            <xsl:apply-templates select="marc:controlfield" mode="sort-props"/>
            <xsl:apply-templates select="marc:datafield" mode="sort-props">
                <xsl:sort select="data(./@tag)" />
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="@*|node()" mode="sort-props">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" mode="sort-props" />
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>