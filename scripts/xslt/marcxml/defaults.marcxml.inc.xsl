<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="exml foaf geo gndo marc owl skos xs" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns="http://www.loc.gov/MARC21/slim"
    version="2.0">
    
    <!-- 
        INCLUDE Module (not standalone) 
        *******************************
    -->
    <xsl:import href="utils/functs.xsl"/>
    
    
    <!--
        *************************************************************
        FUNCTIONS
        *************************************************************
    -->
    
    <xsl:function name="marc:create-008-string">
        <xsl:param name="record" />
        <xsl:variable name="creation-date">
            <xsl:choose>
                <xsl:when test="$record/revision/change">
                    <xsl:value-of select="format-date(xs:date(data($record/revision/change[last()]/@when)), '[Y01]-[M01]-[D01]')"/>
                </xsl:when>
                <xsl:when test="$record/ancestor::collection/metadata/revision/change">
                    <xsl:value-of select="format-date(xs:date(data($record/ancestor::collection/metadata/revision[1]/change[last()]/@when)), '[Y01]-[M01]-[D01]')"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="xs:string(format-date(current-date(), '[Y01]-[M01]-[D01]'))"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="p00-05">                            <!-- 00-05 = Datum der Ersterfassung im Format JJMMTT -->
            <xsl:value-of select="translate($creation-date, '-', '')"/>
        </xsl:variable>              
        <xsl:variable name="p06">n</xsl:variable>               <!-- 06 Direkte oder indirekte geografische Unterteilung: n = nicht anwendbar -->
        <xsl:variable name="p07">|</xsl:variable>               <!-- 07 Umschriftsstandard: | = keine Angabe -->
        <xsl:variable name="p08">|</xsl:variable>               <!-- 08 Sprache des Katalogs: | = keine Angabe -->
        <xsl:variable name="p09">a</xsl:variable>               <!-- 09 Art des Datensatzes: a = default, b = Hinweissatz -->
        <xsl:variable name="p10">z</xsl:variable>               <!-- 10 Formalerschließungsregeln: z = andere -->
        <xsl:variable name="p11">z</xsl:variable>               <!-- 11 Schlagwortkatalog/Thesaurus: z = andere, n = nicht anwendbar -->
        <xsl:variable name="p12">n</xsl:variable>               <!-- 12 Art des Gesamttitels: n = nicht anwendbar -->
        <xsl:variable name="p13">n</xsl:variable>               <!-- 13 gezählter oder ungezählter Gesamttitels: n = nicht anwendbar -->
        <xsl:variable name="p14">a</xsl:variable>               <!-- 14 Verwendung der Ansetzung - Haupt- oder Nebeneintragung: a = geeignet, b = nicht geeignet -->
        <xsl:variable name="p15">|</xsl:variable>               <!-- 15 Verwendung der Anseztung - Nebeneintragung unter einem Schlagwortkatalog/Thesaurus: a = wenn als Schlagwort verwendet, b = wenn nicht als Schlagwort verwendet, | = keine Angabe möglich -->
        <xsl:variable name="p16">n</xsl:variable>               <!-- 16 Verwendung der Ansetzung - Nebeneintragung unter dem Gesamttitels: n = nicht anwendbar -->
        <xsl:variable name="p17">n</xsl:variable>               <!-- 17 Art der Schlagwortunterteilung: n = nicht anwendbar -->
        <xsl:variable name="p18-27" xml:space="preserve">          </xsl:variable>  <!-- nicht definiert - 10 Leerzeichen -->
        <xsl:variable name="p28" xml:space="preserve"> </xsl:variable>  <!-- 28 Art der amtlichen Agentur: o amtliche Agentur - Art unbestimmt, Leerzeichen = keine amtliche Agentur (Default-Wert ist das Leerzeichen) -->
        <xsl:variable name="p29">|</xsl:variable>               <!-- 29 Verweisungsbewertung: | = keine Angabe -->
        <xsl:variable name="p30" xml:space="preserve"> </xsl:variable>               <!-- nicht definiert - 1 Leerzeichen -->
        <xsl:variable name="p31">a</xsl:variable>               <!-- 31 Datensatz in Bearbeitung: a = Datensatz kann verwendet werden -->
        <xsl:variable name="p32">                               <!-- 32 Nicht individualisierter Personenname: n = default, a = Individualisierter Personenname, b = nicht-individulaisierter Personenname -->
            <xsl:choose>
                <xsl:when test="$record[self::person]">a</xsl:when>
                <xsl:otherwise>n</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="p33">n</xsl:variable>               <!-- 33 Level der Normierung: a = vollständige Normierung, c = provisorisch, n = nicht anwendbar -->
        <xsl:variable name="p34-37" xml:space="preserve">    </xsl:variable>  <!-- nicht definiert - 4 Leerzeichen -->
        <xsl:variable name="p38">|</xsl:variable>               <!-- 38 Modifizierter Datensatz: | = keine Angabe -->
        <xsl:variable name="p39">u</xsl:variable>               <!-- 39 Katalogisierungsquelle: c = Kooperatives Katalogisierungsprogramm, d = other, u = unknown -->
        <xsl:value-of select="concat(
            $p00-05, 
            $p06, 
            $p07,
            $p08, 
            $p09, 
            $p10, 
            $p11, 
            $p12, 
            $p13, 
            $p14, 
            $p15,
            $p16,
            $p17,
            $p18-27,
            $p28,
            $p29,
            $p30,
            $p31,
            $p32,
            $p33,
            $p34-37,
            $p38,
            $p39)"/>
    </xsl:function>
    
    
    <xsl:function name="marc:create-leader-string">
        <xsl:param name="record" />
        <xsl:variable name="p00-04">00000</xsl:variable>                    <!-- 00-04 = Länge des Datensatzes - 00000 -->
        <xsl:variable name="p05">                                           <!-- 05 = Status des Datensatzes -->
            <xsl:choose>
                <xsl:when test="$record/@agency = 'create'">n</xsl:when>
                <!-- Umlenkung "c" implementieren -->
                <xsl:when test="$record/@agency = 'merge'">c</xsl:when>
                <xsl:when test="$record/@agency = 'remove'">d</xsl:when>
                <xsl:otherwise><xsl:text> </xsl:text></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="p06">z</xsl:variable>                           <!-- 06 = Art des Datensatzes -->
        <xsl:variable name="p07-08" xml:space="preserve">  </xsl:variable>  <!-- 07-08 = nicht definiert - immer 00 -->
        <xsl:variable name="p09">a</xsl:variable>                           <!-- 09 = Zeichenkodierungsschema UTF-8 -->
        <xsl:variable name="p10">2</xsl:variable>                           <!-- 10 = Indikatorzähler - immer 2 -->
        <xsl:variable name="p11">2</xsl:variable>                           <!-- 11 = Unterfeldcode-Länge - immer 2 -->
        <xsl:variable name="p12-16">00000</xsl:variable>                    <!-- 12-16 = Datenanfangsadresse-Länge - immer 2 -->
        <xsl:variable name="p17">n</xsl:variable>                           <!-- 17 = Vollständigkeit des Datensatzes -->
        <xsl:variable name="p18">c</xsl:variable>                           <!-- 18 = Interpunktion -->
        <xsl:variable name="p19" xml:space="preserve"> </xsl:variable>      <!-- 19 = nicht definiert - immer Leerzeichen -->
        <xsl:variable name="p20">4</xsl:variable>                           <!-- 20 = Länge des Feldlängenabschnitts - immer 4 -->
        <xsl:variable name="p21">5</xsl:variable>                           <!-- 21 = Länge des Zeichenanfangspositionsabeschnittes - immer 5 -->
        <xsl:variable name="p22">0</xsl:variable>                           <!-- 22 = Lände des anwendungsdefinierten Abschnittes - immer 0 -->
        <xsl:variable name="p23">0</xsl:variable>                           <!-- 23 = nicht definiert - immer 0 -->
        <xsl:value-of select="concat(
            $p00-04, 
            $p05, 
            $p06, 
            $p07-08, 
            $p09, 
            $p10, 
            $p11, 
            $p12-16,
            $p17,
            $p18,
            $p19,
            $p20,
            $p21,
            $p22,
            $p23)"/>
    </xsl:function>
    
    
    <xsl:function name="owl:parse-url">
        <xsl:param name="url" />
        <xsl:variable name="data">
            <xsl:choose>
                <xsl:when test="contains($url, 'lccn.loc.gov')">
                    <xsl:text>lccn</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'lccn.loc.gov/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'id.loc.gov/authorities/names')">
                    <xsl:text>lcnaf</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'authorities/names/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'viaf.org')">
                    <xsl:text>viaf</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'viaf/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'orcid.org')">
                    <xsl:text>orcid</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'orcid.org/')"/>
                </xsl:when>
                <xsl:when test="contains($url, 'wikidata.org')">
                    <xsl:text>wikidata</xsl:text>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="substring-after($url, 'wikidata.org/wiki/')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>:</xsl:text>
                    <xsl:value-of select="tokenize($url, '/')[last()]"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="string-join($data)"/>
    </xsl:function>
    
    
    <!-- 
        *************************************************************
        TEMPLATES
        *************************************************************
    -->
    
    <xsl:template name="default-record">
        <xsl:variable name="agency-metadata" select="(./ancestor::collection)[1]/metadata/agency"/>
        <xsl:variable name="ag-isil">
            <xsl:choose>
                <xsl:when test="$agency-metadata and $agency-metadata[@isil]">
                    <xsl:value-of select="data($agency-metadata/@isil)"/>
                </xsl:when>
                <xsl:when test="$agency-isil">
                    <xsl:value-of select="$agency-isil"/>
                </xsl:when>
                <xsl:otherwise />
            </xsl:choose>
        </xsl:variable>
        
        <record type="Authority" xmlns="http://www.loc.gov/MARC21/slim">
            <!-- 1. Check for agency mode ("create", "update", "ignore") -->
            <xsl:call-template name="record-controll-attributes"/>
            
            <xsl:call-template name="leader-controlfields">
                <xsl:with-param name="agency-isil" select="$ag-isil"/>
            </xsl:call-template>
            <xsl:call-template name="df_667_PK">
                <xsl:with-param name="agency-isil" select="$ag-isil"/>
            </xsl:call-template>
            
            <xsl:variable name="types" select="gndo:record-types($gndo:record-types, ./local-name(), ./@gndo:type)"/>
            <xsl:apply-templates select="@*"/>
            <xsl:call-template name="df_040">
                <xsl:with-param name="agency-isil" select="$ag-isil"/>
            </xsl:call-template>
            <xsl:call-template name="df_042">
                <xsl:with-param name="level" select="$cataloging-level"/>
            </xsl:call-template>
            <xsl:call-template name="df_079"/>
            
            <xsl:for-each select="$types">
                <xsl:variable name="tokens" select="tokenize(., ':')"/>
                <datafield tag="075" ind1=" " ind2=" ">
                    <subfield code="b"><xsl:value-of select="$tokens[2]"/></subfield>
                    <subfield code="2"><xsl:value-of select="$tokens[1]"/></subfield>
                </datafield>
            </xsl:for-each>
            <xsl:apply-templates select="node()"/>
        </record>
    </xsl:template>
    
    
    <!-- {{ Leader and Controlfields }}
        $agency-isil (1): string representing an ISIL -->
    <xsl:template name="leader-controlfields">
        <xsl:param name="agency-isil"/>
        
        <xsl:call-template name="leader"/>
        <xsl:call-template name="cf-001"/>
        <xsl:call-template name="cf-003">
            <xsl:with-param name="agency-isil" select="$agency-isil"/>
        </xsl:call-template>
        <xsl:call-template name="cf-005"/>
        <xsl:call-template name="cf-008"/>
    </xsl:template>
    
    
    
    <!-- ######{{ LEADER }}###################################################################### -->
    <!-- {{ LEADER }} -->
    <xsl:template name="leader">
        <leader><xsl:value-of select="marc:create-leader-string(.)"/></leader>
    </xsl:template>
    
    
    
    <!-- ######{{ 00X CONTROLFIELDS }}###################################################################### -->
    <!-- {{ 001: CONTROLFIELD RECORD ID }} -->
    <xsl:template name="cf-001">
        <controlfield tag="001">
            <xsl:choose>
                <xsl:when test=".[@*:id]">
                    <xsl:value-of select="data(./@*:id)"/>
                </xsl:when>
                <xsl:when test=".[@gndo:uri]">
                    <xsl:text>local</xsl:text><xsl:value-of select="tokenize(data(./@gndo:uri), '/')[last()]"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="data(./@*:id)"/></xsl:otherwise>
            </xsl:choose>
        </controlfield>
    </xsl:template>
    
    
    <!-- {{ 003: CONTROLFIELD ISIL }}
        $agency-isil (1): string representing an ISIL -->
    <xsl:template name="cf-003">
        <xsl:param name="agency-isil"/>
        
        <controlfield tag="003">
            <xsl:variable name="agency-metadata" select="(./ancestor::collection)[1]/metadata/agency"/>
            <xsl:choose>
                <xsl:when test="$agency-metadata and $agency-metadata[@isil]">
                    <xsl:value-of select="data($agency-metadata/@isil)"/>
                </xsl:when>
                <xsl:when test="$agency-isil">
                    <xsl:value-of select="$agency-isil"/>
                </xsl:when>
                <xsl:otherwise />
            </xsl:choose>
        </controlfield>
    </xsl:template>
    
    
    <!-- {{ 005: RECORD CREATION DATE }} -->
    <xsl:template name="cf-005">
        <controlfield tag="005">
            <xsl:value-of select="format-dateTime(current-dateTime(), '[Y0001][M01][D01][H01][m01][s01].0')"/>
        </controlfield>
    </xsl:template>
    
    
    <!-- {{ 008: CONTROLSTRING }} -->
    <xsl:template name="cf-008">
        <controlfield tag="008">
            <xsl:value-of select="marc:create-008-string(.)"/>
        </controlfield>
    </xsl:template>
    
    
    
    <!-- ######{{ DATAFIELDS }}###################################################################### -->
    <!-- {{ 024: GND ID and URI }} -->
    <xsl:template match="collection/data/list/element()[@gndo:uri]/@gndo:uri">
        <xsl:variable name="id" select="tokenize(., '/')[last()]"/>
        <datafield tag="035" ind1=" " ind2=" ">
            <subfield code="a">(DE-588)<xsl:value-of select="$id"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 024: sameAs / other authority records of an entity }} -->
    <xsl:template match="owl:sameAs">
        <xsl:variable name="url" select="text()"/>
        <xsl:variable name="parsed" select="owl:parse-url($url)" />
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="@type"><xsl:value-of select="data(@type)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="tokenize($parsed, ':')[1]"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="id">
            <xsl:choose>
                <xsl:when test="@xml:id"><xsl:value-of select="data(@xml:id)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="tokenize($parsed, ':')[2]"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <datafield tag="024" ind1="7" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:if test="not($id = '')">
                <subfield code="a"><xsl:value-of select="$id"/></subfield>
            </xsl:if>
            <subfield code="0"><xsl:value-of select="$url"/></subfield>
            <xsl:if test="not($type = '')">
                <subfield code="2"><xsl:value-of select="$type"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 040: ISIL }} 
        $agency-isil (1): string representing an ISIL
    -->
    <xsl:template name="df_040">
        <xsl:param name="agency-isil"/>
        <xsl:variable name="provider-metadata" select="(./ancestor::collection)[1]/metadata/provider"/>
        <xsl:variable name="agency-metadata" select="(./ancestor::collection)[1]/metadata/agency"/>
        <xsl:variable name="isil">
            <xsl:choose>
                <xsl:when test="$agency-metadata and $agency-metadata[@isil]">
                    <xsl:value-of select="data($agency-metadata/@isil)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$agency-isil"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <datafield tag="040" ind1=" " ind2=" ">
            <subfield code="a">
                <xsl:choose>
                    <xsl:when test="$provider-metadata and $provider-metadata[@isil]">
                        <xsl:value-of select="data($provider-metadata/@isil)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$isil"/>
                    </xsl:otherwise>
                </xsl:choose>
            </subfield>
            <subfield code="9">
                <xsl:text>r:</xsl:text><xsl:value-of select="$isil"/>
            </subfield>
            <subfield code="b">ger</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 042: Katalogisierungslevel }} 
        $level (1): string representing the cataloging level
    -->
    <xsl:template name="df_042">
        <xsl:param name="level"/>
        
        <datafield tag="042" ind1=" " ind2=" ">
            <subfield code="a"><xsl:value-of select="$level"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 043: gndo:geographicAreaCode }} -->
    <!-- 
        Entscheidungslogik:
        *******************
        Wenn der Area Code von der GND nicht unterstützt wird, dann wird er ignoriert. Dass muss so sein, denn er kann so nicht eingespielt werden!
        Das kann im schlimmsten Fall dazu führen, dass ein ganzer Record nicht eingespielt werden kann, da kein gültiger Geographic Area Code mehr vorhanden ist. 
        Siehe hierzu:  https://wiki.dnb.de/x/O5FjBQ (unter Ländercode-Leitfaden und Ländercodes)
    -->
    <xsl:template match="gndo:geographicAreaCode[@gndo:term]">
        <xsl:variable name="term" select="normalize-space(substring-after(@gndo:term, 'geographic-area-code#'))"/>
        <xsl:variable name="unsupported-codes" select="('XA', 'XB', 'XC', 'XD', 'XE', 'XH', 'XI', 'XK', 'XL', 'XM', 'XN', 'XP', 'XQ', 'XR', 'XS', 'XT', 'XU', 'XV', 'XW', 'XX', 'XY', 'XZ')"/>
        <xsl:variable name="is-supported" select="not($term = $unsupported-codes)"/>
        <xsl:if test="$is-supported">
            <datafield tag="043" ind1=" " ind2=" ">
                <xsl:call-template name="property-mode"/>
                <subfield code="c"><xsl:value-of select="$term"/></subfield>
            </datafield>
        </xsl:if>
    </xsl:template>
    
    
    <!-- {{ 065: gndo:subjectCategory }} -->
    <xsl:template match="gndo:gndSubjectCategory">
        <xsl:variable name="notation" select="substring-after(data(@gndo:term), '#')"/>
        <datafield tag="065" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="$notation"/></subfield>
            <subfield code="2">sswd</subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 079: Teilbestandskennzeichen }} -->
    <xsl:template name="df_079">
        <datafield tag="079" ind1=" " ind2=" ">
            <subfield code="a"><xsl:text>g</xsl:text></subfield>
            <subfield code="q"><xsl:text>d</xsl:text></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 377: gndo:languageCode }} -->
    <xsl:template match="gndo:languageCode">
        <xsl:variable name="code" select="@gndo:code"/>
        <datafield tag="377" ind1=" " ind2="7">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="$code"/></subfield>
            <subfield code="2">
                <xsl:choose>
                    <xsl:when test="$code = ('sqi', 'hye', 'eus', 'mya', 'zho', 'ces', 'nld', 'fra', 'kat', 'deu', 'ell', 'isl', 'mkd', 'msa', 'mri', 'fas', 'ron', 'slk', 'bod', 'cym')">
                        <xsl:text>iso639-2t</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>iso639-2b</xsl:otherwise>
                </xsl:choose>
            </subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 550:  }} -->
    <xsl:template match="gndo:broaderTerm">
        <xsl:variable name="link" select="concat('http://d-nb.info/gnd/', tokenize(data(@gndo:ref), '/')[last()], '/about/marcxml')"/>
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="@gndo:type = 'generic'">
                    <term key="broaderTermGeneric" code="obge">Oberbegriff generisch</term>
                </xsl:when>
                <xsl:when test="@gndo:type = 'instantial'">
                    <term key="broaderTermInstantial" code="obin">Oberbegriff instantiell</term>
                </xsl:when>
                <xsl:when test="@gndo:type = 'partitive'">
                    <term key="broaderTermPartitive" code="obpa">Oberbegriff partitiv</term>
                </xsl:when>
                <xsl:when test="@gndo:type = 'with-more-than-one-element'">
                    <term key="broaderTermWithMoreThanOneElement" code="obmo">Oberbegriff mehrgliedrig</term>
                </xsl:when>
                <xsl:otherwise>
                    <term key="broaderTermGeneral" code="obal">Oberbegriff allgemein</term>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="tag">
            <xsl:choose>
                <!-- Ob nun Tags 550, 530 oder 551 liegt an der Range -->
                <xsl:when test="@gndo:type = 'partitive'">
                    <xsl:variable name="type-codes" select="(document($link)//*:datafield[@tag='075']/*:subfield[@code='b'] => data())"/>
                    <xsl:choose>
                        <!--<xsl:when test="'s' = $type-codes">
                            <tag code="550"/>
                        </xsl:when>-->
                        <xsl:when test="'u' = $type-codes">
                            <tag code="530"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <tag code="550"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <tag code="550"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <datafield tag="{$tag/*:tag/@code}" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref">
                <xsl:with-param name="ignore-url" select="false()"/>
            </xsl:call-template>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="4"><xsl:value-of select="normalize-space($type/*:term/@code)"/></subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#<xsl:value-of select="normalize-space($type/*:term/@key)"/></subfield>
            <subfield code="w">r</subfield>
            <subfield code="i"><xsl:value-of select="normalize-space($type/*:term/text())"/></subfield>
        </datafield>
    </xsl:template>
    
    
    
    
    <!-- {{ 667: Notes (INTERNAL) }} -->
    <xsl:template match="skos:note[@gndo:type = 'internal']">
        <xsl:variable name="agency" select="ancestor::collection/metadata/agency"/>
        <datafield tag="667" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:if test="not(empty($agency))">
                <subfield code="5"><xsl:value-of select="normalize-space(data($agency/@isil))"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 667: Projektkennzeichnung }}
        $agency-isil (1): string representing an ISIL
    -->
    <xsl:template name="df_667_PK">
        <xsl:param name="agency-isil"/>
        <xsl:variable name="provider-metadata" select="(./ancestor::collection)[1]/metadata/provider"/>
        <xsl:variable name="provider-title" select="normalize-space($provider-metadata/title/text())"/>
        <xsl:variable name="agency-metadata" select="(./ancestor::collection)[1]/metadata/agency"/>
        <xsl:variable name="isil">
            <xsl:choose>
                <xsl:when test="$provider-metadata and $provider-metadata[@isil]">
                    <xsl:value-of select="data($provider-metadata/@isil)"/>
                </xsl:when>
                <xsl:when test="$agency-metadata and $agency-metadata[@isil]">
                    <xsl:value-of select="data($agency-metadata/@isil)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$agency-isil"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <datafield tag="667" ind1=" " ind2=" ">
            <subfield code="a"><xsl:value-of select="$provider-title"/></subfield>
            <subfield code="5"><xsl:value-of select="$isil"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 670: Homepages }} -->
    <xsl:template match="gndo:homepage|foaf:page">
        <xsl:variable name="url" select="normalize-space(text())"/>
        <xsl:variable name="label" select="if (@gndo:label) then (data(@gndo:label)) else (gndo:base-uri-2-label($url, 'Homepage'))"/>

        <datafield tag="670" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="$label"/></subfield>
            <xsl:if test="@iso-date">
                <subfield code="b"><xsl:text>Stand: </xsl:text><xsl:value-of select="data(@iso-date)"/></subfield>
            </xsl:if>
            <subfield code="u"><xsl:value-of select="$url"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 670: Sources }} -->
    <xsl:template match="data//source">
        <xsl:variable name="title" select="if(child::title) then (child::title/text()) else (string-join(./text()))"/>
        <xsl:variable name="add-info" select="normalize-space(note/text())"/>
        <datafield tag="670" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space($title)"/></subfield>
            <xsl:if test="$add-info">
                <subfield code="b"><xsl:value-of select="$add-info"/></subfield>
            </xsl:if>
            <xsl:if test="@url">
                <subfield code="u"><xsl:value-of select="normalize-space(data(@url))"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 678: gndo:biographicalOrHistoricalInformation }} -->
    <xsl:template match="gndo:biographicalOrHistoricalInformation">
        <datafield tag="678" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="b"><xsl:value-of select="normalize-space(text())"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- {{ 680: Notes }} -->
    <xsl:template match="skos:note[not(@gndo:type)]">
        <datafield tag="680" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
        </datafield>
    </xsl:template>
    
    
    <!-- ######{{ SUBFIELDS, REFS, URIS, IDS }}###################################################################### -->
    
    <xsl:template name="datable-field">
        <xsl:param name="node"/>
        <xsl:variable name="iso-date" select="$node/@iso-date" />
        <xsl:variable name="iso-from" select="$node/@iso-from" />
        <xsl:variable name="iso-to" select="$node/@iso-to" />
        <xsl:choose>
            <xsl:when test="$iso-date">
                <xsl:value-of select="if (count(tokenize($iso-date, '-')) &gt; 1) then (gndo:iso-date2dotted-date( $iso-date )) else ($iso-date)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="from" select="if ($iso-from and count(tokenize($iso-from, '-')) &gt; 1) then (gndo:iso-date2dotted-date( $iso-from )) else ($iso-from)"/>
                <xsl:variable name="to" select="if ($iso-to and count(tokenize($iso-to, '-')) &gt; 1) then (gndo:iso-date2dotted-date( $iso-to )) else ($iso-to)"/>
                <xsl:choose>
                    <xsl:when test="count(($from, $to)) > 1">
                        <xsl:value-of select="$from"/><xsl:text>-</xsl:text><xsl:value-of select="$to"/>
                    </xsl:when>
                    <xsl:when test="not($from = '')">
                        <xsl:value-of select="$from"/><xsl:text>-</xsl:text>
                    </xsl:when>
                    <xsl:when test="not($to = '')">
                        <xsl:text>-</xsl:text><xsl:value-of select="$to"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <xsl:template name="gndo:ref">
        <xsl:param name="ignore-url" select="false()"/>
        <xsl:param name="isil-over-sisc" select="true()"/>
        <xsl:choose>
            <xsl:when test="@gndo:ref">
                <xsl:variable name="ref" select="data(@gndo:ref)"/>
                <xsl:variable name="de-588" select="gndo:uri2id($ref)" />
                <!--
                <xsl:variable name="de-101" select="format-number(xs:int(replace($de-588, '\-', '')), '000000000')" />
                <subfield code="0">(DE-101)<xsl:value-of select="$de-101"/></subfield>
            -->
                <!--<subfield code="0">(gnd)<xsl:value-of select="$de-588"/></subfield>-->
                <subfield code="0">(<xsl:value-of select="gndo:isil($isil-over-sisc)"/>)<xsl:value-of select="$de-588"/></subfield>
                <xsl:if test="$ignore-url = false()">
                    <subfield code="0"><xsl:value-of select="$ref"/></subfield>
                </xsl:if>
            </xsl:when>
            <!-- If there are local references set ... -->
            <xsl:when test="@ref">
                <xsl:variable name="local-id-ref" select="data(@ref)"/>
                <xsl:variable name="local-id" select="substring-after($local-id-ref, '#')"/>
                <xsl:variable name="ag-isil" select="gndo:get-agency-isil(.)"/>
                <xsl:if test="starts-with($local-id-ref, '#')">
                    <subfield code="0"><xsl:text>(</xsl:text><xsl:value-of select="$ag-isil"/><xsl:text>)</xsl:text><xsl:value-of select="$local-id"/></subfield>
                </xsl:if>
            </xsl:when>
        </xsl:choose>        
    </xsl:template>
    
    
    <!-- AGENCY MODES Templates -->
    <xsl:template name="property-mode">
        <xsl:param name="nodes" select="."/>
        <xsl:variable name="parent-record" select="$nodes[1]/parent::element()[@xml:id or @gndo:uri]"/>
        <xsl:if test="$nodes/@agency">
            <xsl:attribute name="exml:mode"><xsl:value-of select="distinct-values($nodes/@agency)"/></xsl:attribute>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="set-gnd-uri-for-record">
        <xsl:if test="@gndo:uri">
            <xsl:attribute name="exml:uri"><xsl:value-of select="@gndo:uri"/></xsl:attribute>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="record-controll-attributes">
        <!-- 1. Set agency mode "update", "create", "merge", "ignore" to MARC21 record -->
        <xsl:choose>
            <xsl:when test="@agency">
                <xsl:attribute name="exml:mode">
                    <xsl:value-of select="@agency"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="exml:mode">ignore</xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
        
        <!-- 2. Set GND-URI to MARC21 record, if there is any and if agency-mode is "update" or "merge"! -->
        <xsl:if test="@gndo:uri and @agency = ('update', 'merge')">
            <xsl:call-template name="set-gnd-uri-for-record"/>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>