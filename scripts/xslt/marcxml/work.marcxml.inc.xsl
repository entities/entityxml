<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:marc="http://www.loc.gov/MARC21/slim"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.loc.gov/MARC21/slim"
    exclude-result-prefixes="xs geo gndo exml marc foaf" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <!-- Retrieve all authors using GND-MARC API -->
    <xsl:variable name="global-authors">
        <xsl:variable name="authors" select="//gndo:firstAuthor[@gndo:ref]"/>
        <xsl:variable name="uris" select="$authors/@gndo:ref"/>
        <xsl:copy-of select="marc:records($uris, ('100', '103', '024'))"/>
    </xsl:variable>
    
    <!-- Retrieve all works using GND-MARC API -->
    <xsl:variable name="global-works">
        <xsl:variable name="works" select="(//gndo:accordingWork[@gndo:ref], //gndo:relatedWork[@gndo:ref], //gndo:precedingWork[@gndo:ref]), //gndo:succeedingWork[@gndo:ref], //gndo:literarySource[@gndo:ref]"/>
        <xsl:variable name="uris" select="$works/@gndo:ref"/>
        <xsl:copy-of select="marc:records($uris, ('100', '103', '024'))"/>
    </xsl:variable>
   
    <!-- author template to print the author details to fields requireing the author of the current work -->
    <xsl:template name="get-author">
        <xsl:variable name="author" select="parent::exml:work/gndo:firstAuthor"/>
        <xsl:choose>
            
            <!-- IF requests are allowed then pull data using the GND-MARC API via http://d-nb.info/gnd/[GND-ID]/about/marcxml -->
            <xsl:when test="$requests = true()">
                
                <!-- First search for an author related to the work using @gndo:ref -->
                <xsl:variable name="author-uri" select="(parent::exml:work/gndo:firstAuthor/@gndo:ref, parent::exml:work/gndo:author/@gndo:ref)[1]"/>
                
                <!-- Then retrieve data from GND-MARC API via http://d-nb.info/gnd/[GND-ID]/about/marcxml -->
                <xsl:variable name="global-author" select="$global-authors//marc:record[@uri = $author-uri]"/>
                <xsl:choose>
                    <xsl:when test="$global-author">
                        <!-- select the preferred name field of the person (Field 100) and select name ($a) and living dates ($d) -->
                        <xsl:variable name="record" select="marc:get-datafields-from-marc($global-author, ('100'))"/>
                        <xsl:variable name="name" select="$record/marc:subfield[@code = 'a']/text()"/>
                        <xsl:variable name="dates" select="$record/marc:subfield[@code = 'd']/text()"/>
                        <subfield code="a">
                            <xsl:value-of select="$name"/>
                        </subfield>
                        <subfield code="d">
                            <xsl:value-of select="$dates"/>
                        </subfield>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="$author">
                            <subfield code="a">
                                <xsl:value-of select="normalize-space($author/text())"/>
                            </subfield>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$author">
                    <subfield code="a">
                        <xsl:value-of select="normalize-space($author/text())"/>
                    </subfield>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- work template to print the work details for fields requireing those information like gndo:relatedWork etc. -->
    <xsl:template name="get-work">
        <xsl:variable name="work-uri" select="data(@gndo:ref)"/>
        <xsl:choose>
            
            <!-- IF requests are allowed then pull data using the GND-MARC API via http://d-nb.info/gnd/[GND-ID]/about/marcxml -->
            <xsl:when test="$requests = true()">
                
                <!-- Then retrieve data from GND-MARC API via http://d-nb.info/gnd/[GND-ID]/about/marcxml -->
                <xsl:variable name="global-work" select="$global-works//marc:record[@uri = $work-uri]"/>
                <xsl:choose>
                    <xsl:when test="$global-work">
                        <!-- select the preferred name field of the work (Field 100) and select authorname ($a), living dates ($d) and title ($t)-->
                        <xsl:variable name="record" select="marc:get-datafields-from-marc($global-work, ('100'))"/>
                        <!--<xsl:copy-of select="$record"></xsl:copy-of>-->
                        <xsl:variable name="name" select="$record/marc:subfield[@code = 'a']/text()"/>
                        <xsl:variable name="dates" select="$record/marc:subfield[@code = 'd']/text()"/>
                        <xsl:variable name="title" select="$record/marc:subfield[@code = 't']/text()"/>
                        <subfield code="a">
                            <xsl:value-of select="$name"/>
                        </subfield>
                        <subfield code="d">
                            <xsl:value-of select="$dates"/>
                        </subfield>
                        <subfield code="t">
                            <xsl:value-of select="$title"/>
                        </subfield>
                    </xsl:when>
                    <xsl:otherwise>
                        <subfield code="t">
                            <xsl:value-of select="normalize-space(text())"/>
                        </subfield>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <subfield code="t">
                    <xsl:value-of select="normalize-space(text())"/>
                </subfield>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- 
        INCLUDE Module (not standalone): Work Records 2 Marc-XML 
        **********************************************************
        - uses external functions from /utils/functs.xsl
    -->
    
    <xsl:template match="exml:work">
        <xsl:call-template name="default-record"/>
    </xsl:template>
    
    <!-- {{ 130: gndo:preferredName }} -->
    <xsl:template match="exml:work/gndo:preferredName">
        <xsl:choose>
            <xsl:when test="parent::exml:work[gndo:firstAuthor]">
                <!-- VERFASSERWERK:
                Trift zu, wenn das Werk einen gndo:firstAuthor zugewiesen hat, der eine Person ist. -->
                <datafield tag="100" ind1="1" ind2=" ">
                    <xsl:call-template name="property-mode"/>
                    <xsl:call-template name="get-author"/>
                    <subfield code="t"><xsl:value-of select="normalize-space(text())"/></subfield>
                    <xsl:if test="@script">
                        <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
                    </xsl:if>
                    <xsl:if test="@xml:lang">
                        <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
                    </xsl:if>
                </datafield>
            </xsl:when>
            <xsl:otherwise>
                <!-- DEFAULT 
                Trift zu, wenn das Werk einen Titel aber keinen gndo:firstAuthor zugewiesen hat. -->
                <datafield tag="130" ind1=" " ind2="0">
                    <xsl:call-template name="property-mode"/>
                    <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
                    <xsl:if test="parent::exml:work[gndo:dateOfPublication]">
                        <subfield code="f"><xsl:value-of select="parent::exml:work/gndo:dateOfPublication[1]/text()"/></subfield>
                    </xsl:if>
                    <xsl:if test="@script">
                        <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
                    </xsl:if>
                    <xsl:if test="@xml:lang">
                        <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
                    </xsl:if>
                </datafield>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <!-- {{ 400: gndo:variantName }} -->
    <xsl:template match="exml:work/gndo:variantName">
        <xsl:variable name="ind1"><xsl:text>1</xsl:text></xsl:variable>
        <datafield tag="400" ind1="{$ind1}" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="get-author"/>
            <subfield code="t"><xsl:value-of select="normalize-space(text())"/></subfield>
            <xsl:if test="@script">
                <subfield code="9">U:<xsl:value-of select="@script"/></subfield>
            </xsl:if>
            <xsl:if test="@xml:lang">
                <subfield code="9">L:<xsl:value-of select="@xml:lang"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    <!-- {{ 380: gndo:formOfWorkAndExpression }} -->
    <xsl:template match="exml:work/gndo:formOfWorkAndExpression">
        <datafield tag="380" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <subfield code="a"><xsl:value-of select="normalize-space(text())"/></subfield>
            <subfield code="2">gnd</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 500: gndo:firstAuthor }} -->
    <xsl:template match="exml:work/gndo:firstAuthor">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <xsl:call-template name="get-author"/>
            <subfield code="4">aut1</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#firstAuthor</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Verfasserschaft1</subfield>
            <subfield code="e">Verfasserschaft1</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 500: gndo:literarySource }} -->
    <xsl:template match="exml:work/gndo:literarySource">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <xsl:call-template name="get-work"/>
            <subfield code="4">vorl</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#literarySource</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Vorlage</subfield>
            <subfield code="9">v:Angeregt durch</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 500: gndo:precedingWork }} -->
    <xsl:template match="exml:work/gndo:precedingWork">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <xsl:call-template name="get-work"/>
            <subfield code="4">vorg</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#precedingWork</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Vorgaenger</subfield>
            <subfield code="9">v:Vorangegangen ist</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 500: gndo:relatedWork }} -->
    <xsl:template match="exml:work/gndo:relatedWork">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <xsl:call-template name="get-work"/>
            <subfield code="4">rela</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#relatedWork</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Relation allgemein</subfield>
            <xsl:if test="@label">
                <subfield code="9">v:<xsl:value-of select="@label"/></subfield>
            </xsl:if>
        </datafield>
    </xsl:template>
    
    <!-- {{ 500: gndo:succeedingWork }} -->
    <xsl:template match="exml:work/gndo:succeedingWork">
        <datafield tag="500" ind1="1" ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <xsl:call-template name="get-work"/>
            <subfield code="4">nach</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#succeedingWork</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Nachfolger</subfield>
            <subfield code="9">v:Gefolgt von</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 530: gndo:accordingWork }} -->
    <xsl:template match="exml:work/gndo:accordingWork">
        <datafield tag="530" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <xsl:call-template name="gndo:ref" />
            <xsl:call-template name="get-work"/>
            <subfield code="4">werk</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#accordingWork</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Zugehöriges Werk</subfield>
            <subfield code="9">v:Teil von</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 548: gndo:dateOfPublication }} -->
    <xsl:template match="exml:work/gndo:dateOfPublication">
        <xsl:variable name="date" select="if (@iso-date) then (data(@iso-date)) else (normalize-space(text()))"/>
        <datafield tag="548" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="$date"/></subfield>
            <subfield code="4">datj</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfPublication</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Erscheinungszeit</subfield>
        </datafield>
    </xsl:template>
    
    <!-- {{ 548: gndo:dateOfProduction }} -->
    <xsl:template match="exml:work/gndo:dateOfProduction">
        <xsl:variable name="date" select="if (@iso-date) then (data(@iso-date)) else (normalize-space(text()))"/>
        <datafield tag="548" ind1=" " ind2=" ">
            <xsl:call-template name="property-mode"/>
            <subfield code="a"><xsl:value-of select="$date"/></subfield>
            <subfield code="4">dats</subfield>
            <subfield code="4">https://d-nb.info/standards/elementset/gnd#dateOfProduction</subfield>
            <subfield code="w">r</subfield>
            <subfield code="i">Erstellungszeit</subfield>
        </datafield>
    </xsl:template>
    
</xsl:stylesheet>