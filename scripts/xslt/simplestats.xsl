<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rng="http://relaxng.org/ns/structure/1.0"
    xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#"
    xmlns:es="https://sub.uni-goettingen.de/met/standards/entity-xml-staging#"
    exclude-result-prefixes="xs" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="3.0">
    
    <xsl:param name="schema-path" select="'../../schema/entityXML.rng'" />
    
    <xsl:output method="text" />
    
    <xsl:variable name="records" select="//collection//list/element()[not(name() = ('abstract', 'title'))]"/>
    <xsl:variable name="properties" select="$records//element()"/>
    <xsl:variable name="defined" select="$properties[name() = (rng:schema-element-names(doc($schema-path)))]"/>
    <xsl:variable name="not-defined" select="$properties[not(name() = (rng:schema-element-names(doc($schema-path))))]"/>
    
    
    <xsl:function name="rng:schema-element-names">
        <xsl:param name="schema" />
        <xsl:for-each select="($schema//rng:element, $schema//rng:attribute)">
            <xsl:value-of select="data(./@name)"/>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:template match="/">
        <xsl:apply-templates select="entityXML"/>
    </xsl:template>
    
    <xsl:template match="entityXML">
        <xsl:text># Simple Statistics</xsl:text>
        <xsl:text>&#10;&gt; </xsl:text>
        <xsl:value-of select="base-uri()" />
        <xsl:text>&#10;&#10;## General</xsl:text>
        <xsl:call-template name="collections" />
        <xsl:call-template name="lists" />
        <xsl:call-template name="records" />
        <xsl:call-template name="properties" />
        <!--<xsl:text>&#10;&#10;-\-\-&#10;</xsl:text>-->
        <xsl:text>&#10;</xsl:text>
        <xsl:text>&#10;## Agency</xsl:text>
        <xsl:call-template name="agency-records" />
        <xsl:call-template name="agency-custom-elements-ratio" />
        <xsl:call-template name="agency-custom-elements" />
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template name="collections">
        <xsl:text>&#10;- Collections: </xsl:text>
        <xsl:value-of select="count(collection)"/>
    </xsl:template>
    
    <xsl:template name="lists">
        <xsl:text>&#10;- Lists: </xsl:text>
        <xsl:value-of select="count(collection//list)"/>
    </xsl:template>
    
    <xsl:template name="records">
        <xsl:variable name="record_count" select="count($records)"/>
        <xsl:text>&#10;- Records: </xsl:text>
        <xsl:value-of select="count($records)"/>
        <xsl:for-each-group select="$records" group-by="name()">
            <xsl:variable name="entity_type" select="name()"/>
            <xsl:variable name="entity_type_count" select="count(current-group())"/>
            <xsl:variable name="type_record_ratio" select="format-number(($entity_type_count div $record_count)*100, '##')"/>
            <xsl:text>&#10;&#09;- </xsl:text>
            <xsl:value-of select="name()"/>
            <xsl:text>: </xsl:text>
            <xsl:value-of select="count(current-group())"/>
            <xsl:text> (</xsl:text>
            <xsl:value-of select="$type_record_ratio"/>
            <xsl:text>%)</xsl:text>
        </xsl:for-each-group>
    </xsl:template>
    
    <xsl:template name="properties">
        <xsl:text>&#10;- Properties : </xsl:text>
        <xsl:value-of select="count($properties)"/>
        <xsl:text>&#10;&#09;- Part of entityXML: </xsl:text>
        <xsl:value-of select="count($defined)"/>
        <xsl:text>&#10;&#09;- Custom Namespaces: </xsl:text>
        <xsl:value-of select="count($properties)-count($defined)"/>
    </xsl:template>
    
    <!--<xsl:template name="agency-staged-records">
        <xsl:variable name="open-records" select="count($records[revision/not(@status) or not(revision/@status = 'closed')])"/>
        <xsl:variable name="closed-records" select="count($records[revision/@status = 'closed'])" />
        <xsl:text>&#10;- Records closed: </xsl:text>
        <xsl:value-of select="$closed-records"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="count($records)"/>
        <xsl:text> (</xsl:text>
        <xsl:variable name="ratio" select="($closed-records div count($records))*100"/>
        <xsl:value-of select="round($ratio)"/>
        <xsl:text>%)</xsl:text>
        <xsl:text> &gt; </xsl:text>
        <xsl:value-of select="$open-records"/>
        <xsl:text> to go!</xsl:text>
    </xsl:template>-->
    
    <xsl:template name="agency-records">
        <xsl:variable name="open-records" select="count($records[revision/not(@status) or not(revision/@status = 'closed')])"/>
        <xsl:variable name="closed-records" select="count($records[revision/@status = 'closed'])" />
        <xsl:text>&#10;- Records closed: </xsl:text>
        <xsl:value-of select="$closed-records"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="count($records)"/>
        <xsl:text> (</xsl:text>
        <xsl:variable name="ratio" select="($closed-records div count($records))*100"/>
        <xsl:value-of select="round($ratio)"/>
        <xsl:text>%)</xsl:text>
        <xsl:text> &gt; </xsl:text>
        <xsl:value-of select="$open-records"/>
        <xsl:text> to go!</xsl:text>
    </xsl:template>
    
    <xsl:template name="agency-custom-elements">
        <xsl:text>&#10;- Custom namespace elements : </xsl:text>
        <xsl:for-each-group select="$not-defined" group-by="name()">
            <xsl:text>&#10;&#09;- </xsl:text>
            <xsl:value-of select="name()"/>
            <xsl:text> (</xsl:text>
            <xsl:value-of select="count(current-group())"/>
            <xsl:text>)</xsl:text>
        </xsl:for-each-group>
    </xsl:template>
    
    <xsl:template name="agency-custom-elements-ratio">
        <xsl:text>&#10;- Custom namespace ratio : </xsl:text>
        <xsl:variable name="def-count" select="count($defined)"/>
        <xsl:variable name="undef-count" select="count($properties)-count($defined)"/>
        <xsl:variable name="ratio" select="round(($undef-count div $def-count)*100)"/>
        <xsl:value-of select="$ratio"/>
        <xsl:text>% (</xsl:text>
        <xsl:value-of select="$undef-count"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$def-count"/>
        <xsl:text>)</xsl:text>
        <xsl:if test="$ratio &gt; 40">
            <xsl:text> ! **Many Custom Elements detected!** !</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="text()" />
    
</xsl:stylesheet>