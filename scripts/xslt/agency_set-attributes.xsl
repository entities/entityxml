<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:dnb="https://www.dnb.de" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:ex="https://gitlab.gwdg.de/usikora/entityxml/tests" 
    xmlns:es="https://sub.uni-goettingen.de/met/standards/entity-xml-staging#"
    xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output indent="yes" method="xml"/>
    
    <xsl:variable name="date" select="format-date(current-date(),'[Y]-[M,2]-[D,2]')"/>
    
    <!-- ******************************* FUNCTIONS ********************************* -->
    
    <xsl:function name="gndo:person-minimum-criteria">
        <xsl:param name="person"/>
        <xsl:variable name="agency" select="$person/@agency"/>
        <xsl:variable name="preferred-name" select="$person/gndo:preferredName"/>
        <xsl:variable name="preferred-name" select="$person/gndo:preferredName"/>
        <xsl:variable name="surname" select="$preferred-name/gndo:surname"/>
        <xsl:variable name="forename" select="$preferred-name/gndo:forename"/>
        <xsl:variable name="periodOfActivity" select="$person/gndo:periodOfActivity"/>
        <xsl:variable name="dateOfBirth" select="$person/gndo:dateOfBirth"/>
        <xsl:variable name="dateOfDeath" select="$person/gndo:dateOfDeath"/>
        <xsl:variable name="professions" select="($person/gndo:professionOrOccupation)"/>
        <xsl:variable name="affiliations" select="($person/gndo:affiliation)"/>
        
        <xsl:variable name="has-forename" select="if ($forename) then true() else false()"/>
        <xsl:variable name="has-surname" select="if ($surname and $surname[not(@cert='low')]) then true() else false()"/>
        <xsl:variable name="has-dates" select="if($periodOfActivity or $dateOfBirth or $dateOfDeath) then (true()) else (false())"/>
        <xsl:variable name="has-profession" select="if(count($professions)>0) then (true()) else (false())"/>
        <xsl:variable name="has-affiliation" select="if(count($affiliations)>0) then (true()) else (false())"/>
        <xsl:variable name="has-affiliation-or-profession" select="$has-affiliation or $has-profession"/>
        <xsl:variable name="has-geographic-area-code" select="if($person/gndo:geographicAreaCode) then (true()) else (false())"/>
        
        <xsl:variable name="tests" select="($has-forename, $has-surname, $has-dates, $has-affiliation-or-profession, $has-geographic-area-code)"/>
        <xsl:variable name="not-matches-criteria" select="$tests = false()"/>
        
        <report nomatch="{$not-matches-criteria}">
            <xsl:choose>
                <xsl:when test="$not-matches-criteria">
                    <xsl:attribute name="agency">ignore</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="$agency"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:text>Vorname:</xsl:text><xsl:value-of select="$has-forename"/>,
            <xsl:text>Nachname:</xsl:text><xsl:value-of select="$has-surname"/>, 
            <xsl:text>Daten(Wirkungsdaten und Lebensdaten):</xsl:text><xsl:value-of select="$has-dates"/>,
            <xsl:text>Affiliation:</xsl:text><xsl:value-of select="$has-affiliation"/>, 
            <xsl:text>Berufe:</xsl:text><xsl:value-of select="$has-profession"/>,
            <xsl:text>Affiliation oder Beruf:</xsl:text><xsl:value-of select="$has-affiliation or $has-profession"/>,
            <xsl:text>Geographischer Bezug:</xsl:text><xsl:value-of select="$has-geographic-area-code"/>
        </report>
    </xsl:function>
        
    <!-- ******************************* TEMPLATES ********************************* -->
    
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="person">
        <xsl:variable name="matches-criteria" select="gndo:person-minimum-criteria(.)"/>
        <xsl:variable name="agency" select="$matches-criteria/@agency"/>
        <xsl:copy>
            <xsl:apply-templates select="@*[not(name()='agency')]"/>
            <xsl:copy-of select="$agency"/>
            <xsl:apply-templates select="node()[not(self::revision)]"/>
            <!--<xsl:copy-of select="$matches-criteria"></xsl:copy-of>-->
            <revision>
                <xsl:apply-templates select="revision/@*"/>
                <xsl:if test="$agency='ignore'">
                    <change when="{$date}" who="UwS" status="embargoed">Datensatz gesperrt, weil Elemente fehlen: <xsl:value-of select="normalize-space($matches-criteria/text())"/>.</change>
                </xsl:if>
                <xsl:if test="$agency='create'">
                    <change when="{$date}" who="UwS" status="candidate">Datensatz zur Überprüfung freigegeben.</change>
                </xsl:if>
                <xsl:apply-templates select="revision/node()"/>
            </revision>
        </xsl:copy>
    </xsl:template>
   
    
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>