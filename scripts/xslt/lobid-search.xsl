<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:geo="http://www.opengis.net/ont/geosparql#"
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#"
    xmlns:lobid="https://lobid.org/gnd/api"
    xmlns:http="http://expath.org/ns/http-client"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
     
    <xsl:template match="/">
        <html>
            <head>
                <style>
                    .toggler {
                        margin-bottom: 30px;
                    }
                    
                    div.hidden {
                        display:none
                    }
                    
                    .toggler:checked ~ div.hidden {
                        display:block;
                    }
                    
                    .record {
                        color: white;
                        border: 1px solid #025aac;
                        -webkit-border-radius: 3px ;
                        -moz-border-radius: 3px;
                        border-radius: 3px;
                        margin-bottom: 20px;
                    }
                    
                    .record-text, .record-data, .record-form {
                        padding: 10px;
                    }
                    
                    .record-header {
                        display: block;
                        padding: 5px;
                        font-size: 1.3em;
                        background: #3271ac;
                        
                    }
                    
                    .record-header > *{
                        margin-left: 5px;
                    }
                    
                    .record-header > span.name {
                        font-weight: bold;
                        display: block;
                    }
                    
                    .record-header > span.id::before {
                        content: "@xml:id: ";
                        font-style: italic;
                    }
                    
                    .record-header > span.gndo-uri::before {
                        content: "@gndo:uri: ";
                        font-style: italic;
                    }
                    
                    .record-header > span.id,
                    .record-header > span.gndo-uri {
                        font-size: 1em;
                    }
                    
                    .record-text {
                        color: black;
                        line-height: 1.5em;
                    }
                    
                    .record-text span.property {
                        border-left: 1px solid;
                        border-bottom: 1px solid;
                        border-color: black;
                    }
                    
                    .record-text span.property span.label{
                        font-size: 0.6em;
                        padding: 0 3px 0 3px;
                    }
                    
                    .record-data {
                        color: black;
                    }
                    
                    .record form{
                    margin:0;
                    }
                    
                </style>
            </head>
            <body>
                <h1><a href="https://lobid.org/">lobid</a> - Quick Search</h1>
                <label>Show Records already in the GND as well: </label><input type="checkbox" class="toggler" />
                <xsl:apply-templates mode="main"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="collection" mode="main">
        <xsl:apply-templates mode="main"/>
    </xsl:template>
    
    <xsl:template match="meta" mode="main"/>
    <xsl:template match="person[not(gndo:preferredName)]" mode="main"/>
    
    
    <xsl:template match="person[gndo:preferredName]" mode="main">
        <div>
            <xsl:attribute name="class">record person <xsl:if test="@gndo:uri">hidden</xsl:if></xsl:attribute>
            <div class="record-header"><xsl:call-template name="record-header"/></div>
            <div class="record-text">
                <xsl:apply-templates mode="fulltext" />
            </div>
            <div class="record-data">
                <xsl:apply-templates mode="structured"/>
            </div>
            <div class="record-form">
                <xsl:call-template name="lobid-form"></xsl:call-template>
            </div>
        </div>
    </xsl:template>
    
    
    <xsl:template name="record-header">
        <xsl:if test="gndo:preferredName">
            <span class="name">
                <xsl:choose>
                    <xsl:when test="gndo:preferredName[element()]">
                        <xsl:value-of select="gndo:preferredName//text()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="gndo:preferredName/text()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </span>
        </xsl:if>
        <xsl:if test="@xml:id">
            <span class="id"><xsl:value-of select="@xml:id"/></span>
        </xsl:if>
        <xsl:if test="@gndo:uri">
            <span class="gndo-uri"><xsl:value-of select="@gndo:uri"/></span>
        </xsl:if>        
    </xsl:template>
    
    
    <xsl:template name="lobid-form">
        <xsl:variable name="name" select="normalize-space(string-join(gndo:preferredName//text()))"/>
        <xsl:variable name="query">
            <xsl:value-of select="lobid:create-query(.)"/>  
        </xsl:variable>
        <form action="https://lobid.org/gnd/search" method="GET" id="resources-form" target="_blank">
            <div class="input-group" id="search-simple">
                <input 
                    autofocus="" type="text" name="q" id="gnd-query" value="{$query}" class="form-control" autocomplete="off" 
                    placeholder='Suchoptionen: AND, OR, AND NOT, ""-Phrasensuche, *-Trunkierung' title="Suchanfrage" />
                <button class="btn btn-default" type="submit" title="Suchen">
                    Search in lobid
                </button>
                
            </div>
        </form>
    </xsl:template>
    
    <xsl:function name="lobid:create-query">
        <xsl:param name="object" />
        <xsl:variable name="preferredName">
            <xsl:choose>
                <xsl:when test="$object/gndo:preferredName[gndo:personalName]"><xsl:value-of select="normalize-space($object/gndo:preferredName/gndo:personalName/text())"/></xsl:when>
                <xsl:when test="$object[self::person]">
                    <xsl:if test="$object/gndo:preferredName[gndo:forename]"><xsl:value-of select="normalize-space($object/gndo:preferredName/gndo:forename/text())"/><xsl:text> </xsl:text></xsl:if>
                    <xsl:if test="$object/gndo:preferredName[gndo:prefix]"><xsl:value-of select="normalize-space($object/gndo:preferredName/gndo:prefix/text())"/><xsl:text> </xsl:text></xsl:if>
                    <xsl:value-of select="normalize-space($object/gndo:preferredName/gndo:surname/text())"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="normalize-space($object/gndo:preferredName/text())"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="dateOfBirthAndDeath">
            <xsl:choose>
                <xsl:when test="$object/gndo:dateOfBirth or $object/gndo:dateOfDeath">
                    <xsl:if test="$object/gndo:dateOfBirth[@iso-date]"><xsl:value-of select="tokenize($object/gndo:dateOfBirth/@iso-date, '-')[1]"/></xsl:if>
                    <xsl:text>-</xsl:text>
                    <xsl:if test="$object/gndo:dateOfDeath[@iso-date]"><xsl:value-of select="tokenize($object/gndo:dateOfDeath/@iso-date, '-')[1]"/></xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="normalize-space(string-join(($preferredName, $dateOfBirthAndDeath),' '))"/>
    </xsl:function>
    
    <!-- FULLTEXT MODE -->
    <xsl:template match="revision" mode="fulltext"/>
    
    <xsl:template match="node()" mode="fulltext">
        <span class="property element">
            <span class="label"><xsl:value-of select="name()"/></span>
            <xsl:apply-templates select="node()|@*" mode="fulltext"/>
        </span>
    </xsl:template>
    
    <xsl:template match="@*" mode="fulltext">
        <span class="property attribute">
            <span class="label">@<xsl:value-of select="name()"/></span>
            <xsl:text>(</xsl:text><xsl:value-of select="."/><xsl:text>)</xsl:text>
        </span>
    </xsl:template>
    
    <xsl:template match="text()" mode="fulltext">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <!-- STRUCTURED MODE -->
    
    <xsl:template match="revision" mode="structured" />
    
    <xsl:template match="*:page|*:sameAs" mode="structured">
        <a href="{text()}" style="display:block" target="_blank"><xsl:value-of select="text()"/></a>
    </xsl:template>
    
</xsl:stylesheet>