<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:rng="http://relaxng.org/ns/structure/1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:teiex="http://www.tei-c.org/ns/Examples"
    xmlns:app="http://sub.uni-goettingen.de/met/standards/application-profiles/app-info#"
    xmlns:doc="https://sub.uni-goettingen.de/met/formats/simpledoc#"
    xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization"
    xmlns:md="http://markdown/functions"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#"
    xmlns:xpath="http://www.w3.org/2005/xpath-functions"
    exclude-result-prefixes="xs app" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="3.0">
    
    <doc:doc>
        <doc:head>entityXML to JSON Conversion</doc:head>
        <doc:p>This script converts entityXML Data into a JSON Representation.</doc:p>
    </doc:doc>
    
    <xsl:output method="text"/>
    <!--<xsl:output method="xml" indent="true"/>-->
    
    
    <xsl:template match="/">
        <xsl:variable name="json-xml">
            <xsl:apply-templates mode="json-xml" />
        </xsl:variable>
        <xsl:value-of select="xml-to-json($json-xml)"/>
        <!--<xsl:copy-of select="$json-xml"></xsl:copy-of>-->
    </xsl:template>
    
    <xsl:template match="entityXML" mode="json-xml">
        <xpath:map>
            <xpath:array key="collection">
                <xsl:apply-templates mode="json-xml"/>
            </xpath:array>
        </xpath:map>
    </xsl:template>
    
    <xsl:template match="collection" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" />
        </xpath:map>
    </xsl:template>
    
    <xsl:template match="collection/metadata" mode="json-xml">
        <xpath:map key="{name()}">
            <xsl:apply-templates mode="json-xml" select="title|abstract"/>
        </xpath:map>
    </xsl:template>
    
    <xsl:template match="title|abstract" mode="json-xml">
        <xpath:string key="{name()}">
            <xsl:value-of select="normalize-space(text())"/>
        </xpath:string>
    </xsl:template>
    
    <xsl:template match="collection/data" mode="json-xml">
        <xpath:array key="data">
            <xsl:apply-templates mode="json-xml" />
        </xpath:array>
    </xsl:template>
    
    <xsl:template match="collection/data/list" mode="json-xml">
        <xpath:map>
            <xsl:apply-templates mode="json-xml" select="@*|title|abstract"/>
            <xpath:array key="records">
                <xsl:apply-templates mode="json-xml" select="element()[not(name() = ('title', 'abstract'))]"></xsl:apply-templates>
            </xpath:array>
        </xpath:map>
    </xsl:template>
    
    <xsl:template match="collection/data/list/element()[not(name() = ('title', 'abstract'))]" mode="json-xml">
        <xpath:map>
            <xpath:string key="class"><xsl:value-of select="./name()"/></xpath:string>
            <xsl:apply-templates mode="json-xml" select="@*" />
            <xsl:call-template name="properties" />
        </xpath:map>
    </xsl:template>
    
    
    <xsl:template match="@*" mode="json-xml">
        <xpath:string key="@{name()}">
            <xsl:value-of select="data(.)"/>
        </xpath:string>
    </xsl:template>
    
    <xsl:template match="@ref" mode="json-xml">
        <xsl:choose>
            <xsl:when test="not(parent::element()[@gndo:ref]) and starts-with(data(.), '#')">
                <xsl:variable name="id" select="substring-after(data(.), '#')"/>
                <xsl:variable name="target" select="root()//element()[@xml:id = $id]"/>
                <xsl:choose>
                    <xsl:when test="$target[@gndo:uri]">
                        <xpath:string key="@gndo:ref">
                            <xsl:value-of select="data($target/@gndo:uri)"/>
                        </xpath:string>
                    </xsl:when>
                    <xsl:otherwise>
                        <xpath:string key="@{name()}">
                            <xsl:value-of select="data(.)"/>
                        </xpath:string>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xpath:string key="@{name()}">
                    <xsl:value-of select="data(.)"/>
                </xpath:string>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    
    
    
    
    
    <xsl:template name="properties">
        <xsl:choose>
            <xsl:when test="empty(element())">
                <xpath:string key="text"><xsl:value-of select="normalize-space(data(.))"/></xpath:string>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each-group select="element()" group-by="./name()">
                    <xsl:variable name="name" select="./name()"/>
                    <xsl:variable name="count" select="count(current-group())"/>
                    
                    <xsl:choose>
                        <xsl:when test="$count = 1">
                            <xsl:choose>
                                <xsl:when test="@* and element()">
                                    <xpath:map key="{$name}">
                                        <xsl:apply-templates mode="json-xml" select="@*"></xsl:apply-templates>
                                        <xsl:call-template name="properties"/>
                                    </xpath:map>
                                </xsl:when>
                                <xsl:when test="@* and child::text()">
                                    <xpath:map key="{$name}">
                                        <xsl:apply-templates mode="json-xml" select="@*"></xsl:apply-templates>
                                        <xpath:string key="text"><xsl:value-of select="normalize-space(data(.))"/></xpath:string>
                                    </xpath:map>
                                </xsl:when>
                                <xsl:when test="@*">
                                    <xpath:map key="{$name}">
                                        <xsl:apply-templates mode="json-xml" select="@*"></xsl:apply-templates>
                                    </xpath:map>
                                </xsl:when>
                                <xsl:when test="element()">
                                    <xpath:map key="{$name}">
                                        <xsl:call-template name="properties"/>
                                    </xpath:map>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xpath:string key="{$name}"><xsl:value-of select="normalize-space(data(.))"/></xpath:string>
                                </xsl:otherwise>
                            </xsl:choose>
                            
                        </xsl:when>
                        <xsl:otherwise>
                            <xpath:array key="{$name}" xmlns="http://www.w3.org/2005/xpath-functions">
                                <xsl:for-each select="current-group()">
                                    <xsl:choose>
                                        <xsl:when test="empty((@*, element()))">
                                            <xpath:string><xsl:value-of select="normalize-space(data(.))"/></xpath:string>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xpath:map>
                                                <xsl:apply-templates mode="json-xml" select="@*"/>
                                                <xsl:call-template name="properties"/>
                                            </xpath:map>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:for-each>
                            </xpath:array>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each-group>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    
    

</xsl:stylesheet>