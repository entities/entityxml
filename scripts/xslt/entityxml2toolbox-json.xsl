<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:doc="https://sub.uni-goettingen.de/met/formats/simpledoc#"
    xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization"
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#"
    xmlns:xpath="http://www.w3.org/2005/xpath-functions"
    exclude-result-prefixes="xs" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/entity-xml#"
    version="2.0">
    
    <doc:doc>
        <doc:head>entityXML to GND Toolbox JSON Conversion</doc:head>
        <doc:p>This script converts entityXML Data into a JSON Representation wich can be fet into the GND Toolbox.</doc:p>
    </doc:doc>
    
    <xsl:output method="text"/>
    <xsl:template match="/">
        <xsl:variable name="json-xml">
            <xsl:apply-templates mode="json-xml" />
        </xsl:variable>
        <xsl:value-of select="xml-to-json($json-xml)"/>
    </xsl:template>
    
    
    <!--<xsl:output method="xml" indent="true"/>
    <xsl:template match="/">
        <xsl:variable name="json-xml">
            <xsl:apply-templates mode="json-xml" />
        </xsl:variable>
        <xsl:copy-of select="$json-xml"></xsl:copy-of>
    </xsl:template>-->
    
    <xsl:template match="entityXML" mode="json-xml">
        <xpath:map>
            <xpath:map key="@context">
                <xpath:string key="gndo">https://d-nb.info/standards/elementset/gnd#</xpath:string>
                <xpath:string key="rdfs">https://www.w3.org/2000/01/rdf-schema#</xpath:string>
                <xpath:string key="dc">http://purl.org/dc/terms/</xpath:string>
                <xpath:string key="owl">http://www.w3.org/2002/07/owl#</xpath:string>
                <xpath:string key="schema">https://schema.org/</xpath:string>
            </xpath:map>
            <xpath:map key="dc:dataset">
                <xpath:string key="dc:dateSubmitted"><xsl:value-of select="xs:string(format-date(current-date(), '[Y01]-[M01]-[D01]'))"/></xpath:string>
                <xpath:string key="dc:creator"><xsl:value-of select="normalize-space(.//provider/title/text()[1])"/></xpath:string>
                <xpath:map key="dc:source">
                    <xpath:string key="rdfs:literal"/>
                    <xpath:string key="dc:identifier"/>
                </xpath:map>
                <xpath:string key="dc:software">entityXML-2-toolbox-conversion</xpath:string>
                <xpath:map key="dc:publisher">
                    <xpath:string key="rdfs:literal"/>
                    <xpath:string key="dc:identifier"/>
                </xpath:map>
                <xpath:map key="dc:contributor">
                    <xpath:string key="rdfs:literal"/>
                    <xpath:string key="dc:identifier"/>
                </xpath:map>
                <xpath:string key="dc:description"><xsl:value-of select="normalize-space(.//provider/abstract/text()[1])"/></xpath:string>
                <xpath:string key="dc:sizeOrDuration"></xpath:string>
            </xpath:map>
            <xsl:call-template name="gather-records"/>
        </xpath:map>
    </xsl:template>
    
    <xsl:template name="gather-records">
        <xsl:for-each-group select="collection/data/list/element()[not(name() = ('title', 'abstract'))]" group-by="name()">
            <xsl:variable name="group" select="current-group()"/>
            <xsl:variable name="group-name" select="$group[1]/name()"/>
            <xpath:array key="{exml:map-names($group-name)}">
                <xsl:apply-templates mode="json-xml" select="$group"/>
            </xpath:array>
        </xsl:for-each-group>
    </xsl:template>
   
   
    <!-- 
        ################################################ 
        RECORD TYPES 
        #################################################
    -->
    <xsl:template match="collection/data/list/person" mode="json-xml">
        <xpath:map>
            <!-- dc:identifier -->
            <xsl:call-template name="identifier"/>
            
            <!-- gndo:preferredNameOfThePerson -->
            <xpath:map key="gndo:preferredNameOfThePerson">
                <xpath:string key="@type">Person</xpath:string>
                <xpath:string key="gndo:forename">
                    <xsl:value-of select="normalize-space(gndo:preferredName/gndo:forename/text())"/>
                </xpath:string>
                <xpath:string key="gndo:surname">
                    <xsl:value-of select="normalize-space(gndo:preferredName/gndo:surname/text())"/>
                </xpath:string>
                <xpath:string key="gndo:personalName">
                    <xsl:value-of select="normalize-space(gndo:preferredName/gndo:personalName/text())"/>
                </xpath:string>
                <xpath:string key="gndo:nameAddition">
                    <xsl:value-of select="normalize-space(gndo:preferredName/gndo:nameAddition/text())"/>
                </xpath:string>
                <xpath:string key="gndo:prefix">
                    <xsl:value-of select="normalize-space(gndo:preferredName/gndo:prefix/text())"/>
                </xpath:string>
                <xpath:string key="gndo:counting">
                    <xsl:value-of select="normalize-space(gndo:preferredName/gndo:counting/text())"/>
                </xpath:string>
                <xpath:string key="gndo:additionalName">
                    <xsl:value-of select="normalize-space(gndo:preferredName/gndo:additionalName/text())"/>
                </xpath:string>
            </xpath:map>
            
            <!-- gndo:variantNameOfThePerson -->
            <xpath:array key="gndo:variantNameOfThePerson">
                <xsl:for-each select="gndo:variantName">
                    <xpath:map>
                        <xpath:string key="@type">Person</xpath:string>
                        <xpath:string key="gndo:forename">
                            <xsl:value-of select="normalize-space(gndo:forename/text())"/>
                        </xpath:string>
                        <xpath:string key="gndo:surname">
                            <xsl:value-of select="normalize-space(gndo:surname/text())"/>
                        </xpath:string>
                        <xpath:string key="gndo:personalName">
                            <xsl:value-of select="normalize-space(gndo:personalName/text())"/>
                        </xpath:string>
                        <xpath:string key="gndo:nameAddition">
                            <xsl:value-of select="normalize-space(gndo:nameAddition/text())"/>
                        </xpath:string>
                        <xpath:string key="gndo:prefix">
                            <xsl:value-of select="normalize-space(gndo:prefix/text())"/>
                        </xpath:string>
                        <xpath:string key="gndo:counting">
                            <xsl:value-of select="normalize-space(gndo:counting/text())"/>
                        </xpath:string>
                        <xpath:string key="gndo:additionalName">
                            <xsl:value-of select="normalize-space(gndo:preferredName/gndo:additionalName/text())"/>
                        </xpath:string>
                    </xpath:map>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:gender -->
            <xpath:map key="gndo:gender">
                <xpath:string key="rdfs:literal"><xsl:value-of select="normalize-space(gndo:gender/text())"/></xpath:string>
                <xpath:string key="gndo:gndIdentifier"><xsl:value-of select="gndo:gender/@gndo:term"/></xpath:string>
            </xpath:map> 
            
            <!-- gndo:professionOrOccupation -->
            <xpath:array key="gndo:professionOrOccupation">
                <xsl:for-each select="gndo:professionOrOccupation">
                    <xpath:map>
                        <xpath:string key="rdfs:literal"><xsl:value-of select="normalize-space(text())"/></xpath:string>
                        <xpath:string key="gndo:gndIdentifier"><xsl:value-of select="@gndo:ref"/></xpath:string>
                    </xpath:map>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:functionOrRole -->
            <xpath:array key="gndo:functionOrRole">
                <xsl:for-each select="gndo:functionOrRole">
                    <xpath:map>
                        <xpath:string key="rdfs:literal"><xsl:value-of select="normalize-space(text())"/></xpath:string>
                        <xpath:string key="gndo:gndIdentifier"><xsl:value-of select="@gndo:term"/></xpath:string>
                    </xpath:map>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:affiliation -->
            <xpath:array key="gndo:affiliation">
                <xsl:for-each select="gndo:affiliation">
                    <xpath:map>
                        <xpath:string key="rdfs:literal"><xsl:value-of select="normalize-space(text())"/></xpath:string>
                        <xpath:string key="gndo:gndIdentifier"><xsl:value-of select="@gndo:ref"/></xpath:string>
                    </xpath:map>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:academicDegree -->
            <xpath:string key="gndo:academicDegree">
                <xsl:value-of select="normalize-space(gndo:academicDegree/text())"/>
            </xpath:string>
            
            <!-- gndo:titleOfNobility -->
            <xpath:map key="gndo:titleOfNobility">
                <xpath:string key="rdfs:literal"><xsl:value-of select="normalize-space(gndo:titleOfNobility/text())"/></xpath:string>
                <xpath:string key="gndo:gndIdentifier"><xsl:value-of select="gndo:titleOfNobility/@gndo:ref"/></xpath:string>
            </xpath:map>
            
            <!-- gndo:placeOfBirth -->
            <xpath:array key="gndo:placeOfBirth">
                <xsl:for-each select="gndo:placeOfBirth">
                    <xsl:call-template name="places"/>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:dateOfBirth -->
            <xsl:call-template name="date">
                <xsl:with-param name="element" select="gndo:dateOfBirth"/>
                <xsl:with-param name="default-name" select="'gndo:dateOfBirth'"/>
            </xsl:call-template>
            
            <!-- gndo:placeOfDeath -->
            <xpath:array key="gndo:placeOfDeath">
                <xsl:for-each select="gndo:placeOfDeath">
                    <xsl:call-template name="places"/>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:dateOfDeath -->
            <xsl:call-template name="date">
                <xsl:with-param name="element" select="gndo:dateOfDeath"/>
                <xsl:with-param name="default-name" select="'gndo:dateOfDeath'"/>
            </xsl:call-template>
            
            <!-- gndo:associatedPlace aka. gndo:placeOfActivity -->
            <xpath:array key="gndo:associatedPlace">
                <xsl:for-each select="gndo:placeOfAcitivity">
                    <xsl:call-template name="places"/>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:periodOfActivity -->
            <xpath:map key="gndo:periodOfActivity">
                <xsl:variable name="element" select="gndo:periodOfActivity"/>
                <xsl:variable name="literal" select="normalize-space($element/text())"/>
                <xsl:variable name="beginning" select="$element/@iso-from"/>
                <xsl:variable name="end" select="$element/@iso-to"/>
                
                <xpath:map key="gndo:beginningOfPeriod">
                    <xpath:string key="schema:date"><xsl:value-of select="$beginning"/></xpath:string>
                    <xpath:string key="rdfs:literal"><xsl:value-of select="$literal"/></xpath:string>
                </xpath:map>
                <xpath:map key="gndo:endOfPeriod">
                    <xpath:string key="schema:date"><xsl:value-of select="$end"/></xpath:string>
                    <xpath:string key="rdfs:literal"><xsl:value-of select="$literal"/></xpath:string>
                </xpath:map>
            </xpath:map>
            
            <!-- gndo:biographicalOrHistoricalInformation -->
            <xpath:array key="gndo:biographicalOrHistoricalInformation">
                <xsl:for-each select="gndo:biographicalOrHistoricalInformation">
                    <xpath:map>
                        <xpath:string key="rdfs:label"><xsl:value-of select="@gndo:label"/></xpath:string>
                        <xpath:string key="rdfs:literal"><xsl:value-of select="normalize-space(text())"/></xpath:string>
                    </xpath:map>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:gndIdentifier -->
            <xpath:string key="gndo:gndIdentifier"></xpath:string>
            
            <!-- owl:sameAs -->
            <xpath:array key="owl:sameAs">
                <xsl:for-each select="owl:sameAs">
                    <xpath:map>
                        <xpath:string key="dc:source"></xpath:string>
                        <xpath:string key="dc:identifier"><xsl:value-of select="normalize-space(text())"/></xpath:string>
                    </xpath:map>
                </xsl:for-each>
            </xpath:array>
            
            <!-- gndo:geographicAreaCode -->
            <xpath:string key="gndo:geographicAreaCode">
                <xsl:value-of select="gndo:geographicAreaCode/@gndo:term"/>
            </xpath:string>
        </xpath:map>
    </xsl:template>
    
    
    <xsl:template match="collection/data/list/element()[not(name() = ('title', 'abstract', 'person'))]" mode="json-xml">
        <xpath:map>
            <xsl:call-template name="identifier"/>
        </xpath:map>
    </xsl:template>
    
    
    <!-- 
        ################################################ 
        PROPERTIES 
        #################################################
    -->
    <xsl:template name="identifier">
        <xpath:string key="dc:identifier"><xsl:value-of select="data(@xml:id)"/></xpath:string>
    </xsl:template>
    
    <xsl:template name="places">
        <xpath:map>
            <xpath:string key="rdfs:literal"><xsl:value-of select="normalize-space(text())"/></xpath:string>
            <xpath:string key="gndo:gndIdentifier"><xsl:value-of select="@gndo:ref"/></xpath:string>
        </xpath:map>
    </xsl:template>
    
    <xsl:template name="date">
        <xsl:param name="element"/>
        <xsl:param name="default-name"/>
        <xsl:variable name="name" select="if ($default-name) then ($default-name) else ($element/name())"/>
        <xsl:variable name="literal" select="normalize-space($element/text())"/>
        
        <xpath:map key="{$name}">
            <xpath:string key="schema:date"><xsl:value-of select="$element/@iso-date"/></xpath:string>
            <xpath:string key="rdfs:literal"><xsl:value-of select="$literal"/></xpath:string>
        </xpath:map>
    </xsl:template>
    
   
    <!-- 
        ################################################ 
        FUNCTIONS 
        #################################################
    -->
    <xsl:function name="exml:map-names">
        <xsl:param name="name"/>
        <xsl:choose>
            <xsl:when test="$name = 'person'">gndo:differentiatedPerson</xsl:when>
            <xsl:otherwise><xsl:value-of select="$name"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
</xsl:stylesheet>