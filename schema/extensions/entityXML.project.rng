<?xml version="1.0" encoding="UTF-8"?>
<grammar 
    xmlns="http://relaxng.org/ns/structure/1.0"
    xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
    xmlns:doc="https://sub.uni-goettingen.de/met/formats/simpledoc#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" 
    xmlns:exml="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    ns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
    
    <include href="../entityXML.rng">
        <define name="elem.project">
            <element name="project">
                <a:documentation xml:lang="de">(<doc:label>Projektinformationen</doc:label>) <doc:desc>Informationen zu dem Projekt, das die in der Ressource enthaltenen Entitäten erschlossen hat.</doc:desc></a:documentation>
                <a:documentation xml:lang="en">(<doc:label>projectinfomation</doc:label>) <doc:desc>Information about the project describing entities in the resource.</doc:desc></a:documentation>
                <attribute name="id">
                    <a:documentation xml:lang="de">(<doc:label>Projekt-ID</doc:label>) <doc:desc>Identifier für das Projekt.</doc:desc></a:documentation>
                    <data type="string"/>
                </attribute>
                <optional>
                    <attribute name="isil"/>
                </optional>
                <ref name="model.metadata.elems"/>
                <ref name="elem.project.specs"/>
                <ref name="elem.project.interview"/>                
            </element>
        </define>
    </include>
    
    <define name="elem.block">
        <element name="block">
            <element name="question">
                <text/>
            </element>
            <element name="desc">
                <text/>
            </element>
        </element>
    </define>
    
    <define name="elem.div">
        <element name="div">
            <element name="title">
                <text/>
            </element>
            <oneOrMore>
                <ref name="elem.block"/>
            </oneOrMore>
        </element>
    </define>
    
    <define name="elem.project.authorityFile">
        <element name="authorityFile">
            <attribute name="xml:base">
                <data type="anyURI"/>
            </attribute>
            <empty/>
        </element>
    </define>
    
    <define name="elem.project.authorityFiles">
        <element name="authorityFiles">
            <zeroOrMore>
                <ref name="elem.project.authorityFile"/>
            </zeroOrMore>
        </element>
    </define>
    
    <define name="elem.project.collection">
        <element name="collection">
            <ref name="elem.project.format"/>
            <ref name="elem.project.timeframe"/>
        </element>
    </define>
    
    <define name="elem.project.duration">
        <element name="duration">
            <attribute name="from"></attribute>
            <attribute name="to"></attribute>
            <text/>
        </element>
    </define>
    
    
    <define name="elem.project.entities">
        <element name="entities">
            <attribute name="count"></attribute>
            <attribute name="types"></attribute>
            <text/>
        </element>
    </define>
    
    <define name="elem.project.format">
        <element name="format">
            <text/>
        </element>
    </define>
    
    <define name="elem.project.idno">
        <element name="idno">
            <attribute name="type"/>
            <text/>
        </element>
    </define>
    
    <define name="elem.project.interview">
        <element name="interview">
            <zeroOrMore>
                <ref name="elem.div"/>
            </zeroOrMore>
        </element>
    </define>
    
    <define name="elem.project.specs">
        <element name="specs">
            <ref name="elem.project.idno"/>
            <ref name="elem.project.duration"/>
            <ref name="elem.project.entities"/>
            <ref name="elem.project.authorityFiles"/>
            <ref name="elem.project.collection"/>
        </element>
    </define>
    
    <define name="elem.project.timeframe">
        <element name="timeframe">
            <attribute name="freq"/>
            <oneOrMore>
                <ref name="elem.tf.date"/>
            </oneOrMore>
        </element>
    </define>
    
    <define name="elem.tf.date">
        <element name="date">
            <attribute name="when"/>
            <text/>
        </element>
    </define>
    
</grammar>