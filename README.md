# entityXML
> Textmarkup XML Format zur halbstrukturierten Beschreibung von Entitäten
> 
> **Anmerkung**: entityXML ist momentan eine Konzeptstudie im ALPHA Stadium, daher sind Änderungen am Datenmodell momentan an der Tagesordnung.

## Download
- **GitLab**: clone this Repository from GitLab using the "Clone" Button 
- **Git**: 
  - Using HTTPS: `git clone https://gitlab.gwdg.de/entities/entityxml.git`
  - Using SSH: `git clone git@gitlab.gwdg.de:entities/entityxml.git`


## Abstract
entityXML provides markup to encode entities like Persons, Places etc.


## Documentation
For more information visit the official german documentation on [entities.pages.gwdg.de/entityxml](https://entities.pages.gwdg.de/entityxml)

You will also find a single file markdown documentation here: [`docs/markdown/entityXML_documentation.md`](docs/markdown/entityXML_documentation.md)

> **Note**: A deprecated version of the documentation is placed in this repositories wiki. It is **not up-to-date** and will be removed eventually, so do yourself a favour and stick to the official documentation linked above.


## How to use?
If you want to keep it simple copy these Processing Instructions into an empty XML-File and go:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```
These Instructions reference the schema, its schematron validation routines and the author mode css right from the GitLab. This (and an online connection) is everything to start working with entityXML in oXygen. If you prefere more, just download the framework as described below and integrate it into oXygen.

### entityXML oXygen Framework
entityXML comes with a Document Type Association Framework that can be used by oXygen XML Editor.

#### Preferred Setup: Install entityXML as oXygen Addon
Installing **entityXML as an add-on is the preferred way of using the framework professionaly** and has the advantage that it automatically checks for updates. Accordingly, the installed frameworks always remain up-to-date and do not have to be updated manually.

To install entityXML as an add-on, you will need the URL of the add-on updates.xml configuration file:

```
https://gitlab.gwdg.de/entities/updates/-/raw/main/entityxml/updates.xml
```

Now in oXygen XML navigate to `Hilfe > Neue Add-Ons installieren` and copy the URL into the field `Add-Ons zeigen von`.
Now you can select the Add-Ons you like to install. From now on oXygen is checking for Updates on every startup.

Please find further information in the official [entityXML Handbook](https://entities.pages.gwdg.de/entityxml/#setup/#verwendung-als-oxygen-xml-framework) and in the [oXygen Documentation](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/topics/installing-and-updating-add-ons.html).

#### Alternative Setup for developers (Option 1): Fix Integration into oXygen Frameworks
- Go to the `frameworks` directory of your oXygen XML Application
- Create a directory `entityXML`
- Copy the content of this repository into the created folder


#### Alternative Setup for developers (Option 2): Loose Integration as Additonal Framework 
- Go to `Optionen` > `Einstellungen`
- Choose `Dokumenttypen-Zuordnung` from the List on the left and chose the subentry `Orte`
  - Add the path to the parent directory to `Zusätzliche Framework-Verzeichnisse` where your entityXML Folder lives on your computer
  

If you want to use the entityXML Framework within [TextGrid Laboratory](https://textgrid.de/):


- Go to `Fenster` > `Benutzervorgaben`
- Choose `Oxygen XML Editor` > `Document Type Association` from the List on the left and chose the subentry `Locations`
  - Add the path to the parent directory to `Additonal framework directories` where your entityXML Folder lives on your computer



Now you can use the entityXML Framework within oXygen, e.g. to do preconfigured conversions, Validation, Author Mode etc.


## Repo Structure

```text
assets/               # CSS and JS Files
  └── css/
       └── author/    # Author Mode CSS    
  └── js/
docs/                 # XML and Markdown Documentations
  └── markdown/       
samples/              # entityXML Samplefiles to exemplify 
schema/               # entityXML Schema
scripts/              # XSLT and XQuery Conversions
  └── xquery/
  └── xslt/ 
templates/            # Copy & Paste Templates
            
```


## Licence and Contact
entityXML is published under MIT Licence.

Questions, remarks or other kinds of comments?: Uwe Sikora, SUB Göttingen [<sikora@sub.uni-goettingen.de>](mailto:sikora@sub.uni-goettingen.de)