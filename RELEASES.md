# v0.6.5 (Beta)
- Anpassung der MARC-XML Transformation
- Anpassungen am Datenmodell, siehe hierzu im Handbuch unter "Anhang 3"
- Konkordanzskript hinzugefügt

# v0.6.0 (Beta)
- Anpassung der MARC-XML Transformation: 
  - Es wird kein Link mehr zur GND-Agentur Schematron Validierung gesetzt! Die Validierung kann durch die Verwendung eines separaten Frameworks durchgeführt werden.
  - Verfeinerung der Konversionen
- Kleinere Anpassungen am Datenmodell

# v0.5.2 (Alpha)
- `img` Property für jeden Record hinzugefügt.

# v0.5.1 (Alpha)
- Framework wird nun als oXygen Add-On angeboten

# v0.5.0 (Alpha)