
<a name="entity-biogr-histogr"></a>
# Biographische und/oder historische Angaben zur Entität

Um ergänzende biographische, historische oder andere Angaben zu einer Entität in textlicher Form zu dokumentieren, dient das Element [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463). Es ist *optional* und *wiederholbar*, sprich: Für eine Entität können beliebig viele dieser Angaben erstellt werden.

Zusätzliche lässt sich die verwendete Sprache der Angabe via ``@xml:lang`` eindeutig spezifizieren. Als default (sprich, falls kein `@xml:lang` gesetzt ist) gilt die Sprache der Angabe als **Deutsch**.

*Eine Anmerkung zur Form der Angabe*: Der Erfassungsleitfaden zum äquivalenten PICA3/MARC 21 Feld **[678](https://wiki.dnb.de/download/attachments/50759357/678.pdf)** (Biografische, historische und andere Angaben) empfielt einen "möglichst prägnant[en] und kurz[en]" Text, wobei die verwendete Sprache auf Deutsch festgelegt ist.

```xml
<place xml:id="goslar" gndo:uri="http://d-nb.info/gnd/4021643-3">
   <gndo:preferredName>Goslar</gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation>Kreisstadt des Landkreises 
      Goslar, Niedersachsen</gndo:biographicalOrHistoricalInformation>
   <gndo:biographicalOrHistoricalInformation xml:lang="en">Historic town in 
      Lower Saxony, Germany and the administrative centre 
      of the district of Goslar.</gndo:biographicalOrHistoricalInformation>
</place>
```
