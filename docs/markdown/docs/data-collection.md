
<a name="data-collection"></a>
# Datenbereich

Der Datenbereich einer [`entityXML`](specs-elems.md#entity-xml) Ressource wird durch das Element[`data`](specs-elems.md#data-section) repräsentiert, das eine oder mehrere Datenlisten ([`list`](specs-elems.md#data-list)) enthalten kann.

Jede Liste kann durch einen Titel ([`title`](specs-elems.md#d17e6862)) und ein Abstract (`abstract`) näher beschrieben werden:

```xml
<data>
   <list>
      <title><!-- Titel der Liste --></title>
      <abstract><!-- Kurzbeschreibung der Liste --></abstract>
      <!-- Einer oder mehrere Entitäteneinträge folgen hier -->
   </list>
</data>
```

Jede Liste bildet schließlich den Kontext für die zu dokumentierenden Entitätseinträge.
