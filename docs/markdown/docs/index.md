
<a name="introduction"></a>
# entityXML: Ein Austausch- und Speicherformat

entityXML ist (bisher) eine Konzeptstudie in der Version *0.6.5* (BETA), die darauf abzielt, ein einheitliches XML basiertes Datenformat für die Text+ GND Agentur zu modellieren.

Das Format soll dabei vorallem drei Aspekte abdecken: Die Bereitstellung eines einheitlichen (1) **Austausch**- und (2) **Speicherformats** zur Beschreibung von Entitäten, das direkt auf die GND gemappt werden kann, und der Text+ GND Agentur zudem als (3) **Workflow-Steuerungsinstrument** dient.

<a name="docs_d14e88"></a>
## Als Austauschformat

Als <span class="emph">Austauschformat</span> für die Text+ GND-Agentur soll entityXML als transparentes und dokumentiertes Format dienen, um Daten zwischen Datenlieferanten (etwa digitale Editions- oder Erschließungsprojekte) und der Text+ GND Agentur systematisch (d.h. gemäß eines wohl definierten Ablaufs) austauschen zu können. Aus diesem Grund baut das entityXML Schema grundlegend auf der [GND-Ontologie](https://d-nb.info/standards/elementset/gnd) als Beschreibungsmodell von Entitäten auf. 

Daten, die in entityXML angelegt und ausgetauscht werden, können mit Hilfe des [entityXML
                     Schemas](resources.md#entityxml-schema-intro) sowohl auf Nutzer- als auch auf Agenturseite validiert werden, um eine erste und regelmäßige Qualitätssicherung zu gewährleisten. Das Datenmodell ermöglicht in der Folge die Implementierung [generischer
                     Transformationsszenarien](resources.md#entityxml-scripts-intro), die in Zukunft unteranderem den Einspeisevorgang in die GND unterstützen sollen. 

Desweiteren untertützt entityXML die Kommunikation zwischen Datenlieferanten und der Text+ GND Agentur, indem Fragen, Anmerkungen und anderweitige Informationen entweder für eine ganze Lieferung oder für einzelne Einträge direkt mit erfasst und ausgewertet werden können.

Das Wichtigste zum Aspekt "<span class="emph">Austauschformat</span>" in Kürze: 

- entityXML baut auf den Klassen und Properties der GND Ontologie auf
- Qualitätssicherung in Hinblick auf die Spezifikation der GND durch Validierung mit dem entityXML Schema
- Automatisierte Transformationsszenarien zum Export von entityXML Daten (z.B. zur Einspeisung in die GND)
- Workflowsteuerung der GND-Agentur
- Fragen an eine GND Agentur können direkt an den Daten dokumentiert werden


<a name="docs_d14e130"></a>
## Als Speicherformat

Als <span class="emph">Speicherformat</span> für Projekte bietet entityXML zahlreiche Möglichkeiten der Verwendung: Einerseits hat man hiermit eine niedrigschwellige Möglichkeit an der Hand, Entitäten wartungsarm und ohne komplexe Speicherlösung zu dokumentieren und zu speichern. Die erschlossenen Daten bleiben somit zum Einen als Erschließungsleistung der entsprechenden Projekte transparent (z.B. zum Nachweis von Arbeitsleistungen für Förderinstitutionen). Zum Zweiten können sie einfach in bestehende Forschungsdatenmanagementkonzepte (z.B. spezifische Versionierungsworkflows, Langzeitarchivierung etc.) eingebunden werden. Zum Dritten (und hier liegt ein echter Vorteil zu Excel und Co.) können die (XML) Daten, die auf diese Weise entstehen, direkt in projektspezifischen Softwarelösungen genutzt werden (z.B. Einspeisung in Webportale, Datenbanken, Erstellung von Registern, etc.).

entityXML basiert zwar auf der Idee, die GND-Ontologie zur Beschreibung von Entitäten zu verwenden. Allerdings ist das Schema <span class="emph">offen</span> gehalten, sprich: Sofern an die Daten, die dokumentiert werden sollen, Anforderungen gestellt werden, die sich über die GND-Ontologie nicht beschreiben lassen, dann bietet entityXML die Möglichkeit, [schemafremde
                     Elemente und Attribute aus eigenen Namensräumen](custom-namespaces.md) mit zu erschließen. Diese Informationen werden zwar bei der Einspeisung in die GND vernachlässigt, nichtsdestoweniger bleiben sie den Projekten als integrale Bestandteile der Forschungsdaten erhalten und können selbstverständlich weiter genutzt werden.

Darüberhinaus bietet entityXML die Möglichkeit sowohl auf Datensammlungs- als auch auf Eintragsebene detailierte Revisionsbeschreibungen vorzunehmen, um Recherchestände zu dokumentieren - und kann dementsprechend dabei helfen projektinterne Arbeitsabläufe zu unterstützen.

Das Wichtigste zum Aspekt "<span class="emph">Speicherformat</span>" in Kürze: 

- Projekte können ihre Daten unabhängig von einer komplexen Speicherlösung speichern
- Recherchearbeit bleibt transparent und nachvollziehbar
- Daten bleiben als Forschungsdaten des Projektes transparent 
- Daten können versioniert werden
- Bearbeitungsstand kann beschrieben und so dokumentiert werden
- Enkoppelung von Projekten und GND in Hinblick auf Zeitpläne, da Datenlieferanten nicht auf die Eintragung in die GND warten müssen, sondern ihre Daten direkt nutzen können.
- Möglichkeit der Verwendung von Elementen in *custom namespaces*
- Markup, also etwas mit dem vorallem Editionsprojekte Erfahrung haben

