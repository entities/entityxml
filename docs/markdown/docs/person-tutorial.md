
<a name="docs_d14e2517"></a>
# HOW TO: Person

Dieses Tutorial dient der Verdeutlichung, wie man in entityXML eine Person dokumentiert.

Als Beispiel werden wir **[Ernest Chevalier](https://fr.wikipedia.org/wiki/Ernest_Chevalier)** (Warum ausgerechnet Ernest Chevalier, werden wir im Laufe des Tutorials sehen) beschreiben, über den unsere Recherche folgende Informationen ergeben hat: 
```text
Chevalier, Ernest (männlich)
Französischer Politiker
Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
Geboren: 14.08.1820, Villers-en-Vexin
Gestorben: 05.12.1887, Paris
```


Wir starten mit dem [`person`](specs-elems.md#person-record) Element, dass wir im Datenbereich (`data/list`) unserer entityXML Ressource bereits angelegt haben. Da jede Entität bzw. jeder Eintrag in entityXML durch eine ID eindeutig identifiziert werden <span class="emph">muss</span>, fügen wir `@xml:id` hinzu. Im Grunde ist die Form der ID frei wählbar, solange es sich um einen [NCName](https://www.w3.org/TR/xml-names11/#NT-NCName) handelt, vereinfacht gesagt: Eine Zeichenkette, die mit einem Buchstaben beginnt und kein ":" enthält. Für unser Beispiel bleiben wir der Einfachheit halber bei `ernest_chevalier`. 

Ausserdem wollen wir einer potenziellen GND Agentur, die diese Daten später verarbeiten soll, mitteilen, dass es sich hierbei um einen Eintrag handelt, der in der GND angelegt werden soll. Das machen wir mit Hilfe von `@agency`, dem wir den Wert "create" geben.

```xml
<data>
   <list>
      
      <!-- Einträge davor -->
      
      <person xml:id="ernest_chevalier" agency="create"></person>
      
      <!-- Einträge danach -->
   </list>
</data>
```

Jetzt ist der Eintrag initial angelegt und identifiziert. Nun können wir unseren Personeneintrag nach und nach mit den recherchierten Daten anreichern. Am Einfachsten wäre es, wenn wir unser Rechercheergebnis erstmal in den leeren Eintrag kopieren. So haben wir schoneinmal alles zusammen:

```xml
<person xml:id="ernest_chevalier" agency="create">
   Chevalier, Ernest (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
</person>

```

Bevor wir uns nun dran machen, Daten in diesem Eintrag zu erschließen, legen wir noch eine Revisionsbeschreibung via [`revision`](specs-elems.md#revision) an und dokumentieren die Bearbeitungsphase und den Zeitpunkt, an dem wir mit diesem Eintrag begonnen haben. Der Eintrag ist ganz frisch angelegt und befindet sich somit in der Anfangsphase; dementsprechend verzeichnen wir "opened" im `@status` Attribut von `revision` und legen einen ersten Änderungseintrag mit dem aktuellen Datum an:

```xml
<person xml:id="ernest_chevalier" agency="create">
   Chevalier, Ernest (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>

```

Bei Ernest Chevalier handelt es sich um eine Person aus Fleisch und Blut und nicht etwa um eine fiktive Person oder gar einen Gott. Dementsprechend gilt er als 'gndo:DifferentiatedPerson' und so benötigen wir keine weitere Spezifizierung via `@gndo:type`.

Da entityXML als **Markupformat** konzipiert ist, können die (derzeit noch) in unstrukturierter, rein menschenlesbarer Form vorliegendenden Daten sukzessive ausgezeichnet werden. Damit ein Personeneintrag (ohne GND-URI) valide ist, d.h. dem Schema von entityXML entspricht, müssen wir mindestend eine **Vorzugsbenennung** angeben, also den Namen, unter dem Ernest Chevalier hauptsächlich bekannt ist. Dafür verwenden wir [`gndo:preferredName`](specs-elems.md#person-record-preferredName):

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>Chevalier, Ernest</gndo:preferredName> (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Das entityXML Schema sieht bei einem Personennamen vor, dass er entweder aus einem *persönlichen Namen* ([`gndo:personalName`](specs-elems.md#d17e5276)) oder aus einem *Vor*- ([`gndo:forename`](specs-elems.md#d17e4522)) und einem *Nachnamen* ([`gndo:surname`](specs-elems.md#d17e5896)) besteht:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Sofern wir in *oXygen XML* oder einem anderen Editor, der "schema-aware" ist, also eine Schemadatei zur automatischen Validierung verwenden kann und dementsprechend Rückmeldung gibt, sehen wir nun ein valides Stück XML: Unser Personeneintrag zu Ernest Chevalier ist eindeutig in der XML identifiziert und durch eine Vorzugsbenennung benannt und erfüllt so die Mindestanforderungen. Nun kümmern wir uns um die weiteren Daten.

Da wir das **biologische Geschlecht** von Herrn Chevalier recherchiert haben und uns diese Information maschienenlesbar zur Verfügung stehen soll, verwenden wir [`gndo:gender`](specs-elems.md#d17e4581) zur Auszeichnung der Information im Text:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender>männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Nun meldet sich die Validierung abermals zu Wort, denn das entityXML Schema sieht vor, dass [`gndo:gender`](specs-elems.md#d17e4581) das Geschlecht explizit über einen Bezeichner aus dem [GND Vokabular "Gender"](https://d-nb.info/standards/vocab/gnd/gender#) ausweist. Dafür verwenden wir `@gndo:term`; die zulässigen Werte sind bereits vordefiniert:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Bisher habe ich noch garnicht gesagt, warum wir Ernest Chevalier überhaupt anlegen. Wie für viele Andere gibt es auch für ihn (noch) keinen Eintrag in der GND. Interessant ist er für unser kleines Tutorial allerdings aus einem anderen Grund, denn die Beschreibung von Ernest Chevalier ist Teil eines <span class="emph">konkreten Anwendungsfalles</span>: Ein Editionsprojekt (im vorliegenden Falle die [Mollenhauer
                     Gesamtausgabe](https://www.uni-goettingen.de/de/klaus+mollenhauer+gesamtausgabe+%28kmg%29/584741.html)) identifiziert alle in der digitalen Edition namentlich aufgeführten Personen über GND-URIs. Für die Fälle, für die kein GND-URI, also kein Eintrag in der GND existiert, sollen die Personen trotzdem eindeutig identifiziert und mit weiteren Daten dokumentiert werden, um hiermit weitere Informationsprozesse zu ermöglichen bzw. zu steuern (z.B. automatisierte Registererstellung, Anzeige zusätzlicher Informationen auf dem Projektportal zu einer Person, Austausch der dokumentierten Daten mit der GND etc.).

Und genau aus diesem Anwendungsszenario stammt die Information "*Vergessene Zusammenhänge (S. 139)*", denn genau hier erwähnt [Klaus Mollenhauer](https://d-nb.info/gnd/118583352) den von uns hier in der Erschließung befindlichen Ernest Chevalier. Entsprechend geben wir diese Referenz als [`gndo:publication`](specs-elems.md#d17e5604) mit dem Verweis auf die Ausgabe von 2003 im **Katalog der Deutschen Nationalbibliothek** via `@dnb:catalogue` an. In der GND existiert zudem ein Werknormdatensatz für die "Vergessenen Zusammenhänge", den wir ebenfalls dokumentieren wollen und mittels `@gndo:ref` angeben:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Bevor wir weiter machen und die restlichen Daten auszeichnen, sollten wir zunächst explizit machen, **wo** wir diese Informationen eigentlich her haben. Wir stützen uns auf die Informationen, die in der französischen Wikipedia aufgeführt sind und verwenden [`source`](specs-elems.md#elem.source), um den *URL der entsprechenden Seite* explizit als **Quelle** auszuzeichnen:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Hierbei handelt es sich auch gleichsam um ein **Dokument für zusätzliche Informationen** zu Ernest Chevalier und da wir nicht alle Einzelheiten in unserem Datensatz aufführen wollen, machen wir durch [`foaf:page`](specs-elems.md#elem.foaf.page) deutlich, dass man auf der Wikiseite näheres zu der hier beschriebenen Person finden kann. Dafür dublizieren wir den URL der französischen Wikipedia Seite und zeichnen ihn entsprechend aus:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

So, nun haben wir es fast geschafft. Bleiben noch die Lebendaten und der Beruf, die bisher "nur" in menschenlesbarer Form im Eintrag existieren.

Um **Geburts- und Sterbedatum** maschienenlesbar auszuzeichnen, verwenden wir [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth) und [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath). Da es sich um Datumsangaben handelt, weisen wir zusätzlich zur menschenlesbaren Form des Datums ein obligatorisches ISO-Datum via `@iso-date` aus:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, Villers-en-Vexin
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

**Geburts- und Sterbeorte** zeichnen wir mittels [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth) und [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath) aus. Auch hier recherchieren wir noch zusätzlich die entsprechenden *Normdateneinträge für Orte in der GND* und weisen diese unter Verwendung der entsprechenden GND-URIs via `@gndo:ref` mit aus, zumindest, sofern wir hierzu fündig werden; ich hatte bei "Villers-en-Vexin" keinen Erfolg und konnte den Ort über die GND nicht identifizieren. Nun sollten wir alle Lebensdaten erschlossen haben:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, <gndo:placeOfDeath 
      gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Als letzte kümmern wir uns noch um die Auszeichnung des Berufs. Ernest Chevalier war Politiker und das hauptberuflich. Wir verwenden [`gndo:professionOrOccupation`](specs-elems.md#d17e5488) und fügen das Attribut `@gndo:type` mit dem Wert "**significant**" hinzu, um auch diese Information menschen- und maschienenlesbar zu dokumentieren; `@gndo:type` dient nämlich in diesem Kontext zu Klassifikation einer Beschäftigung als **Hauptbeschäftigung**. Für den Beruf "Politiker" gibt es einen Deskriptor in den GND Sachbegriffen und zwar "https://d-nb.info/gnd/4046517-2". Diesen geben wir wieder unter Verwendung von `@gndo:ref` mit an:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" 
      gndo:type="significant">Politiker</gndo:professionOrOccupation>
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, <gndo:placeOfDeath 
      gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

<span class="emph">Geschafft</span>! Unser Eintrag zu Ernest Chevalier ist abgeschlossen und (zumindest für die Belange dieses Tutorials) mit genügend Daten angereichert. Den Abschluss dokumentieren wir noch in der Revisionsbeschreibung mittels [`change`](specs-elems.md#d17e2624) und markieren den Eintrag mit Hilfe des `@status` Attributs als Kandidaten für die Übertragung in die GND:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> 
   <!-- Weitere Inhalte der Übersichtlichkeit halber ausgeblendet -->
   <revision status="opened">
      <change when="2023-02-10" who="US" status="candidate">Bearbeitung abgeschlossen; bereit für die Übernahme in die GND.</change>
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Man muss die Daten nicht - wie in diesem Tutorial geschehen - als *Text* strukturieren. Man kann sich auch für eine eher *strukturiertere Form* entscheiden. Dafür nehmen wir einfach unser Beispiel und löschen alle textlichen Informationen, die in einer strukturierten Form eher nicht enthalten sein sollten: 

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname> 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName>
   <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth> 
   <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>
   <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR">Frankreich</gndo:geographicAreaCode>
   <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" gndo:type="significant">Politiker</gndo:professionOrOccupation>
   <gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086" gndo:ref="https://d-nb.info/gnd/118641166X">
      <gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>
      <title>Vergessene Zusammenhänge</title>
      <date>2003</date>, S. 139
   </gndo:publication> 
   <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   <revision status="opened">
      <change when="2023-02-10" who="US" status="candidate">Bearbeitung abgeschlossen; bereit für die Übernahme in die GND.</change>
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Wir haben uns nun bei diesem Schritt noch dazu entschieden, einen **geographischen Bezug** für Ernest Chevalier mit aufzunehmen, und zwar *Frankreich*. Dazu verwenden wir [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode) und weisen einen entsprechenden Deskriptor aus dem [GND Vokabular
                     "Geographic Area Code"](https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR) via `@gndo:term` mit aus.

Und damit sind wir am Ende dieses Tutorial angekommen. Der originale Eintrag zu Chevalier befindet sich übrigens in [`samples/KMG-GND.20230203.0.5.1.xml`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/samples/KMG-GND.20230203.0.5.1.xml) mit der ID `ernest_chevalier`. 

Viel Spaß beim eigenen Anlegen von Personeneinträgen!
