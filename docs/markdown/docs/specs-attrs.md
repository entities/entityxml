
<a name="specs-attrs"></a>
# Attributspezifikationen

Derzeit umfasst das entityXML Schema **57** spezifizierte Attribute.

Die technische Spezifikation für Attribute umfasst folgende Eigenschaften: 

- **Name**: Der Attributsname bzw. XML-name, der das Attribut bezeichnet.
- **Datatype**: Angabe zum spezifizierten Datentyp des Attributs.
- **Label**: Ein oder mehrere Labels in Deutsch (und Englisch).
- **Description**: Eine oder mehrere Beschreibungen, die die Semantik des Attributs beschreiben.
- **Contained by**: Führt die Elemente auf, in denen das entsprechende Attrbut verwendet wird bzw. werden kann.
- **Predefined Values**: Falls spezifiziert, werden hier vordefinierte Werte angegeben.
- **Validation**: Falls spezifiziert, werden hier Validierungsregeln (z.B. Schematron Pattern) für das entsprechende Element angegeben.



<a name="d17e2331"></a>
### ANY ATTRIBUTE
|     |     |
| --- | --- |
| **Name** | *Any name.*  |
| **Datatype** | string |
| **Label** (de) | ANY ATTRIBUTE |
| **Label** (en) | ANY ATTRIBUTE |
| **Description** (de) | Ein Attribut mit frei wählbarem **QName**. |
| **Description** (en) | - |
| **Contained by** | [`<anyElement.broad>`](specs-elems.md#any-all)  [`<anyElement.narrow>`](specs-elems.md#any-restricted) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.record.agency"></a>
### Agenturanfrage
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string (*Schematron Pattern*) |
| **Label** (de) | Agenturanfrage |
| **Label** (en) | agency contact |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Predefined Values***  (multiple choice)

 - **create**: (Eintrag in der GND neu anlegen) Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
 - **update**: (Normdatensatz mit Daten aus dem Eintrag erweitern) Der Normdatensatz soll durch neue Informationen angereichert werden.
 - **merge**: (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
 - **ignore**: (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht bearbeitet.

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="@agency">
      <sch:report test=".='create' and parent::element()[@gndo:uri]" role="error">(Wrong agency mode): You can't choose agency-mode
                        "create" if the entity already exists within the GND-Authority File!</sch:report>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e7177"></a>
### Aktion (Agentur)
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string |
| **Label** (de) | Aktion (Agentur) |
| **Label** (en) | Task (Agentur) |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf eine Property durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`embodimentOf`](specs-elems.md#d17e1553)  [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:abbreviatedName`](specs-elems.md#d17e3272)  [`gndo:academicDegree`](specs-elems.md#d17e3318)  [`gndo:accordingWork`](specs-elems.md#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:affiliation`](specs-elems.md#d17e3398)  [`gndo:author`](specs-elems.md#d17e3422)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:contributor`](specs-elems.md#d17e3635)  [`gndo:counting`](specs-elems.md#d17e3608)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfConferenceOrEvent`](specs-elems.md#d17e4073)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:dateOfPublication`](specs-elems.md#d17e4177)  [`gndo:dateOfTermination`](specs-elems.md#d17e4205)  [`gndo:editor`](specs-elems.md#d17e4281)  [`gndo:exhibitor`](specs-elems.md#d17e4336)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:fieldOfStudy`](specs-elems.md#d17e4451)  [`gndo:firstAuthor`](specs-elems.md#d17e4491)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e4421)  [`gndo:functionOrRole`](specs-elems.md#d17e4550)  [`gndo:gender`](specs-elems.md#d17e4581)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](specs-elems.md#d17e4775)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:literarySource`](specs-elems.md#d17e4888)  [`gndo:organizerOrHost`](specs-elems.md#d17e4943)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:place`](specs-elems.md#entity-place-standard)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e5119)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e5302)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:professionOrOccupation`](specs-elems.md#d17e5488)  [`gndo:pseudonym`](specs-elems.md#d17e5556)  [`gndo:publication`](specs-elems.md#d17e5604)  [`gndo:relatedWork`](specs-elems.md#d17e5806)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e5866)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:titleOfNobility`](specs-elems.md#d17e5969)  [`gndo:topic`](specs-elems.md#d17e6005)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`realizationOf`](specs-elems.md#d17e1479)  [`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)  [`wgs84:lat`](specs-elems.md#d17e3192)  [`wgs84:long`](specs-elems.md#d17e3205) |

***Predefined Values***  (multiple choice)

 - **add**: (Eigenschaft anlegen) Die Eigenschaft soll dem GND-Normdatensatz des Records hinzugefügt werden.
 - **ignore**: (Eigenschaft ingonieren) Die Eigenschaft wird von der Agentur nicht bearbeitet.
 - **remove**: (Eigenschaft entfernen) Die Eigenschaft soll aus dem GND-Normdatensatz entfernt werden.

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-from"></a>
### Anfangsdatum
|     |     |
| --- | --- |
| **Name** | `@iso-from`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Anfangsdatum |
| **Label** (en) | Startdate |
| **Description** (de) | Ein Anfangsdatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | A startdate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfConferenceOrEvent`](specs-elems.md#d17e4073)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7538"></a>
### Angereicherte Eigenschaft
|     |     |
| --- | --- |
| **Name** | `@enriched`  |
| **Datatype** | anyURI |
| **Label** (de) | Angereicherte Eigenschaft |
| **Label** (en) | Enriched Property |
| **Description** (de) | Diese Eigenschaft ist automatisch mithilfe des angegebenen externen Datendienstes angereichert wurden! |
| **Description** (en) | This Property was automatically added to the record using the documented Dataservice! |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`embodimentOf`](specs-elems.md#d17e1553)  [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:abbreviatedName`](specs-elems.md#d17e3272)  [`gndo:academicDegree`](specs-elems.md#d17e3318)  [`gndo:accordingWork`](specs-elems.md#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:affiliation`](specs-elems.md#d17e3398)  [`gndo:author`](specs-elems.md#d17e3422)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:contributor`](specs-elems.md#d17e3635)  [`gndo:counting`](specs-elems.md#d17e3608)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfConferenceOrEvent`](specs-elems.md#d17e4073)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:dateOfPublication`](specs-elems.md#d17e4177)  [`gndo:dateOfTermination`](specs-elems.md#d17e4205)  [`gndo:editor`](specs-elems.md#d17e4281)  [`gndo:exhibitor`](specs-elems.md#d17e4336)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:fieldOfStudy`](specs-elems.md#d17e4451)  [`gndo:firstAuthor`](specs-elems.md#d17e4491)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e4421)  [`gndo:functionOrRole`](specs-elems.md#d17e4550)  [`gndo:gender`](specs-elems.md#d17e4581)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](specs-elems.md#d17e4775)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:literarySource`](specs-elems.md#d17e4888)  [`gndo:organizerOrHost`](specs-elems.md#d17e4943)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:place`](specs-elems.md#entity-place-standard)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e5119)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e5302)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:professionOrOccupation`](specs-elems.md#d17e5488)  [`gndo:pseudonym`](specs-elems.md#d17e5556)  [`gndo:publication`](specs-elems.md#d17e5604)  [`gndo:relatedWork`](specs-elems.md#d17e5806)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e5866)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:titleOfNobility`](specs-elems.md#d17e5969)  [`gndo:topic`](specs-elems.md#d17e6005)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`realizationOf`](specs-elems.md#d17e1479)  [`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)  [`wgs84:lat`](specs-elems.md#d17e3192)  [`wgs84:long`](specs-elems.md#d17e3205) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6668"></a>
### Anmerkungsart
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Anmerkungsart |
| **Description** (de) | Angabe zur Art der Anmerkung, sprich, ob es sich um eine interne Anmerkung handelt. |
| **Contained by** | [`skos:note`](specs-elems.md#elem.skos.note) |

***Predefined Values***  

 - **internal**: (Interne Anmerkung)Eine Anmerkung, die ausschließlich für interne Zwecke dokumentiert wird.

---

<p style="margin-bottom:60px"></p>

<a name="d17e7628"></a>
### Anreicherung
|     |     |
| --- | --- |
| **Name** | `@enrich`  |
| **Datatype** | boolean |
| **Label** (de) | Anreicherung |
| **Label** (en) | enrichment |
| **Description** (de) | Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern! |
| **Description** (en) | The Record is marked to be enriched by a separate enrichment routine provided by an additional conversion script! |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

---

<p style="margin-bottom:60px"></p>

<a name="rev-status"></a>
### Bearbeitungsphase
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsphase |
| **Label** (en) | Phase |
| **Description** (de) | Die Phase der Bearbeitung, in der sich die Datensammlung oder der Eintrag momentan befindet. |
| **Description** (en) | The phase of a records editing-lifecycle. |
| **Contained by** | [`revision`](specs-elems.md#revision) |

***Predefined Values***  

 - **opened**: (Geöffnet, *default*) Die Informationsressource wurde angelegt und befindet sich derzeit in der Bearbeitung durch den Datenlieferanten.
 - **staged**: (Auf dem Prüfstand) Die Informationsressource befindet sich derzeit in der Kontrolle durch eine GND-Agentur: Es wird geprüft, ob die geltenden Qualitätskriterien erfüllt sind. Mögliche Rückfragen und Vorschläge zur Anpassungen werden durch die GND-Agentur dokumentiert und den Datenlieferanten mitgeteilt.
 - **closed**: (Abgeschlossen) Alle Arbeiten an der Informationsressource wurden durchgeführt. Sie gilt als abgeschlossen.

---

<p style="margin-bottom:60px"></p>

<a name="change-status"></a>
### Bearbeitungsstadium
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsstadium |
| **Description** (de) | Der momentane Bearbeitungsstatus der Informationsressource. |
| **Contained by** | [`change`](specs-elems.md#d17e2624) |

***Predefined Values***  

 - **draft**: (Draft, *default*) Die Informationsressource befindet sich in der Bearbeitung.
 - **candidate**: (Kandidat) Die Bearbeitung ist soweit abgeschlossen. Die Informationsressource gilt nun als potenzieller Kandidat zur Übertragung an die GND und ist somit bereit zur Prüfung durch eine GND-Agentur.
 - **embargoed**: (Gesperrt) Die Informationsressource ist derzeit gesperrt. Sie erfüllt entweder nicht die erforderlichen Qualitätskriterien oder ist aus einem anderen Grund, der in dem Änderungseintrag vermerkt ist, gesperrt. Sie muss entsprechend überarbeitet werden.
 - **withdrawn**: (Zurückgezogen) Die Informationsressource wurde zurückgezogen. Sie wird in Hinblick auf eine Übertragung an die GND ignoriert.
 - **cleared**: (Überarbeitet) Die Informationsressource wurde in Hinblick auf die geltenden Qualitätskriterien überarbeitet und gilt nun als überarbeiteter Kandidat, der erneut geprüft wird.
 - **approved**: (Angenommen) Die Prüfung der Informationsressource ist abgeschlossen. Sie erfüllt alle erforderlichen Qualitätsstandards und ist bereit zur Übertragung an die GND. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **submitted**: (An GND geliefert) Die Daten der Informationsressource wurden an die GND zum Übertragen geliefert. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **published**: (Veröffentlicht) Die Daten der Informationsressource wurden in der GND veröffentlicht. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.

---

<p style="margin-bottom:60px"></p>

<a name="d17e6145"></a>
### Breite
|     |     |
| --- | --- |
| **Name** | `@width`  |
| **Datatype** | string |
| **Label** (de) | Breite |
| **Description** (de) | Breite des Bildes. |
| **Contained by** | [`img`](specs-elems.md#elem.img) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6885"></a>
### Certainty
|     |     |
| --- | --- |
| **Name** | `@cert`  |
| **Datatype** | string |
| **Label** (de) | Certainty |
| **Description** (de) | Angabe zum Status der getroffenen Aussage. `@cert` hat einen vorgegebenen, exklusiven Wertebereich ("low", "middle", "high"). |
| **Contained by** | [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfPublication`](specs-elems.md#d17e4177)  [`gndo:dateOfTermination`](specs-elems.md#d17e4205)  [`gndo:forename`](specs-elems.md#d17e4522)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:surname`](specs-elems.md#d17e5896) |

***Predefined Values***  

 - **low**: (unsicher) Die Aussage ist unsicher
 - **middle**: (wahrscheinlich) Die Aussage ist wahrscheinlich
 - **high**: (belegt) Die Aussage ist belegt

---

<p style="margin-bottom:60px"></p>

<a name="d17e5638"></a>
### DNB Katalog
|     |     |
| --- | --- |
| **Name** | `@dnb:catalogue`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | DNB Katalog |
| **Description** (de) | Verknüpfung einer `gndo:publication` mit einem URL eines Eintrags im [Katalog der Deutschen Nationalbibliothek](https://portal.dnb.de/opac.htm). |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e5604) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e7724"></a>
### Datum
|     |     |
| --- | --- |
| **Name** | `@when`  |
| **Datatype** | date dateTime  |
| **Label** (de) | Datum |
| **Description** (de) | Angabe des Datums der Änderung. |
| **Contained by** | [`change`](specs-elems.md#d17e2624)  [`mappingLabel`](specs-elems.md#d17e6259) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-to"></a>
### Enddatum
|     |     |
| --- | --- |
| **Name** | `@iso-to`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Enddatum |
| **Label** (en) | Enddate |
| **Description** (de) | Ein Enddatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An enddate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfConferenceOrEvent`](specs-elems.md#d17e4073)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="event-types"></a>
### Eventtyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Eventtyp |
| **Label** (en) | Type of a Person |
| **Description** (de) | Typisierung des dokumentierten Events (z.B. als Veranstaltungsfolge). Wenn kein `@gndo:type` vergeben wird, gilt die Person als [gndo:ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent). |
| **Description** (en) | Spezifies the event encoded as eg. series of conference or event). If no `@gndo:type` is provided the default is [gndo:ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent). |
| **Contained by** | [`event`](specs-elems.md#d17e320) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent**: (Kongressfolge oder Veranstaltungsfolge) [GNDO](https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5783"></a>
### GND Beziehungstyp-Code
|     |     |
| --- | --- |
| **Name** | `@gndo:code`  |
| **Datatype** | string |
| **Label** (de) | GND Beziehungstyp-Code |
| **Label** (en) | GND Relation Typecode |
| **Description** (de) | Spezifiziert die Art der ausgewiesenen Beziehung über einen **GND-CODE** (vgl. hierzu [Vollständige Liste der GND-Codes für Beziehungen für das Feld 500](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://opus.k10plus.de/frontdoor/deliver/index/docId/417/file/K10plus_Tabelle_4-Codes_500.pdf&ved=2ahUKEwiHmvXt6vaHAxVp8wIHHavBORUQFnoECBoQAQ&usg=AOvVaw3Kh3sIAIamPs7ObESHL1ZF)) vom K10Pplus-Online-Publikations-Server. |
| **Description** (en) | Spezifies the type of the encoded relation using a **GND-CODE**, cf . [Vollständige Liste der GND-Codes für Beziehungen für das Feld 500](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://opus.k10plus.de/frontdoor/deliver/index/docId/417/file/K10plus_Tabelle_4-Codes_500.pdf&ved=2ahUKEwiHmvXt6vaHAxVp8wIHHavBORUQFnoECBoQAQ&usg=AOvVaw3Kh3sIAIamPs7ObESHL1ZF)) from the K10Pplus-Online-Publications-Server. |
| **Contained by** | [`gndo:relatesTo`](specs-elems.md#d17e5757) |

***Predefined Values***  

 - **adre**: (Adressat) 
 - **anno**: (Annotator) 
 - **arch**: (Architekt) 
 - **arra**: (Arrangeur) 
 - **aust**: (Aussteller) 
 - **aut1**: (Verfasser, erster) 
 - **auta**: (Verfasser) 
 - **autf**: (Verfasser, fiktiver) 
 - **autg**: (Verfasser, zugeschriebener) 
 - **autw**: (Verfasser, zweifelhafter) 
 - **autz**: (Verfasser, zitierter) 
 - **bauh**: (Bauherr) 
 - **bear**: (Bearbeiter) 
 - **befr**: (Besitzer, früherer) 
 - **besi**: (Besitzer) 
 - **bete**: (Beteiligte) 
 - **beza**: (Bekanntschaft mit) 
 - **bezb**: (Beziehung beruflich) 
 - **bezf**: (Beziehung familiär) 
 - **bilh**: (Bildhauer) 
 - **bubi**: (Buchbinder) 
 - **chre**: (Choreograf) 
 - **comp**: (Compiler) 
 - **desi**: (Designer) 
 - **dich**: (Textdichter) 
 - **druc**: (Drucker) 
 - **erfi**: (Erfinder) 
 - **feie**: (Gefeierte oder dargestellte Person/Familie) 
 - **foto**: (Fotograf) 
 - **gest**: (Buchgestalter) 
 - **grav**: (Graveur, Stecher) 
 - **grue**: (Gründer) 
 - **hers**: (Hersteller) 
 - **hrsg**: (Herausgeber) 
 - **illu**: (Illustrator, Illuminator) 
 - **istm**: (Instrumentalmusiker) 
 - **kame**: (Verantwortlicher Kameramann) 
 - **kart**: (Kartograf) 
 - **kom1**: (Komponist, erster) 
 - **koma**: (Komponist) 
 - **komg**: (Komponist, zugeschriebener) 
 - **komm**: (Kommentator) 
 - **komw**: (Komponist, zweifelhafter) 
 - **komz**: (Komponist, zitierter) 
 - **kopi**: (Kopist) 
 - **korr**: (Korrespondenzpartner) 
 - **kue1**: (Künstler, erster) 
 - **kueg**: (Künstler, zugeschriebener) 
 - **kuen**: (Künstler) 
 - **kuew**: (Künstler, zweifelhafter) 
 - **kuez**: (Künstler, zitierter) 
 - **kura**: (Kurator) 
 - **leih**: (Leihgeber) 
 - **libr**: (Librettist) 
 - **lith**: (Lithograf) 
 - **malr**: (Maler) 
 - **mitg**: (Mitglied) 
 - **musi**: (Musiker) 
 - **nawi**: (Name, wirklicher) 
 - **obpa**: (Oberbegriff, partitiv) 
 - **pseu**: (Pseudonym) 
 - **radi**: (Radierer) 
 - **reda**: (Redakteur) 
 - **regi**: (Regisseur) 
 - **rela**: (Relation (allgemein)) 
 - **rest**: (Restaurator) 
 - **saen**: (Sänger) 
 - **saml**: (Sammler) 
 - **spon**: (Sponsor, Mäzen) 
 - **spre**: (Sprecher) 
 - **stif**: (Stifter) 
 - **them**: (Thema) 
 - **uebe**: (Übersetzer) 
 - **urhe**: (Urheber) 
 - **vbal**: (Verwandter Begriff (allgemein)) 
 - **verr**: (Veranlasser) 
 - **vfrd**: (Drehbuchautor) 
 - **widm**: (Widmungsempfänger) 

---

<p style="margin-bottom:60px"></p>

<a name="d17e4603"></a>
### GND Gender Descriptor URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Gender Descriptor URI |
| **Description** (de) | Ein oder mehrere URIs von Termen aus dem GND Vokabular "Gender" (https://d-nb.info/standards/vocab/gnd/gender#). |
| **Contained by** | [`gndo:gender`](specs-elems.md#d17e4581) |

***Predefined Values***  (multiple choice)

 - **https://d-nb.info/standards/vocab/gnd/gender#female**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#male**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#notKnown**: *no description available*

---

<p style="margin-bottom:60px"></p>

<a name="d17e6949"></a>
### GND Geographic Area Code Term
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Geographic Area Code Term |
| **Description** (de) | URI eines Terms aus dem GND Vokabular *[Geographic Area Code](https://d-nb.info/standards/vocab/gnd/geographic-area-code)* |
| **Contained by** | [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode) |

***Predefined Values***  

 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#NTHH**: (Neutral Zone (-1993))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA**: (Europe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AAAT**: (Austria (-12.11.1918))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AD**: (Andorra)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AL**: (Albania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT**: (Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-1**: (Burgenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-2**: (Carinthia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-3**: (Lower Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-4**: (Upper Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-5**: (Salzburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-6**: (Styria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-7**: (Tyrol)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-8**: (Vorarlberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-9**: (Vienna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AX**: (Ǻland Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BA**: (Bosnia and Hercegovina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BE**: (Belgium)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BG**: (Bulgaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BY**: (Belarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH**: (Switzerland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AG**: (Aargau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AI**: (Appenzell Innerrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AR**: (Appenzell Ausserrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BE**: (Bern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BL**: (Basel-Landschaft)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BS**: (Basel-Stadt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-FR**: (Fribourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GE**: (Geneva)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GL**: (Glarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GR**: (Grisons)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-JU**: (Jura)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-LU**: (Luzern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NE**: (Neuchâtel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NW**: (Nidwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-OW**: (Obwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SG**: (St. Gallen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SH**: (Schaffhausen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SO**: (Solothurn)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SZ**: (Schwyz)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TG**: (Thurgau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TI**: (Ticino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-UR**: (Uri)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VD**: (Vaud)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VS**: (Valais)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZG**: (Zug)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZH**: (Zürich)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSHH**: (Czechoslovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSXX**: (Serbia and Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CY**: (Cyprus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CZ**: (Czech Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DDDE**: (Germany East)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE**: (Germany)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BB**: (Brandenburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BE**: (Berlin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BW**: (Baden-Württemberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BY**: (Bavaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HB**: (Bremen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HE**: (Hesse)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HH**: (Hamburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-MV**: (Mecklenburg-Vorpommern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI**: (Lower Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NW**: (North Rhine-Westphalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-RP**: (Rhineland-Palatinate)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SH**: (Schleswig-Holstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SL**: (Saarland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SN**: (Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-ST**: (Saxony-Anhalt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-TH**: (Thuringia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DK**: (Denmark)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DXDE**: (Germany (-1949))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-EE**: (Estonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ES**: (Spain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FI**: (Finland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR**: (France)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GB**: (Great Britain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GG**: (Guernsey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GI**: (Gibraltar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GR**: (Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HR**: (Croatia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HU**: (Hungary)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IE**: (Ireland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IM**: (Isle of Man)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IS**: (Iceland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT**: (Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT-32**: (Trentino-Alto Adige Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-JE**: (Jersey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LI**: (Liechtenstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LT**: (Lithuania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LU**: (Luxembourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LV**: (Latvia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MC**: (Monaco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MD**: (Moldova)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ME**: (Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MK**: (North Macedonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MT**: (Malta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NL**: (Netherlands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NO**: (Norway)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PL**: (Poland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PT**: (Portugal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-QV**: (Kosovo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RO**: (Romania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RS**: (Serbia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RU**: (Russia (Federation))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SE**: (Sweden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SI**: (Slovenia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SK**: (Slovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SM**: (San Marino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SUHH**: (Soviet Union)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-UA**: (Ukraine)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-VA**: (Vatican City)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-YUCS**: (Yugoslavia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB**: (Asia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AE**: (United Arab Emirates)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AF**: (Afghanistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AM**: (Armenia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AZ**: (Azerbaijan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BD**: (Bangladesh)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BH**: (Bahrain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BN**: (Brunei)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BT**: (Bhutan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BUMM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN**: (China)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN-54**: (Tibet (China))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-GE**: (Georgia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-HK**: (Hong Kong)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-ID**: (Indonesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IL**: (Israel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IN**: (India)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IQ**: (Iraq)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IR**: (Iran)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JO**: (Jordan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JP**: (Japan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KG**: (Kyrgyzstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KH**: (Cambodia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KP**: (Korea (North))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KR**: (Korea (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KW**: (Kuwait)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KZ**: (Kazakhstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LA**: (Laos)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LB**: (Lebanon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LK**: (Sri Lanka)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MN**: (Mongolia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MO**: (Macau (China : Special Administrative Region))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MV**: (Maldives)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MY**: (Malaysia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-NP**: (Nepal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-OM**: (Oman)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PH**: (Philippines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PK**: (Pakistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-QA**: (Qatar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SA**: (Saudi Arabia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SG**: (Singapore)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SKIN**: (Sikkim)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SY**: (Syria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TH**: (Thailand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TJ**: (Tajikistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TM**: (Turkmenistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TPTL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TR**: (Turkey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TW**: (Taiwan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-UZ**: (Uzbekistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VDVN**: (Vietnam (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VN**: (Vietnam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YDYE**: (Yemen (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YE**: (Yemen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC**: (Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AIDJ**: (French Afars and Issas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AO**: (Angola)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BF**: (Burkina Faso)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BI**: (Burundi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BJ**: (Benin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BW**: (Botswana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CD**: (Congo (Democratic Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CF**: (Central African Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CG**: (Congo (Brazzaville))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CI**: (Côte d'Ivoire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CM**: (Cameroon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CV**: (Cape Verde)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DJ**: (Djibouti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DYBJ**: (Dahomey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DZ**: (Algeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EG**: (Egypt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EH**: (Western Sahara)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ER**: (Eritrea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ET**: (Ethiopia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GA**: (Gabon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GH**: (Ghana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GM**: (Gambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GN**: (Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GQ**: (Equatorial Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GW**: (Guinea-Bissau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-HVBF**: (Upper Volta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KE**: (Kenya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KM**: (Comoros)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LR**: (Liberia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LS**: (Lesotho)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LY**: (Libya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MA**: (Morocco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MG**: (Madagascar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ML**: (Mali)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MR**: (Mauritania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MU**: (Mauritius)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MW**: (Malawi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MZ**: (Mozambique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NA**: (Namibia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NE**: (Niger)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NG**: (Nigeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RHZW**: (Southern Rhodesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RW**: (Rwanda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SC**: (Seychelles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SD**: (Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SL**: (Sierra Leone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SN**: (Senegal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SO**: (Somalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SS**: (South Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ST**: (Sao Tome and Principe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SZ**: (Swaziland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TD**: (Chad)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TG**: (Togo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TN**: (Tunisia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TZ**: (Tanzania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-UG**: (Uganda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-YT**: (Mayotte)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZA**: (South Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZM**: (Zambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZRCD**: (Zaire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZW**: (Zimbabwe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD**: (America)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AG**: (Antigua and Barbuda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AI**: (Anguilla)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-ANHH**: (Netherlands Antilles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AR**: (Argentina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AS**: (American Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AW**: (Aruba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BB**: (Barbados)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BL**: (Saint Barthélemy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BM**: (Bermuda Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BO**: (Bolivia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BR**: (Brazil)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BQ**: (Bonaire, Sint Eustatius and Saba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BS**: (Bahamas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BZ**: (Belize)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CA**: (Canada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CL**: (Chile)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CO**: (Colombia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CR**: (Costa Rica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CU**: (Cuba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CW**: (Curaçao)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DM**: (Dominica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DO**: (Dominican Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-EC**: (Ecuador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GD**: (Grenada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GF**: (French Guiana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GP**: (Guadeloupe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GT**: (Guatemala)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GY**: (Guyana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HN**: (Honduras)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HT**: (Haiti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-JM**: (Jamaica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KN**: (Saint Kitts-Nevis)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KY**: (Cayman Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-LC**: (Saint Lucia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MF**: (Saint Martin, Northern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MQ**: (Martinique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MS**: (Montserrat)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MX**: (Mexico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-NI**: (Nicaragua)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PA**: (Panama)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PE**: (Peru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PM**: (Saint Pierre and Miquelon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PR**: (Puerto Rico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PY**: (Paraguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PZPA**: (Panama Canal Zone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SR**: (Suriname)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SV**: (El Salvador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SX**: (Sint Maarten (Dutch part))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TC**: (Turks and Caicos Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TT**: (Trinidad and Tobago)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-US**: (United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-UY**: (Uruguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VC**: (Saint Vincent and the Grenadines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VE**: (Venezuela)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VG**: (British Virgin Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VI**: (Virgin Islands of the United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE**: (Australia, Oceania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-AU**: (Australia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CC**: (Cocos (Keeling) Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CK**: (Cook Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CTKI**: (Canton and Enderbury)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CX**: (Christmas Island (Indian Ocean))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FJ**: (Fiji)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FM**: (Micronesia (Federated States))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GEHH**: (Gilbert Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GU**: (Guam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-KI**: (Kiribati)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MH**: (Marshall Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MP**: (Northern Mariana Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NC**: (New Caledonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NF**: (Norfolk Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NHVU**: (New Hebrides)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NR**: (Nauru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NZ**: (New Zealand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PCHH**: (Pacific Islands (Trust Territory))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PF**: (French Polynesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PG**: (Papua New Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PW**: (Palau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-SB**: (Solomon Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TK**: (Tokelau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TO**: (Tonga)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TV**: (Tuvalu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-VU**: (Vanuatu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WF**: (Wallis and Futuna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WS**: (Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XH**: (Arctic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI**: (Antarctic Ocean/Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-AQ**: (Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-BQAQ**: (British Antarctic Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-FQHH**: (Terres australes et antarctiques françaises)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-HM**: (Heard and McDonald Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-NQAQ**: (Queen Maud Land)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK**: (Atlantic Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-BV**: (Bouvet Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FK**: (Falkland Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FO**: (Faroe Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GL**: (Greenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GS**: (South Georgia and the South Sandwich Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SH**: (Saint Helena)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SJ**: (Svalbard and Jan Mayen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL**: (Indian Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-IO**: (British Indian Ocean Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-RE**: (Réunion)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-TF**: (French Southern Territories (the))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM**: (Pacific Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-JTUM**: (Johnston Atoll)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-MIUM**: (Midway Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-NU**: (Niue)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PN**: (Pitcairn Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PUUM**: (American Territory in the Pacific (-1986))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-UM**: (United States Misc. Pacific Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-WKUM**: (Wake Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XN**: (Outer Space)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XP**: (International Organizations)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XQ**: (World)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XR**: (Orient)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XS**: (Ancient Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XT**: (Rome)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XU**: (Byzantine Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XV**: (Ottoman Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XW**: (Palestinian Arabs)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XX**: (Arab Countries)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XY**: (Jews)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ**: (Imaginary places)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#ZZ**: (Country unknown)

---

<p style="margin-bottom:60px"></p>

<a name="d17e7026"></a>
### GND Term URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND Term URI |
| **Description** (de) | URI eines Terms entweder aus der **GND** (https://d-nb.info/gnd/) oder einem **GND Vokabular** (https://d-nb.info/standards/vocab/) |
| **Contained by** | [`gndo:functionOrRole`](specs-elems.md#d17e4550)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/standards/vocab/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e7005"></a>
### GND-Referenz
|     |     |
| --- | --- |
| **Name** | `@gndo:ref`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-Referenz |
| **Label** (en) | GND-Reference |
| **Description** (de) | Referenz auf eine Entität in der GND, identifiziert durch einen GND-URI. |
| **Description** (en) | Reference to an entity in the GND identified by a GND-URI. |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`embodimentOf`](specs-elems.md#d17e1553)  [`gndo:accordingWork`](specs-elems.md#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:affiliation`](specs-elems.md#d17e3398)  [`gndo:author`](specs-elems.md#d17e3422)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:contributor`](specs-elems.md#d17e3635)  [`gndo:editor`](specs-elems.md#d17e4281)  [`gndo:exhibitor`](specs-elems.md#d17e4336)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:fieldOfStudy`](specs-elems.md#d17e4451)  [`gndo:firstAuthor`](specs-elems.md#d17e4491)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e4421)  [`gndo:literarySource`](specs-elems.md#d17e4888)  [`gndo:organizerOrHost`](specs-elems.md#d17e4943)  [`gndo:place`](specs-elems.md#entity-place-standard)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e5119)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e5302)  [`gndo:professionOrOccupation`](specs-elems.md#d17e5488)  [`gndo:pseudonym`](specs-elems.md#d17e5556)  [`gndo:publication`](specs-elems.md#d17e5604)  [`gndo:relatedWork`](specs-elems.md#d17e5806)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e5866)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:titleOfNobility`](specs-elems.md#d17e5969)  [`gndo:topic`](specs-elems.md#d17e6005)  [`realizationOf`](specs-elems.md#d17e1479)  [`term`](specs-elems.md#d17e6829) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6984"></a>
### GND-URI
|     |     |
| --- | --- |
| **Name** | `@gndo:uri`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-URI |
| **Label** (en) | GND-URI |
| **Description** (de) | Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e268"></a>
### GNDO-Typ der beschriebenen Entität
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | GNDO-Typ der beschriebenen Entität |
| **Description** (de) | URI eines Entitätstyps (Class) aus der [GNDO](https://d-nb.info/standards/elementset/gnd#). |
| **Contained by** | [`entity`](specs-elems.md#entity-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CorporateBody**: (Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#CorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#Company)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent**: (Konferenz oder Veranstaltung) [GNDO](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent)
 - **https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent**: (Kongressfolge oder Veranstaltungsfolge) [GNDO](https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent)
 - **https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson**: (Individualisierte Person) [GNDO](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson)
 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#Gods)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#Spirits)
 - **https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName**: (Geografikum) [GNDO](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName)
 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#AdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#Country)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#FictivePlace)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#MemberState)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousTerritory)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#WayBorderOrLine)
 - **https://d-nb.info/standards/elementset/gnd#Work**: (Werk) [GNDO](https://d-nb.info/standards/elementset/gnd#Work)
 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#Collection)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectiveManuscript)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#Expression)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#Manuscript)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalWork)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6157"></a>
### Höhe
|     |     |
| --- | --- |
| **Name** | `@height`  |
| **Datatype** | string |
| **Label** (de) | Höhe |
| **Description** (de) | Höhe des Bildes. |
| **Contained by** | [`img`](specs-elems.md#elem.img) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7042"></a>
### ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | ID |
| **Description** (de) | Eine interne ID zur Identifikation des entsprechenden Elements. |
| **Contained by** | [`list`](specs-elems.md#data-list)  [`respStmt`](specs-elems.md#respStmt) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6925"></a>
### ISIL/Bibliothekssiegel
|     |     |
| --- | --- |
| **Name** | `@isil`  |
| **Datatype** | string (*RNG Pattern Matching*) |
| **Label** (de) | ISIL/Bibliothekssiegel |
| **Label** (de) | ISIL/Acronym for libraries |
| **Description** (de) | Eindeutiger Identifikator der Organisation als ISIL (International Standard Identifier for Libraries and Related Organisations). |
| **Description** (de) | ISIL of an Organisation (International Standard Identifier for Libraries and Related Organisations). |
| **Contained by** | [`agency`](specs-elems.md#agency-stmt)  [`provider`](specs-elems.md#data-provider) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">[A-Z]{1,4}-[a-zA-Z0-9\-/:]{1,11}</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-date"></a>
### ISO-Datum
|     |     |
| --- | --- |
| **Name** | `@iso-date`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum |
| **Label** (en) | ISO-Date |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An ISO date in the Format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfPublication`](specs-elems.md#d17e4177)  [`gndo:dateOfTermination`](specs-elems.md#d17e4205)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-notBefore"></a>
### ISO-Datum (frühestes)
|     |     |
| --- | --- |
| **Name** | `@iso-notBefore`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum (frühestes) |
| **Label** (en) | ISO-Date (earliest) |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ als früheste mögliche Zeitangabe für ein Event. |
| **Description** (en) | The earliest possible date for the event in standard form YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfPublication`](specs-elems.md#d17e4177)  [`gndo:dateOfTermination`](specs-elems.md#d17e4205) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-notAfter"></a>
### ISO-Datum (spätestes)
|     |     |
| --- | --- |
| **Name** | `@iso-notAfter`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum (spätestes) |
| **Label** (en) | ISO-Date (earliest) |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ als späteste mögliche Zeitangabe für ein Event. |
| **Description** (en) | The latest possible date for the event in standard form YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfPublication`](specs-elems.md#d17e4177)  [`gndo:dateOfTermination`](specs-elems.md#d17e4205) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6189"></a>
### Identifier-Art
|     |     |
| --- | --- |
| **Name** | `@type`  |
| **Datatype** | string |
| **Label** (de) | Identifier-Art |
| **Description** (de) | Art des Identifiers |
| **Contained by** | [`idno`](specs-elems.md#elem.idno) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e720"></a>
### Körperschaftstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Körperschaftstyp |
| **Label** (en) | Type of a Corporate Body |
| **Description** (de) | Typisierung der dokumentierten Körperschaft (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody). |
| **Description** (en) | Spezifies the Corporate Body encoded as eg. fictional corporate body, company etc.). If no `@gndo:type` is provided the default is [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody). |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#Company)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6963"></a>
### Label
|     |     |
| --- | --- |
| **Name** | `@gndo:label`  |
| **Datatype** | string |
| **Label** (de) | Label |
| **Label** (en) | Label |
| **Description** (de) | Angabe eines Labels, dass mit der entsprechenden Information assoziiert wird, z.B. zur Anzeigensteuerung. |
| **Description** (en) | - |
| **Contained by** | [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7241"></a>
### Labeltyp (Mapping)
|     |     |
| --- | --- |
| **Name** | `@type`  |
| **Datatype** | string |
| **Label** (de) | Labeltyp (Mapping) |
| **Description** (de) | GND Entitätstyp, auf den das Label bezogen werden kann. |
| **Contained by** | [`mappingLabel`](specs-elems.md#d17e6259)  [`term`](specs-elems.md#d17e6829) |

***Predefined Values***  (multiple choice)

 - **CorporateBody**: ((CorporateBody)[https://d-nb.info/standards/elementset/gnd#CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody)
 - **ConferenceOrEvent**: ((ConferenceOrEvent)[https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent)
 - **DifferentiatedPerson**: ((DifferentiatedPerson)[https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson)
 - **PlaceOrGeographicName**: ((PlaceOrGeographicName)[https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName)
 - **Work**: ((Work)[https://d-nb.info/standards/elementset/gnd#Work](https://d-nb.info/standards/elementset/gnd#Work)
 - **Company**: ((Company)[https://d-nb.info/standards/elementset/gnd#Company](https://d-nb.info/standards/elementset/gnd#Company)
 - **FictiveCorporateBody**: ((FictiveCorporateBody)[https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - **MusicalCorporateBody**: ((MusicalCorporateBody)[https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - **OrganOfCorporateBody**: ((OrganOfCorporateBody)[https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - **ProjectOrProgram**: ((ProjectOrProgram)[https://d-nb.info/standards/elementset/gnd#ProjectOrProgram](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - **ReligiousAdministrativeUnit**: ((ReligiousAdministrativeUnit)[https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - **ReligiousCorporateBody**: ((ReligiousCorporateBody)[https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)
 - **CollectivePseudonym**: ((CollectivePseudonym)[https://d-nb.info/standards/elementset/gnd#CollectivePseudonym](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - **Gods**: ((Gods)[https://d-nb.info/standards/elementset/gnd#Gods](https://d-nb.info/standards/elementset/gnd#Gods)
 - **LiteraryOrLegendaryCharacter**: ((LiteraryOrLegendaryCharacter)[https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - **Pseudonym**: ((Pseudonym)[https://d-nb.info/standards/elementset/gnd#Pseudonym](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - **RoyalOrMemberOfARoyalHouse**: ((RoyalOrMemberOfARoyalHouse)[https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - **Spirits**: ((Spirits)[https://d-nb.info/standards/elementset/gnd#Spirits](https://d-nb.info/standards/elementset/gnd#Spirits)
 - **AdministrativeUnit**: ((AdministrativeUnit)[https://d-nb.info/standards/elementset/gnd#AdministrativeUnit](https://d-nb.info/standards/elementset/gnd#AdministrativeUnit)
 - **BuildingOrMemorial**: ((BuildingOrMemorial)[https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial](https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial)
 - **Country**: ((Country)[https://d-nb.info/standards/elementset/gnd#Country](https://d-nb.info/standards/elementset/gnd#Country)
 - **ExtraterrestrialTerritory**: ((ExtraterrestrialTerritory)[https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory](https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory)
 - **FictivePlace**: ((FictivePlace)[https://d-nb.info/standards/elementset/gnd#FictivePlace](https://d-nb.info/standards/elementset/gnd#FictivePlace)
 - **MemberState**: ((MemberState)[https://d-nb.info/standards/elementset/gnd#MemberState](https://d-nb.info/standards/elementset/gnd#MemberState)
 - **NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: ((NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)[https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit](https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)
 - **NaturalGeographicUnit**: ((NaturalGeographicUnit)[https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit](https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit)
 - **ReligiousTerritory**: ((ReligiousTerritory)[https://d-nb.info/standards/elementset/gnd#ReligiousTerritory](https://d-nb.info/standards/elementset/gnd#ReligiousTerritory)
 - **TerritorialCorporateBodyOrAdministrativeUnit**: ((TerritorialCorporateBodyOrAdministrativeUnit)[https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit](https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit)
 - **WayBorderOrLine**: ((WayBorderOrLine)[https://d-nb.info/standards/elementset/gnd#WayBorderOrLine](https://d-nb.info/standards/elementset/gnd#WayBorderOrLine)
 - **Collection**: ((Collection)[https://d-nb.info/standards/elementset/gnd#Collection](https://d-nb.info/standards/elementset/gnd#Collection)
 - **CollectiveManuscript**: ((CollectiveManuscript)[https://d-nb.info/standards/elementset/gnd#CollectiveManuscript](https://d-nb.info/standards/elementset/gnd#CollectiveManuscript)
 - **Expression**: ((Expression)[https://d-nb.info/standards/elementset/gnd#Expression](https://d-nb.info/standards/elementset/gnd#Expression)
 - **Manuscript**: ((Manuscript)[https://d-nb.info/standards/elementset/gnd#Manuscript](https://d-nb.info/standards/elementset/gnd#Manuscript)
 - **MusicalWork**: ((MusicalWork)[https://d-nb.info/standards/elementset/gnd#MusicalWork](https://d-nb.info/standards/elementset/gnd#MusicalWork)
 - **ProvenanceCharacteristic**: ((ProvenanceCharacteristic)[https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic](https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic)
 - **VersionOfAMusicalWork**: ((VersionOfAMusicalWork)[https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork](https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork)
 - **SubjectHeading**: ((SubjectHeading)[https://d-nb.info/standards/elementset/gnd#SubjectHeading](https://d-nb.info/standards/elementset/gnd#SubjectHeading)

---

<p style="margin-bottom:60px"></p>

<a name="d17e4864"></a>
### Language Code
|     |     |
| --- | --- |
| **Name** | `@gndo:code`  |
| **Datatype** | string |
| **Label** (de) | Language Code |
| **Description** (de) | Ein Sprachcode aus dem [LOC ISO 693-2 Language Vocabulary](http://id.loc.gov/vocabulary/iso639-2/) |
| **Contained by** | [`gndo:languageCode`](specs-elems.md#d17e4843) |

***Predefined Values***  

 - **aar**: (Afar): aar (ISO 639-3), aa (ISO 639-1); Afar [Afar]: Einzelsprache, Lebend
 - **abk**: (Abkhazian): abk (ISO 639-3), ab (ISO 639-1); Abchasisch [Abkhazian]: Einzelsprache, Lebend
 - **ace**: (Achinese): ace (ISO 639-3); Achinesisch [Achinese]: Einzelsprache, Lebend
 - **ach**: (Acoli): ach (ISO 639-3); Acholi [Acoli]: Einzelsprache, Lebend
 - **ada**: (Adangme): ada (ISO 639-3); Dangme [Adangme]: Einzelsprache, Lebend
 - **ady**: (Adyghe; Adygei): ady (ISO 639-3); Adygeisch [Adyghe; Adygei]: Einzelsprache, Lebend
 - **afa**: (Afro-Asiatic languages): afa (ISO 639-5); Afroasiatische Sprachen [Afro-Asiatic languages]: Sprachfamilie
 - **afh**: (Afrihili): afh (ISO 639-3); Afrihili [Afrihili]: Einzelsprache, Konstruiert
 - **afr**: (Afrikaans): afr (ISO 639-3), af (ISO 639-1); Afrikaans [Afrikaans]: Einzelsprache, Lebend
 - **ain**: (Ainu): ain (ISO 639-3); Ainu [Ainu]: Einzelsprache, Lebend
 - **aka**: (Akan): aka (ISO 639-3), ak (ISO 639-1); Akan [Akan]: Makrosprache, Lebend
 - **akk**: (Akkadian): akk (ISO 639-3); Akkadisch [Akkadian]: Einzelsprache, Alt
 - **ale**: (Aleut): ale (ISO 639-3); Aleutisch [Aleut]: Einzelsprache, Lebend
 - **alg**: (Algonquian languages): alg (ISO 639-5); Algonkin-Sprachen [Algonquian languages]: Sprachfamilie
 - **alt**: (Southern Altai): alt (ISO 639-3); Südaltaisch [Southern Altai]: Einzelsprache, Lebend
 - **amh**: (Amharic): amh (ISO 639-3), am (ISO 639-1); Amharisch [Amharic]: Einzelsprache, Lebend
 - **ang**: (English, Old (ca. 450–1100)): ang (ISO 639-3); Altenglisch [English, Old (ca. 450–1100)]: Einzelsprache, Historisch
 - **anp**: (Angika): anp (ISO 639-3); Angika [Angika]: Einzelsprache, Lebend
 - **apa**: (Apache languages): apa (ISO 639-5); Apache-Sprachen [Apache languages]: Sprachfamilie
 - **ara**: (Arabic): ara (ISO 639-3), ar (ISO 639-1); Arabisch [Arabic]: Makrosprache, Lebend
 - **arc**: (Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)): arc (ISO 639-3); Reichsaramäisch [Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)]: Einzelsprache, Alt
 - **arg**: (Aragonese): arg (ISO 639-3), an (ISO 639-1); Aragonesisch [Aragonese]: Einzelsprache, Lebend
 - **arn**: (Mapudungun; Mapuche): arn (ISO 639-3); Mapudungun [Mapudungun; Mapuche]: Einzelsprache, Lebend
 - **arp**: (Arapaho): arp (ISO 639-3); Arapaho [Arapaho]: Einzelsprache, Lebend
 - **art**: (Artificial languages): art (ISO 639-5); Konstruierte Sprachen [Artificial languages]: Sprachfamilie
 - **arw**: (Arawak): arw (ISO 639-3); Arawak [Arawak]: Einzelsprache, Lebend
 - **asm**: (Assamese): asm (ISO 639-3), as (ISO 639-1); Assamesisch [Assamese]: Einzelsprache, Lebend
 - **ast**: (Asturian; Bable; Leonese; Asturleonese): ast (ISO 639-3); Asturisch [Asturian; Bable; Leonese; Asturleonese]: Einzelsprache, Lebend
 - **ath**: (Athapascan languages): ath (ISO 639-5); Athapaskische Sprachen [Athapascan languages]: Sprachfamilie
 - **aus**: (Australian languages): aus (ISO 639-5); Australische Sprachen [Australian languages]: Sprachfamilie
 - **ava**: (Avaric): ava (ISO 639-3), av (ISO 639-1); Awarisch [Avaric]: Einzelsprache, Lebend
 - **ave**: (Avestan): ave (ISO 639-3), ae (ISO 639-1); Avestisch [Avestan]: Einzelsprache, Alt
 - **awa**: (Awadhi): awa (ISO 639-3); Awadhi [Awadhi]: Einzelsprache, Lebend
 - **aym**: (Aymara): aym (ISO 639-3), ay (ISO 639-1); Aymara [Aymara]: Makrosprache, Lebend
 - **aze**: (Azerbaijani): aze (ISO 639-3), az (ISO 639-1); Aserbaidschanisch [Azerbaijani]: Makrosprache, Lebend
 - **bad**: (Banda languages): bad (ISO 639-5); Banda-Sprachen [Banda languages]: Sprachfamilie
 - **bai**: (Bamileke languages): bai (ISO 639-5); Bamileke-Sprachen [Bamileke languages]: Sprachfamilie
 - **bak**: (Bashkir): bak (ISO 639-3), ba (ISO 639-1); Baschkirisch [Bashkir]: Einzelsprache, Lebend
 - **bal**: (Baluchi): bal (ISO 639-3); Belutschisch [Baluchi]: Makrosprache, Lebend
 - **bam**: (Bambara): bam (ISO 639-3), bm (ISO 639-1); Bambara [Bambara]: Einzelsprache, Lebend
 - **ban**: (Balinese): ban (ISO 639-3); Balinesisch [Balinese]: Einzelsprache, Lebend
 - **bas**: (Basa): bas (ISO 639-3); Bassa [Basa]: Einzelsprache, Lebend
 - **bat**: (Baltic languages): bat (ISO 639-5); Baltische Sprachen [Baltic languages]: Sprachfamilie
 - **bej**: (Beja; Bedawiyet): bej (ISO 639-3); Bedscha [Beja; Bedawiyet]: Einzelsprache, Lebend
 - **bel**: (Belarusian): bel (ISO 639-3), be (ISO 639-1); Belarussisch [Belarusian]: Einzelsprache, Lebend
 - **bem**: (Bemba): bem (ISO 639-3); Bemba [Bemba]: Einzelsprache, Lebend
 - **ben**: (Bengali): ben (ISO 639-3), bn (ISO 639-1); Bengalisch [Bengali]: Einzelsprache, Lebend
 - **ber**: (Berber languages): ber (ISO 639-5); Berbersprachen [Berber languages]: Sprachfamilie
 - **bho**: (Bhojpuri): bho (ISO 639-3); Bhojpuri [Bhojpuri]: Einzelsprache, Lebend
 - **bih**: (Bihari languages): bih (ISO 639-5), bh (ISO 639-1); Bihari [Bihari languages]: Sprachfamilie
 - **bik**: (Bikol): bik (ISO 639-3); Bikolano [Bikol]: Makrosprache, Lebend
 - **bin**: (Bini; Edo): bin (ISO 639-3); Edo [Bini; Edo]: Einzelsprache, Lebend
 - **bis**: (Bislama): bis (ISO 639-3), bi (ISO 639-1); Bislama [Bislama]: Einzelsprache, Lebend
 - **bla**: (Siksika): bla (ISO 639-3); Blackfoot [Siksika]: Einzelsprache, Lebend
 - **bnt**: (Bantu (Other)): bnt (ISO 639-5); Bantusprachen [Bantu (Other)]: Sprachfamilie
 - **bos**: (Bosnian): bos (ISO 639-3), bs (ISO 639-1); Bosnisch [Bosnian]: Einzelsprache, Lebend
 - **bra**: (Braj): bra (ISO 639-3); Braj-Bhakha [Braj]: Einzelsprache, Lebend
 - **bre**: (Breton): bre (ISO 639-3), br (ISO 639-1); Bretonisch [Breton]: Einzelsprache, Lebend
 - **btk**: (Batak languages): btk (ISO 639-5); Bataksprachen [Batak languages]: Sprachfamilie
 - **bua**: (Buriat): bua (ISO 639-3); Burjatisch [Buriat]: Makrosprache, Lebend
 - **bug**: (Buginese): bug (ISO 639-3); Buginesisch [Buginese]: Einzelsprache, Lebend
 - **bul**: (Bulgarian): bul (ISO 639-3), bg (ISO 639-1); Bulgarisch [Bulgarian]: Einzelsprache, Lebend
 - **byn**: (Blin; Bilin): byn (ISO 639-3); Blin [Blin; Bilin]: Einzelsprache, Lebend
 - **cad**: (Caddo): cad (ISO 639-3); Caddo [Caddo]: Einzelsprache, Lebend
 - **cai**: (Central American Indian languages): cai (ISO 639-5); Mesoamerikanische Sprachen [Central American Indian languages]: Sprachfamilie
 - **car**: (Galibi Carib): car (ISO 639-3); Karib [Galibi Carib]: Einzelsprache, Lebend
 - **cat**: (Catalan; Valencian): cat (ISO 639-3), ca (ISO 639-1); Katalanisch, Valencianisch [Catalan; Valencian]: Einzelsprache, Lebend
 - **cau**: (Caucasian languages): cau (ISO 639-5); Kaukasische Sprachen [Caucasian languages]: Sprachfamilie
 - **ceb**: (Cebuano): ceb (ISO 639-3); Cebuano [Cebuano]: Einzelsprache, Lebend
 - **cel**: (Celtic languages): cel (ISO 639-5); Keltische Sprachen [Celtic languages]: Sprachfamilie
 - **cha**: (Chamorro): cha (ISO 639-3), ch (ISO 639-1); Chamorro [Chamorro]: Einzelsprache, Lebend
 - **chb**: (Chibcha): chb (ISO 639-3); Chibcha [Chibcha]: Einzelsprache, Ausgestorben
 - **che**: (Chechen): che (ISO 639-3), ce (ISO 639-1); Tschetschenisch [Chechen]: Einzelsprache, Lebend
 - **chg**: (Chagatai): chg (ISO 639-3); Tschagataisch [Chagatai]: Einzelsprache, Ausgestorben
 - **chk**: (Chuukese): chk (ISO 639-3); Chuukesisch [Chuukese]: Einzelsprache, Lebend
 - **chm**: (Mari): chm (ISO 639-3); Mari [Mari]: Makrosprache, Lebend
 - **chn**: (Chinook jargon): chn (ISO 639-3); Chinook Wawa [Chinook jargon]: Einzelsprache, Lebend
 - **cho**: (Choctaw): cho (ISO 639-3); Choctaw [Choctaw]: Einzelsprache, Lebend
 - **chp**: (Chipewyan; Dene Suline): chp (ISO 639-3); Chipewyan [Chipewyan; Dene Suline]: Einzelsprache, Lebend
 - **chr**: (Cherokee): chr (ISO 639-3); Cherokee [Cherokee]: Einzelsprache, Lebend
 - **chu**: (Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic): chu (ISO 639-3), cu (ISO 639-1); Kirchenslawisch, Altkirchenslawisch [Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic]: Einzelsprache, Alt
 - **chv**: (Chuvash): chv (ISO 639-3), cv (ISO 639-1); Tschuwaschisch [Chuvash]: Einzelsprache, Lebend
 - **chy**: (Cheyenne): chy (ISO 639-3); Cheyenne [Cheyenne]: Einzelsprache, Lebend
 - **cmc**: (Chamic languages): cmc (ISO 639-5); Chamische Sprachen [Chamic languages]: Sprachfamilie
 - **cnr**: (Montenegrin): cnr (ISO 639-3); Montenegrinisch [Montenegrin]: Einzelsprache, Lebend
 - **cop**: (Coptic): cop (ISO 639-3); Koptisch [Coptic]: Einzelsprache, Ausgestorben
 - **cor**: (Cornish): cor (ISO 639-3), kw (ISO 639-1); Kornisch [Cornish]: Einzelsprache, Lebend
 - **cos**: (Corsican): cos (ISO 639-3), co (ISO 639-1); Korsisch [Corsican]: Einzelsprache, Lebend
 - **cpe**: (Creoles and pidgins, English based): cpe (ISO 639-5); Englisch-basierte Kreols und Pidgins [Creoles and pidgins, English based]: Sprachfamilie
 - **cpf**: (Creoles and pidgins, French-based): cpf (ISO 639-5); Französisch-basierte Kreols und Pidgins [Creoles and pidgins, French-based]: Sprachfamilie
 - **cpp**: (Creoles and pidgins, Portuguese-based): cpp (ISO 639-5); Portugiesisch-basierte Kreols und Pidgins [Creoles and pidgins, Portuguese-based]: Sprachfamilie
 - **cre**: (Cree): cre (ISO 639-3), cr (ISO 639-1); Cree [Cree]: Makrosprache, Lebend
 - **crh**: (Crimean Tatar; Crimean Turkish): crh (ISO 639-3); Krimtatarisch [Crimean Tatar; Crimean Turkish]: Einzelsprache, Lebend
 - **crp**: (Creoles and pidgins): crp (ISO 639-5); Kreol- und Pidginsprachen [Creoles and pidgins]: Sprachfamilie
 - **csb**: (Kashubian): csb (ISO 639-3); Kaschubisch [Kashubian]: Einzelsprache, Lebend
 - **cus**: (Cushitic languages): cus (ISO 639-5); Kuschitische Sprachen [Cushitic languages]: Sprachfamilie
 - **dak**: (Dakota): dak (ISO 639-3); Dakota [Dakota]: Einzelsprache, Lebend
 - **dan**: (Danish): dan (ISO 639-3), da (ISO 639-1); Dänisch [Danish]: Einzelsprache, Lebend
 - **dar**: (Dargwa): dar (ISO 639-3); Darginisch [Dargwa]: Einzelsprache, Lebend
 - **day**: (Land Dayak languages): day (ISO 639-5); Land-Dayak-Sprachen [Land Dayak languages]: Sprachfamilie
 - **del**: (Delaware): del (ISO 639-3); Delawarisch [Delaware]: Makrosprache, Lebend
 - **den**: (Slave (Athapascan)): den (ISO 639-3); Slavey [Slave (Athapascan)]: Makrosprache, Lebend
 - **dgr**: (Dogrib): dgr (ISO 639-3); Dogrib [Dogrib]: Einzelsprache, Lebend
 - **din**: (Dinka): din (ISO 639-3); Dinka [Dinka]: Makrosprache, Lebend
 - **div**: (Divehi; Dhivehi; Maldivian): div (ISO 639-3), dv (ISO 639-1); Dhivehi [Divehi; Dhivehi; Maldivian]: Einzelsprache, Lebend
 - **doi**: (Dogri): doi (ISO 639-3); Dogri [Dogri]: Makrosprache, Lebend
 - **dra**: (Dravidian languages): dra (ISO 639-5); Dravidische Sprachen [Dravidian languages]: Sprachfamilie
 - **dsb**: (Lower Sorbian): dsb (ISO 639-3); Niedersorbisch [Lower Sorbian]: Einzelsprache, Lebend
 - **dua**: (Duala): dua (ISO 639-3); Duala [Duala]: Einzelsprache, Lebend
 - **dum**: (Dutch, Middle (ca. 1050–1350)): dum (ISO 639-3); Mittelniederländisch [Dutch, Middle (ca. 1050–1350)]: Einzelsprache, Historisch
 - **dyu**: (Dyula): dyu (ISO 639-3); Dioula [Dyula]: Einzelsprache, Lebend
 - **dzo**: (Dzongkha): dzo (ISO 639-3), dz (ISO 639-1); Dzongkha [Dzongkha]: Einzelsprache, Lebend
 - **efi**: (Efik): efi (ISO 639-3); Efik [Efik]: Einzelsprache, Lebend
 - **egy**: (Egyptian (Ancient)): egy (ISO 639-3); Ägyptisch [Egyptian (Ancient)]: Einzelsprache, Alt
 - **eka**: (Ekajuk): eka (ISO 639-3); Ekajuk [Ekajuk]: Einzelsprache, Lebend
 - **elx**: (Elamite): elx (ISO 639-3); Elamisch [Elamite]: Einzelsprache, Alt
 - **eng**: (English): eng (ISO 639-3), en (ISO 639-1); Englisch [English]: Einzelsprache, Lebend
 - **enm**: (English, Middle (1100–1500)): enm (ISO 639-3); Mittelenglisch [English, Middle (1100–1500)]: Einzelsprache, Historisch
 - **epo**: (Esperanto): epo (ISO 639-3), eo (ISO 639-1); Esperanto [Esperanto]: Einzelsprache, Konstruiert
 - **est**: (Estonian): est (ISO 639-3), et (ISO 639-1); Estnisch [Estonian]: Makrosprache, Lebend
 - **ewe**: (Ewe): ewe (ISO 639-3), ee (ISO 639-1); Ewe [Ewe]: Einzelsprache, Lebend
 - **ewo**: (Ewondo): ewo (ISO 639-3); Ewondo [Ewondo]: Einzelsprache, Lebend
 - **fan**: (Fang): fan (ISO 639-3); Fang [Fang]: Einzelsprache, Lebend
 - **fao**: (Faroese): fao (ISO 639-3), fo (ISO 639-1); Färöisch [Faroese]: Einzelsprache, Lebend
 - **fat**: (Fanti): fat (ISO 639-3); Fante [Fanti]: Einzelsprache, Lebend
 - **fij**: (Fijian): fij (ISO 639-3), fj (ISO 639-1); Fidschi [Fijian]: Einzelsprache, Lebend
 - **fil**: (Filipino; Pilipino): fil (ISO 639-3); Filipino [Filipino; Pilipino]: Einzelsprache, Lebend
 - **fin**: (Finnish): fin (ISO 639-3), fi (ISO 639-1); Finnisch [Finnish]: Einzelsprache, Lebend
 - **fiu**: (Finno-Ugrian languages): fiu (ISO 639-5); Finno-ugrische Sprachen [Finno-Ugrian languages]: Sprachfamilie
 - **fon**: (Fon): fon (ISO 639-3); Fon [Fon]: Einzelsprache, Lebend
 - **frm**: (French, Middle (ca. 1400–1600)): frm (ISO 639-3); Mittelfranzösisch [French, Middle (ca. 1400–1600)]: Einzelsprache, Historisch
 - **fro**: (French, Old (842–ca. 1400)): fro (ISO 639-3); Altfranzösisch [French, Old (842–ca. 1400)]: Einzelsprache, Historisch
 - **frr**: (Northern Frisian): frr (ISO 639-3); Nordfriesisch [Northern Frisian]: Einzelsprache, Lebend
 - **frs**: (East Frisian Low Saxon): frs (ISO 639-3); Ostfriesisches Platt [East Frisian Low Saxon]: Einzelsprache, Lebend
 - **fry**: (Western Frisian): fry (ISO 639-3), fy (ISO 639-1); Westfriesisch [Western Frisian]: Einzelsprache, Lebend
 - **ful**: (Fulah): ful (ISO 639-3), ff (ISO 639-1); Fulfulde [Fulah]: Makrosprache, Lebend
 - **fur**: (Friulian): fur (ISO 639-3); Furlanisch [Friulian]: Einzelsprache, Lebend
 - **gaa**: (Einzelsprache): gaa (ISO 639-3), Ga (ISO 639-1); Ga [Einzelsprache]: Lebend, Gã 
 - **gay**: (Gayo): gay (ISO 639-3); Gayo [Gayo]: Einzelsprache, Lebend
 - **gba**: (Gbaya): gba (ISO 639-3); Gbaya-Sprachen [Gbaya]: Makrosprache, Lebend
 - **gem**: (Germanic languages): gem (ISO 639-5); Germanische Sprachen [Germanic languages]: Sprachfamilie
 - **gez**: (Geez): gez (ISO 639-3); Altäthiopisch [Geez]: Einzelsprache, Alt
 - **gil**: (Gilbertese): gil (ISO 639-3); Kiribatisch, Gilbertesisch [Gilbertese]: Einzelsprache, Lebend
 - **gla**: (Gaelic; Scottish Gaelic): gla (ISO 639-3), gd (ISO 639-1); Schottisch-gälisch [Gaelic; Scottish Gaelic]: Einzelsprache, Lebend
 - **gle**: (Irish): gle (ISO 639-3), ga (ISO 639-1); Irisch [Irish]: Einzelsprache, Lebend
 - **glg**: (Galician): glg (ISO 639-3), gl (ISO 639-1); Galicisch, Galegisch [Galician]: Einzelsprache, Lebend
 - **glv**: (Manx): glv (ISO 639-3), gv (ISO 639-1); Manx,Manx-Gälisch [Manx]: Einzelsprache, Lebend
 - **gmh**: (German, Middle High (ca. 1050–1500)): gmh (ISO 639-3); Mittelhochdeutsch [German, Middle High (ca. 1050–1500)]: Einzelsprache, Historisch
 - **goh**: (German, Old High (ca. 750–1050)): goh (ISO 639-3); Althochdeutsch [German, Old High (ca. 750–1050)]: Einzelsprache, Historisch
 - **gon**: (Gondi): gon (ISO 639-3); Gondi [Gondi]: Makrosprache, Lebend
 - **gor**: (Gorontalo): gor (ISO 639-3); Gorontalo [Gorontalo]: Einzelsprache, Lebend
 - **got**: (Gothic): got (ISO 639-3); Gotisch [Gothic]: Einzelsprache, Alt
 - **grb**: (Grebo): grb (ISO 639-3); Grebo [Grebo]: Makrosprache, Lebend
 - **grc**: (Greek, Ancient (bis 1453)): grc (ISO 639-3); Altgriechisch [Greek, Ancient (bis 1453)]: Einzelsprache, Historisch
 - **grn**: (Guarani): grn (ISO 639-3), gn (ISO 639-1); Guaraní [Guarani]: Makrosprache, Lebend
 - **gsw**: (Swiss German; Alemannic; Alsatian): gsw (ISO 639-3); Schweizerdeutsch [Swiss German; Alemannic; Alsatian]: Einzelsprache, Lebend
 - **guj**: (Gujarati): guj (ISO 639-3), gu (ISO 639-1); Gujarati [Gujarati]: Einzelsprache, Lebend
 - **gwi**: (Gwich'in): gwi (ISO 639-3); Gwich'in (Sprache) [Gwich'in]: Einzelsprache, Lebend
 - **hai**: (Haida): hai (ISO 639-3); Haida [Haida]: Makrosprache, Lebend
 - **hat**: (Haitian; Haitian Creole): hat (ISO 639-3), ht (ISO 639-1); Haitianisch-Kreolisch [Haitian; Haitian Creole]: Einzelsprache, Lebend
 - **hau**: (Hausa): hau (ISO 639-3), ha (ISO 639-1); Hausa [Hausa]: Einzelsprache, Lebend
 - **haw**: (Hawaiian): haw (ISO 639-3); Hawaiisch [Hawaiian]: Einzelsprache, Lebend
 - **heb**: (Hebrew): heb (ISO 639-3), he (ISO 639-1); Hebräisch [Hebrew]: Einzelsprache, Lebend
 - **her**: (Herero): her (ISO 639-3), hz (ISO 639-1); Otjiherero [Herero]: Einzelsprache, Lebend
 - **hil**: (Hiligaynon): hil (ISO 639-3); Hiligaynon [Hiligaynon]: Einzelsprache, Lebend
 - **him**: (Himachali languages; Western Pahari languages): him (ISO 639-5); West-Paharisprachen [Himachali languages; Western Pahari languages]: Sprachfamilie
 - **hin**: (Hindi): hin (ISO 639-3), hi (ISO 639-1); Hindi [Hindi]: Einzelsprache, Lebend
 - **hit**: (Hittite): hit (ISO 639-3); Hethitisch [Hittite]: Einzelsprache, Alt
 - **hmn**: (Hmong; Mong): hmn (ISO 639-3); Hmong-Sprache [Hmong; Mong]: Makrosprache, Lebend
 - **hmo**: (Hiri Motu): hmo (ISO 639-3), ho (ISO 639-1); Hiri Motu [Hiri Motu]: Einzelsprache, Lebend
 - **hrv**: (Croatian): hrv (ISO 639-3), hr (ISO 639-1); Kroatisch [Croatian]: Einzelsprache, Lebend
 - **hsb**: (Upper Sorbian): hsb (ISO 639-3); Obersorbisch [Upper Sorbian]: Einzelsprache, Lebend
 - **hun**: (Hungarian): hun (ISO 639-3), hu (ISO 639-1); Ungarisch [Hungarian]: Einzelsprache, Lebend
 - **hup**: (Hupa): hup (ISO 639-3); Hoopa [Hupa]: Einzelsprache, Lebend
 - **iba**: (Iban): iba (ISO 639-3); Iban [Iban]: Einzelsprache, Lebend
 - **ibo**: (Igbo): ibo (ISO 639-3), ig (ISO 639-1); Igbo [Igbo]: Einzelsprache, Lebend
 - **ido**: (Ido): ido (ISO 639-3), io (ISO 639-1); Ido [Ido]: Einzelsprache, Konstruiert
 - **iii**: (Sichuan Yi; Nuosu): iii (ISO 639-3), ii (ISO 639-1); Yi [Sichuan Yi; Nuosu]: Einzelsprache, Lebend
 - **ijo**: (Ijo languages): ijo (ISO 639-5); Ijo-Sprachen [Ijo languages]: Sprachfamilie
 - **iku**: (Inuktitut): iku (ISO 639-3), iu (ISO 639-1); Inuktitut [Inuktitut]: Makrosprache, Lebend
 - **ile**: (Interlingue; Occidental): ile (ISO 639-3), ie (ISO 639-1); Interlingue [Interlingue; Occidental]: Einzelsprache, Konstruiert
 - **ilo**: (Iloko): ilo (ISO 639-3); Ilokano [Iloko]: Einzelsprache, Lebend
 - **ina**: (Interlingua (International Auxiliary Language Association)): ina (ISO 639-3), ia (ISO 639-1); Interlingua [Interlingua (International Auxiliary Language Association)]: Einzelsprache, Konstruiert
 - **inc**: (Indic languages): inc (ISO 639-5); Indoarische Sprachen [Indic languages]: Sprachfamilie
 - **ind**: (Indonesian): ind (ISO 639-3), id (ISO 639-1); Indonesisch [Indonesian]: Einzelsprache, Lebend
 - **ine**: (Indo-European languages): ine (ISO 639-5); Indogermanische Sprachen [Indo-European languages]: Sprachfamilie
 - **inh**: (Ingush): inh (ISO 639-3); Inguschisch [Ingush]: Einzelsprache, Lebend
 - **ipk**: (Inupiaq): ipk (ISO 639-3), ik (ISO 639-1); Inupiaq [Inupiaq]: Makrosprache, Lebend
 - **ira**: (Iranian languages): ira (ISO 639-5); Iranische Sprachen [Iranian languages]: Sprachfamilie
 - **iro**: (Iroquoian languages): iro (ISO 639-5); Irokesische Sprachen [Iroquoian languages]: Sprachfamilie
 - **ita**: (Italian): ita (ISO 639-3), it (ISO 639-1); Italienisch [Italian]: Einzelsprache, Lebend
 - **jav**: (Javanese): jav (ISO 639-3), jv (ISO 639-1); Javanisch [Javanese]: Einzelsprache, Lebend
 - **jbo**: (Lojban): jbo (ISO 639-3); Lojban [Lojban]: Einzelsprache, Konstruiert
 - **jpn**: (Japanese): jpn (ISO 639-3), ja (ISO 639-1); Japanisch [Japanese]: Einzelsprache, Lebend
 - **jpr**: (Judeo-Persian): jpr (ISO 639-3); Judäo-Persisch [Judeo-Persian]: Einzelsprache, Lebend
 - **jrb**: (Judeo-Arabic): jrb (ISO 639-3); Judäo-Arabisch [Judeo-Arabic]: Makrosprache, Lebend
 - **kaa**: (Kara-Kalpak): kaa (ISO 639-3); Karakalpakisch [Kara-Kalpak]: Einzelsprache, Lebend
 - **kab**: (Kabyle): kab (ISO 639-3); Kabylisch [Kabyle]: Einzelsprache, Lebend
 - **kac**: (Kachin; Jingpho): kac (ISO 639-3); Jingpo [Kachin; Jingpho]: Einzelsprache, Lebend
 - **kal**: (Kalaallisut; Greenlandic): kal (ISO 639-3), kl (ISO 639-1); Grönländisch, Kalaallisut [Kalaallisut; Greenlandic]: Einzelsprache, Lebend
 - **kam**: (Kamba): kam (ISO 639-3); Kikamba [Kamba]: Einzelsprache, Lebend
 - **kan**: (Kannada): kan (ISO 639-3), kn (ISO 639-1); Kannada [Kannada]: Einzelsprache, Lebend
 - **kar**: (Karen languages): kar (ISO 639-5); Karenische Sprachen [Karen languages]: Sprachfamilie
 - **kas**: (Kashmiri): kas (ISO 639-3), ks (ISO 639-1); Kashmiri [Kashmiri]: Einzelsprache, Lebend
 - **kau**: (Kanuri): kau (ISO 639-3), kr (ISO 639-1); Kanuri [Kanuri]: Makrosprache, Lebend
 - **kaw**: (Kawi): kaw (ISO 639-3); Kawi, Altjavanisch [Kawi]: Einzelsprache, Alt
 - **kaz**: (Kazakh): kaz (ISO 639-3), kk (ISO 639-1); Kasachisch [Kazakh]: Einzelsprache, Lebend
 - **kbd**: (Kabardian): kbd (ISO 639-3); Kabardinisch, Ost-Tscherkessisch [Kabardian]: Einzelsprache, Lebend
 - **kha**: (Khasi): kha (ISO 639-3); Khasi [Khasi]: Einzelsprache, Lebend
 - **khi**: (Khoisan languages): khi (ISO 639-5); Khoisansprachen [Khoisan languages]: Sprachfamilie
 - **khm**: (Central Khmer): khm (ISO 639-3), km (ISO 639-1); Khmer [Central Khmer]: Einzelsprache, Lebend
 - **kho**: (Khotanese; Sakan): kho (ISO 639-3); Khotanesisch [Khotanese; Sakan]: Einzelsprache, Alt
 - **kik**: (Kikuyu; Gikuyu): kik (ISO 639-3), ki (ISO 639-1); Kikuyu [Kikuyu; Gikuyu]: Einzelsprache, Lebend
 - **kin**: (Kinyarwanda): kin (ISO 639-3), rw (ISO 639-1); Kinyarwanda, Ruandisch [Kinyarwanda]: Einzelsprache, Lebend
 - **kir**: (Kirghiz; Kyrgyz): kir (ISO 639-3), ky (ISO 639-1); Kirgisisch [Kirghiz; Kyrgyz]: Einzelsprache, Lebend
 - **kmb**: (Kimbundu): kmb (ISO 639-3); Kimbundu [Kimbundu]: Einzelsprache, Lebend
 - **kok**: (Konkani): kok (ISO 639-3); Konkani [Konkani]: Makrosprache, Lebend
 - **kom**: (Komi): kom (ISO 639-3), kv (ISO 639-1); Komi [Komi]: Makrosprache, Lebend
 - **kon**: (Kongo): kon (ISO 639-3), kg (ISO 639-1); Kikongo [Kongo]: Makrosprache, Lebend
 - **kor**: (Korean): kor (ISO 639-3), ko (ISO 639-1); Koreanisch [Korean]: Einzelsprache, Lebend
 - **kos**: (Kosraean): kos (ISO 639-3); Kosraeanisch [Kosraean]: Einzelsprache, Lebend
 - **kpe**: (Kpelle): kpe (ISO 639-3); Kpelle [Kpelle]: Makrosprache, Lebend
 - **krc**: (Karachay-Balkar): krc (ISO 639-3); Karatschai-balkarisch [Karachay-Balkar]: Einzelsprache, Lebend
 - **krl**: (Karelian): krl (ISO 639-3); Karelisch [Karelian]: Einzelsprache, Lebend
 - **kro**: (Kru languages): kro (ISO 639-5); Kru-Sprachen [Kru languages]: Sprachfamilie
 - **kru**: (Kurukh): kru (ISO 639-3); Kurukh [Kurukh]: Einzelsprache, Lebend
 - **kua**: (Kuanyama; Kwanyama): kua (ISO 639-3), kj (ISO 639-1); oshiKwanyama [Kuanyama; Kwanyama]: Einzelsprache, Lebend
 - **kum**: (Kumyk): kum (ISO 639-3); Kumykisch [Kumyk]: Einzelsprache, Lebend
 - **kur**: (Kurdish): kur (ISO 639-3), ku (ISO 639-1); Kurdisch [Kurdish]: Makrosprache, Lebend
 - **kut**: (Kutenai): kut (ISO 639-3); Kutanaha [Kutenai]: Einzelsprache, Lebend
 - **lad**: (Ladino): lad (ISO 639-3); Judenspanisch, Ladino, Sephardisch [Ladino]: Einzelsprache, Lebend
 - **lah**: (Lahnda): lah (ISO 639-3); Lahnda, Westpanjabi [Lahnda]: Makrosprache, Lebend
 - **lam**: (Lamba): lam (ISO 639-3); Lamba [Lamba]: Einzelsprache, Lebend
 - **lao**: (Lao): lao (ISO 639-3), lo (ISO 639-1); Laotisch [Lao]: Einzelsprache, Lebend
 - **lat**: (Latin): lat (ISO 639-3), la (ISO 639-1); Latein [Latin]: Einzelsprache, Alt
 - **lav**: (Latvian): lav (ISO 639-3), lv (ISO 639-1); Lettisch [Latvian]: Makrosprache, Lebend
 - **lez**: (Lezghian): lez (ISO 639-3); Lesgisch [Lezghian]: Einzelsprache, Lebend
 - **lim**: (Limburgan; Limburger; Limburgish): lim (ISO 639-3), li (ISO 639-1); Limburgisch, Südniederfränkisch [Limburgan; Limburger; Limburgish]: Einzelsprache, Lebend
 - **lin**: (Lingala): lin (ISO 639-3), ln (ISO 639-1); Lingála [Lingala]: Einzelsprache, Lebend
 - **lit**: (Lithuanian): lit (ISO 639-3), lt (ISO 639-1); Litauisch [Lithuanian]: Einzelsprache, Lebend
 - **lol**: (Mongo): lol (ISO 639-3); Lomongo [Mongo]: Einzelsprache, Lebend
 - **loz**: (Lozi): loz (ISO 639-3); Lozi [Lozi]: Einzelsprache, Lebend
 - **ltz**: (Luxembourgish; Letzeburgesch): ltz (ISO 639-3), lb (ISO 639-1); Luxemburgisch [Luxembourgish; Letzeburgesch]: Einzelsprache, Lebend
 - **lua**: (Luba-Lulua): lua (ISO 639-3); Tschiluba [Luba-Lulua]: Einzelsprache, Lebend
 - **lub**: (Luba-Katanga): lub (ISO 639-3), lu (ISO 639-1); Kiluba [Luba-Katanga]: Einzelsprache, Lebend
 - **lug**: (Ganda): lug (ISO 639-3), lg (ISO 639-1); Luganda [Ganda]: Einzelsprache, Lebend
 - **lui**: (Luiseno): lui (ISO 639-3); Luiseño [Luiseno]: Einzelsprache, Ausgestorben
 - **lun**: (Lunda): lun (ISO 639-3); Chilunda [Lunda]: Einzelsprache, Lebend
 - **luo**: (Luo (Kenya and Tanzania)): luo (ISO 639-3); Luo [Luo (Kenya and Tanzania)]: Einzelsprache, Lebend
 - **lus**: (Lushai): lus (ISO 639-3); Mizo, Lushai [Lushai]: Einzelsprache, Lebend
 - **mad**: (Madurese): mad (ISO 639-3); Maduresisch [Madurese]: Einzelsprache, Lebend
 - **mag**: (Magahi): mag (ISO 639-3); Magadhi [Magahi]: Einzelsprache, Lebend
 - **mah**: (Marshallese): mah (ISO 639-3), mh (ISO 639-1); Marshallesisch [Marshallese]: Einzelsprache, Lebend
 - **mai**: (Maithili): mai (ISO 639-3); Maithili [Maithili]: Einzelsprache, Lebend
 - **mak**: (Makasar): mak (ISO 639-3); Makassar [Makasar]: Einzelsprache, Lebend
 - **mal**: (Malayalam): mal (ISO 639-3), ml (ISO 639-1); Malayalam [Malayalam]: Einzelsprache, Lebend
 - **man**: (Mandingo): man (ISO 639-3); Manding [Mandingo]: Makrosprache, Lebend
 - **map**: (Austronesian languages): map (ISO 639-5); Austronesische Sprachen [Austronesian languages]: Sprachfamilie
 - **mar**: (Marathi): mar (ISO 639-3), mr (ISO 639-1); Marathi [Marathi]: Einzelsprache, Lebend
 - **mas**: (Masai): mas (ISO 639-3); Maa, Kimaasai [Masai]: Einzelsprache, Lebend
 - **mdf**: (Moksha): mdf (ISO 639-3); Mokschanisch [Moksha]: Einzelsprache, Lebend
 - **mdr**: (Mandar): mdr (ISO 639-3); Mandar [Mandar]: Einzelsprache, Lebend
 - **men**: (Mende): men (ISO 639-3); Mende [Mende]: Einzelsprache, Lebend
 - **mga**: (Irish, Middle (900–1200)): mga (ISO 639-3); Mittelirisch [Irish, Middle (900–1200)]: Einzelsprache, Historisch
 - **mic**: (Mi'kmaq; Micmac): mic (ISO 639-3); Míkmawísimk [Mi'kmaq; Micmac]: Einzelsprache, Lebend
 - **min**: (Minangkabau): min (ISO 639-3); Minangkabauisch [Minangkabau]: Einzelsprache, Lebend
 - **mis**: (Uncoded languages): mis (ISO 639-3); „Unkodiert“ [Uncoded languages]: Speziell
 - **mkh**: (Mon-Khmer languages): mkh (ISO 639-5); Mon-Khmer-Sprachen [Mon-Khmer languages]: Sprachfamilie
 - **mlg**: (Malagasy): mlg (ISO 639-3), mg (ISO 639-1); Malagasy, Malagassi [Malagasy]: Makrosprache, Lebend
 - **mlt**: (Maltese): mlt (ISO 639-3), mt (ISO 639-1); Maltesisch [Maltese]: Einzelsprache, Lebend
 - **mnc**: (Manchu): mnc (ISO 639-3); Mandschurisch [Manchu]: Einzelsprache, Lebend
 - **mni**: (Manipuri): mni (ISO 639-3); Meitei [Manipuri]: Einzelsprache, Lebend
 - **mno**: (Manobo languages): mno (ISO 639-5); Manobo-Sprachen [Manobo languages]: Sprachfamilie
 - **moh**: (Mohawk): moh (ISO 639-3); Mohawk [Mohawk]: Einzelsprache, Lebend
 - **mon**: (Mongolian): mon (ISO 639-3), mn (ISO 639-1); Mongolisch [Mongolian]: Makrosprache, Lebend
 - **mos**: (Mossi): mos (ISO 639-3); Mòoré [Mossi]: Einzelsprache, Lebend
 - **mul**: (Multiple languages): mul (ISO 639-3); „Mehrsprachig“ [Multiple languages]: Speziell
 - **mun**: (Munda languages): mun (ISO 639-5); Munda-Sprachen [Munda languages]: Sprachfamilie
 - **mus**: (Creek): mus (ISO 639-3); Muskogee-Sprachen [Creek]: Sprachfamilie
 - **mwl**: (Mirandese): mwl (ISO 639-3); Mirandés [Mirandese]: Einzelsprache, Lebend
 - **mwr**: (Marwari): mwr (ISO 639-3); Marwari [Marwari]: Makrosprache, Lebend
 - **myn**: (Mayan languages): myn (ISO 639-5); Maya-Sprachen [Mayan languages]: Sprachfamilie
 - **myv**: (Erzya): myv (ISO 639-3); Ersjanisch, Ersja-Mordwinisch [Erzya]: Einzelsprache, Lebend
 - **nah**: (Nahuatl languages): nah (ISO 639-5); Nahuatl [Nahuatl languages]: Sprachfamilie
 - **nai**: (North American Indian languages): nai (ISO 639-5); Nordamerikanische Sprachen [North American Indian languages]: Sprachfamilie
 - **nap**: (Neapolitan): nap (ISO 639-3); Neapolitanisch [Neapolitan]: Einzelsprache, Lebend
 - **nau**: (Nauru): nau (ISO 639-3), na (ISO 639-1); Nauruisch [Nauru]: Einzelsprache, Lebend
 - **nav**: (Navajo; Navaho): nav (ISO 639-3), nv (ISO 639-1); Navajo [Navajo; Navaho]: Einzelsprache, Lebend
 - **nbl**: (Ndebele, South; South Ndebele): nbl (ISO 639-3), nr (ISO 639-1); Süd-Ndebele [Ndebele, South; South Ndebele]: Einzelsprache, Lebend
 - **nde**: (Ndebele, North; North Ndebele): nde (ISO 639-3), nd (ISO 639-1); Nord-Ndebele [Ndebele, North; North Ndebele]: Einzelsprache, Lebend
 - **ndo**: (Ndonga): ndo (ISO 639-3), ng (ISO 639-1); Ndonga [Ndonga]: Einzelsprache, Lebend
 - **nds**: (Low German; Low Saxon; German, Low; Saxon, Low): nds (ISO 639-3); Niederdeutsch, Plattdeutsch [Low German; Low Saxon; German, Low; Saxon, Low]: Einzelsprache, Lebend
 - **nep**: (Nepali): nep (ISO 639-3), ne (ISO 639-1); Nepali [Nepali]: Makrosprache, Lebend
 - **new**: (Nepal Bhasa; Newari): new (ISO 639-3); Newari [Nepal Bhasa; Newari]: Einzelsprache, Lebend
 - **nia**: (Nias): nia (ISO 639-3); Nias [Nias]: Einzelsprache, Lebend
 - **nic**: (Niger-Kordofanian languages): nic (ISO 639-5); Niger-Kongo-Sprachen [Niger-Kordofanian languages]: Sprachfamilie
 - **niu**: (Niuean): niu (ISO 639-3); Niueanisch [Niuean]: Einzelsprache, Lebend
 - **nno**: (Norwegian Nynorsk; Nynorsk, Norwegian): nno (ISO 639-3), nn (ISO 639-1); Nynorsk [Norwegian Nynorsk; Nynorsk, Norwegian]: Einzelsprache, Lebend
 - **nob**: (Bokmål, Norwegian; Norwegian Bokmål): nob (ISO 639-3), nb (ISO 639-1); Bokmål [Bokmål, Norwegian; Norwegian Bokmål]: Einzelsprache, Lebend
 - **nog**: (Nogai): nog (ISO 639-3); Nogaisch [Nogai]: Einzelsprache, Lebend
 - **non**: (Norse, Old): non (ISO 639-3); Altnordisch [Norse, Old]: Einzelsprache, Historisch
 - **nor**: (Norwegian): nor (ISO 639-3), no (ISO 639-1); Norwegisch [Norwegian]: Makrosprache, Lebend
 - **nqo**: (N'Ko): nqo (ISO 639-3); N’Ko [N'Ko]: Einzelsprache, Lebend
 - **nso**: (Pedi; Sepedi; Northern Sotho): nso (ISO 639-3); Nord-Sotho [Pedi; Sepedi; Northern Sotho]: Einzelsprache, Lebend
 - **nub**: (Nubian languages): nub (ISO 639-5); Nubische Sprachen [Nubian languages]: Sprachfamilie
 - **nwc**: (Classical Newari; Old Newari; Classical Nepal Bhasa): nwc (ISO 639-3); Klassisches Newari [Classical Newari; Old Newari; Classical Nepal Bhasa]: Einzelsprache, Historisch
 - **nya**: (Chichewa; Chewa; Nyanja): nya (ISO 639-3), ny (ISO 639-1); Chichewa [Chichewa; Chewa; Nyanja]: Einzelsprache, Lebend
 - **nym**: (Nyamwezi): nym (ISO 639-3); Nyamwesi [Nyamwezi]: Einzelsprache, Lebend
 - **nyn**: (Nyankole): nyn (ISO 639-3); Runyankole, Runyankore [Nyankole]: Einzelsprache, Lebend
 - **nyo**: (Nyoro): nyo (ISO 639-3); Runyoro [Nyoro]: Einzelsprache, Lebend
 - **nzi**: (Nzima): nzi (ISO 639-3); Nzema [Nzima]: Einzelsprache, Lebend
 - **oci**: (Occitan (ab 1500); Provençal): oci (ISO 639-3), oc (ISO 639-1); Okzitanisch [Occitan (ab 1500); Provençal]: Einzelsprache, Lebend
 - **oji**: (Ojibwa): oji (ISO 639-3), oj (ISO 639-1); Ojibwe [Ojibwa]: Makrosprache, Lebend
 - **ori**: (Oriya): ori (ISO 639-3), or (ISO 639-1); Oriya [Oriya]: Makrosprache, Lebend
 - **orm**: (Oromo): orm (ISO 639-3), om (ISO 639-1); Oromo [Oromo]: Makrosprache, Lebend
 - **osa**: (Osage): osa (ISO 639-3); Osage [Osage]: Einzelsprache, Lebend
 - **oss**: (Ossetian; Ossetic): oss (ISO 639-3), os (ISO 639-1); Ossetisch [Ossetian; Ossetic]: Einzelsprache, Lebend
 - **ota**: (Turkish, Ottoman (1500–1928)): ota (ISO 639-3); Osmanisch, osmanisches Türkisch [Turkish, Ottoman (1500–1928)]: Einzelsprache, Historisch
 - **oto**: (Otomian languages): oto (ISO 639-5); Oto-Pame-Sprachen [Otomian languages]: Sprachfamilie
 - **paa**: (Papuan languages): paa (ISO 639-5); Papuasprachen [Papuan languages]: Sprachfamilie
 - **pag**: (Pangasinan): pag (ISO 639-3); Pangasinensisch [Pangasinan]: Einzelsprache, Lebend
 - **pal**: (Pahlavi): pal (ISO 639-3); Mittelpersisch, Pahlavi [Pahlavi]: Einzelsprache, Alt
 - **pam**: (Pampanga; Kapampangan): pam (ISO 639-3); Kapampangan [Pampanga; Kapampangan]: Einzelsprache, Lebend
 - **pan**: (Panjabi; Punjabi): pan (ISO 639-3), pa (ISO 639-1); Panjabi, Pandschabi [Panjabi; Punjabi]: Einzelsprache, Lebend
 - **pap**: (Papiamento): pap (ISO 639-3); Papiamentu [Papiamento]: Einzelsprache, Lebend
 - **pau**: (Palauan): pau (ISO 639-3); Palauisch [Palauan]: Einzelsprache, Lebend
 - **peo**: (Persian, Old (ca. 600–400 v. Chr.)): peo (ISO 639-3); Altpersisch [Persian, Old (ca. 600–400 v. Chr.)]: Einzelsprache, Historisch
 - **phi**: (Philippine languages): phi (ISO 639-5); Philippinische Sprachen [Philippine languages]: Sprachfamilie
 - **phn**: (Phoenician): phn (ISO 639-3); Phönizisch, Punisch [Phoenician]: Einzelsprache, Alt
 - **pli**: (Pali): pli (ISO 639-3), pi (ISO 639-1); Pali [Pali]: Einzelsprache, Alt
 - **pol**: (Polish): pol (ISO 639-3), pl (ISO 639-1); Polnisch [Polish]: Einzelsprache, Lebend
 - **pon**: (Pohnpeian): pon (ISO 639-3); Pohnpeanisch [Pohnpeian]: Einzelsprache, Lebend
 - **por**: (Portuguese): por (ISO 639-3), pt (ISO 639-1); Portugiesisch [Portuguese]: Einzelsprache, Lebend
 - **pra**: (Prakrit languages): pra (ISO 639-5); Prakrit [Prakrit languages]: Sprachfamilie
 - **pro**: (Provençal, Old (bis 1500); Old Occitan (bis 1500)): pro (ISO 639-3); Altokzitanisch, Altprovenzalisch [Provençal, Old (bis 1500); Old Occitan (bis 1500)]: Einzelsprache, Historisch
 - **pus**: (Pushto; Pashto): pus (ISO 639-3), ps (ISO 639-1); Paschtunisch [Pushto; Pashto]: Makrosprache, Lebend
 - **qaa-qtz**: Reserviert für lokale Nutzung, Reserved for local use
 - **que**: (Quechua): que (ISO 639-3), qwe (ISO 639-3), qu (ISO 639-1); Quechua [Quechua]: Makrosprache, Lebend
 - **raj**: (Rajasthani): raj (ISO 639-3); Rajasthani [Rajasthani]: Makrosprache, Lebend
 - **rap**: (Rapanui): rap (ISO 639-3); Rapanui [Rapanui]: Einzelsprache, Lebend
 - **rar**: (Rarotongan; Cook Islands Maori): rar (ISO 639-3); Rarotonganisch, Māori der Cookinseln [Rarotongan; Cook Islands Maori]: Einzelsprache, Lebend
 - **roa**: (Romance languages): roa (ISO 639-5); Romanische Sprachen [Romance languages]: Sprachfamilie
 - **roh**: (Romansh): roh (ISO 639-3), rm (ISO 639-1); Bündnerromanisch, Romanisch [Romansh]: Einzelsprache, Lebend
 - **rom**: (Romany): rom (ISO 639-3); Romani, Romanes [Romany]: Makrosprache, Lebend
 - **run**: (Rundi): run (ISO 639-3), rn (ISO 639-1); Kirundi [Rundi]: Einzelsprache, Lebend
 - **rup**: (Aromanian; Arumanian; Macedo-Romanian): rup (ISO 639-3); Aromunisch, Makedoromanisch [Aromanian; Arumanian; Macedo-Romanian]: Einzelsprache, Lebend
 - **rus**: (Russian): rus (ISO 639-3), ru (ISO 639-1); Russisch [Russian]: Einzelsprache, Lebend
 - **sad**: (Sandawe): sad (ISO 639-3); Sandawe [Sandawe]: Einzelsprache, Lebend
 - **sag**: (Sango): sag (ISO 639-3), sg (ISO 639-1); Sango [Sango]: Einzelsprache, Lebend
 - **sah**: (Yakut): sah (ISO 639-3); Jakutisch [Yakut]: Einzelsprache, Lebend
 - **sai**: (South American Indian (Other)): sai (ISO 639-5); Südamerikanische Sprachen [South American Indian (Other)]: Sprachfamilie
 - **sal**: (Salishan languages): sal (ISO 639-5); Salish-Sprachen [Salishan languages]: Sprachfamilie
 - **sam**: (Samaritan Aramaic): sam (ISO 639-3); Samaritanisch [Samaritan Aramaic]: Einzelsprache, Ausgestorben
 - **san**: (Sanskrit): san (ISO 639-3), sa (ISO 639-1); Sanskrit [Sanskrit]: Einzelsprache, Alt
 - **sas**: (Sasak): sas (ISO 639-3); Sasak [Sasak]: Einzelsprache, Lebend
 - **sat**: (Santali): sat (ISO 639-3); Santali [Santali]: Einzelsprache, Lebend
 - **scn**: (Sicilian): scn (ISO 639-3); Sizilianisch [Sicilian]: Einzelsprache, Lebend
 - **sco**: (Scots): sco (ISO 639-3); Scots [Scots]: Einzelsprache, Lebend
 - **sel**: (Selkup): sel (ISO 639-3); Selkupisch [Selkup]: Einzelsprache, Lebend
 - **sem**: (Semitic languages): sem (ISO 639-5); Semitische Sprachen [Semitic languages]: Sprachfamilie
 - **sga**: (Irish, Old (bis 900)): sga (ISO 639-3); Altirisch [Irish, Old (bis 900)]: Einzelsprache, Historisch
 - **sgn**: (Sign Languages): sgn (ISO 639-5); Gebärdensprache [Sign Languages]: Sprachfamilie
 - **shn**: (Shan): shn (ISO 639-3); Shan [Shan]: Einzelsprache, Lebend
 - **sid**: (Sidamo): sid (ISO 639-3); Sidama [Sidamo]: Einzelsprache, Lebend
 - **sin**: (Sinhala; Sinhalese): sin (ISO 639-3), si (ISO 639-1); Singhalesisch [Sinhala; Sinhalese]: Einzelsprache, Lebend
 - **sio**: (Siouan languages): sio (ISO 639-5); Sioux-Sprachen [Siouan languages]: Sprachfamilie
 - **sit**: (Sino-Tibetan languages): sit (ISO 639-5); Sinotibetische Sprachen [Sino-Tibetan languages]: Sprachfamilie
 - **sla**: (Slavic languages): sla (ISO 639-5); Slawische Sprachen [Slavic languages]: Sprachfamilie
 - **slv**: (Slovenian): slv (ISO 639-3), sl (ISO 639-1); Slowenisch [Slovenian]: Einzelsprache, Lebend
 - **sma**: (Southern Sami): sma (ISO 639-3); Südsamisch [Southern Sami]: Einzelsprache, Lebend
 - **sme**: (Northern Sami): sme (ISO 639-3), se (ISO 639-1); Nordsamisch [Northern Sami]: Einzelsprache, Lebend
 - **smi**: (Sami languages): smi (ISO 639-5); Samische Sprachen [Sami languages]: Sprachfamilie
 - **smj**: (Lule Sami): smj (ISO 639-3); Lulesamisch [Lule Sami]: Einzelsprache, Lebend
 - **smn**: (Inari Sami): smn (ISO 639-3); Inarisamisch [Inari Sami]: Einzelsprache, Lebend
 - **smo**: (Samoan): smo (ISO 639-3), sm (ISO 639-1); Samoanisch [Samoan]: Einzelsprache, Lebend
 - **sms**: (Skolt Sami): sms (ISO 639-3); Skoltsamisch [Skolt Sami]: Einzelsprache, Lebend
 - **sna**: (Shona): sna (ISO 639-3), sn (ISO 639-1); Shona [Shona]: Einzelsprache, Lebend
 - **snd**: (Sindhi): snd (ISO 639-3), sd (ISO 639-1); Sindhi [Sindhi]: Einzelsprache, Lebend
 - **snk**: (Soninke): snk (ISO 639-3); Soninke [Soninke]: Einzelsprache, Lebend
 - **sog**: (Sogdian): sog (ISO 639-3); Sogdisch [Sogdian]: Einzelsprache, Alt
 - **som**: (Somali): som (ISO 639-3), so (ISO 639-1); Somali [Somali]: Einzelsprache, Lebend
 - **son**: (Songhai languages): son (ISO 639-5); Songhai-Sprachen [Songhai languages]: Sprachfamilie
 - **sot**: (Sotho, Southern): sot (ISO 639-3), st (ISO 639-1); Sesotho, Süd-Sotho [Sotho, Southern]: Einzelsprache, Lebend
 - **spa**: (Spanish; Castilian): spa (ISO 639-3), es (ISO 639-1); Spanisch, Kastilisch [Spanish; Castilian]: Einzelsprache, Lebend
 - **srd**: (Sardinian): srd (ISO 639-3), sc (ISO 639-1); Sardisch [Sardinian]: Makrosprache, Lebend
 - **srn**: (Sranan Tongo): srn (ISO 639-3); Sranantongo [Sranan Tongo]: Einzelsprache, Lebend
 - **srp**: (Serbian): srp (ISO 639-3), sr (ISO 639-1); Serbisch [Serbian]: Einzelsprache, Lebend
 - **srr**: (Serer): srr (ISO 639-3); Serer [Serer]: Einzelsprache, Lebend
 - **ssa**: (Nilo-Saharan languages): ssa (ISO 639-5); Nilosaharanische Sprachen [Nilo-Saharan languages]: Sprachfamilie
 - **ssw**: (Swati): ssw (ISO 639-3), ss (ISO 639-1); Siswati [Swati]: Einzelsprache, Lebend
 - **suk**: (Sukuma): suk (ISO 639-3); Sukuma [Sukuma]: Einzelsprache, Lebend
 - **sun**: (Sundanese): sun (ISO 639-3), su (ISO 639-1); Sundanesisch [Sundanese]: Einzelsprache, Lebend
 - **sus**: (Susu): sus (ISO 639-3); Susu [Susu]: Einzelsprache, Lebend
 - **sux**: (Sumerian): sux (ISO 639-3); Sumerisch [Sumerian]: Einzelsprache, Alt
 - **swa**: (Swahili): swa (ISO 639-3), sw (ISO 639-1); Swahili [Swahili]: Makrosprache, Lebend
 - **swe**: (Swedish): swe (ISO 639-3), sv (ISO 639-1); Schwedisch [Swedish]: Einzelsprache, Lebend
 - **syc**: (Classical Syriac): syc (ISO 639-3); Syrisch [Classical Syriac]: Einzelsprache, Historisch
 - **syr**: (Syriac): syr (ISO 639-3); Nordost-Neuaramäisch [Syriac]: Makrosprache, Lebend
 - **tah**: (Tahitian): tah (ISO 639-3), ty (ISO 639-1); Tahitianisch, Tahitisch [Tahitian]: Einzelsprache, Lebend
 - **tai**: (Tai languages): tai (ISO 639-5); Tai-Sprachen [Tai languages]: Sprachfamilie
 - **tam**: (Tamil): tam (ISO 639-3), ta (ISO 639-1); Tamil [Tamil]: Einzelsprache, Lebend
 - **tat**: (Tatar): tat (ISO 639-3), tt (ISO 639-1); Tatarisch [Tatar]: Einzelsprache, Lebend
 - **tel**: (Telugu): tel (ISO 639-3), te (ISO 639-1); Telugu [Telugu]: Einzelsprache, Lebend
 - **tem**: (Timne): tem (ISO 639-3); Temne [Timne]: Einzelsprache, Lebend
 - **ter**: (Tereno): ter (ISO 639-3); Terena [Tereno]: Einzelsprache, Lebend
 - **tet**: (Tetum): tet (ISO 639-3); Tetum [Tetum]: Einzelsprache, Lebend
 - **tgk**: (Tajik): tgk (ISO 639-3), tg (ISO 639-1); Tadschikisch [Tajik]: Einzelsprache, Lebend
 - **tgl**: (Tagalog): tgl (ISO 639-3), tl (ISO 639-1); Tagalog [Tagalog]: Einzelsprache, Lebend
 - **tha**: (Thai): tha (ISO 639-3), th (ISO 639-1); Thai [Thai]: Einzelsprache, Lebend
 - **tig**: (Tigre): tig (ISO 639-3); Tigre [Tigre]: Einzelsprache, Lebend
 - **tir**: (Tigrinya): tir (ISO 639-3), ti (ISO 639-1); Tigrinya [Tigrinya]: Einzelsprache, Lebend
 - **tiv**: (Tiv): tiv (ISO 639-3); Tiv [Tiv]: Einzelsprache, Lebend
 - **tkl**: (Tokelau): tkl (ISO 639-3); Tokelauisch [Tokelau]: Einzelsprache, Lebend
 - **tlh**: (Klingon; tlhIngan-Hol): tlh (ISO 639-3); Klingonisch [Klingon; tlhIngan-Hol]: Einzelsprache, Konstruiert
 - **tli**: (Tlingit): tli (ISO 639-3); Tlingit [Tlingit]: Einzelsprache, Lebend
 - **tmh**: (Tamashek): tmh (ISO 639-3); Tuareg [Tamashek]: Makrosprache, Lebend
 - **tog**: (Tonga (Nyasa)): tog (ISO 639-3); ChiTonga [Tonga (Nyasa)]: Einzelsprache, Lebend
 - **ton**: (Tonga (Tonga Islands)): ton (ISO 639-3), to (ISO 639-1); Tongaisch [Tonga (Tonga Islands)]: Einzelsprache, Lebend
 - **tpi**: (Tok Pisin): tpi (ISO 639-3); Tok Pisin, Neuguinea-Pidgin [Tok Pisin]: Einzelsprache, Lebend
 - **tsi**: (Tsimshian): tsi (ISO 639-3); Tsimshian [Tsimshian]: Einzelsprache, Lebend
 - **tsn**: (Tswana): tsn (ISO 639-3), tn (ISO 639-1); Setswana [Tswana]: Einzelsprache, Lebend
 - **tso**: (Tsonga): tso (ISO 639-3), ts (ISO 639-1); Xitsonga [Tsonga]: Einzelsprache, Lebend
 - **tuk**: (Turkmen): tuk (ISO 639-3), tk (ISO 639-1); Turkmenisch [Turkmen]: Einzelsprache, Lebend
 - **tum**: (Tumbuka): tum (ISO 639-3); Tumbuka [Tumbuka]: Einzelsprache, Lebend
 - **tup**: (Tupi languages): tup (ISO 639-5); Tupí-Sprachen [Tupi languages]: Sprachfamilie
 - **tur**: (Turkish): tur (ISO 639-3), tr (ISO 639-1); Türkisch [Turkish]: Einzelsprache, Lebend
 - **tut**: (Altaic languages): tut (ISO 639-5); Altaische Sprachen [Altaic languages]: Sprachfamilie
 - **tvl**: (Tuvalu): tvl (ISO 639-3); Tuvaluisch [Tuvalu]: Einzelsprache, Lebend
 - **twi**: (Twi): twi (ISO 639-3), tw (ISO 639-1); Twi [Twi]: Einzelsprache, Lebend
 - **tyv**: (Tuvinian): tyv (ISO 639-3); Tuwinisch [Tuvinian]: Einzelsprache, Lebend
 - **udm**: (Udmurt): udm (ISO 639-3); Udmurtisch [Udmurt]: Einzelsprache, Lebend
 - **uga**: (Ugaritic): uga (ISO 639-3); Ugaritisch [Ugaritic]: Einzelsprache, Alt
 - **uig**: (Uighur; Uyghur): uig (ISO 639-3), ug (ISO 639-1); Uigurisch [Uighur; Uyghur]: Einzelsprache, Lebend
 - **ukr**: (Ukrainian): ukr (ISO 639-3), uk (ISO 639-1); Ukrainisch [Ukrainian]: Einzelsprache, Lebend
 - **umb**: (Umbundu): umb (ISO 639-3); Umbundu [Umbundu]: Einzelsprache, Lebend
 - **und**: (Undetermined): und (ISO 639-3); „Undefiniert“ [Undetermined]: Speziell
 - **urd**: (Urdu): urd (ISO 639-3), ur (ISO 639-1); Urdu [Urdu]: Einzelsprache, Lebend
 - **uzb**: (Uzbek): uzb (ISO 639-3), uz (ISO 639-1); Usbekisch [Uzbek]: Makrosprache, Lebend
 - **vai**: (Vai): vai (ISO 639-3); Vai [Vai]: Einzelsprache, Lebend
 - **ven**: (Venda): ven (ISO 639-3), ve (ISO 639-1); Tshivenda [Venda]: Einzelsprache, Lebend
 - **vie**: (Vietnamese): vie (ISO 639-3), vi (ISO 639-1); Vietnamesisch [Vietnamese]: Einzelsprache, Lebend
 - **vol**: (Volapük): vol (ISO 639-3), vo (ISO 639-1); Volapük [Volapük]: Einzelsprache, Konstruiert
 - **vot**: (Votic): vot (ISO 639-3); Wotisch [Votic]: Einzelsprache, Lebend
 - **wak**: (Wakashan languages): wak (ISO 639-5); Wakash-Sprachen [Wakashan languages]: Sprachfamilie
 - **wal**: (Walamo): wal (ISO 639-3); Wolaytta [Walamo]: Einzelsprache, Lebend
 - **war**: (Waray): war (ISO 639-3); Wáray-Wáray [Waray]: Einzelsprache, Lebend
 - **was**: (Washo): was (ISO 639-3); Washoe [Washo]: Einzelsprache, Lebend
 - **wen**: (Sorbian languages): wen (ISO 639-5); Sorbische Sprache [Sorbian languages]: Sprachfamilie
 - **wln**: (Walloon): wln (ISO 639-3), wa (ISO 639-1); Wallonisch [Walloon]: Einzelsprache, Lebend
 - **wol**: (Wolof): wol (ISO 639-3), wo (ISO 639-1); Wolof [Wolof]: Einzelsprache, Lebend
 - **xal**: (Kalmyk; Oirat): xal (ISO 639-3); Kalmückisch [Kalmyk; Oirat]: Einzelsprache, Lebend
 - **xho**: (Xhosa): xho (ISO 639-3), xh (ISO 639-1); isiXhosa [Xhosa]: Einzelsprache, Lebend
 - **yao**: (Yao): yao (ISO 639-3); Yao [Yao]: Einzelsprache, Lebend
 - **yap**: (Yapese): yap (ISO 639-3); Yapesisch [Yapese]: Einzelsprache, Lebend
 - **yid**: (Yiddish): yid (ISO 639-3), yi (ISO 639-1); Jiddisch [Yiddish]: Makrosprache, Lebend
 - **yor**: (Yoruba): yor (ISO 639-3), yo (ISO 639-1); Yoruba [Yoruba]: Einzelsprache, Lebend
 - **ypk**: (Yupik languages): ypk (ISO 639-5); Yupik-Sprachen [Yupik languages]: Sprachfamilie
 - **zap**: (Zapotec): zap (ISO 639-3); Zapotekisch [Zapotec]: Makrosprache, Lebend
 - **zbl**: (Blissymbols; Blissymbolics; Bliss): zbl (ISO 639-3); Bliss-Symbol [Blissymbols; Blissymbolics; Bliss]: Einzelsprache, Konstruiert
 - **zen**: (Zenaga): zen (ISO 639-3); Zenaga [Zenaga]: Einzelsprache, Lebend
 - **zgh**: (Standard Moroccan Tamazight): zgh (ISO 639-3); Marokkanisches Tamazight [Standard Moroccan Tamazight]: Einzelsprache, Lebend
 - **zha**: (Zhuang; Chuang): zha (ISO 639-3), za (ISO 639-1); Zhuang [Zhuang; Chuang]: Makrosprache, Lebend
 - **znd**: (Zande languages): znd (ISO 639-5); Zande-Sprachen [Zande languages]: Sprachfamilie
 - **zul**: (Zulu): zul (ISO 639-3), zu (ISO 639-1); isiZulu [Zulu]: Einzelsprache, Lebend
 - **zun**: (Zuni): zun (ISO 639-3); Zuñi [Zuni]: Einzelsprache, Lebend
 - **zxx**: (No linguistic content; Not applicable): zxx (ISO 639-3); „Kein sprachlicher Inhalt; Nicht anwendbar“ [No linguistic content; Not applicable]: Speziell
 - **zza**: (Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki): zza (ISO 639-3); Zazaisch [Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki]: Makrosprache, Lebend
 - **alb**: (Albanian): alb (ISO 639-2/B), sqi (ISO 639-3), sq (ISO 639-1); Albanisch [Albanian]: Makrosprache, Lebend
 - **arm**: (Armenian): arm (ISO 639-2/B), hye (ISO 639-3), hy (ISO 639-1); Armenisch [Armenian]: Einzelsprache, Lebend
 - **baq**: (Basque): baq (ISO 639-2/B), eus (ISO 639-3), eu (ISO 639-1); Baskisch [Basque]: Einzelsprache, Lebend
 - **bur**: (Burmese): bur (ISO 639-2/B), mya (ISO 639-3), my (ISO 639-1); Birmanisch [Burmese]: Einzelsprache, Lebend
 - **chi**: (Chinese): chi (ISO 639-2/B), zho (ISO 639-3), zh (ISO 639-1); Chinesisch [Chinese]: Makrosprache, Lebend
 - **cze**: (Czech): cze (ISO 639-2/B), ces (ISO 639-3), cs (ISO 639-1); Tschechisch [Czech]: Einzelsprache, Lebend
 - **dut**: (Dutch; Flemish): dut (ISO 639-2/B), nld (ISO 639-3), nl (ISO 639-1); Niederländisch, Belgisches Niederländisch [Dutch; Flemish]: Einzelsprache, Lebend
 - **fre**: (French): fre (ISO 639-2/B), fra (ISO 639-3), fr (ISO 639-1); Französisch [French]: Einzelsprache, Lebend
 - **geo**: (Georgian): geo (ISO 639-2/B), kat (ISO 639-3), ka (ISO 639-1); Georgisch [Georgian]: Einzelsprache, Lebend
 - **ger**: (German): ger (ISO 639-2/B), deu (ISO 639-3), de (ISO 639-1); Deutsch [German]: Einzelsprache, Lebend
 - **gre**: (Greek, Modern (ab 1453)): gre (ISO 639-2/B), ell (ISO 639-3), el (ISO 639-1); Griechisch [Greek, Modern (ab 1453)]: Einzelsprache, Lebend
 - **ice**: (Icelandic): ice (ISO 639-2/B), isl (ISO 639-3), is (ISO 639-1); Isländisch [Icelandic]: Einzelsprache, Lebend
 - **mac**: (Macedonian): mac (ISO 639-2/B), mkd (ISO 639-3), mk (ISO 639-1); Mazedonisch [Macedonian]: Einzelsprache, Lebend
 - **mao**: (Maori): mao (ISO 639-2/B), mri (ISO 639-3), mi (ISO 639-1); Maori [Maori]: Einzelsprache, Lebend
 - **may**: (Malay): may (ISO 639-2/B), msa (ISO 639-3), ms (ISO 639-1); Malaiisch [Malay]: Makrosprache, Lebend
 - **per**: (Persian): per (ISO 639-2/B), fas (ISO 639-3), fa (ISO 639-1); Persisch [Persian]: Makrosprache, Lebend
 - **rum**: (Romanian; Moldavian; Moldovan): rum (ISO 639-2/B), ron (ISO 639-3), ro (ISO 639-1); Rumänisch [Romanian; Moldavian; Moldovan]: Einzelsprache, Lebend
 - **slo**: (Slovak): slo (ISO 639-2/B), slk (ISO 639-3), sk (ISO 639-1); Slowakisch [Slovak]: Einzelsprache, Lebend
 - **tib**: (Tibetan): tib (ISO 639-2/B), bod (ISO 639-3), bo (ISO 639-1); Tibetisch [Tibetan]: Einzelsprache, Lebend
 - **wel**: (Welsh): wel (ISO 639-2/B), cym (ISO 639-3), cy (ISO 639-1); Walisisch [Welsh]: Einzelsprache, Lebend

---

<p style="margin-bottom:60px"></p>

<a name="d17e7229"></a>
### Name (Mapping)
|     |     |
| --- | --- |
| **Name** | `@name`  |
| **Datatype** | string |
| **Label** (de) | Name (Mapping) |
| **Description** (de) | Name des Labels bzw. Terms |
| **Contained by** | [`mappingLabel`](specs-elems.md#d17e6259) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e1371"></a>
### Ortstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Ortstyp |
| **Label** (en) | Type of a Place |
| **Description** (de) | Typisierung des dokumentierten Orts (z.B. als fiktiver Ort, Gebäude oder Land). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName). |
| **Description** (en) | Spezifies the place encoded as eg. fictional Place, Building or Country). If no `@gndo:type` is provided the default is [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName). |
| **Contained by** | [`place`](specs-elems.md#d17e1265) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#AdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#Country)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#FictivePlace)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#MemberState)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousTerritory)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#WayBorderOrLine)

---

<p style="margin-bottom:60px"></p>

<a name="person-types"></a>
### Personentyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Personentyp |
| **Label** (en) | Type of a Person |
| **Description** (de) | Typisierung der dokumentierten Person (z.B. als fiktive Person, Gott oder royale Person). Wenn kein `@gndo:type` vergeben wird, gilt die Person als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson). |
| **Description** (en) | Spezifies the person encoded as eg. fictional character, god oder royal). If no `@gndo:type` is provided the default is [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson). |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#Gods)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#Spirits)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6773"></a>
### Projekt ID
|     |     |
| --- | --- |
| **Name** | `@project`  |
| **Datatype** | string |
| **Label** (de) | Projekt ID |
| **Description** (de) | Identifiers einer Projekts, mit dem der Datensatz in einem entityXML Store assoziiert ist. |
| **Contained by** | [`store:store`](specs-elems.md#d17e6745) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6476"></a>
### Provider-ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | Provider-ID |
| **Description** (de) | Der GND-Agentur Identifier des Datenproviders. Dieser Identifier wird in der Regel direkt von der GND-Agentur, bei der der Datenlieferant registriert ist, vergeben. |
| **Contained by** | [`provider`](specs-elems.md#data-provider) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e3174"></a>
### Quelle der Koordinaten
|     |     |
| --- | --- |
| **Name** | `@geo:source`  |
| **Datatype** | anyURI |
| **Label** (de) | Quelle der Koordinaten |
| **Label** (en) | Source of Coordinates |
| **Description** (de) | Ein URL zur Quelle der documentierten Koordinaten. |
| **Description** (en) | An URL pointing to the source of the coordinates encoded. |
| **Contained by** | [`geo:hasGeometry`](specs-elems.md#d17e3150) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7648"></a>
### Record ID
|     |     |
| --- | --- |
| **Name** | `@xml:id`  |
| **Datatype** | ID |
| **Label** (de) | Record ID |
| **Description** (de) | Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend! |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7662"></a>
### Referenz
|     |     |
| --- | --- |
| **Name** | `@ref`  |
| **Datatype** | anyURI (*Schematron Pattern*) |
| **Label** (de) | Referenz |
| **Description** (de) | Verweis auf die ID (`@xml:id`) eines Records in der selben entityXML Ressource oder auf einen HTTP-URI. |
| **Contained by** | [`bf:instanceOf`](specs-elems.md#instance-of)  [`embodimentOf`](specs-elems.md#d17e1553)  [`gndo:accordingWork`](specs-elems.md#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:affiliation`](specs-elems.md#d17e3398)  [`gndo:author`](specs-elems.md#d17e3422)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:contributor`](specs-elems.md#d17e3635)  [`gndo:editor`](specs-elems.md#d17e4281)  [`gndo:exhibitor`](specs-elems.md#d17e4336)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:fieldOfStudy`](specs-elems.md#d17e4451)  [`gndo:firstAuthor`](specs-elems.md#d17e4491)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e4421)  [`gndo:literarySource`](specs-elems.md#d17e4888)  [`gndo:organizerOrHost`](specs-elems.md#d17e4943)  [`gndo:place`](specs-elems.md#entity-place-standard)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](specs-elems.md#d17e5119)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e5302)  [`gndo:pseudonym`](specs-elems.md#d17e5556)  [`gndo:publication`](specs-elems.md#d17e5604)  [`gndo:relatedWork`](specs-elems.md#d17e5806)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e5866)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:titleOfNobility`](specs-elems.md#d17e5969)  [`gndo:topic`](specs-elems.md#d17e6005)  [`realizationOf`](specs-elems.md#d17e1479) |

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="@ref[starts-with(., '#')]" role="error">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:assert test="//element()[@xml:id = $id]">(Wrong ID): There is no ID "<sch:value-of select="$id"/>" in this resource! Your
                        reference is most likely wrong!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5652"></a>
### Rolle der Entität (Publikation)
|     |     |
| --- | --- |
| **Name** | `@role`  |
| **Datatype** | string |
| **Label** (de) | Rolle der Entität (Publikation) |
| **Description** (de) | Hinweis darauf, welche Rolle die beschreibene Entität in Zusammenhang mit der Publikation spielt. |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e5604) |

***Predefined Values***  

 - **author**: (Autor:in *default*) Die beschriebene Entität (Person) ist der/die Autor:in der Publikation.
 - **editor**: (Herausgeber:in) Die beschriebene Entität (Person) ist der/die Herausgeber:in der Publikation.
 - **about**: (About) Die Publikation handelt von der beschriebenen Entität.

---

<p style="margin-bottom:60px"></p>

<a name="d17e7703"></a>
### Schrift
|     |     |
| --- | --- |
| **Name** | `@script`  |
| **Datatype** | string |
| **Label** (de) | Schrift |
| **Label** (en) | Script |
| **Description** (de) | Die Schrift, in der die Information angegeben ist wie in ISO 15924 definiert, vgl. http://www.unicode.org/iso15924/codelists.html. |
| **Description** (en) | The script, the information is given in as defined by ISO 15924, see: http://www.unicode.org/iso15924/codelists.html. |
| **Contained by** | [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard) |

***Predefined Values***  

 - **Adlm**: (Adlam) Scriptcode für Adlam (#166), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Afak**: (Afaka) Scriptcode für Afaka (#439), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Aghb**: (Caucasian Albanian) Scriptcode für Caucasian Albanian (#239), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ahom**: (Ahom, Tai Ahom) Scriptcode für Ahom, Tai Ahom (#338), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Arab**: (Arabic) Scriptcode für Arabic (#160), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Aran**: (Arabic (Nastaliq variant)) Scriptcode für Arabic (Nastaliq variant) (#161), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Armi**: (Imperial Aramaic) Scriptcode für Imperial Aramaic (#124), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Armn**: (Armenian) Scriptcode für Armenian (#230), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Avst**: (Avestan) Scriptcode für Avestan (#134), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bali**: (Balinese) Scriptcode für Balinese (#360), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bamu**: (Bamum) Scriptcode für Bamum (#435), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bass**: (Bassa Vah) Scriptcode für Bassa Vah (#259), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Batk**: (Batak) Scriptcode für Batak (#365), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Beng**: (Bengali (Bangla)) Scriptcode für Bengali (Bangla) (#325), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bhks**: (Bhaiksuki) Scriptcode für Bhaiksuki (#334), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Blis**: (Blissymbols) Scriptcode für Blissymbols (#550), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bopo**: (Bopomofo) Scriptcode für Bopomofo (#285), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Brah**: (Brahmi) Scriptcode für Brahmi (#300), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Brai**: (Braille) Scriptcode für Braille (#570), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bugi**: (Buginese) Scriptcode für Buginese (#367), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Buhd**: (Buhid) Scriptcode für Buhid (#372), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cakm**: (Chakma) Scriptcode für Chakma (#349), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cans**: (Unified Canadian Aboriginal Syllabics) Scriptcode für Unified Canadian Aboriginal Syllabics (#440), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cari**: (Carian) Scriptcode für Carian (#201), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cham**: (Cham) Scriptcode für Cham (#358), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cher**: (Cherokee) Scriptcode für Cherokee (#445), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Chis**: (Chisoi) Scriptcode für Chisoi (#298), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Chrs**: (Chorasmian) Scriptcode für Chorasmian (#109), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cirt**: (Cirth) Scriptcode für Cirth (#291), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Copt**: (Coptic) Scriptcode für Coptic (#204), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cpmn**: (Cypro-Minoan) Scriptcode für Cypro-Minoan (#402), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cprt**: (Cypriot syllabary) Scriptcode für Cypriot syllabary (#403), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cyrl**: (Cyrillic) Scriptcode für Cyrillic (#220), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cyrs**: (Cyrillic (Old Church Slavonic variant)) Scriptcode für Cyrillic (Old Church Slavonic variant) (#221), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Deva**: (Devanagari (Nagari)) Scriptcode für Devanagari (Nagari) (#315), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Diak**: (Dives Akuru) Scriptcode für Dives Akuru (#342), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Dogr**: (Dogra) Scriptcode für Dogra (#328), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Dsrt**: (Deseret (Mormon)) Scriptcode für Deseret (Mormon) (#250), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Dupl**: (Duployan shorthand, Duployan stenography) Scriptcode für Duployan shorthand, Duployan stenography (#755), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Egyd**: (Egyptian demotic) Scriptcode für Egyptian demotic (#070), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Egyh**: (Egyptian hieratic) Scriptcode für Egyptian hieratic (#060), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Egyp**: (Egyptian hieroglyphs) Scriptcode für Egyptian hieroglyphs (#050), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Elba**: (Elbasan) Scriptcode für Elbasan (#226), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Elym**: (Elymaic) Scriptcode für Elymaic (#128), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ethi**: (Ethiopic (Geʻez)) Scriptcode für Ethiopic (Geʻez) (#430), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gara**: (Garay) Scriptcode für Garay (#164), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Geok**: (Khutsuri (Asomtavruli and Nuskhuri)) Scriptcode für Khutsuri (Asomtavruli and Nuskhuri) (#241), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Geor**: (Georgian (Mkhedruli and Mtavruli)) Scriptcode für Georgian (Mkhedruli and Mtavruli) (#240), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Glag**: (Glagolitic) Scriptcode für Glagolitic (#225), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gong**: (Gunjala Gondi) Scriptcode für Gunjala Gondi (#312), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gonm**: (Masaram Gondi) Scriptcode für Masaram Gondi (#313), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Goth**: (Gothic) Scriptcode für Gothic (#206), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gran**: (Grantha) Scriptcode für Grantha (#343), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Grek**: (Greek) Scriptcode für Greek (#200), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gujr**: (Gujarati) Scriptcode für Gujarati (#320), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gukh**: (Gurung Khema) Scriptcode für Gurung Khema (#397), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Guru**: (Gurmukhi) Scriptcode für Gurmukhi (#310), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hanb**: (Han with Bopomofo (alias for Han + Bopomofo)) Scriptcode für Han with Bopomofo (alias for Han + Bopomofo) (#503), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hang**: (Hangul (Hangŭl, Hangeul)) Scriptcode für Hangul (Hangŭl, Hangeul) (#286), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hani**: (Han (Hanzi, Kanji, Hanja)) Scriptcode für Han (Hanzi, Kanji, Hanja) (#500), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hano**: (Hanunoo (Hanunóo)) Scriptcode für Hanunoo (Hanunóo) (#371), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hans**: (Han (Simplified variant)) Scriptcode für Han (Simplified variant) (#501), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hant**: (Han (Traditional variant)) Scriptcode für Han (Traditional variant) (#502), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hatr**: (Hatran) Scriptcode für Hatran (#127), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hebr**: (Hebrew) Scriptcode für Hebrew (#125), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hira**: (Hiragana) Scriptcode für Hiragana (#410), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hluw**: (Anatolian Hieroglyphs (Luwian Hieroglyphs, Hittite Hieroglyphs)) Scriptcode für Anatolian Hieroglyphs (Luwian Hieroglyphs, Hittite Hieroglyphs) (#080), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hmng**: (Pahawh Hmong) Scriptcode für Pahawh Hmong (#450), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hmnp**: (Nyiakeng Puachue Hmong) Scriptcode für Nyiakeng Puachue Hmong (#451), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hrkt**: (Japanese syllabaries (alias for Hiragana + Katakana)) Scriptcode für Japanese syllabaries (alias for Hiragana + Katakana) (#412), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hung**: (Old Hungarian (Hungarian Runic)) Scriptcode für Old Hungarian (Hungarian Runic) (#176), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Inds**: (Indus (Harappan)) Scriptcode für Indus (Harappan) (#610), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ital**: (Old Italic (Etruscan, Oscan, etc.)) Scriptcode für Old Italic (Etruscan, Oscan, etc.) (#210), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Jamo**: (Jamo (alias for Jamo subset of Hangul)) Scriptcode für Jamo (alias for Jamo subset of Hangul) (#284), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Java**: (Javanese) Scriptcode für Javanese (#361), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Jpan**: (Japanese (alias for Han + Hiragana + Katakana)) Scriptcode für Japanese (alias for Han + Hiragana + Katakana) (#413), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Jurc**: (Jurchen) Scriptcode für Jurchen (#510), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kali**: (Kayah Li) Scriptcode für Kayah Li (#357), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kana**: (Katakana) Scriptcode für Katakana (#411), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kawi**: (Kawi) Scriptcode für Kawi (#368), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Khar**: (Kharoshthi) Scriptcode für Kharoshthi (#305), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Khmr**: (Khmer) Scriptcode für Khmer (#355), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Khoj**: (Khojki) Scriptcode für Khojki (#322), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kitl**: (Khitan large script) Scriptcode für Khitan large script (#505), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kits**: (Khitan small script) Scriptcode für Khitan small script (#288), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Knda**: (Kannada) Scriptcode für Kannada (#345), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kore**: (Korean (alias for Hangul + Han)) Scriptcode für Korean (alias for Hangul + Han) (#287), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kpel**: (Kpelle) Scriptcode für Kpelle (#436), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Krai**: (Kirat Rai) Scriptcode für Kirat Rai (#396), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kthi**: (Kaithi) Scriptcode für Kaithi (#317), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lana**: (Tai Tham (Lanna)) Scriptcode für Tai Tham (Lanna) (#351), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Laoo**: (Lao) Scriptcode für Lao (#356), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Latf**: (Latin (Fraktur variant)) Scriptcode für Latin (Fraktur variant) (#217), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Latg**: (Latin (Gaelic variant)) Scriptcode für Latin (Gaelic variant) (#216), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Latn**: (Latin) Scriptcode für Latin (#215), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Leke**: (Leke) Scriptcode für Leke (#364), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lepc**: (Lepcha (Róng)) Scriptcode für Lepcha (Róng) (#335), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Limb**: (Limbu) Scriptcode für Limbu (#336), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lina**: (Linear A) Scriptcode für Linear A (#400), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Linb**: (Linear B) Scriptcode für Linear B (#401), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lisu**: (Lisu (Fraser)) Scriptcode für Lisu (Fraser) (#399), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Loma**: (Loma) Scriptcode für Loma (#437), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lyci**: (Lycian) Scriptcode für Lycian (#202), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lydi**: (Lydian) Scriptcode für Lydian (#116), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mahj**: (Mahajani) Scriptcode für Mahajani (#314), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Maka**: (Makasar) Scriptcode für Makasar (#366), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mand**: (Mandaic, Mandaean) Scriptcode für Mandaic, Mandaean (#140), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mani**: (Manichaean) Scriptcode für Manichaean (#139), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Marc**: (Marchen) Scriptcode für Marchen (#332), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Maya**: (Mayan hieroglyphs) Scriptcode für Mayan hieroglyphs (#090), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Medf**: (Medefaidrin (Oberi Okaime, Oberi Ɔkaimɛ)) Scriptcode für Medefaidrin (Oberi Okaime, Oberi Ɔkaimɛ) (#265), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mend**: (Mende Kikakui) Scriptcode für Mende Kikakui (#438), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Merc**: (Meroitic Cursive) Scriptcode für Meroitic Cursive (#101), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mero**: (Meroitic Hieroglyphs) Scriptcode für Meroitic Hieroglyphs (#100), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mlym**: (Malayalam) Scriptcode für Malayalam (#347), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Modi**: (Modi, Moḍī) Scriptcode für Modi, Moḍī (#324), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mong**: (Mongolian) Scriptcode für Mongolian (#145), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Moon**: (Moon (Moon code, Moon script, Moon type)) Scriptcode für Moon (Moon code, Moon script, Moon type) (#218), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mroo**: (Mro, Mru) Scriptcode für Mro, Mru (#264), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mtei**: (Meitei Mayek (Meithei, Meetei)) Scriptcode für Meitei Mayek (Meithei, Meetei) (#337), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mult**: (Multani) Scriptcode für Multani (#323), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mymr**: (Myanmar (Burmese)) Scriptcode für Myanmar (Burmese) (#350), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nagm**: (Nag Mundari) Scriptcode für Nag Mundari (#295), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nand**: (Nandinagari) Scriptcode für Nandinagari (#311), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Narb**: (Old North Arabian (Ancient North Arabian)) Scriptcode für Old North Arabian (Ancient North Arabian) (#106), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nbat**: (Nabataean) Scriptcode für Nabataean (#159), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Newa**: (Newa, Newar, Newari, Nepāla lipi) Scriptcode für Newa, Newar, Newari, Nepāla lipi (#333), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nkdb**: (Naxi Dongba (na²¹ɕi³³ to³³ba²¹, Nakhi Tomba)) Scriptcode für Naxi Dongba (na²¹ɕi³³ to³³ba²¹, Nakhi Tomba) (#085), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nkgb**: (Naxi Geba (na²¹ɕi³³ gʌ²¹ba²¹, 'Na-'Khi ²Ggŏ-¹baw, Nakhi Geba)) Scriptcode für Naxi Geba (na²¹ɕi³³ gʌ²¹ba²¹, 'Na-'Khi ²Ggŏ-¹baw, Nakhi Geba) (#420), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nkoo**: (N’Ko) Scriptcode für N’Ko (#165), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nshu**: (Nüshu) Scriptcode für Nüshu (#499), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ogam**: (Ogham) Scriptcode für Ogham (#212), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Olck**: (Ol Chiki (Ol Cemet’, Ol, Santali)) Scriptcode für Ol Chiki (Ol Cemet’, Ol, Santali) (#261), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Onao**: (Ol Onal) Scriptcode für Ol Onal (#296), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Orkh**: (Old Turkic, Orkhon Runic) Scriptcode für Old Turkic, Orkhon Runic (#175), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Orya**: (Oriya (Odia)) Scriptcode für Oriya (Odia) (#327), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Osge**: (Osage) Scriptcode für Osage (#219), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Osma**: (Osmanya) Scriptcode für Osmanya (#260), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ougr**: (Old Uyghur) Scriptcode für Old Uyghur (#143), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Palm**: (Palmyrene) Scriptcode für Palmyrene (#126), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Pauc**: (Pau Cin Hau) Scriptcode für Pau Cin Hau (#263), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Pcun**: (Proto-Cuneiform) Scriptcode für Proto-Cuneiform (#015), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Pelm**: (Proto-Elamite) Scriptcode für Proto-Elamite (#016), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Perm**: (Old Permic) Scriptcode für Old Permic (#227), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phag**: (Phags-pa) Scriptcode für Phags-pa (#331), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phli**: (Inscriptional Pahlavi) Scriptcode für Inscriptional Pahlavi (#131), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phlp**: (Psalter Pahlavi) Scriptcode für Psalter Pahlavi (#132), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phlv**: (Book Pahlavi) Scriptcode für Book Pahlavi (#133), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phnx**: (Phoenician) Scriptcode für Phoenician (#115), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Plrd**: (Miao (Pollard)) Scriptcode für Miao (Pollard) (#282), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Piqd**: (Klingon (KLI pIqaD)) Scriptcode für Klingon (KLI pIqaD) (#293), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Prti**: (Inscriptional Parthian) Scriptcode für Inscriptional Parthian (#130), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Psin**: (Proto-Sinaitic) Scriptcode für Proto-Sinaitic (#103), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Qaaa**: (Reserved for private use (start)) Scriptcode für Reserved for private use (start) (#900), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Qabx**: (Reserved for private use (end)) Scriptcode für Reserved for private use (end) (#949), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ranj**: (Ranjana) Scriptcode für Ranjana (#303), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Rjng**: (Rejang (Redjang, Kaganga)) Scriptcode für Rejang (Redjang, Kaganga) (#363), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Rohg**: (Hanifi Rohingya) Scriptcode für Hanifi Rohingya (#167), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Roro**: (Rongorongo) Scriptcode für Rongorongo (#620), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Runr**: (Runic) Scriptcode für Runic (#211), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Samr**: (Samaritan) Scriptcode für Samaritan (#123), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sara**: (Sarati) Scriptcode für Sarati (#292), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sarb**: (Old South Arabian) Scriptcode für Old South Arabian (#105), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Saur**: (Saurashtra) Scriptcode für Saurashtra (#344), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sgnw**: (SignWriting) Scriptcode für SignWriting (#095), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Shaw**: (Shavian (Shaw)) Scriptcode für Shavian (Shaw) (#281), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Shrd**: (Sharada, Śāradā) Scriptcode für Sharada, Śāradā (#319), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Shui**: (Shuishu) Scriptcode für Shuishu (#530), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sidd**: (Siddham, Siddhaṃ, Siddhamātṛkā) Scriptcode für Siddham, Siddhaṃ, Siddhamātṛkā (#302), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sidt**: (Sidetic) Scriptcode für Sidetic (#180), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sind**: (Khudawadi, Sindhi) Scriptcode für Khudawadi, Sindhi (#318), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sinh**: (Sinhala) Scriptcode für Sinhala (#348), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sogd**: (Sogdian) Scriptcode für Sogdian (#141), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sogo**: (Old Sogdian) Scriptcode für Old Sogdian (#142), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sora**: (Sora Sompeng) Scriptcode für Sora Sompeng (#398), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Soyo**: (Soyombo) Scriptcode für Soyombo (#329), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sund**: (Sundanese) Scriptcode für Sundanese (#362), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sunu**: (Sunuwar) Scriptcode für Sunuwar (#274), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sylo**: (Syloti Nagri) Scriptcode für Syloti Nagri (#316), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syrc**: (Syriac) Scriptcode für Syriac (#135), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syre**: (Syriac (Estrangelo variant)) Scriptcode für Syriac (Estrangelo variant) (#138), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syrj**: (Syriac (Western variant)) Scriptcode für Syriac (Western variant) (#137), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syrn**: (Syriac (Eastern variant)) Scriptcode für Syriac (Eastern variant) (#136), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tagb**: (Tagbanwa) Scriptcode für Tagbanwa (#373), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Takr**: (Takri, Ṭākrī, Ṭāṅkrī) Scriptcode für Takri, Ṭākrī, Ṭāṅkrī (#321), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tale**: (Tai Le) Scriptcode für Tai Le (#353), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Talu**: (New Tai Lue) Scriptcode für New Tai Lue (#354), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Taml**: (Tamil) Scriptcode für Tamil (#346), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tang**: (Tangut) Scriptcode für Tangut (#520), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tavt**: (Tai Viet) Scriptcode für Tai Viet (#359), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tayo**: (Tai Yo) Scriptcode für Tai Yo (#380), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Telu**: (Telugu) Scriptcode für Telugu (#340), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Teng**: (Tengwar) Scriptcode für Tengwar (#290), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tfng**: (Tifinagh (Berber)) Scriptcode für Tifinagh (Berber) (#120), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tglg**: (Tagalog (Baybayin, Alibata)) Scriptcode für Tagalog (Baybayin, Alibata) (#370), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Thaa**: (Thaana) Scriptcode für Thaana (#170), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Thai**: (Thai) Scriptcode für Thai (#352), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tibt**: (Tibetan) Scriptcode für Tibetan (#330), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tirh**: (Tirhuta) Scriptcode für Tirhuta (#326), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tnsa**: (Tangsa) Scriptcode für Tangsa (#275), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Todr**: (Todhri) Scriptcode für Todhri (#229), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tols**: (Tolong Siki) Scriptcode für Tolong Siki (#299), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Toto**: (Toto) Scriptcode für Toto (#294), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tutg**: (Tulu-Tigalari) Scriptcode für Tulu-Tigalari (#341), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ugar**: (Ugaritic) Scriptcode für Ugaritic (#040), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Vaii**: (Vai) Scriptcode für Vai (#470), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Visp**: (Visible Speech) Scriptcode für Visible Speech (#280), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Vith**: (Vithkuqi) Scriptcode für Vithkuqi (#228), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Wara**: (Warang Citi (Varang Kshiti)) Scriptcode für Warang Citi (Varang Kshiti) (#262), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Wcho**: (Wancho) Scriptcode für Wancho (#283), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Wole**: (Woleai) Scriptcode für Woleai (#480), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Xpeo**: (Old Persian) Scriptcode für Old Persian (#030), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Xsux**: (Cuneiform, Sumero-Akkadian) Scriptcode für Cuneiform, Sumero-Akkadian (#020), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Yezi**: (Yezidi) Scriptcode für Yezidi (#192), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Yiii**: (Yi) Scriptcode für Yi (#460), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zanb**: (Zanabazar Square (Zanabazarin Dörböljin Useg, Xewtee Dörböljin Bicig, Horizontal Square Script)) Scriptcode für Zanabazar Square (Zanabazarin Dörböljin Useg, Xewtee Dörböljin Bicig, Horizontal Square Script) (#339), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zinh**: (Code for inherited script) Scriptcode für Code for inherited script (#994), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zmth**: (Mathematical notation) Scriptcode für Mathematical notation (#995), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zsye**: (Symbols (Emoji variant)) Scriptcode für Symbols (Emoji variant) (#993), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zsym**: (Symbols) Scriptcode für Symbols (#996), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zxxx**: (Code for unwritten documents) Scriptcode für Code for unwritten documents (#997), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zyyy**: (Code for undetermined script) Scriptcode für Code for undetermined script (#998), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zzzz**: (Code for uncoded script) Scriptcode für Code for uncoded script (#999), vgl. http://www.unicode.org/iso15924/codelists.html

---

<p style="margin-bottom:60px"></p>

<a name="d17e5520"></a>
### Signifikanz
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Signifikanz |
| **Description** (de) | Angabe zur Signifikanz der Beschäftigung einer Person, d.h. ob es sich bei der angegebenen Beschäftigung um ihre hauptsächliche bzw. charakteristische Beschäfigung handelt. |
| **Contained by** | [`gndo:professionOrOccupation`](specs-elems.md#d17e5488) |

***Predefined Values***  

 - **significant**: (charakteristisch) Der Beruf oder der Tätigkeitsbereich ist für die Person charakteristisch.

---

<p style="margin-bottom:60px"></p>

<a name="d17e7157"></a>
### Sprache
|     |     |
| --- | --- |
| **Name** | `@xml:lang`  |
| **Datatype** | string |
| **Label** (de) | Sprache |
| **Label** (en) | Language |
| **Description** (de) | Die Sprache, in der die Information angegeben ist. |
| **Description** (en) | The language, the information is given in. |
| **Contained by** | [`dc:title`](specs-elems.md#d17e3011)  [`gndo:abbreviatedName`](specs-elems.md#d17e3272)  [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:titleOfNobility`](specs-elems.md#d17e5969)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`skos:note`](specs-elems.md#elem.skos.note) |

***Predefined Values***  

 - **aar**: (Afar): aar (ISO 639-3), aa (ISO 639-1); Afar [Afar]: Einzelsprache, Lebend
 - **abk**: (Abkhazian): abk (ISO 639-3), ab (ISO 639-1); Abchasisch [Abkhazian]: Einzelsprache, Lebend
 - **ace**: (Achinese): ace (ISO 639-3); Achinesisch [Achinese]: Einzelsprache, Lebend
 - **ach**: (Acoli): ach (ISO 639-3); Acholi [Acoli]: Einzelsprache, Lebend
 - **ada**: (Adangme): ada (ISO 639-3); Dangme [Adangme]: Einzelsprache, Lebend
 - **ady**: (Adyghe; Adygei): ady (ISO 639-3); Adygeisch [Adyghe; Adygei]: Einzelsprache, Lebend
 - **afa**: (Afro-Asiatic languages): afa (ISO 639-5); Afroasiatische Sprachen [Afro-Asiatic languages]: Sprachfamilie
 - **afh**: (Afrihili): afh (ISO 639-3); Afrihili [Afrihili]: Einzelsprache, Konstruiert
 - **afr**: (Afrikaans): afr (ISO 639-3), af (ISO 639-1); Afrikaans [Afrikaans]: Einzelsprache, Lebend
 - **ain**: (Ainu): ain (ISO 639-3); Ainu [Ainu]: Einzelsprache, Lebend
 - **aka**: (Akan): aka (ISO 639-3), ak (ISO 639-1); Akan [Akan]: Makrosprache, Lebend
 - **akk**: (Akkadian): akk (ISO 639-3); Akkadisch [Akkadian]: Einzelsprache, Alt
 - **ale**: (Aleut): ale (ISO 639-3); Aleutisch [Aleut]: Einzelsprache, Lebend
 - **alg**: (Algonquian languages): alg (ISO 639-5); Algonkin-Sprachen [Algonquian languages]: Sprachfamilie
 - **alt**: (Southern Altai): alt (ISO 639-3); Südaltaisch [Southern Altai]: Einzelsprache, Lebend
 - **amh**: (Amharic): amh (ISO 639-3), am (ISO 639-1); Amharisch [Amharic]: Einzelsprache, Lebend
 - **ang**: (English, Old (ca. 450–1100)): ang (ISO 639-3); Altenglisch [English, Old (ca. 450–1100)]: Einzelsprache, Historisch
 - **anp**: (Angika): anp (ISO 639-3); Angika [Angika]: Einzelsprache, Lebend
 - **apa**: (Apache languages): apa (ISO 639-5); Apache-Sprachen [Apache languages]: Sprachfamilie
 - **ara**: (Arabic): ara (ISO 639-3), ar (ISO 639-1); Arabisch [Arabic]: Makrosprache, Lebend
 - **arc**: (Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)): arc (ISO 639-3); Reichsaramäisch [Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)]: Einzelsprache, Alt
 - **arg**: (Aragonese): arg (ISO 639-3), an (ISO 639-1); Aragonesisch [Aragonese]: Einzelsprache, Lebend
 - **arn**: (Mapudungun; Mapuche): arn (ISO 639-3); Mapudungun [Mapudungun; Mapuche]: Einzelsprache, Lebend
 - **arp**: (Arapaho): arp (ISO 639-3); Arapaho [Arapaho]: Einzelsprache, Lebend
 - **art**: (Artificial languages): art (ISO 639-5); Konstruierte Sprachen [Artificial languages]: Sprachfamilie
 - **arw**: (Arawak): arw (ISO 639-3); Arawak [Arawak]: Einzelsprache, Lebend
 - **asm**: (Assamese): asm (ISO 639-3), as (ISO 639-1); Assamesisch [Assamese]: Einzelsprache, Lebend
 - **ast**: (Asturian; Bable; Leonese; Asturleonese): ast (ISO 639-3); Asturisch [Asturian; Bable; Leonese; Asturleonese]: Einzelsprache, Lebend
 - **ath**: (Athapascan languages): ath (ISO 639-5); Athapaskische Sprachen [Athapascan languages]: Sprachfamilie
 - **aus**: (Australian languages): aus (ISO 639-5); Australische Sprachen [Australian languages]: Sprachfamilie
 - **ava**: (Avaric): ava (ISO 639-3), av (ISO 639-1); Awarisch [Avaric]: Einzelsprache, Lebend
 - **ave**: (Avestan): ave (ISO 639-3), ae (ISO 639-1); Avestisch [Avestan]: Einzelsprache, Alt
 - **awa**: (Awadhi): awa (ISO 639-3); Awadhi [Awadhi]: Einzelsprache, Lebend
 - **aym**: (Aymara): aym (ISO 639-3), ay (ISO 639-1); Aymara [Aymara]: Makrosprache, Lebend
 - **aze**: (Azerbaijani): aze (ISO 639-3), az (ISO 639-1); Aserbaidschanisch [Azerbaijani]: Makrosprache, Lebend
 - **bad**: (Banda languages): bad (ISO 639-5); Banda-Sprachen [Banda languages]: Sprachfamilie
 - **bai**: (Bamileke languages): bai (ISO 639-5); Bamileke-Sprachen [Bamileke languages]: Sprachfamilie
 - **bak**: (Bashkir): bak (ISO 639-3), ba (ISO 639-1); Baschkirisch [Bashkir]: Einzelsprache, Lebend
 - **bal**: (Baluchi): bal (ISO 639-3); Belutschisch [Baluchi]: Makrosprache, Lebend
 - **bam**: (Bambara): bam (ISO 639-3), bm (ISO 639-1); Bambara [Bambara]: Einzelsprache, Lebend
 - **ban**: (Balinese): ban (ISO 639-3); Balinesisch [Balinese]: Einzelsprache, Lebend
 - **bas**: (Basa): bas (ISO 639-3); Bassa [Basa]: Einzelsprache, Lebend
 - **bat**: (Baltic languages): bat (ISO 639-5); Baltische Sprachen [Baltic languages]: Sprachfamilie
 - **bej**: (Beja; Bedawiyet): bej (ISO 639-3); Bedscha [Beja; Bedawiyet]: Einzelsprache, Lebend
 - **bel**: (Belarusian): bel (ISO 639-3), be (ISO 639-1); Belarussisch [Belarusian]: Einzelsprache, Lebend
 - **bem**: (Bemba): bem (ISO 639-3); Bemba [Bemba]: Einzelsprache, Lebend
 - **ben**: (Bengali): ben (ISO 639-3), bn (ISO 639-1); Bengalisch [Bengali]: Einzelsprache, Lebend
 - **ber**: (Berber languages): ber (ISO 639-5); Berbersprachen [Berber languages]: Sprachfamilie
 - **bho**: (Bhojpuri): bho (ISO 639-3); Bhojpuri [Bhojpuri]: Einzelsprache, Lebend
 - **bih**: (Bihari languages): bih (ISO 639-5), bh (ISO 639-1); Bihari [Bihari languages]: Sprachfamilie
 - **bik**: (Bikol): bik (ISO 639-3); Bikolano [Bikol]: Makrosprache, Lebend
 - **bin**: (Bini; Edo): bin (ISO 639-3); Edo [Bini; Edo]: Einzelsprache, Lebend
 - **bis**: (Bislama): bis (ISO 639-3), bi (ISO 639-1); Bislama [Bislama]: Einzelsprache, Lebend
 - **bla**: (Siksika): bla (ISO 639-3); Blackfoot [Siksika]: Einzelsprache, Lebend
 - **bnt**: (Bantu (Other)): bnt (ISO 639-5); Bantusprachen [Bantu (Other)]: Sprachfamilie
 - **bos**: (Bosnian): bos (ISO 639-3), bs (ISO 639-1); Bosnisch [Bosnian]: Einzelsprache, Lebend
 - **bra**: (Braj): bra (ISO 639-3); Braj-Bhakha [Braj]: Einzelsprache, Lebend
 - **bre**: (Breton): bre (ISO 639-3), br (ISO 639-1); Bretonisch [Breton]: Einzelsprache, Lebend
 - **btk**: (Batak languages): btk (ISO 639-5); Bataksprachen [Batak languages]: Sprachfamilie
 - **bua**: (Buriat): bua (ISO 639-3); Burjatisch [Buriat]: Makrosprache, Lebend
 - **bug**: (Buginese): bug (ISO 639-3); Buginesisch [Buginese]: Einzelsprache, Lebend
 - **bul**: (Bulgarian): bul (ISO 639-3), bg (ISO 639-1); Bulgarisch [Bulgarian]: Einzelsprache, Lebend
 - **byn**: (Blin; Bilin): byn (ISO 639-3); Blin [Blin; Bilin]: Einzelsprache, Lebend
 - **cad**: (Caddo): cad (ISO 639-3); Caddo [Caddo]: Einzelsprache, Lebend
 - **cai**: (Central American Indian languages): cai (ISO 639-5); Mesoamerikanische Sprachen [Central American Indian languages]: Sprachfamilie
 - **car**: (Galibi Carib): car (ISO 639-3); Karib [Galibi Carib]: Einzelsprache, Lebend
 - **cat**: (Catalan; Valencian): cat (ISO 639-3), ca (ISO 639-1); Katalanisch, Valencianisch [Catalan; Valencian]: Einzelsprache, Lebend
 - **cau**: (Caucasian languages): cau (ISO 639-5); Kaukasische Sprachen [Caucasian languages]: Sprachfamilie
 - **ceb**: (Cebuano): ceb (ISO 639-3); Cebuano [Cebuano]: Einzelsprache, Lebend
 - **cel**: (Celtic languages): cel (ISO 639-5); Keltische Sprachen [Celtic languages]: Sprachfamilie
 - **cha**: (Chamorro): cha (ISO 639-3), ch (ISO 639-1); Chamorro [Chamorro]: Einzelsprache, Lebend
 - **chb**: (Chibcha): chb (ISO 639-3); Chibcha [Chibcha]: Einzelsprache, Ausgestorben
 - **che**: (Chechen): che (ISO 639-3), ce (ISO 639-1); Tschetschenisch [Chechen]: Einzelsprache, Lebend
 - **chg**: (Chagatai): chg (ISO 639-3); Tschagataisch [Chagatai]: Einzelsprache, Ausgestorben
 - **chk**: (Chuukese): chk (ISO 639-3); Chuukesisch [Chuukese]: Einzelsprache, Lebend
 - **chm**: (Mari): chm (ISO 639-3); Mari [Mari]: Makrosprache, Lebend
 - **chn**: (Chinook jargon): chn (ISO 639-3); Chinook Wawa [Chinook jargon]: Einzelsprache, Lebend
 - **cho**: (Choctaw): cho (ISO 639-3); Choctaw [Choctaw]: Einzelsprache, Lebend
 - **chp**: (Chipewyan; Dene Suline): chp (ISO 639-3); Chipewyan [Chipewyan; Dene Suline]: Einzelsprache, Lebend
 - **chr**: (Cherokee): chr (ISO 639-3); Cherokee [Cherokee]: Einzelsprache, Lebend
 - **chu**: (Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic): chu (ISO 639-3), cu (ISO 639-1); Kirchenslawisch, Altkirchenslawisch [Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic]: Einzelsprache, Alt
 - **chv**: (Chuvash): chv (ISO 639-3), cv (ISO 639-1); Tschuwaschisch [Chuvash]: Einzelsprache, Lebend
 - **chy**: (Cheyenne): chy (ISO 639-3); Cheyenne [Cheyenne]: Einzelsprache, Lebend
 - **cmc**: (Chamic languages): cmc (ISO 639-5); Chamische Sprachen [Chamic languages]: Sprachfamilie
 - **cnr**: (Montenegrin): cnr (ISO 639-3); Montenegrinisch [Montenegrin]: Einzelsprache, Lebend
 - **cop**: (Coptic): cop (ISO 639-3); Koptisch [Coptic]: Einzelsprache, Ausgestorben
 - **cor**: (Cornish): cor (ISO 639-3), kw (ISO 639-1); Kornisch [Cornish]: Einzelsprache, Lebend
 - **cos**: (Corsican): cos (ISO 639-3), co (ISO 639-1); Korsisch [Corsican]: Einzelsprache, Lebend
 - **cpe**: (Creoles and pidgins, English based): cpe (ISO 639-5); Englisch-basierte Kreols und Pidgins [Creoles and pidgins, English based]: Sprachfamilie
 - **cpf**: (Creoles and pidgins, French-based): cpf (ISO 639-5); Französisch-basierte Kreols und Pidgins [Creoles and pidgins, French-based]: Sprachfamilie
 - **cpp**: (Creoles and pidgins, Portuguese-based): cpp (ISO 639-5); Portugiesisch-basierte Kreols und Pidgins [Creoles and pidgins, Portuguese-based]: Sprachfamilie
 - **cre**: (Cree): cre (ISO 639-3), cr (ISO 639-1); Cree [Cree]: Makrosprache, Lebend
 - **crh**: (Crimean Tatar; Crimean Turkish): crh (ISO 639-3); Krimtatarisch [Crimean Tatar; Crimean Turkish]: Einzelsprache, Lebend
 - **crp**: (Creoles and pidgins): crp (ISO 639-5); Kreol- und Pidginsprachen [Creoles and pidgins]: Sprachfamilie
 - **csb**: (Kashubian): csb (ISO 639-3); Kaschubisch [Kashubian]: Einzelsprache, Lebend
 - **cus**: (Cushitic languages): cus (ISO 639-5); Kuschitische Sprachen [Cushitic languages]: Sprachfamilie
 - **dak**: (Dakota): dak (ISO 639-3); Dakota [Dakota]: Einzelsprache, Lebend
 - **dan**: (Danish): dan (ISO 639-3), da (ISO 639-1); Dänisch [Danish]: Einzelsprache, Lebend
 - **dar**: (Dargwa): dar (ISO 639-3); Darginisch [Dargwa]: Einzelsprache, Lebend
 - **day**: (Land Dayak languages): day (ISO 639-5); Land-Dayak-Sprachen [Land Dayak languages]: Sprachfamilie
 - **del**: (Delaware): del (ISO 639-3); Delawarisch [Delaware]: Makrosprache, Lebend
 - **den**: (Slave (Athapascan)): den (ISO 639-3); Slavey [Slave (Athapascan)]: Makrosprache, Lebend
 - **dgr**: (Dogrib): dgr (ISO 639-3); Dogrib [Dogrib]: Einzelsprache, Lebend
 - **din**: (Dinka): din (ISO 639-3); Dinka [Dinka]: Makrosprache, Lebend
 - **div**: (Divehi; Dhivehi; Maldivian): div (ISO 639-3), dv (ISO 639-1); Dhivehi [Divehi; Dhivehi; Maldivian]: Einzelsprache, Lebend
 - **doi**: (Dogri): doi (ISO 639-3); Dogri [Dogri]: Makrosprache, Lebend
 - **dra**: (Dravidian languages): dra (ISO 639-5); Dravidische Sprachen [Dravidian languages]: Sprachfamilie
 - **dsb**: (Lower Sorbian): dsb (ISO 639-3); Niedersorbisch [Lower Sorbian]: Einzelsprache, Lebend
 - **dua**: (Duala): dua (ISO 639-3); Duala [Duala]: Einzelsprache, Lebend
 - **dum**: (Dutch, Middle (ca. 1050–1350)): dum (ISO 639-3); Mittelniederländisch [Dutch, Middle (ca. 1050–1350)]: Einzelsprache, Historisch
 - **dyu**: (Dyula): dyu (ISO 639-3); Dioula [Dyula]: Einzelsprache, Lebend
 - **dzo**: (Dzongkha): dzo (ISO 639-3), dz (ISO 639-1); Dzongkha [Dzongkha]: Einzelsprache, Lebend
 - **efi**: (Efik): efi (ISO 639-3); Efik [Efik]: Einzelsprache, Lebend
 - **egy**: (Egyptian (Ancient)): egy (ISO 639-3); Ägyptisch [Egyptian (Ancient)]: Einzelsprache, Alt
 - **eka**: (Ekajuk): eka (ISO 639-3); Ekajuk [Ekajuk]: Einzelsprache, Lebend
 - **elx**: (Elamite): elx (ISO 639-3); Elamisch [Elamite]: Einzelsprache, Alt
 - **eng**: (English): eng (ISO 639-3), en (ISO 639-1); Englisch [English]: Einzelsprache, Lebend
 - **enm**: (English, Middle (1100–1500)): enm (ISO 639-3); Mittelenglisch [English, Middle (1100–1500)]: Einzelsprache, Historisch
 - **epo**: (Esperanto): epo (ISO 639-3), eo (ISO 639-1); Esperanto [Esperanto]: Einzelsprache, Konstruiert
 - **est**: (Estonian): est (ISO 639-3), et (ISO 639-1); Estnisch [Estonian]: Makrosprache, Lebend
 - **ewe**: (Ewe): ewe (ISO 639-3), ee (ISO 639-1); Ewe [Ewe]: Einzelsprache, Lebend
 - **ewo**: (Ewondo): ewo (ISO 639-3); Ewondo [Ewondo]: Einzelsprache, Lebend
 - **fan**: (Fang): fan (ISO 639-3); Fang [Fang]: Einzelsprache, Lebend
 - **fao**: (Faroese): fao (ISO 639-3), fo (ISO 639-1); Färöisch [Faroese]: Einzelsprache, Lebend
 - **fat**: (Fanti): fat (ISO 639-3); Fante [Fanti]: Einzelsprache, Lebend
 - **fij**: (Fijian): fij (ISO 639-3), fj (ISO 639-1); Fidschi [Fijian]: Einzelsprache, Lebend
 - **fil**: (Filipino; Pilipino): fil (ISO 639-3); Filipino [Filipino; Pilipino]: Einzelsprache, Lebend
 - **fin**: (Finnish): fin (ISO 639-3), fi (ISO 639-1); Finnisch [Finnish]: Einzelsprache, Lebend
 - **fiu**: (Finno-Ugrian languages): fiu (ISO 639-5); Finno-ugrische Sprachen [Finno-Ugrian languages]: Sprachfamilie
 - **fon**: (Fon): fon (ISO 639-3); Fon [Fon]: Einzelsprache, Lebend
 - **frm**: (French, Middle (ca. 1400–1600)): frm (ISO 639-3); Mittelfranzösisch [French, Middle (ca. 1400–1600)]: Einzelsprache, Historisch
 - **fro**: (French, Old (842–ca. 1400)): fro (ISO 639-3); Altfranzösisch [French, Old (842–ca. 1400)]: Einzelsprache, Historisch
 - **frr**: (Northern Frisian): frr (ISO 639-3); Nordfriesisch [Northern Frisian]: Einzelsprache, Lebend
 - **frs**: (East Frisian Low Saxon): frs (ISO 639-3); Ostfriesisches Platt [East Frisian Low Saxon]: Einzelsprache, Lebend
 - **fry**: (Western Frisian): fry (ISO 639-3), fy (ISO 639-1); Westfriesisch [Western Frisian]: Einzelsprache, Lebend
 - **ful**: (Fulah): ful (ISO 639-3), ff (ISO 639-1); Fulfulde [Fulah]: Makrosprache, Lebend
 - **fur**: (Friulian): fur (ISO 639-3); Furlanisch [Friulian]: Einzelsprache, Lebend
 - **gaa**: (Einzelsprache): gaa (ISO 639-3), Ga (ISO 639-1); Ga [Einzelsprache]: Lebend, Gã 
 - **gay**: (Gayo): gay (ISO 639-3); Gayo [Gayo]: Einzelsprache, Lebend
 - **gba**: (Gbaya): gba (ISO 639-3); Gbaya-Sprachen [Gbaya]: Makrosprache, Lebend
 - **gem**: (Germanic languages): gem (ISO 639-5); Germanische Sprachen [Germanic languages]: Sprachfamilie
 - **gez**: (Geez): gez (ISO 639-3); Altäthiopisch [Geez]: Einzelsprache, Alt
 - **gil**: (Gilbertese): gil (ISO 639-3); Kiribatisch, Gilbertesisch [Gilbertese]: Einzelsprache, Lebend
 - **gla**: (Gaelic; Scottish Gaelic): gla (ISO 639-3), gd (ISO 639-1); Schottisch-gälisch [Gaelic; Scottish Gaelic]: Einzelsprache, Lebend
 - **gle**: (Irish): gle (ISO 639-3), ga (ISO 639-1); Irisch [Irish]: Einzelsprache, Lebend
 - **glg**: (Galician): glg (ISO 639-3), gl (ISO 639-1); Galicisch, Galegisch [Galician]: Einzelsprache, Lebend
 - **glv**: (Manx): glv (ISO 639-3), gv (ISO 639-1); Manx,Manx-Gälisch [Manx]: Einzelsprache, Lebend
 - **gmh**: (German, Middle High (ca. 1050–1500)): gmh (ISO 639-3); Mittelhochdeutsch [German, Middle High (ca. 1050–1500)]: Einzelsprache, Historisch
 - **goh**: (German, Old High (ca. 750–1050)): goh (ISO 639-3); Althochdeutsch [German, Old High (ca. 750–1050)]: Einzelsprache, Historisch
 - **gon**: (Gondi): gon (ISO 639-3); Gondi [Gondi]: Makrosprache, Lebend
 - **gor**: (Gorontalo): gor (ISO 639-3); Gorontalo [Gorontalo]: Einzelsprache, Lebend
 - **got**: (Gothic): got (ISO 639-3); Gotisch [Gothic]: Einzelsprache, Alt
 - **grb**: (Grebo): grb (ISO 639-3); Grebo [Grebo]: Makrosprache, Lebend
 - **grc**: (Greek, Ancient (bis 1453)): grc (ISO 639-3); Altgriechisch [Greek, Ancient (bis 1453)]: Einzelsprache, Historisch
 - **grn**: (Guarani): grn (ISO 639-3), gn (ISO 639-1); Guaraní [Guarani]: Makrosprache, Lebend
 - **gsw**: (Swiss German; Alemannic; Alsatian): gsw (ISO 639-3); Schweizerdeutsch [Swiss German; Alemannic; Alsatian]: Einzelsprache, Lebend
 - **guj**: (Gujarati): guj (ISO 639-3), gu (ISO 639-1); Gujarati [Gujarati]: Einzelsprache, Lebend
 - **gwi**: (Gwich'in): gwi (ISO 639-3); Gwich'in (Sprache) [Gwich'in]: Einzelsprache, Lebend
 - **hai**: (Haida): hai (ISO 639-3); Haida [Haida]: Makrosprache, Lebend
 - **hat**: (Haitian; Haitian Creole): hat (ISO 639-3), ht (ISO 639-1); Haitianisch-Kreolisch [Haitian; Haitian Creole]: Einzelsprache, Lebend
 - **hau**: (Hausa): hau (ISO 639-3), ha (ISO 639-1); Hausa [Hausa]: Einzelsprache, Lebend
 - **haw**: (Hawaiian): haw (ISO 639-3); Hawaiisch [Hawaiian]: Einzelsprache, Lebend
 - **heb**: (Hebrew): heb (ISO 639-3), he (ISO 639-1); Hebräisch [Hebrew]: Einzelsprache, Lebend
 - **her**: (Herero): her (ISO 639-3), hz (ISO 639-1); Otjiherero [Herero]: Einzelsprache, Lebend
 - **hil**: (Hiligaynon): hil (ISO 639-3); Hiligaynon [Hiligaynon]: Einzelsprache, Lebend
 - **him**: (Himachali languages; Western Pahari languages): him (ISO 639-5); West-Paharisprachen [Himachali languages; Western Pahari languages]: Sprachfamilie
 - **hin**: (Hindi): hin (ISO 639-3), hi (ISO 639-1); Hindi [Hindi]: Einzelsprache, Lebend
 - **hit**: (Hittite): hit (ISO 639-3); Hethitisch [Hittite]: Einzelsprache, Alt
 - **hmn**: (Hmong; Mong): hmn (ISO 639-3); Hmong-Sprache [Hmong; Mong]: Makrosprache, Lebend
 - **hmo**: (Hiri Motu): hmo (ISO 639-3), ho (ISO 639-1); Hiri Motu [Hiri Motu]: Einzelsprache, Lebend
 - **hrv**: (Croatian): hrv (ISO 639-3), hr (ISO 639-1); Kroatisch [Croatian]: Einzelsprache, Lebend
 - **hsb**: (Upper Sorbian): hsb (ISO 639-3); Obersorbisch [Upper Sorbian]: Einzelsprache, Lebend
 - **hun**: (Hungarian): hun (ISO 639-3), hu (ISO 639-1); Ungarisch [Hungarian]: Einzelsprache, Lebend
 - **hup**: (Hupa): hup (ISO 639-3); Hoopa [Hupa]: Einzelsprache, Lebend
 - **iba**: (Iban): iba (ISO 639-3); Iban [Iban]: Einzelsprache, Lebend
 - **ibo**: (Igbo): ibo (ISO 639-3), ig (ISO 639-1); Igbo [Igbo]: Einzelsprache, Lebend
 - **ido**: (Ido): ido (ISO 639-3), io (ISO 639-1); Ido [Ido]: Einzelsprache, Konstruiert
 - **iii**: (Sichuan Yi; Nuosu): iii (ISO 639-3), ii (ISO 639-1); Yi [Sichuan Yi; Nuosu]: Einzelsprache, Lebend
 - **ijo**: (Ijo languages): ijo (ISO 639-5); Ijo-Sprachen [Ijo languages]: Sprachfamilie
 - **iku**: (Inuktitut): iku (ISO 639-3), iu (ISO 639-1); Inuktitut [Inuktitut]: Makrosprache, Lebend
 - **ile**: (Interlingue; Occidental): ile (ISO 639-3), ie (ISO 639-1); Interlingue [Interlingue; Occidental]: Einzelsprache, Konstruiert
 - **ilo**: (Iloko): ilo (ISO 639-3); Ilokano [Iloko]: Einzelsprache, Lebend
 - **ina**: (Interlingua (International Auxiliary Language Association)): ina (ISO 639-3), ia (ISO 639-1); Interlingua [Interlingua (International Auxiliary Language Association)]: Einzelsprache, Konstruiert
 - **inc**: (Indic languages): inc (ISO 639-5); Indoarische Sprachen [Indic languages]: Sprachfamilie
 - **ind**: (Indonesian): ind (ISO 639-3), id (ISO 639-1); Indonesisch [Indonesian]: Einzelsprache, Lebend
 - **ine**: (Indo-European languages): ine (ISO 639-5); Indogermanische Sprachen [Indo-European languages]: Sprachfamilie
 - **inh**: (Ingush): inh (ISO 639-3); Inguschisch [Ingush]: Einzelsprache, Lebend
 - **ipk**: (Inupiaq): ipk (ISO 639-3), ik (ISO 639-1); Inupiaq [Inupiaq]: Makrosprache, Lebend
 - **ira**: (Iranian languages): ira (ISO 639-5); Iranische Sprachen [Iranian languages]: Sprachfamilie
 - **iro**: (Iroquoian languages): iro (ISO 639-5); Irokesische Sprachen [Iroquoian languages]: Sprachfamilie
 - **ita**: (Italian): ita (ISO 639-3), it (ISO 639-1); Italienisch [Italian]: Einzelsprache, Lebend
 - **jav**: (Javanese): jav (ISO 639-3), jv (ISO 639-1); Javanisch [Javanese]: Einzelsprache, Lebend
 - **jbo**: (Lojban): jbo (ISO 639-3); Lojban [Lojban]: Einzelsprache, Konstruiert
 - **jpn**: (Japanese): jpn (ISO 639-3), ja (ISO 639-1); Japanisch [Japanese]: Einzelsprache, Lebend
 - **jpr**: (Judeo-Persian): jpr (ISO 639-3); Judäo-Persisch [Judeo-Persian]: Einzelsprache, Lebend
 - **jrb**: (Judeo-Arabic): jrb (ISO 639-3); Judäo-Arabisch [Judeo-Arabic]: Makrosprache, Lebend
 - **kaa**: (Kara-Kalpak): kaa (ISO 639-3); Karakalpakisch [Kara-Kalpak]: Einzelsprache, Lebend
 - **kab**: (Kabyle): kab (ISO 639-3); Kabylisch [Kabyle]: Einzelsprache, Lebend
 - **kac**: (Kachin; Jingpho): kac (ISO 639-3); Jingpo [Kachin; Jingpho]: Einzelsprache, Lebend
 - **kal**: (Kalaallisut; Greenlandic): kal (ISO 639-3), kl (ISO 639-1); Grönländisch, Kalaallisut [Kalaallisut; Greenlandic]: Einzelsprache, Lebend
 - **kam**: (Kamba): kam (ISO 639-3); Kikamba [Kamba]: Einzelsprache, Lebend
 - **kan**: (Kannada): kan (ISO 639-3), kn (ISO 639-1); Kannada [Kannada]: Einzelsprache, Lebend
 - **kar**: (Karen languages): kar (ISO 639-5); Karenische Sprachen [Karen languages]: Sprachfamilie
 - **kas**: (Kashmiri): kas (ISO 639-3), ks (ISO 639-1); Kashmiri [Kashmiri]: Einzelsprache, Lebend
 - **kau**: (Kanuri): kau (ISO 639-3), kr (ISO 639-1); Kanuri [Kanuri]: Makrosprache, Lebend
 - **kaw**: (Kawi): kaw (ISO 639-3); Kawi, Altjavanisch [Kawi]: Einzelsprache, Alt
 - **kaz**: (Kazakh): kaz (ISO 639-3), kk (ISO 639-1); Kasachisch [Kazakh]: Einzelsprache, Lebend
 - **kbd**: (Kabardian): kbd (ISO 639-3); Kabardinisch, Ost-Tscherkessisch [Kabardian]: Einzelsprache, Lebend
 - **kha**: (Khasi): kha (ISO 639-3); Khasi [Khasi]: Einzelsprache, Lebend
 - **khi**: (Khoisan languages): khi (ISO 639-5); Khoisansprachen [Khoisan languages]: Sprachfamilie
 - **khm**: (Central Khmer): khm (ISO 639-3), km (ISO 639-1); Khmer [Central Khmer]: Einzelsprache, Lebend
 - **kho**: (Khotanese; Sakan): kho (ISO 639-3); Khotanesisch [Khotanese; Sakan]: Einzelsprache, Alt
 - **kik**: (Kikuyu; Gikuyu): kik (ISO 639-3), ki (ISO 639-1); Kikuyu [Kikuyu; Gikuyu]: Einzelsprache, Lebend
 - **kin**: (Kinyarwanda): kin (ISO 639-3), rw (ISO 639-1); Kinyarwanda, Ruandisch [Kinyarwanda]: Einzelsprache, Lebend
 - **kir**: (Kirghiz; Kyrgyz): kir (ISO 639-3), ky (ISO 639-1); Kirgisisch [Kirghiz; Kyrgyz]: Einzelsprache, Lebend
 - **kmb**: (Kimbundu): kmb (ISO 639-3); Kimbundu [Kimbundu]: Einzelsprache, Lebend
 - **kok**: (Konkani): kok (ISO 639-3); Konkani [Konkani]: Makrosprache, Lebend
 - **kom**: (Komi): kom (ISO 639-3), kv (ISO 639-1); Komi [Komi]: Makrosprache, Lebend
 - **kon**: (Kongo): kon (ISO 639-3), kg (ISO 639-1); Kikongo [Kongo]: Makrosprache, Lebend
 - **kor**: (Korean): kor (ISO 639-3), ko (ISO 639-1); Koreanisch [Korean]: Einzelsprache, Lebend
 - **kos**: (Kosraean): kos (ISO 639-3); Kosraeanisch [Kosraean]: Einzelsprache, Lebend
 - **kpe**: (Kpelle): kpe (ISO 639-3); Kpelle [Kpelle]: Makrosprache, Lebend
 - **krc**: (Karachay-Balkar): krc (ISO 639-3); Karatschai-balkarisch [Karachay-Balkar]: Einzelsprache, Lebend
 - **krl**: (Karelian): krl (ISO 639-3); Karelisch [Karelian]: Einzelsprache, Lebend
 - **kro**: (Kru languages): kro (ISO 639-5); Kru-Sprachen [Kru languages]: Sprachfamilie
 - **kru**: (Kurukh): kru (ISO 639-3); Kurukh [Kurukh]: Einzelsprache, Lebend
 - **kua**: (Kuanyama; Kwanyama): kua (ISO 639-3), kj (ISO 639-1); oshiKwanyama [Kuanyama; Kwanyama]: Einzelsprache, Lebend
 - **kum**: (Kumyk): kum (ISO 639-3); Kumykisch [Kumyk]: Einzelsprache, Lebend
 - **kur**: (Kurdish): kur (ISO 639-3), ku (ISO 639-1); Kurdisch [Kurdish]: Makrosprache, Lebend
 - **kut**: (Kutenai): kut (ISO 639-3); Kutanaha [Kutenai]: Einzelsprache, Lebend
 - **lad**: (Ladino): lad (ISO 639-3); Judenspanisch, Ladino, Sephardisch [Ladino]: Einzelsprache, Lebend
 - **lah**: (Lahnda): lah (ISO 639-3); Lahnda, Westpanjabi [Lahnda]: Makrosprache, Lebend
 - **lam**: (Lamba): lam (ISO 639-3); Lamba [Lamba]: Einzelsprache, Lebend
 - **lao**: (Lao): lao (ISO 639-3), lo (ISO 639-1); Laotisch [Lao]: Einzelsprache, Lebend
 - **lat**: (Latin): lat (ISO 639-3), la (ISO 639-1); Latein [Latin]: Einzelsprache, Alt
 - **lav**: (Latvian): lav (ISO 639-3), lv (ISO 639-1); Lettisch [Latvian]: Makrosprache, Lebend
 - **lez**: (Lezghian): lez (ISO 639-3); Lesgisch [Lezghian]: Einzelsprache, Lebend
 - **lim**: (Limburgan; Limburger; Limburgish): lim (ISO 639-3), li (ISO 639-1); Limburgisch, Südniederfränkisch [Limburgan; Limburger; Limburgish]: Einzelsprache, Lebend
 - **lin**: (Lingala): lin (ISO 639-3), ln (ISO 639-1); Lingála [Lingala]: Einzelsprache, Lebend
 - **lit**: (Lithuanian): lit (ISO 639-3), lt (ISO 639-1); Litauisch [Lithuanian]: Einzelsprache, Lebend
 - **lol**: (Mongo): lol (ISO 639-3); Lomongo [Mongo]: Einzelsprache, Lebend
 - **loz**: (Lozi): loz (ISO 639-3); Lozi [Lozi]: Einzelsprache, Lebend
 - **ltz**: (Luxembourgish; Letzeburgesch): ltz (ISO 639-3), lb (ISO 639-1); Luxemburgisch [Luxembourgish; Letzeburgesch]: Einzelsprache, Lebend
 - **lua**: (Luba-Lulua): lua (ISO 639-3); Tschiluba [Luba-Lulua]: Einzelsprache, Lebend
 - **lub**: (Luba-Katanga): lub (ISO 639-3), lu (ISO 639-1); Kiluba [Luba-Katanga]: Einzelsprache, Lebend
 - **lug**: (Ganda): lug (ISO 639-3), lg (ISO 639-1); Luganda [Ganda]: Einzelsprache, Lebend
 - **lui**: (Luiseno): lui (ISO 639-3); Luiseño [Luiseno]: Einzelsprache, Ausgestorben
 - **lun**: (Lunda): lun (ISO 639-3); Chilunda [Lunda]: Einzelsprache, Lebend
 - **luo**: (Luo (Kenya and Tanzania)): luo (ISO 639-3); Luo [Luo (Kenya and Tanzania)]: Einzelsprache, Lebend
 - **lus**: (Lushai): lus (ISO 639-3); Mizo, Lushai [Lushai]: Einzelsprache, Lebend
 - **mad**: (Madurese): mad (ISO 639-3); Maduresisch [Madurese]: Einzelsprache, Lebend
 - **mag**: (Magahi): mag (ISO 639-3); Magadhi [Magahi]: Einzelsprache, Lebend
 - **mah**: (Marshallese): mah (ISO 639-3), mh (ISO 639-1); Marshallesisch [Marshallese]: Einzelsprache, Lebend
 - **mai**: (Maithili): mai (ISO 639-3); Maithili [Maithili]: Einzelsprache, Lebend
 - **mak**: (Makasar): mak (ISO 639-3); Makassar [Makasar]: Einzelsprache, Lebend
 - **mal**: (Malayalam): mal (ISO 639-3), ml (ISO 639-1); Malayalam [Malayalam]: Einzelsprache, Lebend
 - **man**: (Mandingo): man (ISO 639-3); Manding [Mandingo]: Makrosprache, Lebend
 - **map**: (Austronesian languages): map (ISO 639-5); Austronesische Sprachen [Austronesian languages]: Sprachfamilie
 - **mar**: (Marathi): mar (ISO 639-3), mr (ISO 639-1); Marathi [Marathi]: Einzelsprache, Lebend
 - **mas**: (Masai): mas (ISO 639-3); Maa, Kimaasai [Masai]: Einzelsprache, Lebend
 - **mdf**: (Moksha): mdf (ISO 639-3); Mokschanisch [Moksha]: Einzelsprache, Lebend
 - **mdr**: (Mandar): mdr (ISO 639-3); Mandar [Mandar]: Einzelsprache, Lebend
 - **men**: (Mende): men (ISO 639-3); Mende [Mende]: Einzelsprache, Lebend
 - **mga**: (Irish, Middle (900–1200)): mga (ISO 639-3); Mittelirisch [Irish, Middle (900–1200)]: Einzelsprache, Historisch
 - **mic**: (Mi'kmaq; Micmac): mic (ISO 639-3); Míkmawísimk [Mi'kmaq; Micmac]: Einzelsprache, Lebend
 - **min**: (Minangkabau): min (ISO 639-3); Minangkabauisch [Minangkabau]: Einzelsprache, Lebend
 - **mis**: (Uncoded languages): mis (ISO 639-3); „Unkodiert“ [Uncoded languages]: Speziell
 - **mkh**: (Mon-Khmer languages): mkh (ISO 639-5); Mon-Khmer-Sprachen [Mon-Khmer languages]: Sprachfamilie
 - **mlg**: (Malagasy): mlg (ISO 639-3), mg (ISO 639-1); Malagasy, Malagassi [Malagasy]: Makrosprache, Lebend
 - **mlt**: (Maltese): mlt (ISO 639-3), mt (ISO 639-1); Maltesisch [Maltese]: Einzelsprache, Lebend
 - **mnc**: (Manchu): mnc (ISO 639-3); Mandschurisch [Manchu]: Einzelsprache, Lebend
 - **mni**: (Manipuri): mni (ISO 639-3); Meitei [Manipuri]: Einzelsprache, Lebend
 - **mno**: (Manobo languages): mno (ISO 639-5); Manobo-Sprachen [Manobo languages]: Sprachfamilie
 - **moh**: (Mohawk): moh (ISO 639-3); Mohawk [Mohawk]: Einzelsprache, Lebend
 - **mon**: (Mongolian): mon (ISO 639-3), mn (ISO 639-1); Mongolisch [Mongolian]: Makrosprache, Lebend
 - **mos**: (Mossi): mos (ISO 639-3); Mòoré [Mossi]: Einzelsprache, Lebend
 - **mul**: (Multiple languages): mul (ISO 639-3); „Mehrsprachig“ [Multiple languages]: Speziell
 - **mun**: (Munda languages): mun (ISO 639-5); Munda-Sprachen [Munda languages]: Sprachfamilie
 - **mus**: (Creek): mus (ISO 639-3); Muskogee-Sprachen [Creek]: Sprachfamilie
 - **mwl**: (Mirandese): mwl (ISO 639-3); Mirandés [Mirandese]: Einzelsprache, Lebend
 - **mwr**: (Marwari): mwr (ISO 639-3); Marwari [Marwari]: Makrosprache, Lebend
 - **myn**: (Mayan languages): myn (ISO 639-5); Maya-Sprachen [Mayan languages]: Sprachfamilie
 - **myv**: (Erzya): myv (ISO 639-3); Ersjanisch, Ersja-Mordwinisch [Erzya]: Einzelsprache, Lebend
 - **nah**: (Nahuatl languages): nah (ISO 639-5); Nahuatl [Nahuatl languages]: Sprachfamilie
 - **nai**: (North American Indian languages): nai (ISO 639-5); Nordamerikanische Sprachen [North American Indian languages]: Sprachfamilie
 - **nap**: (Neapolitan): nap (ISO 639-3); Neapolitanisch [Neapolitan]: Einzelsprache, Lebend
 - **nau**: (Nauru): nau (ISO 639-3), na (ISO 639-1); Nauruisch [Nauru]: Einzelsprache, Lebend
 - **nav**: (Navajo; Navaho): nav (ISO 639-3), nv (ISO 639-1); Navajo [Navajo; Navaho]: Einzelsprache, Lebend
 - **nbl**: (Ndebele, South; South Ndebele): nbl (ISO 639-3), nr (ISO 639-1); Süd-Ndebele [Ndebele, South; South Ndebele]: Einzelsprache, Lebend
 - **nde**: (Ndebele, North; North Ndebele): nde (ISO 639-3), nd (ISO 639-1); Nord-Ndebele [Ndebele, North; North Ndebele]: Einzelsprache, Lebend
 - **ndo**: (Ndonga): ndo (ISO 639-3), ng (ISO 639-1); Ndonga [Ndonga]: Einzelsprache, Lebend
 - **nds**: (Low German; Low Saxon; German, Low; Saxon, Low): nds (ISO 639-3); Niederdeutsch, Plattdeutsch [Low German; Low Saxon; German, Low; Saxon, Low]: Einzelsprache, Lebend
 - **nep**: (Nepali): nep (ISO 639-3), ne (ISO 639-1); Nepali [Nepali]: Makrosprache, Lebend
 - **new**: (Nepal Bhasa; Newari): new (ISO 639-3); Newari [Nepal Bhasa; Newari]: Einzelsprache, Lebend
 - **nia**: (Nias): nia (ISO 639-3); Nias [Nias]: Einzelsprache, Lebend
 - **nic**: (Niger-Kordofanian languages): nic (ISO 639-5); Niger-Kongo-Sprachen [Niger-Kordofanian languages]: Sprachfamilie
 - **niu**: (Niuean): niu (ISO 639-3); Niueanisch [Niuean]: Einzelsprache, Lebend
 - **nno**: (Norwegian Nynorsk; Nynorsk, Norwegian): nno (ISO 639-3), nn (ISO 639-1); Nynorsk [Norwegian Nynorsk; Nynorsk, Norwegian]: Einzelsprache, Lebend
 - **nob**: (Bokmål, Norwegian; Norwegian Bokmål): nob (ISO 639-3), nb (ISO 639-1); Bokmål [Bokmål, Norwegian; Norwegian Bokmål]: Einzelsprache, Lebend
 - **nog**: (Nogai): nog (ISO 639-3); Nogaisch [Nogai]: Einzelsprache, Lebend
 - **non**: (Norse, Old): non (ISO 639-3); Altnordisch [Norse, Old]: Einzelsprache, Historisch
 - **nor**: (Norwegian): nor (ISO 639-3), no (ISO 639-1); Norwegisch [Norwegian]: Makrosprache, Lebend
 - **nqo**: (N'Ko): nqo (ISO 639-3); N’Ko [N'Ko]: Einzelsprache, Lebend
 - **nso**: (Pedi; Sepedi; Northern Sotho): nso (ISO 639-3); Nord-Sotho [Pedi; Sepedi; Northern Sotho]: Einzelsprache, Lebend
 - **nub**: (Nubian languages): nub (ISO 639-5); Nubische Sprachen [Nubian languages]: Sprachfamilie
 - **nwc**: (Classical Newari; Old Newari; Classical Nepal Bhasa): nwc (ISO 639-3); Klassisches Newari [Classical Newari; Old Newari; Classical Nepal Bhasa]: Einzelsprache, Historisch
 - **nya**: (Chichewa; Chewa; Nyanja): nya (ISO 639-3), ny (ISO 639-1); Chichewa [Chichewa; Chewa; Nyanja]: Einzelsprache, Lebend
 - **nym**: (Nyamwezi): nym (ISO 639-3); Nyamwesi [Nyamwezi]: Einzelsprache, Lebend
 - **nyn**: (Nyankole): nyn (ISO 639-3); Runyankole, Runyankore [Nyankole]: Einzelsprache, Lebend
 - **nyo**: (Nyoro): nyo (ISO 639-3); Runyoro [Nyoro]: Einzelsprache, Lebend
 - **nzi**: (Nzima): nzi (ISO 639-3); Nzema [Nzima]: Einzelsprache, Lebend
 - **oci**: (Occitan (ab 1500); Provençal): oci (ISO 639-3), oc (ISO 639-1); Okzitanisch [Occitan (ab 1500); Provençal]: Einzelsprache, Lebend
 - **oji**: (Ojibwa): oji (ISO 639-3), oj (ISO 639-1); Ojibwe [Ojibwa]: Makrosprache, Lebend
 - **ori**: (Oriya): ori (ISO 639-3), or (ISO 639-1); Oriya [Oriya]: Makrosprache, Lebend
 - **orm**: (Oromo): orm (ISO 639-3), om (ISO 639-1); Oromo [Oromo]: Makrosprache, Lebend
 - **osa**: (Osage): osa (ISO 639-3); Osage [Osage]: Einzelsprache, Lebend
 - **oss**: (Ossetian; Ossetic): oss (ISO 639-3), os (ISO 639-1); Ossetisch [Ossetian; Ossetic]: Einzelsprache, Lebend
 - **ota**: (Turkish, Ottoman (1500–1928)): ota (ISO 639-3); Osmanisch, osmanisches Türkisch [Turkish, Ottoman (1500–1928)]: Einzelsprache, Historisch
 - **oto**: (Otomian languages): oto (ISO 639-5); Oto-Pame-Sprachen [Otomian languages]: Sprachfamilie
 - **paa**: (Papuan languages): paa (ISO 639-5); Papuasprachen [Papuan languages]: Sprachfamilie
 - **pag**: (Pangasinan): pag (ISO 639-3); Pangasinensisch [Pangasinan]: Einzelsprache, Lebend
 - **pal**: (Pahlavi): pal (ISO 639-3); Mittelpersisch, Pahlavi [Pahlavi]: Einzelsprache, Alt
 - **pam**: (Pampanga; Kapampangan): pam (ISO 639-3); Kapampangan [Pampanga; Kapampangan]: Einzelsprache, Lebend
 - **pan**: (Panjabi; Punjabi): pan (ISO 639-3), pa (ISO 639-1); Panjabi, Pandschabi [Panjabi; Punjabi]: Einzelsprache, Lebend
 - **pap**: (Papiamento): pap (ISO 639-3); Papiamentu [Papiamento]: Einzelsprache, Lebend
 - **pau**: (Palauan): pau (ISO 639-3); Palauisch [Palauan]: Einzelsprache, Lebend
 - **peo**: (Persian, Old (ca. 600–400 v. Chr.)): peo (ISO 639-3); Altpersisch [Persian, Old (ca. 600–400 v. Chr.)]: Einzelsprache, Historisch
 - **phi**: (Philippine languages): phi (ISO 639-5); Philippinische Sprachen [Philippine languages]: Sprachfamilie
 - **phn**: (Phoenician): phn (ISO 639-3); Phönizisch, Punisch [Phoenician]: Einzelsprache, Alt
 - **pli**: (Pali): pli (ISO 639-3), pi (ISO 639-1); Pali [Pali]: Einzelsprache, Alt
 - **pol**: (Polish): pol (ISO 639-3), pl (ISO 639-1); Polnisch [Polish]: Einzelsprache, Lebend
 - **pon**: (Pohnpeian): pon (ISO 639-3); Pohnpeanisch [Pohnpeian]: Einzelsprache, Lebend
 - **por**: (Portuguese): por (ISO 639-3), pt (ISO 639-1); Portugiesisch [Portuguese]: Einzelsprache, Lebend
 - **pra**: (Prakrit languages): pra (ISO 639-5); Prakrit [Prakrit languages]: Sprachfamilie
 - **pro**: (Provençal, Old (bis 1500); Old Occitan (bis 1500)): pro (ISO 639-3); Altokzitanisch, Altprovenzalisch [Provençal, Old (bis 1500); Old Occitan (bis 1500)]: Einzelsprache, Historisch
 - **pus**: (Pushto; Pashto): pus (ISO 639-3), ps (ISO 639-1); Paschtunisch [Pushto; Pashto]: Makrosprache, Lebend
 - **qaa-qtz**: Reserviert für lokale Nutzung, Reserved for local use
 - **que**: (Quechua): que (ISO 639-3), qwe (ISO 639-3), qu (ISO 639-1); Quechua [Quechua]: Makrosprache, Lebend
 - **raj**: (Rajasthani): raj (ISO 639-3); Rajasthani [Rajasthani]: Makrosprache, Lebend
 - **rap**: (Rapanui): rap (ISO 639-3); Rapanui [Rapanui]: Einzelsprache, Lebend
 - **rar**: (Rarotongan; Cook Islands Maori): rar (ISO 639-3); Rarotonganisch, Māori der Cookinseln [Rarotongan; Cook Islands Maori]: Einzelsprache, Lebend
 - **roa**: (Romance languages): roa (ISO 639-5); Romanische Sprachen [Romance languages]: Sprachfamilie
 - **roh**: (Romansh): roh (ISO 639-3), rm (ISO 639-1); Bündnerromanisch, Romanisch [Romansh]: Einzelsprache, Lebend
 - **rom**: (Romany): rom (ISO 639-3); Romani, Romanes [Romany]: Makrosprache, Lebend
 - **run**: (Rundi): run (ISO 639-3), rn (ISO 639-1); Kirundi [Rundi]: Einzelsprache, Lebend
 - **rup**: (Aromanian; Arumanian; Macedo-Romanian): rup (ISO 639-3); Aromunisch, Makedoromanisch [Aromanian; Arumanian; Macedo-Romanian]: Einzelsprache, Lebend
 - **rus**: (Russian): rus (ISO 639-3), ru (ISO 639-1); Russisch [Russian]: Einzelsprache, Lebend
 - **sad**: (Sandawe): sad (ISO 639-3); Sandawe [Sandawe]: Einzelsprache, Lebend
 - **sag**: (Sango): sag (ISO 639-3), sg (ISO 639-1); Sango [Sango]: Einzelsprache, Lebend
 - **sah**: (Yakut): sah (ISO 639-3); Jakutisch [Yakut]: Einzelsprache, Lebend
 - **sai**: (South American Indian (Other)): sai (ISO 639-5); Südamerikanische Sprachen [South American Indian (Other)]: Sprachfamilie
 - **sal**: (Salishan languages): sal (ISO 639-5); Salish-Sprachen [Salishan languages]: Sprachfamilie
 - **sam**: (Samaritan Aramaic): sam (ISO 639-3); Samaritanisch [Samaritan Aramaic]: Einzelsprache, Ausgestorben
 - **san**: (Sanskrit): san (ISO 639-3), sa (ISO 639-1); Sanskrit [Sanskrit]: Einzelsprache, Alt
 - **sas**: (Sasak): sas (ISO 639-3); Sasak [Sasak]: Einzelsprache, Lebend
 - **sat**: (Santali): sat (ISO 639-3); Santali [Santali]: Einzelsprache, Lebend
 - **scn**: (Sicilian): scn (ISO 639-3); Sizilianisch [Sicilian]: Einzelsprache, Lebend
 - **sco**: (Scots): sco (ISO 639-3); Scots [Scots]: Einzelsprache, Lebend
 - **sel**: (Selkup): sel (ISO 639-3); Selkupisch [Selkup]: Einzelsprache, Lebend
 - **sem**: (Semitic languages): sem (ISO 639-5); Semitische Sprachen [Semitic languages]: Sprachfamilie
 - **sga**: (Irish, Old (bis 900)): sga (ISO 639-3); Altirisch [Irish, Old (bis 900)]: Einzelsprache, Historisch
 - **sgn**: (Sign Languages): sgn (ISO 639-5); Gebärdensprache [Sign Languages]: Sprachfamilie
 - **shn**: (Shan): shn (ISO 639-3); Shan [Shan]: Einzelsprache, Lebend
 - **sid**: (Sidamo): sid (ISO 639-3); Sidama [Sidamo]: Einzelsprache, Lebend
 - **sin**: (Sinhala; Sinhalese): sin (ISO 639-3), si (ISO 639-1); Singhalesisch [Sinhala; Sinhalese]: Einzelsprache, Lebend
 - **sio**: (Siouan languages): sio (ISO 639-5); Sioux-Sprachen [Siouan languages]: Sprachfamilie
 - **sit**: (Sino-Tibetan languages): sit (ISO 639-5); Sinotibetische Sprachen [Sino-Tibetan languages]: Sprachfamilie
 - **sla**: (Slavic languages): sla (ISO 639-5); Slawische Sprachen [Slavic languages]: Sprachfamilie
 - **slv**: (Slovenian): slv (ISO 639-3), sl (ISO 639-1); Slowenisch [Slovenian]: Einzelsprache, Lebend
 - **sma**: (Southern Sami): sma (ISO 639-3); Südsamisch [Southern Sami]: Einzelsprache, Lebend
 - **sme**: (Northern Sami): sme (ISO 639-3), se (ISO 639-1); Nordsamisch [Northern Sami]: Einzelsprache, Lebend
 - **smi**: (Sami languages): smi (ISO 639-5); Samische Sprachen [Sami languages]: Sprachfamilie
 - **smj**: (Lule Sami): smj (ISO 639-3); Lulesamisch [Lule Sami]: Einzelsprache, Lebend
 - **smn**: (Inari Sami): smn (ISO 639-3); Inarisamisch [Inari Sami]: Einzelsprache, Lebend
 - **smo**: (Samoan): smo (ISO 639-3), sm (ISO 639-1); Samoanisch [Samoan]: Einzelsprache, Lebend
 - **sms**: (Skolt Sami): sms (ISO 639-3); Skoltsamisch [Skolt Sami]: Einzelsprache, Lebend
 - **sna**: (Shona): sna (ISO 639-3), sn (ISO 639-1); Shona [Shona]: Einzelsprache, Lebend
 - **snd**: (Sindhi): snd (ISO 639-3), sd (ISO 639-1); Sindhi [Sindhi]: Einzelsprache, Lebend
 - **snk**: (Soninke): snk (ISO 639-3); Soninke [Soninke]: Einzelsprache, Lebend
 - **sog**: (Sogdian): sog (ISO 639-3); Sogdisch [Sogdian]: Einzelsprache, Alt
 - **som**: (Somali): som (ISO 639-3), so (ISO 639-1); Somali [Somali]: Einzelsprache, Lebend
 - **son**: (Songhai languages): son (ISO 639-5); Songhai-Sprachen [Songhai languages]: Sprachfamilie
 - **sot**: (Sotho, Southern): sot (ISO 639-3), st (ISO 639-1); Sesotho, Süd-Sotho [Sotho, Southern]: Einzelsprache, Lebend
 - **spa**: (Spanish; Castilian): spa (ISO 639-3), es (ISO 639-1); Spanisch, Kastilisch [Spanish; Castilian]: Einzelsprache, Lebend
 - **srd**: (Sardinian): srd (ISO 639-3), sc (ISO 639-1); Sardisch [Sardinian]: Makrosprache, Lebend
 - **srn**: (Sranan Tongo): srn (ISO 639-3); Sranantongo [Sranan Tongo]: Einzelsprache, Lebend
 - **srp**: (Serbian): srp (ISO 639-3), sr (ISO 639-1); Serbisch [Serbian]: Einzelsprache, Lebend
 - **srr**: (Serer): srr (ISO 639-3); Serer [Serer]: Einzelsprache, Lebend
 - **ssa**: (Nilo-Saharan languages): ssa (ISO 639-5); Nilosaharanische Sprachen [Nilo-Saharan languages]: Sprachfamilie
 - **ssw**: (Swati): ssw (ISO 639-3), ss (ISO 639-1); Siswati [Swati]: Einzelsprache, Lebend
 - **suk**: (Sukuma): suk (ISO 639-3); Sukuma [Sukuma]: Einzelsprache, Lebend
 - **sun**: (Sundanese): sun (ISO 639-3), su (ISO 639-1); Sundanesisch [Sundanese]: Einzelsprache, Lebend
 - **sus**: (Susu): sus (ISO 639-3); Susu [Susu]: Einzelsprache, Lebend
 - **sux**: (Sumerian): sux (ISO 639-3); Sumerisch [Sumerian]: Einzelsprache, Alt
 - **swa**: (Swahili): swa (ISO 639-3), sw (ISO 639-1); Swahili [Swahili]: Makrosprache, Lebend
 - **swe**: (Swedish): swe (ISO 639-3), sv (ISO 639-1); Schwedisch [Swedish]: Einzelsprache, Lebend
 - **syc**: (Classical Syriac): syc (ISO 639-3); Syrisch [Classical Syriac]: Einzelsprache, Historisch
 - **syr**: (Syriac): syr (ISO 639-3); Nordost-Neuaramäisch [Syriac]: Makrosprache, Lebend
 - **tah**: (Tahitian): tah (ISO 639-3), ty (ISO 639-1); Tahitianisch, Tahitisch [Tahitian]: Einzelsprache, Lebend
 - **tai**: (Tai languages): tai (ISO 639-5); Tai-Sprachen [Tai languages]: Sprachfamilie
 - **tam**: (Tamil): tam (ISO 639-3), ta (ISO 639-1); Tamil [Tamil]: Einzelsprache, Lebend
 - **tat**: (Tatar): tat (ISO 639-3), tt (ISO 639-1); Tatarisch [Tatar]: Einzelsprache, Lebend
 - **tel**: (Telugu): tel (ISO 639-3), te (ISO 639-1); Telugu [Telugu]: Einzelsprache, Lebend
 - **tem**: (Timne): tem (ISO 639-3); Temne [Timne]: Einzelsprache, Lebend
 - **ter**: (Tereno): ter (ISO 639-3); Terena [Tereno]: Einzelsprache, Lebend
 - **tet**: (Tetum): tet (ISO 639-3); Tetum [Tetum]: Einzelsprache, Lebend
 - **tgk**: (Tajik): tgk (ISO 639-3), tg (ISO 639-1); Tadschikisch [Tajik]: Einzelsprache, Lebend
 - **tgl**: (Tagalog): tgl (ISO 639-3), tl (ISO 639-1); Tagalog [Tagalog]: Einzelsprache, Lebend
 - **tha**: (Thai): tha (ISO 639-3), th (ISO 639-1); Thai [Thai]: Einzelsprache, Lebend
 - **tig**: (Tigre): tig (ISO 639-3); Tigre [Tigre]: Einzelsprache, Lebend
 - **tir**: (Tigrinya): tir (ISO 639-3), ti (ISO 639-1); Tigrinya [Tigrinya]: Einzelsprache, Lebend
 - **tiv**: (Tiv): tiv (ISO 639-3); Tiv [Tiv]: Einzelsprache, Lebend
 - **tkl**: (Tokelau): tkl (ISO 639-3); Tokelauisch [Tokelau]: Einzelsprache, Lebend
 - **tlh**: (Klingon; tlhIngan-Hol): tlh (ISO 639-3); Klingonisch [Klingon; tlhIngan-Hol]: Einzelsprache, Konstruiert
 - **tli**: (Tlingit): tli (ISO 639-3); Tlingit [Tlingit]: Einzelsprache, Lebend
 - **tmh**: (Tamashek): tmh (ISO 639-3); Tuareg [Tamashek]: Makrosprache, Lebend
 - **tog**: (Tonga (Nyasa)): tog (ISO 639-3); ChiTonga [Tonga (Nyasa)]: Einzelsprache, Lebend
 - **ton**: (Tonga (Tonga Islands)): ton (ISO 639-3), to (ISO 639-1); Tongaisch [Tonga (Tonga Islands)]: Einzelsprache, Lebend
 - **tpi**: (Tok Pisin): tpi (ISO 639-3); Tok Pisin, Neuguinea-Pidgin [Tok Pisin]: Einzelsprache, Lebend
 - **tsi**: (Tsimshian): tsi (ISO 639-3); Tsimshian [Tsimshian]: Einzelsprache, Lebend
 - **tsn**: (Tswana): tsn (ISO 639-3), tn (ISO 639-1); Setswana [Tswana]: Einzelsprache, Lebend
 - **tso**: (Tsonga): tso (ISO 639-3), ts (ISO 639-1); Xitsonga [Tsonga]: Einzelsprache, Lebend
 - **tuk**: (Turkmen): tuk (ISO 639-3), tk (ISO 639-1); Turkmenisch [Turkmen]: Einzelsprache, Lebend
 - **tum**: (Tumbuka): tum (ISO 639-3); Tumbuka [Tumbuka]: Einzelsprache, Lebend
 - **tup**: (Tupi languages): tup (ISO 639-5); Tupí-Sprachen [Tupi languages]: Sprachfamilie
 - **tur**: (Turkish): tur (ISO 639-3), tr (ISO 639-1); Türkisch [Turkish]: Einzelsprache, Lebend
 - **tut**: (Altaic languages): tut (ISO 639-5); Altaische Sprachen [Altaic languages]: Sprachfamilie
 - **tvl**: (Tuvalu): tvl (ISO 639-3); Tuvaluisch [Tuvalu]: Einzelsprache, Lebend
 - **twi**: (Twi): twi (ISO 639-3), tw (ISO 639-1); Twi [Twi]: Einzelsprache, Lebend
 - **tyv**: (Tuvinian): tyv (ISO 639-3); Tuwinisch [Tuvinian]: Einzelsprache, Lebend
 - **udm**: (Udmurt): udm (ISO 639-3); Udmurtisch [Udmurt]: Einzelsprache, Lebend
 - **uga**: (Ugaritic): uga (ISO 639-3); Ugaritisch [Ugaritic]: Einzelsprache, Alt
 - **uig**: (Uighur; Uyghur): uig (ISO 639-3), ug (ISO 639-1); Uigurisch [Uighur; Uyghur]: Einzelsprache, Lebend
 - **ukr**: (Ukrainian): ukr (ISO 639-3), uk (ISO 639-1); Ukrainisch [Ukrainian]: Einzelsprache, Lebend
 - **umb**: (Umbundu): umb (ISO 639-3); Umbundu [Umbundu]: Einzelsprache, Lebend
 - **und**: (Undetermined): und (ISO 639-3); „Undefiniert“ [Undetermined]: Speziell
 - **urd**: (Urdu): urd (ISO 639-3), ur (ISO 639-1); Urdu [Urdu]: Einzelsprache, Lebend
 - **uzb**: (Uzbek): uzb (ISO 639-3), uz (ISO 639-1); Usbekisch [Uzbek]: Makrosprache, Lebend
 - **vai**: (Vai): vai (ISO 639-3); Vai [Vai]: Einzelsprache, Lebend
 - **ven**: (Venda): ven (ISO 639-3), ve (ISO 639-1); Tshivenda [Venda]: Einzelsprache, Lebend
 - **vie**: (Vietnamese): vie (ISO 639-3), vi (ISO 639-1); Vietnamesisch [Vietnamese]: Einzelsprache, Lebend
 - **vol**: (Volapük): vol (ISO 639-3), vo (ISO 639-1); Volapük [Volapük]: Einzelsprache, Konstruiert
 - **vot**: (Votic): vot (ISO 639-3); Wotisch [Votic]: Einzelsprache, Lebend
 - **wak**: (Wakashan languages): wak (ISO 639-5); Wakash-Sprachen [Wakashan languages]: Sprachfamilie
 - **wal**: (Walamo): wal (ISO 639-3); Wolaytta [Walamo]: Einzelsprache, Lebend
 - **war**: (Waray): war (ISO 639-3); Wáray-Wáray [Waray]: Einzelsprache, Lebend
 - **was**: (Washo): was (ISO 639-3); Washoe [Washo]: Einzelsprache, Lebend
 - **wen**: (Sorbian languages): wen (ISO 639-5); Sorbische Sprache [Sorbian languages]: Sprachfamilie
 - **wln**: (Walloon): wln (ISO 639-3), wa (ISO 639-1); Wallonisch [Walloon]: Einzelsprache, Lebend
 - **wol**: (Wolof): wol (ISO 639-3), wo (ISO 639-1); Wolof [Wolof]: Einzelsprache, Lebend
 - **xal**: (Kalmyk; Oirat): xal (ISO 639-3); Kalmückisch [Kalmyk; Oirat]: Einzelsprache, Lebend
 - **xho**: (Xhosa): xho (ISO 639-3), xh (ISO 639-1); isiXhosa [Xhosa]: Einzelsprache, Lebend
 - **yao**: (Yao): yao (ISO 639-3); Yao [Yao]: Einzelsprache, Lebend
 - **yap**: (Yapese): yap (ISO 639-3); Yapesisch [Yapese]: Einzelsprache, Lebend
 - **yid**: (Yiddish): yid (ISO 639-3), yi (ISO 639-1); Jiddisch [Yiddish]: Makrosprache, Lebend
 - **yor**: (Yoruba): yor (ISO 639-3), yo (ISO 639-1); Yoruba [Yoruba]: Einzelsprache, Lebend
 - **ypk**: (Yupik languages): ypk (ISO 639-5); Yupik-Sprachen [Yupik languages]: Sprachfamilie
 - **zap**: (Zapotec): zap (ISO 639-3); Zapotekisch [Zapotec]: Makrosprache, Lebend
 - **zbl**: (Blissymbols; Blissymbolics; Bliss): zbl (ISO 639-3); Bliss-Symbol [Blissymbols; Blissymbolics; Bliss]: Einzelsprache, Konstruiert
 - **zen**: (Zenaga): zen (ISO 639-3); Zenaga [Zenaga]: Einzelsprache, Lebend
 - **zgh**: (Standard Moroccan Tamazight): zgh (ISO 639-3); Marokkanisches Tamazight [Standard Moroccan Tamazight]: Einzelsprache, Lebend
 - **zha**: (Zhuang; Chuang): zha (ISO 639-3), za (ISO 639-1); Zhuang [Zhuang; Chuang]: Makrosprache, Lebend
 - **znd**: (Zande languages): znd (ISO 639-5); Zande-Sprachen [Zande languages]: Sprachfamilie
 - **zul**: (Zulu): zul (ISO 639-3), zu (ISO 639-1); isiZulu [Zulu]: Einzelsprache, Lebend
 - **zun**: (Zuni): zun (ISO 639-3); Zuñi [Zuni]: Einzelsprache, Lebend
 - **zxx**: (No linguistic content; Not applicable): zxx (ISO 639-3); „Kein sprachlicher Inhalt; Nicht anwendbar“ [No linguistic content; Not applicable]: Speziell
 - **zza**: (Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki): zza (ISO 639-3); Zazaisch [Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki]: Makrosprache, Lebend
 - **bod**: (Tibetan [Terminology Code]): tib (ISO 639-2/B), bod (ISO 639-3), bo (ISO 639-1); Tibetisch [Tibetan]: Einzelsprache, Lebend
 - **ces**: (Czech [Terminology Code]): cze (ISO 639-2/B), ces (ISO 639-3), cs (ISO 639-1); Tschechisch [Czech]: Einzelsprache, Lebend
 - **cym**: (Welsh [Terminology Code]): wel (ISO 639-2/B), cym (ISO 639-3), cy (ISO 639-1); Walisisch [Welsh]: Einzelsprache, Lebend
 - **deu**: (German [Terminology Code]): ger (ISO 639-2/B), deu (ISO 639-3), de (ISO 639-1); Deutsch [German]: Einzelsprache, Lebend
 - **ell**: (Greek, Modern (ab 1453) [Terminology Code]): gre (ISO 639-2/B), ell (ISO 639-3), el (ISO 639-1); Griechisch [Greek, Modern (ab 1453)]: Einzelsprache, Lebend
 - **eus**: (Basque [Terminology Code]): baq (ISO 639-2/B), eus (ISO 639-3), eu (ISO 639-1); Baskisch [Basque]: Einzelsprache, Lebend
 - **fas**: (Persian [Terminology Code]): per (ISO 639-2/B), fas (ISO 639-3), fa (ISO 639-1); Persisch [Persian]: Makrosprache, Lebend
 - **fra**: (French [Terminology Code]): fre (ISO 639-2/B), fra (ISO 639-3), fr (ISO 639-1); Französisch [French]: Einzelsprache, Lebend
 - **hye**: (Armenian [Terminology Code]): arm (ISO 639-2/B), hye (ISO 639-3), hy (ISO 639-1); Armenisch [Armenian]: Einzelsprache, Lebend
 - **isl**: (Icelandic [Terminology Code]): ice (ISO 639-2/B), isl (ISO 639-3), is (ISO 639-1); Isländisch [Icelandic]: Einzelsprache, Lebend
 - **kat**: (Georgian [Terminology Code]): geo (ISO 639-2/B), kat (ISO 639-3), ka (ISO 639-1); Georgisch [Georgian]: Einzelsprache, Lebend
 - **mkd**: (Macedonian [Terminology Code]): mac (ISO 639-2/B), mkd (ISO 639-3), mk (ISO 639-1); Mazedonisch [Macedonian]: Einzelsprache, Lebend
 - **mri**: (Maori [Terminology Code]): mao (ISO 639-2/B), mri (ISO 639-3), mi (ISO 639-1); Maori [Maori]: Einzelsprache, Lebend
 - **msa**: (Malay [Terminology Code]): may (ISO 639-2/B), msa (ISO 639-3), ms (ISO 639-1); Malaiisch [Malay]: Makrosprache, Lebend
 - **mya**: (Burmese [Terminology Code]): bur (ISO 639-2/B), mya (ISO 639-3), my (ISO 639-1); Birmanisch [Burmese]: Einzelsprache, Lebend
 - **nld**: (Dutch; Flemish [Terminology Code]): dut (ISO 639-2/B), nld (ISO 639-3), nl (ISO 639-1); Niederländisch, Belgisches Niederländisch [Dutch; Flemish]: Einzelsprache, Lebend
 - **ron**: (Romanian; Moldavian; Moldovan [Terminology Code]): rum (ISO 639-2/B), ron (ISO 639-3), ro (ISO 639-1); Rumänisch [Romanian; Moldavian; Moldovan]: Einzelsprache, Lebend
 - **slk**: (Slovak [Terminology Code]): slo (ISO 639-2/B), slk (ISO 639-3), sk (ISO 639-1); Slowakisch [Slovak]: Einzelsprache, Lebend
 - **sqi**: (Albanian [Terminology Code]): alb (ISO 639-2/B), sqi (ISO 639-3), sq (ISO 639-1); Albanisch [Albanian]: Makrosprache, Lebend
 - **zho**: (Chinese [Terminology Code]): chi (ISO 639-2/B), zho (ISO 639-3), zh (ISO 639-1); Chinesisch [Chinese]: Makrosprache, Lebend

---

<p style="margin-bottom:60px"></p>

<a name="d17e7691"></a>
### Term
|     |     |
| --- | --- |
| **Name** | `@term`  |
| **Datatype** | string |
| **Label** (de) | Term |
| **Description** (de) | - |
| **Contained by** |  |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6132"></a>
### URL
|     |     |
| --- | --- |
| **Name** | `@src`  |
| **Datatype** | anyURI |
| **Label** (de) | URL |
| **Description** (de) | URL bzw. Pfad zum Bild. |
| **Contained by** | [`img`](specs-elems.md#elem.img) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6713"></a>
### URL
|     |     |
| --- | --- |
| **Name** | `@url`  |
| **Datatype** | string |
| **Label** (de) | URL |
| **Description** (de) | URL zu einer Quelle, falls vorhanden. |
| **Contained by** | [`source`](specs-elems.md#elem.source) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e2640"></a>
### Verantwortliche Person
|     |     |
| --- | --- |
| **Name** | `@who`  |
| **Datatype** | IDREF |
| **Label** (de) | Verantwortliche Person |
| **Description** (de) | Die ID einer Person, die für die Änderung verantwortlich zeichnet. |
| **Contained by** | [`change`](specs-elems.md#d17e2624) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5396"></a>
### Vorzugsbenennung in originalschriftlicher Form
|     |     |
| --- | --- |
| **Name** | `@type`  |
| **Datatype** | string |
| **Label** () | Vorzugsbenennung in originalschriftlicher Form |
| **Description** () | Die angegebene Vorzugsbenennung entspricht der originalschriftlicher Form. |
| **Contained by** | [`gndo:preferredName`](specs-elems.md#person-record-preferredName) |

***Predefined Values***  

 - **original**: *no description available*

---

<p style="margin-bottom:60px"></p>

<a name="d17e1737"></a>
### Werkstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Werkstyp |
| **Label** (en) | Type of a Work |
| **Description** (de) | Typisierung des dokumentierten Werks (z.B. als Sammlung, Werk der Musik etc.). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:Work](https://d-nb.info/standards/elementset/gnd#Work). |
| **Description** (en) | Spezifies the work encoded as eg. collection, musical work etc. If no `@gndo:type` is provided the default is [gndo:Work](https://d-nb.info/standards/elementset/gnd#Work). |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#Collection)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectiveManuscript)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#Expression)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#Manuscript)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalWork)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6517"></a>
### Zieladresse (Hyperlink)
|     |     |
| --- | --- |
| **Name** | `@target`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | Zieladresse (Hyperlink) |
| **Description** (de) | Zieladresse eines Hyperlinks, repräsentiert durch einen URL nach HTTP oder HTTPS Schema. |
| **Contained by** | [`ref`](specs-elems.md#elem.ref) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(https|http)://(\S+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6761"></a>
### entityXML Store ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | string |
| **Label** (de) | entityXML Store ID |
| **Description** (de) | Identifier des Datensatzes in einem entityXML Store. |
| **Contained by** | [`store:store`](specs-elems.md#d17e6745) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e3656"></a>
### gndo:role
|     |     |
| --- | --- |
| **Name** | `@gndo:role`  |
| **Datatype** | string |
| **Contained by** | [`gndo:contributor`](specs-elems.md#d17e3635) |

***Predefined Values***  

 - **gndo:accreditedAuthor**: (Zugeschriebener Verfasser) An author, artist, etc., relating him/her to a resource for which there is or once was substantial authority for designating that person as author, creator, etc. of the work. Same as [https://d-nb.info/standards/elementset/gnd#accreditedAuthor](https://d-nb.info/standards/elementset/gnd#accreditedAuthor).
 - **gndo:accreditedComposer**: (Zugeschriebener Komponist) An author, artist, etc., relating him/her to a resource for which there is or once was substantial authority for designating that person as author, creator, etc. of the work .Same as [https://d-nb.info/standards/elementset/gnd#accreditedComposer](https://d-nb.info/standards/elementset/gnd#accreditedComposer)
 - **gndo:addressee**: (Adressat) A person, family, or organization to whom the correspondence in a work is addressed. Same as [https://d-nb.info/standards/elementset/gnd#addressee](https://d-nb.info/standards/elementset/gnd#addressee)
 - **gndo:annotator**: (Annotator) A person who makes manuscript annotations on an item. Same as [https://d-nb.info/standards/elementset/gnd#annotator](https://d-nb.info/standards/elementset/gnd#annotator)
 - **gndo:arranger**: (Arrangeur) A person, family, or organization contributing to a musical work by rewriting the composition for a medium of performance different from that for which the work was originally intended, or modifying the work for the same medium of performance, etc., such that the musical substance of the original composition remains essentially unchanged. For extensive modification that effectively results in the creation of a new musical work, see composer. Same as [https://d-nb.info/standards/elementset/gnd#arranger](https://d-nb.info/standards/elementset/gnd#arranger)
 - **gndo:author**: (Verfasser) A person, family, or organization responsible for creating a work that is primarily textual in content, regardless of media type (e.g., printed text, spoken word, electronic text, tactile text) or genre (e.g., poems, novels, screenplays, blogs). Use also for persons, etc., creating a new work by paraphrasing, rewriting, or adapting works by another creator such that the modification has substantially changed the nature and content of the original or changed the medium of expression.Same as [https://d-nb.info/standards/elementset/gnd#author](https://d-nb.info/standards/elementset/gnd#author)
 - **gndo:bookbinder**: (Buchbinder) A person who binds an item. Same as [https://d-nb.info/standards/elementset/gnd#bookbinder](https://d-nb.info/standards/elementset/gnd#bookbinder)
 - **gndo:bookdesigner**: (Buchgestalter) A person or organization involved in manufacturing a manifestation by being responsible for the entire graphic design of a book, including arrangement of type and illustration, choice of materials, and process used. Same as [https://d-nb.info/standards/elementset/gnd#bookdesigner](https://d-nb.info/standards/elementset/gnd#bookdesigner)
 - **gndo:cartographer**: (Kartograf) A person, family, or organization responsible for creating a map, atlas, globe, or other cartographic work. Same as [https://d-nb.info/standards/elementset/gnd#cartographer](https://d-nb.info/standards/elementset/gnd#cartographer)
 - **gndo:choreographer**: (Choreograf) A person responsible for creating or contributing to a work of movement. Same as [https://d-nb.info/standards/elementset/gnd#choreographer](https://d-nb.info/standards/elementset/gnd#choreographer).
 - **gndo:citedAuthor**: (Zitierter Verfasser) A person or organization whose work is largely quoted or extracted in works to which he or she did not contribute directly. Such quotations are found particularly in exhibition catalogs, collections of photographs, etc. Same as [https://d-nb.info/standards/elementset/gnd#citedAuthor](https://d-nb.info/standards/elementset/gnd#citedAuthor).
 - **gndo:citedComposer**: (Zitierter Komponist) A person or organization whose work is largely quoted or extracted in works to which he or she did not contribute directly. Such quotations are found particularly in exhibition catalogs, collections of photographs, etc. Same as [https://d-nb.info/standards/elementset/gnd#citedComposer](https://d-nb.info/standards/elementset/gnd#citedComposer).
 - **gndo:compiler**: (Kompilator) A person, family, or organization responsible for creating a new work (e.g., a bibliography, a directory) through the act of compilation, e.g., selecting, arranging, aggregating, and editing data, information, etc. Same as [https://d-nb.info/standards/elementset/gnd#compiler](https://d-nb.info/standards/elementset/gnd#compiler).
 - **gndo:composer**: (Komponist) A person, family, or organization responsible for creating or contributing to a musical resource by adding music to a work that originally lacked it or supplements it. Same as [https://d-nb.info/standards/elementset/gnd#composer](https://d-nb.info/standards/elementset/gnd#composer).
 - **gndo:conferrer**: (Leihgeber) A person or organization permitting the temporary use of a book, manuscript, etc., such as for photocopying or microfilming. Same as [https://d-nb.info/standards/elementset/gnd#conferrer](https://d-nb.info/standards/elementset/gnd#conferrer).
 - **gndo:copist**: (Kopist) A person or family who is known as scribe or copyist. Same as [https://d-nb.info/standards/elementset/gnd#copist](https://d-nb.info/standards/elementset/gnd#copist). Same as [https://d-nb.info/standards/elementset/gnd#copist](https://d-nb.info/standards/elementset/gnd#copist)
 - **gndo:creator**: (Urheber) A person or organization performing the work, i.e., the name of a person or organization associated with the intellectual content of the work. This category does not include the publisher or personal affiliation, or sponsor except where it is also the corporate author. Same as [https://d-nb.info/standards/elementset/gnd#creator](https://d-nb.info/standards/elementset/gnd#creator).
 - **gndo:designer**: (Designer) A person, family, or organization responsible for creating a design for an object. Same as [https://d-nb.info/standards/elementset/gnd#designer](https://d-nb.info/standards/elementset/gnd#designer).
 - **gndo:director**: (Regisseur) A person responsible for the general management and supervision of a filmed performance, a radio or television program, etc. Same as [https://d-nb.info/standards/elementset/gnd#director](https://d-nb.info/standards/elementset/gnd#director).
 - **gndo:directorOfPhotography**: (Verantwortlicher Kameramann) A person in charge of photographing a motion picture, who plans the technical aspets of lighting and photographing of scenes, and often assists the director in the choice of angles, camera setups, and lighting moods. He or she may also supervise the further processing of filmed material up to the completion of the work print. Cinematographer is also referred to as director of photography. Do not confuse with videographer. Same as [https://d-nb.info/standards/elementset/gnd#directorOfPhotography](https://d-nb.info/standards/elementset/gnd#directorOfPhotography).
 - **gndo:doubtfulAuthor**: (Angezweifelter Verfasser) A person or organization to which authorship has been dubiously or incorrectly ascribed. Same as [https://d-nb.info/standards/elementset/gnd#doubtfulAuthor](https://d-nb.info/standards/elementset/gnd#doubtfulAuthor).
 - **gndo:doubtfulComposer**: (Angezweifelter Komponist) A person or organization to which authorship has been dubiously or incorrectly ascribed. Same as [https://d-nb.info/standards/elementset/gnd#doubtfulComposer](https://d-nb.info/standards/elementset/gnd#doubtfulComposer).
 - **gndo:editor**: (Herausgeber) A person, family, or organization contributing to a resource by revising or elucidating the content, e.g., adding an introduction, notes, or other critical matter. An editor may also prepare a resource for production, publication, or distribution. For major revisions, adaptations, etc., that substantially change the nature and content of the original work, resulting in a new work, see author. Same as [https://d-nb.info/standards/elementset/gnd#editor](https://d-nb.info/standards/elementset/gnd#editor).
 - **gndo:engraver**: (Graveur) A person or organization who cuts letters, figures, etc. on a surface, such as a wooden or metal plate used for printing. Same as [https://d-nb.info/standards/elementset/gnd#engraver](https://d-nb.info/standards/elementset/gnd#engraver).
 - **gndo:etcher**: (Radierer) A person or organization who produces text or images for printing by subjecting metal, glass, or some other surface to acid or the corrosive action of some other substance. Same as [https://d-nb.info/standards/elementset/gnd#etcher](https://d-nb.info/standards/elementset/gnd#etcher).
 - **gndo:fictitiousAuthor**: (Fiktiver Verfasser) A fictitious person, family, or corporate body ascertained to be the author. Same as [https://d-nb.info/standards/elementset/gnd#fictitiousAuthor](https://d-nb.info/standards/elementset/gnd#fictitiousAuthor).
 - **gndo:firstAuthor**: (Erste Verfasserschaft) A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators. Same as [https://d-nb.info/standards/elementset/gnd#firstAuthor](https://d-nb.info/standards/elementset/gnd#firstAuthor).
 - **gndo:firstComposer**: (Erster Komponist) A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators. Same as [https://d-nb.info/standards/elementset/gnd#firstComposer](https://d-nb.info/standards/elementset/gnd#firstComposer).
 - **gndo:illustratorOrIlluminator**: (Illustrator oder Illuminator) A person, family, or organization contributing to a resource by supplementing the primary content with drawings, diagrams, photographs, etc. If the work is primarily the artistic content created by this entity, use artist or photographer. Same as [https://d-nb.info/standards/elementset/gnd#illustratorOrIlluminator](https://d-nb.info/standards/elementset/gnd#illustratorOrIlluminator).
 - **gndo:instrumentalist**: (Instrumentalmusiker) A performer contributing to a resource by playing a musical instrument. Same as [https://d-nb.info/standards/elementset/gnd#instrumentalist](https://d-nb.info/standards/elementset/gnd#instrumentalist).
 - **gndo:librettist**: (Librettist) An author of a libretto of an opera or other stage work, or an oratorio. Same as [https://d-nb.info/standards/elementset/gnd#librettist](https://d-nb.info/standards/elementset/gnd#librettist).
 - **gndo:lithographer**: (Litograf) A person or organization who prepares the stone or plate for lithographic printing, including a graphic artist creating a design directly on the surface from which printing will be done. Same as [https://d-nb.info/standards/elementset/gnd#lithographer](https://d-nb.info/standards/elementset/gnd#lithographer).
 - **gndo:narrator**: (Sprecher) A performer contributing to a resource by reading or speaking in order to give an account of an act, occurrence, course of events, etc. Same as [https://d-nb.info/standards/elementset/gnd#narrator](https://d-nb.info/standards/elementset/gnd#narrator).
 - **gndo:painter**: (Maler) A person or family who paints. Same as [https://d-nb.info/standards/elementset/gnd#painter](https://d-nb.info/standards/elementset/gnd#painter).
 - **gndo:photographer**: (Fotograf) A person, family, or organization responsible for creating a photographic work. Same as [https://d-nb.info/standards/elementset/gnd#photographer](https://d-nb.info/standards/elementset/gnd#photographer).
 - **gndo:poet**: (Dichter) An author of the words of a non-dramatic musical work (e.g. the text of a song), except for oratorios. Same as [https://d-nb.info/standards/elementset/gnd#poet](https://d-nb.info/standards/elementset/gnd#poet).
 - **gndo:printer**: (Drucker) A person, family, or organization involved in manufacturing a manifestation of printed text, notated music, etc., from type or plates, such as a book, newspaper, magazine, broadside, score, etc. Same as [https://d-nb.info/standards/elementset/gnd#printer](https://d-nb.info/standards/elementset/gnd#printer).
 - **gndo:revisor**: (Bearbeiter) A person or organization who 1) reworks a musical composition, usually for a different medium, or 2) rewrites novels or stories for motion pictures or other audiovisual medium. Same as [https://d-nb.info/standards/elementset/gnd#revisor](https://d-nb.info/standards/elementset/gnd#revisor).
 - **gndo:screenwriter**: (Drehbuchautor) An author of a screenplay, script, or scene. Same as [https://d-nb.info/standards/elementset/gnd#screenwriter](https://d-nb.info/standards/elementset/gnd#screenwriter).
 - **gndo:singer**: (Sänger) A performer contributing to a resource by using his/her/their voice, with or without instrumental accompaniment, to produce music. A singer's performance may or may not include actual words. Same as [https://d-nb.info/standards/elementset/gnd#singer](https://d-nb.info/standards/elementset/gnd#singer).
 - **gndo:subeditor**: (Redakteur) A person or organization who writes or develops the framework for an item without being intellectually responsible for its content. Same as [https://d-nb.info/standards/elementset/gnd#subeditor](https://d-nb.info/standards/elementset/gnd#subeditor).
 - **gndo:writerOfAddedCommentary**: (Kommentator (schriftlich)) A person or organization responsible for the commentary or explanatory notes about a text. For the writer of manuscript annotations in a printed book, use Annotator. Same as [https://d-nb.info/standards/elementset/gnd#writerOfAddedCommentary](https://d-nb.info/standards/elementset/gnd#writerOfAddedCommentary)

---

<p style="margin-bottom:60px"></p>

<a name="d17e3541"></a>
### gndo:type
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Contained by** | [`gndo:broaderTerm`](specs-elems.md#d17e3502) |

***Predefined Values***  

 - **general**: (Oberbegriff allgemein) Broader term . Same as [https://d-nb.info/standards/elementset/gnd#broaderTermGeneral](https://d-nb.info/standards/elementset/gnd#broaderTermGeneral).
 - **generic**: (Oberbegriff generisch) The generic relation is a semantic relation between two concepts where the intension of one of the concepts includes that of the other concepts and at least one additional delimiting characteristic is added. Same as [https://d-nb.info/standards/elementset/gnd#broaderTermGeneric](https://d-nb.info/standards/elementset/gnd#broaderTermGeneric).
 - **instantial**: (Oberbegriff instantiell) The instance relationship links a general concept such as a class of things or events, and an individual instance oft hat class, which is often represented by a proper name. Same as [https://d-nb.info/standards/elementset/gnd#broaderTermInstantial](https://d-nb.info/standards/elementset/gnd#broaderTermInstantial).
 - **partitive**: (Oberbegriff partitiv) The hierarchical whole-part relationship covers a limited range of situations in which a part of an entity or system belongs uniquely to a particular possessing whole. When applied to persons, this is the relation between a single person (particularly gods) and hierarchically broader groups of gods and mythologic entities. Same as [https://d-nb.info/standards/elementset/gnd#broaderTermPartitive](https://d-nb.info/standards/elementset/gnd#broaderTermPartitive).
 - **with-more-than-one-element**: (Oberbegriff mehrgliedrig) Broader term (with more than one element). Same as [https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement](https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement).

---

<p style="margin-bottom:60px"></p>

<a name="d17e6815"></a>
### name
|     |     |
| --- | --- |
| **Name** | `@name`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](specs-elems.md#d17e6801) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6817"></a>
### timestamp
|     |     |
| --- | --- |
| **Name** | `@timestamp`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](specs-elems.md#d17e6801) |

---

<p style="margin-bottom:60px"></p>

