
<a name="custom-namespaces"></a>
# Verwendung fremder Namensräume

Bevor wir das allgemeine Element- und Attributsset eines Entitätseintrags in entityXML durchgehen, widmen wir uns kurz der Frage "Was, wenn ich Informationen dokumentieren möchte, die nicht im Schema spezifiziert sind?".

entityXML verhält sich als Format in Hinblick auf die Auszeichnung von Informationen innerhalb aller Entitätseinträge ([`entity`](specs-elems.md#entity-record) und alle seine Unterklassen wie z.B. [`person`](specs-elems.md#person-record) oder [`place`](specs-elems.md#d17e1265)) <span class="emph">offen</span>: Die Spezifikation macht zwar gewisse Vorgaben in Hinblick auf Elemente aus der GNDO und anderen Formaten, sowie diesbezügliche Validierungsregeln; nichtdestoweniger erlaubt es ausdrücklich Elemente und Attribute aus anderen Namensräumen (sog. *custom elements* bzw. *attributes*), [die nicht im
                        Rahmen der entityXML Spezifikation definiert sind](entityxml-dm-intro.md) und somit als <span class="emph">spezifikationsfremde Namensräume</span> gelten.

Dementsprechend sind folgende Elemente von der Verwendung als Custom Elements innerhalb von Entitätseinträgen ausgeschlossen, da sie fester Bestandteil der entityXML Spezifikation sind:

**Elemente:**



- http://id.loc.gov/ontologies/bibframe/**instanceOf**
- http://xmlns.com/foaf/0.1/**page**
- http://purl.org/dc/elements/1.1/**title**
- http://www.w3.org/2004/02/skos/core#**note**
- http://www.w3.org/2002/07/owl#**sameAs**
- http://www.opengis.net/ont/geosparql#**hasGeometry**
- http://www.w3.org/2003/01/geo/wgs84_pos#**lat**
- http://www.w3.org/2003/01/geo/wgs84_pos#**long**

**Generell alle Elemente aus folgenden Namensräumen:**



- https://sub.uni-goettingen.de/met/standards/entity-xml#
- https://sub.uni-goettingen.de/met/standards/entity-store#
- https://d-nb.info/standards/elementset/gnd#

<a name="docs_d14e1451"></a>
## Von der einfachen Anwendung ...

Die Implementierung von entityXML ist so angelegt, dass es als Speicherformat von (Forschungs- )projekten verwendet werden kann, um Entitätsbeschreibungen als Forschungsdaten so frei wie möglich und so restriktiv wie nötig (aus Perspektive der späteren Datenverarbeitung durch eine GND Agentur) zu dokumentieren und zu nutzen.

Um *custom elements* oder *attributes* in einem entityXML Entitätseintrag zu verwenden, muss zunächst ein entsprechender beliebiger Namensraum definiert werden, der nicht durch die vorliegende Spezifikation ausgeschlossen wird (siehe oben). Für gewöhnlich bietet sich der Wurzelknoten [`entityXML`](specs-elems.md#entity-xml) dafür an:

```xml
<entityXML 
   xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
   xmlns:example="http://example.com/irgendein-custom-namespace">
   <!-- ... -->
</entityXML>
```

Nichtsdestoweniger kann der bzw. die Namespaces auch **direkt am jeweiligen Entitätselement** definiert werden:

```xml
<entity xml:id="max_muster" xmlns:example="http://example.com/mein-namespace">
   <skos:note>Eine fiktive Person, die wahrescheinlich mal einen Finger verloren hat.</skos:note>
   <source>Eine kleine Karte in meiner neuen Geldbörse</source>
   <dc:title>Mustermann, Max</dc:title>
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref>
   <example:finger-anzahl>9</example:finger-anzahl>
</entity>
```

So ist es möglich, für jede Entität jegliche Art von Datum zu dokumentieren - je nach den eigenen Anforderungen, die an die Daten gestellt werden.

<a name="docs_d14e1484"></a>
## ... hin zum Mittelweg ...

In der Regel dienen *custom elements* dazu, zusätzliche Daten in Einträgen zu dokumentieren, **die in der GND als Normdatei keinen Platz finden** und dementsprechen auch nicht über die GNDO beschrieben werden können.

Diese Verwendungsweise von *custom elements* als "Erweiterungen" der GNDO Beschreibung um (projektspezifische) zusätzliche Daten verstehen wir als "Goldenen Weg" bzw. den optimalen Mittelweg:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363" 
   xmlns:social="http://social-media.statistics.de/kennzahlen">
   <gndo:preferredName>
     <gndo:surname>Nguyen-Kim</gndo:surname>, 
     <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <social:twitter when="2023-01-11" 
      follower="450602" 
      joined="2013-01-01">maithi_nk</social:twitter>
   <social:youtube when="2023-01-11" 
      follower="1480000" 
      joined="2016-09-08">maiLab</social:youtube>
</person>
```

Dieses Beispiel erweitert einen Personeneintrag um Angaben zu Social-Media Accounts und diesbezügliche Kennzahlen wie Anzahl der Follower, Beitrittsdatum und Account-ID. Diese Daten können dann z.B. als Grundlage eigener statistischer Analysen dienen.

<a name="lido"></a>
## ... und auf die Spitze getrieben!

Im Prinzip kann man dieses Konzept auf die Spitze treiben und z.B. einen kompletten <span class="emph">LIDO</span> Datensatz in einem [`entity`](specs-elems.md#entity-record) Eintrag (und nur hier) unterbringen:

```xml
<entity xml:id="lido00099" gndo:type="https://d-nb.info/standards/elementset/gnd#Work">
     <lido:lido xmlns="http://www.lido-schema.org"
         xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:skos="http://www.w3.org/2004/02/skos/core#"
         xmlns:lido="http://www.lido-schema.org">
         <lido:lidoRecID lido:source="Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg" lido:type="http://terminology.lido-schema.org/lido00100">DE-Mb112/lido-obj00154983</lido:lidoRecID>
         <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099" lido:pref="http://terminology.lido-schema.org/lido00170">https://www.uffizi.it/opere/botticelli-primavera</lido:objectPublishedID>
         <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099" lido:pref="http://terminology.lido-schema.org/lido00170">https://d-nb.info/gnd/4069615-7</lido:objectPublishedID>
         <lido:category>
             <skos:Concept rdf:about="http://terminology.lido-schema.org/lido00096">
                 <skos:prefLabel xml:lang="en">Human-made object</skos:prefLabel>
                 <skos:exactMatch>http://vocab.getty.edu/aat/300386957</skos:exactMatch>
                 <skos:exactMatch>http://erlangen-crm.org/current/E22_Human-Made_Object</skos:exactMatch>
             </skos:Concept>
             <lido:term lido:addedSearchTerm="yes">man-made object</lido:term>
         </lido:category>
         <!-- ... -->
     </lido:lido>
 </entity>
```

Das wird durch die besondere Implementierung des `entity` Elements ermöglicht, das zwei mögliche Optionen der Beschreibung von Entitäten zulässt: (1) Die Beschreibung einer Entität gemäß des im entityXML Schema spezifizierten Standardsets von Elementen und Attributen oder (2) die Beschreibung einer Entität durch ein beliebiges Set an Elementen und Attributen, die nicht den Namensräumen <https://sub.uni-goettingen.de/met/standards/entity-xml#>, <https://sub.uni-goettingen.de/met/standards/entity-store#> und <https://d-nb.info/standards/elementset/gnd#> angehören.

> **Warum ist bei Option 2 der GNDO Namespace ausgeschlossen?**  
> Berechtigte Frage! Weil für Daten, die mithilfe der GNDO Terme beschrieben werden, die in diesem Schema implementierten Entitätsklassen gedacht sind. Für diese ist spezifisch geregelt, welche Daten mit welchen GNDO-Beschreibungskategorien erschlossen werden können bzw. sollen. Von diesem Standpunkt aus betrachtet, ergibt es keinen Sinn, dass in `entity` andere GNDO Elemente verwendet werden (können), als die, die bereits im Schema spezifiziert werden. Wenn die Beschreibung einer Entität also gemäß der GNDO erfolgen soll, dann kann man die entsprechenden Eintragselemente verwenden und sollte nicht auf `entity` ausweichen.


Im abgebildeten Beipiel wird also der extremste Weg eingeschlagen, der in entityXML möglich ist: Die **vollständige Integration eines fremden XML-Formats** (sofern es nicht den Namespace der GND oder die beiden entityXML Namespaces integriert).

`@gndo:type` dient der Identifikation des Entitätstyps, der durch das fremde Format beschrieben wird. In unserem Beispiel handelt es sich um ein Gemälde, was sich grob als `gndo:Work` fassen lässt.

<a name="docs_d14e1539"></a>
## Arbeiten mit schemafremden Elementen im Author Mode

oXygen XML arbeitet im Author Mode mit einem hilfreichen Werkzeug, dass sich ["Content Completion Assistant"](https://www.oxygenxml.com/doc/versions/25.0/ug-editor/topics/content-completion-author-mode.html#content-completion-author-mode) nennt. Hierbei handelt sich um ein Eingabeinterface für Elemente, das in seiner Standardeinstellung nur die Eingabe von schema-bekannten Elementen erlaubt.

Um die Eingabe schemafremder Elemente zu erlauben geht man in die Einstellungen `Editor > Bearbeitungsmodus > Autor > Schemabewusst` und entfernt den Haken bei "Nur das Einfügen gültiger Elemente und Attribute erlauben".

<a name="docs_d14e1552"></a>
## Verfahrensweise mit Daten aus fremden Namensräumen bei der Text+ GND-Agentur

Daten, die mit Hilfe von *custom elements* erschlossen werden, werden in Hinblick auf die Anlage neuer Einträge bzw. die Anreicherung bereits bestehender Einträge in der GND ignoriert. Bei einer Verarbeitung von entityXML Ressourcen durch die Text+ GND Agentur werden ausschließlich die Elemente betrachtet, die fester Bestandteil der Spezifikation sind. 

Nichtsdestoweniger bleiben die Daten in *custom elements* in den Einträgen erhalten. Sie sind weiterhin Bestandteile des projektinternen Forschungsdatenlebenszyklus, können in diesem Rahmen versioniert und in andere Informationsprozesse eingebracht werden.
