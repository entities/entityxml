
<a name="entity-title"></a>
# Namens- bzw. Titelangaben

Eine Entität hat in der Regel einen Namen, durch den dann Bezug auf sie genommen werden kann. In entityXML werden drei Oberkategorien von Namen unterschieden: (1) Vorzugsbezeichnung, (2) Alternative Namensform bzw. Namensvariante und (3) (Ressourcen)titel.

Eine **Vorzugsbenennung** definiert den hauptsächlich gebräuchlichen, also den <span class="emph">bevorzugten Namen,</span> mit dem auf eine Entität bezug genommen wird. Hierfür wird das Element [``gndo:preferedName``](specs-elems.md#person-record-preferredName) verwendet, wobei sich optional die verwendete Sprache mit `@xml:lang` spezifizieren lässt; default ist hier Deutsch:

```xml
<place xml:id="santiago-de-chile">
   <gndo:preferredName xml:lang="es">Santiago de Chile</gndo:preferredName>
</place>
```

**Namensvarianten** bzw. Alternative Namensformen werden via [``gndo:variantName``](specs-elems.md#elem.gndo.variantName.standard) erfasst. Hierbei handelt es sich um <span class="emph">zusätzliche Namensformen</span>, unter denen die Entität ebenfalls bekannt ist, sofern vorhanden. Das können Spitz- oder Kosenamen sein, genauso wie Rufnamen. Wie bei `gndo:preferedName` kann die verwendete Sprache einer Namensvariante durch `@xml:lang` spezifiziert werden:

```xml
<place xml:id="santiago-de-chile">
   <!-- ... -->
   <gndo:variantName>Santiago</gndo:variantName>
   <gndo:variantName>Sanhatten</gndo:variantName>
   <skos:note>"Sanhatten", weil Santiago de Chile vom Cerro San Cristóbal wie Manhatten aussieht.</skos:note>
</place>
```

Vorzugsbenennungen und Namensvarianten werden bei jedem Entitätstyp leicht unterschiedlich gehandhabt, sowohl, was mögliche Inhalte als auch die Validierung betrifft. Dementsprechend lohnt sich ein Blick an die entsprechenden Stellen in diesem Handbuch:


- [Erschließung von Namen bei Personen](person-record.md#person-name)

<a name="docs_d14e1853"></a>
## Titel als Fallback

In entityXML gibt es noch die Möglichkeit einen **Fallback Titel** für eine Entität mittels [`dc:title`](specs-elems.md#d17e3011) zu vergeben, falls aus irgendwelchen Gründen keine Vorzugsbenennung oder Namensvariante dokumentiert werden soll. Mögliche Fälle wären z.B. Einträge, die bereits in der GND vorhanden sind, und für die man in seinen eigenen Einträgen keine eigene Namensformen anführen möchte:

```xml
<place xml:id="goslar_rathaus" gndo:uri="http://d-nb.info/gnd/4231845-2">
   <dc:title>Rathaus Goslar</dc:title>
</place>
```
