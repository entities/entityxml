
<a name="lifecycle"></a>
# entityXML Lifecycle

Bevor wir weiter über die möglichen Metadaten einer entityXML Datensammlung reden, sollten wir einen Schritt zurück gehen und uns zunächst das <span class="emph">Lifecyclekonzept</span> von entityXML und das hiermit in Zusammenhang stehende Workflowmodell aus Sicht der Text+ GND-Agentur näher ansehen:

![markdown/docs/img/revision-workflow.png](img/revision-workflow.png)

Der prototypische entityXML Workflow orientiert sich an drei grundlegenden Phasen, die sowohl von einer Datensammlung als auch von jedem Eintrag einzeln durchlaufen werden können. Im Folgenden spreche ich daher von <span class="emph">Informationsressource</span> und meine damit sowohl eine entityXML `collection` als auch die einzelnen definierten Typen von Eintitätseinträgen (näheres hierzu weiter unten):

**(1) Eröffnung und Bearbeitung durch den potenziellen Datenlieferanten**: Hierbei handelt es sich um die Anfangsphase einer Informationsressource, die damit als <span class="emph">eröffnet</span> ("opened") gilt: Ein Projekt, eine Forschergruppe oder einfach Irgendwer, der gerne Entitäten dokumentiert, legt in diesem Stadium eine entityXML Datei an und beginnt eine Sammlung mit Einträgen zu füllen. Diese Einträge – und damit auch die Sammlung – befindet sich somit in der ständigen Bearbeitung, d.h. Hintergründe und Informationen werden recherchiert und in den entsprechenden Einträgen dokumentiert. Im besten Falle werden die entityXML Daten, die so entstehen, von dem Projekt oder der Person schon in einem eigenen Informationssystem verarbeitet und individuell genutzt.

> **'Bedenke die Möglichkeiten'**  
> An dieser Stelle möchte ich erwähnen, dass der Workflow einzig und allein davon abhängt, was man mit den Daten am Ende machen möchte! Man kann entityXML auch benutzen, ohne überhaupt jemals einen Austausch mit der GND in Erwägung zu ziehen. Genausogut kann eine entityXML auch maschienell als <span class="emph">Nebenprodukt</span> erzeugt werden, sprich: Die Daten werden primär in einem ganz anderen System erschlossen und entityXML dient lediglich als Exportformat, das dann wiederum an eine GND-Agentur geht. Im Prinzip sind der Phantasie hier keine Grenzen gesetzt. Zumindest aus Sicht der Text+ GND-Agentur stellt sich der Workflow so dar, wie oben gezeigt!


**(2) Überprüfung und Bearbeitung durch die Text+ GND-Agentur**: In dieser zweiten Phase befindet sich eine Informationsressource auf dem Prüfstand ("staged") bei der Text+ GND-Agentur, sprich: Die Informationsressource wird daraufhin geprüft, ob die geltenden Qualitätskriterien erfüllt sind. Sollte diese Prüfung negativ ausfallen, wird die Agentur von Fall zu Fall entscheiden, (1) ob die Daten durch Transformationen mit angemessenem Aufwand entsprechend angepasst werden können oder, (2) ob eine erneute Bearbeitung durch den verantwortlichen Datenlieferanten erforderlich ist. Festgestellte Probleme in und Rückfragen zu den Daten werden in jedem Fall durch die Agentur direkt an den Daten dokumentiert und an den Datenlieferanten zurückgemeldet. In dem Moment, in dem die Daten die Qualitätskriterien erfüllen, tritt die Informationsressource in das dritte Stadium ein.

**(3) Abschluss der Bearbeitung durch die Text+ GND-Agentur**: Diese dritte und letzte Phase markiert das Ende der Bearbeitung einer Informationsressource, die nun die entsprechenden Qualitätskriterien erfüllt und somit abschließend durch die Text+ GND-Agentur in die GND eingespielt, d.h. veröffentlicht werden kann: GND-URIs für neue Einträge werden erstellt und an den entsprechenden Stellen in der entityXML Datei dokumentiert, mögliche Änderungen wurden an Normdateneinträgen vorgenommen etc. Als letzte Handlung wird der Datenlieferant über den Abschluss benachrichtigt, indem er die aktualisierte und nun als <span class="emph">abgeschlossen</span> ("closed") geltende entityXML Ressource, angereichert mit den entsprechenden Information, zurück erhält.

> Die zweite und dritte Phase ist strukturell vergleichbar mit einem Prozesses, den man in der Informationsverarbeitung auch als [ETL](https://de.wikipedia.org/wiki/ETL-Prozess) ("Extract, Transform, Load") kennt: In ETL Prozessen werden heterogene Quelldaten zuerst extrahiert und für anschließende Transformationsschritte aufbereitet. In der Folge werden die Daten durch unterschiedliche Transformationen in ein definiertes Zielformat konvertiert, dass dann letztlich in ein definiertes Zielsystem eingespielt ("geladen") wird. 


Innerhalb dieser drei Phasen kann eine Sammlung bzw. ein Eintrag acht unterschiedliche <span class="emph">Bearbeitungsstadien</span> durchlaufen. Diese lassen sich als einzelne Entwicklungsschritte einer Ressource in ihrem Lebenszyklus beschreiben – von dem Moment der Erstellung an bis zur Publikation:

![markdown/docs/img/revision-steps.png](img/revision-steps.png)

Eine Erklärung zu den hier abgebildeten acht Stadien bleibe ich noch für einen kurzen Moment schuldig; sie folgt in wenigen Absätzen. Bis hierher ging es vorallem darum, einen Eindruck davon zu gewinnen, wie der Lebenszyklus einer entityXML Ressource aufgebaut ist (bzw. sein kann) und wie sich die Text+ GND-Agentur hierauf aufbauend einen Verarbeitungsworkflow definiert hat.

<a name="revision-desc"></a>
## Revisionsbeschreibung

Bleibt die Frage, wie der geschilderte Lebenszyklus in den Daten dokumentiert wird? Dafür dient die <span class="emph">Revisionsbeschreibung</span>, die durch das Element [`revision`](specs-elems.md#revision) repräsentiert wird.

Revisionsbeschreibungen sind für Sammlungen ([`collection`](specs-elems.md#collection)) als auch für Entitätseinträge ([`entity`](specs-elems.md#entity-record), [`person`](specs-elems.md#person-record), [`place`](specs-elems.md#d17e1265) usw.) implementiert. Für Sammlungen ist `revision` obligatorisch! Für Einträge ist `revision` solange optional, bis ein Eintrag explizit für die Bearbeitung durch eine GND-Agentur vorgesehen wird (durch die Verwendung von `@agency` angezeigt, aber mehr dazu [später](agency-mode-record.md)). Als <span class="emph">Best Practice</span> gilt jedenfalls, dass jeder Eintrag von Anfang an eine konsistente Revisionsbeschreibung nachweisen kann. 

Eine Revisionsbeschreibung besteht im Kern aus zwei Kerninformationen: Der momentanen <span class="emph">Bearbeitungsphase</span> (siehe oben), in dem sich die Sammlung oder der Entitätseintrag befindet, sowie mindestens einer, aber in der Regel mehrere <span class="emph">Änderungseinträge</span>, die einzelne, vorgenommene Änderungen oder Arbeitsschritte dokumentieren.

Die Bearbeitungsphase, also ob sich die Sammlung oder der Eintrag in der anfänglichen Bearbeitungsphase ("**opened**") oder schon auf dem Prüfstand einer GND-Agentur ("**staged**") befindet oder bereits fertig bearbeitet und abgeschlossen ("**closed**") ist, wird in [`@status`](specs-attrs.md#rev-status) angegeben.

Änderungseinträge werden durch [`change`](specs-elems.md#d17e2624) Elemente innerhalb von `revision` angelegt und dienen der knappen und präzisen textlichen Dokumentation von Änderungen, Erweiterungen oder anderer Arbeiten, die an einer Sammlung oder einem Eintrag vorgenommen wurden. Zusätzlich verzeichnet ein Änderungseintrag das Datum, an dem er angelegt wurde via `@when` als ISO 8601 sowie die ID der verantworlich zeichnenden Person via `@who`:

```xml
<revision status="opened">
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen.
   </change>
</revision>
```

> **Who?**  
> Die verantwortlich zeichnende Person muss selbstverständlich vorher bei den bearbeitenden Personen in der [Providerbeschreibung](metadata-section.md#provider-stmt) definiert werden.


**Achtung**: Änderungseinträge werden immer von oben nach unten sortiert, ausgehend von dem jüngsten Eintrag, sprich: Der Eintrag mit dem aktuellsten Datum befindet sich immer an erster Stelle in der Revisionsbeschreibung!

Ein Änderungseintrag hat wie schon das übergeordnete `revision` Element ein [`@status`](specs-attrs.md#change-status) Attribut. Hier kommen nun die oben bereits angesprochenen <span class="emph">acht Bearbeitungsstadien</span> ins Spiel, die auf diese Weise in der Revisionsbeschreibung angegeben werden <span class="emph">können</span>. "Können" heißt: Nicht jeder Änderungseintrag muss ein Bearbeitungsstadium ausweisen, aber wenn z. B. besondere Meilsensteine anstehen (etwa der Tag, an dem die entityXML an die Text+ GND-Agentur geschickt wird), sollte zumindest der aktuellste Eintrag ein explizites Bearbeitungsstadium dokumentieren:

```xml
<revision status="opened">
   <change when="2023-01-05" who="MM" status="candidate">
      Bearbeitung abgeschlossen und an die Text+ GND-Agentur geschickt.
   </change>
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen
   </change>
</revision>
```

Und hier nun die versprochene Erklärung zu den einzelnen Stadien:



- **draft**: (Draft, *default*) Die Informationsressource befindet sich in
                                der Bearbeitung.
- **candidate**: (Kandidat) Die Bearbeitung ist soweit abgeschlossen. Die
                                Informationsressource gilt nun als potenzieller Kandidat zur Übertragung an die GND und ist somit bereit zur Prüfung
                                durch eine GND-Agentur.
- **embargoed**: (Gesperrt) Die Informationsressource ist derzeit gesperrt. Sie
                                erfüllt entweder nicht die erforderlichen Qualitätskriterien oder ist aus einem anderen Grund, der in dem
                                Änderungseintrag vermerkt ist, gesperrt. Sie muss entsprechend überarbeitet werden.
- **withdrawn**: (Zurückgezogen) Die Informationsressource wurde zurückgezogen.
                                Sie wird in Hinblick auf eine Übertragung an die GND ignoriert.
- **cleared**: (Überarbeitet) Die Informationsressource wurde in Hinblick auf
                                die geltenden Qualitätskriterien überarbeitet und gilt nun als überarbeiteter Kandidat, der erneut geprüft
                                wird.
- **approved**: (Angenommen) Die Prüfung der Informationsressource ist
                                abgeschlossen. Sie erfüllt alle erforderlichen Qualitätsstandards und ist bereit zur Übertragung an die GND. Dieser
                                Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
- **submitted**: (An GND geliefert) Die Daten der Informationsressource wurden
                                an die GND zum Übertragen geliefert. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur
                                vorbehalten.
- **published**: (Veröffentlicht) Die Daten der Informationsressource wurden in
                                der GND veröffentlicht. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur
                            vorbehalten.



Wenn also kein Stadium explizit gemacht wird, gilt eine Sammlung oder ein Eintrag als "**draft**". Die folgende Abbildung verdeutlicht den Lebenszyklus nochmal anhand der acht Stadien in einer auf das Wesentliche reduzierten Form:

![markdown/docs/img/revision-steps.condensed.png](img/revision-steps.condensed.png)

<a name="docs_d14e1249"></a>
### Anmerkung zur Ausführlichkeit von Revisionsbeschreibungen

Wie immer bei textlicher Dokumentation stellt sich auch bei der Revisionsbeschreibung die Frage, was auf welche weise und vorallem wie ausführlich dokumentiert werden soll? Nun, sowohl der Inhalt als auch die Ausführlichkeit der Beschreibung liegt in der Verantwortung der Bearbeiter\*innen der jeweiligen entityXML Ressource, denn diese wissen wahrscheinlich am Besten, was hilfreiche Informationen sind, die mit Blick auf die Arbeit an ihren Daten festgehalten werden sollten.

Dennoch empfielt es sich, in diesem Bereich nicht nur für sich, sondern auch *für Dritte nachvollziehbar zu dokumentieren*. Die Revisionsbeschreibung dient zwar in erster Linie dazu, Bearbeitungsstände zu dokumentieren, kann und <span class="emph">soll</span> aber auch zur Unterstützung der Kommunikation mit einer GND-Agentur beitragen. Dementsprechend bietet es sich an, an dieser Stelle hilfreiche Informationen nicht nur für die primären Bearbeiter\*innen sondern auch für Agenturmitarbeiter\*innen zu hinterlegen, die in Hinblick auf den Umgang mit der entsprechenden Sammlung oder dem entsprechenden Eintrag von Relevanz sind bzw. sein können. Dazu gehören z.B. Erwartungen oder auch Wünsche, die an die Agentur gestellt werden, genauso wie hilfreiche Hinweise, die die Arbeit erleichtern können:

```xml
<!-- Beispiel einer Revisionsbeschreibung eines Eintrags, der angelegt werden soll -->
<revision status="opened">
   <change when="2022-12-09" who="MFZ" status="candidate">Nicht in GND enthalten. Bitte eintragen.</change>
</revision>
```

```xml
<revision status="opened">
   <change when="2023-01-05" who="MM" status="candidate">
      Bearbeitung abgeschlossen und an die Text+ GND-Agentur geschickt. 
      Die Sammlung besteht aus drei Listen: (1) Personen, (2) Orte, (3) Körperschaften. 
      Bei den Körperschaften bestehen zu einzelnen Einträgen noch
      wichtige Fragen an die GND-Agentur. Diese sind in den 
      entsprechenden Revisionsbeschreibungen hinterlegt. Die Einträge
      sind als "embargoed" gekennzeichnet.
   </change>
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen
   </change>
</revision>
```

Das letzte Beispiel zeigt, dass auch Hinweise bzw. Anmerkungen zu einer Sammlung oder einem Eintrag durchaus hilfreich sein können. In gleicher Weise wird auch die Agentur an dieser Stelle mögliche Rückmeldungen oder Rückfragen dokumentieren, so wie z.B. für den Eintrag mit der ID `ulrich_renner` (GND: [https://d-nb.info/gnd/141596643](https://d-nb.info/gnd/141596643)) in der Beispieldatei [KMG-GND.20230203.0.5.1.xml](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/samples/KMG-GND.20230203.0.5.1.xml):

```xml
<revision status="staged">
   <change when="2023-01-19" who="US" status="approved">
      Die Daten hinsichtlich der verknüpften Publikationen stammen nicht aus 
      dem GND Eintrag (https://d-nb.info/gnd/141596643) sondern aus dem DNB Katalog: 
      Die Katalogseinträge zu den Ausgaben sind mit dem falschen GND Eintrag 
      verknüpft. Die Anfrage wird an die DNB zur Änderung im Katalog weitergeleitet.
   </change>
   <change when="2022-12-08" who="MFZ" status="candidate">Fehlerhafter Eintrag; nicht-zugehörige Literatur gefunden</change>
</revision>
```
