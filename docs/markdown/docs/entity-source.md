
<a name="entity-source"></a>
# Quellenangabe

Das Wissen um eine Entität ist das Ergebniss von Recherche und diese beruht wiederum auf der Interpretation von Quellen. Angaben zu Quellen, aus denen die Informationsinhalte eines Entitätseintrages (z.B. eine bevorzugte Namensform oder biographische bzw. historische Informationen) zusammengestellt werden, werden via [`source`](specs-elems.md#elem.source) dokumentiert. Hierbei handelt es sich bei bibliographischen Quellen für gewöhnlich um ein Kurzzitat der Quelle oder, falls es sich um eine nicht-bibliographische Quelle oder eine Onlineressource handelt, um einen prägnanten Begriff zur Bestimmung der Art der Quelle.

Onlineressourcen werden mit ihrem URL - optimaler Weise ein Permalink - via `@url` dokumentiert:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:variantName>
      <gndo:forename>Mai-Thi</gndo:forename>
      <gndo:surname>Leiendecker</gndo:surname>
   </gndo:variantName>
   <source url="https://de.wikipedia.org/wiki/Mai_Thi_Nguyen-Kim">Wikipedia</source>
</person>
```

```xml
<person xml:id="a_v_jungenfeld">
   <gndo:preferredName>
      <gndo:surname>Gedult von Jungenfeld</gndo:surname>
      <gndo:forename>Arnold Ferdinand</gndo:forename>
   </gndo:preferredName>
   <source url="http://http://scans.hebis.de/22/23/04/22230490_exl-1.jpg">Provenienzmerkmal</source>
   <source url="https://de.wikipedia.org/wiki/Arnold_von_Jungenfeld">Wikipedia</source>
</person>
```

Zusätzlich können weitere Informationen angegeben werden, die die Quelle oder einzelne Aspekte näher erläutern, z.B. Zugriffsdatum auf Webseiten. Hierzu werden in [`source`](specs-elems.md#elem.source) die Elemente [`title`](specs-elems.md#d17e6862) und [`note`](specs-elems.md#add-note) verwendet: [`title`](specs-elems.md#d17e6862), um den Kurztitel der Quelle zu erschließen und [`note`](specs-elems.md#add-note), um den erleuternden Text zu dokumentieren.

```xml
<person xml:id="a_v_jungenfeld">
   <gndo:preferredName>
      <gndo:surname>Gedult von Jungenfeld</gndo:surname>
      <gndo:forename>Arnold Ferdinand</gndo:forename>
   </gndo:preferredName>
   <source url="https://de.wikipedia.org/wiki/Arnold_von_Jungenfeld"><title>Wikipedia</title><note>Stand: 2024-08-06</note></source>
</person>
```

Wenn es sich bei einer Quelle um eine Monographie handelt, dann sollte das Kurzzitat mindestens aus dem Verfassernamen mit Vornamen (auch abgekürzt), Titel und Jahr bestehen.

Bei unselbständigen Veröffentlichungen ist der Mindeststandard die Angabe von Verfassername mit Vorname (auch abgekürzt), Titel, Jahr, Titel der Zeitschrift/Zeitung mit Jahrgang - und/oder Bandzählung sowie Seitenzahl.

**Zusätzliche Ressourcen**:


- Das Feld 670 (Quellenangabe) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/670.pdf)
