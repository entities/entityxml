
<a name="setup"></a>
# Setup

entityXML ist nicht bloß als XML-Format, sondern vorallem als <span class="emph">Werkzeugkasten</span> gedacht, der einerseits das entityXML Schema und andererseits zahlreiche Tools (Konversionen, Stylesheets etc.) umfasst. 

Zusätzlich liefert entityXML ein <span class="emph">Document Type Association Framework</span> für den [oXygen XML Editor](https://www.oxygenxml.com/) aus (im Folgenden "Framework"), das als [oXygen Add-On installiert werden kann](#entityxml-addon). Selbstverständlich können die einzelnen Komponenten aber auch ohne oXygen verwendet werden.

Die folgenden Schritte lohnen sich also, wenn man oXygen oder einen anderen XML Editor verwendet, der "schema-bewusst" arbeitet, d.h. die Daten direkt in Abhängigkeit des angegebenen Validierungsschema überprüft.

<a name="docs_d14e223"></a>
## Einfache Verwendung: entityXML for takeaway

Um entityXML auf die einfachste Art zu nutzen, kopiert man einfach die folgenden 4 Zeilen Code (hierbei handelt es sich um sog. *Processing Instructions*) in eine leere XML Datei und ist fertig:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

Diese *Processing Instructions* verweisen auf das entityXML RNG Schema, die Schematron Validierungsroutinen und die Author Mode CSS im GitLab. Das ist alles, um zu starten! Wenn man auf diese Art mit entityXML arbeitet, arbeitet man immer mit der aktuellen Schemaversion. Allerdings benötigt man eine aktive Internetverbindung.

Hier eine ausführlicheres Rumpftemplate zum einfachen Copy-Paste:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:dnb="https://www.dnb.de" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#"> 
    <collection>
        <metadata>
            <title></title>
            <abstract></abstract>
            <provider id=""></provider>
            <revision status=""></revision>
        </metadata>
        <data>
            <list>
                
            </list>
        </data>
    </collection>
</entityXML>
```

<a name="entityxml-offline"></a>
## Download

Wer sich nicht von einer aktiven Internetverbindung abhängig machen möchte, der kann sich das [entityXML GitLab Repository](https://gitlab.gwdg.de/entities/entityxml) einfach runterladen. Hier hat man zwei Möglichkeiten:

**(1)** Download des Repositories über das Download Icon im GitLab. Hier kann man zwischen 4 Kompressionsformaten wählen (*.zip, *.tar.gz, *.tar.bz2, *.tar) und bekommt dann ein entsprechend komprimiertes Archiv, aus dem dann das entityXML Verzeichnis entpackt und dann an einem beliebigen Ort abgelegt werden kann.

**(2)** Klon des GitLab Repositories mittels Git:


- Via *HTTP*: (`git clone https://gitlab.gwdg.de/entities/entityxml.git`)
- Via *SSH*: (`git clone git@gitlab.gwdg.de:entities/entityxml.git`)

Auf diese Weise hat man die Möglichkeit die lokal gespeicherte Version von entityXML mit Hilfe von Git immer up-to-date zu den Entwicklungsständen im GitLab zu halten.

Diese Art, entityXML auf seinem lokalen Rechner zu speichern ist eher für erfahrene **Entwickler*innen** gedacht, die das entityXML Framework aktiv mitentwickeln oder für eigene Zwecke anpasssen möchten. Für **Anwender*innen** empfielt sich die [Installation
                  als oXygen Add-On](#entityxml-addon).

Damit stehen alle Ressourcen zur Verfügung, um offline zu arbeiten. Was sich nun ändert, sind die Pfade, die in den *Processing Instructions* verwendet werden, um die entsprechenden Dateien zu lokalisieren.

**Hier ein Beispiel**: Mal angenommen, wir laden das entityXML Repository auf einen lokalen Computer runter, und zwar - sagen wir - in das Verzeichnis `Schreibtisch/Arbeit/Entitätenerschließung`. Dieser Ordner hat ein Unterverzeichnis `data`, in dem wir unsere entityXML Dateien anlegen und pflegen möchten. Zum Zwecke dieses Beispiels legen wir eine entityXML Datei an, die wir `entitäten_personen.xml` nennen. Die Ordnerstruktur sieht dann wie folgt aus:

```text
Schreibtisch/
 └── Arbeit/
      └── Entitätenerschließung/
           └── data/		
                └── entitäten_personen.xml
           └── entityXML/	
                └── docs
                └── schema/
                     └── entityXML.rng
                └── assets/
                     └── css/
                          └── author/
                               └── entities.author.css
                └── ...
```

Vor diesem Hintgrund müssten wir die oben angeführten Pfade in unserer Beispieldatei `entitäten_personen.xml` entsprechend unserer Verzeichnisstruktur anpassen:

```xml
<?xml-model href="../entityXML/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="../entityXML/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="../entityXML/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="../entityXML/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>

```

<a name="oxygen-framwork"></a>
## Verwendung als oXygen XML Framework

entityXML wird als <span class="emph">oXygen XML Framework</span> ausgeliefert. Somit kann das Wissen um die Validierung und Verarbeitung von entityXML Daten in oXygen XML fest integriert werden: Sofern eine entityXML Ressource in oXygen geöffnet wird, wird sie automatisch validiert. Fest definierte Transformationsszenarien stehen gleichsam direkt zur Verfügung. Kurz um: Wer regelmäßig mit entityXML arbeitet und die volle Bandbreite des entityXML Werkzeugkastens nutzen möchte, erleichtert sich die Arbeit durch die Frameworkintegration von entityXML in oXygen XML enorm. 

Wählt man diesen Weg, weden außerdem die *Processing Instructions* zu Beginn einer entityXML Ressource überflüssig.

<a name="entityxml-addon"></a>
### Standardinstallation als oXygen Add-On für Anwender*innen

Anwender*innen, die entityXML im oXygen XML-Editor in vollem Umfang nutzen wollen, können das Framework als [oXygen Add-On](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/glossary/plugin.html) installieren. Hierbei handelt es sich um die einfachste Art entityXML zu installieren. Außerdem kümmert sich oXygen bei dieser Installationart in der Folge selbständig um Updates, sprich: Wenn eine neue Version von entityXML veröffentlicht wird, weist oXygen darauf hin und das Framework kann direkt auf die neueste Version geupdated werden!

Um entityXML als Add-On zu installieren, wird wie folgt verfahren:



- Kopiere diesen Link: [`https://gitlab.gwdg.de/entities/updates/-/raw/main/entityxml/updates.xml`](https://gitlab.gwdg.de/entities/updates/-/raw/main/entityxml/updates.xml). Das ist der Link zur entityXML Update Site, die alle veröffentlichten Versionen von entityXML listet.
- Klicke in oXygen auf `Hilfe > Neue Add-Ons installieren`.
- Kopierten Link in dem Feld `Add-Ons zeigen von` einfügen.
- Nun kann die aktuelle (oder eine alte) Version von entityXML installiert werden.
- Abschließend weist oXygen darauf hin, dass die Anwendung neu gestartet werden muss.


Nun sind sie im Besitz von entityXML und können es ohne Einschränkungen nutzen!

<a name="entityxml-framework-integration"></a>
### Integration als Framework für Entwickler*innen

Für Entwickler*innen, die entityXML mitentwickeln oder für ihre eigenen Zwecke anpassen, bietet es sich an, entityXML als Framework in oXygen XML zu integrieren. Hier gibt es im Grunde **zwei Optionen**. Beide setzen voraus, dass das entityXML GitLab Repository als Kopie auf den entsprechenden Rechner lokal vorhanden ist, entweder als [Download](#entityxml-offline) oder als [geklontes Git Repository](https://gitlab.gwdg.de/entities/entityxml):

<a name="docs_d14e386"></a>
#### Option 1: Feste Integration



- Gehe zum `frameworks` Verzeichnis der oXygen XML Anwendung.
- ... Erstelle ein Verzeichnis `entityXML`.
- ... Kopiere den Inhalt des entityXML Repositories in das erstellte Verzeichnis.


<a name="docs_d14e403"></a>
#### Option 2 (Bevorzugt): Lockere Integration als zusätzliches Frameworks

Hierbei handelt es sich m.E. um die beste Option, um entityXML als Framework in oXygen einzubinden, denn mit der Hilfe von [Git](https://git-scm.com/) kann man so das lokale Verzeichnis von entityXML mit dem offiziellen GitLab Repository immer up-to-date halten (wenn man möchte):



- Wähle `Optionen` > `Einstellungen` im Menü aus.
- ... Wähle `Dokumenttypen-Zuordnung` von der linken Liste aus und klicke auf den Untereintrag `Orte`.
- ... Füge den Pfad zum Elternverzeichnis des entityXML Repositories auf deinem Computer zu `Zusätzliche Framework-Verzeichnisse` hinzu.


Auf diese Weise lässt sich entityXML auch in das oXygen Plugin des [TextGrid Laboratorys](https://textgrid.de/download) integrieren:



- Wähle `Fenster` > `Benutzervorgaben` im Menü aus.
- ... Wähle `Oxygen XML Editor` > `Document Type Association` von der linken Liste aus und klicke auf den Untereintrag `Locations`.
- ... Füge den Pfad zum Elternverzeichnis des entityXML Repositories auf deinem Computer zu `Additonal framework directories` hinzu.

