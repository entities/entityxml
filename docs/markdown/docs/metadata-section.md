
<a name="metadata-section"></a>
# Metadaten einer Datensammlung

Der Metadatenbereich einer Datensammlung wird durch das Element [`metadata`](specs-elems.md#collection-metadata) repräsentiert und stellt die Möglichkeit zur Verfügung, administrative und deskriptive Metadaten für eine Datensammlung zu dokumentieren.

In [`metadata`](specs-elems.md#collection-metadata) können z.B. der Titel einer Datensammlung, Bearbeiter oder andere Verantwortlichkeiten, sowie Informationen hinsichtlich des Projekts bzw. des Kontexts, aus dem heraus die Entitäteneinträge der Datensammlung erschlossen wurden, angegeben werden.

Folgende Elemente können in diesem Kontext verwendet werden: 

- **Titel** [`title`](specs-elems.md#d17e6862) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext
                    beschrieben wird."
- **Abstract (Metadaten)** [`abstract`](specs-elems.md#d17e6334) (*verpflichtend*)  
  "Eine kurze
                Beschreibung."
- **Verantwortlichkeit (Metadaten)** [`respStmt`](specs-elems.md#respStmt) (*optional und wiederholbar*)  
  "Angabe zur Rolle einer Person oder
                    Institution in Hinblick auf die Bearbeitung einer Datensammlung."
- **Datenprovider** [`provider`](specs-elems.md#data-provider) (*verpflichtend*)  
  "Informationen zum Datenprovider der Datensammlung und der
                    in diesem Rahmen dokumentierten Entitäten."
- **GND-Agentur** [`agency`](specs-elems.md#agency-stmt) (*optional*)  
  "Informationen zur GND-Agentur, die die Datensammlung
                    bearbeitet."
- **Revisionsbeschreibung** [`revision`](specs-elems.md#revision) (*verpflichtend*)  
  "Statusinformationen zu einer Ressource
                    (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur
                    Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung
                    nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der
                Revisionsbeschreibung!"



```xml
<metadata>
   <title>[Resource Title]</title>
   <provider id="[Provider-ID]">
      <title>[Name des Lieferanten bzw. Erschließungskontexts der Datensammlung]</title>
      <respStmt id="[Person-ID]">
         <resp>[Rolle]</resp>
         <name>[Eigenname der Person]</name>
         <contact>
            <mail>[Email Adresse der Person]</mail>
         </contact>
      </respStmt>
      <abstract>[Kurzbeschreibung über den Lieferanten bzw. Erschließungskontext]</abstract>
   </provider>
   <respStmt id="[Person-ID]">
      <resp>[Rolle]</resp>
      <name>[Eigenname der Person]</name>
      <contact>
         <mail>[Email Adresse der Person]</mail>
      </contact>
   </respStmt>
   <abstract>[Kurzbeschreibung zur Datenlieferung]</abstract>
   <revision status="opened">[Dokumentation der Bearbeitungsstände der vorliegenden Ressource]</revision>
</metadata>
```

<a name="docs_d14e997"></a>
## Titel einer Sammlung

Der Titel einer Sammlung wird via [`title`](specs-elems.md#d17e6862) dokumentiert. Hierbei handelt es sich für gewöhnlich um die Vorzugsbenennung einer Sammlung.

```xml
<title>Editionsprojekt XY: Entitäten</title>
```

<a name="docs_d14e1010"></a>
## Kurzbeschreibung

Die Angabe von `abstract` ist für jede Datensammlung obligatorisch. `abstract` umfasst eine Kurzbeschreibung, die den Entstehungskontext der Datensammlung näher beschreibt.

```xml
<abstract>Sammlung von Entitäten, die im Kontext des digitalen Editionsprojekts XY erschlossen wurden.</abstract>
```

<a name="provider-stmt"></a>
## Providerbeschreibung

Das Element [`provider`](specs-elems.md#data-provider) umfasst deskriptive und administrative Metadaten zu dem Datenlieferanten bzw. dem Kontext, aus dem die in der entsprechenden entityXML Ressource dokumentierten Entitätseinträge heraus erschlossen wurden.

Die Providerbeschreibung ist **optional** aber empfohlen und kann Informationen wie *Name des Providers*, *personelle Verantwortlichkeiten* und eine *Kurzbeschreibung* enthalten: 

- **Provider-ID** [`@id`](specs-attrs.md#d17e6476) (*verpflichtend*)  
  "Der GND-Agentur Identifier des Datenproviders. Dieser
                        Identifier wird in der Regel direkt von der GND-Agentur, bei der der Datenlieferant registriert ist,
                    vergeben."
- **ISIL/BibliothekssiegelISIL/Acronym for libraries** [`@isil`](specs-attrs.md#d17e6925) (*optional*)  
  "Eindeutiger Identifikator der Organisation als
                    ISIL (International Standard Identifier for Libraries and Related Organisations).ISIL of an Organisation (International
                    Standard Identifier for Libraries and Related Organisations)."
- **Titel** [`title`](specs-elems.md#d17e6862) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext
                    beschrieben wird."
- **Abstract (Metadaten)** [`abstract`](specs-elems.md#d17e6334) (*verpflichtend*)  
  "Eine kurze
                Beschreibung."
- **Verantwortlichkeit (Metadaten)** [`respStmt`](specs-elems.md#respStmt) (*optional und wiederholbar*)  
  "Angabe zur Rolle einer Person oder
                    Institution in Hinblick auf die Bearbeitung einer Datensammlung."



Folgendes Template verdeutlicht die Struktur des Providerstatments:

```xml
<provider id="[Projekt-ID]">
   <name>[Projekttitel]</name>
   <respStmt id="[Person-ID]">
     <resp>[Rolle]</resp>
     <name>[Eigenname der Person]</name>
     <contact>
         <mail>[Email Adresse der Person]</mail>
         <address>[Adresse der Person]</address>
         <phone>[Telefonnummer]</phone>
         <web>[URL einer Homepage]</web>
     </contact>
   </respStmt>
   <abstract>[Kurzbeschreibung des Providers bzw Erschließungskontexts]</abstract>
</provider>
```

<a name="resp-stmt"></a>
## Beschreibung von Verantwortlichkeiten

Personen, die im Rahmen eines [Providerstatements](#provider-stmt) oder eine [Datensammlung](data-collection.md) als Verantwortliche (z.B. "Bearbeiter", "Koordinator" etc.) dokumentiert werden sollen, können via [`respStmt`](specs-elems.md#respStmt) dokumentiert werden. Folgende Elemente können in diesem Rahmen dokumentiert werden: 

- **ID** [`@id`](specs-attrs.md#d17e7042) (*verpflichtend*)  
  "Eine interne ID zur Identifikation des entsprechenden
                    Elements."
- **Rolle (Verantwortlichkeit)** [`resp`](specs-elems.md#d17e6383) (*verpflichtend*)  
  "Eine Rolle, die der Person oder
                        Institution im Rahmen der Bearbeitung einer Datensammlung zugeschrieben wird."
- **Name (Verantworlichkeit)** [`name`](specs-elems.md#d17e6400) (*verpflichtend*)  
  "Name der verantwortlichen Person oder
                        Institution."
- **Kontaktinformationen** [`contact`](specs-elems.md#contact-metadata) (*optional*)  
  "Informationen, die zur Kontaktaufnahme genutzt
                    werden können."



Zusätzliche Informationen zur Kontaktaufnahme können via [`contact`](specs-elems.md#contact-metadata) erfolgen. Erforderlich ist hier die Angabe einer Mailadresse (`mail`); alle weiteren Informationen sind optional: 

- **Mailadresse** [`mail`](specs-elems.md#d17e2902) (*verpflichtend und wiederholbar*)  
  "Email Adresse zur
                            Kontaktaufnahme."
- **Telefon** [`phone`](specs-elems.md#d17e2922) (*optional*)  
  "Telefonnummer zur
                            Kontaktaufnahme"
- **Adresse** [`address`](specs-elems.md#d17e2942) (*optional*)  
  "Postalische Anschrift."
- **Homepage (Kontakt)** [`web`](specs-elems.md#d17e2962) (*optional*)  
  "Ein URL zu einer offiziellen oder
                                persönlichen Homepage."



```xml
<respStmt id="MM">
   <resp>Bearbeiter</resp>
   <name>Mustermann, Max</name>
   <contact>
      <mail>muster@entitxml.de</mail>
      <address>Am Datenstrom 5, 1100101 Modulstadt</address>
      <phone>0551-1100101</phone>
      <web>www.entityxml-beispiele.de</web>
   </contact>
</respStmt>
```
