
<a name="specs-elems"></a>
# Elementspezifikationen

Derzeit umfasst das entityXML Schema **118** spezifizierte Elemente.

Die technische Spezifikation für Elemente umfasst folgende Eigenschaften: 

- **Name**: Der Elementname bzw. XML-Tag, der das Element bezeichnet.
- **Equivalents**: Externe Definitionen equivalenter Terme (z.B. in der GNDO).
- **Label**: Ein oder mehrere Labels in Deutsch (und Englisch).
- **Description**: Eine oder mehrere Beschreibungen, die die Semantik des Elements beschreiben.
- **Attributes**: Sofern ein Element Attribute enthalten kann, werden diese hier aufgeführt.
- **Contained by**: Führt die Elemente auf, in denen das entsprechende Element verwendet wird bzw. werden kann.
- **May Contain**: Führt Elemente auf, die innerhalb des entsprechenden Elements verwendet werden bzw. werden können.
- **Content Model**: Abstrahiertes Inhaltsmodell des Elements.
- **Validation**: Falls spezifiziert, werden hier Validierungsregeln (z.B. Schematron Pattern) für das entsprechende Element angegeben.



<a name="any-restricted"></a>
### ANY ELEMENT (eng gefasst)
|     |     |
| --- | --- |
| **Name** | *Any name except those listed below or associated with a namespace listed below.*  |
| **Label** (de) | ANY ELEMENT (eng gefasst) |
| **Label** (en) | ANY ELEMENT (narrow) |
| **Description** (de) | Ein Element, dessen **QName** einem *custom namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist. |
| **Description** (en) | - |
| **Attributes** | [`@<anyAttribute>`](specs-attrs.md#d17e2331)   |
| **Contained by** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted) |

***Content Model***  
```xml
<content>
   <anyAttribute repeatable="true"/>
   <anyOrder>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
</content>
```


***Permited Namespaces & Names***  

- **ns**: https://sub.uni-goettingen.de/met/standards/entity-xml#
- **ns**: https://sub.uni-goettingen.de/met/standards/entity-store#
- **ns**: https://d-nb.info/standards/elementset/gnd#
- **name**: http://id.loc.gov/ontologies/bibframe/**instanceOf**
- **name**: http://xmlns.com/foaf/0.1/**page**
- **name**: http://purl.org/dc/elements/1.1/**title**
- **name**: http://www.w3.org/2004/02/skos/core#**note**
- **name**: http://www.w3.org/2002/07/owl#**sameAs**
- **name**: http://www.opengis.net/ont/geosparql#**hasGeometry**
- **name**: http://www.w3.org/2003/01/geo/wgs84_pos#**lat**
- **name**: http://www.w3.org/2003/01/geo/wgs84_pos#**long**

---

<p style="margin-bottom:60px"></p>

<a name="any-all"></a>
### ANY ELEMENT (weit gefasst)
|     |     |
| --- | --- |
| **Name** | *Any name except those listed below or associated with a namespace listed below.*  |
| **Label** (de) | ANY ELEMENT (weit gefasst) |
| **Label** (en) | ANY ELEMENT (broad) |
| **Description** (de) | Ein Element, dessen **QName** einem *custom namespace* angehört. Dieses Element kann im extremsten Fall einen vollständigen Datensatz eines anderen XML-Formats repräsentieren. |
| **Description** (en) | - |
| **Attributes** | [`@<anyAttribute>`](specs-attrs.md#d17e2331)   |
| **Contained by** | [`<anyElement.broad>`](specs-elems.md#any-all)  [`entity`](specs-elems.md#entity-record) |
| **May contain** | [`<anyElement.broad>`](specs-elems.md#any-all) |

***Content Model***  
```xml
<content>
   <anyAttribute repeatable="true"/>
   <anyOrder>
      <anyElement type="broad" repeatable="true"/>
      <text/>
   </anyOrder>
</content>
```


***Permited Namespaces & Names***  

- **ns**: https://sub.uni-goettingen.de/met/standards/entity-xml#
- **ns**: https://sub.uni-goettingen.de/met/standards/entity-store#
- **ns**: https://d-nb.info/standards/elementset/gnd#

---

<p style="margin-bottom:60px"></p>

<a name="d17e3272"></a>
### Abgekürzter Name
|     |     |
| --- | --- |
| **Name** | `gndo:abbreviatedName`  |
| **See also** | [gndo:abbreviatedName](https://d-nb.info/standards/elementset/gnd#abbreviatedName), [gndo:abbreviatedNameForTheConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForTheConferenceOrEvent), [gndo:abbreviatedNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForTheCorporateBody), [gndo:abbreviatedNameForThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForThePlaceOrGeographicName), [gndo:abbreviatedNameForTheWork](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForTheWork) |
| **Label** (de) | Abgekürzter Name |
| **Label** (en) | Abbreviated name |
| **Description** (de) | Eine abgekürzte Namensform der dokumentierten Entität. |
| **Description** (en) | An abbreviated Name for the entity encoded. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@xml:lang`](specs-attrs.md#d17e7157) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`event`](specs-elems.md#d17e320)  [`place`](specs-elems.md#d17e1265)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6334"></a>
### Abstract (Metadaten)
|     |     |
| --- | --- |
| **Name** | `abstract`  |
| **Label** (de) | Abstract (Metadaten) |
| **Label** (en) | Abstract (Metadata) |
| **Description** (de) | Eine kurze Beschreibung. |
| **Description** (en) | A short description. |
| **Contained by** | [`agency`](specs-elems.md#agency-stmt)  [`metadata`](specs-elems.md#collection-metadata)  [`provider`](specs-elems.md#data-provider) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5969"></a>
### Adelstitel
|     |     |
| --- | --- |
| **Name** | `gndo:titleOfNobility`  |
| **See also** | [gndo:titleOfNobility](https://d-nb.info/standards/elementset/gnd#titleOfNobility) |
| **Label** (de) | Adelstitel |
| **Label** (en) | Title of nobility |
| **Description** (de) | Ein Adelsname der dokumentierten Person oder Personengruppe. |
| **Description** (en) | A title of nobility held by a person or family. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)  [`@xml:lang`](specs-attrs.md#d17e7157) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2942"></a>
### Adresse
|     |     |
| --- | --- |
| **Name** | `address`  |
| **Label** (de) | Adresse |
| **Label** (en) | address |
| **Description** (de) | Postalische Anschrift. |
| **Description** (en) | - |
| **Contained by** | [`contact`](specs-elems.md#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3318"></a>
### Akademischer Grad
|     |     |
| --- | --- |
| **Name** | `gndo:academicDegree`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#academicDegree](https://d-nb.info/standards/elementset/gnd#academicDegree) |
| **Label** (de) | Akademischer Grad |
| **Label** (en) | Academic degree |
| **Description** (de) | Angaben zum akademischen Grad der Person, z.B. "Dr. jur.". |
| **Description** (en) | Description with regard to the academic degree of a person |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.skos.note"></a>
### Anmerkung
|     |     |
| --- | --- |
| **Name** | `skos:note`  |
| **See also** | [skos:note](https://www.w3.org/2009/08/skos-reference/skos.html#note) |
| **Label** (de) | Anmerkung |
| **Label** (en) | Note |
| **Description** (de) | Eine Anmerkung zum Eintrag. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:type`](specs-attrs.md#d17e6668)[`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4205"></a>
### Auflösungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfTermination`  |
| **See also** | [gndo:dateOfTermination](https://d-nb.info/standards/elementset/gnd#dateOfTermination) |
| **Label** (de) | Auflösungsdatum |
| **Label** (en) | Date of termination |
| **Description** (de) | Das Datum, an dem die beschriebene Entität aufgelöst wurde. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@cert`](specs-attrs.md#d17e6885)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-date`](specs-attrs.md#attr.iso-date)  [`@iso-notAfter`](specs-attrs.md#attr.iso-notAfter)[`@iso-notBefore`](specs-attrs.md#attr.iso-notBefore)   |
| **Contained by** |  |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1449"></a>
### Ausdrucksform
|     |     |
| --- | --- |
| **Name** | `expression`  |
| **Label** (de) | Ausdrucksform |
| **Label** (en) | Expression |
| **Description** (de) | Ausdrucksform eines Werks nach FRBR. |
| **Description** (en) | Expression of a Work (FRBR). |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`realizationOf`](specs-elems.md#d17e1479)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="realizationOf"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4336"></a>
### Aussteller
|     |     |
| --- | --- |
| **Name** | `gndo:exhibitor`  |
| **See also** | [gndo:exhibitor](https://d-nb.info/standards/elementset/gnd#exhibitor) |
| **Label** (de) | Aussteller |
| **Label** (en) | Exhibitor |
| **Description** (de) | Die Person, Familie oder Körperschaft, die verantwortlich für die Ausstellung ist. |
| **Description** (en) | A person, family, or corporate body in charge of an exhibition. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`event`](specs-elems.md#d17e320) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4491"></a>
### Autor, erste Verfasserschaft
|     |     |
| --- | --- |
| **Name** | `gndo:firstAuthor`  |
| **See also** | [gndo:firstAuthor](https://d-nb.info/standards/elementset/gnd#firstAuthor) |
| **Label** (de) | Autor, erste Verfasserschaft |
| **Label** (en) | First author |
| **Description** (de) | Die Person oder Körperschaft, die verantwortlich für die primäre Verfasserschaft an einem Werk oder einer Veröffentlichung zeichnet. |
| **Description** (en) | A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e5604)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4311"></a>
### Beiname, Gattungsname, Titulatur, Territorium
|     |     |
| --- | --- |
| **Name** | `gndo:epithetGenericNameTitleOrTerritory`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#epithetGenericNameTitleOrTerritory](https://d-nb.info/standards/elementset/gnd#epithetGenericNameTitleOrTerritory) |
| **Label** (de) | Beiname, Gattungsname, Titulatur, Territorium |
| **Label** (en) | Epithet, generic name, title or territory |
| **Description** (de) | Beiname einer Person, bei dem es sich um ein Epitheton, Titel oder eine Ortsassoziation handelt. |
| **Description** (en) | - |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5488"></a>
### Beruf oder Beschäftigung
|     |     |
| --- | --- |
| **Name** | `gndo:professionOrOccupation`  |
| **See also** | [gndo:professionOrOccupation](https://d-nb.info/standards/elementset/gnd#professionOrOccupation) |
| **Label** (de) | Beruf oder Beschäftigung |
| **Label** (en) | Profession or occupation |
| **Description** (de) | Angabe zum Beruf, Tätigkeitsbereich o.Ä., der dokumentierten Person. Zulässige Deskriptoren stammen aus der GND-Systematik und entprechen GND-URIs von Sachbegriffen, z.B. https://d-nb.info/gnd/4168391-2 ("Lyriker"). |
| **Description** (en) | A profession or occupation practiced by a person or family |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@gndo:type`](specs-attrs.md#d17e5520) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:professionOrOccupation">
      <sch:assert test="@gndo:ref" role="info">(Missing Reference to Profession GND Record): It is recommended to provide a
                        GNDO-URI to a GND-Record of this Profession or Occupation.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3635"></a>
### Beteiligte Person
|     |     |
| --- | --- |
| **Name** | `gndo:contributor`  |
| **Label** (de) | Beteiligte Person |
| **Label** (en) | Contributor |
| **Description** (de) | Eine Person, Familie oder Organisation, die für Beiträge zur Ressource verantwortlich ist. Dies schließt diejenigen ein, deren Arbeit zu einem größeren Werk beigetragen wurde, wie z. B. einem Sammelband, einer fortlaufenden Publikation oder einer anderen Zusammenstellung von Einzelwerken. |
| **Description** (en) | A person, family or organization responsible for making contributions to the resource. This includes those whose work has been contributed to a larger work, such as an anthology, serial publication, or other compilation of individual works. If a more specific role is available, prefer that, e.g. editor, compiler, illustrator |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@gndo:role`](specs-attrs.md#d17e3656)[`@ref`](specs-attrs.md#d17e7662)   |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="gndo:role" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5757"></a>
### Beziehung
|     |     |
| --- | --- |
| **Name** | `gndo:relatesTo`  |
| **Label** (de) | Beziehung |
| **Label** (en) | Relates to |
| **Description** (de) | Beziehung der beschriebenen Entität zu einer anderen Entität, entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der GNDO! |
| **Description** (en) | Relation to another Entity in the GND or in the current entityXML resource. This property doesn't have a direct equivalent in the GNDO! |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:code`](specs-attrs.md#d17e5783)[`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <choice>
      <attribute name="gndo:ref" required="true"/>
      <attribute name="ref" required="true"/>
   </choice>
   <attribute name="gndo:code" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3345"></a>
### Beziehung, Bekanntschaft, Freundschaft
|     |     |
| --- | --- |
| **Name** | `gndo:acquaintanceshipOrFriendship`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#acquaintanceshipOrFriendship](https://d-nb.info/standards/elementset/gnd#acquaintanceshipOrFriendship) |
| **Label** (de) | Beziehung, Bekanntschaft, Freundschaft |
| **Label** (en) | Acquaintanceship or friendship |
| **Description** (de) | Beziehung zwischen einer Person, Familie oder Körperschaft und einer anderen Person, Familie oder Körperschaft, die nicht näher bestimmt werden kann. |
| **Description** (en) | Relationship between a person, family, or corporate body and another person, family, or corporate body which cannot be specified more closely. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)  [`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |
| **May contain** | [`gndo:counting`](specs-elems.md#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](specs-elems.md#d17e4311)  [`gndo:forename`](specs-elems.md#d17e4522)  [`gndo:nameAddition`](specs-elems.md#d17e4918)  [`gndo:personalName`](specs-elems.md#d17e5276)  [`gndo:prefix`](specs-elems.md#d17e5463)  [`gndo:surname`](specs-elems.md#d17e5896)  [`note`](specs-elems.md#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <choice>
      <attribute name="gndo:ref" required="true"/>
      <attribute name="ref" required="true"/>
   </choice>
   <attribute name="xml:lang"/>
   <anyOrder>
      <choice>
         <element name="gndo:personalName" required="true"/>
         <anyOrder>
            <element name="gndo:forename" required="true"/>
            <element name="gndo:prefix"/>
            <element name="gndo:surname" required="true"/>
            <element name="gndo:counting"/>
            <text/>
         </anyOrder>
      </choice>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
      <element name="note"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.img"></a>
### Bild
|     |     |
| --- | --- |
| **Name** | `img`  |
| **Label** (de) | Bild |
| **Label** (en) | Image |
| **Description** (de) | Ein Bild bzw. ein URL zu einer Bildressource. |
| **Description** (en) | An image represented by an URL or Path. |
| **Attributes** | [`@height`](specs-attrs.md#d17e6157)[`@src`](specs-attrs.md#d17e6132)  [`@width`](specs-attrs.md#d17e6145)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="src" required="true"/>
   <attribute name="width"/>
   <attribute name="height"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3463"></a>
### Biographische Angaben
|     |     |
| --- | --- |
| **Name** | `gndo:biographicalOrHistoricalInformation`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#biographicalOrHistoricalInformation](https://d-nb.info/standards/elementset/gnd#biographicalOrHistoricalInformation) |
| **Label** (de) | Biographische Angaben |
| **Label** (en) | bibliographic or historical Information |
| **Description** (de) | Biographische oder historische Angaben über die Entität. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@xml:lang`](specs-attrs.md#d17e7157) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-section"></a>
### Daten Bereich
|     |     |
| --- | --- |
| **Name** | `data`  |
| **Label** (de) | Daten Bereich |
| **Label** (en) | data area |
| **Description** (de) | - |
| **Description** (en) | - |
| **Contained by** | [`collection`](specs-elems.md#collection) |
| **May contain** | [`list`](specs-elems.md#data-list) |

***Content Model***  
```xml
<content>
   <element name="list" required="true" repeatable="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-provider"></a>
### Datenprovider
|     |     |
| --- | --- |
| **Name** | `provider`  |
| **Label** (de) | Datenprovider |
| **Label** (en) | Dataprovider |
| **Description** (de) | Informationen zum Datenprovider der Datensammlung und der in diesem Rahmen dokumentierten Entitäten. |
| **Description** (en) | Information about the dataprovider of the datacollection and the contained entities. |
| **Attributes** | [`@id`](specs-attrs.md#d17e6476)  [`@isil`](specs-attrs.md#d17e6925)   |
| **Contained by** | [`metadata`](specs-elems.md#collection-metadata) |
| **May contain** | [`abstract`](specs-elems.md#d17e6334)  [`respStmt`](specs-elems.md#respStmt)[`title`](specs-elems.md#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="id" required="true"/>
   <attribute name="isil"/>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract" required="true"/>
      <element name="respStmt" repeatable="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="collection"></a>
### Datensammlung
|     |     |
| --- | --- |
| **Name** | `collection`  |
| **Label** (de) | Datensammlung |
| **Label** (en) | Data Collection |
| **Description** (de) | Eine Datensammlung, die Entity Records enthält. Eine Datensammlung kann in einer oder mehrerer Listen organisisert sein, um die Sammlung weiter zu strukturieren. |
| **Description** (en) | A Datacollection containing Entity records. A datacollection can be organised using one or many lists to provide a more detailed structure to the collection. |
| **Contained by** | [`entityXML`](specs-elems.md#entity-xml) |
| **May contain** | [`data`](specs-elems.md#data-section)[`metadata`](specs-elems.md#collection-metadata)   |

***Content Model***  
```xml
<content>
   <element name="metadata" required="true"/>
   <element name="data" required="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2985"></a>
### Datum
|     |     |
| --- | --- |
| **Name** | `date`  |
| **See also** | [dc:date](http://purl.org/dc/elements/1.1/date) |
| **Label** (de) | Datum |
| **Label** (en) | Date |
| **Description** (de) | Ein Zeitpunkt oder Zeitraum, der mit einem Ereignis im Lebenszyklus der beschriebenen Ressource in Zusammenhang steht. |
| **Description** (en) | A point or period of time associated with an event in the lifecycle of the resource. |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e5604) |

***Content Model***  
```xml
<content>
   <choice/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-xml"></a>
### EntityXML Ressource
|     |     |
| --- | --- |
| **Name** | `entityXML`  |
| **Label** (de) | EntityXML Ressource |
| **Label** (en) | EntityXML Resource |
| **Description** (de) | Eine entityXML Ressource, die eine Datensammlung oder eine Projektbeschreibung enthält. |
| **Description** (en) | - |
| **Contained by** |  |
| **May contain** | [`collection`](specs-elems.md#collection)  [`mapping`](specs-elems.md#d17e6226)  [`store:store`](specs-elems.md#d17e6745) |

***Content Model***  
```xml
<content>
   <choice>
      <group>
         <choice>
            <element name="collection" required="true"/>
         </choice>
         <element name="mapping"/>
      </group>
      <element name="mapping" required="true"/>
   </choice>
   <element name="store:store"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-record"></a>
### Entität (Record)
|     |     |
| --- | --- |
| **Name** | `entity`  |
| **Label** (de) | Entität (Record) |
| **Label** (en) | Entity (Record) |
| **Description** (de) | Ein Entitätsdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). `entity` ermöglicht es zudem, ganze Datensätze eines fremden Namespaces zu integrieren. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:type`](specs-attrs.md#d17e268)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.broad>`](specs-elems.md#any-all)[`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)  [`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <attribute name="gndo:type" required="true"/>
   <choice>
      <group>
         <anyOrder>
            <element name="gndo:homepage" repeatable="true"/>
            <element name="gndo:preferredName"/>
            <element name="gndo:variantName" repeatable="true"/>
            <element name="dc:title"/>
            <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
            <element name="dublicateGndIdentifier" repeatable="true"/>
            <element name="gndo:geographicAreaCode" repeatable="true"/>
            <element name="gndo:gndIdentifier" repeatable="true"/>
            <element name="gndo:gndSubjectCategory" repeatable="true"/>
            <element name="img" repeatable="true"/>
            <element name="gndo:languageCode" repeatable="true"/>
            <element name="gndo:relatesTo" repeatable="true"/>
            <element name="ref" repeatable="true"/>
            <element name="owl:sameAs" repeatable="true"/>
            <element name="skos:note" repeatable="true"/>
            <element name="source" repeatable="true"/>
            <anyElement type="narrow" repeatable="true"/>
            <text/>
         </anyOrder>
         <element name="revision"/>
      </group>
      <anyElement type="broad" required="true"/>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="add-note"></a>
### Erleuternde Anmerkung
|     |     |
| --- | --- |
| **Name** | `note`  |
| **Label** (de) | Erleuternde Anmerkung |
| **Label** (en) | explanatory note |
| **Description** (de) | Zusätzliche erläuternder Text zur Informationen, die beschrieben wird. |
| **Description** (en) | Additional explanatory text about the information described. |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:organizerOrHost`](specs-elems.md#d17e4943)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument)  [`source`](specs-elems.md#elem.source) |

***Content Model***  
```xml
<content>
   <anyOrder>
      <text/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4177"></a>
### Erscheinungszeit
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfPublication`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#dateOfPublication](https://d-nb.info/standards/elementset/gnd#dateOfPublication) |
| **Label** (de) | Erscheinungszeit |
| **Label** (en) | Date of publication |
| **Description** (de) | Erscheinungsdatum der ersten Ausdrucksform des Werks. |
| **Description** (en) | Date of publication of the first expression of a work. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@cert`](specs-attrs.md#d17e6885)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-date`](specs-attrs.md#attr.iso-date)  [`@iso-notAfter`](specs-attrs.md#attr.iso-notAfter)[`@iso-notBefore`](specs-attrs.md#attr.iso-notBefore)   |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e320"></a>
### Event
|     |     |
| --- | --- |
| **Name** | `event`  |
| **See also** | [gndo:ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent) |
| **Label** (de) | Event |
| **Label** (en) | Event |
| **Description** (de) | Ein temporales Ereignis, wie z.B. eine Veranstaltung, Konferenz. |
| **Description** (en) | An Event as a temporal entity, e.g. a conference. |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:type`](specs-attrs.md#event-types)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`gndo:abbreviatedName`](specs-elems.md#d17e3272)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:dateOfConferenceOrEvent`](specs-elems.md#d17e4073)  [`gndo:exhibitor`](specs-elems.md#d17e4336)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:organizerOrHost`](specs-elems.md#d17e4943)  [`gndo:place`](specs-elems.md#entity-place-standard)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:topic`](specs-elems.md#d17e6005)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:dateOfConferenceOrEvent"/>
      <element name="gndo:exhibitor" repeatable="true"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:organizerOrHost" repeatable="true"/>
      <element name="gndo:place" required="true"/>
      <element name="gndo:preferredName" required="true"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4367"></a>
### Familiäre Beziehung
|     |     |
| --- | --- |
| **Name** | `gndo:familialRelationship`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#familialRelationship](https://d-nb.info/standards/elementset/gnd#familialRelationship) |
| **Label** (de) | Familiäre Beziehung |
| **Label** (en) | Familial relationship |
| **Description** (de) | Eine familiäre Beziehung zwischen einer Person oder Familie und einer anderen Person oder Familie. |
| **Description** (en) | A family relationship between a person or family and another person or family |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)  [`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |
| **May contain** | [`gndo:counting`](specs-elems.md#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](specs-elems.md#d17e4311)  [`gndo:forename`](specs-elems.md#d17e4522)  [`gndo:nameAddition`](specs-elems.md#d17e4918)  [`gndo:personalName`](specs-elems.md#d17e5276)  [`gndo:prefix`](specs-elems.md#d17e5463)  [`gndo:surname`](specs-elems.md#d17e5896)  [`note`](specs-elems.md#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <choice>
      <attribute name="gndo:ref" required="true"/>
      <attribute name="ref" required="true"/>
   </choice>
   <attribute name="xml:lang"/>
   <anyOrder>
      <choice>
         <element name="gndo:personalName" required="true"/>
         <anyOrder>
            <element name="gndo:forename" required="true"/>
            <element name="gndo:prefix"/>
            <element name="gndo:surname" required="true"/>
            <element name="gndo:counting"/>
            <text/>
         </anyOrder>
      </choice>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
      <element name="note"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4421"></a>
### Form des Werks und der Expression
|     |     |
| --- | --- |
| **Name** | `gndo:formOfWorkAndExpression`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#formOfWorkAndExpression](https://d-nb.info/standards/elementset/gnd#formOfWorkAndExpression) |
| **Label** (de) | Form des Werks und der Expression |
| **Label** (en) | Form of work and expression |
| **Description** (de) | ... |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4550"></a>
### Funktion oder Rolle
|     |     |
| --- | --- |
| **Name** | `gndo:functionOrRole`  |
| **See also** | [gndo:functionOrRole](https://d-nb.info/standards/elementset/gnd#functionOrRole) |
| **Label** (de) | Funktion oder Rolle |
| **Label** (de) | Function or role |
| **Description** (de) | Anm.: Seit 2017 aus der GND entfernt! |
| **Description** (de) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:term`](specs-attrs.md#d17e7026) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="agency-stmt"></a>
### GND-Agentur
|     |     |
| --- | --- |
| **Name** | `agency`  |
| **Label** (de) | GND-Agentur |
| **Label** (en) | GND-agency |
| **Description** (de) | Informationen zur GND-Agentur, die die Datensammlung bearbeitet. |
| **Description** (en) | Information about the GND-agency working on the datacollection. |
| **Attributes** | [`@isil`](specs-attrs.md#d17e6925)   |
| **Contained by** | [`metadata`](specs-elems.md#collection-metadata) |
| **May contain** | [`abstract`](specs-elems.md#d17e6334)  [`respStmt`](specs-elems.md#respStmt)[`title`](specs-elems.md#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="isil" required="true"/>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract"/>
      <element name="respStmt" repeatable="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4693"></a>
### GND-Identifier
|     |     |
| --- | --- |
| **Name** | `gndo:gndIdentifier`  |
| **Label** (de) | GND-Identifier |
| **Label** (en) | GND-Identifier |
| **Description** (de) | Der GND-Identifier eines in der GND eingetragenen Normdatensatzes sofern vorhanden |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@cert`](specs-attrs.md#d17e6885)[`@enriched`](specs-attrs.md#d17e7538)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:gndIdentifier" role="error">
      <sch:assert test="matches(./text(), '^([\w\d-])+$')">
         <sch:name/>(Invalid content): <sch:name/> must contain just one single GND-ID (eg. "118602802" or "2148150-7")!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="dublicates"></a>
### GND-Identifier einer Dublette
|     |     |
| --- | --- |
| **Name** | `dublicateGndIdentifier`  |
| **Label** (de) | GND-Identifier einer Dublette |
| **Label** (en) | GND-Identifier of a dublicate |
| **Description** (de) | Der GND-Identifier einer möglichen Dublette. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@cert`](specs-attrs.md#d17e6885)[`@enriched`](specs-attrs.md#d17e7538)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:dublicateGndIdentifier" role="error">
      <sch:assert test="matches(./text(), '^([\w\d-])+$')">
         <sch:name/>(Invalid content): <sch:name/> must contain a valid GND-ID (eg. "118602802" or "2148150-7")!</sch:assert>
      <sch:assert test="parent::element()[@gndo:uri]">
         <sch:name/>(Missing GND-URI): It is mandatory to provide a GND-URI of the Entity recorded if <sch:name/> is used!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4734"></a>
### GND-Sachgruppe
|     |     |
| --- | --- |
| **Name** | `gndo:gndSubjectCategory`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#gndSubjectCategory](https://d-nb.info/standards/elementset/gnd#gndSubjectCategory) |
| **Label** (de) | GND-Sachgruppe |
| **Label** (en) | GND subject category |
| **Description** (de) | Term aus dem GND-Sachgruppen Vokabular (ehemals SWD) |
| **Description** (en) | Descriptor from GND Subject Categories Vocabulary (ehemals SWD) |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:term`](specs-attrs.md#d17e7026)[`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:gndSubjectCategory/@gndo:term" role="error">
      <sch:assert test="matches(., '(http|https)://d-nb.info/standards/vocab/gnd/gnd-sc#(.+)')"> (Missing or wrong vocabulary term): It
                        is mandatory to provide an URI Descriptor from the [GND Sachgruppen Vocabulary](https://d-nb.info/standards/vocab/gnd/gnd-sc)!
                    </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-dateOfBirth"></a>
### Geburtsdatum (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfBirth`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#dateOfBirth](https://d-nb.info/standards/elementset/gnd#dateOfBirth) |
| **Label** (de) | Geburtsdatum (Person) |
| **Label** (en) | Date of birth (Person) |
| **Description** (de) | Das Geburtsdatum der Person. |
| **Description** (en) | The birth date of a person. This can be a year, a year-month combination or a full date. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@cert`](specs-attrs.md#d17e6885)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-date`](specs-attrs.md#attr.iso-date)  [`@iso-notAfter`](specs-attrs.md#attr.iso-notAfter)[`@iso-notBefore`](specs-attrs.md#attr.iso-notBefore)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfBirth"></a>
### Geburtsort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfBirth`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfBirth](https://d-nb.info/standards/elementset/gnd#placeOfBirth) |
| **Label** (de) | Geburtsort (Person) |
| **Label** (en) | Place of Birth (Person) |
| **Description** (de) | Der Geburtsort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfBirth">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via
                        `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1265"></a>
### Geografikum
|     |     |
| --- | --- |
| **Name** | `place`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName) |
| **Label** (de) | Geografikum |
| **Label** (en) | Place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:type`](specs-attrs.md#d17e1371)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`geo:hasGeometry`](specs-elems.md#d17e3150)  [`gndo:abbreviatedName`](specs-elems.md#d17e3272)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](specs-elems.md#d17e4775)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:place`](specs-elems.md#entity-place-standard)  [`gndo:precedingPlaceOrGeographicName`](specs-elems.md#d17e5302)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:succeedingPlaceOrGeographicName`](specs-elems.md#d17e5866)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="geo:hasGeometry"/>
      <element name="gndo:hierarchicalSuperiorOfPlaceOrGeographicName"/>
      <element name="gndo:place"/>
      <element name="gndo:precedingPlaceOrGeographicName"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="gndo:succeedingPlaceOrGeographicName"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:place[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3192"></a>
### Geographische Breite
|     |     |
| --- | --- |
| **Name** | `wgs84:lat`  |
| **Label** (de) | Geographische Breite |
| **Description** (de) | Geographische Breite in der Dezimalschreibweise. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538) |
| **Contained by** | [`geo:hasGeometry`](specs-elems.md#d17e3150) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3205"></a>
### Geographische Länge
|     |     |
| --- | --- |
| **Name** | `wgs84:long`  |
| **Label** (de) | Geographische Länge |
| **Description** (de) | Geographische Länge in der Dezimalschreibweise. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538) |
| **Contained by** | [`geo:hasGeometry`](specs-elems.md#d17e3150) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.geographicAreaCode"></a>
### Geographischer Schwerpunkt
|     |     |
| --- | --- |
| **Name** | `gndo:geographicAreaCode`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#geographicAreaCode](https://d-nb.info/standards/elementset/gnd#geographicAreaCode) |
| **Label** (de) | Geographischer Schwerpunkt |
| **Label** (en) | Geographic Area Code |
| **Description** (de) | Ländercode(s) der Staat(en), denen die Entität zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden! |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:term`](specs-attrs.md#d17e6949) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
                <!--<sch:rule context="gndo:geographicAreaCode" role="error">
                    <sch:assert test="@gndo:term or @term">
                        (Missing Code): It is mandatory to provide either a `@gndo:term` or a custom code via `@code` on `<sch:name/>`!
                    </sch:assert>
                </sch:rule>-->
                <!--<sch:rule context="gndo:geographicAreaCode/@term" role="warning">
                    <sch:assert test=". = 'ZZ'">
                        (Custom Terms will be ignored): Custom Terms in `@<sch:name/>` on gndo:geographicAreaCode will be handled by the GND-Agency as "unknown Geographic Area"!
                    </sch:assert>
                </sch:rule>-->
   <sch:rule context="gndo:geographicAreaCode/@gndo:term" role="error">
      <sch:assert test="matches(., '(http|https)://d-nb.info/standards/vocab/gnd/geographic-area-code#(.+)')"> (Missing vocabulary
                        term): It is mandatory to provide an URI Descriptor from the [GND Area Code
                        Vocabulary](https://d-nb.info/standards/vocab/gnd/geographic-area-code). </sch:assert>
   </sch:rule>
   <sch:rule context="gndo:geographicAreaCode" role="warning">
      <sch:let name="term" value="@gndo:term"/>
      <sch:let name="banned-values"
               value="('XA', 'XB', 'XC', 'XD', 'XE', 'XH', 'XI', 'XK', 'XL', 'XM', 'XN', 'XP', 'XQ', 'XR', 'XS', 'XT', 'XU', 'XV', 'XW', 'XX', 'XY', 'XZ')"/>
      <sch:assert test="not(tokenize($term, '#')[2] = $banned-values)">(Unsupported Area Code): This Area Code will be ignored during the process of MARC21 Templating since it is missing a country.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3150"></a>
### Geometry eines geographischen Features
|     |     |
| --- | --- |
| **Name** | `geo:hasGeometry`  |
| **See also** | [https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_property_geohasgeometry](https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_property_geohasgeometry) |
| **Label** (de) | Geometry eines geographischen Features |
| **Label** (en) | geometry of a geographic feature |
| **Attributes** | [`@geo:source`](specs-attrs.md#d17e3174)   |
| **Contained by** | [`place`](specs-elems.md#d17e1265) |
| **May contain** | [`wgs84:lat`](specs-elems.md#d17e3192)  [`wgs84:long`](specs-elems.md#d17e3205) |

***Content Model***  
```xml
<content>
   <attribute name="geo:source"/>
   <element name="wgs84:lat" required="true"/>
   <element name="wgs84:long" required="true"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="geo:hasGeometry">
      <sch:assert test="@geo:source" role="warning">(Missing source): It is strongly recommended to provide an URL to the source of the
                        encoded coordinates!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="wgs84:long | wgs84:lat">
      <sch:assert test="matches(text(), '(\+|\-)?\d{1,3}\.\d{5,6}')" role="error">(Wrong value encoding): The value of "<sch:name/>"
                        must match '(\+|\-)?\d{1,3}\.\d{5,6}'! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4581"></a>
### Geschlecht
|     |     |
| --- | --- |
| **Name** | `gndo:gender`  |
| **See also** | [gndo:gender](https://d-nb.info/standards/elementset/gnd#gender) |
| **Label** (de) | Geschlecht |
| **Label** (en) | gender |
| **Description** (de) | Angaben zum Geschlecht einer Person. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:term`](specs-attrs.md#d17e4603) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="prop-playedInstrument"></a>
### Gespieltes Instrument
|     |     |
| --- | --- |
| **Name** | `gndo:playedInstrument`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#playedInstrument](https://d-nb.info/standards/elementset/gnd#playedInstrument) |
| **Label** (de) | Gespieltes Instrument |
| **Label** (en) | Played instrument |
| **Description** (de) | Ein Instrument, dass durch die beschriebene Entität gespielt wird. |
| **Description** (en) | An instrument played by the entity described. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |
| **May contain** | [`label`](specs-elems.md#d17e6206)  [`note`](specs-elems.md#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <choice>
      <text/>
      <group>
         <element name="label" required="true"/>
         <element name="note" required="true"/>
      </group>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.owl.sameAs"></a>
### Gleiche Entität
|     |     |
| --- | --- |
| **Name** | `owl:sameAs`  |
| **See also** | [https://www.w3.org/TR/owl-ref/#sameAs-def](https://www.w3.org/TR/owl-ref/#sameAs-def) |
| **Label** (de) | Gleiche Entität |
| **Label** (en) | Same as |
| **Description** (de) | HTTP-URI der selben Entität in einem anderen Normdatendienst. |
| **Description** (en) | links an individual to an individual. This statement indicates that two URI references actually refer to the same thing: the individuals have the same "identity". |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4135"></a>
### Gründungs- und Auflösungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfEstablishmentAndTermination`  |
| **See also** | [gndo:dateOfEstablishment](https://d-nb.info/standards/elementset/gnd#dateOfEstablishment) |
| **Label** (de) | Gründungs- und Auflösungsdatum |
| **Label** (en) | Date of establishment and termination |
| **Description** (de) | Zeitraum, innerhalb dessen die beschriebene Entität bestand. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-from`](specs-attrs.md#attr.iso-from)  [`@iso-to`](specs-attrs.md#attr.iso-to) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:dateOfEstablishmentAndTermination">
      <sch:assert test="@iso-from and @iso-to" role="error">(start and end date missing): It is mandatory to provide a star- and
                        endpoint in time to encode a timespan like `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4104"></a>
### Gründungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfEstablishment`  |
| **See also** | [gndo:dateOfEstablishment](https://d-nb.info/standards/elementset/gnd#dateOfEstablishment) |
| **Label** (de) | Gründungsdatum |
| **Label** (en) | Date of establishment |
| **Description** (de) | Das Datum, an dem die beschriebene Entität entstanden ist bzw. gegründet wurde. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@cert`](specs-attrs.md#d17e6885)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-date`](specs-attrs.md#attr.iso-date)  [`@iso-notAfter`](specs-attrs.md#attr.iso-notAfter)[`@iso-notBefore`](specs-attrs.md#attr.iso-notBefore)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4281"></a>
### Herausgeber
|     |     |
| --- | --- |
| **Name** | `gndo:editor`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#editor](https://d-nb.info/standards/elementset/gnd#editor) |
| **Label** (de) | Herausgeber |
| **Label** (en) | Editor |
| **Description** (de) | ... |
| **Description** (en) | A person, family, or organization contributing to a resource by revising or elucidating the content, e.g., adding an introduction, notes, or other critical matter. An editor may also prepare a resource for production, publication, or distribution. For major revisions, adaptations, etc., that substantially change the nature and content of the original work, resulting in a new work, see author. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4775"></a>
### Hierarchisch übergeordnete geographische Einheit
|     |     |
| --- | --- |
| **Name** | `gndo:hierarchicalSuperiorOfPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#hierarchicalSuperiorOfPlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#hierarchicalSuperiorOfPlaceOrGeographicName) |
| **Label** (de) | Hierarchisch übergeordnete geographische Einheit |
| **Label** (en) | Hierarchical superior of place or geographic name |
| **Description** (de) | Eine übergeordnete geographische Einheit (z.B. Körperschaft, Rechtssprechung) der beschriebenen Entität. |
| **Description** (en) | A hierarchically superordinate unit (corporate body, conference, jurisdiction) of the described unit (corporate body, conference, jurisdiction). |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538) |
| **Contained by** | [`place`](specs-elems.md#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.homepage"></a>
### Homepage (GNDO)
|     |     |
| --- | --- |
| **Name** | `gndo:homepage`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#homepage](https://d-nb.info/standards/elementset/gnd#homepage) |
| **Label** (de) | Homepage (GNDO) |
| **Label** (en) | homepage (GNDO) |
| **Description** (de) | URL einer Webpräsenz der Person, z.B. von Wikipedia. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:label`](specs-attrs.md#d17e6963)[`@iso-date`](specs-attrs.md#attr.iso-date)   |
| **Contained by** | [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`place`](specs-elems.md#d17e1265)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date"/>
   <attribute name="gndo:label"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2962"></a>
### Homepage (Kontakt)
|     |     |
| --- | --- |
| **Name** | `web`  |
| **Label** (de) | Homepage (Kontakt) |
| **Label** (en) | Homepage (Contact) |
| **Description** (de) | Ein URL zu einer offiziellen oder persönlichen Homepage. |
| **Description** (en) | - |
| **Contained by** | [`contact`](specs-elems.md#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.ref"></a>
### Hyperlink
|     |     |
| --- | --- |
| **Name** | `ref`  |
| **Label** (de) | Hyperlink |
| **Label** (en) | Hyperlink |
| **Description** (de) | Ein Link zu einer Webressource, die weitere Informationen über die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target` dokumentiert. |
| **Description** (en) | A link to another web ressource containing information about the described entity. |
| **Attributes** | [`@target`](specs-attrs.md#d17e6517) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`gndo:publication`](specs-elems.md#d17e5604)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="target"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:ref[not(@target)]" role="error">
      <sch:assert test="matches(text(), '^(https|http)://(\S+)$')">(No explizit URL): The content of `<sch:name/>` is not a
                        HTTP/HTTPS-URL! This is okay, but then you need to provide an HTTP/HTTPS-URL using `@target`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.idno"></a>
### Identifier
|     |     |
| --- | --- |
| **Name** | `idno`  |
| **Label** (de) | Identifier |
| **Label** (en) | Identifier |
| **Description** (de) | Ein Identifier für die Ressource, die hiermit beschrieben wird. |
| **Description** (en) | An Identifier of the Resource described. |
| **Attributes** | [`@type`](specs-attrs.md#d17e6189) |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e5604) |

***Content Model***  
```xml
<content>
   <attribute name="type" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5806"></a>
### In Beziehung stehendes Werk
|     |     |
| --- | --- |
| **Name** | `gndo:relatedWork`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#relatedWork](https://d-nb.info/standards/elementset/gnd#relatedWork) |
| **Label** (de) | In Beziehung stehendes Werk |
| **Label** (en) | Related work |
| **Description** (de) | ... |
| **Description** (en) | ... |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="instance-of"></a>
### Instanziierung von
|     |     |
| --- | --- |
| **Name** | `bf:instanceOf`  |
| **See also** | [bf:instantiates](http://bibfra.me/vocab/lite/instantiates) |
| **Label** (de) | Instanziierung von |
| **Label** (en) | Instance of |
| **Description** (de) | Verknüpfung auf ein Werk, das in der dokumentierten Manifestation instanziiert bzw. manifestiert wird. |
| **Description** (en) | Links to a Work the Instance described instantiates or manifests. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`manifestation`](specs-elems.md#d17e1518) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="bf:instanceOf/@ref[starts-with(., '#')]">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:let name="target" value="root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::entity:work]" role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work but is
                            "<sch:value-of select="$target/name()"/>"! </sch:assert>
      <!--<sch:report test="." role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work but is "<sch:value-of select="$target/name()"/>"! </sch:report>-->
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="contact-metadata"></a>
### Kontaktinformationen
|     |     |
| --- | --- |
| **Name** | `contact`  |
| **Label** (de) | Kontaktinformationen |
| **Label** (en) | Contact |
| **Description** (de) | Informationen, die zur Kontaktaufnahme genutzt werden können. |
| **Description** (en) | - |
| **Contained by** | [`respStmt`](specs-elems.md#respStmt) |
| **May contain** | [`address`](specs-elems.md#d17e2942)  [`mail`](specs-elems.md#d17e2902)  [`phone`](specs-elems.md#d17e2922)  [`web`](specs-elems.md#d17e2962) |

***Content Model***  
```xml
<content>
   <anyOrder>
      <element name="mail" required="true" repeatable="true"/>
      <element name="phone"/>
      <element name="address"/>
      <element name="web"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2845"></a>
### Kurzbeschreibung (Sammlung)
|     |     |
| --- | --- |
| **Name** | `abstract`  |
| **Label** (de) | Kurzbeschreibung (Sammlung) |
| **Label** (en) | Abstract (Collection) |
| **Description** (de) | Kurzbeschreibung eines Sammlungsabschnitts. |
| **Description** (en) | - |
| **Contained by** | [`list`](specs-elems.md#data-list) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="corporate-body-record"></a>
### Körperschaft
|     |     |
| --- | --- |
| **Name** | `corporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#corporateBody](https://d-nb.info/standards/elementset/gnd#corporateBody) |
| **Label** (de) | Körperschaft |
| **Label** (en) | Corporate Body |
| **Description** (de) | Ein Körperschaftsdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:type`](specs-attrs.md#d17e720)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:abbreviatedName`](specs-elems.md#d17e3272)  [`gndo:affiliation`](specs-elems.md#d17e3398)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:placeOfBusiness`](specs-elems.md#d17e5119)  [`gndo:precedingCorporateBody`](specs-elems.md#d17e5335)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:publication`](specs-elems.md#d17e5604)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:succeedingCorporateBody`](specs-elems.md#d17e5836)  [`gndo:temporaryName`](specs-elems.md#d17e5924)  [`gndo:topic`](specs-elems.md#d17e6005)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:placeOfBusiness"/>
      <element name="gndo:precedingCorporateBody"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:succeedingCorporateBody"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:corporateBody[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6206"></a>
### Label
|     |     |
| --- | --- |
| **Name** | `label`  |
| **Label** (de) | Label |
| **Label** (en) | Label |
| **Description** (de) | Ein menschenlesbares Label. |
| **Description** (en) | A humanreadable label. |
| **Contained by** | [`gndo:organizerOrHost`](specs-elems.md#d17e4943)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-list"></a>
### Liste (Daten Bereich)
|     |     |
| --- | --- |
| **Name** | `list`  |
| **Label** (de) | Liste (Daten Bereich) |
| **Label** (en) | List (Data Area) |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@id`](specs-attrs.md#d17e7042)   |
| **Contained by** | [`data`](specs-elems.md#data-section) |
| **May contain** | [`abstract`](specs-elems.md#d17e2845)  [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`title`](specs-elems.md#d17e6862)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="id"/>
   <element name="title"/>
   <element name="abstract"/>
   <anyOrder>
      <element name="corporateBody" repeatable="true"/>
      <element name="entity" repeatable="true"/>
      <element name="event" repeatable="true"/>
      <element name="expression" repeatable="true"/>
      <element name="manifestation" repeatable="true"/>
      <element name="person" repeatable="true"/>
      <element name="place" repeatable="true"/>
      <element name="subjectHeading" repeatable="true"/>
      <element name="work" repeatable="true"/>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:list/element()[@xml:id][@agency][not(@agency = 'ignore')]"
             role="error">
      <sch:assert test="entity:revision">(Missing revision description): If you are going to exchange your data with a GND
                                Agency you need to provide a revision description for the Entry encoded!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2902"></a>
### Mailadresse
|     |     |
| --- | --- |
| **Name** | `mail`  |
| **Label** (de) | Mailadresse |
| **Label** (en) | Mailaddress |
| **Description** (de) | Email Adresse zur Kontaktaufnahme. |
| **Description** (en) | - |
| **Contained by** | [`contact`](specs-elems.md#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1518"></a>
### Manifestation
|     |     |
| --- | --- |
| **Name** | `manifestation`  |
| **Label** (de) | Manifestation |
| **Label** (en) | Manifestation |
| **Description** (de) | Manifestation einer Ausdrucksform eines Werks nach FRBR. |
| **Description** (en) | Manifestation of an Expression of a Work (FRBR). |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`bf:instanceOf`](specs-elems.md#instance-of)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`embodimentOf`](specs-elems.md#d17e1553)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="bf:instanceOf"/>
      <element name="embodimentOf"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6226"></a>
### Mapping
|     |     |
| --- | --- |
| **Name** | `mapping`  |
| **Label** (de) | Mapping |
| **Label** (en) | Mapping |
| **Description** (de) | Mapping Sektion, in der Labels und äquivalente Terme aus der GND dokumentiert werden können. |
| **Description** (en) | Mapping section to document labels and equivalent GND terms. |
| **Contained by** | [`entityXML`](specs-elems.md#entity-xml) |
| **May contain** | [`mappingLabel`](specs-elems.md#d17e6259)[`metadata`](specs-elems.md#collection-metadata)   |

***Content Model***  
```xml
<content>
   <element name="metadata"/>
   <element name="mappingLabel" required="true" repeatable="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6259"></a>
### Mapping-Label
|     |     |
| --- | --- |
| **Name** | `mappingLabel`  |
| **Label** (de) | Mapping-Label |
| **Label** (en) | Mapping-Label |
| **Description** (de) | Ein Labels, dass auf einen äquivalenten Terme aus der GND gemapped wird. |
| **Description** (en) | A labels being mapped to an equivalent GND term. |
| **Attributes** | [`@name`](specs-attrs.md#d17e7229)  [`@type`](specs-attrs.md#d17e7241)  [`@when`](specs-attrs.md#d17e7724)   |
| **Contained by** | [`mapping`](specs-elems.md#d17e6226) |
| **May contain** | [`term`](specs-elems.md#d17e6829) |

***Content Model***  
```xml
<content>
   <attribute name="name" required="true"/>
   <attribute name="type"/>
   <attribute name="when"/>
   <element name="term" repeatable="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="collection-metadata"></a>
### Metadaten
|     |     |
| --- | --- |
| **Name** | `metadata`  |
| **Label** (de) | Metadaten |
| **Label** (en) | Metadata |
| **Description** (de) | Bereich, in dem grundlegende Metadaten z.B. zu einer Datensammlung oder einem Mapping (z.B. eine Kurzbeschreibung der Sammlung, Angaben zum Datenlieferant, eine Revisionsbeschreibung der Sammlung etc.) dokumentiert werden können. |
| **Description** (en) | Section where Metadata e.g. with regard to the Data Collection is described, e.g. a brief description of the collection, information about the data provider, revision descriptions etc. |
| **Contained by** | [`collection`](specs-elems.md#collection)  [`mapping`](specs-elems.md#d17e6226) |
| **May contain** | [`abstract`](specs-elems.md#d17e6334)  [`agency`](specs-elems.md#agency-stmt)  [`provider`](specs-elems.md#data-provider)  [`respStmt`](specs-elems.md#respStmt)  [`revision`](specs-elems.md#revision)[`title`](specs-elems.md#d17e6862)   |

***Content Model***  
```xml
<content>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract" required="true"/>
      <element name="respStmt" repeatable="true"/>
      <element name="provider" required="true"/>
      <element name="agency"/>
      <element name="revision" required="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5836"></a>
### Nachfolgende Körperschaft (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:succeedingCorporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#succeedingCorporateBody](https://d-nb.info/standards/elementset/gnd#succeedingCorporateBody) |
| **Label** (de) | Nachfolgende Körperschaft (Körperschaft) |
| **Label** (en) | Succeeding corporate body (Corporal Body) |
| **Description** (de) | Eine Nachfolgeinstitution der Körperschaft. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5866"></a>
### Nachfolgendes Geografikum
|     |     |
| --- | --- |
| **Name** | `gndo:succeedingPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#succeedingPlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#succeedingPlaceOrGeographicName) |
| **Label** (de) | Nachfolgendes Geografikum |
| **Label** (en) | Succeeding place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`place`](specs-elems.md#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5896"></a>
### Nachname
|     |     |
| --- | --- |
| **Name** | `gndo:surname`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#surname](https://d-nb.info/standards/elementset/gnd#surname) |
| **Label** (de) | Nachname |
| **Label** (en) | Surname |
| **Description** (de) | Der Nach- oder Familienname, unter dem eine Person bekannt ist. |
| **Description** (en) | Last name of a person |
| **Attributes** | [`@cert`](specs-attrs.md#d17e6885) |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName) |

***Content Model***  
```xml
<content>
   <attribute name="cert"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6400"></a>
### Name (Verantworlichkeit)
|     |     |
| --- | --- |
| **Name** | `name`  |
| **Label** (de) | Name (Verantworlichkeit) |
| **Label** (en) | Name (Responsibility Statement) |
| **Description** (de) | Name der verantwortlichen Person oder Institution. |
| **Description** (en) | Name of a person or institution taking a certain responsibility with regard to a datacollection. |
| **Contained by** | [`respStmt`](specs-elems.md#respStmt) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5463"></a>
### Namenspräfix
|     |     |
| --- | --- |
| **Name** | `gndo:prefix`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#prefix](https://d-nb.info/standards/elementset/gnd#prefix) |
| **Label** (de) | Namenspräfix |
| **Label** (en) | Prefix |
| **Description** (de) | Namensteil, der dem Namen vorangestellt sind (z.B. Adelskennzeichnungen, "von", "zu" etc.). |
| **Description** (en) | Part of a name, which is prefixed. |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4918"></a>
### Namenszusatz
|     |     |
| --- | --- |
| **Name** | `gndo:nameAddition`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#nameAddition](https://d-nb.info/standards/elementset/gnd#nameAddition) |
| **Label** (de) | Namenszusatz |
| **Label** (en) | Name addition |
| **Description** (de) | Zusätzliches Element des Namens, unter dem eine Person bekannt ist, z.B. "Graf von Wallmoden". |
| **Description** (en) | - |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3502"></a>
### Oberbegriff
|     |     |
| --- | --- |
| **Name** | `gndo:broaderTerm`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#broaderTerm](https://d-nb.info/standards/elementset/gnd#broaderTerm), [https://d-nb.info/standards/elementset/gnd#broaderTermGeneral](https://d-nb.info/standards/elementset/gnd#broaderTermGeneral), [https://d-nb.info/standards/elementset/gnd#broaderTermGeneric](https://d-nb.info/standards/elementset/gnd#broaderTermGeneric), [https://d-nb.info/standards/elementset/gnd#broaderTermInstantial](https://d-nb.info/standards/elementset/gnd#broaderTermInstantial), [https://d-nb.info/standards/elementset/gnd#broaderTermPartitive](https://d-nb.info/standards/elementset/gnd#broaderTermPartitive), [https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement](https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement) |
| **Label** (de) | Oberbegriff |
| **Label** (en) | Broader term |
| **Description** (de) | Oberbegriff bzw. Oberklasse der beschriebenen Entität. |
| **Description** (en) | A Broader term for the concept the described entity is an instance of. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@gndo:type`](specs-attrs.md#d17e3541)[`@ref`](specs-attrs.md#d17e7662)  [`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:broaderTerm">
      <sch:assert test="@gndo:ref" role="error">broader-term.1: (Descriptor URI missing): It is mandatory to provide an URI referenze of
                        a GND-Descriptor via `@gndo:ref` for `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-place-standard"></a>
### Ort
|     |     |
| --- | --- |
| **Name** | `gndo:place`  |
| **See also** | [gndo:place](https://d-nb.info/standards/elementset/gnd#place) |
| **Label** (de) | Ort |
| **Label** (en) | Place |
| **Description** (de) | Ein Land, Staat, Provinz, Ort etc., wo die beschriebene Entität angesiedelt ist, stattfand o.Ä. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND-Systematik für diesen Ort erfasst werden. |
| **Description** (en) | A country, state, province, etc., or place where the entity described is placed, happend etc. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`event`](specs-elems.md#d17e320)  [`place`](specs-elems.md#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:place">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for the encoded Place
                        in `<sch:name/>` via `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record"></a>
### Person (Record)
|     |     |
| --- | --- |
| **Name** | `person`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#Person](https://d-nb.info/standards/elementset/gnd#Person) |
| **Label** (de) | Person (Record) |
| **Label** (en) | Person (Record) |
| **Description** (de) | Ein Personendatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:type`](specs-attrs.md#person-types)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`foaf:page`](specs-elems.md#elem.foaf.page)  [`gndo:academicDegree`](specs-elems.md#d17e3318)  [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:affiliation`](specs-elems.md#d17e3398)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)  [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:fieldOfStudy`](specs-elems.md#d17e4451)  [`gndo:functionOrRole`](specs-elems.md#d17e4550)  [`gndo:gender`](specs-elems.md#d17e4581)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)  [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)  [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)  [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)  [`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:professionOrOccupation`](specs-elems.md#d17e5488)  [`gndo:pseudonym`](specs-elems.md#d17e5556)  [`gndo:publication`](specs-elems.md#d17e5604)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:titleOfNobility`](specs-elems.md#d17e5969)  [`gndo:topic`](specs-elems.md#d17e6005)  [`gndo:variantName`](specs-elems.md#person-record-variantName)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:acquaintanceshipOrFriendship" repeatable="true"/>
      <element name="gndo:academicDegree"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfBirth"/>
      <element name="gndo:dateOfDeath"/>
      <element name="gndo:periodOfActivity" repeatable="true"/>
      <element name="gndo:familialRelationship" repeatable="true"/>
      <element name="gndo:fieldOfStudy" repeatable="true"/>
      <element name="gndo:functionOrRole" repeatable="true"/>
      <element name="gndo:gender"/>
      <element name="gndo:placeOfActivity" repeatable="true"/>
      <element name="gndo:placeOfBirth"/>
      <element name="gndo:placeOfDeath"/>
      <element name="gndo:playedInstrument" repeatable="true"/>
      <choice>
         <element name="gndo:preferredName"/>
         <group>
            <element name="gndo:preferredName" required="true"/>
            <element name="gndo:preferredName" required="true"/>
         </group>
      </choice>
      <element name="gndo:professionOrOccupation" repeatable="true"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:pseudonym" repeatable="true"/>
      <element name="gndo:titleOfNobility" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person[not(@gndo:uri)]">
      <sch:assert test="gndo:geographicAreaCode" role="error">(Geographic Area Code is missing): It is mandatory to provide a
                        `gndo:geographicAreaCode` for a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person/gndo:periodOfActivity">
      <sch:report test="parent::node()[gndo:dateOfBirth or gndo:dateOfDeath]" role="info">(Period of Activity and Dates of Living): Providing gndo:periodOfActivity and 
                        Dates of Living (gndo:dateOfBirth and/or gndo:dateOfDeath) in combination is not supported by the GND-Specifications and thus could lead to problems during dataimport!</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person/gndo:preferredName[@type='original']">
      <sch:assert test="parent::node()[gndo:preferredName[not(@type)]]" role="error">(No corresponding standard preferred name) You are only allowed to document an original form of a preferred name if you already encoded a standard preferred name!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person/gndo:preferredName[not(@type)]">
      <sch:report test="count(parent::node()/gndo:preferredName[not(@type)]) = 2"
                  role="error">(Original preferred Name not marked) If you provide two preferred names one must be marked as the original form using `@type='original'`!</sch:report>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5276"></a>
### Persönlicher Name
|     |     |
| --- | --- |
| **Name** | `gndo:personalName`  |
| **See also** | [gndo:personalName](https://d-nb.info/standards/elementset/gnd#personalName) |
| **Label** (de) | Persönlicher Name |
| **Label** (en) | Personal name |
| **Description** (de) | Ein Name, unter dem die Person gemeinhin bekannt ist. Ein persönlicher Name wird dann angegeben, wenn die Person nicht durch einen Vor- oder Nachname benannt wird, z.B. "Aristoteles" |
| **Description** (en) | - |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5556"></a>
### Pseudonym
|     |     |
| --- | --- |
| **Name** | `gndo:pseudonym`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#pseudonym](https://d-nb.info/standards/elementset/gnd#pseudonym) |
| **Label** (de) | Pseudonym |
| **Label** (en) | Pseudonym |
| **Description** (de) | Ein Pseudonym, unter dem die dokumentierte Person ebenfalls bekannt ist. |
| **Description** (en) | Links a person's real identity to an identity under which one or more persons act, e. g. write, compose or create art, but that is not the person's real name (i. e. a pseudonym). |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:pseudonym">
      <sch:assert test="@gndo:ref or @ref" role="warning">(Missing Reference to Pseudonym Record): It is recommended to provide a
                        GNDO-URI to a GND-Record of this Pseudonym or to a `person[@type='https://d-nb.info/standards/elementset/gnd#Pseudonym']` in
                        this dataset using `@ref`.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5604"></a>
### Publikation
|     |     |
| --- | --- |
| **Name** | `gndo:publication`  |
| **See also** | [gndo:publication](https://d-nb.info/standards/elementset/gnd#publication) |
| **Label** (de) | Publikation |
| **Label** (en) | publication |
| **Description** (de) | Eine Publikation, die mit der Entität in Zusammenhang steht. Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden sind und auch (3) um Titel über die Entität handeln. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@dnb:catalogue`](specs-attrs.md#d17e5638)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)  [`@role`](specs-attrs.md#d17e5652)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`person`](specs-elems.md#person-record) |
| **May contain** | [`add`](specs-elems.md#d17e2597)  [`date`](specs-elems.md#d17e2985)[`gndo:firstAuthor`](specs-elems.md#d17e4491)  [`idno`](specs-elems.md#elem.idno)  [`ref`](specs-elems.md#elem.ref)  [`title`](specs-elems.md#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="dnb:catalogue"/>
   <attribute name="role"/>
   <anyOrder>
      <element name="ref"/>
      <element name="idno" repeatable="true"/>
      <choice>
         <anyOrder>
            <element name="gndo:firstAuthor" repeatable="true"/>
            <element name="title" required="true"/>
            <element name="add"/>
            <element name="date" required="true"/>
            <text/>
         </anyOrder>
         <text/>
      </choice>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication">
      <sch:let name="ref" value="substring-after(data(@ref), '#')"/>
      <sch:let name="internal-work-record" value="//entity:work[@xml:id = $ref]"/>
      <sch:assert test="@gndo:ref or @dnb:catalogue or not(empty($internal-work-record))"
                  role="warning">[WARN] To identify a
                        publication it is recommended to provide either a Reference to a Record in the DNB Catalogue (`@dnb:catalogue`) or an URI of a
                        Work Entity in the GND (`@gndo:ref`).</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[@dnb:catalogue]">
      <sch:report test="@agency='remove'" role="warning">[WARN] (NO GND Data) Your request concerns data that is part of the DNB
                        Catalogue. A GND-Agency doesn't have any access to these datasets.</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[not(@role) or @role='author']">
      <sch:report test="gndo:firstAuthor" role="error">(Who is the author?) You didn't use `@role` which implies that the publication's
                        author is actually the entity described! If so you must not use the `author` element again within the publication! Otherwise
                        you need to be explizit about the Role of the entity described using `@role` on the publication!</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[@role='about']">
      <sch:assert test="gndo:firstAuthor" role="error">(Missing author) If the publication is ABOUT the entity described you need to
                        encode the primary author of the publication!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.source"></a>
### Quellenangabe
|     |     |
| --- | --- |
| **Name** | `source`  |
| **Label** (de) | Quellenangabe |
| **Label** (en) | Source |
| **Description** (de) | Angaben zu einer Quelle, die verwendet wurde, um die in diesem Eintrag gegebenen Informationen zu recherchieren. |
| **Description** (en) | A source which was used to identitfy the resource. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@url`](specs-attrs.md#d17e6713)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |
| **May contain** | [`note`](specs-elems.md#add-note)[`title`](specs-elems.md#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="url"/>
   <choice>
      <group>
         <anyOrder>
            <element name="title" required="true"/>
            <element name="note"/>
         </anyOrder>
      </group>
      <text/>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="revision"></a>
### Revisionsbeschreibung
|     |     |
| --- | --- |
| **Name** | `revision`  |
| **Label** (de) | Revisionsbeschreibung |
| **Label** (en) | Revision Description |
| **Description** (de) | Statusinformationen zu einer Ressource (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der Revisionsbeschreibung! |
| **Description** (en) | Description containing information about the editing status of the ressource. |
| **Attributes** | [`@status`](specs-attrs.md#rev-status)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`metadata`](specs-elems.md#collection-metadata)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |
| **May contain** | [`change`](specs-elems.md#d17e2624) |

***Content Model***  
```xml
<content>
   <attribute name="status" required="true"/>
   <element name="change" repeatable="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:revision[@status = 'closed']" role="error">
      <sch:assert test="(entity:change[@status])[1][@status = ('published', 'withdrawn')]">(Missing prerequisites): You can't close a
                        resource if it is not at least published or withdrawn!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:revision[@status ='staged']" role="error">
      <sch:assert test="(entity:change[@status])[1][@status=('candidate', 'approved', 'embargoed', 'submitted', 'published')]">(Missing
                        prerequisites): You can't stage a ressource as long as the ressource is not a candidate, approved or embargoed!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2624"></a>
### Revisionseintrag
|     |     |
| --- | --- |
| **Name** | `change`  |
| **Label** (de) | Revisionseintrag |
| **Label** (en) | Change |
| **Description** (de) | Zusammenfassende Beschreibung der vorgenommenen Änderung an einer Informationsressource (Sammlung oder Eintrag). Der aktuellste Eintrag steht immer am Anfang der Revisionsbeschreibung. |
| **Description** (en) | Summary of the change made to the informationressource. |
| **Attributes** | [`@status`](specs-attrs.md#change-status)[`@when`](specs-attrs.md#d17e7724)  [`@who`](specs-attrs.md#d17e2640)   |
| **Contained by** | [`revision`](specs-elems.md#revision) |

***Content Model***  
```xml
<content>
   <attribute name="when" required="true"/>
   <attribute name="who" required="true"/>
   <attribute name="status"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:change[@when][preceding-sibling::entity:change]"
             role="error">
      <sch:let name="preceeding-change" value="preceding-sibling::entity:change[1]"/>
      <sch:assert test="xs:date($preceeding-change/@when) &gt; xs:date(@when) or xs:date($preceeding-change/@when) = xs:date(@when)">
                        (Youngest always on top!) A change entry of a younger date must be placed on top of older entries aka. the youngest is always
                        the first in the list! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:data//entity:change[@status = ('candidate', 'cleared')]"
             role="warning">
      <sch:assert test="parent::entity:revision/parent::element()[@agency]"> [WARN] (Agency Action recommended): It is recommended to
                        provide information on the action, a GND agency is expected to do with regard to the record! So please use `@agency` on the
                        record to choose an action. </sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:change[@status = ('approved', 'submitted', 'published')]"
             role="error">
      <sch:let name="who-id" value="data(@who)"/>
      <sch:let name="resp-person"
               value="ancestor::entity:collection/entity:metadata//entity:respStmt[@id = $who-id]"/>
      <sch:assert test="$resp-person[parent::entity:agency]"> (Missing authority): Sorry <sch:value-of select="$resp-person/entity:name/text()"/>, you don't have permission to set the resources stage to "<sch:value-of select="data(@status)"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6383"></a>
### Rolle (Verantwortlichkeit)
|     |     |
| --- | --- |
| **Name** | `resp`  |
| **Label** (de) | Rolle (Verantwortlichkeit) |
| **Label** (en) | Role (Responsibility Statement) |
| **Description** (de) | Eine Rolle, die der Person oder Institution im Rahmen der Bearbeitung einer Datensammlung zugeschrieben wird. |
| **Description** (en) | A role attributed to a person or institution with regard to a datacollection. |
| **Contained by** | [`respStmt`](specs-elems.md#respStmt) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1592"></a>
### Schlagwort
|     |     |
| --- | --- |
| **Name** | `subjectHeading`  |
| **Label** (de) | Schlagwort |
| **Label** (en) | Subject Heading |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:topic`](specs-elems.md#d17e6005)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5119"></a>
### Sitz (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfBusiness`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfBusiness](https://d-nb.info/standards/elementset/gnd#placeOfBusiness) |
| **Label** (de) | Sitz (Körperschaft) |
| **Label** (en) | Place of Business (Corporal Body) |
| **Description** (de) | Der (Haupt)sitz der Körperschaft. |
| **Description** (en) | (Main)place of the Business. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4843"></a>
### Sprachraum
|     |     |
| --- | --- |
| **Name** | `gndo:languageCode`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#languageCode](https://d-nb.info/standards/elementset/gnd#languageCode) |
| **Label** (de) | Sprachraum |
| **Label** (en) | language code |
| **Description** (de) | Code(s) des Sprachraumes, dem die Entität zugeordnet werden kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben werden! |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:code`](specs-attrs.md#d17e4864) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:code" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-dateOfDeath"></a>
### Sterbedatum (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfDeath`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#dateOfDeath](https://d-nb.info/standards/elementset/gnd#dateOfDeath) |
| **Label** (de) | Sterbedatum (Person) |
| **Label** (en) | Date of death (Person) |
| **Description** (de) | Das Sterbedatum der Person. |
| **Description** (en) | The death date of a person. This can be a year, a year-month combination or a full date. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@cert`](specs-attrs.md#d17e6885)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-date`](specs-attrs.md#attr.iso-date)  [`@iso-notAfter`](specs-attrs.md#attr.iso-notAfter)[`@iso-notBefore`](specs-attrs.md#attr.iso-notBefore)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfDeath"></a>
### Sterbeort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfDeath`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfDeath](https://d-nb.info/standards/elementset/gnd#placeOfDeath) |
| **Label** (de) | Sterbeort (Person) |
| **Label** (en) | Place of death (Person) |
| **Description** (de) | Der Sterbeort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfDeath">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via
                        `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4451"></a>
### Studienfach
|     |     |
| --- | --- |
| **Name** | `gndo:fieldOfStudy`  |
| **See also** | [gndo:fieldOfStudy](https://d-nb.info/standards/elementset/gnd#fieldOfStudy) |
| **Label** (de) | Studienfach |
| **Label** (en) | Field of study |
| **Description** (de) | Das Studienfach bzw. -gebiet einer Person. Über `@gndo:ref` kann ein entsprechendes Schlagwort aus der GND mit erfasst werden. |
| **Description** (en) | A person’s field of study. Use `@gndo:ref` to encode an additional GND Subject Category. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:fieldOfStudy">
      <sch:assert test="@gndo:ref" role="warning">[WARN](Missing Subject Category URI) It is recommended to provide an URI of a GND
                        Subject Category using `@gndo:ref`.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2922"></a>
### Telefon
|     |     |
| --- | --- |
| **Name** | `phone`  |
| **Label** (de) | Telefon |
| **Label** (en) | phone |
| **Description** (de) | Telefonnummer zur Kontaktaufnahme |
| **Description** (en) | - |
| **Contained by** | [`contact`](specs-elems.md#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6829"></a>
### Term (Mapping)
|     |     |
| --- | --- |
| **Name** | `term`  |
| **Label** (de) | Term (Mapping) |
| **Label** (en) | Term (Mapping) |
| **Description** (de) | Ein Term aus der GND als Deskriptor. |
| **Description** (en) | A GND term as descriptor. |
| **Attributes** | [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@type`](specs-attrs.md#d17e7241) |
| **Contained by** | [`mappingLabel`](specs-elems.md#d17e6259) |

***Content Model***  
```xml
<content>
   <attribute name="gndo:ref" required="true"/>
   <attribute name="type"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6005"></a>
### Thema
|     |     |
| --- | --- |
| **Name** | `gndo:topic`  |
| **See also** | [gndo:topic](https://d-nb.info/standards/elementset/gnd#topic) |
| **Label** (de) | Thema |
| **Label** (en) | Topic |
| **Description** (de) | Thema, das sich auf eine Körperschaft, eine Konferenz, eine Person, eine Familie, ein Thema oder ein Werk bezieht. |
| **Description** (en) | Topic that is related to a corporate body, conference, person, family, subject heading or work. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`event`](specs-elems.md#d17e320)  [`person`](specs-elems.md#person-record)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6862"></a>
### Titel
|     |     |
| --- | --- |
| **Name** | `title`  |
| **Label** (de) | Titel |
| **Label** (en) | Title |
| **Description** (de) | Ein Titel für das Informationsobjekt, das in diesem Kontext beschrieben wird. |
| **Description** (en) | A title of the information-object, described in this very context. |
| **Contained by** | [`agency`](specs-elems.md#agency-stmt)  [`gndo:publication`](specs-elems.md#d17e5604)  [`list`](specs-elems.md#data-list)  [`metadata`](specs-elems.md#collection-metadata)  [`provider`](specs-elems.md#data-provider)  [`source`](specs-elems.md#elem.source) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3011"></a>
### Titel (Record)
|     |     |
| --- | --- |
| **Name** | `dc:title`  |
| **See also** | [dc:title](http://purl.org/dc/elements/1.1/title) |
| **Label** (de) | Titel (Record) |
| **Label** (en) | title (record) |
| **Description** (de) | Ein Titel für die Ressource. `dc:title` wird dann zur Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll. |
| **Description** (en) | A title of the record if no preferred or variant name is provided. |
| **Attributes** | [`@xml:lang`](specs-attrs.md#d17e7157) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`expression`](specs-elems.md#d17e1449)  [`manifestation`](specs-elems.md#d17e1518)  [`person`](specs-elems.md#person-record)  [`place`](specs-elems.md#d17e1265)  [`subjectHeading`](specs-elems.md#d17e1592)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="dc:title" role="warning">
      <sch:assert test="parent::node()[not(gndo:preferredName)]">(`dc:title` superfluous): <sch:name/> already contains a preferred
                        name(`gndo:preferredName`) which makes `dc:title` superfluous!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="node()[parent::entity:list][self::entity:*][@gndo:uri or @xml:id][not(gndo:preferredName)][not(gndo:variantName)][not(self::entity:entity)]"
             role="warning">
      <sch:assert test="dc:title">[WARN] (fallback `dc:title` recommended): It is recommended to use `dc:title` to encode a fallback
                        title for `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2597"></a>
### Titelzusatz
|     |     |
| --- | --- |
| **Name** | `add`  |
| **Label** (de) | Titelzusatz |
| **Label** (en) | Addition to the title |
| **Description** (de) | Zusätze zum Titel im Rahmen der bibliographischen Angabe. |
| **Description** (en) | Additions to the title in the biblipgraphic reference. |
| **Contained by** | [`gndo:publication`](specs-elems.md#d17e5604) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-variantName"></a>
### Variante Namensform (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:variantName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#variantName](https://d-nb.info/standards/elementset/gnd#variantName) |
| **Label** (de) | Variante Namensform (Person) |
| **Label** (en) | Variant Name (Person) |
| **Description** (de) | Eine alternative Namensform, unter der eine Person ebenfalls bekannt ist. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@script`](specs-attrs.md#d17e7703)  [`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |
| **May contain** | [`gndo:counting`](specs-elems.md#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](specs-elems.md#d17e4311)[`gndo:forename`](specs-elems.md#d17e4522)  [`gndo:nameAddition`](specs-elems.md#d17e4918)  [`gndo:personalName`](specs-elems.md#d17e5276)  [`gndo:prefix`](specs-elems.md#d17e5463)  [`gndo:surname`](specs-elems.md#d17e5896)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="script"/>
   <choice>
      <element name="gndo:personalName" required="true"/>
      <anyOrder>
         <element name="gndo:forename" required="true"/>
         <element name="gndo:prefix"/>
         <element name="gndo:surname" required="true"/>
         <element name="gndo:counting"/>
         <text/>
      </anyOrder>
   </choice>
   <anyOrder>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
   </anyOrder>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.variantName.standard"></a>
### Variante Namensform (Standard)
|     |     |
| --- | --- |
| **Name** | `gndo:variantName`  |
| **See also** | [gndo:variantName](https://d-nb.info/standards/elementset/gnd#variantName), [gndo:variantNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#variantNameForTheCorporateBody), [gndo:variantNameForTheWork](https://d-nb.info/standards/elementset/gnd#variantNameForTheWork), [gndo:variantNameForThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#variantNameForThePlaceOrGeographicName) |
| **Label** (de) | Variante Namensform (Standard) |
| **Label** (en) | Variant Name (Standard) |
| **Description** (de) | Eine alternative Namensform, um die beschriebenen Entität zu bezeichnen. |
| **Description** (en) | A variant name to denominate the documented Entity. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@script`](specs-attrs.md#d17e7703)[`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`place`](specs-elems.md#d17e1265)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="script"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4943"></a>
### Veranstalter oder Gastgeber
|     |     |
| --- | --- |
| **Name** | `gndo:organizerOrHost`  |
| **See also** | [gndo:organizerOrHost](https://d-nb.info/standards/elementset/gnd#organizerOrHost) |
| **Label** (de) | Veranstalter oder Gastgeber |
| **Label** (en) | Organizer or host |
| **Description** (de) | Eine Person, Familie oder Organisation, die die Ausstellung, Veranstaltung, Konferenz usw. organisiert, die zu einer Ressource geführt hat. |
| **Description** (en) | A person, family, or organization organizing the exhibit, event, conference, etc., which gave rise to a resource. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)   |
| **Contained by** | [`event`](specs-elems.md#d17e320) |
| **May contain** | [`label`](specs-elems.md#d17e6206)  [`note`](specs-elems.md#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <choice>
      <text/>
      <group>
         <element name="label" required="true"/>
         <element name="note" required="true"/>
      </group>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4073"></a>
### Veranstaltungsdaten
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfConferenceOrEvent`  |
| **See also** | [gndo:dateOfConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#dateOfConferenceOrEvent) |
| **Label** (de) | Veranstaltungsdaten |
| **Label** (en) | Date of conference or event |
| **Description** (de) | Zeitraum, innerhalb dessen das beschriebene Event stattfand. |
| **Description** (en) | Timespan of the event described. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-from`](specs-attrs.md#attr.iso-from)  [`@iso-to`](specs-attrs.md#attr.iso-to) |
| **Contained by** | [`event`](specs-elems.md#d17e320) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="respStmt"></a>
### Verantwortlichkeit (Metadaten)
|     |     |
| --- | --- |
| **Name** | `respStmt`  |
| **Label** (de) | Verantwortlichkeit (Metadaten) |
| **Label** (en) | Responsibility Statement (Metadata) |
| **Description** (de) | Angabe zur Rolle einer Person oder Institution in Hinblick auf die Bearbeitung einer Datensammlung. |
| **Description** (en) | A statement about responsibilities of a person or institution with regard to a datacollection. |
| **Attributes** | [`@id`](specs-attrs.md#d17e7042)   |
| **Contained by** | [`agency`](specs-elems.md#agency-stmt)  [`metadata`](specs-elems.md#collection-metadata)  [`provider`](specs-elems.md#data-provider) |
| **May contain** | [`contact`](specs-elems.md#contact-metadata)[`name`](specs-elems.md#d17e6400)  [`resp`](specs-elems.md#d17e6383)   |

***Content Model***  
```xml
<content>
   <attribute name="id" required="true"/>
   <element name="resp" required="true"/>
   <element name="name" required="true"/>
   <element name="contact"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3422"></a>
### Verfasser
|     |     |
| --- | --- |
| **Name** | `gndo:author`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#author](https://d-nb.info/standards/elementset/gnd#author) |
| **Label** (de) | Verfasser |
| **Label** (en) | Author |
| **Description** (de) | (Verfasser) |
| **Description** (en) | A person, family, or organization responsible for creating a work that is primarily textual in content, regardless of media type (e.g., printed text, spoken word, electronic text, tactile text) or genre (e.g., poems, novels, screenplays, blogs). Use also for persons, etc., creating a new work by paraphrasing, rewriting, or adapting works by another creator such that the modification has substantially changed the nature and content of the original or changed the medium of expression. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:author">
      <sch:assert test="parent::element()/gndo:firstAuthor" role="error">(Missing first author) You need to use gndo:firstAuthor for the main author of a work. gndo:author is used to describe further authors.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5335"></a>
### Vorherige Körperschaft (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:precedingCorporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#precedingCorporateBody](https://d-nb.info/standards/elementset/gnd#precedingCorporateBody) |
| **Label** (de) | Vorherige Körperschaft (Körperschaft) |
| **Label** (en) | Preceding corporate body (Corporal Body) |
| **Description** (de) | Eine Vorgängerinstitution der Körperschaft. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5302"></a>
### Vorheriges Geografikum
|     |     |
| --- | --- |
| **Name** | `gndo:precedingPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#precedingPlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#precedingPlaceOrGeographicName) |
| **Label** (de) | Vorheriges Geografikum |
| **Label** (en) | Preceding place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`place`](specs-elems.md#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4888"></a>
### Vorlage
|     |     |
| --- | --- |
| **Name** | `gndo:literarySource`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#literarySource](https://d-nb.info/standards/elementset/gnd#literarySource) |
| **Label** (de) | Vorlage |
| **Label** (en) | Literary source |
| **Description** (de) | Die beschriebene Entität ist eine Instanziierung des verknüpften Werks. |
| **Description** (en) | The described entity is a realization of the related work. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4522"></a>
### Vorname
|     |     |
| --- | --- |
| **Name** | `gndo:forename`  |
| **See also** | [gndo:forename](https://d-nb.info/standards/elementset/gnd#forename) |
| **Label** (de) | Vorname |
| **Label** (en) | Forename |
| **Description** (de) | Ein oder mehrere Vornamen einer Person. |
| **Description** (en) | One or many prenames of person. |
| **Attributes** | [`@cert`](specs-attrs.md#d17e6885) |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName) |

***Content Model***  
```xml
<content>
   <attribute name="cert"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-preferredName"></a>
### Vorzugsbenennung (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:preferredName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#preferredName](https://d-nb.info/standards/elementset/gnd#preferredName) |
| **Label** (de) | Vorzugsbenennung (Person) |
| **Label** (en) | Preferred Name (Person) |
| **Description** (de) | Die bevorzugte Namensform zur Denomination einer Person. |
| **Description** (en) | A preferred name to denominate a person. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@script`](specs-attrs.md#d17e7703)  [`@type`](specs-attrs.md#d17e5396)  [`@xml:lang`](specs-attrs.md#d17e7157)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |
| **May contain** | [`gndo:counting`](specs-elems.md#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](specs-elems.md#d17e4311)[`gndo:forename`](specs-elems.md#d17e4522)  [`gndo:nameAddition`](specs-elems.md#d17e4918)  [`gndo:personalName`](specs-elems.md#d17e5276)  [`gndo:prefix`](specs-elems.md#d17e5463)  [`gndo:surname`](specs-elems.md#d17e5896)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="script"/>
   <attribute name="type"/>
   <choice>
      <element name="gndo:personalName" required="true"/>
      <anyOrder>
         <element name="gndo:forename" required="true"/>
         <element name="gndo:prefix"/>
         <element name="gndo:surname" required="true"/>
         <element name="gndo:counting"/>
         <text/>
      </anyOrder>
   </choice>
   <anyOrder>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:preferredName[@type='original']">
      <sch:assert test="@script" role="error">(Missing script statement): It is required to explicitly provide script information via `@script` regarding the original form of the preferred name.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5423"></a>
### Vorzugsbenennung (Standard)
|     |     |
| --- | --- |
| **Name** | `gndo:preferredName`  |
| **See also** | [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName), [gndo:preferredNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#preferredNameForTheCorporateBody), [gndo:preferredNameForTheWork](https://d-nb.info/standards/elementset/gnd#preferredNameForTheWork) |
| **Label** (de) | Vorzugsbenennung (Standard) |
| **Label** (en) | preferred name (default) |
| **Description** (de) | Eine bevorzugte Namensform, um die beschriebenen Entität zu bezeichnen. |
| **Description** (en) | A preferred name to denominate the documented Entity. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@xml:lang`](specs-attrs.md#d17e7157) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`entity`](specs-elems.md#entity-record)  [`event`](specs-elems.md#d17e320)  [`place`](specs-elems.md#d17e1265)  [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.foaf.page"></a>
### Webseite
|     |     |
| --- | --- |
| **Name** | `foaf:page`  |
| **See also** | [foaf:page](http://xmlns.com/foaf/0.1/#term_page) |
| **Label** (de) | Webseite |
| **Label** (en) | Website |
| **Description** (de) | URL eines Dokuments mit näheren Informationen über die beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.). |
| **Description** (en) | URL of a document about that thing, eg. a Wikipedia page. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:label`](specs-attrs.md#d17e6963)[`@iso-date`](specs-attrs.md#attr.iso-date)   |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date"/>
   <attribute name="gndo:label"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1648"></a>
### Werk
|     |     |
| --- | --- |
| **Name** | `work`  |
| **See also** | [gndo:Work](https://d-nb.info/standards/elementset/gnd#Work) |
| **Label** (de) | Werk |
| **Label** (en) | Work |
| **Description** (de) | Ein Werksdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](specs-attrs.md#attr.record.agency)  [`@enrich`](specs-attrs.md#d17e7628)  [`@gndo:type`](specs-attrs.md#d17e1737)  [`@gndo:uri`](specs-attrs.md#d17e6984)  [`@xml:id`](specs-attrs.md#d17e7648)   |
| **Contained by** | [`list`](specs-elems.md#data-list) |
| **May contain** | [`<anyElement.narrow>`](specs-elems.md#any-restricted)  [`dc:title`](specs-elems.md#d17e3011)  [`dublicateGndIdentifier`](specs-elems.md#dublicates)  [`gndo:abbreviatedName`](specs-elems.md#d17e3272)  [`gndo:accordingWork`](specs-elems.md#d17e3242)  [`gndo:author`](specs-elems.md#d17e3422)  [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)  [`gndo:broaderTerm`](specs-elems.md#d17e3502)  [`gndo:contributor`](specs-elems.md#d17e3635)  [`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)  [`gndo:dateOfPublication`](specs-elems.md#d17e4177)  [`gndo:editor`](specs-elems.md#d17e4281)  [`gndo:firstAuthor`](specs-elems.md#d17e4491)  [`gndo:formOfWorkAndExpression`](specs-elems.md#d17e4421)  [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](specs-elems.md#d17e4693)  [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)  [`gndo:homepage`](specs-elems.md#elem.gndo.homepage)  [`gndo:languageCode`](specs-elems.md#d17e4843)  [`gndo:literarySource`](specs-elems.md#d17e4888)  [`gndo:preferredName`](specs-elems.md#d17e5423)  [`gndo:relatedWork`](specs-elems.md#d17e5806)  [`gndo:relatesTo`](specs-elems.md#d17e5757)  [`gndo:topic`](specs-elems.md#d17e6005)  [`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)  [`img`](specs-elems.md#elem.img)  [`owl:sameAs`](specs-elems.md#elem.owl.sameAs)  [`ref`](specs-elems.md#elem.ref)  [`revision`](specs-elems.md#revision)[`skos:note`](specs-elems.md#elem.skos.note)  [`source`](specs-elems.md#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:accordingWork" repeatable="true"/>
      <element name="gndo:author" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:contributor" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:dateOfPublication"/>
      <element name="gndo:editor" repeatable="true"/>
      <element name="gndo:firstAuthor"/>
      <element name="gndo:formOfWorkAndExpression" repeatable="true"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:literarySource" repeatable="true"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:relatedWork" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:work[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="prop-periodOfActivity"></a>
### Wirkungsdaten
|     |     |
| --- | --- |
| **Name** | `gndo:periodOfActivity`  |
| **Label** (de) | Wirkungsdaten |
| **Label** (en) | Period of activity |
| **Description** (de) | Zeitraum, innerhalb dessen die beschriebene Entität aktiv war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind. Wenn ein exaktes Wirkungsdatum erschlossen werden soll, dann wird das Datum als ISO 8601 YYYY-MM-DD in sowohl `@iso-from` als auch in `@iso-to` angegeben. |
| **Description** (en) | A person’s known period of activity |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@iso-from`](specs-attrs.md#attr.iso-from)  [`@iso-to`](specs-attrs.md#attr.iso-to)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |
| **May contain** | [`label`](specs-elems.md#d17e6206)  [`note`](specs-elems.md#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
   <choice>
      <text/>
      <group>
         <element name="label" required="true"/>
         <element name="note" required="true"/>
      </group>
   </choice>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:periodOfActivity">
      <sch:assert test="@iso-from or @iso-to" role="error">(Point in time missing): It is mandatory to provide either a start- or
                        endpoint in time (or both) to encode a timespan like `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfActivity"></a>
### Wirkungsort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfActivity`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfActivity](https://d-nb.info/standards/elementset/gnd#placeOfActivity) |
| **Label** (de) | Wirkungsort (Person) |
| **Label** (en) | Place of activity (Person) |
| **Description** (de) | Ein Wirkungsort, an dem die Person gewirkt oder gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | A person’s or family’s place of activity |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@iso-date`](specs-attrs.md#attr.iso-date)  [`@iso-from`](specs-attrs.md#attr.iso-from)  [`@iso-to`](specs-attrs.md#attr.iso-to)[`@ref`](specs-attrs.md#d17e7662)   |
| **Contained by** | [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <attribute name="iso-from"/>
      <attribute name="iso-to"/>
   </choice>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfActivity">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via
                        `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5924"></a>
### Zeitweiser Name
|     |     |
| --- | --- |
| **Name** | `gndo:temporaryName`  |
| **See also** | [gndo:temporaryName](https://d-nb.info/standards/elementset/gnd#temporaryName), [gndo:temporaryNameOfTheConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#temporaryNameOfTheConferenceOrEvent), [gndo:temporaryNameOfTheCorporateBody](https://d-nb.info/standards/elementset/gnd#temporaryNameOfTheCorporateBody), [gndo:temporaryNameOfThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#temporaryNameOfThePlaceOrGeographicName) |
| **Label** (de) | Zeitweiser Name |
| **Label** (en) | Temporary name |
| **Description** (de) | Eine Namensform, die zeitweise zur Bezeichnung der dokumentierten Entität benutzt wurde. |
| **Description** (en) | A name used temporarily to denominate the entity encoded. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662)  [`@xml:lang`](specs-attrs.md#d17e7157) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`event`](specs-elems.md#d17e320)  [`place`](specs-elems.md#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3242"></a>
### Zugehöriges Werk
|     |     |
| --- | --- |
| **Name** | `gndo:accordingWork`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#accordingWork](https://d-nb.info/standards/elementset/gnd#accordingWork) |
| **Label** (de) | Zugehöriges Werk |
| **Label** (en) | According work |
| **Description** (de) | ... |
| **Description** (en) | ... |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`work`](specs-elems.md#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3398"></a>
### Zugehörigkeit
|     |     |
| --- | --- |
| **Name** | `gndo:affiliation`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#affiliation](https://d-nb.info/standards/elementset/gnd#affiliation) |
| **Label** (de) | Zugehörigkeit |
| **Description** (de) | Die Person oder Körperschaft ist zugehörig zu einer Körperschaft, oder ist mit einem Ort oder einem Event verbunden. |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`corporateBody`](specs-elems.md#corporate-body-record)  [`person`](specs-elems.md#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3608"></a>
### Zählung
|     |     |
| --- | --- |
| **Name** | `gndo:counting`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#counting](https://d-nb.info/standards/elementset/gnd#counting) |
| **Label** (de) | Zählung |
| **Label** (en) | Counting |
| **Description** (de) | Eine Zählung als Namensbestandteil (z.B. in Ramses **II** oder Sethos **I**). |
| **Description** (en) | A Counting as Part of a proper name (eg. Ramesses **II** or Seti **II**). |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538) |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)  [`gndo:familialRelationship`](specs-elems.md#d17e4367)  [`gndo:preferredName`](specs-elems.md#person-record-preferredName)  [`gndo:variantName`](specs-elems.md#person-record-variantName) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1553"></a>
### embodimentOf
|     |     |
| --- | --- |
| **Name** | `embodimentOf`  |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`manifestation`](specs-elems.md#d17e1518) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:embodimentOf/@ref">
      <sch:let name="id" value="data(.)"/>
      <sch:let name="target" value="root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::entity:expression]" role="error"> Referenced Entity "<sch:value-of select="$id"/>"
                                    must be an expression but is "<sch:value-of select="$target/name()"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6745"></a>
### entityXML Store Metadaten
|     |     |
| --- | --- |
| **Name** | `store:store`  |
| **Label** (de) | entityXML Store Metadaten |
| **Label** (en) | entityXML Store metadata |
| **Description** (de) | Metadaten, die in einem entityXML Store automatisch erzeugt und zur Verwaltung des Datensatzes verwendet werden. |
| **Description** (en) | Metadata automatically generated by an entityXML Store to manage the dataset. |
| **Attributes** | [`@id`](specs-attrs.md#d17e6761)  [`@project`](specs-attrs.md#d17e6773)   |
| **Contained by** | [`entityXML`](specs-elems.md#entity-xml) |
| **May contain** | [`store:workflow`](specs-elems.md#d17e6785) |

***Content Model***  
```xml
<content>
   <attribute name="id"/>
   <attribute name="project"/>
   <element name="store:workflow"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6785"></a>
### entityXML Store Workflow
|     |     |
| --- | --- |
| **Name** | `store:workflow`  |
| **Label** (de) | entityXML Store Workflow |
| **Label** (en) | entityXML Store workflow |
| **Description** (de) | Informationen zum entityXML Store Workflow des jeweiligen Informationsobjekts (Datensatz oder Record). |
| **Description** (en) | Information with regard to the entityXML Store Workflow of the Informationobject (dataset or record). |
| **Contained by** | [`store:store`](specs-elems.md#d17e6745) |
| **May contain** | [`store:step`](specs-elems.md#d17e6801) |

***Content Model***  
```xml
<content>
   <element name="store:step"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6801"></a>
### entityXML Store Workflow Schritt
|     |     |
| --- | --- |
| **Name** | `store:step`  |
| **Label** (de) | entityXML Store Workflow Schritt |
| **Label** (en) | entityXML Store workflow Step |
| **Description** (de) | Ein Schritt im Managementworkflow. |
| **Description** (en) | A step of the management workflow. |
| **Attributes** | [`@name`](specs-attrs.md#d17e6815)  [`@timestamp`](specs-attrs.md#d17e6817) |
| **Contained by** | [`store:workflow`](specs-elems.md#d17e6785) |

***Content Model***  
```xml
<content>
   <attribute name="name" required="true"/>
   <attribute name="timestamp" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1479"></a>
### realizationOf
|     |     |
| --- | --- |
| **Name** | `realizationOf`  |
| **Attributes** | [`@agency`](specs-attrs.md#d17e7177)  [`@enriched`](specs-attrs.md#d17e7538)  [`@gndo:ref`](specs-attrs.md#d17e7005)  [`@ref`](specs-attrs.md#d17e7662) |
| **Contained by** | [`expression`](specs-elems.md#d17e1449) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:realizationOf/@ref">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:let name="target" value="./root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::*:work]" role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work
                                    but is "<sch:value-of select="$target/name()"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

