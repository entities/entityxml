
<a name="docs_d14e681"></a>
# Arbeiten in oXygen XML

Unabhängig davon, ob entityXML nun als Framework in oXygen installiert ist, oder nicht: Die einzelnen Ressourcen, die entityXML als *Werkzeugkasten* ausmachen, stehen auch unabhängig von einer festen Integration als oXygen Framework zur [Verfügung](resources.md). 

Die feste Integration von entityXML als Framework ist, wie bereits schon erwähnt, vorallem für Nutzer*innen interessant, die häufig mit dem Format arbeiten, denn es erleichtert die Anwendung von Konversionen oder die Benutzung des Author Modes wesentlich.

<a name="oxygen-author-mode"></a>
## oXygen Author Mode

Um die Arbeit mit entityXML in oXygen intuitiver zu gestalten, werden mehrere <span class="emph">CSS Dateien</span> bereitgestellt, um die Autoren Ansicht zu strukturieren und mit einem benutzerfreundlicheren Design als der XML Ansicht auszustatten.

<a name="docs_d14e705"></a>
### Views

Views definieren die generelle Struktur des entityXML Author Modes.


- **Basis View** (`/assets/css/author/entities.author.css`): Die Haupt-CSS des entityXML Frameworks, obligatorisch für den oXygen Author Mode. Dieser View rendert Einträge in prosaischer Form.
- **Strukturierte Ansicht** (`/assets/css/author/entities.author.structured.css`): Eine zusätzliche CSS zum Basis View, um Einträge in *halbstrukturierter Form* darzustellen. Eine bessere Übersichtlichkeit der Daten ist m.E. garantiert! 
	- **Vollstrukturierte Ansicht** (`/assets/css/author/entities.author.structured.full.css`): Erweiterung des Strukturierten Views, der alle Elemente eines Eintrags hierachisch abbildet.
- **Interaktive Ansicht** (`/assets/css/author/entities.author.interactive.css`): Zusätzliche CSS, die die Eingabe im Author Mode interaktiver durch Schaltflächen unterstützt. So können z.B. Revisionsbeschreibungen intuitiver angelegt werden oder IDs für Einträge automatisch erzeugt werden.

<a name="docs_d14e741"></a>
### Filter

Filter steuern die Anzeige von Einträgen nach gewissen Kriterien.


- **Filter "Nur offene Einträge"** (`/assets/css/author/agency.open-records.css`): Filter Modus, der nur alle Einträge anzeigt, die noch nicht als abgeschlossen markiert sind.
- **Filter "Nur Einträge ohne GND-URI"** (`/assets/css/author/agency.no-gnd-uri.css`): Filter Modus, der nur alle Einträge anzeigt, die keinen GND-URI verzeichnen.

<a name="author-mode-framework"></a>
### Verwendung des Author Mode bei installiertem Framework

Wenn entityXML bereits als Framework installiert ist, müssen keine weiteren Schritte unternommen werden. Wechselt man nun im oXygen XML Editor in den <span class="emph">Author Mode</span> (Unten links am Bildschirmrand unter **`Autor`**, siehe auch die offizielle [oXygen Dokomentation](https://www.oxygenxml.com/doc/versions/25.0/ug-editor/topics/editing-xml-documents-author.html)) können unter dem **`Stile`** Dropdownreiter (Oberer Menüleiste) die verschiedenen, im Framework vorkonfigurierten Stile bzw. Views zu- oder abgeschaltet werden, je nach persönlicher Vorliebe.

<a name="docs_d14e781"></a>
### Verwendung des Author Mode ohne Framework Installation

Falls entityXML nicht als Framework installiert ist, lassen sich die CSS Stylesheets für den <span class="emph">Author Mode</span>, wie im [Setup Kapitel](setup.md) einleitend beschrieben, auch manuell ohne Aufwand in die jeweilige entityXML Ressource einbinden und entsprechend verwenden. Dazu muss folgende *Processing Instruction* hinter die XML-Deklaration der entityXML Datei, die bearbeitet werden soll, gesetzt werden, um den **Basis View** des entityXML Author Modes zu verwenden:

```xml
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
```

Um zusätzlich die **Strukturierte Ansicht** nutzen zu können, muss eine weitere *Processing Instruction* hinter die Erste gesetzt werden:

```xml
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

**Wichtig (!)** ist hierbei, dass die **Titel** (der `title` Parameter in der *Processing Instruction*) der beiden verknüpften CSS Ressourcen identisch sind, damit oXygen beide Dateien zusammenführt und daraus einen View rendert. Als Konvention verwenden wir hierfür **`entityxml`**. Das Ergebnis in der XML Datei sollte nun etwa so aussehen:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- Hier die Autor Mode CSS -->
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
<!-- Weitere Processing Instructions, z.B. zur Schemaanbindung -->
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#">
    <!-- ... -->
</entityXML>
```

Nun kann der Author Mode, wie bei einer [festen Installation des entityXML Frameworks](#author-mode-framework) (siehe oben) genutzt werden.

Diese Form der Author Mode CSS Integration bringt den (verschmerzbaren) Nachteil mit sich, dass zwischen einzelnen Views nicht kompfortabel über den **`Stile`** Reiter hin und her gewechselt werden kann. Um die optionale **Strukturierte Ansicht** zu deaktivieren oder zu aktivieren, muss die entsprechende *Processing Instruction* entweder auskommentiert bzw. gelöscht werden (Strukturierte Ansicht ist aus) oder gesetzt sein (Strukturierte Ansicht ist an). 

Dieser Ansatz setzt eine bestehende Internetverbindung voraus. Wenn man sicher gehen will, dass das ganze Setup auch <span class="emph">offline</span> funktioniert, [kann man sich die Dateien lokal speichern und die Pfade in
                           den Processing Instructions entprechend anpassen](setup.md#entityxml-offline).
