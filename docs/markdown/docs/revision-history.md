
<a name="revision-history"></a>
# Revisionen



##0.6.5+ BETA (Nightly) (2024-10-22)  

- Added Validation on `gndo:geographicAreaCode` and updated Documentation.
- Added `@cert` to `gndo:forename` and `gndo:surname`.
- Added `gndo:topic`, `gndo:dateOfConferenceOrEvent`, `gndo:organizerOrHost` and `gndo:place` on `event` entity.
- Added `gndo:dateOfConferenceOrEvent` on `event` entity.


##0.6.5 BETA (Nightly) (2024-08-06)  

- Added `gndo:playedInstrument` on `person` entity.
- Added Languagecode vocabulary for `@xml:lang`.
- Added `@script` to `gndo:variantName`.
- Added `@script` and `@type='original'` to Persons `gndo:preferredName` which makes it possible to encode an additional preferred Name in original script.
- Added `gndo:familialRelationship` and `gndo:acquaintanceshipOrFriendship` to Person.
- Removed `gndo:relatesTo/gndo:type` and added `gndo:relatesTo/gndo:code` incl. list of GND relation-type-codes.
- Made `gndo:professionOrOccupation` capable of documenting a simple Literal without GND-URI reference. 
- Changed Contentmodell for `source`: (1) removed `ref` since there is already `@url`; (2) added `title` and `note` to Contentmodel.


##0.6.0 BETA (2024-07-24)  

- Added `@isil` to `provider`.
- Added first elements to markup "Provenienzmerkmale" using `work`.


##0.5.3 ALPHA (2024-03-01)  

- Added `mapping` structure.


##0.5.2 ALPHA (2024-02-26)  

- Added `gndo:dateOfEstablishmentAndTermination` to events, places and works. The property now may contain text.
- Added `ref` to `source` content model.
- Added `img` property. Now it is possible to document images for every entity record.


##0.5.1 ALPHA (2023-02-08)  

- Preparations for the BETA Version. New Attributes (`@iso-notBefore`, `@iso-notAfter`) added to Date Elements.
- GND-Toolbox Konversion added to serialize entityXML Data to optimised Toolbox JSON.
- separated record types.
- revised `revision` description model.
- cleaned `gndo:publication` and added simple bibliographic model.
- changed record-id to `@xml:id`
- added `bf:instanceOf` to `manifestation` entity.
- changed `gndo:page` to `gndo:homepage` (cf. v0.5.0 > v0.5.1 update script `/scripts/xslt/update/050-051.xsl`)
- added `gndo:succeedingPlaceOrGeographicName`, `gndo:temporaryName`, `gndo:titleOfNobility`, `gndo:abbreviatedName`, `gndo:fieldOfStudy` and `gndo:abbreviatedName`.
- added `gndo:relatesTo` property to all records (which has no direct equivalent in the GNDO but may be helpfull to map out relations between entities anyway).
- added `event` record which more or less translates to `gndo:ConferenceOrEvent` but is not fully moddled yet.
- changed `gndo:dateOfActivity` to `gndo:periodOfActivity` (cf. v0.5.0 > v0.5.1 update script `/scripts/xslt/update/050-051.xsl`)
- refined Validation on `gndo:publication` concerning `@ref`, `@gndo:ref`, `@dnb:catalogue`


##0.5.0 ALPHA (2023-01-25)  

- **First published Version 0.5.0** (ALPHA) is now open for testing.
- Renamed former `project` element to `provider` and placed in `entityXML/collection`
- Changes on the Content Model of `entity`: ANY ELEMENT concept finetuned.
- Added `@url` to `source`
- Added `gndo:affiliation` to `person` and `corporateBody`
- Added `@dnb:catalogue` to `gndo:publication`
- Added `gndo:pseudonym` to `person`.
- Added `@gndo:type` to `person` to diferentiate subclasses like gods, fictional charcters etc.
- Modified `@ref` to either point to an internal ID or to an external URI.
- Added `@gndo:ref` to `gndo:professionOrOccupation` to make `@gndo:term` more clearer.


##0.3.2 ALPHA (Unpublished) (2022-11-18)  

- Cleaned Structure and introduced models
- Added Support for other schemes (like LIDO) using `rng:element/rng:anyName` concept.


##0.3.1 ALPHA (Unpublished) (2022-11-11)  

- Added manifestation and expression record.
- Added Property Class with agency features.


##0.3.0 ALPHA (Unpublished) (2022-10-05)  
Added store-namespace and schema.


##0.2.1 (Unpublished) (2022-07-06)  

- Added `work` Entity and provided MARC mapping.
- Added `@id` to `list` element.


##0.2 (Unpublished) (2022-05-10)  
Added GND-Reference abstract class.


##0.1.4 (Unpublished) (2022-04-21)  
Simplified Entity lists. Now every Record needs an ID (either `@id` or `@gndo:uri`).


##0.1.3 (Unpublished) (2022-04-20)  
Better revision description; entityXML now supports arbitrary elements within entity markup.


##0.1.2 (Unpublished) (2022-04-19)  
Structural Changes concerning project and collection container.


##0.1.1 (Unpublished) (2022-03-31)  
Reduced Content: Integrating just persons and places for now!


##0.1 (Unpublished) (2022-03-29)  
Created RELAX-NG Schema.

