
<a name="person-record"></a>
# Personen

Ein **Personeneintrag** (Personrecord) enthält Informationen zu *realen* oder *fiktiven Personen*. entityXML definiert ein festes Set an Eigenschaften aus dem GNDO-Namensraum, um Informationen zu dokumentieren, die von der GND unterstützt werden. Zusätzlich ist es möglich weitere Eigenschaften in einem <span class="emph">eigenen Namensraum</span> (Elemente und Attribute in sog. [custom namespaces](custom-namespaces.md)) in einem Personeneintrag zu dokumentieren.

Ein Personeneintrag wird durch das Element [`person`](specs-elems.md#person-record) repräsentiert und benötigt eine **`@xml:id`**.

Sofern für die Person bereits ein Normdatensatz in der GND vorhanden ist, wird der entsprechende GND HTTP-URI in `@gndo:uri` dokumentiert. Falls nicht, ist die Angabe einer <span class="emph">Vorzugsbenennung</span> via `gndo:preferredName` oder einer <span class="emph">Namensvariante</span> via `gndo:variantName` **verpflichtend**.

```xml
<person xml:id="mmuster">
    <gndo:preferredName><!-- ... --></gndo:preferredName>
</person>
```

<a name="docs_d14e2094"></a>
## Unterscheidung von Personentypen

Entitäten, die in `person` erschlossen werden, gelten per default als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc), und können via [``@gndo:type``](specs-attrs.md#person-types) als Unterklasse einer gndo:DifferentiatedPerson weiter spezifiziert werden. Sofern es sich z.B. nicht um eine reale Person, sondern um eine fingierte Person bzw. eine Figur aus einem fiktionalen Werk handelt, dann würde der Eintrag entsprechend über `@gndo:type` mit '[https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)' spezifiziert werden:

```xml
<person xml:id="malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName><gndo:personalName>Malachlabra</gndo:personalName></gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation xml:lang="en">Malachlabra was an archfiend and the daughter of Dispater in the late 14th century DR. She was ambushed by the Simbul in 1372 DR.</gndo:biographicalOrHistoricalInformation>
   <skos:note>Malachlabra ist ein Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
</person>
```

<a name="person-name"></a>
## Erschließung von Vorzugsbenennung und Namensvarianten

Zur Erschließung von Namensformen für Personen dienen folgende Elemente:

- [`gndo:preferredName`](specs-elems.md#person-record-preferredName), **Vorzugsbenennung (Person)**: Die bevorzugte Namensform zur Denomination
                    einer Person.
- [`gndo:variantName`](specs-elems.md#person-record-variantName), **Variante Namensform (Person)**: Eine alternative Namensform, unter der eine
                    Person ebenfalls bekannt ist.


Für Personeinträge ist die Angabe einer <span class="emph">Vorzugsbenennung</span>**obligatorisch**, sofern die Person nicht bereits über einen GND-URI identifiziert wurde. Die Angabe von (beliebig vielen) <span class="emph">Namensvarianten</span> ist optional.

Eine Vorzugs- bzw. Alternativbenennung kann auf *zwei alternative Arten* angegeben werden:

(1) unter Angabe **verschiedener Namensbestandteile**

- **Vorname** [`gndo:forename`](specs-elems.md#d17e4522) (*verpflichtend*)  
  "Ein oder mehrere Vornamen einer
                Person."
- **Namenspräfix** [`gndo:prefix`](specs-elems.md#d17e5463) (*optional*)  
  "Namensteil, der dem Namen vorangestellt sind (z.B.
                    Adelskennzeichnungen, "von", "zu" etc.)."
- **Nachname** [`gndo:surname`](specs-elems.md#d17e5896) (*verpflichtend*)  
  "Der Nach- oder Familienname, unter dem eine Person bekannt
                    ist."
- **Zählung** [`gndo:counting`](specs-elems.md#d17e3608) (*optional*)  
  "Eine Zählung als Namensbestandteil (z.B. in Ramses **II** oder
                    Sethos **I**)."
- **Namenszusatz** [`gndo:nameAddition`](specs-elems.md#d17e4918) (*optional und wiederholbar*)  
  "Zusätzliches Element des Namens, unter dem eine Person
                    bekannt ist, z.B. "Graf von Wallmoden"."
- **Beiname, Gattungsname, Titulatur, Territorium** [`gndo:epithetGenericNameTitleOrTerritory`](specs-elems.md#d17e4311) (*optional und wiederholbar*)  
  "Beiname einer Person, bei
                    dem es sich um ein Epitheton, Titel oder eine Ortsassoziation handelt."



(2) unter Angabe eines **persönlichen Namens**

- **Persönlicher Name** [`gndo:personalName`](specs-elems.md#d17e5276) (*verpflichtend*)  
  "Ein Name, unter dem die Person gemeinhin bekannt ist.
                    Ein persönlicher Name wird dann angegeben, wenn die Person nicht durch einen Vor- oder Nachname benannt wird, z.B.
                    "Aristoteles""



```xml
<!-- Beispiel Vorzugsbenennung mit Vor- und Nachnamen-->
<gndo:preferredName>
    <gndo:forename>Ludolph Christian</gndo:forename>
    <gndo:surname> Meier</gndo:surname>
</gndo:preferredName>

<!-- Beispiel Vorzugsbenennung mit persönlichem Namen-->
<gndo:variantName>
    <gndo:personalName>Aristoteles</gndo:personalName>
</gndo:variantName>

<!-- Beispiel Namensvariante mit präfigiertem "von"-->
<gndo:variantName>
    <gndo:forename>Georg</gndo:forename>
    <gndo:prefix>von</gndo:prefix>
    <gndo:surname>Mejern</gndo:surname>
</gndo:variantName>
```

Sowohl Vorzugsbenennungen als auch Namensvarianten können zusätzlich zur westlich normalisierten Form auch in **originalschriftlicher Form** angegeben werden.

Im Falle der Auszeichnung einer originalschriftlichen Vorzugsbennenung müssen drei Kriterien erfüllt sein: (1) Der Eintrag enthält eine Vorzugsbenennung laut Standardansetzung ,(2) die originalschriftliche Vorzugsbenennung ist via `@type` als `original` gekennzeichnet und (3) Sprach- als auch Schriftinformationen (`@xml:lang` und `@script`) sind explizit angegeben:

```xml
<gndo:preferredName type="original" xml:lang="ara" script="Arab">
   <gndo:forename>احمد بن محمد</gndo:forename>
   <gndo:prefix>ال</gndo:prefix>
   <gndo:surname>مرزوقي</gndo:surname>
</gndo:preferredName>
```

Namensvarianten werden ebenfalls mit expliziten Sprach- und Schriftinformationen (`@xml:lang` und `@script`) angelegt:

```xml
<gndo:variantName xml:lang="ara" script="Arab">
   <gndo:personalName>رضا كحالة, عمر</gndo:personalName>
</gndo:variantName>
```

<a name="person-geographical-area-code"></a>
## Geographische Zuordnung

Die geographische Zuordnung von Personen zu einem Ort ist in der GND verpflichtend, so auch in entityXML! Wie [weiter oben](entity-geographic-area.md) beschrieben wird hierfür [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode) verwendet, inkl. der Angabe eines Terms via `@gndo:term` aus dem Vokabular "[Geographic Area Codes](https://d-nb.info/standards/vocab/gnd/geographic-area-code)": 

<a name="person-dates"></a>
## Lebens- und Wirkungsdaten

Lebens- und Wirkungsdaten sind wichtige Identifikationsmerkmale einer Person. Sie lassen sich mittels folgender Elemente dokumentieren: 

- [`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth), **Geburtsdatum (Person)**: Das Geburtsdatum der
                Person.
- [`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath), **Sterbedatum (Person)**: Das Sterbedatum der
                Person.
- [`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity), **Wirkungsdaten**: Zeitraum, innerhalb dessen die beschriebene Entität aktiv
                    war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind. Wenn ein exaktes Wirkungsdatum erschlossen werden soll, dann wird das Datum als ISO 8601 YYYY-MM-DD in sowohl `@iso-from` als auch in `@iso-to` angegeben.


Die Elemente `gndo:dateOfBirth` und `gndo:dateOfDeath` markieren einen <span class="emph">Zeitpunkt</span>, dessen Datum als [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html), also in der Form **YYYY-MM-DD** via [``@iso-date``](specs-attrs.md#attr.iso-date) dokumentiert wird.

```xml
<gndo:dateOfBirth iso-date="1980-01-11"></gndo:dateOfBirth>
<gndo:dateOfDeath iso-date="2020-01-11"></gndo:dateOfDeath>
```

Sofern sich Datumsangeben nicht genau bestimmen lassen, kann der wahrscheinliche Zeitpunkt über die Attribute [``@is-notBefore``](specs-attrs.md#attr.iso-notBefore) (Angabe des frühest möglichen Datums) und [``@iso-notAfter``](specs-attrs.md#attr.iso-notAfter) (Angabe des spätest möglichen Datums) näher eingegränzt werden.

```xml
<gndo:dateOfBirth iso-notBefore="1980">nach 1980</gndo:dateOfBirth>
```

`gndo:periodeOfActivity` beschreibt einen <span class="emph">Zeitraum</span> und verwendet dementsprechend je ein Attribute zur Beschreibung eines Anfangs- ([``@iso-from``](specs-attrs.md#attr.iso-from)) und eines Enddatums ([``@iso-to``](specs-attrs.md#attr.iso-to)), die ebenfalls dem Format ISO 8601 entsprechen müssen. Die Angabe von midestens einem Start- oder einem Enddatum ist hier verpflichtend. Ein Wirkungszeitraum wird nur dann erschlossen, wenn kein Geburts- und Sterbedatum für die beschriebene Person bekannt ist.

```xml
<gndo:periodOfActivity iso-from="1940" iso-to="2000"></gndo:periodOfActivity>
```

Sofern der Wirkungszeitraum als *exaktes Wirkungsdatum* erhoben werden soll, wird das Datum als ISO 8601 YYYY-MM-DD sowohl in `@iso-from` also auch `@iso-to` angegeben.

Zusätzlich zu den eher maschienenlesbaren Datumsangaben in den Attributen, kann eine menschenlesbare Repräsentation des Datums als Text innerhalb des jeweiligen Datums- oder Zeitraumelements angegeben werden.

```xml
<gndo:dateOfBirth iso-date="1980-01-11">11 Januar 1985</gndo:dateOfBirth>
<gndo:dateOfDeath iso-notAfter="1950">vor 1950</gndo:dateOfDeath>
<gndo:periodOfActivity iso-from="1940" iso-to="2000">1940-2000</gndo:periodOfActivity>
```

Wenn zu einem Wirkungsdatum noch eine Erläuterung angegeben werden soll, dann kann dies vie [`note`](specs-elems.md#add-note) erfolgen. Dazu wird innerhalb von `gndo:periodOfActivity` der menschenlesbare Text via [`label`](specs-elems.md#d17e6206) und die zusätzliche Erläuterung in [`note`](specs-elems.md#add-note) gesetzt:

```xml
<gndo:periodOfActivity iso-from="1940" iso-to="2000">
  <label>1980-2000</label>
  <note>Mitglied der entityXML-Gilde</note> 
</gndo:periodOfActivity>
```

<a name="person-places"></a>
## Lebens- und Wirkungsorte

Lebens- und Wirkungsorte einer Person werden durch folgende Elemente erschlossen: 

- [`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth), **Geburtsort (Person)**: Der Geburtsort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.
- [`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath), **Sterbeort (Person)**: Der Sterbeort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.
- [`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity), **Wirkungsort (Person)**: Ein Wirkungsort, an dem die Person gewirkt oder
                    gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung
                    erfolgt die Erfassung des Ortes als Freitext im Element.


Der Ort wird als Text innerhalb des jeweiligen Ortelements dokumentiert. Es wird empfohlen zusätzlich den entsprechenden GND-URI des Orts via `@gndo:ref` anzugeben.

```xml
<gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4007666-0">Bonn</gndo:placeOfBirth>
<gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4023118-5">Hamburg</gndo:placeOfDeath>
<gndo:placeOfActivity gndo:ref="https://d-nb.info/gnd/4021477-1">Göttingen</gndo:placeOfDeath>
```

Für Wirkungsorte können zusätzlichen zeitliche Angaben via `@iso-date` oder `@iso-from` und `@iso-to` gemacht werden:

```xml
<gndo:placeOfActivity gndo:ref="https://d-nb.info/gnd/4021477-1" iso-from="1950" iso-to="1955">Göttingen</gndo:placeOfDeath>
```

<a name="person-gender"></a>
## Angaben zum Geschlecht

Angaben zum Geschlecht einer Person können via [`gndo:gender`](specs-elems.md#d17e4581) dokumentiert werden. Mittels `@gndo:type` wird ein Term oder mehrere Terme aus dem GND Vokabular "Gender" [https://d-nb.info/standards/vocab/gnd/gender#](https://d-nb.info/standards/vocab/gnd/gender#) dokumentiert. Eine zusätzliche Beschreibung kann zusätzlich als Text innerhalb des Elements verzeichnet werden. 

```xml
<gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#female">weiblich</gndo:gender>
```

```xml
<gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#female https://d-nb.info/standards/vocab/gnd/gender#male">nichtbinär</gndo:gender>
```

<a name="person-profession"></a>
## Berufe und Tätigkeiten

Berufe und/oder Tätigkeitsfelder einer Person lassen sich mittels [`gndo:professionOrOccupation`](specs-elems.md#d17e5488) dokumentieren.

```xml
<gndo:professionOrOccupation">Jongleuse</gndo:professionOrOccupation>
```

Optional aber empfohlen ist die zusätzliche Angabe eines Sachbegriffs aus der GND über `@gndo:ref`, und zwar als URI:

```xml
<gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4025098-2">Historiker</gndo:professionOrOccupation>
```

Berufe und Tätigkeiten lassen sich optional via `@type` als Hauptbeschäftigung, sprich als 'significant' spezifizieren:

```xml
<gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4025098-2">
   Historiker
</gndo:professionOrOccupation>
<gndo:professionOrOccupation gndo:type="significant" gndo:ref="https://d-nb.info/gnd/4161681-9">
   Informationswissenschaftler
</gndo:professionOrOccupation>
```

<a name="person-publications"></a>
## Titelangaben/Veröffentlichungen

Über [`gndo:publication`](specs-elems.md#d17e5604) lassen sich Veröffentlichungen dokumentieren, die mit der beschriebenen Person in Zusammenhang stehen. Das kingt unspezifisch und genau so ist es auch gedacht: Veröffentlichungen bzw. "Titelangaben" gelten in der GND vorallem als weiteres <span class="emph">Distinguierungssmerkmal</span>, um die Person von Anderen zu unterscheiden, sprich: Die auf diese Weise verzeichneten Titel belegen einerseits die Existenz der Person als Entität und enthalten andererseits weitere Informationen (z.B. Vorzugsbenennungen, Kontextinformationen etc.), um im Zweifelsfall homonyme Personen von einander unterscheiden zu können.

In `gndo:publication` können dementsprechend Veröffentlichungen dokumentiert werden, die …


- … von der Person selber stammen (<span class="emph">autorschaftlicher Berzug</span>)
- … von der Person herausgegeben wurden (<span class="emph">herausgeberschaftlicher Bezug</span>)
- … von der Person handeln (<span class="emph">thematischer Bezug</span>)

Diese drei unterschiedlichen Bezüge werden im optionalen `@role` Attribut in `gndo:publication` erfasst, das folgenden Wertebereich vordefiniert: "**auhor**" (autorschaftlicher Bezug), "**editor**" (herausgeberschaftlicher Bezug), "**about**" (thematischer Bezug). Sofern `@role` nicht explizit ausgewiesen wird, gilt der Bezug als autorschaftlich, sprich: "author" gilt als *default*.

Innerhalb von `gndo:publication` gibt es ein Basisset von Elementen, die verwendet werden können, um ein grundständiges bibliographisches Zitat zu strukturieren:


- **Autor, erste Verfasserschaft** [`gndo:firstAuthor`](specs-elems.md#d17e4491) (*optional und wiederholbar*)  
  "Die Person oder Körperschaft, die
                    verantwortlich für die primäre Verfasserschaft an einem Werk oder einer Veröffentlichung zeichnet."
- **Titel** [`title`](specs-elems.md#d17e6862) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext
                    beschrieben wird."
- **Titelzusatz** [`add`](specs-elems.md#d17e2597) (*optional*)  
  "Zusätze zum Titel im Rahmen der bibliographischen
                    Angabe."
- **Datum** [`date`](specs-elems.md#d17e2985) (*verpflichtend*)  
  "Ein Zeitpunkt oder Zeitraum, der mit einem Ereignis im Lebenszyklus
                    der beschriebenen Ressource in Zusammenhang steht."


> **Warum nur ein 'Basisset'?**  
> entityXML implementiert (bisher) nur das Nötigste an Elementen, um eine bibliographsche Angabe zu beschreiben, die sich später in einen MARC-Datensatz konvertieren lässt. An dieser Stelle soll nicht die Ausführlichkeit eines Bibliothekskatalogs abgebildet werden, sondern lediglich die Informationseinheiten, die als eigenständige Felder in den MARC Feldern 670 und 672 benötigt werden. 


```xml
<person xml:id="uwe_sikora" gndo:uri="https://d-nb.info/gnd/1155094360">
   <gndo:preferredName>
      <gndo:forename>Uwe</gndo:forename>
      <gndo:surname>Sikora</gndo:surname>
   </gndo:preferredName>
   <gndo:publication>
      <title>entityXML Handbuch</title>, 
      <date>2023</date>
   </gndo:publication>
</person>
```

Sofern Titelangaben verzeichnet werden, die von der beschreibenenen Person handeln, wird die Angabe eines <span class="emph">Autor\*innennamens</span> zur Pflicht:

```xml
<person xml:id="wilhelm_meister" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName>
      <gndo:surname>Meister</gndo:surname>
      <gndo:forename>Wilhelm</gndo:forename>
   </gndo:preferredName>
   <gndo:publication role="about" gndo:ref="https://d-nb.info/gnd/4123299-9">
      <gndo:firstAuthor>Goethe, Johann Wolfgang von</gndo:firstAuthor>
      <title>Wilhelm-Meister-Romane</title>
      <date>1776</date>
   </gndo:publication>
</person>
```

<a name="docs_d14e2441"></a>
## Beispiel 1: Maxima Musterfrau

Wie alle Einträge kann ein Personeneintrag in eher prosaischer Form verfasst werden:

```xml
<person xml:id="maxima_musterfrau" agency="create">
    <gndo:preferredName><gndo:surname>Musterfrau</gndo:surname>,<gndo:forename>Maxima</gndo:forename>  
    </gndo:preferredName>, auch bekannt als <gndo:variantName><gndo:personalName>Maxima</gndo:personalName>
    </gndo:variantName>, geboren <gndo:dateOfBirth iso-date="1990-05-25">25.05.1990</gndo:dateOfBirth> in 
    <gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4592817-4">Wuhlheide (Berlin)</gndo:placeOfBirth> ist 
    die Tochter von Max Mustermann. Ein Eintrag in der GND könnte <gndo:gndIdentifier cert="low">118500775</gndo:gndIdentifier> 
    sein. Man kann aufjedenfall sagen, dass sie in
    <gndo:placeOfActivity>Berlin</gndo:placeOfActivity> aktiv ist und zwar als bekannte
    <gndo:professionOrOccupation>Aktivistin</gndo:professionOrOccupation> der Initiative "Normdaten für alle aber 
    ordentlich!". Sie arbeitet als freiberufliche 
    <gndo:professionOrOccupation>Bibliothekarin</gndo:professionOrOccupation> in ihrer frei
    zugänglichen Privatbibliothek. Ihre Homepage <foaf:page>http://rettet-normdaten-ev.org/</foaf:page> bietet 
    zahlreiche Informationen zu Normdaten. <skos:note>Dieser Eintrag ist ein Test, unterschiedliche Aspekte der 
        Validierung und der Inhaltsmodelle abzuprüfen.</skos:note>
    <revision status="opened">
        <change when="2022-05-10" who="US">Berlin URI bei placeOfActivity raussuchen und nachtragen.</change>
        <change when="2022-04-06" who="US">Die GND-ID 118500775 identifiziert nicht Maxima Musterfrau sondern Theodor W. Adorno.</change>
        <change when="2022-03-31" who="MM">Eintrag angelegt. Bitte in die GND überführen.</change>
    </revision>
</person>
```

<a name="docs_d14e2451"></a>
## Beispiel 2: Ernest Chevalier

Hier ein Beispiel, dass im Gegensatz zur prosaischen Form eine eher strukturierte Form eines Eintrages verdeutlicht:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName>
   <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>
   <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>
   <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR">Frankreich</gndo:geographicAreaCode>
   <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" gndo:type="significant">Politiker</gndo:professionOrOccupation>
   <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   <revision status="opened">
      <change when="2023-01-21" who="MM">Bitte in die GND aufnehmen</change>
   </revision>
</person>
```

<a name="docs_d14e2461"></a>
## Modell: Personeneintrag

Das **Modell** für einen Personeneintrag sieht wie folgt aus:

```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:acquaintanceshipOrFriendship" repeatable="true"/>
      <element name="gndo:academicDegree"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfBirth"/>
      <element name="gndo:dateOfDeath"/>
      <element name="gndo:periodOfActivity" repeatable="true"/>
      <element name="gndo:familialRelationship" repeatable="true"/>
      <element name="gndo:fieldOfStudy" repeatable="true"/>
      <element name="gndo:functionOrRole" repeatable="true"/>
      <element name="gndo:gender"/>
      <element name="gndo:placeOfActivity" repeatable="true"/>
      <element name="gndo:placeOfBirth"/>
      <element name="gndo:placeOfDeath"/>
      <element name="gndo:playedInstrument" repeatable="true"/>
      <choice>
         <element name="gndo:preferredName"/>
         <group>
            <element name="gndo:preferredName" required="true"/>
            <element name="gndo:preferredName" required="true"/>
         </group>
      </choice>
      <element name="gndo:professionOrOccupation" repeatable="true"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:pseudonym" repeatable="true"/>
      <element name="gndo:titleOfNobility" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


**[`@gndo:type`](specs-attrs.md#person-types)**  
(*Personentyp*): Typisierung der dokumentierten Person (z.B. als
                            fiktive Person, Gott oder royale Person). Wenn kein `@gndo:type` vergeben wird, gilt die Person als
                            [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson).
  **Möglicher Wertebereich**:  


 - '*https://d-nb.info/standards/elementset/gnd#CollectivePseudonym*' : (Sammelpseudonym)
                [GNDO](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - '*https://d-nb.info/standards/elementset/gnd#Gods*' : (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#Gods)
 - '*https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter*' : (Literarische oder Sagengestalt)
                [GNDO](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - '*https://d-nb.info/standards/elementset/gnd#Pseudonym*' : (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - '*https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse*' : (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses)
                [GNDO](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - '*https://d-nb.info/standards/elementset/gnd#Spirits*' : (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#Spirits)



**[`@xml:id`](specs-attrs.md#d17e7648)**  ***ID***  
(*Record ID*): Angabe eines Identifiers zur Identifikation eines
                    Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!

**[`@gndo:uri`](specs-attrs.md#d17e6984)**  ***anyURI***  
(*GND-URI*): Angabe des GND-HTTP URIs der beschriebenen Entität in der GND,
                    sofern vorhanden.(http|https)://d-nb.info/gnd/(.+)

**[`@agency`](specs-attrs.md#attr.record.agency)**  
(*Agenturanfrage*): Anfrage an die Agentur, welche Aktion in Hinblick auf den
                    Eintrag durchgeführt werden soll.
  **Möglicher Wertebereich**:  


 - '*create*' : (Eintrag in der GND neu anlegen) Der Eintrag soll als
                                Normdatensatz neu in der GND angelegt werden.
 - '*update*' : (Normdatensatz mit Daten aus dem Eintrag erweitern) Der
                                Normdatensatz soll durch neue Informationen angereichert werden.
 - '*merge*' : (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag
                                dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist,
                                zusammengeführt werden.
 - '*ignore*' : (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht
                                bearbeitet.



**[`@enrich`](specs-attrs.md#d17e7628)**  ***boolean***  
(*Anreicherung*): Der Record soll mit Daten eines externen Datendienstes
                    angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt,
                    um den Record anzureichern!

**[`foaf:page`](specs-elems.md#elem.foaf.page)**  
(*Webseite*): URL eines Dokuments mit näheren Informationen über die
                    beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.).

**[`gndo:acquaintanceshipOrFriendship`](specs-elems.md#d17e3345)**  
(*Beziehung, Bekanntschaft, Freundschaft*): Beziehung zwischen einer Person, Familie oder Körperschaft und einer anderen Person, Familie oder Körperschaft, die nicht näher bestimmt werden kann.

**[`gndo:academicDegree`](specs-elems.md#d17e3318)**  
(*Akademischer Grad*): Angaben zum akademischen Grad der Person, z.B. "Dr.
                    jur.".

**[`gndo:affiliation`](specs-elems.md#d17e3398)**  
(*Zugehörigkeit*): Die Person oder Körperschaft ist zugehörig zu einer
                    Körperschaft, oder ist mit einem Ort oder einem Event verbunden.

**[`gndo:broaderTerm`](specs-elems.md#d17e3502)**  
(*Oberbegriff*): Oberbegriff bzw. Oberklasse der beschriebenen
                    Entität.

**[`gndo:dateOfBirth`](specs-elems.md#person-record-dateOfBirth)**  
(*Geburtsdatum (Person)*): Das Geburtsdatum der
                Person.

**[`gndo:dateOfDeath`](specs-elems.md#person-record-dateOfDeath)**  
(*Sterbedatum (Person)*): Das Sterbedatum der
                Person.

**[`gndo:periodOfActivity`](specs-elems.md#prop-periodOfActivity)**  
(*Wirkungsdaten*): Zeitraum, innerhalb dessen die beschriebene Entität aktiv
                    war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind. Wenn ein exaktes Wirkungsdatum erschlossen werden soll, dann wird das Datum als ISO 8601 YYYY-MM-DD in sowohl `@iso-from` als auch in `@iso-to` angegeben.

**[`gndo:familialRelationship`](specs-elems.md#d17e4367)**  
(*Familiäre Beziehung*): Eine familiäre Beziehung zwischen einer Person oder Familie und einer anderen Person oder Familie.

**[`gndo:fieldOfStudy`](specs-elems.md#d17e4451)**  
(*Studienfach*): Das Studienfach bzw. -gebiet einer Person. Über `@gndo:ref`
                    kann ein entsprechendes Schlagwort aus der GND mit erfasst werden.

**[`gndo:functionOrRole`](specs-elems.md#d17e4550)**  
(*Funktion oder RolleFunction or role*): Anm.: Seit 2017 aus der GND
                entfernt!

**[`gndo:gender`](specs-elems.md#d17e4581)**  
(*Geschlecht*): Angaben zum Geschlecht einer
                Person.

**[`gndo:placeOfActivity`](specs-elems.md#person-record-placeOfActivity)**  
(*Wirkungsort (Person)*): Ein Wirkungsort, an dem die Person gewirkt oder
                    gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung
                    erfolgt die Erfassung des Ortes als Freitext im Element.

**[`gndo:placeOfBirth`](specs-elems.md#person-record-placeOfBirth)**  
(*Geburtsort (Person)*): Der Geburtsort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.

**[`gndo:placeOfDeath`](specs-elems.md#person-record-placeOfDeath)**  
(*Sterbeort (Person)*): Der Sterbeort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.

**[`gndo:playedInstrument`](specs-elems.md#prop-playedInstrument)**  
(*Gespieltes Instrument*): Ein Instrument, dass durch die beschriebene Entität gespielt wird.

**[`gndo:preferredName`](specs-elems.md#person-record-preferredName)**  
(*Vorzugsbenennung (Person)*): Die bevorzugte Namensform zur Denomination
                    einer Person.

**[`gndo:professionOrOccupation`](specs-elems.md#d17e5488)**  
(*Beruf oder Beschäftigung*): Angabe zum Beruf, Tätigkeitsbereich o.Ä., der
                    dokumentierten Person. Zulässige Deskriptoren stammen aus der GND-Systematik und entprechen GND-URIs von Sachbegriffen, z.B.
                    https://d-nb.info/gnd/4168391-2 ("Lyriker").

**[`gndo:publication`](specs-elems.md#d17e5604)**  
(*Publikation*): Eine Publikation, die mit der Entität in Zusammenhang steht.
                    Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden
                    sind und auch (3) um Titel über die Entität handeln.

**[`gndo:pseudonym`](specs-elems.md#d17e5556)**  
(*Pseudonym*): Ein Pseudonym, unter dem die dokumentierte Person ebenfalls
                    bekannt ist.

**[`gndo:titleOfNobility`](specs-elems.md#d17e5969)**  
(*Adelstitel*): Ein Adelsname der dokumentierten Person oder
                    Personengruppe.

**[`gndo:topic`](specs-elems.md#d17e6005)**  
(*Thema*): Thema, das sich auf eine Körperschaft, eine Konferenz, eine Person, eine Familie, ein Thema oder ein Werk bezieht.

**[`gndo:variantName`](specs-elems.md#person-record-variantName)**  
(*Variante Namensform (Person)*): Eine alternative Namensform, unter der eine
                    Person ebenfalls bekannt ist.

**[`dc:title`](specs-elems.md#d17e3011)**  
(*Titel (Record)*): Ein Titel für die Ressource. `dc:title` wird dann zur
                    Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll.

**[`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)**  
(*Biographische Angaben*): Biographische oder historische Angaben über die
                    Entität.

**[`dublicateGndIdentifier`](specs-elems.md#dublicates)**  
(*GND-Identifier einer Dublette*): Der GND-Identifier einer möglichen
                    Dublette.

**[`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)**  
(*Geographischer Schwerpunkt*): Ländercode(s) der Staat(en), denen die Entität
                    zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und
                    eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular
                    (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!

**[`gndo:gndIdentifier`](specs-elems.md#d17e4693)**  
(*GND-Identifier*): Der GND-Identifier eines in der GND eingetragenen
                    Normdatensatzes sofern vorhanden

**[`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)**  
(*GND-Sachgruppe*): Term aus dem GND-Sachgruppen Vokabular (ehemals
                    SWD)

**[`img`](specs-elems.md#elem.img)**  
(*Bild*): Ein Bild bzw. ein URL zu einer
                Bildressource.

**[`gndo:languageCode`](specs-elems.md#d17e4843)**  
(*Sprachraum*): Code(s) des Sprachraumes, dem die Entität zugeordnet werden
                    kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein
                    Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben
                werden!

**[`gndo:relatesTo`](specs-elems.md#d17e5757)**  
(*Beziehung*): Beziehung der beschriebenen Entität zu einer anderen Entität,
                    entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der
                    GNDO!

**[`ref`](specs-elems.md#elem.ref)**  
(*Hyperlink*): Ein Link zu einer Webressource, die weitere Informationen über
                    die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target`
                    dokumentiert.

**[`owl:sameAs`](specs-elems.md#elem.owl.sameAs)**  
(*Gleiche Entität*): HTTP-URI der selben Entität in einem anderen
                    Normdatendienst.

**[`skos:note`](specs-elems.md#elem.skos.note)**  
(*Anmerkung*): Eine Anmerkung zum Eintrag.

**[`source`](specs-elems.md#elem.source)**  
(*Quellenangabe*): Angaben zu einer Quelle, die verwendet wurde, um die in
                    diesem Eintrag gegebenen Informationen zu recherchieren.

**[`anyElement.narrow`](specs-elems.md#any-restricted)**  
(*ANY ELEMENT (eng gefasst)*): Ein Element, dessen **QName** einem *custom
                    namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist.

**[`revision`](specs-elems.md#revision)**  
(*Revisionsbeschreibung*): Statusinformationen zu einer Ressource
                    (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur
                    Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung
                    nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der
                Revisionsbeschreibung!

---

