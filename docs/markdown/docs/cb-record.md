
<a name="cb-record"></a>
# Körperschaften

[TODO]

```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:placeOfBusiness"/>
      <element name="gndo:precedingCorporateBody"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:succeedingCorporateBody"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


**[`@gndo:type`](specs-attrs.md#d17e720)**  
(*Körperschaftstyp*): Typisierung der dokumentierten Körperschaft
                            (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als
                            [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody).
  **Möglicher Wertebereich**:  


 - '*https://d-nb.info/standards/elementset/gnd#Company*' : (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#Company)
 - '*https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody*' : (Fiktive Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - '*https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody*' : (Musikalische Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - '*https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody*' : (Organ einer Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - '*https://d-nb.info/standards/elementset/gnd#ProjectOrProgram*' : (Projekt oder Programm)
                [GNDO](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit*' : (Religiöse Verwaltungseinheit)
                [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody*' : (Religiöse Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)



**[`@xml:id`](specs-attrs.md#d17e7648)**  ***ID***  
(*Record ID*): Angabe eines Identifiers zur Identifikation eines
                    Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!

**[`@gndo:uri`](specs-attrs.md#d17e6984)**  ***anyURI***  
(*GND-URI*): Angabe des GND-HTTP URIs der beschriebenen Entität in der GND,
                    sofern vorhanden.(http|https)://d-nb.info/gnd/(.+)

**[`@agency`](specs-attrs.md#attr.record.agency)**  
(*Agenturanfrage*): Anfrage an die Agentur, welche Aktion in Hinblick auf den
                    Eintrag durchgeführt werden soll.
  **Möglicher Wertebereich**:  


 - '*create*' : (Eintrag in der GND neu anlegen) Der Eintrag soll als
                                Normdatensatz neu in der GND angelegt werden.
 - '*update*' : (Normdatensatz mit Daten aus dem Eintrag erweitern) Der
                                Normdatensatz soll durch neue Informationen angereichert werden.
 - '*merge*' : (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag
                                dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist,
                                zusammengeführt werden.
 - '*ignore*' : (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht
                                bearbeitet.



**[`@enrich`](specs-attrs.md#d17e7628)**  ***boolean***  
(*Anreicherung*): Der Record soll mit Daten eines externen Datendienstes
                    angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt,
                    um den Record anzureichern!

**[`foaf:page`](specs-elems.md#elem.foaf.page)**  
(*Webseite*): URL eines Dokuments mit näheren Informationen über die
                    beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.).

**[`gndo:abbreviatedName`](specs-elems.md#d17e3272)**  
(*Abgekürzter Name*): Eine abgekürzte Namensform der dokumentierten
                    Entität.

**[`gndo:affiliation`](specs-elems.md#d17e3398)**  
(*Zugehörigkeit*): Die Person oder Körperschaft ist zugehörig zu einer
                    Körperschaft, oder ist mit einem Ort oder einem Event verbunden.

**[`gndo:broaderTerm`](specs-elems.md#d17e3502)**  
(*Oberbegriff*): Oberbegriff bzw. Oberklasse der beschriebenen
                    Entität.

**[`gndo:dateOfEstablishment`](specs-elems.md#d17e4104)**  
(*Gründungsdatum*): Das Datum, an dem die beschriebene Entität entstanden ist
                    bzw. gegründet wurde.

**[`gndo:dateOfEstablishmentAndTermination`](specs-elems.md#d17e4135)**  
(*Gründungs- und Auflösungsdatum*): Zeitraum, innerhalb dessen die
                    beschriebene Entität bestand.

**[`gndo:placeOfBusiness`](specs-elems.md#d17e5119)**  
(*Sitz (Körperschaft)*): Der (Haupt)sitz der
                Körperschaft.

**[`gndo:precedingCorporateBody`](specs-elems.md#d17e5335)**  
(*Vorherige Körperschaft (Körperschaft)*): Eine Vorgängerinstitution der
                    Körperschaft.

**[`gndo:preferredName`](specs-elems.md#d17e5423)**  
(*Vorzugsbenennung (Standard)*): Eine bevorzugte Namensform, um die
                    beschriebenen Entität zu bezeichnen.

**[`gndo:publication`](specs-elems.md#d17e5604)**  
(*Publikation*): Eine Publikation, die mit der Entität in Zusammenhang steht.
                    Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden
                    sind und auch (3) um Titel über die Entität handeln.

**[`gndo:succeedingCorporateBody`](specs-elems.md#d17e5836)**  
(*Nachfolgende Körperschaft (Körperschaft)*): Eine Nachfolgeinstitution der
                    Körperschaft.

**[`gndo:temporaryName`](specs-elems.md#d17e5924)**  
(*Zeitweiser Name*): Eine Namensform, die zeitweise zur Bezeichnung der
                    dokumentierten Entität benutzt wurde.

**[`gndo:topic`](specs-elems.md#d17e6005)**  
(*Thema*): Thema, das sich auf eine Körperschaft, eine Konferenz, eine Person, eine Familie, ein Thema oder ein Werk bezieht.

**[`gndo:variantName`](specs-elems.md#elem.gndo.variantName.standard)**  
(*Variante Namensform (Standard)*): Eine alternative Namensform, um die
                    beschriebenen Entität zu bezeichnen.

**[`dc:title`](specs-elems.md#d17e3011)**  
(*Titel (Record)*): Ein Titel für die Ressource. `dc:title` wird dann zur
                    Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll.

**[`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463)**  
(*Biographische Angaben*): Biographische oder historische Angaben über die
                    Entität.

**[`dublicateGndIdentifier`](specs-elems.md#dublicates)**  
(*GND-Identifier einer Dublette*): Der GND-Identifier einer möglichen
                    Dublette.

**[`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode)**  
(*Geographischer Schwerpunkt*): Ländercode(s) der Staat(en), denen die Entität
                    zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und
                    eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular
                    (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!

**[`gndo:gndIdentifier`](specs-elems.md#d17e4693)**  
(*GND-Identifier*): Der GND-Identifier eines in der GND eingetragenen
                    Normdatensatzes sofern vorhanden

**[`gndo:gndSubjectCategory`](specs-elems.md#d17e4734)**  
(*GND-Sachgruppe*): Term aus dem GND-Sachgruppen Vokabular (ehemals
                    SWD)

**[`img`](specs-elems.md#elem.img)**  
(*Bild*): Ein Bild bzw. ein URL zu einer
                Bildressource.

**[`gndo:languageCode`](specs-elems.md#d17e4843)**  
(*Sprachraum*): Code(s) des Sprachraumes, dem die Entität zugeordnet werden
                    kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein
                    Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben
                werden!

**[`gndo:relatesTo`](specs-elems.md#d17e5757)**  
(*Beziehung*): Beziehung der beschriebenen Entität zu einer anderen Entität,
                    entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der
                    GNDO!

**[`ref`](specs-elems.md#elem.ref)**  
(*Hyperlink*): Ein Link zu einer Webressource, die weitere Informationen über
                    die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target`
                    dokumentiert.

**[`owl:sameAs`](specs-elems.md#elem.owl.sameAs)**  
(*Gleiche Entität*): HTTP-URI der selben Entität in einem anderen
                    Normdatendienst.

**[`skos:note`](specs-elems.md#elem.skos.note)**  
(*Anmerkung*): Eine Anmerkung zum Eintrag.

**[`source`](specs-elems.md#elem.source)**  
(*Quellenangabe*): Angaben zu einer Quelle, die verwendet wurde, um die in
                    diesem Eintrag gegebenen Informationen zu recherchieren.

**[`anyElement.narrow`](specs-elems.md#any-restricted)**  
(*ANY ELEMENT (eng gefasst)*): Ein Element, dessen **QName** einem *custom
                    namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist.

**[`revision`](specs-elems.md#revision)**  
(*Revisionsbeschreibung*): Statusinformationen zu einer Ressource
                    (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur
                    Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung
                    nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der
                Revisionsbeschreibung!

---

