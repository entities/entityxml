
<a name="entity-geographic-area"></a>
# Geographischer Schwerpunkt einer Entität

Entitäten lassen sich geographisch einordnen bzw. bestimmen: Bei Personen kann man örtlich gebundene Lebensmittelpunkte oder Wirkungsorte beschreiben. Körperschaften und Bauwerke lassen sich mit Ländern und Ortschaften assozieren, genauso wie Werke der Literatur, Kunst und Wissenschaft usw.

Zur Dokumentation eines geographischen Schwerpunkts dient [``gndo:geographicAreaCode``](specs-elems.md#elem.gndo.geographicAreaCode). Um eine <span class="emph">maschienelle Verarbeitung</span> dieser Information zu gewährleisten, muss ein Ländercode nach ISO 3166 via `@gndo:term` angegeben werden. Die GND stellt hierfür das Vokabular "[Geographic Area Codes](https://d-nb.info/standards/vocab/gnd/geographic-area-code)" zur Verfügung:

```xml
<place xml:id="goslar_rathaus" gndo:uri="http://d-nb.info/gnd/4231845-2">
   <gndo:preferredName>Rathaus Goslar</gndo:preferredName>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI">
       Niedersachsen
   </gndo:geographicAreaCode>
   <!-- ... -->
</place>
```

Eine geographische Einordnung entspricht in der Regel realen Orten, Gebieten etc. Für Fällen, bei denen eine Entität geographisch einem fingierten Ort zugeordnet werden soll, wird der Deskriptor `https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ` verwendet.

```xml
<person xml:id="malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName><gndo:personalName>Malachlabra</gndo:personalName></gndo:preferredName>
   <foaf:page>https://forgottenrealms.fandom.com/wiki/Malachlabra</foaf:page>
   <skos:note>Malachlabra ist ein fiktiver Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ">
      Avernus, Neun Höllen (Fiktiver Ort)
   </gndo:geographicAreaCode>
</person>
```

Sofern der geographische Schwerpunkt einer Entität unbekannt ist, dann kann dies über den Deskriptor `https://d-nb.info/standards/vocab/gnd/geographic-area-code#ZZ` verzeichnet werden.

Für eine Einspielung in die GND gelten **besonderer Anforderungen** in Hinblick auf verwendete Area Codes! Kontinentangaben (z.B. `XA`) müssen **immer** mit der Angabe eines konkreten Landes kompletiert sein, z.B. `XA-DE`.

Zur korrekten Erschließung von Ländercodes entsprechend der GND-Spezifika siehe im Besonderen im [GND-Wiki](https://wiki.dnb.de/x/O5FjBQ) unter "Ländercode-Leitfaden" und "Ländercodes"

**Zusätzliche Ressourcen**:


- Das Feld 043 (Ländercode) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/043.pdf)
