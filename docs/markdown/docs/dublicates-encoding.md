
<a name="dublicates-encoding"></a>
# Dokumentation von Mehrfacheinträgen

Als entscheidendes Kriterium für einen Normdatendienst gilt die ein-eindeutige Identifizierbarkeit von Entitäten durch Identifier. Datendienste wie die GND arbeiten daher mit hohem Ressourceneinsatz daran, ihre Daten frei von Dubletten zu halten. Nichtsdestoweniger kann es vorkommen, dass im Rahmen der GND Mehrfacheinträge zu einer Entität vorhanden sind, die bisher noch nicht dedubliziert wurden.

entityXML ermöglicht die Dokumentation solcher **Mehrfacheinträgen** via [`dublicateGndIdentifier`](specs-elems.md#dublicates), hier am Beispiel eines Personeneintrags:

```xml
<person xml:id="edwald_albee" gndo:uri="https://d-nb.info/gnd/118501380">
   <dc:title>Albee, Edwald</dc:title>
   <dublicateGndIdentifier>185806961</dublicateGndIdentifier>
</person>
```

Der Normdatensatz, dessen HTTP-URI in `@gndo:uri` erhoben wird, gilt immer als **Haupteintrag** und die über `dubcliateGndIdentifier` erschlossenen GND-IDs beschreiben *dublikate Einträge*, die mit dem Haupteintrag zusammengeführt werden sollten. 

Um eine gewünschte Zusammenführung für den Eintrag anzugeben, wird der entsprechende Eintrag via `@agency` als `merge` gekennzeichnet:

```xml
<person xml:id="edwald_albee" gndo:uri="https://d-nb.info/gnd/118501380" agency="merge">
   <dc:title>Albee, Edwald</dc:title>
   <dublicateGndIdentifier>185806961</dublicateGndIdentifier>
</person>
```

In der Regel werden Dubletten, wenn sie im Rahmen der Dedublizierung erkannt wurden, innerhalb der GND zusammengeführt und auf einen entsprechenden Haupteintrag umgeleitet. Es kann dennoch vorkommen, dass die URL-Weiterleitung für einzelne Einträge nicht vorgenommen wurde und der entsprechende Eintrag einen Hinweis: "Status: Datensatz ist nicht mehr Bestandteil der Gemeinsamen Normdatei (GND)" verzeichnet.
