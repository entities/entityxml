
<a name="agency-mode-record"></a>
# Verarbeitungshinweis für die Agentur

Als Austauschformat sollte entityXML vorallem dem <span class="emph">Datenaustausch</span> (genauer: den Prozesse des Austauschens von Daten) zwischen Datengebern (z.B. Projekte, die für ihre eigenen Zwecke Entitäten beschreiben) auf der einen und einer GND-Agentur auf der anderen Seite eine definierte Struktur geben - so die Idee. Zu dieser Struktur gehören auch Informationen darüber, was mit einem Eintrag aus Sicht der Datengeber eigentlich geschehen soll: Handelt es sich bei einem Eintrag um eine Entität, die noch nicht in der GND enthalten ist und daher angelegt werden soll? Oder sollen die in einem Eintrag enthaltenen Information einem bereits bestehenden Normdatensatz hinzugefügt werden?

Der **Verarbeitungshinweis** soll diese Fragen addressierbar machen: Für jeden Eintrag lässt sich mit Hilfe von [``@agency``](specs-attrs.md#attr.record.agency) ein Hinweis vergeben, wie der Eintrag von einer GND Agentur verarbeitet werden soll. Diesbezüglich stehen folgende Deskriptoren zur Verfügung:


- *create*: Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
- *update*: Ein bestehender GND Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, soll durch neue Informationen aus diesem Eintrag angereichert werden.
- *merge*: Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem GND Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
- *ignore*: Der Eintrag wird von der Agentur nicht bearbeitet.

```xml
<place xml:id="lummerland" agency="create">
   <gndo:preferredName>Lummerland</gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation>
      Eine fiktive Insel aus den Geschichten von Michael Ende.
   </gndo:biographicalOrHistoricalInformation>
   <!-- ... -->
</place>
```


> **Experimentelles Feature**  
> Der Verarbeitungshinweis ist zwar ein essentieller Bestandteil, befindet sich momentan aber noch in der Erprobung. Wie produktiv sich hiermit die Bearbeitung von Einträgen strukturieren lässt, wird sich erst noch in der Praxis zeigen müssen.


