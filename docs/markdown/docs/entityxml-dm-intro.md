
<a name="entityxml-dm-intro"></a>
# Allgemeines

Das entityXML <span class="emph">Dokument Modell</span> verwendet folgenden Standard Namensraum:

**`https://sub.uni-goettingen.de/met/standards/entity-xml#`**

Zusätzliche Namespaces, die in entityXML Schema verwendet werden, sind: 

- **bf**: [http://id.loc.gov/ontologies/bibframe/](http://id.loc.gov/ontologies/bibframe/)
- **dc**: [http://purl.org/dc/elements/1.1/](http://purl.org/dc/elements/1.1/)
- **dnb**: [https://www.dnb.de](https://www.dnb.de)
- **foaf**: [http://xmlns.com/foaf/0.1/](http://xmlns.com/foaf/0.1/)
- **geo**: [http://www.opengis.net/ont/geosparql#](http://www.opengis.net/ont/geosparql#)
- **gndo**: [https://d-nb.info/standards/elementset/gnd#](https://d-nb.info/standards/elementset/gnd#)
- **owl**: [http://www.w3.org/2002/07/owl#](http://www.w3.org/2002/07/owl#)
- **skos**: [http://www.w3.org/2004/02/skos/core#](http://www.w3.org/2004/02/skos/core#)
- **wgs84**: [http://www.w3.org/2003/01/geo/wgs84_pos#](http://www.w3.org/2003/01/geo/wgs84_pos#)


entityXML Dokumente bestehen aus einem [`entityXML`](specs-elems.md#entity-xml) Wurzelelement, das eine oder mehrere Sammlungen enthalten kann:

```xml
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:foaf="http://xmlns.com/foaf/0.1/" 
   xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
   xmlns:owl="http://www.w3.org/2002/07/owl#"
   xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
   xmlns:geo="http://www.opengis.net/ont/geosparql#" 
   xmlns:dnb="https://www.dnb.de"
   xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#">
   <collection>
      <metadata>
         <!-- Metadata Section -->
      </metadata>
      <data>
         <!-- Data Section -->
      </data>
   </collection>
</entityXML>
```

<a name="docs_d14e954"></a>
## Datensammlung

Ein entityXML Datensammlung wird durch das Element [`collection`](specs-elems.md#collection) repräsentiert, das sich aus einem Metadaten- ([`metadata`](specs-elems.md#collection-metadata)) und einem Datenbereich ([`data`](specs-elems.md#data-section)) zusammensetzt. 

 Der Metadatenbereich stellt den Rahmen bereit, in dem Metadaten mit Blick auf den Erschließungskontext einer Datensammlung dokumentiert werden können. 

Im Datenbereich werden die eigentlichen Entitäteneinträge erschlossen bzw. dokumentiert.
