
<a name="entity-same-as"></a>
# Gleiche Entität in anderer Normdatei oder anderem Datendienst

Entitäten können bereits in anderen (Norm)datendiensten verzeichnet sein (z.B. [Library of Congress Name Authority
                     File](https://id.loc.gov/authorities/names.html), [VIAF](https://viaf.org/), [Deutschen Digitalen Bibliothek](https://www.deutsche-digitale-bibliothek.de/), [Wikidata](https://www.wikidata.org/) etc.) 

Einträge, die in entityXML angelegt werden, lassen sich mit solchen "externen" Einträgen anderer Datendienste mittels [``owl:sameAs``](specs-elems.md#elem.owl.sameAs) in Verbindung bringen. Dies dient grundsätzlich der eindeutigen Identifizierung der beschriebenen Entität:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:preferredName>
      <gndo:surname>Nguyen-Kim</gndo:surname>, <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <owl:sameAs>https://orcid.org/0000-0003-4787-0508</owl:sameAs>
   <owl:sameAs>https://viaf.org/viaf/3094149844949202960003</owl:sameAs>
   <owl:sameAs>https://www.wikidata.org/wiki/Q29169693</owl:sameAs>
   <owl:sameAs>https://id.loc.gov/authorities/names/n2019011012</owl:sameAs>
</person>
```

Darüberhinaus lassen sich so auch andere Informationsprozesse anschließen, um bspw. auf Informationen, die durch die entsprechenden Datendienste zur Verfügung gestellt werden, zuzugreifen.
