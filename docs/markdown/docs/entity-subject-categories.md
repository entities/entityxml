
<a name="entity-subject-categories"></a>
# Sachgruppen

Durch die Zuordnung zu Sachgruppen können Entitäten systematisch klassifiziert werden. In entityXML werden die [Sachgruppen der GND](https://d-nb.info/standards/vocab/gnd/gnd-sc.html) (ehemals SWD, Schlagwortnormdatei) verwendet.

Die Dokumentation einer oder mehrerer Sachgruppen für eine Entität erfolgt durch [``gndo:gndSubjectCategory``](.md#elem.gndo.gndSubjectCategory), wobei jede Sachgruppe in jeweils einem eigenen `gndo:gndSubjectCategory` angegeben wird. Obligatorisch ist die Angabe des Sachgruppen-URIs aus dem oben angeführten [GND-Sachgruppen Vokabulars](https://d-nb.info/standards/vocab/gnd/gnd-sc.html) via `@gndo:term`. Empfohlen wird zudem die Angabe des menschenlesbaren Labels innerhalb des Elements als Textknoten:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:preferredName>
      <gndo:surname>Nguyen-Kim</gndo:surname>, <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#22.5p">Personen zu Chemie</gndo:gndSubjectCategory>
   <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#15.4p">Personen zu Rundfunk, Neuen Medien</gndo:gndSubjectCategory>             
</person>
```

Sofern englisch-sprachige (oder andere nicht-deutsch-sprachige) Labels bei der Dokumentation verwendet werden, bietet es sich an Entsprechendes via `@xml:lang` anzugeben:

```xml
<gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#15.4p" xml:lang="en">Persons associated with broadcasting, with new media</gndo:gndSubjectCategory>
```
