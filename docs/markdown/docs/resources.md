
<a name="resources"></a>
# Ressourcen

entityXML umfasst als Werkzeugkasten zahlreiche Komponenten und Ressourcen, die sich im offiziellen [GitLab Repository](https://gitlab.gwdg.de/entities/entityxml) befinden:

- `/schema`: Hier liegen die <span class="emph">Validierungsroutinen</span> von entityXML, sprich ein RNG-Schema inkl. Schematron Regeln
- `/scripts`: Hier liegen <span class="emph">XSLT</span> und <span class="emph">XQuery Skripte</span> zur Verarbeitung von entityXML bereit
- `/assets`: Zusätzliche Dateien und <span class="emph">CSS Stylesheets</span>, die unter anderem für den oXygen Author Modus verwendet werden
- `/samples`: Hier liegen entityXML Beispiele zum Nachvollziehen und Ausprobieren.
- `/templates`: Copy & Paste Templates zu einzelnen Entitätseinträgen.


<a name="entityxml-schema-intro"></a>
## Schema

Im Verzeichnis `schema/` befindet sich das Herzstück von entityXML: Das RNG-Validierungsschema [**`entityXML.rng`**](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/entityXML.rng). 
> **Version 0.3.2 als 'Untermieter'**  
>  Offiziell aus Gründen der Abwärtskompatibilität (inoffiziell aus dem einfachen Grund, dass sich entityXML noch in der ALPHA-Phase befindet) "schlummert" hier noch die "ältere Schwester" (v.0.3.2) der aktuellen Version. Nach Abschluss der ALPHA Phase wird sie ausziehen. 



Das Validierungsscheme ist in der XML Syntax von **[RELAX NG](https://relaxng.org/)** (kurz: <span class="emph">RNG</span>) implementiert. RNG ist eine XML Validerungssprache, mit deren Hilfe XML-Modelle, also ein definiertes Set von **Elementen, Attributen zzgl. ihrer Inhaltsmodelle**, erstellt werden können. RNG sorgt also vorallem dafür, dass die Struktur einer entityXML Ressource klar definiert ist: Welches Element darf welche anderen Elemente enthalten? Welche Attribute können in welchen Elementen gesetzt werden? Welche Werte können in einem Attribut benutzte werden? Alles Fragen, die im Rahmen des RNG-Schemas beantwortet werden.

Das RNG-Schema enthält zusätzlich **[Schematron](https://www.schematron.com/)**, eine weitere Validierungssprache für XML Dateien, die im Rahmen von entityXML der **erweiterten Qualitätssicherung** dient: sog. "Business Rules", also kleine in Schematron definierte Regeln, sorgen für eine feingliedrigere Überprüfung von entityXML Daten als es allein mit RNG möglich wäre. Als Beispiel für eine konkrete Schematron Regel in entityXML wäre hier die Überprüfung der obligatorischen Vorzugsbenennung für Entitäten zu nennen, sofern diese nicht bereits durch einen GND-URI (`@gndo:uri`) mit einem Normdateneintrag in der GND identifiziert wurden. Die hinterliegende "Businessrule" wäre etwa: "Wie in der GND, ist jede Entität durch eine bevorzugte Namensform benannt." 

<a name="docs_d14e574"></a>
### Erweiterungen

RNG ermöglicht es, verhältnismäßig einfach <span class="emph">Erweiterungen</span> zu einem existierenden Schema zu implementieren. Im Verzeichnis `schema/extensions` befinden sich bereits zwei Erweiterungen zum entityXML Schema, die spezifische Anwendungsfälle abbilden.

[`entityXML.bibframe.rng`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/extensions/entityXML.bibframe.rng) implementiert einen `bf:Instance` Eintrag, wobei es sich um eine an [Bibframe](https://www.loc.gov/bibframe/) orientierte Klasse handelt, mit der Instanziierungen von Werken beschrieben werden können, und die nun stellvertretend für `manifestation` und `expression` Einträge verwendet werden kann.

[`entityXML.project.rng`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/extensions/entityXML.project.rng) hingegen implementiert ein `project` Element, das im Rahmen des Workflows der Text+ GND Agentur benutzt wird, um Metadaten zu Projekten zu erfassen, und das, unabhängig von gelieferten Datensammlungen.

<a name="entityxml-scripts-intro"></a>
## Skripte

Im Verzeichnis `scripts/` befinden sich verschiedene Verarbeitungsskripte für entityXML Daten. Die Masse ist in <span class="emph">XSL</span> oder <span class="emph">XQuery</span> geschrieben. Dabei soll es aber nicht bleiben.

Die Spezifikation einer XML-Grammatik wie entityXML ist eine Sache. Die Verarbeitung von Daten, die dieser Grammatik folgen, eine Andere. Und genau dieser zweite Aspekt, also die Verarbeitung von entityXML Daten und die Erzeugung von Informationen auf Grundlage dieser Daten durch (automatisierte) Informationsprozesse ist der Ausgangspunkt für die Skripte in diesem Verzeichnis.

**Nehmen wir beispielsweise folgendes Szenario**: Projekt X erschließt Entitäten mit entityXML. Die projektspezifische Datenbank ist allerdings keine XML Datenbank, sondern eine JSON-Datenbank. Was tun? entityXML ist zwar nicht entity*JSON*, aber lässt sich über [`entityxml2json.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2json.xsl) einfach in <span class="emph">JSON</span> konvertieren. 

**Betrachten wir ein zweites Szenario**: Forschergruppe Y arbeitet an einer spezifischen Personendatenbank mit projektspezifischen Daten, möchte aber Informationen aus der GND in ihre eigenen Bestände nachladen (und nicht per copy-paste in ihre Einträge händisch kopieren). [`enrich-records.lobid.xquery`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xquery/enrich-records.lobid.xquery) reichert alle Einträge mit Daten aus [LOBID](https://lobid.org/gnd) an, die mit `@enrich="true"` markiert sind und GND-URIs verzeichnen.

**Ein abschließendes drittes Szenario**: Ein langfristiges Ziel der Text+ GND Agentur ist die (halb)automatisierte Einspeisung von entityXML Daten in die GND. Die GND (konkreter meine ich hier das Informationssystem der GND) kennt allerdings kein entityXML, genausowenig wie Microsoft Excel oder Microsoft Word (oder andere Formate, die gerne ins Land gefürt werden, um Daten an die GND zwecks Austausch zu senden). Aber was im Rahmen des GND Workflows für den Austausch von Daten eine Rolle spielt, ist [MARC](https://www.loc.gov/marc/) (siehe auch hier bei der [DNB](https://www.dnb.de/DE/Professionell/Metadatendienste/Exportformate/MARC21/marc21_node.html)). MARC kennen nun allerdings die wenigsten Nutzer und Nutzerinnen der GND … ein Dilemma zeichnet sich ab. *Long story short*: Für dieses Problem gibt es [`entityxml2marcxml.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/marcxml/entityxml2marcxml.xsl), um aus einer entityXML Ressource ein Format - nämlich [MARC-XML](https://www.loc.gov/standards/marcxml/) - zu erzeugen, dass sich in die GND einspielen lässt.

Im Kapitel "[Serialisierungen](serialisation.md)" findet sich eine kleine Auflistung bereits existierender Konversionsroutinen.

<a name="docs_d14e663"></a>
## Assets

Im Verzeichnis `assets/` sind zusätzliche Dateien untergebracht, die bei der Verarbeitung von entityXML verwendet werden (können).

Die wohl prominentesten Dateien in diesem Verzeichnis, genauer in `assets/author` sind die <span class="emph">CSS Dateien</span>, die das Rendering von entityXML Dateien im Rahmen des [Author Modes von oXygen XML](oxygen-editor.md#oxygen-author-mode) definieren.
