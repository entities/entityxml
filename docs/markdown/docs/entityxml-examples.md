
<a name="entityxml-examples"></a>
# Beispiele

Dieser Bereich ist Beispielauszeichnungen in entityXML gewidmed. Hier finden sich ganze Beispieleinträge genau so wie kleinere Schnipsel, um einzelne Auszeichnungsstrategien zu verdeutlichen.

<a name="docs_d14e2781"></a>
## Personeneintrag, vollständig

Hier mal ein exemplarischer Personendatensatz, der so ziemlich alle Eigenschaften abbildet.

> **Fun Fact**  
> Bei diesem Datensatz handelt es sich um unseren Testdatensatz für die MARC-Konversion, sprich: Wenn ihr euch hieran haltet, dann könnt ihr davon ausgehen, dass späteres MARC, das aus euren Daten erzeugt wird, sauberes GND-MARC darstellt!


```xml
<person xml:id="p0123456789" gndo:uri="https://d-nb.info/gnd/132010445" agency="merge">
  <foaf:page>https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/uwe-sikora/</foaf:page>
  <gndo:academicDegree>Prof. Dr.</gndo:academicDegree>
  <gndo:acquaintanceshipOrFriendship gndo:ref="https://d-nb.info/gnd/118535307">
      <gndo:forename>Anna</gndo:forename>
      <gndo:surname>Freud</gndo:surname>
      <note>Korrespondenzpartnerin</note>
  </gndo:acquaintanceshipOrFriendship>
  <gndo:affiliation gndo:ref="https://d-nb.info/gnd/2024315-7">Georg-August-Universität Göttingen</gndo:affiliation>
  <gndo:biographicalOrHistoricalInformation>Max Mustermann existiert nicht! Er ist ein Dummy, um alles zu testen!</gndo:biographicalOrHistoricalInformation>
  <gndo:dateOfBirth iso-date="1500-05-10">10.05.1500</gndo:dateOfBirth>
  <gndo:dateOfDeath iso-date="2024-08-06">06.08.2024</gndo:dateOfDeath>
  <gndo:familialRelationship>
      <gndo:forename>Luise</gndo:forename>
      <gndo:surname>Schmidt</gndo:surname>
      <note>Mutter</note>
  </gndo:familialRelationship>
  <gndo:familialRelationship gndo:ref="https://d-nb.info/gnd/4021477-1">
      <gndo:forename>Heinrich</gndo:forename>
      <gndo:surname>Schmidt</gndo:surname>
      <gndo:prefix>von der</gndo:prefix>
      <note>Bruder</note>
  </gndo:familialRelationship>
  <gndo:fieldOfStudy gndo:ref="https://d-nb.info/gnd/1271364182">Modellierung von Daten</gndo:fieldOfStudy>
  <gndo:functionOrRole></gndo:functionOrRole>
  <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male https://d-nb.info/standards/vocab/gnd/gender#female">divers</gndo:gender>
  <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE"/>
  <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#9.5p">9.5p Personen zu Soziologie, Gesellschaft, Arbeit, Sozialgeschichte </gndo:gndSubjectCategory>
  <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#34.3p">Iwas mit Sport</gndo:gndSubjectCategory>
  <gndo:languageCode gndo:term="http://id.loc.gov/vocabulary/iso639-2/deu">de</gndo:languageCode>
  <gndo:periodOfActivity iso-from="1500" iso-to="2024">1500-2024</gndo:periodOfActivity>
  <gndo:periodOfActivity iso-from="1500" iso-to="2024">
      <label>1500-2024</label>
      <note>Überirdisches Leben</note>
  </gndo:periodOfActivity>
  <gndo:placeOfActivity gndo:ref="https://d-nb.info/gnd/4021477-1" iso-date="2002-10-11">Göttingen</gndo:placeOfActivity>
  <gndo:placeOfActivity iso-from="2003-05" iso-to="2004-06">Göttingen</gndo:placeOfActivity>
  <gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4021477-1">Goettingen</gndo:placeOfBirth>
  <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4021477-1">Die Stadt, die Baustellen schafft!</gndo:placeOfDeath>
  <gndo:playedInstrument>Tenor</gndo:playedInstrument>
  <gndo:playedInstrument gndo:ref="https://d-nb.info/gnd/4148299-2">
      <label>Countertenor</label> 
      <note>Stimmlage</note>
  </gndo:playedInstrument>
  <gndo:preferredName>
      <gndo:surname>Mustermann</gndo:surname>, 
      <gndo:forename>Max</gndo:forename>
  </gndo:preferredName>
  <gndo:preferredName type="original" script="Arab" xml:lang="ara">
      <gndo:forename>احمد بن محمد</gndo:forename>
      <gndo:prefix>ال</gndo:prefix>
      <gndo:surname>مرزوقي</gndo:surname>
  </gndo:preferredName>
  <gndo:professionOrOccupation gndo:type="significant" gndo:ref="https://d-nb.info/gnd/4025243-7">Hochschullehrer</gndo:professionOrOccupation>
  <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/1271364182">Datenfuzzi</gndo:professionOrOccupation>
  <gndo:professionOrOccupation>Gummibärchentester</gndo:professionOrOccupation>
  <gndo:pseudonym gndo:ref="https://d-nb.info/gnd/1155094360">Uwe Sikora</gndo:pseudonym>
  <gndo:publication gndo:ref="https://d-nb.info/1290732000"><title>Paradoxien der Demokratie</title><date>2024</date></gndo:publication>
  <gndo:publication>Werk ohne Autor</gndo:publication>
  <gndo:relatesTo gndo:code="beza" gndo:ref="https://d-nb.info/gnd/1155094360">Lehrer von: Uwe Sikora</gndo:relatesTo>
  <gndo:titleOfNobility>Universalfürst von Groß-Göttingen</gndo:titleOfNobility>
  <gndo:variantName>
      <gndo:prefix>von</gndo:prefix>
      <gndo:surname>Musterjunge</gndo:surname>, 
      <gndo:forename>Maximilian</gndo:forename>
      <gndo:counting>III.</gndo:counting>
      <gndo:nameAddition>Graf von Großgöttingen</gndo:nameAddition>
      <gndo:epithetGenericNameTitleOrTerritory>Sonnengeküster</gndo:epithetGenericNameTitleOrTerritory>
  </gndo:variantName>
  <gndo:variantName>
      <gndo:personalName>Mustermann</gndo:personalName>
      <gndo:epithetGenericNameTitleOrTerritory>Der Grimmige</gndo:epithetGenericNameTitleOrTerritory>
      <gndo:nameAddition>von und zu Göttingen</gndo:nameAddition>
  </gndo:variantName>
  <gndo:variantName xml:lang="ara" script="Arab">
      <gndo:personalName>رضا كحالة, عمر</gndo:personalName>
  </gndo:variantName>
  <gndo:variantName>
      <gndo:personalName>b. Sina</gndo:personalName>
  </gndo:variantName>
  <owl:sameAs>http://viaf.org/viaf/45840123</owl:sameAs>
  <owl:sameAs>http://id.loc.gov/rwo/agents/n88010268</owl:sameAs>
  <skos:note>Das hier ist bloß ein Testdatensatz, um alle Felder zu testen, die bei einer Person erschlossen werden können!</skos:note>
  <skos:note gndo:type="internal">Das ist eine interne Anmerkung!</skos:note>
  <source url="https://de.wikipedia.org/wiki/Arnold_von_Jungenfeld">Wikipedia</source>
  <source url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/uwe-sikora/">
      <title>Homepage</title>
      <note>Stand: 29.10.2018</note> 
  </source>
  <revision status="opened">
      <change when="2024-05-20" who="US">Datensatz angelegt.</change>
  </revision>
</person>
```
