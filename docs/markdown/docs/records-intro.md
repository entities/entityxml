
<a name="records-intro"></a>
# Einstieg ins Anlegen von Entitätseinträgen

Ein Entitätseintrag bzw. ein Record repräsentiert die Beschreibung einer konkreten Entität einer bestimmten Klasse. entityXML unterstützt derzeit folgende Entitätsklassen:


- [`entity`](specs-elems.md#entity-record) Allgemeine Entität (eine Art Oberklasse aller anderen Entitätstypen)
- [`person`](specs-elems.md#person-record) Person
- [`corporateBody`](specs-elems.md#corporate-body-record) Körperschaft
- [`place`](specs-elems.md#d17e1265) Ort
- [`work`](specs-elems.md#d17e1648) Werk

Es gibt noch zwei weitere Recordklassen, die bisher pro forma im Schema enthalten sind und noch nicht wirklich ausspezifiziert sind, weil hierfür noch keine Anwendungsfälle bestehen:


- [`expression`](specs-elems.md#d17e1449) Ausdrucksform eines Werks (Expression)
- [`manifestation`](specs-elems.md#d17e1518) Manifestation einers Werks (Manifestation)

entityXML ist in erster Linie ein <span class="emph">Markup Format</span>! Das bedeuted, dass Wissen über eine Eintität zunächst ersteinmal in Form von (menschenlesbarem) Fließtext repräsentiert werden kann. Bei dieser Form der Repräsentation von Daten als Text spricht man auch gerne von <span class="emph">unstrukturierten Daten</span>:

```xml
<entity xml:id="erika_muster">
   In meiner neuen Geldbörse habe ich ein Papierkärtchen entdeckt, dass 
   eine "Mustermann, Erika" verzeichnet. Auf Wikipedia fand ich heraus, 
   dass es sich um eine fiktive Person handelt, die als Platzhalter
   für eine beliebige (reale) Frau steht.
</entity>
```

Dieser Fließtext wird dann in der Folge durch entsprechendes Markup angereichert, um das im Text enthaltene Wissen über eine Entität explizit und <span class="emph">maschienenlesbar</span> (d.h. die Form der Daten ist so angelegt, dass sie einen direkten systematischen Zugriff auf die Informationsinhalte durch eine Maschiene erlaubt) zu machen. Diese durch Markup angereicherte Form von (textlichen) Daten bezeichnet man auch als <span class="emph">semistrukturierte Daten</span>: 

```xml
<entity xml:id="erika_muster">
   In meiner neuen Geldbörse habe ich ein Papierkärtchen entdeckt, dass 
   eine "<dc:title>Mustermann, Erika</dc:title>" verzeichnet. 
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref> 
   schreibt, <note>es handle sich um eine fiktive Person, die als Platzhalter
   für eine beliebige (reale) Frau steht</note>.
</entity>
```

Dementsprechen kann man Entitätsbeschreibungen bzw. Entitätseinträge auch generell in einer eher strukturierten Form anlegen, die weniger "textlich" gehalten ist: 

```xml
<entity xml:id="max_muster">
   <skos:note>Eine fiktive Person, die als Platzhalter fungiert</skos:note>
   <source>Eine kleine Karte in meiner neuen Geldbörse</source>
   <dc:title>Mustermann, Max</dc:title>
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref>
</entity>
```

> **Records in entityXML sind also strukturierte Daten?**  
> Nein, es handelt sich nachwievor um semistrukturierte Daten! Die Informationsinhalte sind zwar strukturierter <span class="emph">dargestellt</span>, aber es ist (und bleibt) Text mit Markup. Nichtsdestoweniger folgen entityXML Daten durch die Anbindung an ein Schema einer definierten Struktur und können somit als "strukturierter" gelten, als andere XML Daten, die nicht nach einem spezifizierten Schema erschlossen wurden.


Auch wenn die in entityXML spezifizierten Entitätsklassen je unterschiedliche Informationen enthalten können, teilen sie sich neben der <span class="emph">[Revisionsbeschreibung](lifecycle.md#revision-desc)</span>, die bereits eingehend erläutert wurde, folgende Elemente und Attribute: 

- **Record ID** [`@xml:id`](specs-attrs.md#d17e7648) (*verpflichtend*)  
  "Angabe eines Identifiers zur Identifikation eines
                    Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!"
- **GND-URI** [`@gndo:uri`](specs-attrs.md#d17e6984) (*optional*)  
  "Angabe des GND-HTTP URIs der beschriebenen Entität in der GND,
                    sofern vorhanden."
- **Agenturanfrage** [`@agency`](specs-attrs.md#attr.record.agency) (*optional*)  
  "Anfrage an die Agentur, welche Aktion in Hinblick auf den
                    Eintrag durchgeführt werden soll."
- **Anreicherung** [`@enrich`](specs-attrs.md#d17e7628) (*optional*)  
  "Der Record soll mit Daten eines externen Datendienstes
                    angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt,
                    um den Record anzureichern!"



- **Titel (Record)** [`dc:title`](specs-elems.md#d17e3011) (*optional*)  
  "Ein Titel für die Ressource. `dc:title` wird dann zur
                    Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll."
- **Biographische Angaben** [`gndo:biographicalOrHistoricalInformation`](specs-elems.md#d17e3463) (*optional und wiederholbar*)  
  "Biographische oder historische Angaben über die
                    Entität."
- **GND-Identifier einer Dublette** [`dublicateGndIdentifier`](specs-elems.md#dublicates) (*optional und wiederholbar*)  
  "Der GND-Identifier einer möglichen
                    Dublette."
- **Geographischer Schwerpunkt** [`gndo:geographicAreaCode`](specs-elems.md#elem.gndo.geographicAreaCode) (*optional und wiederholbar*)  
  "Ländercode(s) der Staat(en), denen die Entität
                    zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und
                    eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular
                    (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!"
- **GND-Identifier** [`gndo:gndIdentifier`](specs-elems.md#d17e4693) (*optional und wiederholbar*)  
  "Der GND-Identifier eines in der GND eingetragenen
                    Normdatensatzes sofern vorhanden"
- **GND-Sachgruppe** [`gndo:gndSubjectCategory`](specs-elems.md#d17e4734) (*optional und wiederholbar*)  
  "Term aus dem GND-Sachgruppen Vokabular (ehemals
                    SWD)"
- **Bild** [`img`](specs-elems.md#elem.img) (*optional und wiederholbar*)  
  "Ein Bild bzw. ein URL zu einer
                Bildressource."
- **Sprachraum** [`gndo:languageCode`](specs-elems.md#d17e4843) (*optional und wiederholbar*)  
  "Code(s) des Sprachraumes, dem die Entität zugeordnet werden
                    kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein
                    Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben
                werden!"
- **Beziehung** [`gndo:relatesTo`](specs-elems.md#d17e5757) (*optional und wiederholbar*)  
  "Beziehung der beschriebenen Entität zu einer anderen Entität,
                    entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der
                    GNDO!"
- **Hyperlink** [`ref`](specs-elems.md#elem.ref) (*optional und wiederholbar*)  
  "Ein Link zu einer Webressource, die weitere Informationen über
                    die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target`
                    dokumentiert."
- **Gleiche Entität** [`owl:sameAs`](specs-elems.md#elem.owl.sameAs) (*optional und wiederholbar*)  
  "HTTP-URI der selben Entität in einem anderen
                    Normdatendienst."
- **Anmerkung** [`skos:note`](specs-elems.md#elem.skos.note) (*optional und wiederholbar*)  
  "Eine Anmerkung zum Eintrag."
- **Quellenangabe** [`source`](specs-elems.md#elem.source) (*optional und wiederholbar*)  
  "Angaben zu einer Quelle, die verwendet wurde, um die in
                    diesem Eintrag gegebenen Informationen zu recherchieren."
- **ANY ELEMENT (eng gefasst)** [`anyElement.narrow`](specs-elems.md#any-restricted) (*optional und wiederholbar*)  
  "Ein Element, dessen **QName** einem *custom
                    namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist."


