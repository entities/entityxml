
<a name="entityxml-versions"></a>
# Versionen

Der <span class="emph">master branch</span> des [entityXML GitLab](https://gitlab.gwdg.de/entities/entityxml) entspricht dem sog. *Nightly Snapshot*, also dem zwischenzeitlichen, (mehr oder weniger) tagesaktuellen und unter Umständen ungetesteten Entwicklungsstand von entityXML.

<span class="emph">Offizielle Releases und Versionen</span> werden in einem separaten [Update Repository](https://gitlab.gwdg.de/entities/updates/-/tree/main/entityxml?ref_type=heads) hochgeladen und können als [oXygen Add-On installiert
                  werden](setup.md#entityxml-addon).Die offiziellen Releases stimmen in der Regel mit den <span class="emph">GitLab Releases</span> überein, die mittels Tags im GitLab vorbereitet und veröffentlich werden. Das <span class="emph">Alpha Release</span> ist z.B. **[Version 0.5.0](https://gitlab.gwdg.de/entities/entityxml/-/releases/v0.5.0)** unter dem Tag [`v0.5.0`](https://gitlab.gwdg.de/entities/entityxml/-/tree/v0.5.0).

Einen Überblick über alle Tags gibt's [hier](https://gitlab.gwdg.de/entities/entityxml/-/tags), über alle GitLab Releases [hier](https://gitlab.gwdg.de/entities/entityxml/-/releases) und die Revisionsbeschreibung [hier](revision-history.md).

Dateien aus Tags können unter Verwendung des entsprechenden Tagnamens aufgerufen bzw. eingebunden werden, z.B. das Schema und die Autor Modus CSS der Version 0.5.0 mit dem Tag `v0.5.0`:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

Um entityXML Ressourcen einer bestimmten Version auf eine aktuellere Version (oder die aktuelle Nightly) zu updaten, können die Scripte im Verzeichnis [`/scripts/xslt/update`](https://gitlab.gwdg.de/entities/entityxml/-/tree/master/scripts/xslt/update) verwendet werden.
