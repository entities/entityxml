# entityXML Framework(v.0.6.5 BETA): Handbuch

## Inhalt

- [entityXML: Ein Austausch- und Speicherformat](#introduction)
	- [Als Austauschformat](#docs_d14e88)
	- [Als Speicherformat](#docs_d14e130)
- [Über dieses Handbuch](#about)
- [Setup und Technisches](#docs_d14e195)
	- [Setup](#setup)
		- [Einfache Verwendung: entityXML for takeaway](#docs_d14e223)
		- [Download](#entityxml-offline)
		- [Verwendung als oXygen XML Framework](#oxygen-framwork)
			- [Standardinstallation als oXygen Add-On für Anwender*innen](#entityxml-addon)
			- [Integration als Framework für Entwickler*innen](#entityxml-framework-integration)
				- [Option 1: Feste Integration](#docs_d14e386)
				- [Option 2 (Bevorzugt): Lockere Integration als zusätzliches Frameworks](#docs_d14e403)
	- [Versionen](#entityxml-versions)
	- [Ressourcen](#resources)
		- [Schema](#entityxml-schema-intro)
			- [Erweiterungen](#docs_d14e574)
		- [Skripte](#entityxml-scripts-intro)
		- [Assets](#docs_d14e663)
	- [Arbeiten in oXygen XML](#docs_d14e681)
		- [oXygen Author Mode](#oxygen-author-mode)
			- [Views](#docs_d14e705)
			- [Filter](#docs_d14e741)
			- [Verwendung des Author Mode bei installiertem Framework](#author-mode-framework)
			- [Verwendung des Author Mode ohne Framework Installation](#docs_d14e781)
- [entityXML Document Model](#docs_d14e865)
	- [Allgemeines](#entityxml-dm-intro)
		- [Datensammlung](#docs_d14e954)
	- [Metadaten einer Datensammlung](#metadata-section)
		- [Titel einer Sammlung](#docs_d14e997)
		- [Kurzbeschreibung](#docs_d14e1010)
		- [Providerbeschreibung](#provider-stmt)
		- [Beschreibung von Verantwortlichkeiten](#resp-stmt)
	- [entityXML Lifecycle](#lifecycle)
		- [Revisionsbeschreibung](#revision-desc)
			- [Anmerkung zur Ausführlichkeit von Revisionsbeschreibungen](#docs_d14e1249)
	- [Datenbereich](#data-collection)
	- [Entitätseintrag im Allgemeinen](#docs_d14e1313)
		- [Einstieg ins Anlegen von Entitätseinträgen](#records-intro)
		- [Verwendung fremder Namensräume](#custom-namespaces)
			- [Von der einfachen Anwendung ...](#docs_d14e1451)
			- [... hin zum Mittelweg ...](#docs_d14e1484)
			- [... und auf die Spitze getrieben!](#lido)
			- [Arbeiten mit schemafremden Elementen im Author Mode](#docs_d14e1539)
			- [Verfahrensweise mit Daten aus fremden Namensräumen bei der Text+ GND-Agentur](#docs_d14e1552)
		- [Verarbeitungshinweis für die Agentur](#agency-mode-record)
		- [Anmerkungen](#entity-notes)
		- [Biographische und/oder historische Angaben zur Entität](#entity-biogr-histogr)
		- [Dokumentation von Mehrfacheinträgen](#dublicates-encoding)
		- [Geographischer Schwerpunkt einer Entität](#entity-geographic-area)
		- [Namens- bzw. Titelangaben](#entity-title)
			- [Titel als Fallback](#docs_d14e1853)
		- [Quellenangabe](#entity-source)
		- [Sachgruppen](#entity-subject-categories)
		- [Gleiche Entität in anderer Normdatei oder anderem Datendienst](#entity-same-as)
		- [Sonstige Webressourcen](#entity-links)
	- [Spezifische Entitätsklassen](#docs_d14e2046)
		- [Personen](#person-record)
			- [Unterscheidung von Personentypen](#docs_d14e2094)
			- [Erschließung von Vorzugsbenennung und Namensvarianten](#person-name)
			- [Geographische Zuordnung](#person-geographical-area-code)
			- [Lebens- und Wirkungsdaten](#person-dates)
			- [Lebens- und Wirkungsorte](#person-places)
			- [Angaben zum Geschlecht](#person-gender)
			- [Berufe und Tätigkeiten](#person-profession)
			- [Titelangaben/Veröffentlichungen](#person-publications)
			- [Beispiel 1: Maxima Musterfrau](#docs_d14e2441)
			- [Beispiel 2: Ernest Chevalier](#docs_d14e2451)
			- [Modell: Personeneintrag](#docs_d14e2461)
		- [Körperschaften](#cb-record)
		- [Orte](#place-record)
		- [Werke](#work-record)
- [Tutorials](#docs_d14e2511)
	- [HOW TO: Person](#docs_d14e2517)
- [Beispiele](#entityxml-examples)
	- [Personeneintrag, vollständig](#docs_d14e2781)
- [Technische Spezifikation](#doc-specs)
- [Serialisierungen](#serialisation)
	- [XML](#docs_d14e2885)
	- [JSON](#docs_d14e2908)
	- [Markdown](#docs_d14e2928)
	- [CSV](#docs_d14e2943)
- [Revisionen](#revision-history)


---

<a name="introduction"></a>
## entityXML: Ein Austausch- und Speicherformat

entityXML ist (bisher) eine Konzeptstudie in der Version *0.6.5* (BETA), die darauf abzielt, ein einheitliches XML basiertes Datenformat für die Text+ GND Agentur zu modellieren.

Das Format soll dabei vorallem drei Aspekte abdecken: Die Bereitstellung eines einheitlichen (1) **Austausch**- und (2) **Speicherformats** zur Beschreibung von Entitäten, das direkt auf die GND gemappt werden kann, und der Text+ GND Agentur zudem als (3) **Workflow-Steuerungsinstrument** dient.

<a name="docs_d14e88"></a>
### Als Austauschformat

Als <span class="emph">Austauschformat</span> für die Text+ GND-Agentur soll entityXML als transparentes und dokumentiertes Format dienen, um Daten zwischen Datenlieferanten (etwa digitale Editions- oder Erschließungsprojekte) und der Text+ GND Agentur systematisch (d.h. gemäß eines wohl definierten Ablaufs) austauschen zu können. Aus diesem Grund baut das entityXML Schema grundlegend auf der [GND-Ontologie](https://d-nb.info/standards/elementset/gnd) als Beschreibungsmodell von Entitäten auf. 

Daten, die in entityXML angelegt und ausgetauscht werden, können mit Hilfe des [entityXML
                     Schemas](#entityxml-schema-intro) sowohl auf Nutzer- als auch auf Agenturseite validiert werden, um eine erste und regelmäßige Qualitätssicherung zu gewährleisten. Das Datenmodell ermöglicht in der Folge die Implementierung [generischer
                     Transformationsszenarien](#entityxml-scripts-intro), die in Zukunft unteranderem den Einspeisevorgang in die GND unterstützen sollen. 

Desweiteren untertützt entityXML die Kommunikation zwischen Datenlieferanten und der Text+ GND Agentur, indem Fragen, Anmerkungen und anderweitige Informationen entweder für eine ganze Lieferung oder für einzelne Einträge direkt mit erfasst und ausgewertet werden können.

Das Wichtigste zum Aspekt "<span class="emph">Austauschformat</span>" in Kürze: 

- entityXML baut auf den Klassen und Properties der GND Ontologie auf
- Qualitätssicherung in Hinblick auf die Spezifikation der GND durch Validierung mit dem entityXML Schema
- Automatisierte Transformationsszenarien zum Export von entityXML Daten (z.B. zur Einspeisung in die GND)
- Workflowsteuerung der GND-Agentur
- Fragen an eine GND Agentur können direkt an den Daten dokumentiert werden


<a name="docs_d14e130"></a>
### Als Speicherformat

Als <span class="emph">Speicherformat</span> für Projekte bietet entityXML zahlreiche Möglichkeiten der Verwendung: Einerseits hat man hiermit eine niedrigschwellige Möglichkeit an der Hand, Entitäten wartungsarm und ohne komplexe Speicherlösung zu dokumentieren und zu speichern. Die erschlossenen Daten bleiben somit zum Einen als Erschließungsleistung der entsprechenden Projekte transparent (z.B. zum Nachweis von Arbeitsleistungen für Förderinstitutionen). Zum Zweiten können sie einfach in bestehende Forschungsdatenmanagementkonzepte (z.B. spezifische Versionierungsworkflows, Langzeitarchivierung etc.) eingebunden werden. Zum Dritten (und hier liegt ein echter Vorteil zu Excel und Co.) können die (XML) Daten, die auf diese Weise entstehen, direkt in projektspezifischen Softwarelösungen genutzt werden (z.B. Einspeisung in Webportale, Datenbanken, Erstellung von Registern, etc.).

entityXML basiert zwar auf der Idee, die GND-Ontologie zur Beschreibung von Entitäten zu verwenden. Allerdings ist das Schema <span class="emph">offen</span> gehalten, sprich: Sofern an die Daten, die dokumentiert werden sollen, Anforderungen gestellt werden, die sich über die GND-Ontologie nicht beschreiben lassen, dann bietet entityXML die Möglichkeit, [schemafremde
                     Elemente und Attribute aus eigenen Namensräumen](#custom-namespaces) mit zu erschließen. Diese Informationen werden zwar bei der Einspeisung in die GND vernachlässigt, nichtsdestoweniger bleiben sie den Projekten als integrale Bestandteile der Forschungsdaten erhalten und können selbstverständlich weiter genutzt werden.

Darüberhinaus bietet entityXML die Möglichkeit sowohl auf Datensammlungs- als auch auf Eintragsebene detailierte Revisionsbeschreibungen vorzunehmen, um Recherchestände zu dokumentieren - und kann dementsprechend dabei helfen projektinterne Arbeitsabläufe zu unterstützen.

Das Wichtigste zum Aspekt "<span class="emph">Speicherformat</span>" in Kürze: 

- Projekte können ihre Daten unabhängig von einer komplexen Speicherlösung speichern
- Recherchearbeit bleibt transparent und nachvollziehbar
- Daten bleiben als Forschungsdaten des Projektes transparent 
- Daten können versioniert werden
- Bearbeitungsstand kann beschrieben und so dokumentiert werden
- Enkoppelung von Projekten und GND in Hinblick auf Zeitpläne, da Datenlieferanten nicht auf die Eintragung in die GND warten müssen, sondern ihre Daten direkt nutzen können.
- Möglichkeit der Verwendung von Elementen in *custom namespaces*
- Markup, also etwas mit dem vorallem Editionsprojekte Erfahrung haben


<a name="about"></a>
## Über dieses Handbuch

Dieses Handbuch ist als Benutzungsleitfaden für entityXML gedacht, der zugleich die technische Dokumentation enthält. Es soll in erster Linie als Einführung und Nachschlagewerk in die Verwendung das Formats dienen, enthält aber auch technische Hinweise zur Verwendung hilfreicher Komponenten, die ebenfalls Teil von entityXML sind.

Das Handbuch wird parallel zur Implementierung des entityXML Schemas (die technische Formatspezifikation) gepflegt, d.h. die Dokumentation der technischen Spezifikationen in <span class="emph">Anhang 1</span> entspricht immer dem Schema.

Wie das Format selbst, befindet sich auch dieses Handbuch im Aufbau! Daher können Fehler zum Einen nicht ausgeschlossen werden. Zum Anderen sind viele Bereich bisher nur unvollständig oder garnicht dokumentiert. Das wird sich in der nächsten Zeit ändern.

Für Fragen, Anregungen etc. zu entityXML stehe ich gerne auch persönlich zur Verfügung. Wenn ihr also in diesem Handbuch nicht fündig werdet, dann schreibt mir doch einfach eine Mail an sikora[at]sub.uni-goettingen.de.

<a name="docs_d14e195"></a>
## Setup und Technisches

<a name="setup"></a>
### Setup

entityXML ist nicht bloß als XML-Format, sondern vorallem als <span class="emph">Werkzeugkasten</span> gedacht, der einerseits das entityXML Schema und andererseits zahlreiche Tools (Konversionen, Stylesheets etc.) umfasst. 

Zusätzlich liefert entityXML ein <span class="emph">Document Type Association Framework</span> für den [oXygen XML Editor](https://www.oxygenxml.com/) aus (im Folgenden "Framework"), das als [oXygen Add-On installiert werden kann](#entityxml-addon). Selbstverständlich können die einzelnen Komponenten aber auch ohne oXygen verwendet werden.

Die folgenden Schritte lohnen sich also, wenn man oXygen oder einen anderen XML Editor verwendet, der "schema-bewusst" arbeitet, d.h. die Daten direkt in Abhängigkeit des angegebenen Validierungsschema überprüft.

<a name="docs_d14e223"></a>
#### Einfache Verwendung: entityXML for takeaway

Um entityXML auf die einfachste Art zu nutzen, kopiert man einfach die folgenden 4 Zeilen Code (hierbei handelt es sich um sog. *Processing Instructions*) in eine leere XML Datei und ist fertig:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

Diese *Processing Instructions* verweisen auf das entityXML RNG Schema, die Schematron Validierungsroutinen und die Author Mode CSS im GitLab. Das ist alles, um zu starten! Wenn man auf diese Art mit entityXML arbeitet, arbeitet man immer mit der aktuellen Schemaversion. Allerdings benötigt man eine aktive Internetverbindung.

Hier eine ausführlicheres Rumpftemplate zum einfachen Copy-Paste:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#" 
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:dnb="https://www.dnb.de" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#"> 
    <collection>
        <metadata>
            <title></title>
            <abstract></abstract>
            <provider id=""></provider>
            <revision status=""></revision>
        </metadata>
        <data>
            <list>
                
            </list>
        </data>
    </collection>
</entityXML>
```

<a name="entityxml-offline"></a>
#### Download

Wer sich nicht von einer aktiven Internetverbindung abhängig machen möchte, der kann sich das [entityXML GitLab Repository](https://gitlab.gwdg.de/entities/entityxml) einfach runterladen. Hier hat man zwei Möglichkeiten:

**(1)** Download des Repositories über das Download Icon im GitLab. Hier kann man zwischen 4 Kompressionsformaten wählen (*.zip, *.tar.gz, *.tar.bz2, *.tar) und bekommt dann ein entsprechend komprimiertes Archiv, aus dem dann das entityXML Verzeichnis entpackt und dann an einem beliebigen Ort abgelegt werden kann.

**(2)** Klon des GitLab Repositories mittels Git:


- Via *HTTP*: (`git clone https://gitlab.gwdg.de/entities/entityxml.git`)
- Via *SSH*: (`git clone git@gitlab.gwdg.de:entities/entityxml.git`)

Auf diese Weise hat man die Möglichkeit die lokal gespeicherte Version von entityXML mit Hilfe von Git immer up-to-date zu den Entwicklungsständen im GitLab zu halten.

Diese Art, entityXML auf seinem lokalen Rechner zu speichern ist eher für erfahrene **Entwickler*innen** gedacht, die das entityXML Framework aktiv mitentwickeln oder für eigene Zwecke anpasssen möchten. Für **Anwender*innen** empfielt sich die [Installation
                  als oXygen Add-On](#entityxml-addon).

Damit stehen alle Ressourcen zur Verfügung, um offline zu arbeiten. Was sich nun ändert, sind die Pfade, die in den *Processing Instructions* verwendet werden, um die entsprechenden Dateien zu lokalisieren.

**Hier ein Beispiel**: Mal angenommen, wir laden das entityXML Repository auf einen lokalen Computer runter, und zwar - sagen wir - in das Verzeichnis `Schreibtisch/Arbeit/Entitätenerschließung`. Dieser Ordner hat ein Unterverzeichnis `data`, in dem wir unsere entityXML Dateien anlegen und pflegen möchten. Zum Zwecke dieses Beispiels legen wir eine entityXML Datei an, die wir `entitäten_personen.xml` nennen. Die Ordnerstruktur sieht dann wie folgt aus:

```text
Schreibtisch/
 └── Arbeit/
      └── Entitätenerschließung/
           └── data/		
                └── entitäten_personen.xml
           └── entityXML/	
                └── docs
                └── schema/
                     └── entityXML.rng
                └── assets/
                     └── css/
                          └── author/
                               └── entities.author.css
                └── ...
```

Vor diesem Hintgrund müssten wir die oben angeführten Pfade in unserer Beispieldatei `entitäten_personen.xml` entsprechend unserer Verzeichnisstruktur anpassen:

```xml
<?xml-model href="../entityXML/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="../entityXML/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="../entityXML/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="../entityXML/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>

```

<a name="oxygen-framwork"></a>
#### Verwendung als oXygen XML Framework

entityXML wird als <span class="emph">oXygen XML Framework</span> ausgeliefert. Somit kann das Wissen um die Validierung und Verarbeitung von entityXML Daten in oXygen XML fest integriert werden: Sofern eine entityXML Ressource in oXygen geöffnet wird, wird sie automatisch validiert. Fest definierte Transformationsszenarien stehen gleichsam direkt zur Verfügung. Kurz um: Wer regelmäßig mit entityXML arbeitet und die volle Bandbreite des entityXML Werkzeugkastens nutzen möchte, erleichtert sich die Arbeit durch die Frameworkintegration von entityXML in oXygen XML enorm. 

Wählt man diesen Weg, weden außerdem die *Processing Instructions* zu Beginn einer entityXML Ressource überflüssig.

<a name="entityxml-addon"></a>
##### Standardinstallation als oXygen Add-On für Anwender*innen

Anwender*innen, die entityXML im oXygen XML-Editor in vollem Umfang nutzen wollen, können das Framework als [oXygen Add-On](https://www.oxygenxml.com/doc/versions/26.0/ug-editor/glossary/plugin.html) installieren. Hierbei handelt es sich um die einfachste Art entityXML zu installieren. Außerdem kümmert sich oXygen bei dieser Installationart in der Folge selbständig um Updates, sprich: Wenn eine neue Version von entityXML veröffentlicht wird, weist oXygen darauf hin und das Framework kann direkt auf die neueste Version geupdated werden!

Um entityXML als Add-On zu installieren, wird wie folgt verfahren:



- Kopiere diesen Link: [`https://gitlab.gwdg.de/entities/updates/-/raw/main/entityxml/updates.xml`](https://gitlab.gwdg.de/entities/updates/-/raw/main/entityxml/updates.xml). Das ist der Link zur entityXML Update Site, die alle veröffentlichten Versionen von entityXML listet.
- Klicke in oXygen auf `Hilfe > Neue Add-Ons installieren`.
- Kopierten Link in dem Feld `Add-Ons zeigen von` einfügen.
- Nun kann die aktuelle (oder eine alte) Version von entityXML installiert werden.
- Abschließend weist oXygen darauf hin, dass die Anwendung neu gestartet werden muss.


Nun sind sie im Besitz von entityXML und können es ohne Einschränkungen nutzen!

<a name="entityxml-framework-integration"></a>
##### Integration als Framework für Entwickler*innen

Für Entwickler*innen, die entityXML mitentwickeln oder für ihre eigenen Zwecke anpassen, bietet es sich an, entityXML als Framework in oXygen XML zu integrieren. Hier gibt es im Grunde **zwei Optionen**. Beide setzen voraus, dass das entityXML GitLab Repository als Kopie auf den entsprechenden Rechner lokal vorhanden ist, entweder als [Download](#entityxml-offline) oder als [geklontes Git Repository](https://gitlab.gwdg.de/entities/entityxml):

<a name="docs_d14e386"></a>
###### Option 1: Feste Integration



- Gehe zum `frameworks` Verzeichnis der oXygen XML Anwendung.
- ... Erstelle ein Verzeichnis `entityXML`.
- ... Kopiere den Inhalt des entityXML Repositories in das erstellte Verzeichnis.


<a name="docs_d14e403"></a>
###### Option 2 (Bevorzugt): Lockere Integration als zusätzliches Frameworks

Hierbei handelt es sich m.E. um die beste Option, um entityXML als Framework in oXygen einzubinden, denn mit der Hilfe von [Git](https://git-scm.com/) kann man so das lokale Verzeichnis von entityXML mit dem offiziellen GitLab Repository immer up-to-date halten (wenn man möchte):



- Wähle `Optionen` > `Einstellungen` im Menü aus.
- ... Wähle `Dokumenttypen-Zuordnung` von der linken Liste aus und klicke auf den Untereintrag `Orte`.
- ... Füge den Pfad zum Elternverzeichnis des entityXML Repositories auf deinem Computer zu `Zusätzliche Framework-Verzeichnisse` hinzu.


Auf diese Weise lässt sich entityXML auch in das oXygen Plugin des [TextGrid Laboratorys](https://textgrid.de/download) integrieren:



- Wähle `Fenster` > `Benutzervorgaben` im Menü aus.
- ... Wähle `Oxygen XML Editor` > `Document Type Association` von der linken Liste aus und klicke auf den Untereintrag `Locations`.
- ... Füge den Pfad zum Elternverzeichnis des entityXML Repositories auf deinem Computer zu `Additonal framework directories` hinzu.


<a name="entityxml-versions"></a>
### Versionen

Der <span class="emph">master branch</span> des [entityXML GitLab](https://gitlab.gwdg.de/entities/entityxml) entspricht dem sog. *Nightly Snapshot*, also dem zwischenzeitlichen, (mehr oder weniger) tagesaktuellen und unter Umständen ungetesteten Entwicklungsstand von entityXML.

<span class="emph">Offizielle Releases und Versionen</span> werden in einem separaten [Update Repository](https://gitlab.gwdg.de/entities/updates/-/tree/main/entityxml?ref_type=heads) hochgeladen und können als [oXygen Add-On installiert
                  werden](#entityxml-addon).Die offiziellen Releases stimmen in der Regel mit den <span class="emph">GitLab Releases</span> überein, die mittels Tags im GitLab vorbereitet und veröffentlich werden. Das <span class="emph">Alpha Release</span> ist z.B. **[Version 0.5.0](https://gitlab.gwdg.de/entities/entityxml/-/releases/v0.5.0)** unter dem Tag [`v0.5.0`](https://gitlab.gwdg.de/entities/entityxml/-/tree/v0.5.0).

Einen Überblick über alle Tags gibt's [hier](https://gitlab.gwdg.de/entities/entityxml/-/tags), über alle GitLab Releases [hier](https://gitlab.gwdg.de/entities/entityxml/-/releases) und die Revisionsbeschreibung [hier](#revision-history).

Dateien aus Tags können unter Verwendung des entsprechenden Tagnamens aufgerufen bzw. eingebunden werden, z.B. das Schema und die Autor Modus CSS der Version 0.5.0 mit dem Tag `v0.5.0`:

```xml
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/schema/entityXML.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/schema/entityXML.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/v0.5.0/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

Um entityXML Ressourcen einer bestimmten Version auf eine aktuellere Version (oder die aktuelle Nightly) zu updaten, können die Scripte im Verzeichnis [`/scripts/xslt/update`](https://gitlab.gwdg.de/entities/entityxml/-/tree/master/scripts/xslt/update) verwendet werden.

<a name="resources"></a>
### Ressourcen

entityXML umfasst als Werkzeugkasten zahlreiche Komponenten und Ressourcen, die sich im offiziellen [GitLab Repository](https://gitlab.gwdg.de/entities/entityxml) befinden:

- `/schema`: Hier liegen die <span class="emph">Validierungsroutinen</span> von entityXML, sprich ein RNG-Schema inkl. Schematron Regeln
- `/scripts`: Hier liegen <span class="emph">XSLT</span> und <span class="emph">XQuery Skripte</span> zur Verarbeitung von entityXML bereit
- `/assets`: Zusätzliche Dateien und <span class="emph">CSS Stylesheets</span>, die unter anderem für den oXygen Author Modus verwendet werden
- `/samples`: Hier liegen entityXML Beispiele zum Nachvollziehen und Ausprobieren.
- `/templates`: Copy & Paste Templates zu einzelnen Entitätseinträgen.


<a name="entityxml-schema-intro"></a>
#### Schema

Im Verzeichnis `schema/` befindet sich das Herzstück von entityXML: Das RNG-Validierungsschema [**`entityXML.rng`**](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/entityXML.rng). 
> **Version 0.3.2 als 'Untermieter'**  
>  Offiziell aus Gründen der Abwärtskompatibilität (inoffiziell aus dem einfachen Grund, dass sich entityXML noch in der ALPHA-Phase befindet) "schlummert" hier noch die "ältere Schwester" (v.0.3.2) der aktuellen Version. Nach Abschluss der ALPHA Phase wird sie ausziehen. 



Das Validierungsscheme ist in der XML Syntax von **[RELAX NG](https://relaxng.org/)** (kurz: <span class="emph">RNG</span>) implementiert. RNG ist eine XML Validerungssprache, mit deren Hilfe XML-Modelle, also ein definiertes Set von **Elementen, Attributen zzgl. ihrer Inhaltsmodelle**, erstellt werden können. RNG sorgt also vorallem dafür, dass die Struktur einer entityXML Ressource klar definiert ist: Welches Element darf welche anderen Elemente enthalten? Welche Attribute können in welchen Elementen gesetzt werden? Welche Werte können in einem Attribut benutzte werden? Alles Fragen, die im Rahmen des RNG-Schemas beantwortet werden.

Das RNG-Schema enthält zusätzlich **[Schematron](https://www.schematron.com/)**, eine weitere Validierungssprache für XML Dateien, die im Rahmen von entityXML der **erweiterten Qualitätssicherung** dient: sog. "Business Rules", also kleine in Schematron definierte Regeln, sorgen für eine feingliedrigere Überprüfung von entityXML Daten als es allein mit RNG möglich wäre. Als Beispiel für eine konkrete Schematron Regel in entityXML wäre hier die Überprüfung der obligatorischen Vorzugsbenennung für Entitäten zu nennen, sofern diese nicht bereits durch einen GND-URI (`@gndo:uri`) mit einem Normdateneintrag in der GND identifiziert wurden. Die hinterliegende "Businessrule" wäre etwa: "Wie in der GND, ist jede Entität durch eine bevorzugte Namensform benannt." 

<a name="docs_d14e574"></a>
##### Erweiterungen

RNG ermöglicht es, verhältnismäßig einfach <span class="emph">Erweiterungen</span> zu einem existierenden Schema zu implementieren. Im Verzeichnis `schema/extensions` befinden sich bereits zwei Erweiterungen zum entityXML Schema, die spezifische Anwendungsfälle abbilden.

[`entityXML.bibframe.rng`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/extensions/entityXML.bibframe.rng) implementiert einen `bf:Instance` Eintrag, wobei es sich um eine an [Bibframe](https://www.loc.gov/bibframe/) orientierte Klasse handelt, mit der Instanziierungen von Werken beschrieben werden können, und die nun stellvertretend für `manifestation` und `expression` Einträge verwendet werden kann.

[`entityXML.project.rng`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/schema/extensions/entityXML.project.rng) hingegen implementiert ein `project` Element, das im Rahmen des Workflows der Text+ GND Agentur benutzt wird, um Metadaten zu Projekten zu erfassen, und das, unabhängig von gelieferten Datensammlungen.

<a name="entityxml-scripts-intro"></a>
#### Skripte

Im Verzeichnis `scripts/` befinden sich verschiedene Verarbeitungsskripte für entityXML Daten. Die Masse ist in <span class="emph">XSL</span> oder <span class="emph">XQuery</span> geschrieben. Dabei soll es aber nicht bleiben.

Die Spezifikation einer XML-Grammatik wie entityXML ist eine Sache. Die Verarbeitung von Daten, die dieser Grammatik folgen, eine Andere. Und genau dieser zweite Aspekt, also die Verarbeitung von entityXML Daten und die Erzeugung von Informationen auf Grundlage dieser Daten durch (automatisierte) Informationsprozesse ist der Ausgangspunkt für die Skripte in diesem Verzeichnis.

**Nehmen wir beispielsweise folgendes Szenario**: Projekt X erschließt Entitäten mit entityXML. Die projektspezifische Datenbank ist allerdings keine XML Datenbank, sondern eine JSON-Datenbank. Was tun? entityXML ist zwar nicht entity*JSON*, aber lässt sich über [`entityxml2json.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2json.xsl) einfach in <span class="emph">JSON</span> konvertieren. 

**Betrachten wir ein zweites Szenario**: Forschergruppe Y arbeitet an einer spezifischen Personendatenbank mit projektspezifischen Daten, möchte aber Informationen aus der GND in ihre eigenen Bestände nachladen (und nicht per copy-paste in ihre Einträge händisch kopieren). [`enrich-records.lobid.xquery`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xquery/enrich-records.lobid.xquery) reichert alle Einträge mit Daten aus [LOBID](https://lobid.org/gnd) an, die mit `@enrich="true"` markiert sind und GND-URIs verzeichnen.

**Ein abschließendes drittes Szenario**: Ein langfristiges Ziel der Text+ GND Agentur ist die (halb)automatisierte Einspeisung von entityXML Daten in die GND. Die GND (konkreter meine ich hier das Informationssystem der GND) kennt allerdings kein entityXML, genausowenig wie Microsoft Excel oder Microsoft Word (oder andere Formate, die gerne ins Land gefürt werden, um Daten an die GND zwecks Austausch zu senden). Aber was im Rahmen des GND Workflows für den Austausch von Daten eine Rolle spielt, ist [MARC](https://www.loc.gov/marc/) (siehe auch hier bei der [DNB](https://www.dnb.de/DE/Professionell/Metadatendienste/Exportformate/MARC21/marc21_node.html)). MARC kennen nun allerdings die wenigsten Nutzer und Nutzerinnen der GND … ein Dilemma zeichnet sich ab. *Long story short*: Für dieses Problem gibt es [`entityxml2marcxml.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/marcxml/entityxml2marcxml.xsl), um aus einer entityXML Ressource ein Format - nämlich [MARC-XML](https://www.loc.gov/standards/marcxml/) - zu erzeugen, dass sich in die GND einspielen lässt.

Im Kapitel "[Serialisierungen](#serialisation)" findet sich eine kleine Auflistung bereits existierender Konversionsroutinen.

<a name="docs_d14e663"></a>
#### Assets

Im Verzeichnis `assets/` sind zusätzliche Dateien untergebracht, die bei der Verarbeitung von entityXML verwendet werden (können).

Die wohl prominentesten Dateien in diesem Verzeichnis, genauer in `assets/author` sind die <span class="emph">CSS Dateien</span>, die das Rendering von entityXML Dateien im Rahmen des [Author Modes von oXygen XML](#oxygen-author-mode) definieren.

<a name="docs_d14e681"></a>
### Arbeiten in oXygen XML

Unabhängig davon, ob entityXML nun als Framework in oXygen installiert ist, oder nicht: Die einzelnen Ressourcen, die entityXML als *Werkzeugkasten* ausmachen, stehen auch unabhängig von einer festen Integration als oXygen Framework zur [Verfügung](#resources). 

Die feste Integration von entityXML als Framework ist, wie bereits schon erwähnt, vorallem für Nutzer*innen interessant, die häufig mit dem Format arbeiten, denn es erleichtert die Anwendung von Konversionen oder die Benutzung des Author Modes wesentlich.

<a name="oxygen-author-mode"></a>
#### oXygen Author Mode

Um die Arbeit mit entityXML in oXygen intuitiver zu gestalten, werden mehrere <span class="emph">CSS Dateien</span> bereitgestellt, um die Autoren Ansicht zu strukturieren und mit einem benutzerfreundlicheren Design als der XML Ansicht auszustatten.

<a name="docs_d14e705"></a>
##### Views

Views definieren die generelle Struktur des entityXML Author Modes.


- **Basis View** (`/assets/css/author/entities.author.css`): Die Haupt-CSS des entityXML Frameworks, obligatorisch für den oXygen Author Mode. Dieser View rendert Einträge in prosaischer Form.
- **Strukturierte Ansicht** (`/assets/css/author/entities.author.structured.css`): Eine zusätzliche CSS zum Basis View, um Einträge in *halbstrukturierter Form* darzustellen. Eine bessere Übersichtlichkeit der Daten ist m.E. garantiert! 
	- **Vollstrukturierte Ansicht** (`/assets/css/author/entities.author.structured.full.css`): Erweiterung des Strukturierten Views, der alle Elemente eines Eintrags hierachisch abbildet.
- **Interaktive Ansicht** (`/assets/css/author/entities.author.interactive.css`): Zusätzliche CSS, die die Eingabe im Author Mode interaktiver durch Schaltflächen unterstützt. So können z.B. Revisionsbeschreibungen intuitiver angelegt werden oder IDs für Einträge automatisch erzeugt werden.

<a name="docs_d14e741"></a>
##### Filter

Filter steuern die Anzeige von Einträgen nach gewissen Kriterien.


- **Filter "Nur offene Einträge"** (`/assets/css/author/agency.open-records.css`): Filter Modus, der nur alle Einträge anzeigt, die noch nicht als abgeschlossen markiert sind.
- **Filter "Nur Einträge ohne GND-URI"** (`/assets/css/author/agency.no-gnd-uri.css`): Filter Modus, der nur alle Einträge anzeigt, die keinen GND-URI verzeichnen.

<a name="author-mode-framework"></a>
##### Verwendung des Author Mode bei installiertem Framework

Wenn entityXML bereits als Framework installiert ist, müssen keine weiteren Schritte unternommen werden. Wechselt man nun im oXygen XML Editor in den <span class="emph">Author Mode</span> (Unten links am Bildschirmrand unter **`Autor`**, siehe auch die offizielle [oXygen Dokomentation](https://www.oxygenxml.com/doc/versions/25.0/ug-editor/topics/editing-xml-documents-author.html)) können unter dem **`Stile`** Dropdownreiter (Oberer Menüleiste) die verschiedenen, im Framework vorkonfigurierten Stile bzw. Views zu- oder abgeschaltet werden, je nach persönlicher Vorliebe.

<a name="docs_d14e781"></a>
##### Verwendung des Author Mode ohne Framework Installation

Falls entityXML nicht als Framework installiert ist, lassen sich die CSS Stylesheets für den <span class="emph">Author Mode</span>, wie im [Setup Kapitel](#setup) einleitend beschrieben, auch manuell ohne Aufwand in die jeweilige entityXML Ressource einbinden und entsprechend verwenden. Dazu muss folgende *Processing Instruction* hinter die XML-Deklaration der entityXML Datei, die bearbeitet werden soll, gesetzt werden, um den **Basis View** des entityXML Author Modes zu verwenden:

```xml
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
```

Um zusätzlich die **Strukturierte Ansicht** nutzen zu können, muss eine weitere *Processing Instruction* hinter die Erste gesetzt werden:

```xml
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
```

**Wichtig (!)** ist hierbei, dass die **Titel** (der `title` Parameter in der *Processing Instruction*) der beiden verknüpften CSS Ressourcen identisch sind, damit oXygen beide Dateien zusammenführt und daraus einen View rendert. Als Konvention verwenden wir hierfür **`entityxml`**. Das Ergebnis in der XML Datei sollte nun etwa so aussehen:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- Hier die Autor Mode CSS -->
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.css" title="entityxml" alternate="no"?>
<?xml-stylesheet type="text/css" href="https://gitlab.gwdg.de/entities/entityxml/-/raw/master/assets/css/author/entities.author.structured.css" title="entityxml" alternate="yes"?>
<!-- Weitere Processing Instructions, z.B. zur Schemaanbindung -->
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" 
    xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
    xmlns:geo="http://www.opengis.net/ont/geosparql#" 
    xmlns:dnb="https://www.dnb.de"
    xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#">
    <!-- ... -->
</entityXML>
```

Nun kann der Author Mode, wie bei einer [festen Installation des entityXML Frameworks](#author-mode-framework) (siehe oben) genutzt werden.

Diese Form der Author Mode CSS Integration bringt den (verschmerzbaren) Nachteil mit sich, dass zwischen einzelnen Views nicht kompfortabel über den **`Stile`** Reiter hin und her gewechselt werden kann. Um die optionale **Strukturierte Ansicht** zu deaktivieren oder zu aktivieren, muss die entsprechende *Processing Instruction* entweder auskommentiert bzw. gelöscht werden (Strukturierte Ansicht ist aus) oder gesetzt sein (Strukturierte Ansicht ist an). 

Dieser Ansatz setzt eine bestehende Internetverbindung voraus. Wenn man sicher gehen will, dass das ganze Setup auch <span class="emph">offline</span> funktioniert, [kann man sich die Dateien lokal speichern und die Pfade in
                           den Processing Instructions entprechend anpassen](#entityxml-offline).

<a name="docs_d14e865"></a>
## entityXML Document Model

<a name="entityxml-dm-intro"></a>
### Allgemeines

Das entityXML <span class="emph">Dokument Modell</span> verwendet folgenden Standard Namensraum:

**`https://sub.uni-goettingen.de/met/standards/entity-xml#`**

Zusätzliche Namespaces, die in entityXML Schema verwendet werden, sind: 

- **bf**: [http://id.loc.gov/ontologies/bibframe/](http://id.loc.gov/ontologies/bibframe/)
- **dc**: [http://purl.org/dc/elements/1.1/](http://purl.org/dc/elements/1.1/)
- **dnb**: [https://www.dnb.de](https://www.dnb.de)
- **foaf**: [http://xmlns.com/foaf/0.1/](http://xmlns.com/foaf/0.1/)
- **geo**: [http://www.opengis.net/ont/geosparql#](http://www.opengis.net/ont/geosparql#)
- **gndo**: [https://d-nb.info/standards/elementset/gnd#](https://d-nb.info/standards/elementset/gnd#)
- **owl**: [http://www.w3.org/2002/07/owl#](http://www.w3.org/2002/07/owl#)
- **skos**: [http://www.w3.org/2004/02/skos/core#](http://www.w3.org/2004/02/skos/core#)
- **wgs84**: [http://www.w3.org/2003/01/geo/wgs84_pos#](http://www.w3.org/2003/01/geo/wgs84_pos#)


entityXML Dokumente bestehen aus einem `entityXML` Wurzelelement, das eine oder mehrere Sammlungen enthalten kann:

```xml
<entityXML xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:foaf="http://xmlns.com/foaf/0.1/" 
   xmlns:gndo="https://d-nb.info/standards/elementset/gnd#" 
   xmlns:owl="http://www.w3.org/2002/07/owl#"
   xmlns:skos="http://www.w3.org/2004/02/skos/core#" 
   xmlns:geo="http://www.opengis.net/ont/geosparql#" 
   xmlns:dnb="https://www.dnb.de"
   xmlns:wgs84="http://www.w3.org/2003/01/geo/wgs84_pos#">
   <collection>
      <metadata>
         <!-- Metadata Section -->
      </metadata>
      <data>
         <!-- Data Section -->
      </data>
   </collection>
</entityXML>
```

<a name="docs_d14e954"></a>
#### Datensammlung

Ein entityXML Datensammlung wird durch das Element `collection` repräsentiert, das sich aus einem Metadaten- (`metadata`) und einem Datenbereich (`data`) zusammensetzt. 

 Der Metadatenbereich stellt den Rahmen bereit, in dem Metadaten mit Blick auf den Erschließungskontext einer Datensammlung dokumentiert werden können. 

Im Datenbereich werden die eigentlichen Entitäteneinträge erschlossen bzw. dokumentiert.

<a name="metadata-section"></a>
### Metadaten einer Datensammlung

Der Metadatenbereich einer Datensammlung wird durch das Element `metadata` repräsentiert und stellt die Möglichkeit zur Verfügung, administrative und deskriptive Metadaten für eine Datensammlung zu dokumentieren.

In `metadata` können z.B. der Titel einer Datensammlung, Bearbeiter oder andere Verantwortlichkeiten, sowie Informationen hinsichtlich des Projekts bzw. des Kontexts, aus dem heraus die Entitäteneinträge der Datensammlung erschlossen wurden, angegeben werden.

Folgende Elemente können in diesem Kontext verwendet werden: 

- **Titel** [`title`](#d17e6862) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext
                    beschrieben wird."
- **Abstract (Metadaten)** [`abstract`](#d17e6334) (*verpflichtend*)  
  "Eine kurze
                Beschreibung."
- **Verantwortlichkeit (Metadaten)** [`respStmt`](#respStmt) (*optional und wiederholbar*)  
  "Angabe zur Rolle einer Person oder
                    Institution in Hinblick auf die Bearbeitung einer Datensammlung."
- **Datenprovider** [`provider`](#data-provider) (*verpflichtend*)  
  "Informationen zum Datenprovider der Datensammlung und der
                    in diesem Rahmen dokumentierten Entitäten."
- **GND-Agentur** [`agency`](#agency-stmt) (*optional*)  
  "Informationen zur GND-Agentur, die die Datensammlung
                    bearbeitet."
- **Revisionsbeschreibung** [`revision`](#revision) (*verpflichtend*)  
  "Statusinformationen zu einer Ressource
                    (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur
                    Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung
                    nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der
                Revisionsbeschreibung!"



```xml
<metadata>
   <title>[Resource Title]</title>
   <provider id="[Provider-ID]">
      <title>[Name des Lieferanten bzw. Erschließungskontexts der Datensammlung]</title>
      <respStmt id="[Person-ID]">
         <resp>[Rolle]</resp>
         <name>[Eigenname der Person]</name>
         <contact>
            <mail>[Email Adresse der Person]</mail>
         </contact>
      </respStmt>
      <abstract>[Kurzbeschreibung über den Lieferanten bzw. Erschließungskontext]</abstract>
   </provider>
   <respStmt id="[Person-ID]">
      <resp>[Rolle]</resp>
      <name>[Eigenname der Person]</name>
      <contact>
         <mail>[Email Adresse der Person]</mail>
      </contact>
   </respStmt>
   <abstract>[Kurzbeschreibung zur Datenlieferung]</abstract>
   <revision status="opened">[Dokumentation der Bearbeitungsstände der vorliegenden Ressource]</revision>
</metadata>
```

<a name="docs_d14e997"></a>
#### Titel einer Sammlung

Der Titel einer Sammlung wird via `title` dokumentiert. Hierbei handelt es sich für gewöhnlich um die Vorzugsbenennung einer Sammlung.

```xml
<title>Editionsprojekt XY: Entitäten</title>
```

<a name="docs_d14e1010"></a>
#### Kurzbeschreibung

Die Angabe von `abstract` ist für jede Datensammlung obligatorisch. `abstract` umfasst eine Kurzbeschreibung, die den Entstehungskontext der Datensammlung näher beschreibt.

```xml
<abstract>Sammlung von Entitäten, die im Kontext des digitalen Editionsprojekts XY erschlossen wurden.</abstract>
```

<a name="provider-stmt"></a>
#### Providerbeschreibung

Das Element `provider` umfasst deskriptive und administrative Metadaten zu dem Datenlieferanten bzw. dem Kontext, aus dem die in der entsprechenden entityXML Ressource dokumentierten Entitätseinträge heraus erschlossen wurden.

Die Providerbeschreibung ist **optional** aber empfohlen und kann Informationen wie *Name des Providers*, *personelle Verantwortlichkeiten* und eine *Kurzbeschreibung* enthalten: 

- **Provider-ID** [`@id`](#d17e6476) (*verpflichtend*)  
  "Der GND-Agentur Identifier des Datenproviders. Dieser
                        Identifier wird in der Regel direkt von der GND-Agentur, bei der der Datenlieferant registriert ist,
                    vergeben."
- **ISIL/BibliothekssiegelISIL/Acronym for libraries** [`@isil`](#d17e6925) (*optional*)  
  "Eindeutiger Identifikator der Organisation als
                    ISIL (International Standard Identifier for Libraries and Related Organisations).ISIL of an Organisation (International
                    Standard Identifier for Libraries and Related Organisations)."
- **Titel** [`title`](#d17e6862) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext
                    beschrieben wird."
- **Abstract (Metadaten)** [`abstract`](#d17e6334) (*verpflichtend*)  
  "Eine kurze
                Beschreibung."
- **Verantwortlichkeit (Metadaten)** [`respStmt`](#respStmt) (*optional und wiederholbar*)  
  "Angabe zur Rolle einer Person oder
                    Institution in Hinblick auf die Bearbeitung einer Datensammlung."



Folgendes Template verdeutlicht die Struktur des Providerstatments:

```xml
<provider id="[Projekt-ID]">
   <name>[Projekttitel]</name>
   <respStmt id="[Person-ID]">
     <resp>[Rolle]</resp>
     <name>[Eigenname der Person]</name>
     <contact>
         <mail>[Email Adresse der Person]</mail>
         <address>[Adresse der Person]</address>
         <phone>[Telefonnummer]</phone>
         <web>[URL einer Homepage]</web>
     </contact>
   </respStmt>
   <abstract>[Kurzbeschreibung des Providers bzw Erschließungskontexts]</abstract>
</provider>
```

<a name="resp-stmt"></a>
#### Beschreibung von Verantwortlichkeiten

Personen, die im Rahmen eines [Providerstatements](#provider-stmt) oder eine [Datensammlung](#data-collection) als Verantwortliche (z.B. "Bearbeiter", "Koordinator" etc.) dokumentiert werden sollen, können via `respStmt` dokumentiert werden. Folgende Elemente können in diesem Rahmen dokumentiert werden: 

- **ID** [`@id`](#d17e7042) (*verpflichtend*)  
  "Eine interne ID zur Identifikation des entsprechenden
                    Elements."
- **Rolle (Verantwortlichkeit)** [`resp`](#d17e6383) (*verpflichtend*)  
  "Eine Rolle, die der Person oder
                        Institution im Rahmen der Bearbeitung einer Datensammlung zugeschrieben wird."
- **Name (Verantworlichkeit)** [`name`](#d17e6400) (*verpflichtend*)  
  "Name der verantwortlichen Person oder
                        Institution."
- **Kontaktinformationen** [`contact`](#contact-metadata) (*optional*)  
  "Informationen, die zur Kontaktaufnahme genutzt
                    werden können."



Zusätzliche Informationen zur Kontaktaufnahme können via `contact` erfolgen. Erforderlich ist hier die Angabe einer Mailadresse (`mail`); alle weiteren Informationen sind optional: 

- **Mailadresse** [`mail`](#d17e2902) (*verpflichtend und wiederholbar*)  
  "Email Adresse zur
                            Kontaktaufnahme."
- **Telefon** [`phone`](#d17e2922) (*optional*)  
  "Telefonnummer zur
                            Kontaktaufnahme"
- **Adresse** [`address`](#d17e2942) (*optional*)  
  "Postalische Anschrift."
- **Homepage (Kontakt)** [`web`](#d17e2962) (*optional*)  
  "Ein URL zu einer offiziellen oder
                                persönlichen Homepage."



```xml
<respStmt id="MM">
   <resp>Bearbeiter</resp>
   <name>Mustermann, Max</name>
   <contact>
      <mail>muster@entitxml.de</mail>
      <address>Am Datenstrom 5, 1100101 Modulstadt</address>
      <phone>0551-1100101</phone>
      <web>www.entityxml-beispiele.de</web>
   </contact>
</respStmt>
```

<a name="lifecycle"></a>
### entityXML Lifecycle

Bevor wir weiter über die möglichen Metadaten einer entityXML Datensammlung reden, sollten wir einen Schritt zurück gehen und uns zunächst das <span class="emph">Lifecyclekonzept</span> von entityXML und das hiermit in Zusammenhang stehende Workflowmodell aus Sicht der Text+ GND-Agentur näher ansehen:

![markdown/docs/img/revision-workflow.png](docs/img/revision-workflow.png)

Der prototypische entityXML Workflow orientiert sich an drei grundlegenden Phasen, die sowohl von einer Datensammlung als auch von jedem Eintrag einzeln durchlaufen werden können. Im Folgenden spreche ich daher von <span class="emph">Informationsressource</span> und meine damit sowohl eine entityXML `collection` als auch die einzelnen definierten Typen von Eintitätseinträgen (näheres hierzu weiter unten):

**(1) Eröffnung und Bearbeitung durch den potenziellen Datenlieferanten**: Hierbei handelt es sich um die Anfangsphase einer Informationsressource, die damit als <span class="emph">eröffnet</span> ("opened") gilt: Ein Projekt, eine Forschergruppe oder einfach Irgendwer, der gerne Entitäten dokumentiert, legt in diesem Stadium eine entityXML Datei an und beginnt eine Sammlung mit Einträgen zu füllen. Diese Einträge – und damit auch die Sammlung – befindet sich somit in der ständigen Bearbeitung, d.h. Hintergründe und Informationen werden recherchiert und in den entsprechenden Einträgen dokumentiert. Im besten Falle werden die entityXML Daten, die so entstehen, von dem Projekt oder der Person schon in einem eigenen Informationssystem verarbeitet und individuell genutzt.

> **'Bedenke die Möglichkeiten'**  
> An dieser Stelle möchte ich erwähnen, dass der Workflow einzig und allein davon abhängt, was man mit den Daten am Ende machen möchte! Man kann entityXML auch benutzen, ohne überhaupt jemals einen Austausch mit der GND in Erwägung zu ziehen. Genausogut kann eine entityXML auch maschienell als <span class="emph">Nebenprodukt</span> erzeugt werden, sprich: Die Daten werden primär in einem ganz anderen System erschlossen und entityXML dient lediglich als Exportformat, das dann wiederum an eine GND-Agentur geht. Im Prinzip sind der Phantasie hier keine Grenzen gesetzt. Zumindest aus Sicht der Text+ GND-Agentur stellt sich der Workflow so dar, wie oben gezeigt!


**(2) Überprüfung und Bearbeitung durch die Text+ GND-Agentur**: In dieser zweiten Phase befindet sich eine Informationsressource auf dem Prüfstand ("staged") bei der Text+ GND-Agentur, sprich: Die Informationsressource wird daraufhin geprüft, ob die geltenden Qualitätskriterien erfüllt sind. Sollte diese Prüfung negativ ausfallen, wird die Agentur von Fall zu Fall entscheiden, (1) ob die Daten durch Transformationen mit angemessenem Aufwand entsprechend angepasst werden können oder, (2) ob eine erneute Bearbeitung durch den verantwortlichen Datenlieferanten erforderlich ist. Festgestellte Probleme in und Rückfragen zu den Daten werden in jedem Fall durch die Agentur direkt an den Daten dokumentiert und an den Datenlieferanten zurückgemeldet. In dem Moment, in dem die Daten die Qualitätskriterien erfüllen, tritt die Informationsressource in das dritte Stadium ein.

**(3) Abschluss der Bearbeitung durch die Text+ GND-Agentur**: Diese dritte und letzte Phase markiert das Ende der Bearbeitung einer Informationsressource, die nun die entsprechenden Qualitätskriterien erfüllt und somit abschließend durch die Text+ GND-Agentur in die GND eingespielt, d.h. veröffentlicht werden kann: GND-URIs für neue Einträge werden erstellt und an den entsprechenden Stellen in der entityXML Datei dokumentiert, mögliche Änderungen wurden an Normdateneinträgen vorgenommen etc. Als letzte Handlung wird der Datenlieferant über den Abschluss benachrichtigt, indem er die aktualisierte und nun als <span class="emph">abgeschlossen</span> ("closed") geltende entityXML Ressource, angereichert mit den entsprechenden Information, zurück erhält.

> Die zweite und dritte Phase ist strukturell vergleichbar mit einem Prozesses, den man in der Informationsverarbeitung auch als [ETL](https://de.wikipedia.org/wiki/ETL-Prozess) ("Extract, Transform, Load") kennt: In ETL Prozessen werden heterogene Quelldaten zuerst extrahiert und für anschließende Transformationsschritte aufbereitet. In der Folge werden die Daten durch unterschiedliche Transformationen in ein definiertes Zielformat konvertiert, dass dann letztlich in ein definiertes Zielsystem eingespielt ("geladen") wird. 


Innerhalb dieser drei Phasen kann eine Sammlung bzw. ein Eintrag acht unterschiedliche <span class="emph">Bearbeitungsstadien</span> durchlaufen. Diese lassen sich als einzelne Entwicklungsschritte einer Ressource in ihrem Lebenszyklus beschreiben – von dem Moment der Erstellung an bis zur Publikation:

![markdown/docs/img/revision-steps.png](docs/img/revision-steps.png)

Eine Erklärung zu den hier abgebildeten acht Stadien bleibe ich noch für einen kurzen Moment schuldig; sie folgt in wenigen Absätzen. Bis hierher ging es vorallem darum, einen Eindruck davon zu gewinnen, wie der Lebenszyklus einer entityXML Ressource aufgebaut ist (bzw. sein kann) und wie sich die Text+ GND-Agentur hierauf aufbauend einen Verarbeitungsworkflow definiert hat.

<a name="revision-desc"></a>
#### Revisionsbeschreibung

Bleibt die Frage, wie der geschilderte Lebenszyklus in den Daten dokumentiert wird? Dafür dient die <span class="emph">Revisionsbeschreibung</span>, die durch das Element `revision` repräsentiert wird.

Revisionsbeschreibungen sind für Sammlungen (`collection`) als auch für Entitätseinträge (`entity`, `person`, `place` usw.) implementiert. Für Sammlungen ist `revision` obligatorisch! Für Einträge ist `revision` solange optional, bis ein Eintrag explizit für die Bearbeitung durch eine GND-Agentur vorgesehen wird (durch die Verwendung von `@agency` angezeigt, aber mehr dazu [später](#agency-mode-record)). Als <span class="emph">Best Practice</span> gilt jedenfalls, dass jeder Eintrag von Anfang an eine konsistente Revisionsbeschreibung nachweisen kann. 

Eine Revisionsbeschreibung besteht im Kern aus zwei Kerninformationen: Der momentanen <span class="emph">Bearbeitungsphase</span> (siehe oben), in dem sich die Sammlung oder der Entitätseintrag befindet, sowie mindestens einer, aber in der Regel mehrere <span class="emph">Änderungseinträge</span>, die einzelne, vorgenommene Änderungen oder Arbeitsschritte dokumentieren.

Die Bearbeitungsphase, also ob sich die Sammlung oder der Eintrag in der anfänglichen Bearbeitungsphase ("**opened**") oder schon auf dem Prüfstand einer GND-Agentur ("**staged**") befindet oder bereits fertig bearbeitet und abgeschlossen ("**closed**") ist, wird in `@status` angegeben.

Änderungseinträge werden durch `change` Elemente innerhalb von `revision` angelegt und dienen der knappen und präzisen textlichen Dokumentation von Änderungen, Erweiterungen oder anderer Arbeiten, die an einer Sammlung oder einem Eintrag vorgenommen wurden. Zusätzlich verzeichnet ein Änderungseintrag das Datum, an dem er angelegt wurde via `@when` als ISO 8601 sowie die ID der verantworlich zeichnenden Person via `@who`:

```xml
<revision status="opened">
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen.
   </change>
</revision>
```

> **Who?**  
> Die verantwortlich zeichnende Person muss selbstverständlich vorher bei den bearbeitenden Personen in der [Providerbeschreibung](#provider-stmt) definiert werden.


**Achtung**: Änderungseinträge werden immer von oben nach unten sortiert, ausgehend von dem jüngsten Eintrag, sprich: Der Eintrag mit dem aktuellsten Datum befindet sich immer an erster Stelle in der Revisionsbeschreibung!

Ein Änderungseintrag hat wie schon das übergeordnete `revision` Element ein `@status` Attribut. Hier kommen nun die oben bereits angesprochenen <span class="emph">acht Bearbeitungsstadien</span> ins Spiel, die auf diese Weise in der Revisionsbeschreibung angegeben werden <span class="emph">können</span>. "Können" heißt: Nicht jeder Änderungseintrag muss ein Bearbeitungsstadium ausweisen, aber wenn z. B. besondere Meilsensteine anstehen (etwa der Tag, an dem die entityXML an die Text+ GND-Agentur geschickt wird), sollte zumindest der aktuellste Eintrag ein explizites Bearbeitungsstadium dokumentieren:

```xml
<revision status="opened">
   <change when="2023-01-05" who="MM" status="candidate">
      Bearbeitung abgeschlossen und an die Text+ GND-Agentur geschickt.
   </change>
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen
   </change>
</revision>
```

Und hier nun die versprochene Erklärung zu den einzelnen Stadien:



- **draft**: (Draft, *default*) Die Informationsressource befindet sich in
                                der Bearbeitung.
- **candidate**: (Kandidat) Die Bearbeitung ist soweit abgeschlossen. Die
                                Informationsressource gilt nun als potenzieller Kandidat zur Übertragung an die GND und ist somit bereit zur Prüfung
                                durch eine GND-Agentur.
- **embargoed**: (Gesperrt) Die Informationsressource ist derzeit gesperrt. Sie
                                erfüllt entweder nicht die erforderlichen Qualitätskriterien oder ist aus einem anderen Grund, der in dem
                                Änderungseintrag vermerkt ist, gesperrt. Sie muss entsprechend überarbeitet werden.
- **withdrawn**: (Zurückgezogen) Die Informationsressource wurde zurückgezogen.
                                Sie wird in Hinblick auf eine Übertragung an die GND ignoriert.
- **cleared**: (Überarbeitet) Die Informationsressource wurde in Hinblick auf
                                die geltenden Qualitätskriterien überarbeitet und gilt nun als überarbeiteter Kandidat, der erneut geprüft
                                wird.
- **approved**: (Angenommen) Die Prüfung der Informationsressource ist
                                abgeschlossen. Sie erfüllt alle erforderlichen Qualitätsstandards und ist bereit zur Übertragung an die GND. Dieser
                                Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
- **submitted**: (An GND geliefert) Die Daten der Informationsressource wurden
                                an die GND zum Übertragen geliefert. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur
                                vorbehalten.
- **published**: (Veröffentlicht) Die Daten der Informationsressource wurden in
                                der GND veröffentlicht. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur
                            vorbehalten.



Wenn also kein Stadium explizit gemacht wird, gilt eine Sammlung oder ein Eintrag als "**draft**". Die folgende Abbildung verdeutlicht den Lebenszyklus nochmal anhand der acht Stadien in einer auf das Wesentliche reduzierten Form:

![markdown/docs/img/revision-steps.condensed.png](docs/img/revision-steps.condensed.png)

<a name="docs_d14e1249"></a>
##### Anmerkung zur Ausführlichkeit von Revisionsbeschreibungen

Wie immer bei textlicher Dokumentation stellt sich auch bei der Revisionsbeschreibung die Frage, was auf welche weise und vorallem wie ausführlich dokumentiert werden soll? Nun, sowohl der Inhalt als auch die Ausführlichkeit der Beschreibung liegt in der Verantwortung der Bearbeiter\*innen der jeweiligen entityXML Ressource, denn diese wissen wahrscheinlich am Besten, was hilfreiche Informationen sind, die mit Blick auf die Arbeit an ihren Daten festgehalten werden sollten.

Dennoch empfielt es sich, in diesem Bereich nicht nur für sich, sondern auch *für Dritte nachvollziehbar zu dokumentieren*. Die Revisionsbeschreibung dient zwar in erster Linie dazu, Bearbeitungsstände zu dokumentieren, kann und <span class="emph">soll</span> aber auch zur Unterstützung der Kommunikation mit einer GND-Agentur beitragen. Dementsprechend bietet es sich an, an dieser Stelle hilfreiche Informationen nicht nur für die primären Bearbeiter\*innen sondern auch für Agenturmitarbeiter\*innen zu hinterlegen, die in Hinblick auf den Umgang mit der entsprechenden Sammlung oder dem entsprechenden Eintrag von Relevanz sind bzw. sein können. Dazu gehören z.B. Erwartungen oder auch Wünsche, die an die Agentur gestellt werden, genauso wie hilfreiche Hinweise, die die Arbeit erleichtern können:

```xml
<!-- Beispiel einer Revisionsbeschreibung eines Eintrags, der angelegt werden soll -->
<revision status="opened">
   <change when="2022-12-09" who="MFZ" status="candidate">Nicht in GND enthalten. Bitte eintragen.</change>
</revision>
```

```xml
<revision status="opened">
   <change when="2023-01-05" who="MM" status="candidate">
      Bearbeitung abgeschlossen und an die Text+ GND-Agentur geschickt. 
      Die Sammlung besteht aus drei Listen: (1) Personen, (2) Orte, (3) Körperschaften. 
      Bei den Körperschaften bestehen zu einzelnen Einträgen noch
      wichtige Fragen an die GND-Agentur. Diese sind in den 
      entsprechenden Revisionsbeschreibungen hinterlegt. Die Einträge
      sind als "embargoed" gekennzeichnet.
   </change>
   <change when="2022-12-19" who="MM">
      Sammlung angelegt und erste Liste mit Personen begonnen
   </change>
</revision>
```

Das letzte Beispiel zeigt, dass auch Hinweise bzw. Anmerkungen zu einer Sammlung oder einem Eintrag durchaus hilfreich sein können. In gleicher Weise wird auch die Agentur an dieser Stelle mögliche Rückmeldungen oder Rückfragen dokumentieren, so wie z.B. für den Eintrag mit der ID `ulrich_renner` (GND: [https://d-nb.info/gnd/141596643](https://d-nb.info/gnd/141596643)) in der Beispieldatei [KMG-GND.20230203.0.5.1.xml](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/samples/KMG-GND.20230203.0.5.1.xml):

```xml
<revision status="staged">
   <change when="2023-01-19" who="US" status="approved">
      Die Daten hinsichtlich der verknüpften Publikationen stammen nicht aus 
      dem GND Eintrag (https://d-nb.info/gnd/141596643) sondern aus dem DNB Katalog: 
      Die Katalogseinträge zu den Ausgaben sind mit dem falschen GND Eintrag 
      verknüpft. Die Anfrage wird an die DNB zur Änderung im Katalog weitergeleitet.
   </change>
   <change when="2022-12-08" who="MFZ" status="candidate">Fehlerhafter Eintrag; nicht-zugehörige Literatur gefunden</change>
</revision>
```

<a name="data-collection"></a>
### Datenbereich

Der Datenbereich einer `entityXML` Ressource wird durch das Element`data` repräsentiert, das eine oder mehrere Datenlisten (`list`) enthalten kann.

Jede Liste kann durch einen Titel (`title`) und ein Abstract (`abstract`) näher beschrieben werden:

```xml
<data>
   <list>
      <title><!-- Titel der Liste --></title>
      <abstract><!-- Kurzbeschreibung der Liste --></abstract>
      <!-- Einer oder mehrere Entitäteneinträge folgen hier -->
   </list>
</data>
```

Jede Liste bildet schließlich den Kontext für die zu dokumentierenden Entitätseinträge.

<a name="docs_d14e1313"></a>
### Entitätseintrag im Allgemeinen

<a name="records-intro"></a>
#### Einstieg ins Anlegen von Entitätseinträgen

Ein Entitätseintrag bzw. ein Record repräsentiert die Beschreibung einer konkreten Entität einer bestimmten Klasse. entityXML unterstützt derzeit folgende Entitätsklassen:


- `entity` Allgemeine Entität (eine Art Oberklasse aller anderen Entitätstypen)
- `person` Person
- `corporateBody` Körperschaft
- `place` Ort
- `work` Werk

Es gibt noch zwei weitere Recordklassen, die bisher pro forma im Schema enthalten sind und noch nicht wirklich ausspezifiziert sind, weil hierfür noch keine Anwendungsfälle bestehen:


- `expression` Ausdrucksform eines Werks (Expression)
- `manifestation` Manifestation einers Werks (Manifestation)

entityXML ist in erster Linie ein <span class="emph">Markup Format</span>! Das bedeuted, dass Wissen über eine Eintität zunächst ersteinmal in Form von (menschenlesbarem) Fließtext repräsentiert werden kann. Bei dieser Form der Repräsentation von Daten als Text spricht man auch gerne von <span class="emph">unstrukturierten Daten</span>:

```xml
<entity xml:id="erika_muster">
   In meiner neuen Geldbörse habe ich ein Papierkärtchen entdeckt, dass 
   eine "Mustermann, Erika" verzeichnet. Auf Wikipedia fand ich heraus, 
   dass es sich um eine fiktive Person handelt, die als Platzhalter
   für eine beliebige (reale) Frau steht.
</entity>
```

Dieser Fließtext wird dann in der Folge durch entsprechendes Markup angereichert, um das im Text enthaltene Wissen über eine Entität explizit und <span class="emph">maschienenlesbar</span> (d.h. die Form der Daten ist so angelegt, dass sie einen direkten systematischen Zugriff auf die Informationsinhalte durch eine Maschiene erlaubt) zu machen. Diese durch Markup angereicherte Form von (textlichen) Daten bezeichnet man auch als <span class="emph">semistrukturierte Daten</span>: 

```xml
<entity xml:id="erika_muster">
   In meiner neuen Geldbörse habe ich ein Papierkärtchen entdeckt, dass 
   eine "<dc:title>Mustermann, Erika</dc:title>" verzeichnet. 
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref> 
   schreibt, <note>es handle sich um eine fiktive Person, die als Platzhalter
   für eine beliebige (reale) Frau steht</note>.
</entity>
```

Dementsprechen kann man Entitätsbeschreibungen bzw. Entitätseinträge auch generell in einer eher strukturierten Form anlegen, die weniger "textlich" gehalten ist: 

```xml
<entity xml:id="max_muster">
   <skos:note>Eine fiktive Person, die als Platzhalter fungiert</skos:note>
   <source>Eine kleine Karte in meiner neuen Geldbörse</source>
   <dc:title>Mustermann, Max</dc:title>
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref>
</entity>
```

> **Records in entityXML sind also strukturierte Daten?**  
> Nein, es handelt sich nachwievor um semistrukturierte Daten! Die Informationsinhalte sind zwar strukturierter <span class="emph">dargestellt</span>, aber es ist (und bleibt) Text mit Markup. Nichtsdestoweniger folgen entityXML Daten durch die Anbindung an ein Schema einer definierten Struktur und können somit als "strukturierter" gelten, als andere XML Daten, die nicht nach einem spezifizierten Schema erschlossen wurden.


Auch wenn die in entityXML spezifizierten Entitätsklassen je unterschiedliche Informationen enthalten können, teilen sie sich neben der <span class="emph">[Revisionsbeschreibung](#revision-desc)</span>, die bereits eingehend erläutert wurde, folgende Elemente und Attribute: 

- **Record ID** [`@xml:id`](#d17e7648) (*verpflichtend*)  
  "Angabe eines Identifiers zur Identifikation eines
                    Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!"
- **GND-URI** [`@gndo:uri`](#d17e6984) (*optional*)  
  "Angabe des GND-HTTP URIs der beschriebenen Entität in der GND,
                    sofern vorhanden."
- **Agenturanfrage** [`@agency`](#attr.record.agency) (*optional*)  
  "Anfrage an die Agentur, welche Aktion in Hinblick auf den
                    Eintrag durchgeführt werden soll."
- **Anreicherung** [`@enrich`](#d17e7628) (*optional*)  
  "Der Record soll mit Daten eines externen Datendienstes
                    angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt,
                    um den Record anzureichern!"



- **Titel (Record)** [`dc:title`](#d17e3011) (*optional*)  
  "Ein Titel für die Ressource. `dc:title` wird dann zur
                    Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll."
- **Biographische Angaben** [`gndo:biographicalOrHistoricalInformation`](#d17e3463) (*optional und wiederholbar*)  
  "Biographische oder historische Angaben über die
                    Entität."
- **GND-Identifier einer Dublette** [`dublicateGndIdentifier`](#dublicates) (*optional und wiederholbar*)  
  "Der GND-Identifier einer möglichen
                    Dublette."
- **Geographischer Schwerpunkt** [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode) (*optional und wiederholbar*)  
  "Ländercode(s) der Staat(en), denen die Entität
                    zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und
                    eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular
                    (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!"
- **GND-Identifier** [`gndo:gndIdentifier`](#d17e4693) (*optional und wiederholbar*)  
  "Der GND-Identifier eines in der GND eingetragenen
                    Normdatensatzes sofern vorhanden"
- **GND-Sachgruppe** [`gndo:gndSubjectCategory`](#d17e4734) (*optional und wiederholbar*)  
  "Term aus dem GND-Sachgruppen Vokabular (ehemals
                    SWD)"
- **Bild** [`img`](#elem.img) (*optional und wiederholbar*)  
  "Ein Bild bzw. ein URL zu einer
                Bildressource."
- **Sprachraum** [`gndo:languageCode`](#d17e4843) (*optional und wiederholbar*)  
  "Code(s) des Sprachraumes, dem die Entität zugeordnet werden
                    kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein
                    Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben
                werden!"
- **Beziehung** [`gndo:relatesTo`](#d17e5757) (*optional und wiederholbar*)  
  "Beziehung der beschriebenen Entität zu einer anderen Entität,
                    entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der
                    GNDO!"
- **Hyperlink** [`ref`](#elem.ref) (*optional und wiederholbar*)  
  "Ein Link zu einer Webressource, die weitere Informationen über
                    die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target`
                    dokumentiert."
- **Gleiche Entität** [`owl:sameAs`](#elem.owl.sameAs) (*optional und wiederholbar*)  
  "HTTP-URI der selben Entität in einem anderen
                    Normdatendienst."
- **Anmerkung** [`skos:note`](#elem.skos.note) (*optional und wiederholbar*)  
  "Eine Anmerkung zum Eintrag."
- **Quellenangabe** [`source`](#elem.source) (*optional und wiederholbar*)  
  "Angaben zu einer Quelle, die verwendet wurde, um die in
                    diesem Eintrag gegebenen Informationen zu recherchieren."
- **ANY ELEMENT (eng gefasst)** [`anyElement.narrow`](#any-restricted) (*optional und wiederholbar*)  
  "Ein Element, dessen **QName** einem *custom
                    namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist."



<a name="custom-namespaces"></a>
#### Verwendung fremder Namensräume

Bevor wir das allgemeine Element- und Attributsset eines Entitätseintrags in entityXML durchgehen, widmen wir uns kurz der Frage "Was, wenn ich Informationen dokumentieren möchte, die nicht im Schema spezifiziert sind?".

entityXML verhält sich als Format in Hinblick auf die Auszeichnung von Informationen innerhalb aller Entitätseinträge (`entity` und alle seine Unterklassen wie z.B. `person` oder `place`) <span class="emph">offen</span>: Die Spezifikation macht zwar gewisse Vorgaben in Hinblick auf Elemente aus der GNDO und anderen Formaten, sowie diesbezügliche Validierungsregeln; nichtdestoweniger erlaubt es ausdrücklich Elemente und Attribute aus anderen Namensräumen (sog. *custom elements* bzw. *attributes*), [die nicht im
                        Rahmen der entityXML Spezifikation definiert sind](#entityxml-dm-intro) und somit als <span class="emph">spezifikationsfremde Namensräume</span> gelten.

Dementsprechend sind folgende Elemente von der Verwendung als Custom Elements innerhalb von Entitätseinträgen ausgeschlossen, da sie fester Bestandteil der entityXML Spezifikation sind:

**Elemente:**



- http://id.loc.gov/ontologies/bibframe/**instanceOf**
- http://xmlns.com/foaf/0.1/**page**
- http://purl.org/dc/elements/1.1/**title**
- http://www.w3.org/2004/02/skos/core#**note**
- http://www.w3.org/2002/07/owl#**sameAs**
- http://www.opengis.net/ont/geosparql#**hasGeometry**
- http://www.w3.org/2003/01/geo/wgs84_pos#**lat**
- http://www.w3.org/2003/01/geo/wgs84_pos#**long**

**Generell alle Elemente aus folgenden Namensräumen:**



- https://sub.uni-goettingen.de/met/standards/entity-xml#
- https://sub.uni-goettingen.de/met/standards/entity-store#
- https://d-nb.info/standards/elementset/gnd#

<a name="docs_d14e1451"></a>
##### Von der einfachen Anwendung ...

Die Implementierung von entityXML ist so angelegt, dass es als Speicherformat von (Forschungs- )projekten verwendet werden kann, um Entitätsbeschreibungen als Forschungsdaten so frei wie möglich und so restriktiv wie nötig (aus Perspektive der späteren Datenverarbeitung durch eine GND Agentur) zu dokumentieren und zu nutzen.

Um *custom elements* oder *attributes* in einem entityXML Entitätseintrag zu verwenden, muss zunächst ein entsprechender beliebiger Namensraum definiert werden, der nicht durch die vorliegende Spezifikation ausgeschlossen wird (siehe oben). Für gewöhnlich bietet sich der Wurzelknoten `entityXML` dafür an:

```xml
<entityXML 
   xmlns="https://sub.uni-goettingen.de/met/standards/entity-xml#" 
   xmlns:example="http://example.com/irgendein-custom-namespace">
   <!-- ... -->
</entityXML>
```

Nichtsdestoweniger kann der bzw. die Namespaces auch **direkt am jeweiligen Entitätselement** definiert werden:

```xml
<entity xml:id="max_muster" xmlns:example="http://example.com/mein-namespace">
   <skos:note>Eine fiktive Person, die wahrescheinlich mal einen Finger verloren hat.</skos:note>
   <source>Eine kleine Karte in meiner neuen Geldbörse</source>
   <dc:title>Mustermann, Max</dc:title>
   <ref target="https://de.wikipedia.org/wiki/Mustermann">Wikipedia</ref>
   <example:finger-anzahl>9</example:finger-anzahl>
</entity>
```

So ist es möglich, für jede Entität jegliche Art von Datum zu dokumentieren - je nach den eigenen Anforderungen, die an die Daten gestellt werden.

<a name="docs_d14e1484"></a>
##### ... hin zum Mittelweg ...

In der Regel dienen *custom elements* dazu, zusätzliche Daten in Einträgen zu dokumentieren, **die in der GND als Normdatei keinen Platz finden** und dementsprechen auch nicht über die GNDO beschrieben werden können.

Diese Verwendungsweise von *custom elements* als "Erweiterungen" der GNDO Beschreibung um (projektspezifische) zusätzliche Daten verstehen wir als "Goldenen Weg" bzw. den optimalen Mittelweg:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363" 
   xmlns:social="http://social-media.statistics.de/kennzahlen">
   <gndo:preferredName>
     <gndo:surname>Nguyen-Kim</gndo:surname>, 
     <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <social:twitter when="2023-01-11" 
      follower="450602" 
      joined="2013-01-01">maithi_nk</social:twitter>
   <social:youtube when="2023-01-11" 
      follower="1480000" 
      joined="2016-09-08">maiLab</social:youtube>
</person>
```

Dieses Beispiel erweitert einen Personeneintrag um Angaben zu Social-Media Accounts und diesbezügliche Kennzahlen wie Anzahl der Follower, Beitrittsdatum und Account-ID. Diese Daten können dann z.B. als Grundlage eigener statistischer Analysen dienen.

<a name="lido"></a>
##### ... und auf die Spitze getrieben!

Im Prinzip kann man dieses Konzept auf die Spitze treiben und z.B. einen kompletten <span class="emph">LIDO</span> Datensatz in einem `entity` Eintrag (und nur hier) unterbringen:

```xml
<entity xml:id="lido00099" gndo:type="https://d-nb.info/standards/elementset/gnd#Work">
     <lido:lido xmlns="http://www.lido-schema.org"
         xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:skos="http://www.w3.org/2004/02/skos/core#"
         xmlns:lido="http://www.lido-schema.org">
         <lido:lidoRecID lido:source="Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg" lido:type="http://terminology.lido-schema.org/lido00100">DE-Mb112/lido-obj00154983</lido:lidoRecID>
         <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099" lido:pref="http://terminology.lido-schema.org/lido00170">https://www.uffizi.it/opere/botticelli-primavera</lido:objectPublishedID>
         <lido:objectPublishedID lido:type="http://terminology.lido-schema.org/lido00099" lido:pref="http://terminology.lido-schema.org/lido00170">https://d-nb.info/gnd/4069615-7</lido:objectPublishedID>
         <lido:category>
             <skos:Concept rdf:about="http://terminology.lido-schema.org/lido00096">
                 <skos:prefLabel xml:lang="en">Human-made object</skos:prefLabel>
                 <skos:exactMatch>http://vocab.getty.edu/aat/300386957</skos:exactMatch>
                 <skos:exactMatch>http://erlangen-crm.org/current/E22_Human-Made_Object</skos:exactMatch>
             </skos:Concept>
             <lido:term lido:addedSearchTerm="yes">man-made object</lido:term>
         </lido:category>
         <!-- ... -->
     </lido:lido>
 </entity>
```

Das wird durch die besondere Implementierung des `entity` Elements ermöglicht, das zwei mögliche Optionen der Beschreibung von Entitäten zulässt: (1) Die Beschreibung einer Entität gemäß des im entityXML Schema spezifizierten Standardsets von Elementen und Attributen oder (2) die Beschreibung einer Entität durch ein beliebiges Set an Elementen und Attributen, die nicht den Namensräumen <https://sub.uni-goettingen.de/met/standards/entity-xml#>, <https://sub.uni-goettingen.de/met/standards/entity-store#> und <https://d-nb.info/standards/elementset/gnd#> angehören.

> **Warum ist bei Option 2 der GNDO Namespace ausgeschlossen?**  
> Berechtigte Frage! Weil für Daten, die mithilfe der GNDO Terme beschrieben werden, die in diesem Schema implementierten Entitätsklassen gedacht sind. Für diese ist spezifisch geregelt, welche Daten mit welchen GNDO-Beschreibungskategorien erschlossen werden können bzw. sollen. Von diesem Standpunkt aus betrachtet, ergibt es keinen Sinn, dass in `entity` andere GNDO Elemente verwendet werden (können), als die, die bereits im Schema spezifiziert werden. Wenn die Beschreibung einer Entität also gemäß der GNDO erfolgen soll, dann kann man die entsprechenden Eintragselemente verwenden und sollte nicht auf `entity` ausweichen.


Im abgebildeten Beipiel wird also der extremste Weg eingeschlagen, der in entityXML möglich ist: Die **vollständige Integration eines fremden XML-Formats** (sofern es nicht den Namespace der GND oder die beiden entityXML Namespaces integriert).

`@gndo:type` dient der Identifikation des Entitätstyps, der durch das fremde Format beschrieben wird. In unserem Beispiel handelt es sich um ein Gemälde, was sich grob als `gndo:Work` fassen lässt.

<a name="docs_d14e1539"></a>
##### Arbeiten mit schemafremden Elementen im Author Mode

oXygen XML arbeitet im Author Mode mit einem hilfreichen Werkzeug, dass sich ["Content Completion Assistant"](https://www.oxygenxml.com/doc/versions/25.0/ug-editor/topics/content-completion-author-mode.html#content-completion-author-mode) nennt. Hierbei handelt sich um ein Eingabeinterface für Elemente, das in seiner Standardeinstellung nur die Eingabe von schema-bekannten Elementen erlaubt.

Um die Eingabe schemafremder Elemente zu erlauben geht man in die Einstellungen `Editor > Bearbeitungsmodus > Autor > Schemabewusst` und entfernt den Haken bei "Nur das Einfügen gültiger Elemente und Attribute erlauben".

<a name="docs_d14e1552"></a>
##### Verfahrensweise mit Daten aus fremden Namensräumen bei der Text+ GND-Agentur

Daten, die mit Hilfe von *custom elements* erschlossen werden, werden in Hinblick auf die Anlage neuer Einträge bzw. die Anreicherung bereits bestehender Einträge in der GND ignoriert. Bei einer Verarbeitung von entityXML Ressourcen durch die Text+ GND Agentur werden ausschließlich die Elemente betrachtet, die fester Bestandteil der Spezifikation sind. 

Nichtsdestoweniger bleiben die Daten in *custom elements* in den Einträgen erhalten. Sie sind weiterhin Bestandteile des projektinternen Forschungsdatenlebenszyklus, können in diesem Rahmen versioniert und in andere Informationsprozesse eingebracht werden.

<a name="agency-mode-record"></a>
#### Verarbeitungshinweis für die Agentur

Als Austauschformat sollte entityXML vorallem dem <span class="emph">Datenaustausch</span> (genauer: den Prozesse des Austauschens von Daten) zwischen Datengebern (z.B. Projekte, die für ihre eigenen Zwecke Entitäten beschreiben) auf der einen und einer GND-Agentur auf der anderen Seite eine definierte Struktur geben - so die Idee. Zu dieser Struktur gehören auch Informationen darüber, was mit einem Eintrag aus Sicht der Datengeber eigentlich geschehen soll: Handelt es sich bei einem Eintrag um eine Entität, die noch nicht in der GND enthalten ist und daher angelegt werden soll? Oder sollen die in einem Eintrag enthaltenen Information einem bereits bestehenden Normdatensatz hinzugefügt werden?

Der **Verarbeitungshinweis** soll diese Fragen addressierbar machen: Für jeden Eintrag lässt sich mit Hilfe von ``@agency`` ein Hinweis vergeben, wie der Eintrag von einer GND Agentur verarbeitet werden soll. Diesbezüglich stehen folgende Deskriptoren zur Verfügung:


- *create*: Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
- *update*: Ein bestehender GND Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, soll durch neue Informationen aus diesem Eintrag angereichert werden.
- *merge*: Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem GND Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
- *ignore*: Der Eintrag wird von der Agentur nicht bearbeitet.

```xml
<place xml:id="lummerland" agency="create">
   <gndo:preferredName>Lummerland</gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation>
      Eine fiktive Insel aus den Geschichten von Michael Ende.
   </gndo:biographicalOrHistoricalInformation>
   <!-- ... -->
</place>
```


> **Experimentelles Feature**  
> Der Verarbeitungshinweis ist zwar ein essentieller Bestandteil, befindet sich momentan aber noch in der Erprobung. Wie produktiv sich hiermit die Bearbeitung von Einträgen strukturieren lässt, wird sich erst noch in der Praxis zeigen müssen.



<a name="entity-notes"></a>
#### Anmerkungen

Für jeden Eintrag können eine oder mehrere optionale, **allgemeine Anmerkungen** mittels ``skos:note`` dokumentiert werden, die zusätzliche Informationen oder Hinweise zu der beschriebenen Entität enthalten. Die Anmerkung sollte dabei in einer Form gehalten sein, die angebracht für eine spätere Veröffentlichung ist.

Die Sprache, in der eine Anmerkung verfasst ist, lässt sich via `@xml:lang` explizit spezifizieren. Wenn keine Sprache explizit angegeben wird, gilt **Deutsch** als default.

Zusätzlich kann über `@gndo:type` angegeben werden, ob es sich um eine im späteren Normdatensatz sichtbare, d.h. öffentliche Anmerkung oder um eine interne Anmerkung handelt, die nicht in den Normdateneintrag übernommen wird.

```xml
<entity xml:id="max_muster">
   <dc:title>Mustermann, Max</dc:title>
   <skos:note>Eine fiktive Person, die als Platzhalter fungiert</skos:note>
   <skos:note gndo:type="internal">Dieser Eintrag hat als Beispiel 
      mittlerweise so einen "langen Bart". Ich denke er hat ausgedient.<skos:note>
</entity>
```

**Zusätzliche Ressourcen**:


- Das MARC 21 Feld 680 (Scope Note) im "[Cataloger's
                        Reference Shelf MARC 21 Format for Classification Data](https://www.itsmarc.com/crs/mergedprojects/conciscl/conciscl/idh_680_clas.htm)"
- Das Feld 680 (Benutzerhinweis) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/680.pdf)

<a name="entity-biogr-histogr"></a>
#### Biographische und/oder historische Angaben zur Entität

Um ergänzende biographische, historische oder andere Angaben zu einer Entität in textlicher Form zu dokumentieren, dient das Element `gndo:biographicalOrHistoricalInformation`. Es ist *optional* und *wiederholbar*, sprich: Für eine Entität können beliebig viele dieser Angaben erstellt werden.

Zusätzliche lässt sich die verwendete Sprache der Angabe via ``@xml:lang`` eindeutig spezifizieren. Als default (sprich, falls kein `@xml:lang` gesetzt ist) gilt die Sprache der Angabe als **Deutsch**.

*Eine Anmerkung zur Form der Angabe*: Der Erfassungsleitfaden zum äquivalenten PICA3/MARC 21 Feld **[678](https://wiki.dnb.de/download/attachments/50759357/678.pdf)** (Biografische, historische und andere Angaben) empfielt einen "möglichst prägnant[en] und kurz[en]" Text, wobei die verwendete Sprache auf Deutsch festgelegt ist.

```xml
<place xml:id="goslar" gndo:uri="http://d-nb.info/gnd/4021643-3">
   <gndo:preferredName>Goslar</gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation>Kreisstadt des Landkreises 
      Goslar, Niedersachsen</gndo:biographicalOrHistoricalInformation>
   <gndo:biographicalOrHistoricalInformation xml:lang="en">Historic town in 
      Lower Saxony, Germany and the administrative centre 
      of the district of Goslar.</gndo:biographicalOrHistoricalInformation>
</place>
```

<a name="dublicates-encoding"></a>
#### Dokumentation von Mehrfacheinträgen

Als entscheidendes Kriterium für einen Normdatendienst gilt die ein-eindeutige Identifizierbarkeit von Entitäten durch Identifier. Datendienste wie die GND arbeiten daher mit hohem Ressourceneinsatz daran, ihre Daten frei von Dubletten zu halten. Nichtsdestoweniger kann es vorkommen, dass im Rahmen der GND Mehrfacheinträge zu einer Entität vorhanden sind, die bisher noch nicht dedubliziert wurden.

entityXML ermöglicht die Dokumentation solcher **Mehrfacheinträgen** via `dublicateGndIdentifier`, hier am Beispiel eines Personeneintrags:

```xml
<person xml:id="edwald_albee" gndo:uri="https://d-nb.info/gnd/118501380">
   <dc:title>Albee, Edwald</dc:title>
   <dublicateGndIdentifier>185806961</dublicateGndIdentifier>
</person>
```

Der Normdatensatz, dessen HTTP-URI in `@gndo:uri` erhoben wird, gilt immer als **Haupteintrag** und die über `dubcliateGndIdentifier` erschlossenen GND-IDs beschreiben *dublikate Einträge*, die mit dem Haupteintrag zusammengeführt werden sollten. 

Um eine gewünschte Zusammenführung für den Eintrag anzugeben, wird der entsprechende Eintrag via `@agency` als `merge` gekennzeichnet:

```xml
<person xml:id="edwald_albee" gndo:uri="https://d-nb.info/gnd/118501380" agency="merge">
   <dc:title>Albee, Edwald</dc:title>
   <dublicateGndIdentifier>185806961</dublicateGndIdentifier>
</person>
```

In der Regel werden Dubletten, wenn sie im Rahmen der Dedublizierung erkannt wurden, innerhalb der GND zusammengeführt und auf einen entsprechenden Haupteintrag umgeleitet. Es kann dennoch vorkommen, dass die URL-Weiterleitung für einzelne Einträge nicht vorgenommen wurde und der entsprechende Eintrag einen Hinweis: "Status: Datensatz ist nicht mehr Bestandteil der Gemeinsamen Normdatei (GND)" verzeichnet.

<a name="entity-geographic-area"></a>
#### Geographischer Schwerpunkt einer Entität

Entitäten lassen sich geographisch einordnen bzw. bestimmen: Bei Personen kann man örtlich gebundene Lebensmittelpunkte oder Wirkungsorte beschreiben. Körperschaften und Bauwerke lassen sich mit Ländern und Ortschaften assozieren, genauso wie Werke der Literatur, Kunst und Wissenschaft usw.

Zur Dokumentation eines geographischen Schwerpunkts dient ``gndo:geographicAreaCode``. Um eine <span class="emph">maschienelle Verarbeitung</span> dieser Information zu gewährleisten, muss ein Ländercode nach ISO 3166 via `@gndo:term` angegeben werden. Die GND stellt hierfür das Vokabular "[Geographic Area Codes](https://d-nb.info/standards/vocab/gnd/geographic-area-code)" zur Verfügung:

```xml
<place xml:id="goslar_rathaus" gndo:uri="http://d-nb.info/gnd/4231845-2">
   <gndo:preferredName>Rathaus Goslar</gndo:preferredName>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI">
       Niedersachsen
   </gndo:geographicAreaCode>
   <!-- ... -->
</place>
```

Eine geographische Einordnung entspricht in der Regel realen Orten, Gebieten etc. Für Fällen, bei denen eine Entität geographisch einem fingierten Ort zugeordnet werden soll, wird der Deskriptor `https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ` verwendet.

```xml
<person xml:id="malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName><gndo:personalName>Malachlabra</gndo:personalName></gndo:preferredName>
   <foaf:page>https://forgottenrealms.fandom.com/wiki/Malachlabra</foaf:page>
   <skos:note>Malachlabra ist ein fiktiver Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ">
      Avernus, Neun Höllen (Fiktiver Ort)
   </gndo:geographicAreaCode>
</person>
```

Sofern der geographische Schwerpunkt einer Entität unbekannt ist, dann kann dies über den Deskriptor `https://d-nb.info/standards/vocab/gnd/geographic-area-code#ZZ` verzeichnet werden.

Für eine Einspielung in die GND gelten **besonderer Anforderungen** in Hinblick auf verwendete Area Codes! Kontinentangaben (z.B. `XA`) müssen **immer** mit der Angabe eines konkreten Landes kompletiert sein, z.B. `XA-DE`.

Zur korrekten Erschließung von Ländercodes entsprechend der GND-Spezifika siehe im Besonderen im [GND-Wiki](https://wiki.dnb.de/x/O5FjBQ) unter "Ländercode-Leitfaden" und "Ländercodes"

**Zusätzliche Ressourcen**:


- Das Feld 043 (Ländercode) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/043.pdf)

<a name="entity-title"></a>
#### Namens- bzw. Titelangaben

Eine Entität hat in der Regel einen Namen, durch den dann Bezug auf sie genommen werden kann. In entityXML werden drei Oberkategorien von Namen unterschieden: (1) Vorzugsbezeichnung, (2) Alternative Namensform bzw. Namensvariante und (3) (Ressourcen)titel.

Eine **Vorzugsbenennung** definiert den hauptsächlich gebräuchlichen, also den <span class="emph">bevorzugten Namen,</span> mit dem auf eine Entität bezug genommen wird. Hierfür wird das Element ``gndo:preferedName`` verwendet, wobei sich optional die verwendete Sprache mit `@xml:lang` spezifizieren lässt; default ist hier Deutsch:

```xml
<place xml:id="santiago-de-chile">
   <gndo:preferredName xml:lang="es">Santiago de Chile</gndo:preferredName>
</place>
```

**Namensvarianten** bzw. Alternative Namensformen werden via ``gndo:variantName`` erfasst. Hierbei handelt es sich um <span class="emph">zusätzliche Namensformen</span>, unter denen die Entität ebenfalls bekannt ist, sofern vorhanden. Das können Spitz- oder Kosenamen sein, genauso wie Rufnamen. Wie bei `gndo:preferedName` kann die verwendete Sprache einer Namensvariante durch `@xml:lang` spezifiziert werden:

```xml
<place xml:id="santiago-de-chile">
   <!-- ... -->
   <gndo:variantName>Santiago</gndo:variantName>
   <gndo:variantName>Sanhatten</gndo:variantName>
   <skos:note>"Sanhatten", weil Santiago de Chile vom Cerro San Cristóbal wie Manhatten aussieht.</skos:note>
</place>
```

Vorzugsbenennungen und Namensvarianten werden bei jedem Entitätstyp leicht unterschiedlich gehandhabt, sowohl, was mögliche Inhalte als auch die Validierung betrifft. Dementsprechend lohnt sich ein Blick an die entsprechenden Stellen in diesem Handbuch:


- [Erschließung von Namen bei Personen](#person-name)

<a name="docs_d14e1853"></a>
##### Titel als Fallback

In entityXML gibt es noch die Möglichkeit einen **Fallback Titel** für eine Entität mittels `dc:title` zu vergeben, falls aus irgendwelchen Gründen keine Vorzugsbenennung oder Namensvariante dokumentiert werden soll. Mögliche Fälle wären z.B. Einträge, die bereits in der GND vorhanden sind, und für die man in seinen eigenen Einträgen keine eigene Namensformen anführen möchte:

```xml
<place xml:id="goslar_rathaus" gndo:uri="http://d-nb.info/gnd/4231845-2">
   <dc:title>Rathaus Goslar</dc:title>
</place>
```

<a name="entity-source"></a>
#### Quellenangabe

Das Wissen um eine Entität ist das Ergebniss von Recherche und diese beruht wiederum auf der Interpretation von Quellen. Angaben zu Quellen, aus denen die Informationsinhalte eines Entitätseintrages (z.B. eine bevorzugte Namensform oder biographische bzw. historische Informationen) zusammengestellt werden, werden via `source` dokumentiert. Hierbei handelt es sich bei bibliographischen Quellen für gewöhnlich um ein Kurzzitat der Quelle oder, falls es sich um eine nicht-bibliographische Quelle oder eine Onlineressource handelt, um einen prägnanten Begriff zur Bestimmung der Art der Quelle.

Onlineressourcen werden mit ihrem URL - optimaler Weise ein Permalink - via `@url` dokumentiert:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:variantName>
      <gndo:forename>Mai-Thi</gndo:forename>
      <gndo:surname>Leiendecker</gndo:surname>
   </gndo:variantName>
   <source url="https://de.wikipedia.org/wiki/Mai_Thi_Nguyen-Kim">Wikipedia</source>
</person>
```

```xml
<person xml:id="a_v_jungenfeld">
   <gndo:preferredName>
      <gndo:surname>Gedult von Jungenfeld</gndo:surname>
      <gndo:forename>Arnold Ferdinand</gndo:forename>
   </gndo:preferredName>
   <source url="http://http://scans.hebis.de/22/23/04/22230490_exl-1.jpg">Provenienzmerkmal</source>
   <source url="https://de.wikipedia.org/wiki/Arnold_von_Jungenfeld">Wikipedia</source>
</person>
```

Zusätzlich können weitere Informationen angegeben werden, die die Quelle oder einzelne Aspekte näher erläutern, z.B. Zugriffsdatum auf Webseiten. Hierzu werden in `source` die Elemente `title` und `note` verwendet: `title`, um den Kurztitel der Quelle zu erschließen und `note`, um den erleuternden Text zu dokumentieren.

```xml
<person xml:id="a_v_jungenfeld">
   <gndo:preferredName>
      <gndo:surname>Gedult von Jungenfeld</gndo:surname>
      <gndo:forename>Arnold Ferdinand</gndo:forename>
   </gndo:preferredName>
   <source url="https://de.wikipedia.org/wiki/Arnold_von_Jungenfeld"><title>Wikipedia</title><note>Stand: 2024-08-06</note></source>
</person>
```

Wenn es sich bei einer Quelle um eine Monographie handelt, dann sollte das Kurzzitat mindestens aus dem Verfassernamen mit Vornamen (auch abgekürzt), Titel und Jahr bestehen.

Bei unselbständigen Veröffentlichungen ist der Mindeststandard die Angabe von Verfassername mit Vorname (auch abgekürzt), Titel, Jahr, Titel der Zeitschrift/Zeitung mit Jahrgang - und/oder Bandzählung sowie Seitenzahl.

**Zusätzliche Ressourcen**:


- Das Feld 670 (Quellenangabe) im [GND-Erfassungsleitfaden](https://wiki.dnb.de/download/attachments/50759357/670.pdf)

<a name="entity-subject-categories"></a>
#### Sachgruppen

Durch die Zuordnung zu Sachgruppen können Entitäten systematisch klassifiziert werden. In entityXML werden die [Sachgruppen der GND](https://d-nb.info/standards/vocab/gnd/gnd-sc.html) (ehemals SWD, Schlagwortnormdatei) verwendet.

Die Dokumentation einer oder mehrerer Sachgruppen für eine Entität erfolgt durch ``gndo:gndSubjectCategory``, wobei jede Sachgruppe in jeweils einem eigenen `gndo:gndSubjectCategory` angegeben wird. Obligatorisch ist die Angabe des Sachgruppen-URIs aus dem oben angeführten [GND-Sachgruppen Vokabulars](https://d-nb.info/standards/vocab/gnd/gnd-sc.html) via `@gndo:term`. Empfohlen wird zudem die Angabe des menschenlesbaren Labels innerhalb des Elements als Textknoten:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:preferredName>
      <gndo:surname>Nguyen-Kim</gndo:surname>, <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#22.5p">Personen zu Chemie</gndo:gndSubjectCategory>
   <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#15.4p">Personen zu Rundfunk, Neuen Medien</gndo:gndSubjectCategory>             
</person>
```

Sofern englisch-sprachige (oder andere nicht-deutsch-sprachige) Labels bei der Dokumentation verwendet werden, bietet es sich an Entsprechendes via `@xml:lang` anzugeben:

```xml
<gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#15.4p" xml:lang="en">Persons associated with broadcasting, with new media</gndo:gndSubjectCategory>
```

<a name="entity-same-as"></a>
#### Gleiche Entität in anderer Normdatei oder anderem Datendienst

Entitäten können bereits in anderen (Norm)datendiensten verzeichnet sein (z.B. [Library of Congress Name Authority
                     File](https://id.loc.gov/authorities/names.html), [VIAF](https://viaf.org/), [Deutschen Digitalen Bibliothek](https://www.deutsche-digitale-bibliothek.de/), [Wikidata](https://www.wikidata.org/) etc.) 

Einträge, die in entityXML angelegt werden, lassen sich mit solchen "externen" Einträgen anderer Datendienste mittels ``owl:sameAs`` in Verbindung bringen. Dies dient grundsätzlich der eindeutigen Identifizierung der beschriebenen Entität:

```xml
<person xml:id="nguyen-kim-mai-thi" gndo:uri="https://d-nb.info/gnd/1135801363">
   <gndo:preferredName>
      <gndo:surname>Nguyen-Kim</gndo:surname>, <gndo:forename>Mai Thi</gndo:forename>
   </gndo:preferredName>
   <owl:sameAs>https://orcid.org/0000-0003-4787-0508</owl:sameAs>
   <owl:sameAs>https://viaf.org/viaf/3094149844949202960003</owl:sameAs>
   <owl:sameAs>https://www.wikidata.org/wiki/Q29169693</owl:sameAs>
   <owl:sameAs>https://id.loc.gov/authorities/names/n2019011012</owl:sameAs>
</person>
```

Darüberhinaus lassen sich so auch andere Informationsprozesse anschließen, um bspw. auf Informationen, die durch die entsprechenden Datendienste zur Verfügung gestellt werden, zuzugreifen.

<a name="entity-links"></a>
#### Sonstige Webressourcen

Links auf sonstige Onlineressourcen, die zusätzliche Informationen über die beschriebene Entität enthalten, können mit Hilfe von ``ref`` dokumentiert werden. Hierbei stehen zwei Auszeichnungsmöglichkeit zur Auswahl:

**(1)** Der Link zur entsprechenden Ressource wird direkt als Text innhalb von `ref` dokumentiert.

**(2)** Der Link zur entsprechenden Ressource wird im `@target` Attribut von `ref` dokumentiert und innerhalb von `ref` wird ein Stellvertretertext angegeben.

```xml
 <!-- Ein Beispiel mit 'ref/@target' und Stellvertretertext -->
 
 <person xml:id="Malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName>
      <gndo:personalName>Malachlabra</gndo:personalName>
   </gndo:preferredName>
   <skos:note>Malachlabra ist ein Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
   <foaf:page>https://forgottenrealms.fandom.com/wiki/Malachlabra</foaf:page>
   <ref target="https://www.reddit.com/r/DnD/comments/806uow/does_anyone_know_anything_about_malachlabra/">
      Reddit: Does anyone know anything about Malachlabra?
   </ref>
</person>
```

```xml
<!-- Das gleiche Beispiel ohne @target -->

<ref>https://www.reddit.com/r/DnD/comments/806uow/does_anyone_know_anything_about_malachlabra/</ref>
```

Es stellt sich vielleicht die Frage, warum man neben ``owl:sameAs``, ``source`` (und ``gndo:homepage`` bzw. ``foaf:page``, die an späterer Stelle in diesem Handbuch beschrieben werden), noch ein weiteres Element benötigt, um zusätzliche Links zu dokumentieren? Zum Einen, weil die eben anzitierten Elemente recht spezifische Sachverhalte abbilden, die wesentlich konkreter sind als "Eine Ressource, die zusätzliche Informationen über die beschriebene Entität enthält". Zum Anderen – und das ist nicht ganz ernst gemeint – freut sich (fast) Jede*r über ein "Sonstiges" Feld.

Ernsthaft: Bei `ref` handelt es sich einfach um eine Möglichkeit, Referenzen auf Online-Ressourcen zu dokumentieren, die nicht so richtig unter die Definitionen von `owl:sameAs`, `source`, `gndo:homepage` oder `foaf:page` passen wollen – nicht mehr und nicht weniger.


> **Keine Entsprechung in der GND**  
> Für diese Information gibt es keine Entsprechung in der GND! Daher wird sie bei der Erstellung eines neuen Normdateneintrags oder beim Update eines Bestehenden ignoriert.



<a name="docs_d14e2046"></a>
### Spezifische Entitätsklassen

<a name="person-record"></a>
#### Personen

Ein **Personeneintrag** (Personrecord) enthält Informationen zu *realen* oder *fiktiven Personen*. entityXML definiert ein festes Set an Eigenschaften aus dem GNDO-Namensraum, um Informationen zu dokumentieren, die von der GND unterstützt werden. Zusätzlich ist es möglich weitere Eigenschaften in einem <span class="emph">eigenen Namensraum</span> (Elemente und Attribute in sog. [custom namespaces](#custom-namespaces)) in einem Personeneintrag zu dokumentieren.

Ein Personeneintrag wird durch das Element `person` repräsentiert und benötigt eine **`@xml:id`**.

Sofern für die Person bereits ein Normdatensatz in der GND vorhanden ist, wird der entsprechende GND HTTP-URI in `@gndo:uri` dokumentiert. Falls nicht, ist die Angabe einer <span class="emph">Vorzugsbenennung</span> via `gndo:preferredName` oder einer <span class="emph">Namensvariante</span> via `gndo:variantName` **verpflichtend**.

```xml
<person xml:id="mmuster">
    <gndo:preferredName><!-- ... --></gndo:preferredName>
</person>
```

<a name="docs_d14e2094"></a>
##### Unterscheidung von Personentypen

Entitäten, die in `person` erschlossen werden, gelten per default als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#id-07e558e11b6629c85c4cf0312a8d9acc), und können via ``@gndo:type`` als Unterklasse einer gndo:DifferentiatedPerson weiter spezifiziert werden. Sofern es sich z.B. nicht um eine reale Person, sondern um eine fingierte Person bzw. eine Figur aus einem fiktionalen Werk handelt, dann würde der Eintrag entsprechend über `@gndo:type` mit '[https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)' spezifiziert werden:

```xml
<person xml:id="malachlabra" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName><gndo:personalName>Malachlabra</gndo:personalName></gndo:preferredName>
   <gndo:biographicalOrHistoricalInformation xml:lang="en">Malachlabra was an archfiend and the daughter of Dispater in the late 14th century DR. She was ambushed by the Simbul in 1372 DR.</gndo:biographicalOrHistoricalInformation>
   <skos:note>Malachlabra ist ein Charakter aus den D&amp;D: Forgotten Realms.</skos:note>
</person>
```

<a name="person-name"></a>
##### Erschließung von Vorzugsbenennung und Namensvarianten

Zur Erschließung von Namensformen für Personen dienen folgende Elemente:

- [`gndo:preferredName`](person-record-preferredName), **Vorzugsbenennung (Person)**: Die bevorzugte Namensform zur Denomination
                    einer Person.
- [`gndo:variantName`](person-record-variantName), **Variante Namensform (Person)**: Eine alternative Namensform, unter der eine
                    Person ebenfalls bekannt ist.


Für Personeinträge ist die Angabe einer <span class="emph">Vorzugsbenennung</span>**obligatorisch**, sofern die Person nicht bereits über einen GND-URI identifiziert wurde. Die Angabe von (beliebig vielen) <span class="emph">Namensvarianten</span> ist optional.

Eine Vorzugs- bzw. Alternativbenennung kann auf *zwei alternative Arten* angegeben werden:

(1) unter Angabe **verschiedener Namensbestandteile**

- **Vorname** [`gndo:forename`](#d17e4522) (*verpflichtend*)  
  "Ein oder mehrere Vornamen einer
                Person."
- **Namenspräfix** [`gndo:prefix`](#d17e5463) (*optional*)  
  "Namensteil, der dem Namen vorangestellt sind (z.B.
                    Adelskennzeichnungen, "von", "zu" etc.)."
- **Nachname** [`gndo:surname`](#d17e5896) (*verpflichtend*)  
  "Der Nach- oder Familienname, unter dem eine Person bekannt
                    ist."
- **Zählung** [`gndo:counting`](#d17e3608) (*optional*)  
  "Eine Zählung als Namensbestandteil (z.B. in Ramses **II** oder
                    Sethos **I**)."
- **Namenszusatz** [`gndo:nameAddition`](#d17e4918) (*optional und wiederholbar*)  
  "Zusätzliches Element des Namens, unter dem eine Person
                    bekannt ist, z.B. "Graf von Wallmoden"."
- **Beiname, Gattungsname, Titulatur, Territorium** [`gndo:epithetGenericNameTitleOrTerritory`](#d17e4311) (*optional und wiederholbar*)  
  "Beiname einer Person, bei
                    dem es sich um ein Epitheton, Titel oder eine Ortsassoziation handelt."



(2) unter Angabe eines **persönlichen Namens**

- **Persönlicher Name** [`gndo:personalName`](#d17e5276) (*verpflichtend*)  
  "Ein Name, unter dem die Person gemeinhin bekannt ist.
                    Ein persönlicher Name wird dann angegeben, wenn die Person nicht durch einen Vor- oder Nachname benannt wird, z.B.
                    "Aristoteles""



```xml
<!-- Beispiel Vorzugsbenennung mit Vor- und Nachnamen-->
<gndo:preferredName>
    <gndo:forename>Ludolph Christian</gndo:forename>
    <gndo:surname> Meier</gndo:surname>
</gndo:preferredName>

<!-- Beispiel Vorzugsbenennung mit persönlichem Namen-->
<gndo:variantName>
    <gndo:personalName>Aristoteles</gndo:personalName>
</gndo:variantName>

<!-- Beispiel Namensvariante mit präfigiertem "von"-->
<gndo:variantName>
    <gndo:forename>Georg</gndo:forename>
    <gndo:prefix>von</gndo:prefix>
    <gndo:surname>Mejern</gndo:surname>
</gndo:variantName>
```

Sowohl Vorzugsbenennungen als auch Namensvarianten können zusätzlich zur westlich normalisierten Form auch in **originalschriftlicher Form** angegeben werden.

Im Falle der Auszeichnung einer originalschriftlichen Vorzugsbennenung müssen drei Kriterien erfüllt sein: (1) Der Eintrag enthält eine Vorzugsbenennung laut Standardansetzung ,(2) die originalschriftliche Vorzugsbenennung ist via `@type` als `original` gekennzeichnet und (3) Sprach- als auch Schriftinformationen (`@xml:lang` und `@script`) sind explizit angegeben:

```xml
<gndo:preferredName type="original" xml:lang="ara" script="Arab">
   <gndo:forename>احمد بن محمد</gndo:forename>
   <gndo:prefix>ال</gndo:prefix>
   <gndo:surname>مرزوقي</gndo:surname>
</gndo:preferredName>
```

Namensvarianten werden ebenfalls mit expliziten Sprach- und Schriftinformationen (`@xml:lang` und `@script`) angelegt:

```xml
<gndo:variantName xml:lang="ara" script="Arab">
   <gndo:personalName>رضا كحالة, عمر</gndo:personalName>
</gndo:variantName>
```

<a name="person-geographical-area-code"></a>
##### Geographische Zuordnung

Die geographische Zuordnung von Personen zu einem Ort ist in der GND verpflichtend, so auch in entityXML! Wie [weiter oben](#entity-geographic-area) beschrieben wird hierfür `gndo:geographicAreaCode` verwendet, inkl. der Angabe eines Terms via `@gndo:term` aus dem Vokabular "[Geographic Area Codes](https://d-nb.info/standards/vocab/gnd/geographic-area-code)": 

<a name="person-dates"></a>
##### Lebens- und Wirkungsdaten

Lebens- und Wirkungsdaten sind wichtige Identifikationsmerkmale einer Person. Sie lassen sich mittels folgender Elemente dokumentieren: 

- [`gndo:dateOfBirth`](person-record-dateOfBirth), **Geburtsdatum (Person)**: Das Geburtsdatum der
                Person.
- [`gndo:dateOfDeath`](person-record-dateOfDeath), **Sterbedatum (Person)**: Das Sterbedatum der
                Person.
- [`gndo:periodOfActivity`](prop-periodOfActivity), **Wirkungsdaten**: Zeitraum, innerhalb dessen die beschriebene Entität aktiv
                    war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind. Wenn ein exaktes Wirkungsdatum erschlossen werden soll, dann wird das Datum als ISO 8601 YYYY-MM-DD in sowohl `@iso-from` als auch in `@iso-to` angegeben.


Die Elemente `gndo:dateOfBirth` und `gndo:dateOfDeath` markieren einen <span class="emph">Zeitpunkt</span>, dessen Datum als [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html), also in der Form **YYYY-MM-DD** via ``@iso-date`` dokumentiert wird.

```xml
<gndo:dateOfBirth iso-date="1980-01-11"></gndo:dateOfBirth>
<gndo:dateOfDeath iso-date="2020-01-11"></gndo:dateOfDeath>
```

Sofern sich Datumsangeben nicht genau bestimmen lassen, kann der wahrscheinliche Zeitpunkt über die Attribute ``@is-notBefore`` (Angabe des frühest möglichen Datums) und ``@iso-notAfter`` (Angabe des spätest möglichen Datums) näher eingegränzt werden.

```xml
<gndo:dateOfBirth iso-notBefore="1980">nach 1980</gndo:dateOfBirth>
```

`gndo:periodeOfActivity` beschreibt einen <span class="emph">Zeitraum</span> und verwendet dementsprechend je ein Attribute zur Beschreibung eines Anfangs- (``@iso-from``) und eines Enddatums (``@iso-to``), die ebenfalls dem Format ISO 8601 entsprechen müssen. Die Angabe von midestens einem Start- oder einem Enddatum ist hier verpflichtend. Ein Wirkungszeitraum wird nur dann erschlossen, wenn kein Geburts- und Sterbedatum für die beschriebene Person bekannt ist.

```xml
<gndo:periodOfActivity iso-from="1940" iso-to="2000"></gndo:periodOfActivity>
```

Sofern der Wirkungszeitraum als *exaktes Wirkungsdatum* erhoben werden soll, wird das Datum als ISO 8601 YYYY-MM-DD sowohl in `@iso-from` also auch `@iso-to` angegeben.

Zusätzlich zu den eher maschienenlesbaren Datumsangaben in den Attributen, kann eine menschenlesbare Repräsentation des Datums als Text innerhalb des jeweiligen Datums- oder Zeitraumelements angegeben werden.

```xml
<gndo:dateOfBirth iso-date="1980-01-11">11 Januar 1985</gndo:dateOfBirth>
<gndo:dateOfDeath iso-notAfter="1950">vor 1950</gndo:dateOfDeath>
<gndo:periodOfActivity iso-from="1940" iso-to="2000">1940-2000</gndo:periodOfActivity>
```

Wenn zu einem Wirkungsdatum noch eine Erläuterung angegeben werden soll, dann kann dies vie `note` erfolgen. Dazu wird innerhalb von `gndo:periodOfActivity` der menschenlesbare Text via `label` und die zusätzliche Erläuterung in `note` gesetzt:

```xml
<gndo:periodOfActivity iso-from="1940" iso-to="2000">
  <label>1980-2000</label>
  <note>Mitglied der entityXML-Gilde</note> 
</gndo:periodOfActivity>
```

<a name="person-places"></a>
##### Lebens- und Wirkungsorte

Lebens- und Wirkungsorte einer Person werden durch folgende Elemente erschlossen: 

- [`gndo:placeOfBirth`](person-record-placeOfBirth), **Geburtsort (Person)**: Der Geburtsort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.
- [`gndo:placeOfDeath`](person-record-placeOfDeath), **Sterbeort (Person)**: Der Sterbeort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.
- [`gndo:placeOfActivity`](person-record-placeOfActivity), **Wirkungsort (Person)**: Ein Wirkungsort, an dem die Person gewirkt oder
                    gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung
                    erfolgt die Erfassung des Ortes als Freitext im Element.


Der Ort wird als Text innerhalb des jeweiligen Ortelements dokumentiert. Es wird empfohlen zusätzlich den entsprechenden GND-URI des Orts via `@gndo:ref` anzugeben.

```xml
<gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4007666-0">Bonn</gndo:placeOfBirth>
<gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4023118-5">Hamburg</gndo:placeOfDeath>
<gndo:placeOfActivity gndo:ref="https://d-nb.info/gnd/4021477-1">Göttingen</gndo:placeOfDeath>
```

Für Wirkungsorte können zusätzlichen zeitliche Angaben via `@iso-date` oder `@iso-from` und `@iso-to` gemacht werden:

```xml
<gndo:placeOfActivity gndo:ref="https://d-nb.info/gnd/4021477-1" iso-from="1950" iso-to="1955">Göttingen</gndo:placeOfDeath>
```

<a name="person-gender"></a>
##### Angaben zum Geschlecht

Angaben zum Geschlecht einer Person können via `gndo:gender` dokumentiert werden. Mittels `@gndo:type` wird ein Term oder mehrere Terme aus dem GND Vokabular "Gender" [https://d-nb.info/standards/vocab/gnd/gender#](https://d-nb.info/standards/vocab/gnd/gender#) dokumentiert. Eine zusätzliche Beschreibung kann zusätzlich als Text innerhalb des Elements verzeichnet werden. 

```xml
<gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#female">weiblich</gndo:gender>
```

```xml
<gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#female https://d-nb.info/standards/vocab/gnd/gender#male">nichtbinär</gndo:gender>
```

<a name="person-profession"></a>
##### Berufe und Tätigkeiten

Berufe und/oder Tätigkeitsfelder einer Person lassen sich mittels `gndo:professionOrOccupation` dokumentieren.

```xml
<gndo:professionOrOccupation">Jongleuse</gndo:professionOrOccupation>
```

Optional aber empfohlen ist die zusätzliche Angabe eines Sachbegriffs aus der GND über `@gndo:ref`, und zwar als URI:

```xml
<gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4025098-2">Historiker</gndo:professionOrOccupation>
```

Berufe und Tätigkeiten lassen sich optional via `@type` als Hauptbeschäftigung, sprich als 'significant' spezifizieren:

```xml
<gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4025098-2">
   Historiker
</gndo:professionOrOccupation>
<gndo:professionOrOccupation gndo:type="significant" gndo:ref="https://d-nb.info/gnd/4161681-9">
   Informationswissenschaftler
</gndo:professionOrOccupation>
```

<a name="person-publications"></a>
##### Titelangaben/Veröffentlichungen

Über `gndo:publication` lassen sich Veröffentlichungen dokumentieren, die mit der beschriebenen Person in Zusammenhang stehen. Das kingt unspezifisch und genau so ist es auch gedacht: Veröffentlichungen bzw. "Titelangaben" gelten in der GND vorallem als weiteres <span class="emph">Distinguierungssmerkmal</span>, um die Person von Anderen zu unterscheiden, sprich: Die auf diese Weise verzeichneten Titel belegen einerseits die Existenz der Person als Entität und enthalten andererseits weitere Informationen (z.B. Vorzugsbenennungen, Kontextinformationen etc.), um im Zweifelsfall homonyme Personen von einander unterscheiden zu können.

In `gndo:publication` können dementsprechend Veröffentlichungen dokumentiert werden, die …


- … von der Person selber stammen (<span class="emph">autorschaftlicher Berzug</span>)
- … von der Person herausgegeben wurden (<span class="emph">herausgeberschaftlicher Bezug</span>)
- … von der Person handeln (<span class="emph">thematischer Bezug</span>)

Diese drei unterschiedlichen Bezüge werden im optionalen `@role` Attribut in `gndo:publication` erfasst, das folgenden Wertebereich vordefiniert: "**auhor**" (autorschaftlicher Bezug), "**editor**" (herausgeberschaftlicher Bezug), "**about**" (thematischer Bezug). Sofern `@role` nicht explizit ausgewiesen wird, gilt der Bezug als autorschaftlich, sprich: "author" gilt als *default*.

Innerhalb von `gndo:publication` gibt es ein Basisset von Elementen, die verwendet werden können, um ein grundständiges bibliographisches Zitat zu strukturieren:


- **Autor, erste Verfasserschaft** [`gndo:firstAuthor`](#d17e4491) (*optional und wiederholbar*)  
  "Die Person oder Körperschaft, die
                    verantwortlich für die primäre Verfasserschaft an einem Werk oder einer Veröffentlichung zeichnet."
- **Titel** [`title`](#d17e6862) (*verpflichtend*)  
  "Ein Titel für das Informationsobjekt, das in diesem Kontext
                    beschrieben wird."
- **Titelzusatz** [`add`](#d17e2597) (*optional*)  
  "Zusätze zum Titel im Rahmen der bibliographischen
                    Angabe."
- **Datum** [`date`](#d17e2985) (*verpflichtend*)  
  "Ein Zeitpunkt oder Zeitraum, der mit einem Ereignis im Lebenszyklus
                    der beschriebenen Ressource in Zusammenhang steht."


> **Warum nur ein 'Basisset'?**  
> entityXML implementiert (bisher) nur das Nötigste an Elementen, um eine bibliographsche Angabe zu beschreiben, die sich später in einen MARC-Datensatz konvertieren lässt. An dieser Stelle soll nicht die Ausführlichkeit eines Bibliothekskatalogs abgebildet werden, sondern lediglich die Informationseinheiten, die als eigenständige Felder in den MARC Feldern 670 und 672 benötigt werden. 


```xml
<person xml:id="uwe_sikora" gndo:uri="https://d-nb.info/gnd/1155094360">
   <gndo:preferredName>
      <gndo:forename>Uwe</gndo:forename>
      <gndo:surname>Sikora</gndo:surname>
   </gndo:preferredName>
   <gndo:publication>
      <title>entityXML Handbuch</title>, 
      <date>2023</date>
   </gndo:publication>
</person>
```

Sofern Titelangaben verzeichnet werden, die von der beschreibenenen Person handeln, wird die Angabe eines <span class="emph">Autor\*innennamens</span> zur Pflicht:

```xml
<person xml:id="wilhelm_meister" gndo:type="https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter">
   <gndo:preferredName>
      <gndo:surname>Meister</gndo:surname>
      <gndo:forename>Wilhelm</gndo:forename>
   </gndo:preferredName>
   <gndo:publication role="about" gndo:ref="https://d-nb.info/gnd/4123299-9">
      <gndo:firstAuthor>Goethe, Johann Wolfgang von</gndo:firstAuthor>
      <title>Wilhelm-Meister-Romane</title>
      <date>1776</date>
   </gndo:publication>
</person>
```

<a name="docs_d14e2441"></a>
##### Beispiel 1: Maxima Musterfrau

Wie alle Einträge kann ein Personeneintrag in eher prosaischer Form verfasst werden:

```xml
<person xml:id="maxima_musterfrau" agency="create">
    <gndo:preferredName><gndo:surname>Musterfrau</gndo:surname>,<gndo:forename>Maxima</gndo:forename>  
    </gndo:preferredName>, auch bekannt als <gndo:variantName><gndo:personalName>Maxima</gndo:personalName>
    </gndo:variantName>, geboren <gndo:dateOfBirth iso-date="1990-05-25">25.05.1990</gndo:dateOfBirth> in 
    <gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4592817-4">Wuhlheide (Berlin)</gndo:placeOfBirth> ist 
    die Tochter von Max Mustermann. Ein Eintrag in der GND könnte <gndo:gndIdentifier cert="low">118500775</gndo:gndIdentifier> 
    sein. Man kann aufjedenfall sagen, dass sie in
    <gndo:placeOfActivity>Berlin</gndo:placeOfActivity> aktiv ist und zwar als bekannte
    <gndo:professionOrOccupation>Aktivistin</gndo:professionOrOccupation> der Initiative "Normdaten für alle aber 
    ordentlich!". Sie arbeitet als freiberufliche 
    <gndo:professionOrOccupation>Bibliothekarin</gndo:professionOrOccupation> in ihrer frei
    zugänglichen Privatbibliothek. Ihre Homepage <foaf:page>http://rettet-normdaten-ev.org/</foaf:page> bietet 
    zahlreiche Informationen zu Normdaten. <skos:note>Dieser Eintrag ist ein Test, unterschiedliche Aspekte der 
        Validierung und der Inhaltsmodelle abzuprüfen.</skos:note>
    <revision status="opened">
        <change when="2022-05-10" who="US">Berlin URI bei placeOfActivity raussuchen und nachtragen.</change>
        <change when="2022-04-06" who="US">Die GND-ID 118500775 identifiziert nicht Maxima Musterfrau sondern Theodor W. Adorno.</change>
        <change when="2022-03-31" who="MM">Eintrag angelegt. Bitte in die GND überführen.</change>
    </revision>
</person>
```

<a name="docs_d14e2451"></a>
##### Beispiel 2: Ernest Chevalier

Hier ein Beispiel, dass im Gegensatz zur prosaischen Form eine eher strukturierte Form eines Eintrages verdeutlicht:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName>
   <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>
   <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>
   <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR">Frankreich</gndo:geographicAreaCode>
   <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" gndo:type="significant">Politiker</gndo:professionOrOccupation>
   <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   <revision status="opened">
      <change when="2023-01-21" who="MM">Bitte in die GND aufnehmen</change>
   </revision>
</person>
```

<a name="docs_d14e2461"></a>
##### Modell: Personeneintrag

Das **Modell** für einen Personeneintrag sieht wie folgt aus:

```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:acquaintanceshipOrFriendship" repeatable="true"/>
      <element name="gndo:academicDegree"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfBirth"/>
      <element name="gndo:dateOfDeath"/>
      <element name="gndo:periodOfActivity" repeatable="true"/>
      <element name="gndo:familialRelationship" repeatable="true"/>
      <element name="gndo:fieldOfStudy" repeatable="true"/>
      <element name="gndo:functionOrRole" repeatable="true"/>
      <element name="gndo:gender"/>
      <element name="gndo:placeOfActivity" repeatable="true"/>
      <element name="gndo:placeOfBirth"/>
      <element name="gndo:placeOfDeath"/>
      <element name="gndo:playedInstrument" repeatable="true"/>
      <choice>
         <element name="gndo:preferredName"/>
         <group>
            <element name="gndo:preferredName" required="true"/>
            <element name="gndo:preferredName" required="true"/>
         </group>
      </choice>
      <element name="gndo:professionOrOccupation" repeatable="true"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:pseudonym" repeatable="true"/>
      <element name="gndo:titleOfNobility" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


**[`@gndo:type`](#person-types)**  
(*Personentyp*): Typisierung der dokumentierten Person (z.B. als
                            fiktive Person, Gott oder royale Person). Wenn kein `@gndo:type` vergeben wird, gilt die Person als
                            [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson).
  **Möglicher Wertebereich**:  


 - '*https://d-nb.info/standards/elementset/gnd#CollectivePseudonym*' : (Sammelpseudonym)
                [GNDO](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - '*https://d-nb.info/standards/elementset/gnd#Gods*' : (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#Gods)
 - '*https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter*' : (Literarische oder Sagengestalt)
                [GNDO](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - '*https://d-nb.info/standards/elementset/gnd#Pseudonym*' : (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - '*https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse*' : (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses)
                [GNDO](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - '*https://d-nb.info/standards/elementset/gnd#Spirits*' : (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#Spirits)



**[`@xml:id`](#d17e7648)**  ***ID***  
(*Record ID*): Angabe eines Identifiers zur Identifikation eines
                    Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!

**[`@gndo:uri`](#d17e6984)**  ***anyURI***  
(*GND-URI*): Angabe des GND-HTTP URIs der beschriebenen Entität in der GND,
                    sofern vorhanden.(http|https)://d-nb.info/gnd/(.+)

**[`@agency`](#attr.record.agency)**  
(*Agenturanfrage*): Anfrage an die Agentur, welche Aktion in Hinblick auf den
                    Eintrag durchgeführt werden soll.
  **Möglicher Wertebereich**:  


 - '*create*' : (Eintrag in der GND neu anlegen) Der Eintrag soll als
                                Normdatensatz neu in der GND angelegt werden.
 - '*update*' : (Normdatensatz mit Daten aus dem Eintrag erweitern) Der
                                Normdatensatz soll durch neue Informationen angereichert werden.
 - '*merge*' : (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag
                                dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist,
                                zusammengeführt werden.
 - '*ignore*' : (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht
                                bearbeitet.



**[`@enrich`](#d17e7628)**  ***boolean***  
(*Anreicherung*): Der Record soll mit Daten eines externen Datendienstes
                    angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt,
                    um den Record anzureichern!

**[`foaf:page`](#elem.foaf.page)**  
(*Webseite*): URL eines Dokuments mit näheren Informationen über die
                    beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.).

**[`gndo:acquaintanceshipOrFriendship`](#d17e3345)**  
(*Beziehung, Bekanntschaft, Freundschaft*): Beziehung zwischen einer Person, Familie oder Körperschaft und einer anderen Person, Familie oder Körperschaft, die nicht näher bestimmt werden kann.

**[`gndo:academicDegree`](#d17e3318)**  
(*Akademischer Grad*): Angaben zum akademischen Grad der Person, z.B. "Dr.
                    jur.".

**[`gndo:affiliation`](#d17e3398)**  
(*Zugehörigkeit*): Die Person oder Körperschaft ist zugehörig zu einer
                    Körperschaft, oder ist mit einem Ort oder einem Event verbunden.

**[`gndo:broaderTerm`](#d17e3502)**  
(*Oberbegriff*): Oberbegriff bzw. Oberklasse der beschriebenen
                    Entität.

**[`gndo:dateOfBirth`](#person-record-dateOfBirth)**  
(*Geburtsdatum (Person)*): Das Geburtsdatum der
                Person.

**[`gndo:dateOfDeath`](#person-record-dateOfDeath)**  
(*Sterbedatum (Person)*): Das Sterbedatum der
                Person.

**[`gndo:periodOfActivity`](#prop-periodOfActivity)**  
(*Wirkungsdaten*): Zeitraum, innerhalb dessen die beschriebene Entität aktiv
                    war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind. Wenn ein exaktes Wirkungsdatum erschlossen werden soll, dann wird das Datum als ISO 8601 YYYY-MM-DD in sowohl `@iso-from` als auch in `@iso-to` angegeben.

**[`gndo:familialRelationship`](#d17e4367)**  
(*Familiäre Beziehung*): Eine familiäre Beziehung zwischen einer Person oder Familie und einer anderen Person oder Familie.

**[`gndo:fieldOfStudy`](#d17e4451)**  
(*Studienfach*): Das Studienfach bzw. -gebiet einer Person. Über `@gndo:ref`
                    kann ein entsprechendes Schlagwort aus der GND mit erfasst werden.

**[`gndo:functionOrRole`](#d17e4550)**  
(*Funktion oder RolleFunction or role*): Anm.: Seit 2017 aus der GND
                entfernt!

**[`gndo:gender`](#d17e4581)**  
(*Geschlecht*): Angaben zum Geschlecht einer
                Person.

**[`gndo:placeOfActivity`](#person-record-placeOfActivity)**  
(*Wirkungsort (Person)*): Ein Wirkungsort, an dem die Person gewirkt oder
                    gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung
                    erfolgt die Erfassung des Ortes als Freitext im Element.

**[`gndo:placeOfBirth`](#person-record-placeOfBirth)**  
(*Geburtsort (Person)*): Der Geburtsort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.

**[`gndo:placeOfDeath`](#person-record-placeOfDeath)**  
(*Sterbeort (Person)*): Der Sterbeort der Person. Über `@gndo:ref` kann ein
                    Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im
                    Element.

**[`gndo:playedInstrument`](#prop-playedInstrument)**  
(*Gespieltes Instrument*): Ein Instrument, dass durch die beschriebene Entität gespielt wird.

**[`gndo:preferredName`](#person-record-preferredName)**  
(*Vorzugsbenennung (Person)*): Die bevorzugte Namensform zur Denomination
                    einer Person.

**[`gndo:professionOrOccupation`](#d17e5488)**  
(*Beruf oder Beschäftigung*): Angabe zum Beruf, Tätigkeitsbereich o.Ä., der
                    dokumentierten Person. Zulässige Deskriptoren stammen aus der GND-Systematik und entprechen GND-URIs von Sachbegriffen, z.B.
                    https://d-nb.info/gnd/4168391-2 ("Lyriker").

**[`gndo:publication`](#d17e5604)**  
(*Publikation*): Eine Publikation, die mit der Entität in Zusammenhang steht.
                    Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden
                    sind und auch (3) um Titel über die Entität handeln.

**[`gndo:pseudonym`](#d17e5556)**  
(*Pseudonym*): Ein Pseudonym, unter dem die dokumentierte Person ebenfalls
                    bekannt ist.

**[`gndo:titleOfNobility`](#d17e5969)**  
(*Adelstitel*): Ein Adelsname der dokumentierten Person oder
                    Personengruppe.

**[`gndo:topic`](#d17e6005)**  
(*Thema*): Thema, das sich auf eine Körperschaft, eine Konferenz, eine Person, eine Familie, ein Thema oder ein Werk bezieht.

**[`gndo:variantName`](#person-record-variantName)**  
(*Variante Namensform (Person)*): Eine alternative Namensform, unter der eine
                    Person ebenfalls bekannt ist.

**[`dc:title`](#d17e3011)**  
(*Titel (Record)*): Ein Titel für die Ressource. `dc:title` wird dann zur
                    Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll.

**[`gndo:biographicalOrHistoricalInformation`](#d17e3463)**  
(*Biographische Angaben*): Biographische oder historische Angaben über die
                    Entität.

**[`dublicateGndIdentifier`](#dublicates)**  
(*GND-Identifier einer Dublette*): Der GND-Identifier einer möglichen
                    Dublette.

**[`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)**  
(*Geographischer Schwerpunkt*): Ländercode(s) der Staat(en), denen die Entität
                    zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und
                    eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular
                    (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!

**[`gndo:gndIdentifier`](#d17e4693)**  
(*GND-Identifier*): Der GND-Identifier eines in der GND eingetragenen
                    Normdatensatzes sofern vorhanden

**[`gndo:gndSubjectCategory`](#d17e4734)**  
(*GND-Sachgruppe*): Term aus dem GND-Sachgruppen Vokabular (ehemals
                    SWD)

**[`img`](#elem.img)**  
(*Bild*): Ein Bild bzw. ein URL zu einer
                Bildressource.

**[`gndo:languageCode`](#d17e4843)**  
(*Sprachraum*): Code(s) des Sprachraumes, dem die Entität zugeordnet werden
                    kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein
                    Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben
                werden!

**[`gndo:relatesTo`](#d17e5757)**  
(*Beziehung*): Beziehung der beschriebenen Entität zu einer anderen Entität,
                    entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der
                    GNDO!

**[`ref`](#elem.ref)**  
(*Hyperlink*): Ein Link zu einer Webressource, die weitere Informationen über
                    die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target`
                    dokumentiert.

**[`owl:sameAs`](#elem.owl.sameAs)**  
(*Gleiche Entität*): HTTP-URI der selben Entität in einem anderen
                    Normdatendienst.

**[`skos:note`](#elem.skos.note)**  
(*Anmerkung*): Eine Anmerkung zum Eintrag.

**[`source`](#elem.source)**  
(*Quellenangabe*): Angaben zu einer Quelle, die verwendet wurde, um die in
                    diesem Eintrag gegebenen Informationen zu recherchieren.

**[`anyElement.narrow`](#any-restricted)**  
(*ANY ELEMENT (eng gefasst)*): Ein Element, dessen **QName** einem *custom
                    namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist.

**[`revision`](#revision)**  
(*Revisionsbeschreibung*): Statusinformationen zu einer Ressource
                    (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur
                    Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung
                    nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der
                Revisionsbeschreibung!

---


<a name="cb-record"></a>
#### Körperschaften

[TODO]

```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:placeOfBusiness"/>
      <element name="gndo:precedingCorporateBody"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:succeedingCorporateBody"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


**[`@gndo:type`](#d17e720)**  
(*Körperschaftstyp*): Typisierung der dokumentierten Körperschaft
                            (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als
                            [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody).
  **Möglicher Wertebereich**:  


 - '*https://d-nb.info/standards/elementset/gnd#Company*' : (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#Company)
 - '*https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody*' : (Fiktive Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - '*https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody*' : (Musikalische Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - '*https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody*' : (Organ einer Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - '*https://d-nb.info/standards/elementset/gnd#ProjectOrProgram*' : (Projekt oder Programm)
                [GNDO](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit*' : (Religiöse Verwaltungseinheit)
                [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - '*https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody*' : (Religiöse Körperschaft)
                [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)



**[`@xml:id`](#d17e7648)**  ***ID***  
(*Record ID*): Angabe eines Identifiers zur Identifikation eines
                    Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend!

**[`@gndo:uri`](#d17e6984)**  ***anyURI***  
(*GND-URI*): Angabe des GND-HTTP URIs der beschriebenen Entität in der GND,
                    sofern vorhanden.(http|https)://d-nb.info/gnd/(.+)

**[`@agency`](#attr.record.agency)**  
(*Agenturanfrage*): Anfrage an die Agentur, welche Aktion in Hinblick auf den
                    Eintrag durchgeführt werden soll.
  **Möglicher Wertebereich**:  


 - '*create*' : (Eintrag in der GND neu anlegen) Der Eintrag soll als
                                Normdatensatz neu in der GND angelegt werden.
 - '*update*' : (Normdatensatz mit Daten aus dem Eintrag erweitern) Der
                                Normdatensatz soll durch neue Informationen angereichert werden.
 - '*merge*' : (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag
                                dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist,
                                zusammengeführt werden.
 - '*ignore*' : (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht
                                bearbeitet.



**[`@enrich`](#d17e7628)**  ***boolean***  
(*Anreicherung*): Der Record soll mit Daten eines externen Datendienstes
                    angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt,
                    um den Record anzureichern!

**[`foaf:page`](#elem.foaf.page)**  
(*Webseite*): URL eines Dokuments mit näheren Informationen über die
                    beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.).

**[`gndo:abbreviatedName`](#d17e3272)**  
(*Abgekürzter Name*): Eine abgekürzte Namensform der dokumentierten
                    Entität.

**[`gndo:affiliation`](#d17e3398)**  
(*Zugehörigkeit*): Die Person oder Körperschaft ist zugehörig zu einer
                    Körperschaft, oder ist mit einem Ort oder einem Event verbunden.

**[`gndo:broaderTerm`](#d17e3502)**  
(*Oberbegriff*): Oberbegriff bzw. Oberklasse der beschriebenen
                    Entität.

**[`gndo:dateOfEstablishment`](#d17e4104)**  
(*Gründungsdatum*): Das Datum, an dem die beschriebene Entität entstanden ist
                    bzw. gegründet wurde.

**[`gndo:dateOfEstablishmentAndTermination`](#d17e4135)**  
(*Gründungs- und Auflösungsdatum*): Zeitraum, innerhalb dessen die
                    beschriebene Entität bestand.

**[`gndo:placeOfBusiness`](#d17e5119)**  
(*Sitz (Körperschaft)*): Der (Haupt)sitz der
                Körperschaft.

**[`gndo:precedingCorporateBody`](#d17e5335)**  
(*Vorherige Körperschaft (Körperschaft)*): Eine Vorgängerinstitution der
                    Körperschaft.

**[`gndo:preferredName`](#d17e5423)**  
(*Vorzugsbenennung (Standard)*): Eine bevorzugte Namensform, um die
                    beschriebenen Entität zu bezeichnen.

**[`gndo:publication`](#d17e5604)**  
(*Publikation*): Eine Publikation, die mit der Entität in Zusammenhang steht.
                    Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden
                    sind und auch (3) um Titel über die Entität handeln.

**[`gndo:succeedingCorporateBody`](#d17e5836)**  
(*Nachfolgende Körperschaft (Körperschaft)*): Eine Nachfolgeinstitution der
                    Körperschaft.

**[`gndo:temporaryName`](#d17e5924)**  
(*Zeitweiser Name*): Eine Namensform, die zeitweise zur Bezeichnung der
                    dokumentierten Entität benutzt wurde.

**[`gndo:topic`](#d17e6005)**  
(*Thema*): Thema, das sich auf eine Körperschaft, eine Konferenz, eine Person, eine Familie, ein Thema oder ein Werk bezieht.

**[`gndo:variantName`](#elem.gndo.variantName.standard)**  
(*Variante Namensform (Standard)*): Eine alternative Namensform, um die
                    beschriebenen Entität zu bezeichnen.

**[`dc:title`](#d17e3011)**  
(*Titel (Record)*): Ein Titel für die Ressource. `dc:title` wird dann zur
                    Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll.

**[`gndo:biographicalOrHistoricalInformation`](#d17e3463)**  
(*Biographische Angaben*): Biographische oder historische Angaben über die
                    Entität.

**[`dublicateGndIdentifier`](#dublicates)**  
(*GND-Identifier einer Dublette*): Der GND-Identifier einer möglichen
                    Dublette.

**[`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)**  
(*Geographischer Schwerpunkt*): Ländercode(s) der Staat(en), denen die Entität
                    zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und
                    eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular
                    (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden!

**[`gndo:gndIdentifier`](#d17e4693)**  
(*GND-Identifier*): Der GND-Identifier eines in der GND eingetragenen
                    Normdatensatzes sofern vorhanden

**[`gndo:gndSubjectCategory`](#d17e4734)**  
(*GND-Sachgruppe*): Term aus dem GND-Sachgruppen Vokabular (ehemals
                    SWD)

**[`img`](#elem.img)**  
(*Bild*): Ein Bild bzw. ein URL zu einer
                Bildressource.

**[`gndo:languageCode`](#d17e4843)**  
(*Sprachraum*): Code(s) des Sprachraumes, dem die Entität zugeordnet werden
                    kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein
                    Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben
                werden!

**[`gndo:relatesTo`](#d17e5757)**  
(*Beziehung*): Beziehung der beschriebenen Entität zu einer anderen Entität,
                    entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der
                    GNDO!

**[`ref`](#elem.ref)**  
(*Hyperlink*): Ein Link zu einer Webressource, die weitere Informationen über
                    die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target`
                    dokumentiert.

**[`owl:sameAs`](#elem.owl.sameAs)**  
(*Gleiche Entität*): HTTP-URI der selben Entität in einem anderen
                    Normdatendienst.

**[`skos:note`](#elem.skos.note)**  
(*Anmerkung*): Eine Anmerkung zum Eintrag.

**[`source`](#elem.source)**  
(*Quellenangabe*): Angaben zu einer Quelle, die verwendet wurde, um die in
                    diesem Eintrag gegebenen Informationen zu recherchieren.

**[`anyElement.narrow`](#any-restricted)**  
(*ANY ELEMENT (eng gefasst)*): Ein Element, dessen **QName** einem *custom
                    namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist.

**[`revision`](#revision)**  
(*Revisionsbeschreibung*): Statusinformationen zu einer Ressource
                    (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur
                    Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung
                    nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der
                Revisionsbeschreibung!

---


<a name="place-record"></a>
#### Orte

[TODO]

---


<a name="work-record"></a>
#### Werke

[TODO]

---


<a name="docs_d14e2511"></a>
## Tutorials

<a name="docs_d14e2517"></a>
### HOW TO: Person

Dieses Tutorial dient der Verdeutlichung, wie man in entityXML eine Person dokumentiert.

Als Beispiel werden wir **[Ernest Chevalier](https://fr.wikipedia.org/wiki/Ernest_Chevalier)** (Warum ausgerechnet Ernest Chevalier, werden wir im Laufe des Tutorials sehen) beschreiben, über den unsere Recherche folgende Informationen ergeben hat: 
```text
Chevalier, Ernest (männlich)
Französischer Politiker
Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
Geboren: 14.08.1820, Villers-en-Vexin
Gestorben: 05.12.1887, Paris
```


Wir starten mit dem `person` Element, dass wir im Datenbereich (`data/list`) unserer entityXML Ressource bereits angelegt haben. Da jede Entität bzw. jeder Eintrag in entityXML durch eine ID eindeutig identifiziert werden <span class="emph">muss</span>, fügen wir `@xml:id` hinzu. Im Grunde ist die Form der ID frei wählbar, solange es sich um einen [NCName](https://www.w3.org/TR/xml-names11/#NT-NCName) handelt, vereinfacht gesagt: Eine Zeichenkette, die mit einem Buchstaben beginnt und kein ":" enthält. Für unser Beispiel bleiben wir der Einfachheit halber bei `ernest_chevalier`. 

Ausserdem wollen wir einer potenziellen GND Agentur, die diese Daten später verarbeiten soll, mitteilen, dass es sich hierbei um einen Eintrag handelt, der in der GND angelegt werden soll. Das machen wir mit Hilfe von `@agency`, dem wir den Wert "create" geben.

```xml
<data>
   <list>
      
      <!-- Einträge davor -->
      
      <person xml:id="ernest_chevalier" agency="create"></person>
      
      <!-- Einträge danach -->
   </list>
</data>
```

Jetzt ist der Eintrag initial angelegt und identifiziert. Nun können wir unseren Personeneintrag nach und nach mit den recherchierten Daten anreichern. Am Einfachsten wäre es, wenn wir unser Rechercheergebnis erstmal in den leeren Eintrag kopieren. So haben wir schoneinmal alles zusammen:

```xml
<person xml:id="ernest_chevalier" agency="create">
   Chevalier, Ernest (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
</person>

```

Bevor wir uns nun dran machen, Daten in diesem Eintrag zu erschließen, legen wir noch eine Revisionsbeschreibung via `revision` an und dokumentieren die Bearbeitungsphase und den Zeitpunkt, an dem wir mit diesem Eintrag begonnen haben. Der Eintrag ist ganz frisch angelegt und befindet sich somit in der Anfangsphase; dementsprechend verzeichnen wir "opened" im `@status` Attribut von `revision` und legen einen ersten Änderungseintrag mit dem aktuellen Datum an:

```xml
<person xml:id="ernest_chevalier" agency="create">
   Chevalier, Ernest (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>

```

Bei Ernest Chevalier handelt es sich um eine Person aus Fleisch und Blut und nicht etwa um eine fiktive Person oder gar einen Gott. Dementsprechend gilt er als 'gndo:DifferentiatedPerson' und so benötigen wir keine weitere Spezifizierung via `@gndo:type`.

Da entityXML als **Markupformat** konzipiert ist, können die (derzeit noch) in unstrukturierter, rein menschenlesbarer Form vorliegendenden Daten sukzessive ausgezeichnet werden. Damit ein Personeneintrag (ohne GND-URI) valide ist, d.h. dem Schema von entityXML entspricht, müssen wir mindestend eine **Vorzugsbenennung** angeben, also den Namen, unter dem Ernest Chevalier hauptsächlich bekannt ist. Dafür verwenden wir `gndo:preferredName`:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>Chevalier, Ernest</gndo:preferredName> (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Das entityXML Schema sieht bei einem Personennamen vor, dass er entweder aus einem *persönlichen Namen* (`gndo:personalName`) oder aus einem *Vor*- (`gndo:forename`) und einem *Nachnamen* (`gndo:surname`) besteht:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (männlich)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Sofern wir in *oXygen XML* oder einem anderen Editor, der "schema-aware" ist, also eine Schemadatei zur automatischen Validierung verwenden kann und dementsprechend Rückmeldung gibt, sehen wir nun ein valides Stück XML: Unser Personeneintrag zu Ernest Chevalier ist eindeutig in der XML identifiziert und durch eine Vorzugsbenennung benannt und erfüllt so die Mindestanforderungen. Nun kümmern wir uns um die weiteren Daten.

Da wir das **biologische Geschlecht** von Herrn Chevalier recherchiert haben und uns diese Information maschienenlesbar zur Verfügung stehen soll, verwenden wir `gndo:gender` zur Auszeichnung der Information im Text:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender>männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Nun meldet sich die Validierung abermals zu Wort, denn das entityXML Schema sieht vor, dass `gndo:gender` das Geschlecht explizit über einen Bezeichner aus dem [GND Vokabular "Gender"](https://d-nb.info/standards/vocab/gnd/gender#) ausweist. Dafür verwenden wir `@gndo:term`; die zulässigen Werte sind bereits vordefiniert:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "Mollenhauer, Klaus: Vergessene Zusammenhänge, 2003, S. 139". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Bisher habe ich noch garnicht gesagt, warum wir Ernest Chevalier überhaupt anlegen. Wie für viele Andere gibt es auch für ihn (noch) keinen Eintrag in der GND. Interessant ist er für unser kleines Tutorial allerdings aus einem anderen Grund, denn die Beschreibung von Ernest Chevalier ist Teil eines <span class="emph">konkreten Anwendungsfalles</span>: Ein Editionsprojekt (im vorliegenden Falle die [Mollenhauer
                     Gesamtausgabe](https://www.uni-goettingen.de/de/klaus+mollenhauer+gesamtausgabe+%28kmg%29/584741.html)) identifiziert alle in der digitalen Edition namentlich aufgeführten Personen über GND-URIs. Für die Fälle, für die kein GND-URI, also kein Eintrag in der GND existiert, sollen die Personen trotzdem eindeutig identifiziert und mit weiteren Daten dokumentiert werden, um hiermit weitere Informationsprozesse zu ermöglichen bzw. zu steuern (z.B. automatisierte Registererstellung, Anzeige zusätzlicher Informationen auf dem Projektportal zu einer Person, Austausch der dokumentierten Daten mit der GND etc.).

Und genau aus diesem Anwendungsszenario stammt die Information "*Vergessene Zusammenhänge (S. 139)*", denn genau hier erwähnt [Klaus Mollenhauer](https://d-nb.info/gnd/118583352) den von uns hier in der Erschließung befindlichen Ernest Chevalier. Entsprechend geben wir diese Referenz als `gndo:publication` mit dem Verweis auf die Ausgabe von 2003 im **Katalog der Deutschen Nationalbibliothek** via `@dnb:catalogue` an. In der GND existiert zudem ein Werknormdatensatz für die "Vergessenen Zusammenhänge", den wir ebenfalls dokumentieren wollen und mittels `@gndo:ref` angeben:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: https://fr.wikipedia.org/wiki/Ernest_Chevalier
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Bevor wir weiter machen und die restlichen Daten auszeichnen, sollten wir zunächst explizit machen, **wo** wir diese Informationen eigentlich her haben. Wir stützen uns auf die Informationen, die in der französischen Wikipedia aufgeführt sind und verwenden `source`, um den *URL der entsprechenden Seite* explizit als **Quelle** auszuzeichnen:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Hierbei handelt es sich auch gleichsam um ein **Dokument für zusätzliche Informationen** zu Ernest Chevalier und da wir nicht alle Einzelheiten in unserem Datensatz aufführen wollen, machen wir durch `foaf:page` deutlich, dass man auf der Wikiseite näheres zu der hier beschriebenen Person finden kann. Dafür dublizieren wir den URL der französischen Wikipedia Seite und zeichnen ihn entsprechend aus:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: 14.08.1820, Villers-en-Vexin
   Gestorben: 05.12.1887, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

So, nun haben wir es fast geschafft. Bleiben noch die Lebendaten und der Beruf, die bisher "nur" in menschenlesbarer Form im Eintrag existieren.

Um **Geburts- und Sterbedatum** maschienenlesbar auszuzeichnen, verwenden wir `gndo:dateOfBirth` und `gndo:dateOfDeath`. Da es sich um Datumsangaben handelt, weisen wir zusätzlich zur menschenlesbaren Form des Datums ein obligatorisches ISO-Datum via `@iso-date` aus:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, Villers-en-Vexin
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, Paris
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

**Geburts- und Sterbeorte** zeichnen wir mittels `gndo:placeOfBirth` und `gndo:placeOfDeath` aus. Auch hier recherchieren wir noch zusätzlich die entsprechenden *Normdateneinträge für Orte in der GND* und weisen diese unter Verwendung der entsprechenden GND-URIs via `@gndo:ref` mit aus, zumindest, sofern wir hierzu fündig werden; ich hatte bei "Villers-en-Vexin" keinen Erfolg und konnte den Ort über die GND nicht identifizieren. Nun sollten wir alle Lebensdaten erschlossen haben:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer Politiker
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, <gndo:placeOfDeath 
      gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Als letzte kümmern wir uns noch um die Auszeichnung des Berufs. Ernest Chevalier war Politiker und das hauptberuflich. Wir verwenden `gndo:professionOrOccupation` und fügen das Attribut `@gndo:type` mit dem Wert "**significant**" hinzu, um auch diese Information menschen- und maschienenlesbar zu dokumentieren; `@gndo:type` dient nämlich in diesem Kontext zu Klassifikation einer Beschäftigung als **Hauptbeschäftigung**. Für den Beruf "Politiker" gibt es einen Deskriptor in den GND Sachbegriffen und zwar "https://d-nb.info/gnd/4046517-2". Diesen geben wir wieder unter Verwendung von `@gndo:ref` mit an:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> (<gndo:gender 
      gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>)
   Französischer <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" 
      gndo:type="significant">Politiker</gndo:professionOrOccupation>
   Erwähnt in "<gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086"
      gndo:ref="https://d-nb.info/gnd/118641166X"><gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>: <title>Vergessene Zusammenhänge</title>, <date>2003</date>, S. 139</gndo:publication>". 
   Wikipedia Seite: <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   Zusätzliche Informationen: <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   Geboren: <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth>, <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   Gestorben: <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>, <gndo:placeOfDeath 
      gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <revision status="opened">
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

<span class="emph">Geschafft</span>! Unser Eintrag zu Ernest Chevalier ist abgeschlossen und (zumindest für die Belange dieses Tutorials) mit genügend Daten angereichert. Den Abschluss dokumentieren wir noch in der Revisionsbeschreibung mittels `change` und markieren den Eintrag mit Hilfe des `@status` Attributs als Kandidaten für die Übertragung in die GND:

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname>, 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName> 
   <!-- Weitere Inhalte der Übersichtlichkeit halber ausgeblendet -->
   <revision status="opened">
      <change when="2023-02-10" who="US" status="candidate">Bearbeitung abgeschlossen; bereit für die Übernahme in die GND.</change>
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Man muss die Daten nicht - wie in diesem Tutorial geschehen - als *Text* strukturieren. Man kann sich auch für eine eher *strukturiertere Form* entscheiden. Dafür nehmen wir einfach unser Beispiel und löschen alle textlichen Informationen, die in einer strukturierten Form eher nicht enthalten sein sollten: 

```xml
<person xml:id="ernest_chevalier" agency="create">
   <gndo:preferredName>
      <gndo:surname>Chevalier</gndo:surname> 
      <gndo:forename>Ernest</gndo:forename>
   </gndo:preferredName>
   <gndo:dateOfBirth iso-date="1820-08-14">14.08.1820</gndo:dateOfBirth> 
   <gndo:dateOfDeath iso-date="1887-12-05">05.12.1887</gndo:dateOfDeath>
   <gndo:placeOfBirth>Villers-en-Vexin</gndo:placeOfBirth>
   <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4044660-8">Paris</gndo:placeOfDeath>
   <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male">männlich</gndo:gender>
   <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR">Frankreich</gndo:geographicAreaCode>
   <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/4046517-2" gndo:type="significant">Politiker</gndo:professionOrOccupation>
   <gndo:publication role="about" dnb:catalogue="https://d-nb.info/968701086" gndo:ref="https://d-nb.info/gnd/118641166X">
      <gndo:firstAuthor>Mollenhauer, Klaus</gndo:firstAuthor>
      <title>Vergessene Zusammenhänge</title>
      <date>2003</date>, S. 139
   </gndo:publication> 
   <foaf:page>https://fr.wikipedia.org/wiki/Ernest_Chevalier</foaf:page>
   <source>https://fr.wikipedia.org/wiki/Ernest_Chevalier</source>
   <revision status="opened">
      <change when="2023-02-10" who="US" status="candidate">Bearbeitung abgeschlossen; bereit für die Übernahme in die GND.</change>
      <change when="2023-02-10" who="US">Eintrag angelegt.</change>
   </revision>
</person>
```

Wir haben uns nun bei diesem Schritt noch dazu entschieden, einen **geographischen Bezug** für Ernest Chevalier mit aufzunehmen, und zwar *Frankreich*. Dazu verwenden wir `gndo:geographicAreaCode` und weisen einen entsprechenden Deskriptor aus dem [GND Vokabular
                     "Geographic Area Code"](https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR) via `@gndo:term` mit aus.

Und damit sind wir am Ende dieses Tutorial angekommen. Der originale Eintrag zu Chevalier befindet sich übrigens in [`samples/KMG-GND.20230203.0.5.1.xml`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/samples/KMG-GND.20230203.0.5.1.xml) mit der ID `ernest_chevalier`. 

Viel Spaß beim eigenen Anlegen von Personeneinträgen!

<a name="entityxml-examples"></a>
## Beispiele

Dieser Bereich ist Beispielauszeichnungen in entityXML gewidmed. Hier finden sich ganze Beispieleinträge genau so wie kleinere Schnipsel, um einzelne Auszeichnungsstrategien zu verdeutlichen.

<a name="docs_d14e2781"></a>
### Personeneintrag, vollständig

Hier mal ein exemplarischer Personendatensatz, der so ziemlich alle Eigenschaften abbildet.

> **Fun Fact**  
> Bei diesem Datensatz handelt es sich um unseren Testdatensatz für die MARC-Konversion, sprich: Wenn ihr euch hieran haltet, dann könnt ihr davon ausgehen, dass späteres MARC, das aus euren Daten erzeugt wird, sauberes GND-MARC darstellt!


```xml
<person xml:id="p0123456789" gndo:uri="https://d-nb.info/gnd/132010445" agency="merge">
  <foaf:page>https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/uwe-sikora/</foaf:page>
  <gndo:academicDegree>Prof. Dr.</gndo:academicDegree>
  <gndo:acquaintanceshipOrFriendship gndo:ref="https://d-nb.info/gnd/118535307">
      <gndo:forename>Anna</gndo:forename>
      <gndo:surname>Freud</gndo:surname>
      <note>Korrespondenzpartnerin</note>
  </gndo:acquaintanceshipOrFriendship>
  <gndo:affiliation gndo:ref="https://d-nb.info/gnd/2024315-7">Georg-August-Universität Göttingen</gndo:affiliation>
  <gndo:biographicalOrHistoricalInformation>Max Mustermann existiert nicht! Er ist ein Dummy, um alles zu testen!</gndo:biographicalOrHistoricalInformation>
  <gndo:dateOfBirth iso-date="1500-05-10">10.05.1500</gndo:dateOfBirth>
  <gndo:dateOfDeath iso-date="2024-08-06">06.08.2024</gndo:dateOfDeath>
  <gndo:familialRelationship>
      <gndo:forename>Luise</gndo:forename>
      <gndo:surname>Schmidt</gndo:surname>
      <note>Mutter</note>
  </gndo:familialRelationship>
  <gndo:familialRelationship gndo:ref="https://d-nb.info/gnd/4021477-1">
      <gndo:forename>Heinrich</gndo:forename>
      <gndo:surname>Schmidt</gndo:surname>
      <gndo:prefix>von der</gndo:prefix>
      <note>Bruder</note>
  </gndo:familialRelationship>
  <gndo:fieldOfStudy gndo:ref="https://d-nb.info/gnd/1271364182">Modellierung von Daten</gndo:fieldOfStudy>
  <gndo:functionOrRole></gndo:functionOrRole>
  <gndo:gender gndo:term="https://d-nb.info/standards/vocab/gnd/gender#male https://d-nb.info/standards/vocab/gnd/gender#female">divers</gndo:gender>
  <gndo:geographicAreaCode gndo:term="https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE"/>
  <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#9.5p">9.5p Personen zu Soziologie, Gesellschaft, Arbeit, Sozialgeschichte </gndo:gndSubjectCategory>
  <gndo:gndSubjectCategory gndo:term="https://d-nb.info/standards/vocab/gnd/gnd-sc#34.3p">Iwas mit Sport</gndo:gndSubjectCategory>
  <gndo:languageCode gndo:term="http://id.loc.gov/vocabulary/iso639-2/deu">de</gndo:languageCode>
  <gndo:periodOfActivity iso-from="1500" iso-to="2024">1500-2024</gndo:periodOfActivity>
  <gndo:periodOfActivity iso-from="1500" iso-to="2024">
      <label>1500-2024</label>
      <note>Überirdisches Leben</note>
  </gndo:periodOfActivity>
  <gndo:placeOfActivity gndo:ref="https://d-nb.info/gnd/4021477-1" iso-date="2002-10-11">Göttingen</gndo:placeOfActivity>
  <gndo:placeOfActivity iso-from="2003-05" iso-to="2004-06">Göttingen</gndo:placeOfActivity>
  <gndo:placeOfBirth gndo:ref="https://d-nb.info/gnd/4021477-1">Goettingen</gndo:placeOfBirth>
  <gndo:placeOfDeath gndo:ref="https://d-nb.info/gnd/4021477-1">Die Stadt, die Baustellen schafft!</gndo:placeOfDeath>
  <gndo:playedInstrument>Tenor</gndo:playedInstrument>
  <gndo:playedInstrument gndo:ref="https://d-nb.info/gnd/4148299-2">
      <label>Countertenor</label> 
      <note>Stimmlage</note>
  </gndo:playedInstrument>
  <gndo:preferredName>
      <gndo:surname>Mustermann</gndo:surname>, 
      <gndo:forename>Max</gndo:forename>
  </gndo:preferredName>
  <gndo:preferredName type="original" script="Arab" xml:lang="ara">
      <gndo:forename>احمد بن محمد</gndo:forename>
      <gndo:prefix>ال</gndo:prefix>
      <gndo:surname>مرزوقي</gndo:surname>
  </gndo:preferredName>
  <gndo:professionOrOccupation gndo:type="significant" gndo:ref="https://d-nb.info/gnd/4025243-7">Hochschullehrer</gndo:professionOrOccupation>
  <gndo:professionOrOccupation gndo:ref="https://d-nb.info/gnd/1271364182">Datenfuzzi</gndo:professionOrOccupation>
  <gndo:professionOrOccupation>Gummibärchentester</gndo:professionOrOccupation>
  <gndo:pseudonym gndo:ref="https://d-nb.info/gnd/1155094360">Uwe Sikora</gndo:pseudonym>
  <gndo:publication gndo:ref="https://d-nb.info/1290732000"><title>Paradoxien der Demokratie</title><date>2024</date></gndo:publication>
  <gndo:publication>Werk ohne Autor</gndo:publication>
  <gndo:relatesTo gndo:code="beza" gndo:ref="https://d-nb.info/gnd/1155094360">Lehrer von: Uwe Sikora</gndo:relatesTo>
  <gndo:titleOfNobility>Universalfürst von Groß-Göttingen</gndo:titleOfNobility>
  <gndo:variantName>
      <gndo:prefix>von</gndo:prefix>
      <gndo:surname>Musterjunge</gndo:surname>, 
      <gndo:forename>Maximilian</gndo:forename>
      <gndo:counting>III.</gndo:counting>
      <gndo:nameAddition>Graf von Großgöttingen</gndo:nameAddition>
      <gndo:epithetGenericNameTitleOrTerritory>Sonnengeküster</gndo:epithetGenericNameTitleOrTerritory>
  </gndo:variantName>
  <gndo:variantName>
      <gndo:personalName>Mustermann</gndo:personalName>
      <gndo:epithetGenericNameTitleOrTerritory>Der Grimmige</gndo:epithetGenericNameTitleOrTerritory>
      <gndo:nameAddition>von und zu Göttingen</gndo:nameAddition>
  </gndo:variantName>
  <gndo:variantName xml:lang="ara" script="Arab">
      <gndo:personalName>رضا كحالة, عمر</gndo:personalName>
  </gndo:variantName>
  <gndo:variantName>
      <gndo:personalName>b. Sina</gndo:personalName>
  </gndo:variantName>
  <owl:sameAs>http://viaf.org/viaf/45840123</owl:sameAs>
  <owl:sameAs>http://id.loc.gov/rwo/agents/n88010268</owl:sameAs>
  <skos:note>Das hier ist bloß ein Testdatensatz, um alle Felder zu testen, die bei einer Person erschlossen werden können!</skos:note>
  <skos:note gndo:type="internal">Das ist eine interne Anmerkung!</skos:note>
  <source url="https://de.wikipedia.org/wiki/Arnold_von_Jungenfeld">Wikipedia</source>
  <source url="https://www.sub.uni-goettingen.de/kontakt/personen-a-z/personendetails/person/uwe-sikora/">
      <title>Homepage</title>
      <note>Stand: 29.10.2018</note> 
  </source>
  <revision status="opened">
      <change when="2024-05-20" who="US">Datensatz angelegt.</change>
  </revision>
</person>
```

<a name="doc-specs"></a>
## Technische Spezifikation

<a name="specs-elems"></a>
### Elementspezifikationen

Derzeit umfasst das entityXML Schema **118** spezifizierte Elemente.

Die technische Spezifikation für Elemente umfasst folgende Eigenschaften: 

- **Name**: Der Elementname bzw. XML-Tag, der das Element bezeichnet.
- **Equivalents**: Externe Definitionen equivalenter Terme (z.B. in der GNDO).
- **Label**: Ein oder mehrere Labels in Deutsch (und Englisch).
- **Description**: Eine oder mehrere Beschreibungen, die die Semantik des Elements beschreiben.
- **Attributes**: Sofern ein Element Attribute enthalten kann, werden diese hier aufgeführt.
- **Contained by**: Führt die Elemente auf, in denen das entsprechende Element verwendet wird bzw. werden kann.
- **May Contain**: Führt Elemente auf, die innerhalb des entsprechenden Elements verwendet werden bzw. werden können.
- **Content Model**: Abstrahiertes Inhaltsmodell des Elements.
- **Validation**: Falls spezifiziert, werden hier Validierungsregeln (z.B. Schematron Pattern) für das entsprechende Element angegeben.



<a name="any-restricted"></a>
#### ANY ELEMENT (eng gefasst)
|     |     |
| --- | --- |
| **Name** | *Any name except those listed below or associated with a namespace listed below.*  |
| **Label** (de) | ANY ELEMENT (eng gefasst) |
| **Label** (en) | ANY ELEMENT (narrow) |
| **Description** (de) | Ein Element, dessen **QName** einem *custom namespace* angehört und nicht im vorliegenden Schema explizit spezifiziert ist. |
| **Description** (en) | - |
| **Attributes** | [`@<anyAttribute>`](#d17e2331)   |
| **Contained by** | [`<anyElement.narrow>`](#any-restricted)  [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted) |

***Content Model***  
```xml
<content>
   <anyAttribute repeatable="true"/>
   <anyOrder>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
</content>
```


***Permited Namespaces & Names***  

- **ns**: https://sub.uni-goettingen.de/met/standards/entity-xml#
- **ns**: https://sub.uni-goettingen.de/met/standards/entity-store#
- **ns**: https://d-nb.info/standards/elementset/gnd#
- **name**: http://id.loc.gov/ontologies/bibframe/**instanceOf**
- **name**: http://xmlns.com/foaf/0.1/**page**
- **name**: http://purl.org/dc/elements/1.1/**title**
- **name**: http://www.w3.org/2004/02/skos/core#**note**
- **name**: http://www.w3.org/2002/07/owl#**sameAs**
- **name**: http://www.opengis.net/ont/geosparql#**hasGeometry**
- **name**: http://www.w3.org/2003/01/geo/wgs84_pos#**lat**
- **name**: http://www.w3.org/2003/01/geo/wgs84_pos#**long**

---

<p style="margin-bottom:60px"></p>

<a name="any-all"></a>
#### ANY ELEMENT (weit gefasst)
|     |     |
| --- | --- |
| **Name** | *Any name except those listed below or associated with a namespace listed below.*  |
| **Label** (de) | ANY ELEMENT (weit gefasst) |
| **Label** (en) | ANY ELEMENT (broad) |
| **Description** (de) | Ein Element, dessen **QName** einem *custom namespace* angehört. Dieses Element kann im extremsten Fall einen vollständigen Datensatz eines anderen XML-Formats repräsentieren. |
| **Description** (en) | - |
| **Attributes** | [`@<anyAttribute>`](#d17e2331)   |
| **Contained by** | [`<anyElement.broad>`](#any-all)  [`entity`](#entity-record) |
| **May contain** | [`<anyElement.broad>`](#any-all) |

***Content Model***  
```xml
<content>
   <anyAttribute repeatable="true"/>
   <anyOrder>
      <anyElement type="broad" repeatable="true"/>
      <text/>
   </anyOrder>
</content>
```


***Permited Namespaces & Names***  

- **ns**: https://sub.uni-goettingen.de/met/standards/entity-xml#
- **ns**: https://sub.uni-goettingen.de/met/standards/entity-store#
- **ns**: https://d-nb.info/standards/elementset/gnd#

---

<p style="margin-bottom:60px"></p>

<a name="d17e3272"></a>
#### Abgekürzter Name
|     |     |
| --- | --- |
| **Name** | `gndo:abbreviatedName`  |
| **See also** | [gndo:abbreviatedName](https://d-nb.info/standards/elementset/gnd#abbreviatedName), [gndo:abbreviatedNameForTheConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForTheConferenceOrEvent), [gndo:abbreviatedNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForTheCorporateBody), [gndo:abbreviatedNameForThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForThePlaceOrGeographicName), [gndo:abbreviatedNameForTheWork](https://d-nb.info/standards/elementset/gnd#abbreviatedNameForTheWork) |
| **Label** (de) | Abgekürzter Name |
| **Label** (en) | Abbreviated name |
| **Description** (de) | Eine abgekürzte Namensform der dokumentierten Entität. |
| **Description** (en) | An abbreviated Name for the entity encoded. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@xml:lang`](#d17e7157) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`event`](#d17e320)  [`place`](#d17e1265)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6334"></a>
#### Abstract (Metadaten)
|     |     |
| --- | --- |
| **Name** | `abstract`  |
| **Label** (de) | Abstract (Metadaten) |
| **Label** (en) | Abstract (Metadata) |
| **Description** (de) | Eine kurze Beschreibung. |
| **Description** (en) | A short description. |
| **Contained by** | [`agency`](#agency-stmt)  [`metadata`](#collection-metadata)  [`provider`](#data-provider) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5969"></a>
#### Adelstitel
|     |     |
| --- | --- |
| **Name** | `gndo:titleOfNobility`  |
| **See also** | [gndo:titleOfNobility](https://d-nb.info/standards/elementset/gnd#titleOfNobility) |
| **Label** (de) | Adelstitel |
| **Label** (en) | Title of nobility |
| **Description** (de) | Ein Adelsname der dokumentierten Person oder Personengruppe. |
| **Description** (en) | A title of nobility held by a person or family. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)  [`@xml:lang`](#d17e7157) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2942"></a>
#### Adresse
|     |     |
| --- | --- |
| **Name** | `address`  |
| **Label** (de) | Adresse |
| **Label** (en) | address |
| **Description** (de) | Postalische Anschrift. |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3318"></a>
#### Akademischer Grad
|     |     |
| --- | --- |
| **Name** | `gndo:academicDegree`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#academicDegree](https://d-nb.info/standards/elementset/gnd#academicDegree) |
| **Label** (de) | Akademischer Grad |
| **Label** (en) | Academic degree |
| **Description** (de) | Angaben zum akademischen Grad der Person, z.B. "Dr. jur.". |
| **Description** (en) | Description with regard to the academic degree of a person |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.skos.note"></a>
#### Anmerkung
|     |     |
| --- | --- |
| **Name** | `skos:note`  |
| **See also** | [skos:note](https://www.w3.org/2009/08/skos-reference/skos.html#note) |
| **Label** (de) | Anmerkung |
| **Label** (en) | Note |
| **Description** (de) | Eine Anmerkung zum Eintrag. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:type`](#d17e6668)[`@xml:lang`](#d17e7157)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4205"></a>
#### Auflösungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfTermination`  |
| **See also** | [gndo:dateOfTermination](https://d-nb.info/standards/elementset/gnd#dateOfTermination) |
| **Label** (de) | Auflösungsdatum |
| **Label** (en) | Date of termination |
| **Description** (de) | Das Datum, an dem die beschriebene Entität aufgelöst wurde. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@cert`](#d17e6885)  [`@enriched`](#d17e7538)  [`@iso-date`](#attr.iso-date)  [`@iso-notAfter`](#attr.iso-notAfter)[`@iso-notBefore`](#attr.iso-notBefore)   |
| **Contained by** |  |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1449"></a>
#### Ausdrucksform
|     |     |
| --- | --- |
| **Name** | `expression`  |
| **Label** (de) | Ausdrucksform |
| **Label** (en) | Expression |
| **Description** (de) | Ausdrucksform eines Werks nach FRBR. |
| **Description** (en) | Expression of a Work (FRBR). |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:languageCode`](#d17e4843)  [`gndo:relatesTo`](#d17e5757)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`realizationOf`](#d17e1479)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="realizationOf"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4336"></a>
#### Aussteller
|     |     |
| --- | --- |
| **Name** | `gndo:exhibitor`  |
| **See also** | [gndo:exhibitor](https://d-nb.info/standards/elementset/gnd#exhibitor) |
| **Label** (de) | Aussteller |
| **Label** (en) | Exhibitor |
| **Description** (de) | Die Person, Familie oder Körperschaft, die verantwortlich für die Ausstellung ist. |
| **Description** (en) | A person, family, or corporate body in charge of an exhibition. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`event`](#d17e320) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4491"></a>
#### Autor, erste Verfasserschaft
|     |     |
| --- | --- |
| **Name** | `gndo:firstAuthor`  |
| **See also** | [gndo:firstAuthor](https://d-nb.info/standards/elementset/gnd#firstAuthor) |
| **Label** (de) | Autor, erste Verfasserschaft |
| **Label** (en) | First author |
| **Description** (de) | Die Person oder Körperschaft, die verantwortlich für die primäre Verfasserschaft an einem Werk oder einer Veröffentlichung zeichnet. |
| **Description** (en) | A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`gndo:publication`](#d17e5604)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4311"></a>
#### Beiname, Gattungsname, Titulatur, Territorium
|     |     |
| --- | --- |
| **Name** | `gndo:epithetGenericNameTitleOrTerritory`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#epithetGenericNameTitleOrTerritory](https://d-nb.info/standards/elementset/gnd#epithetGenericNameTitleOrTerritory) |
| **Label** (de) | Beiname, Gattungsname, Titulatur, Territorium |
| **Label** (en) | Epithet, generic name, title or territory |
| **Description** (de) | Beiname einer Person, bei dem es sich um ein Epitheton, Titel oder eine Ortsassoziation handelt. |
| **Description** (en) | - |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5488"></a>
#### Beruf oder Beschäftigung
|     |     |
| --- | --- |
| **Name** | `gndo:professionOrOccupation`  |
| **See also** | [gndo:professionOrOccupation](https://d-nb.info/standards/elementset/gnd#professionOrOccupation) |
| **Label** (de) | Beruf oder Beschäftigung |
| **Label** (en) | Profession or occupation |
| **Description** (de) | Angabe zum Beruf, Tätigkeitsbereich o.Ä., der dokumentierten Person. Zulässige Deskriptoren stammen aus der GND-Systematik und entprechen GND-URIs von Sachbegriffen, z.B. https://d-nb.info/gnd/4168391-2 ("Lyriker"). |
| **Description** (en) | A profession or occupation practiced by a person or family |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@gndo:type`](#d17e5520) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:professionOrOccupation">
      <sch:assert test="@gndo:ref" role="info">(Missing Reference to Profession GND Record): It is recommended to provide a
                        GNDO-URI to a GND-Record of this Profession or Occupation.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3635"></a>
#### Beteiligte Person
|     |     |
| --- | --- |
| **Name** | `gndo:contributor`  |
| **Label** (de) | Beteiligte Person |
| **Label** (en) | Contributor |
| **Description** (de) | Eine Person, Familie oder Organisation, die für Beiträge zur Ressource verantwortlich ist. Dies schließt diejenigen ein, deren Arbeit zu einem größeren Werk beigetragen wurde, wie z. B. einem Sammelband, einer fortlaufenden Publikation oder einer anderen Zusammenstellung von Einzelwerken. |
| **Description** (en) | A person, family or organization responsible for making contributions to the resource. This includes those whose work has been contributed to a larger work, such as an anthology, serial publication, or other compilation of individual works. If a more specific role is available, prefer that, e.g. editor, compiler, illustrator |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@gndo:role`](#d17e3656)[`@ref`](#d17e7662)   |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="gndo:role" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5757"></a>
#### Beziehung
|     |     |
| --- | --- |
| **Name** | `gndo:relatesTo`  |
| **Label** (de) | Beziehung |
| **Label** (en) | Relates to |
| **Description** (de) | Beziehung der beschriebenen Entität zu einer anderen Entität, entweder in der GND oder in der gleichen entityXML Ressource. Diese Property hat keine direkte Entsprechung in der GNDO! |
| **Description** (en) | Relation to another Entity in the GND or in the current entityXML resource. This property doesn't have a direct equivalent in the GNDO! |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:code`](#d17e5783)[`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <choice>
      <attribute name="gndo:ref" required="true"/>
      <attribute name="ref" required="true"/>
   </choice>
   <attribute name="gndo:code" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3345"></a>
#### Beziehung, Bekanntschaft, Freundschaft
|     |     |
| --- | --- |
| **Name** | `gndo:acquaintanceshipOrFriendship`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#acquaintanceshipOrFriendship](https://d-nb.info/standards/elementset/gnd#acquaintanceshipOrFriendship) |
| **Label** (de) | Beziehung, Bekanntschaft, Freundschaft |
| **Label** (en) | Acquaintanceship or friendship |
| **Description** (de) | Beziehung zwischen einer Person, Familie oder Körperschaft und einer anderen Person, Familie oder Körperschaft, die nicht näher bestimmt werden kann. |
| **Description** (en) | Relationship between a person, family, or corporate body and another person, family, or corporate body which cannot be specified more closely. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)  [`@xml:lang`](#d17e7157)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`gndo:counting`](#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](#d17e4311)  [`gndo:forename`](#d17e4522)  [`gndo:nameAddition`](#d17e4918)  [`gndo:personalName`](#d17e5276)  [`gndo:prefix`](#d17e5463)  [`gndo:surname`](#d17e5896)  [`note`](#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <choice>
      <attribute name="gndo:ref" required="true"/>
      <attribute name="ref" required="true"/>
   </choice>
   <attribute name="xml:lang"/>
   <anyOrder>
      <choice>
         <element name="gndo:personalName" required="true"/>
         <anyOrder>
            <element name="gndo:forename" required="true"/>
            <element name="gndo:prefix"/>
            <element name="gndo:surname" required="true"/>
            <element name="gndo:counting"/>
            <text/>
         </anyOrder>
      </choice>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
      <element name="note"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.img"></a>
#### Bild
|     |     |
| --- | --- |
| **Name** | `img`  |
| **Label** (de) | Bild |
| **Label** (en) | Image |
| **Description** (de) | Ein Bild bzw. ein URL zu einer Bildressource. |
| **Description** (en) | An image represented by an URL or Path. |
| **Attributes** | [`@height`](#d17e6157)[`@src`](#d17e6132)  [`@width`](#d17e6145)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="src" required="true"/>
   <attribute name="width"/>
   <attribute name="height"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3463"></a>
#### Biographische Angaben
|     |     |
| --- | --- |
| **Name** | `gndo:biographicalOrHistoricalInformation`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#biographicalOrHistoricalInformation](https://d-nb.info/standards/elementset/gnd#biographicalOrHistoricalInformation) |
| **Label** (de) | Biographische Angaben |
| **Label** (en) | bibliographic or historical Information |
| **Description** (de) | Biographische oder historische Angaben über die Entität. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@xml:lang`](#d17e7157) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-section"></a>
#### Daten Bereich
|     |     |
| --- | --- |
| **Name** | `data`  |
| **Label** (de) | Daten Bereich |
| **Label** (en) | data area |
| **Description** (de) | - |
| **Description** (en) | - |
| **Contained by** | [`collection`](#collection) |
| **May contain** | [`list`](#data-list) |

***Content Model***  
```xml
<content>
   <element name="list" required="true" repeatable="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-provider"></a>
#### Datenprovider
|     |     |
| --- | --- |
| **Name** | `provider`  |
| **Label** (de) | Datenprovider |
| **Label** (en) | Dataprovider |
| **Description** (de) | Informationen zum Datenprovider der Datensammlung und der in diesem Rahmen dokumentierten Entitäten. |
| **Description** (en) | Information about the dataprovider of the datacollection and the contained entities. |
| **Attributes** | [`@id`](#d17e6476)  [`@isil`](#d17e6925)   |
| **Contained by** | [`metadata`](#collection-metadata) |
| **May contain** | [`abstract`](#d17e6334)  [`respStmt`](#respStmt)[`title`](#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="id" required="true"/>
   <attribute name="isil"/>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract" required="true"/>
      <element name="respStmt" repeatable="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="collection"></a>
#### Datensammlung
|     |     |
| --- | --- |
| **Name** | `collection`  |
| **Label** (de) | Datensammlung |
| **Label** (en) | Data Collection |
| **Description** (de) | Eine Datensammlung, die Entity Records enthält. Eine Datensammlung kann in einer oder mehrerer Listen organisisert sein, um die Sammlung weiter zu strukturieren. |
| **Description** (en) | A Datacollection containing Entity records. A datacollection can be organised using one or many lists to provide a more detailed structure to the collection. |
| **Contained by** | [`entityXML`](#entity-xml) |
| **May contain** | [`data`](#data-section)[`metadata`](#collection-metadata)   |

***Content Model***  
```xml
<content>
   <element name="metadata" required="true"/>
   <element name="data" required="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2985"></a>
#### Datum
|     |     |
| --- | --- |
| **Name** | `date`  |
| **See also** | [dc:date](http://purl.org/dc/elements/1.1/date) |
| **Label** (de) | Datum |
| **Label** (en) | Date |
| **Description** (de) | Ein Zeitpunkt oder Zeitraum, der mit einem Ereignis im Lebenszyklus der beschriebenen Ressource in Zusammenhang steht. |
| **Description** (en) | A point or period of time associated with an event in the lifecycle of the resource. |
| **Contained by** | [`gndo:publication`](#d17e5604) |

***Content Model***  
```xml
<content>
   <choice/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-xml"></a>
#### EntityXML Ressource
|     |     |
| --- | --- |
| **Name** | `entityXML`  |
| **Label** (de) | EntityXML Ressource |
| **Label** (en) | EntityXML Resource |
| **Description** (de) | Eine entityXML Ressource, die eine Datensammlung oder eine Projektbeschreibung enthält. |
| **Description** (en) | - |
| **Contained by** |  |
| **May contain** | [`collection`](#collection)  [`mapping`](#d17e6226)  [`store:store`](#d17e6745) |

***Content Model***  
```xml
<content>
   <choice>
      <group>
         <choice>
            <element name="collection" required="true"/>
         </choice>
         <element name="mapping"/>
      </group>
      <element name="mapping" required="true"/>
   </choice>
   <element name="store:store"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-record"></a>
#### Entität (Record)
|     |     |
| --- | --- |
| **Name** | `entity`  |
| **Label** (de) | Entität (Record) |
| **Label** (en) | Entity (Record) |
| **Description** (de) | Ein Entitätsdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). `entity` ermöglicht es zudem, ganze Datensätze eines fremden Namespaces zu integrieren. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:type`](#d17e268)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.broad>`](#any-all)[`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e4843)  [`gndo:preferredName`](#d17e5423)  [`gndo:relatesTo`](#d17e5757)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)  [`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <attribute name="gndo:type" required="true"/>
   <choice>
      <group>
         <anyOrder>
            <element name="gndo:homepage" repeatable="true"/>
            <element name="gndo:preferredName"/>
            <element name="gndo:variantName" repeatable="true"/>
            <element name="dc:title"/>
            <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
            <element name="dublicateGndIdentifier" repeatable="true"/>
            <element name="gndo:geographicAreaCode" repeatable="true"/>
            <element name="gndo:gndIdentifier" repeatable="true"/>
            <element name="gndo:gndSubjectCategory" repeatable="true"/>
            <element name="img" repeatable="true"/>
            <element name="gndo:languageCode" repeatable="true"/>
            <element name="gndo:relatesTo" repeatable="true"/>
            <element name="ref" repeatable="true"/>
            <element name="owl:sameAs" repeatable="true"/>
            <element name="skos:note" repeatable="true"/>
            <element name="source" repeatable="true"/>
            <anyElement type="narrow" repeatable="true"/>
            <text/>
         </anyOrder>
         <element name="revision"/>
      </group>
      <anyElement type="broad" required="true"/>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="add-note"></a>
#### Erleuternde Anmerkung
|     |     |
| --- | --- |
| **Name** | `note`  |
| **Label** (de) | Erleuternde Anmerkung |
| **Label** (en) | explanatory note |
| **Description** (de) | Zusätzliche erläuternder Text zur Informationen, die beschrieben wird. |
| **Description** (en) | Additional explanatory text about the information described. |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:organizerOrHost`](#d17e4943)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:playedInstrument`](#prop-playedInstrument)  [`source`](#elem.source) |

***Content Model***  
```xml
<content>
   <anyOrder>
      <text/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4177"></a>
#### Erscheinungszeit
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfPublication`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#dateOfPublication](https://d-nb.info/standards/elementset/gnd#dateOfPublication) |
| **Label** (de) | Erscheinungszeit |
| **Label** (en) | Date of publication |
| **Description** (de) | Erscheinungsdatum der ersten Ausdrucksform des Werks. |
| **Description** (en) | Date of publication of the first expression of a work. |
| **Attributes** | [`@agency`](#d17e7177)  [`@cert`](#d17e6885)  [`@enriched`](#d17e7538)  [`@iso-date`](#attr.iso-date)  [`@iso-notAfter`](#attr.iso-notAfter)[`@iso-notBefore`](#attr.iso-notBefore)   |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e320"></a>
#### Event
|     |     |
| --- | --- |
| **Name** | `event`  |
| **See also** | [gndo:ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent) |
| **Label** (de) | Event |
| **Label** (en) | Event |
| **Description** (de) | Ein temporales Ereignis, wie z.B. eine Veranstaltung, Konferenz. |
| **Description** (en) | An Event as a temporal entity, e.g. a conference. |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:type`](#event-types)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:abbreviatedName`](#d17e3272)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:dateOfConferenceOrEvent`](#d17e4073)  [`gndo:exhibitor`](#d17e4336)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e4843)  [`gndo:organizerOrHost`](#d17e4943)  [`gndo:place`](#entity-place-standard)  [`gndo:preferredName`](#d17e5423)  [`gndo:relatesTo`](#d17e5757)  [`gndo:temporaryName`](#d17e5924)  [`gndo:topic`](#d17e6005)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:dateOfConferenceOrEvent"/>
      <element name="gndo:exhibitor" repeatable="true"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:organizerOrHost" repeatable="true"/>
      <element name="gndo:place" required="true"/>
      <element name="gndo:preferredName" required="true"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4367"></a>
#### Familiäre Beziehung
|     |     |
| --- | --- |
| **Name** | `gndo:familialRelationship`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#familialRelationship](https://d-nb.info/standards/elementset/gnd#familialRelationship) |
| **Label** (de) | Familiäre Beziehung |
| **Label** (en) | Familial relationship |
| **Description** (de) | Eine familiäre Beziehung zwischen einer Person oder Familie und einer anderen Person oder Familie. |
| **Description** (en) | A family relationship between a person or family and another person or family |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)  [`@xml:lang`](#d17e7157)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`gndo:counting`](#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](#d17e4311)  [`gndo:forename`](#d17e4522)  [`gndo:nameAddition`](#d17e4918)  [`gndo:personalName`](#d17e5276)  [`gndo:prefix`](#d17e5463)  [`gndo:surname`](#d17e5896)  [`note`](#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <choice>
      <attribute name="gndo:ref" required="true"/>
      <attribute name="ref" required="true"/>
   </choice>
   <attribute name="xml:lang"/>
   <anyOrder>
      <choice>
         <element name="gndo:personalName" required="true"/>
         <anyOrder>
            <element name="gndo:forename" required="true"/>
            <element name="gndo:prefix"/>
            <element name="gndo:surname" required="true"/>
            <element name="gndo:counting"/>
            <text/>
         </anyOrder>
      </choice>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
      <element name="note"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4421"></a>
#### Form des Werks und der Expression
|     |     |
| --- | --- |
| **Name** | `gndo:formOfWorkAndExpression`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#formOfWorkAndExpression](https://d-nb.info/standards/elementset/gnd#formOfWorkAndExpression) |
| **Label** (de) | Form des Werks und der Expression |
| **Label** (en) | Form of work and expression |
| **Description** (de) | ... |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4550"></a>
#### Funktion oder Rolle
|     |     |
| --- | --- |
| **Name** | `gndo:functionOrRole`  |
| **See also** | [gndo:functionOrRole](https://d-nb.info/standards/elementset/gnd#functionOrRole) |
| **Label** (de) | Funktion oder Rolle |
| **Label** (de) | Function or role |
| **Description** (de) | Anm.: Seit 2017 aus der GND entfernt! |
| **Description** (de) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:term`](#d17e7026) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="agency-stmt"></a>
#### GND-Agentur
|     |     |
| --- | --- |
| **Name** | `agency`  |
| **Label** (de) | GND-Agentur |
| **Label** (en) | GND-agency |
| **Description** (de) | Informationen zur GND-Agentur, die die Datensammlung bearbeitet. |
| **Description** (en) | Information about the GND-agency working on the datacollection. |
| **Attributes** | [`@isil`](#d17e6925)   |
| **Contained by** | [`metadata`](#collection-metadata) |
| **May contain** | [`abstract`](#d17e6334)  [`respStmt`](#respStmt)[`title`](#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="isil" required="true"/>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract"/>
      <element name="respStmt" repeatable="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4693"></a>
#### GND-Identifier
|     |     |
| --- | --- |
| **Name** | `gndo:gndIdentifier`  |
| **Label** (de) | GND-Identifier |
| **Label** (en) | GND-Identifier |
| **Description** (de) | Der GND-Identifier eines in der GND eingetragenen Normdatensatzes sofern vorhanden |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@cert`](#d17e6885)[`@enriched`](#d17e7538)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:gndIdentifier" role="error">
      <sch:assert test="matches(./text(), '^([\w\d-])+$')">
         <sch:name/>(Invalid content): <sch:name/> must contain just one single GND-ID (eg. "118602802" or "2148150-7")!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="dublicates"></a>
#### GND-Identifier einer Dublette
|     |     |
| --- | --- |
| **Name** | `dublicateGndIdentifier`  |
| **Label** (de) | GND-Identifier einer Dublette |
| **Label** (en) | GND-Identifier of a dublicate |
| **Description** (de) | Der GND-Identifier einer möglichen Dublette. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@cert`](#d17e6885)[`@enriched`](#d17e7538)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:dublicateGndIdentifier" role="error">
      <sch:assert test="matches(./text(), '^([\w\d-])+$')">
         <sch:name/>(Invalid content): <sch:name/> must contain a valid GND-ID (eg. "118602802" or "2148150-7")!</sch:assert>
      <sch:assert test="parent::element()[@gndo:uri]">
         <sch:name/>(Missing GND-URI): It is mandatory to provide a GND-URI of the Entity recorded if <sch:name/> is used!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4734"></a>
#### GND-Sachgruppe
|     |     |
| --- | --- |
| **Name** | `gndo:gndSubjectCategory`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#gndSubjectCategory](https://d-nb.info/standards/elementset/gnd#gndSubjectCategory) |
| **Label** (de) | GND-Sachgruppe |
| **Label** (en) | GND subject category |
| **Description** (de) | Term aus dem GND-Sachgruppen Vokabular (ehemals SWD) |
| **Description** (en) | Descriptor from GND Subject Categories Vocabulary (ehemals SWD) |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:term`](#d17e7026)[`@xml:lang`](#d17e7157)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:gndSubjectCategory/@gndo:term" role="error">
      <sch:assert test="matches(., '(http|https)://d-nb.info/standards/vocab/gnd/gnd-sc#(.+)')"> (Missing or wrong vocabulary term): It
                        is mandatory to provide an URI Descriptor from the [GND Sachgruppen Vocabulary](https://d-nb.info/standards/vocab/gnd/gnd-sc)!
                    </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-dateOfBirth"></a>
#### Geburtsdatum (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfBirth`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#dateOfBirth](https://d-nb.info/standards/elementset/gnd#dateOfBirth) |
| **Label** (de) | Geburtsdatum (Person) |
| **Label** (en) | Date of birth (Person) |
| **Description** (de) | Das Geburtsdatum der Person. |
| **Description** (en) | The birth date of a person. This can be a year, a year-month combination or a full date. |
| **Attributes** | [`@agency`](#d17e7177)  [`@cert`](#d17e6885)  [`@enriched`](#d17e7538)  [`@iso-date`](#attr.iso-date)  [`@iso-notAfter`](#attr.iso-notAfter)[`@iso-notBefore`](#attr.iso-notBefore)   |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfBirth"></a>
#### Geburtsort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfBirth`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfBirth](https://d-nb.info/standards/elementset/gnd#placeOfBirth) |
| **Label** (de) | Geburtsort (Person) |
| **Label** (en) | Place of Birth (Person) |
| **Description** (de) | Der Geburtsort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfBirth">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via
                        `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1265"></a>
#### Geografikum
|     |     |
| --- | --- |
| **Name** | `place`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName) |
| **Label** (de) | Geografikum |
| **Label** (en) | Place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:type`](#d17e1371)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`geo:hasGeometry`](#d17e3150)  [`gndo:abbreviatedName`](#d17e3272)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](#d17e4775)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e4843)  [`gndo:place`](#entity-place-standard)  [`gndo:precedingPlaceOrGeographicName`](#d17e5302)  [`gndo:preferredName`](#d17e5423)  [`gndo:relatesTo`](#d17e5757)  [`gndo:succeedingPlaceOrGeographicName`](#d17e5866)  [`gndo:temporaryName`](#d17e5924)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="geo:hasGeometry"/>
      <element name="gndo:hierarchicalSuperiorOfPlaceOrGeographicName"/>
      <element name="gndo:place"/>
      <element name="gndo:precedingPlaceOrGeographicName"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="gndo:succeedingPlaceOrGeographicName"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:place[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3192"></a>
#### Geographische Breite
|     |     |
| --- | --- |
| **Name** | `wgs84:lat`  |
| **Label** (de) | Geographische Breite |
| **Description** (de) | Geographische Breite in der Dezimalschreibweise. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538) |
| **Contained by** | [`geo:hasGeometry`](#d17e3150) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3205"></a>
#### Geographische Länge
|     |     |
| --- | --- |
| **Name** | `wgs84:long`  |
| **Label** (de) | Geographische Länge |
| **Description** (de) | Geographische Länge in der Dezimalschreibweise. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538) |
| **Contained by** | [`geo:hasGeometry`](#d17e3150) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.geographicAreaCode"></a>
#### Geographischer Schwerpunkt
|     |     |
| --- | --- |
| **Name** | `gndo:geographicAreaCode`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#geographicAreaCode](https://d-nb.info/standards/elementset/gnd#geographicAreaCode) |
| **Label** (de) | Geographischer Schwerpunkt |
| **Label** (en) | Geographic Area Code |
| **Description** (de) | Ländercode(s) der Staat(en), denen die Entität zugeordnet werden kann (z.B. Lebensmittelpunkt bzw. Schwerpunkt ihres Wirkens bei Personen). Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, muss via `@gndo:term` ein Deskriptor aus dem GND Area Code Vokabular (https://d-nb.info/standards/vocab/gnd/geographic-area-code) angegeben werden! |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:term`](#d17e6949) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
                <!--<sch:rule context="gndo:geographicAreaCode" role="error">
                    <sch:assert test="@gndo:term or @term">
                        (Missing Code): It is mandatory to provide either a `@gndo:term` or a custom code via `@code` on `<sch:name/>`!
                    </sch:assert>
                </sch:rule>-->
                <!--<sch:rule context="gndo:geographicAreaCode/@term" role="warning">
                    <sch:assert test=". = 'ZZ'">
                        (Custom Terms will be ignored): Custom Terms in `@<sch:name/>` on gndo:geographicAreaCode will be handled by the GND-Agency as "unknown Geographic Area"!
                    </sch:assert>
                </sch:rule>-->
   <sch:rule context="gndo:geographicAreaCode/@gndo:term" role="error">
      <sch:assert test="matches(., '(http|https)://d-nb.info/standards/vocab/gnd/geographic-area-code#(.+)')"> (Missing vocabulary
                        term): It is mandatory to provide an URI Descriptor from the [GND Area Code
                        Vocabulary](https://d-nb.info/standards/vocab/gnd/geographic-area-code). </sch:assert>
   </sch:rule>
   <sch:rule context="gndo:geographicAreaCode" role="warning">
      <sch:let name="term" value="@gndo:term"/>
      <sch:let name="banned-values"
               value="('XA', 'XB', 'XC', 'XD', 'XE', 'XH', 'XI', 'XK', 'XL', 'XM', 'XN', 'XP', 'XQ', 'XR', 'XS', 'XT', 'XU', 'XV', 'XW', 'XX', 'XY', 'XZ')"/>
      <sch:assert test="not(tokenize($term, '#')[2] = $banned-values)">(Unsupported Area Code): This Area Code will be ignored during the process of MARC21 Templating since it is missing a country.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3150"></a>
#### Geometry eines geographischen Features
|     |     |
| --- | --- |
| **Name** | `geo:hasGeometry`  |
| **See also** | [https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_property_geohasgeometry](https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html#_property_geohasgeometry) |
| **Label** (de) | Geometry eines geographischen Features |
| **Label** (en) | geometry of a geographic feature |
| **Attributes** | [`@geo:source`](#d17e3174)   |
| **Contained by** | [`place`](#d17e1265) |
| **May contain** | [`wgs84:lat`](#d17e3192)  [`wgs84:long`](#d17e3205) |

***Content Model***  
```xml
<content>
   <attribute name="geo:source"/>
   <element name="wgs84:lat" required="true"/>
   <element name="wgs84:long" required="true"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="geo:hasGeometry">
      <sch:assert test="@geo:source" role="warning">(Missing source): It is strongly recommended to provide an URL to the source of the
                        encoded coordinates!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="wgs84:long | wgs84:lat">
      <sch:assert test="matches(text(), '(\+|\-)?\d{1,3}\.\d{5,6}')" role="error">(Wrong value encoding): The value of "<sch:name/>"
                        must match '(\+|\-)?\d{1,3}\.\d{5,6}'! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4581"></a>
#### Geschlecht
|     |     |
| --- | --- |
| **Name** | `gndo:gender`  |
| **See also** | [gndo:gender](https://d-nb.info/standards/elementset/gnd#gender) |
| **Label** (de) | Geschlecht |
| **Label** (en) | gender |
| **Description** (de) | Angaben zum Geschlecht einer Person. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:term`](#d17e4603) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:term" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="prop-playedInstrument"></a>
#### Gespieltes Instrument
|     |     |
| --- | --- |
| **Name** | `gndo:playedInstrument`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#playedInstrument](https://d-nb.info/standards/elementset/gnd#playedInstrument) |
| **Label** (de) | Gespieltes Instrument |
| **Label** (en) | Played instrument |
| **Description** (de) | Ein Instrument, dass durch die beschriebene Entität gespielt wird. |
| **Description** (en) | An instrument played by the entity described. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`label`](#d17e6206)  [`note`](#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <choice>
      <text/>
      <group>
         <element name="label" required="true"/>
         <element name="note" required="true"/>
      </group>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.owl.sameAs"></a>
#### Gleiche Entität
|     |     |
| --- | --- |
| **Name** | `owl:sameAs`  |
| **See also** | [https://www.w3.org/TR/owl-ref/#sameAs-def](https://www.w3.org/TR/owl-ref/#sameAs-def) |
| **Label** (de) | Gleiche Entität |
| **Label** (en) | Same as |
| **Description** (de) | HTTP-URI der selben Entität in einem anderen Normdatendienst. |
| **Description** (en) | links an individual to an individual. This statement indicates that two URI references actually refer to the same thing: the individuals have the same "identity". |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4135"></a>
#### Gründungs- und Auflösungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfEstablishmentAndTermination`  |
| **See also** | [gndo:dateOfEstablishment](https://d-nb.info/standards/elementset/gnd#dateOfEstablishment) |
| **Label** (de) | Gründungs- und Auflösungsdatum |
| **Label** (en) | Date of establishment and termination |
| **Description** (de) | Zeitraum, innerhalb dessen die beschriebene Entität bestand. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@iso-from`](#attr.iso-from)  [`@iso-to`](#attr.iso-to) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:dateOfEstablishmentAndTermination">
      <sch:assert test="@iso-from and @iso-to" role="error">(start and end date missing): It is mandatory to provide a star- and
                        endpoint in time to encode a timespan like `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4104"></a>
#### Gründungsdatum
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfEstablishment`  |
| **See also** | [gndo:dateOfEstablishment](https://d-nb.info/standards/elementset/gnd#dateOfEstablishment) |
| **Label** (de) | Gründungsdatum |
| **Label** (en) | Date of establishment |
| **Description** (de) | Das Datum, an dem die beschriebene Entität entstanden ist bzw. gegründet wurde. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@cert`](#d17e6885)  [`@enriched`](#d17e7538)  [`@iso-date`](#attr.iso-date)  [`@iso-notAfter`](#attr.iso-notAfter)[`@iso-notBefore`](#attr.iso-notBefore)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4281"></a>
#### Herausgeber
|     |     |
| --- | --- |
| **Name** | `gndo:editor`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#editor](https://d-nb.info/standards/elementset/gnd#editor) |
| **Label** (de) | Herausgeber |
| **Label** (en) | Editor |
| **Description** (de) | ... |
| **Description** (en) | A person, family, or organization contributing to a resource by revising or elucidating the content, e.g., adding an introduction, notes, or other critical matter. An editor may also prepare a resource for production, publication, or distribution. For major revisions, adaptations, etc., that substantially change the nature and content of the original work, resulting in a new work, see author. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4775"></a>
#### Hierarchisch übergeordnete geographische Einheit
|     |     |
| --- | --- |
| **Name** | `gndo:hierarchicalSuperiorOfPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#hierarchicalSuperiorOfPlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#hierarchicalSuperiorOfPlaceOrGeographicName) |
| **Label** (de) | Hierarchisch übergeordnete geographische Einheit |
| **Label** (en) | Hierarchical superior of place or geographic name |
| **Description** (de) | Eine übergeordnete geographische Einheit (z.B. Körperschaft, Rechtssprechung) der beschriebenen Entität. |
| **Description** (en) | A hierarchically superordinate unit (corporate body, conference, jurisdiction) of the described unit (corporate body, conference, jurisdiction). |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538) |
| **Contained by** | [`place`](#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.homepage"></a>
#### Homepage (GNDO)
|     |     |
| --- | --- |
| **Name** | `gndo:homepage`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#homepage](https://d-nb.info/standards/elementset/gnd#homepage) |
| **Label** (de) | Homepage (GNDO) |
| **Label** (en) | homepage (GNDO) |
| **Description** (de) | URL einer Webpräsenz der Person, z.B. von Wikipedia. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:label`](#d17e6963)[`@iso-date`](#attr.iso-date)   |
| **Contained by** | [`entity`](#entity-record)  [`event`](#d17e320)  [`place`](#d17e1265)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date"/>
   <attribute name="gndo:label"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2962"></a>
#### Homepage (Kontakt)
|     |     |
| --- | --- |
| **Name** | `web`  |
| **Label** (de) | Homepage (Kontakt) |
| **Label** (en) | Homepage (Contact) |
| **Description** (de) | Ein URL zu einer offiziellen oder persönlichen Homepage. |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.ref"></a>
#### Hyperlink
|     |     |
| --- | --- |
| **Name** | `ref`  |
| **Label** (de) | Hyperlink |
| **Label** (en) | Hyperlink |
| **Description** (de) | Ein Link zu einer Webressource, die weitere Informationen über die im Eintrag beschriebene Entität enthält. Der entsprechende URL wird entweder direkt als Text in `ref` oder via `@target` dokumentiert. |
| **Description** (en) | A link to another web ressource containing information about the described entity. |
| **Attributes** | [`@target`](#d17e6517) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`gndo:publication`](#d17e5604)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="target"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:ref[not(@target)]" role="error">
      <sch:assert test="matches(text(), '^(https|http)://(\S+)$')">(No explizit URL): The content of `<sch:name/>` is not a
                        HTTP/HTTPS-URL! This is okay, but then you need to provide an HTTP/HTTPS-URL using `@target`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.idno"></a>
#### Identifier
|     |     |
| --- | --- |
| **Name** | `idno`  |
| **Label** (de) | Identifier |
| **Label** (en) | Identifier |
| **Description** (de) | Ein Identifier für die Ressource, die hiermit beschrieben wird. |
| **Description** (en) | An Identifier of the Resource described. |
| **Attributes** | [`@type`](#d17e6189) |
| **Contained by** | [`gndo:publication`](#d17e5604) |

***Content Model***  
```xml
<content>
   <attribute name="type" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5806"></a>
#### In Beziehung stehendes Werk
|     |     |
| --- | --- |
| **Name** | `gndo:relatedWork`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#relatedWork](https://d-nb.info/standards/elementset/gnd#relatedWork) |
| **Label** (de) | In Beziehung stehendes Werk |
| **Label** (en) | Related work |
| **Description** (de) | ... |
| **Description** (en) | ... |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="instance-of"></a>
#### Instanziierung von
|     |     |
| --- | --- |
| **Name** | `bf:instanceOf`  |
| **See also** | [bf:instantiates](http://bibfra.me/vocab/lite/instantiates) |
| **Label** (de) | Instanziierung von |
| **Label** (en) | Instance of |
| **Description** (de) | Verknüpfung auf ein Werk, das in der dokumentierten Manifestation instanziiert bzw. manifestiert wird. |
| **Description** (en) | Links to a Work the Instance described instantiates or manifests. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`manifestation`](#d17e1518) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="bf:instanceOf/@ref[starts-with(., '#')]">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:let name="target" value="root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::entity:work]" role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work but is
                            "<sch:value-of select="$target/name()"/>"! </sch:assert>
      <!--<sch:report test="." role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work but is "<sch:value-of select="$target/name()"/>"! </sch:report>-->
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="contact-metadata"></a>
#### Kontaktinformationen
|     |     |
| --- | --- |
| **Name** | `contact`  |
| **Label** (de) | Kontaktinformationen |
| **Label** (en) | Contact |
| **Description** (de) | Informationen, die zur Kontaktaufnahme genutzt werden können. |
| **Description** (en) | - |
| **Contained by** | [`respStmt`](#respStmt) |
| **May contain** | [`address`](#d17e2942)  [`mail`](#d17e2902)  [`phone`](#d17e2922)  [`web`](#d17e2962) |

***Content Model***  
```xml
<content>
   <anyOrder>
      <element name="mail" required="true" repeatable="true"/>
      <element name="phone"/>
      <element name="address"/>
      <element name="web"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2845"></a>
#### Kurzbeschreibung (Sammlung)
|     |     |
| --- | --- |
| **Name** | `abstract`  |
| **Label** (de) | Kurzbeschreibung (Sammlung) |
| **Label** (en) | Abstract (Collection) |
| **Description** (de) | Kurzbeschreibung eines Sammlungsabschnitts. |
| **Description** (en) | - |
| **Contained by** | [`list`](#data-list) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="corporate-body-record"></a>
#### Körperschaft
|     |     |
| --- | --- |
| **Name** | `corporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#corporateBody](https://d-nb.info/standards/elementset/gnd#corporateBody) |
| **Label** (de) | Körperschaft |
| **Label** (en) | Corporate Body |
| **Description** (de) | Ein Körperschaftsdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:type`](#d17e720)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`foaf:page`](#elem.foaf.page)  [`gndo:abbreviatedName`](#d17e3272)  [`gndo:affiliation`](#d17e3398)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:languageCode`](#d17e4843)  [`gndo:placeOfBusiness`](#d17e5119)  [`gndo:precedingCorporateBody`](#d17e5335)  [`gndo:preferredName`](#d17e5423)  [`gndo:publication`](#d17e5604)  [`gndo:relatesTo`](#d17e5757)  [`gndo:succeedingCorporateBody`](#d17e5836)  [`gndo:temporaryName`](#d17e5924)  [`gndo:topic`](#d17e6005)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:placeOfBusiness"/>
      <element name="gndo:precedingCorporateBody"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:succeedingCorporateBody"/>
      <element name="gndo:temporaryName" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:corporateBody[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6206"></a>
#### Label
|     |     |
| --- | --- |
| **Name** | `label`  |
| **Label** (de) | Label |
| **Label** (en) | Label |
| **Description** (de) | Ein menschenlesbares Label. |
| **Description** (en) | A humanreadable label. |
| **Contained by** | [`gndo:organizerOrHost`](#d17e4943)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:playedInstrument`](#prop-playedInstrument) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="data-list"></a>
#### Liste (Daten Bereich)
|     |     |
| --- | --- |
| **Name** | `list`  |
| **Label** (de) | Liste (Daten Bereich) |
| **Label** (en) | List (Data Area) |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@id`](#d17e7042)   |
| **Contained by** | [`data`](#data-section) |
| **May contain** | [`abstract`](#d17e2845)  [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`title`](#d17e6862)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="id"/>
   <element name="title"/>
   <element name="abstract"/>
   <anyOrder>
      <element name="corporateBody" repeatable="true"/>
      <element name="entity" repeatable="true"/>
      <element name="event" repeatable="true"/>
      <element name="expression" repeatable="true"/>
      <element name="manifestation" repeatable="true"/>
      <element name="person" repeatable="true"/>
      <element name="place" repeatable="true"/>
      <element name="subjectHeading" repeatable="true"/>
      <element name="work" repeatable="true"/>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:list/element()[@xml:id][@agency][not(@agency = 'ignore')]"
             role="error">
      <sch:assert test="entity:revision">(Missing revision description): If you are going to exchange your data with a GND
                                Agency you need to provide a revision description for the Entry encoded!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2902"></a>
#### Mailadresse
|     |     |
| --- | --- |
| **Name** | `mail`  |
| **Label** (de) | Mailadresse |
| **Label** (en) | Mailaddress |
| **Description** (de) | Email Adresse zur Kontaktaufnahme. |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1518"></a>
#### Manifestation
|     |     |
| --- | --- |
| **Name** | `manifestation`  |
| **Label** (de) | Manifestation |
| **Label** (en) | Manifestation |
| **Description** (de) | Manifestation einer Ausdrucksform eines Werks nach FRBR. |
| **Description** (en) | Manifestation of an Expression of a Work (FRBR). |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`bf:instanceOf`](#instance-of)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`embodimentOf`](#d17e1553)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:languageCode`](#d17e4843)  [`gndo:relatesTo`](#d17e5757)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="bf:instanceOf"/>
      <element name="embodimentOf"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6226"></a>
#### Mapping
|     |     |
| --- | --- |
| **Name** | `mapping`  |
| **Label** (de) | Mapping |
| **Label** (en) | Mapping |
| **Description** (de) | Mapping Sektion, in der Labels und äquivalente Terme aus der GND dokumentiert werden können. |
| **Description** (en) | Mapping section to document labels and equivalent GND terms. |
| **Contained by** | [`entityXML`](#entity-xml) |
| **May contain** | [`mappingLabel`](#d17e6259)[`metadata`](#collection-metadata)   |

***Content Model***  
```xml
<content>
   <element name="metadata"/>
   <element name="mappingLabel" required="true" repeatable="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6259"></a>
#### Mapping-Label
|     |     |
| --- | --- |
| **Name** | `mappingLabel`  |
| **Label** (de) | Mapping-Label |
| **Label** (en) | Mapping-Label |
| **Description** (de) | Ein Labels, dass auf einen äquivalenten Terme aus der GND gemapped wird. |
| **Description** (en) | A labels being mapped to an equivalent GND term. |
| **Attributes** | [`@name`](#d17e7229)  [`@type`](#d17e7241)  [`@when`](#d17e7724)   |
| **Contained by** | [`mapping`](#d17e6226) |
| **May contain** | [`term`](#d17e6829) |

***Content Model***  
```xml
<content>
   <attribute name="name" required="true"/>
   <attribute name="type"/>
   <attribute name="when"/>
   <element name="term" repeatable="true"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="collection-metadata"></a>
#### Metadaten
|     |     |
| --- | --- |
| **Name** | `metadata`  |
| **Label** (de) | Metadaten |
| **Label** (en) | Metadata |
| **Description** (de) | Bereich, in dem grundlegende Metadaten z.B. zu einer Datensammlung oder einem Mapping (z.B. eine Kurzbeschreibung der Sammlung, Angaben zum Datenlieferant, eine Revisionsbeschreibung der Sammlung etc.) dokumentiert werden können. |
| **Description** (en) | Section where Metadata e.g. with regard to the Data Collection is described, e.g. a brief description of the collection, information about the data provider, revision descriptions etc. |
| **Contained by** | [`collection`](#collection)  [`mapping`](#d17e6226) |
| **May contain** | [`abstract`](#d17e6334)  [`agency`](#agency-stmt)  [`provider`](#data-provider)  [`respStmt`](#respStmt)  [`revision`](#revision)[`title`](#d17e6862)   |

***Content Model***  
```xml
<content>
   <anyOrder>
      <element name="title" required="true"/>
      <element name="abstract" required="true"/>
      <element name="respStmt" repeatable="true"/>
      <element name="provider" required="true"/>
      <element name="agency"/>
      <element name="revision" required="true"/>
   </anyOrder>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5836"></a>
#### Nachfolgende Körperschaft (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:succeedingCorporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#succeedingCorporateBody](https://d-nb.info/standards/elementset/gnd#succeedingCorporateBody) |
| **Label** (de) | Nachfolgende Körperschaft (Körperschaft) |
| **Label** (en) | Succeeding corporate body (Corporal Body) |
| **Description** (de) | Eine Nachfolgeinstitution der Körperschaft. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5866"></a>
#### Nachfolgendes Geografikum
|     |     |
| --- | --- |
| **Name** | `gndo:succeedingPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#succeedingPlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#succeedingPlaceOrGeographicName) |
| **Label** (de) | Nachfolgendes Geografikum |
| **Label** (en) | Succeeding place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`place`](#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5896"></a>
#### Nachname
|     |     |
| --- | --- |
| **Name** | `gndo:surname`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#surname](https://d-nb.info/standards/elementset/gnd#surname) |
| **Label** (de) | Nachname |
| **Label** (en) | Surname |
| **Description** (de) | Der Nach- oder Familienname, unter dem eine Person bekannt ist. |
| **Description** (en) | Last name of a person |
| **Attributes** | [`@cert`](#d17e6885) |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <attribute name="cert"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6400"></a>
#### Name (Verantworlichkeit)
|     |     |
| --- | --- |
| **Name** | `name`  |
| **Label** (de) | Name (Verantworlichkeit) |
| **Label** (en) | Name (Responsibility Statement) |
| **Description** (de) | Name der verantwortlichen Person oder Institution. |
| **Description** (en) | Name of a person or institution taking a certain responsibility with regard to a datacollection. |
| **Contained by** | [`respStmt`](#respStmt) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5463"></a>
#### Namenspräfix
|     |     |
| --- | --- |
| **Name** | `gndo:prefix`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#prefix](https://d-nb.info/standards/elementset/gnd#prefix) |
| **Label** (de) | Namenspräfix |
| **Label** (en) | Prefix |
| **Description** (de) | Namensteil, der dem Namen vorangestellt sind (z.B. Adelskennzeichnungen, "von", "zu" etc.). |
| **Description** (en) | Part of a name, which is prefixed. |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4918"></a>
#### Namenszusatz
|     |     |
| --- | --- |
| **Name** | `gndo:nameAddition`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#nameAddition](https://d-nb.info/standards/elementset/gnd#nameAddition) |
| **Label** (de) | Namenszusatz |
| **Label** (en) | Name addition |
| **Description** (de) | Zusätzliches Element des Namens, unter dem eine Person bekannt ist, z.B. "Graf von Wallmoden". |
| **Description** (en) | - |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3502"></a>
#### Oberbegriff
|     |     |
| --- | --- |
| **Name** | `gndo:broaderTerm`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#broaderTerm](https://d-nb.info/standards/elementset/gnd#broaderTerm), [https://d-nb.info/standards/elementset/gnd#broaderTermGeneral](https://d-nb.info/standards/elementset/gnd#broaderTermGeneral), [https://d-nb.info/standards/elementset/gnd#broaderTermGeneric](https://d-nb.info/standards/elementset/gnd#broaderTermGeneric), [https://d-nb.info/standards/elementset/gnd#broaderTermInstantial](https://d-nb.info/standards/elementset/gnd#broaderTermInstantial), [https://d-nb.info/standards/elementset/gnd#broaderTermPartitive](https://d-nb.info/standards/elementset/gnd#broaderTermPartitive), [https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement](https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement) |
| **Label** (de) | Oberbegriff |
| **Label** (en) | Broader term |
| **Description** (de) | Oberbegriff bzw. Oberklasse der beschriebenen Entität. |
| **Description** (en) | A Broader term for the concept the described entity is an instance of. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@gndo:type`](#d17e3541)[`@ref`](#d17e7662)  [`@xml:lang`](#d17e7157)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="gndo:type"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:broaderTerm">
      <sch:assert test="@gndo:ref" role="error">broader-term.1: (Descriptor URI missing): It is mandatory to provide an URI referenze of
                        a GND-Descriptor via `@gndo:ref` for `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="entity-place-standard"></a>
#### Ort
|     |     |
| --- | --- |
| **Name** | `gndo:place`  |
| **See also** | [gndo:place](https://d-nb.info/standards/elementset/gnd#place) |
| **Label** (de) | Ort |
| **Label** (en) | Place |
| **Description** (de) | Ein Land, Staat, Provinz, Ort etc., wo die beschriebene Entität angesiedelt ist, stattfand o.Ä. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND-Systematik für diesen Ort erfasst werden. |
| **Description** (en) | A country, state, province, etc., or place where the entity described is placed, happend etc. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`event`](#d17e320)  [`place`](#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:place">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for the encoded Place
                        in `<sch:name/>` via `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record"></a>
#### Person (Record)
|     |     |
| --- | --- |
| **Name** | `person`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#Person](https://d-nb.info/standards/elementset/gnd#Person) |
| **Label** (de) | Person (Record) |
| **Label** (en) | Person (Record) |
| **Description** (de) | Ein Personendatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:type`](#person-types)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`foaf:page`](#elem.foaf.page)  [`gndo:academicDegree`](#d17e3318)  [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:affiliation`](#d17e3398)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:fieldOfStudy`](#d17e4451)  [`gndo:functionOrRole`](#d17e4550)  [`gndo:gender`](#d17e4581)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:languageCode`](#d17e4843)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:playedInstrument`](#prop-playedInstrument)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:professionOrOccupation`](#d17e5488)  [`gndo:pseudonym`](#d17e5556)  [`gndo:publication`](#d17e5604)  [`gndo:relatesTo`](#d17e5757)  [`gndo:titleOfNobility`](#d17e5969)  [`gndo:topic`](#d17e6005)  [`gndo:variantName`](#person-record-variantName)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="foaf:page" repeatable="true"/>
      <element name="gndo:acquaintanceshipOrFriendship" repeatable="true"/>
      <element name="gndo:academicDegree"/>
      <element name="gndo:affiliation" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfBirth"/>
      <element name="gndo:dateOfDeath"/>
      <element name="gndo:periodOfActivity" repeatable="true"/>
      <element name="gndo:familialRelationship" repeatable="true"/>
      <element name="gndo:fieldOfStudy" repeatable="true"/>
      <element name="gndo:functionOrRole" repeatable="true"/>
      <element name="gndo:gender"/>
      <element name="gndo:placeOfActivity" repeatable="true"/>
      <element name="gndo:placeOfBirth"/>
      <element name="gndo:placeOfDeath"/>
      <element name="gndo:playedInstrument" repeatable="true"/>
      <choice>
         <element name="gndo:preferredName"/>
         <group>
            <element name="gndo:preferredName" required="true"/>
            <element name="gndo:preferredName" required="true"/>
         </group>
      </choice>
      <element name="gndo:professionOrOccupation" repeatable="true"/>
      <element name="gndo:publication" repeatable="true"/>
      <element name="gndo:pseudonym" repeatable="true"/>
      <element name="gndo:titleOfNobility" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person[not(@gndo:uri)]">
      <sch:assert test="gndo:geographicAreaCode" role="error">(Geographic Area Code is missing): It is mandatory to provide a
                        `gndo:geographicAreaCode` for a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person/gndo:periodOfActivity">
      <sch:report test="parent::node()[gndo:dateOfBirth or gndo:dateOfDeath]" role="info">(Period of Activity and Dates of Living): Providing gndo:periodOfActivity and 
                        Dates of Living (gndo:dateOfBirth and/or gndo:dateOfDeath) in combination is not supported by the GND-Specifications and thus could lead to problems during dataimport!</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person/gndo:preferredName[@type='original']">
      <sch:assert test="parent::node()[gndo:preferredName[not(@type)]]" role="error">(No corresponding standard preferred name) You are only allowed to document an original form of a preferred name if you already encoded a standard preferred name!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:person/gndo:preferredName[not(@type)]">
      <sch:report test="count(parent::node()/gndo:preferredName[not(@type)]) = 2"
                  role="error">(Original preferred Name not marked) If you provide two preferred names one must be marked as the original form using `@type='original'`!</sch:report>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5276"></a>
#### Persönlicher Name
|     |     |
| --- | --- |
| **Name** | `gndo:personalName`  |
| **See also** | [gndo:personalName](https://d-nb.info/standards/elementset/gnd#personalName) |
| **Label** (de) | Persönlicher Name |
| **Label** (en) | Personal name |
| **Description** (de) | Ein Name, unter dem die Person gemeinhin bekannt ist. Ein persönlicher Name wird dann angegeben, wenn die Person nicht durch einen Vor- oder Nachname benannt wird, z.B. "Aristoteles" |
| **Description** (en) | - |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5556"></a>
#### Pseudonym
|     |     |
| --- | --- |
| **Name** | `gndo:pseudonym`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#pseudonym](https://d-nb.info/standards/elementset/gnd#pseudonym) |
| **Label** (de) | Pseudonym |
| **Label** (en) | Pseudonym |
| **Description** (de) | Ein Pseudonym, unter dem die dokumentierte Person ebenfalls bekannt ist. |
| **Description** (en) | Links a person's real identity to an identity under which one or more persons act, e. g. write, compose or create art, but that is not the person's real name (i. e. a pseudonym). |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:pseudonym">
      <sch:assert test="@gndo:ref or @ref" role="warning">(Missing Reference to Pseudonym Record): It is recommended to provide a
                        GNDO-URI to a GND-Record of this Pseudonym or to a `person[@type='https://d-nb.info/standards/elementset/gnd#Pseudonym']` in
                        this dataset using `@ref`.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5604"></a>
#### Publikation
|     |     |
| --- | --- |
| **Name** | `gndo:publication`  |
| **See also** | [gndo:publication](https://d-nb.info/standards/elementset/gnd#publication) |
| **Label** (de) | Publikation |
| **Label** (en) | publication |
| **Description** (de) | Eine Publikation, die mit der Entität in Zusammenhang steht. Es kann sich hierbei (1) um eigene Titel der Entität (Person), (2) um Titel, die von der Entität (Person) herausgegeben worden sind und auch (3) um Titel über die Entität handeln. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@dnb:catalogue`](#d17e5638)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)  [`@role`](#d17e5652)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`person`](#person-record) |
| **May contain** | [`add`](#d17e2597)  [`date`](#d17e2985)[`gndo:firstAuthor`](#d17e4491)  [`idno`](#elem.idno)  [`ref`](#elem.ref)  [`title`](#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="dnb:catalogue"/>
   <attribute name="role"/>
   <anyOrder>
      <element name="ref"/>
      <element name="idno" repeatable="true"/>
      <choice>
         <anyOrder>
            <element name="gndo:firstAuthor" repeatable="true"/>
            <element name="title" required="true"/>
            <element name="add"/>
            <element name="date" required="true"/>
            <text/>
         </anyOrder>
         <text/>
      </choice>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication">
      <sch:let name="ref" value="substring-after(data(@ref), '#')"/>
      <sch:let name="internal-work-record" value="//entity:work[@xml:id = $ref]"/>
      <sch:assert test="@gndo:ref or @dnb:catalogue or not(empty($internal-work-record))"
                  role="warning">[WARN] To identify a
                        publication it is recommended to provide either a Reference to a Record in the DNB Catalogue (`@dnb:catalogue`) or an URI of a
                        Work Entity in the GND (`@gndo:ref`).</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[@dnb:catalogue]">
      <sch:report test="@agency='remove'" role="warning">[WARN] (NO GND Data) Your request concerns data that is part of the DNB
                        Catalogue. A GND-Agency doesn't have any access to these datasets.</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[not(@role) or @role='author']">
      <sch:report test="gndo:firstAuthor" role="error">(Who is the author?) You didn't use `@role` which implies that the publication's
                        author is actually the entity described! If so you must not use the `author` element again within the publication! Otherwise
                        you need to be explizit about the Role of the entity described using `@role` on the publication!</sch:report>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:publication[@role='about']">
      <sch:assert test="gndo:firstAuthor" role="error">(Missing author) If the publication is ABOUT the entity described you need to
                        encode the primary author of the publication!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.source"></a>
#### Quellenangabe
|     |     |
| --- | --- |
| **Name** | `source`  |
| **Label** (de) | Quellenangabe |
| **Label** (en) | Source |
| **Description** (de) | Angaben zu einer Quelle, die verwendet wurde, um die in diesem Eintrag gegebenen Informationen zu recherchieren. |
| **Description** (en) | A source which was used to identitfy the resource. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@url`](#d17e6713)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |
| **May contain** | [`note`](#add-note)[`title`](#d17e6862)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="url"/>
   <choice>
      <group>
         <anyOrder>
            <element name="title" required="true"/>
            <element name="note"/>
         </anyOrder>
      </group>
      <text/>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="revision"></a>
#### Revisionsbeschreibung
|     |     |
| --- | --- |
| **Name** | `revision`  |
| **Label** (de) | Revisionsbeschreibung |
| **Label** (en) | Revision Description |
| **Description** (de) | Statusinformationen zu einer Ressource (Datensammlung oder einzelner Entitätseintrag), die einzelne Bearbeitungsstände, Fragen oder anderweitige Anmerkungen zur Verarbeitung der entsprechenden Ressource dokumentiert. Die einzelnen Einträge (via `change`) werden ausgehend vom Datum von jung nach alt absteigend sortiert, sprich: Der jüngste Eintrag steht als Erstes in der Revisionsbeschreibung! |
| **Description** (en) | Description containing information about the editing status of the ressource. |
| **Attributes** | [`@status`](#rev-status)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`metadata`](#collection-metadata)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |
| **May contain** | [`change`](#d17e2624) |

***Content Model***  
```xml
<content>
   <attribute name="status" required="true"/>
   <element name="change" repeatable="true"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:revision[@status = 'closed']" role="error">
      <sch:assert test="(entity:change[@status])[1][@status = ('published', 'withdrawn')]">(Missing prerequisites): You can't close a
                        resource if it is not at least published or withdrawn!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:revision[@status ='staged']" role="error">
      <sch:assert test="(entity:change[@status])[1][@status=('candidate', 'approved', 'embargoed', 'submitted', 'published')]">(Missing
                        prerequisites): You can't stage a ressource as long as the ressource is not a candidate, approved or embargoed!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2624"></a>
#### Revisionseintrag
|     |     |
| --- | --- |
| **Name** | `change`  |
| **Label** (de) | Revisionseintrag |
| **Label** (en) | Change |
| **Description** (de) | Zusammenfassende Beschreibung der vorgenommenen Änderung an einer Informationsressource (Sammlung oder Eintrag). Der aktuellste Eintrag steht immer am Anfang der Revisionsbeschreibung. |
| **Description** (en) | Summary of the change made to the informationressource. |
| **Attributes** | [`@status`](#change-status)[`@when`](#d17e7724)  [`@who`](#d17e2640)   |
| **Contained by** | [`revision`](#revision) |

***Content Model***  
```xml
<content>
   <attribute name="when" required="true"/>
   <attribute name="who" required="true"/>
   <attribute name="status"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:change[@when][preceding-sibling::entity:change]"
             role="error">
      <sch:let name="preceeding-change" value="preceding-sibling::entity:change[1]"/>
      <sch:assert test="xs:date($preceeding-change/@when) &gt; xs:date(@when) or xs:date($preceeding-change/@when) = xs:date(@when)">
                        (Youngest always on top!) A change entry of a younger date must be placed on top of older entries aka. the youngest is always
                        the first in the list! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:data//entity:change[@status = ('candidate', 'cleared')]"
             role="warning">
      <sch:assert test="parent::entity:revision/parent::element()[@agency]"> [WARN] (Agency Action recommended): It is recommended to
                        provide information on the action, a GND agency is expected to do with regard to the record! So please use `@agency` on the
                        record to choose an action. </sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:change[@status = ('approved', 'submitted', 'published')]"
             role="error">
      <sch:let name="who-id" value="data(@who)"/>
      <sch:let name="resp-person"
               value="ancestor::entity:collection/entity:metadata//entity:respStmt[@id = $who-id]"/>
      <sch:assert test="$resp-person[parent::entity:agency]"> (Missing authority): Sorry <sch:value-of select="$resp-person/entity:name/text()"/>, you don't have permission to set the resources stage to "<sch:value-of select="data(@status)"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6383"></a>
#### Rolle (Verantwortlichkeit)
|     |     |
| --- | --- |
| **Name** | `resp`  |
| **Label** (de) | Rolle (Verantwortlichkeit) |
| **Label** (en) | Role (Responsibility Statement) |
| **Description** (de) | Eine Rolle, die der Person oder Institution im Rahmen der Bearbeitung einer Datensammlung zugeschrieben wird. |
| **Description** (en) | A role attributed to a person or institution with regard to a datacollection. |
| **Contained by** | [`respStmt`](#respStmt) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1592"></a>
#### Schlagwort
|     |     |
| --- | --- |
| **Name** | `subjectHeading`  |
| **Label** (de) | Schlagwort |
| **Label** (en) | Subject Heading |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:languageCode`](#d17e4843)  [`gndo:relatesTo`](#d17e5757)  [`gndo:topic`](#d17e6005)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5119"></a>
#### Sitz (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfBusiness`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfBusiness](https://d-nb.info/standards/elementset/gnd#placeOfBusiness) |
| **Label** (de) | Sitz (Körperschaft) |
| **Label** (en) | Place of Business (Corporal Body) |
| **Description** (de) | Der (Haupt)sitz der Körperschaft. |
| **Description** (en) | (Main)place of the Business. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4843"></a>
#### Sprachraum
|     |     |
| --- | --- |
| **Name** | `gndo:languageCode`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#languageCode](https://d-nb.info/standards/elementset/gnd#languageCode) |
| **Label** (de) | Sprachraum |
| **Label** (en) | language code |
| **Description** (de) | Code(s) des Sprachraumes, dem die Entität zugeordnet werden kann. Um die Datenqualität zu erhöhen und eine automatisierte Verarbeitung zu erleichtern, kann via @gndo:code zsätzlich ein Deskriptor aus dem LOC ISO 693-2 Language Vokabular (http://id.loc.gov/vocabulary/iso639-2/) angegeben werden! |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:code`](#d17e4864) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:code" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-dateOfDeath"></a>
#### Sterbedatum (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfDeath`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#dateOfDeath](https://d-nb.info/standards/elementset/gnd#dateOfDeath) |
| **Label** (de) | Sterbedatum (Person) |
| **Label** (en) | Date of death (Person) |
| **Description** (de) | Das Sterbedatum der Person. |
| **Description** (en) | The death date of a person. This can be a year, a year-month combination or a full date. |
| **Attributes** | [`@agency`](#d17e7177)  [`@cert`](#d17e6885)  [`@enriched`](#d17e7538)  [`@iso-date`](#attr.iso-date)  [`@iso-notAfter`](#attr.iso-notAfter)[`@iso-notBefore`](#attr.iso-notBefore)   |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="cert"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <group>
         <choice>
            <group>
               <attribute name="iso-notBefore" required="true"/>
               <attribute name="iso-notAfter" required="true"/>
            </group>
            <attribute name="iso-notBefore" required="true"/>
            <attribute name="iso-notAfter" required="true"/>
         </choice>
      </group>
   </choice>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfDeath"></a>
#### Sterbeort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfDeath`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfDeath](https://d-nb.info/standards/elementset/gnd#placeOfDeath) |
| **Label** (de) | Sterbeort (Person) |
| **Label** (en) | Place of death (Person) |
| **Description** (de) | Der Sterbeort der Person. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfDeath">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via
                        `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4451"></a>
#### Studienfach
|     |     |
| --- | --- |
| **Name** | `gndo:fieldOfStudy`  |
| **See also** | [gndo:fieldOfStudy](https://d-nb.info/standards/elementset/gnd#fieldOfStudy) |
| **Label** (de) | Studienfach |
| **Label** (en) | Field of study |
| **Description** (de) | Das Studienfach bzw. -gebiet einer Person. Über `@gndo:ref` kann ein entsprechendes Schlagwort aus der GND mit erfasst werden. |
| **Description** (en) | A person’s field of study. Use `@gndo:ref` to encode an additional GND Subject Category. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:fieldOfStudy">
      <sch:assert test="@gndo:ref" role="warning">[WARN](Missing Subject Category URI) It is recommended to provide an URI of a GND
                        Subject Category using `@gndo:ref`.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2922"></a>
#### Telefon
|     |     |
| --- | --- |
| **Name** | `phone`  |
| **Label** (de) | Telefon |
| **Label** (en) | phone |
| **Description** (de) | Telefonnummer zur Kontaktaufnahme |
| **Description** (en) | - |
| **Contained by** | [`contact`](#contact-metadata) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6829"></a>
#### Term (Mapping)
|     |     |
| --- | --- |
| **Name** | `term`  |
| **Label** (de) | Term (Mapping) |
| **Label** (en) | Term (Mapping) |
| **Description** (de) | Ein Term aus der GND als Deskriptor. |
| **Description** (en) | A GND term as descriptor. |
| **Attributes** | [`@gndo:ref`](#d17e7005)  [`@type`](#d17e7241) |
| **Contained by** | [`mappingLabel`](#d17e6259) |

***Content Model***  
```xml
<content>
   <attribute name="gndo:ref" required="true"/>
   <attribute name="type"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6005"></a>
#### Thema
|     |     |
| --- | --- |
| **Name** | `gndo:topic`  |
| **See also** | [gndo:topic](https://d-nb.info/standards/elementset/gnd#topic) |
| **Label** (de) | Thema |
| **Label** (en) | Topic |
| **Description** (de) | Thema, das sich auf eine Körperschaft, eine Konferenz, eine Person, eine Familie, ein Thema oder ein Werk bezieht. |
| **Description** (en) | Topic that is related to a corporate body, conference, person, family, subject heading or work. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`event`](#d17e320)  [`person`](#person-record)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6862"></a>
#### Titel
|     |     |
| --- | --- |
| **Name** | `title`  |
| **Label** (de) | Titel |
| **Label** (en) | Title |
| **Description** (de) | Ein Titel für das Informationsobjekt, das in diesem Kontext beschrieben wird. |
| **Description** (en) | A title of the information-object, described in this very context. |
| **Contained by** | [`agency`](#agency-stmt)  [`gndo:publication`](#d17e5604)  [`list`](#data-list)  [`metadata`](#collection-metadata)  [`provider`](#data-provider)  [`source`](#elem.source) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3011"></a>
#### Titel (Record)
|     |     |
| --- | --- |
| **Name** | `dc:title`  |
| **See also** | [dc:title](http://purl.org/dc/elements/1.1/title) |
| **Label** (de) | Titel (Record) |
| **Label** (en) | title (record) |
| **Description** (de) | Ein Titel für die Ressource. `dc:title` wird dann zur Bezeichnung einer Entität verwendet, wenn keine Vorzugsbenennung dokumentiert wird bzw. werden soll. |
| **Description** (en) | A title of the record if no preferred or variant name is provided. |
| **Attributes** | [`@xml:lang`](#d17e7157) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="dc:title" role="warning">
      <sch:assert test="parent::node()[not(gndo:preferredName)]">(`dc:title` superfluous): <sch:name/> already contains a preferred
                        name(`gndo:preferredName`) which makes `dc:title` superfluous!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="node()[parent::entity:list][self::entity:*][@gndo:uri or @xml:id][not(gndo:preferredName)][not(gndo:variantName)][not(self::entity:entity)]"
             role="warning">
      <sch:assert test="dc:title">[WARN] (fallback `dc:title` recommended): It is recommended to use `dc:title` to encode a fallback
                        title for `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e2597"></a>
#### Titelzusatz
|     |     |
| --- | --- |
| **Name** | `add`  |
| **Label** (de) | Titelzusatz |
| **Label** (en) | Addition to the title |
| **Description** (de) | Zusätze zum Titel im Rahmen der bibliographischen Angabe. |
| **Description** (en) | Additions to the title in the biblipgraphic reference. |
| **Contained by** | [`gndo:publication`](#d17e5604) |

***Content Model***  
```xml
<content>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-variantName"></a>
#### Variante Namensform (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:variantName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#variantName](https://d-nb.info/standards/elementset/gnd#variantName) |
| **Label** (de) | Variante Namensform (Person) |
| **Label** (en) | Variant Name (Person) |
| **Description** (de) | Eine alternative Namensform, unter der eine Person ebenfalls bekannt ist. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@script`](#d17e7703)  [`@xml:lang`](#d17e7157)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`gndo:counting`](#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](#d17e4311)[`gndo:forename`](#d17e4522)  [`gndo:nameAddition`](#d17e4918)  [`gndo:personalName`](#d17e5276)  [`gndo:prefix`](#d17e5463)  [`gndo:surname`](#d17e5896)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="script"/>
   <choice>
      <element name="gndo:personalName" required="true"/>
      <anyOrder>
         <element name="gndo:forename" required="true"/>
         <element name="gndo:prefix"/>
         <element name="gndo:surname" required="true"/>
         <element name="gndo:counting"/>
         <text/>
      </anyOrder>
   </choice>
   <anyOrder>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
   </anyOrder>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.gndo.variantName.standard"></a>
#### Variante Namensform (Standard)
|     |     |
| --- | --- |
| **Name** | `gndo:variantName`  |
| **See also** | [gndo:variantName](https://d-nb.info/standards/elementset/gnd#variantName), [gndo:variantNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#variantNameForTheCorporateBody), [gndo:variantNameForTheWork](https://d-nb.info/standards/elementset/gnd#variantNameForTheWork), [gndo:variantNameForThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#variantNameForThePlaceOrGeographicName) |
| **Label** (de) | Variante Namensform (Standard) |
| **Label** (en) | Variant Name (Standard) |
| **Description** (de) | Eine alternative Namensform, um die beschriebenen Entität zu bezeichnen. |
| **Description** (en) | A variant name to denominate the documented Entity. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@script`](#d17e7703)[`@xml:lang`](#d17e7157)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`place`](#d17e1265)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="script"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4943"></a>
#### Veranstalter oder Gastgeber
|     |     |
| --- | --- |
| **Name** | `gndo:organizerOrHost`  |
| **See also** | [gndo:organizerOrHost](https://d-nb.info/standards/elementset/gnd#organizerOrHost) |
| **Label** (de) | Veranstalter oder Gastgeber |
| **Label** (en) | Organizer or host |
| **Description** (de) | Eine Person, Familie oder Organisation, die die Ausstellung, Veranstaltung, Konferenz usw. organisiert, die zu einer Ressource geführt hat. |
| **Description** (en) | A person, family, or organization organizing the exhibit, event, conference, etc., which gave rise to a resource. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)   |
| **Contained by** | [`event`](#d17e320) |
| **May contain** | [`label`](#d17e6206)  [`note`](#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <choice>
      <text/>
      <group>
         <element name="label" required="true"/>
         <element name="note" required="true"/>
      </group>
   </choice>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4073"></a>
#### Veranstaltungsdaten
|     |     |
| --- | --- |
| **Name** | `gndo:dateOfConferenceOrEvent`  |
| **See also** | [gndo:dateOfConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#dateOfConferenceOrEvent) |
| **Label** (de) | Veranstaltungsdaten |
| **Label** (en) | Date of conference or event |
| **Description** (de) | Zeitraum, innerhalb dessen das beschriebene Event stattfand. |
| **Description** (en) | Timespan of the event described. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@iso-from`](#attr.iso-from)  [`@iso-to`](#attr.iso-to) |
| **Contained by** | [`event`](#d17e320) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="respStmt"></a>
#### Verantwortlichkeit (Metadaten)
|     |     |
| --- | --- |
| **Name** | `respStmt`  |
| **Label** (de) | Verantwortlichkeit (Metadaten) |
| **Label** (en) | Responsibility Statement (Metadata) |
| **Description** (de) | Angabe zur Rolle einer Person oder Institution in Hinblick auf die Bearbeitung einer Datensammlung. |
| **Description** (en) | A statement about responsibilities of a person or institution with regard to a datacollection. |
| **Attributes** | [`@id`](#d17e7042)   |
| **Contained by** | [`agency`](#agency-stmt)  [`metadata`](#collection-metadata)  [`provider`](#data-provider) |
| **May contain** | [`contact`](#contact-metadata)[`name`](#d17e6400)  [`resp`](#d17e6383)   |

***Content Model***  
```xml
<content>
   <attribute name="id" required="true"/>
   <element name="resp" required="true"/>
   <element name="name" required="true"/>
   <element name="contact"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3422"></a>
#### Verfasser
|     |     |
| --- | --- |
| **Name** | `gndo:author`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#author](https://d-nb.info/standards/elementset/gnd#author) |
| **Label** (de) | Verfasser |
| **Label** (en) | Author |
| **Description** (de) | (Verfasser) |
| **Description** (en) | A person, family, or organization responsible for creating a work that is primarily textual in content, regardless of media type (e.g., printed text, spoken word, electronic text, tactile text) or genre (e.g., poems, novels, screenplays, blogs). Use also for persons, etc., creating a new work by paraphrasing, rewriting, or adapting works by another creator such that the modification has substantially changed the nature and content of the original or changed the medium of expression. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:author">
      <sch:assert test="parent::element()/gndo:firstAuthor" role="error">(Missing first author) You need to use gndo:firstAuthor for the main author of a work. gndo:author is used to describe further authors.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5335"></a>
#### Vorherige Körperschaft (Körperschaft)
|     |     |
| --- | --- |
| **Name** | `gndo:precedingCorporateBody`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#precedingCorporateBody](https://d-nb.info/standards/elementset/gnd#precedingCorporateBody) |
| **Label** (de) | Vorherige Körperschaft (Körperschaft) |
| **Label** (en) | Preceding corporate body (Corporal Body) |
| **Description** (de) | Eine Vorgängerinstitution der Körperschaft. |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5302"></a>
#### Vorheriges Geografikum
|     |     |
| --- | --- |
| **Name** | `gndo:precedingPlaceOrGeographicName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#precedingPlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#precedingPlaceOrGeographicName) |
| **Label** (de) | Vorheriges Geografikum |
| **Label** (en) | Preceding place or geographic name |
| **Description** (de) | - |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`place`](#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4888"></a>
#### Vorlage
|     |     |
| --- | --- |
| **Name** | `gndo:literarySource`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#literarySource](https://d-nb.info/standards/elementset/gnd#literarySource) |
| **Label** (de) | Vorlage |
| **Label** (en) | Literary source |
| **Description** (de) | Die beschriebene Entität ist eine Instanziierung des verknüpften Werks. |
| **Description** (en) | The described entity is a realization of the related work. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e4522"></a>
#### Vorname
|     |     |
| --- | --- |
| **Name** | `gndo:forename`  |
| **See also** | [gndo:forename](https://d-nb.info/standards/elementset/gnd#forename) |
| **Label** (de) | Vorname |
| **Label** (en) | Forename |
| **Description** (de) | Ein oder mehrere Vornamen einer Person. |
| **Description** (en) | One or many prenames of person. |
| **Attributes** | [`@cert`](#d17e6885) |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <attribute name="cert"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-preferredName"></a>
#### Vorzugsbenennung (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:preferredName`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#preferredName](https://d-nb.info/standards/elementset/gnd#preferredName) |
| **Label** (de) | Vorzugsbenennung (Person) |
| **Label** (en) | Preferred Name (Person) |
| **Description** (de) | Die bevorzugte Namensform zur Denomination einer Person. |
| **Description** (en) | A preferred name to denominate a person. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@script`](#d17e7703)  [`@type`](#d17e5396)  [`@xml:lang`](#d17e7157)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`gndo:counting`](#d17e3608)  [`gndo:epithetGenericNameTitleOrTerritory`](#d17e4311)[`gndo:forename`](#d17e4522)  [`gndo:nameAddition`](#d17e4918)  [`gndo:personalName`](#d17e5276)  [`gndo:prefix`](#d17e5463)  [`gndo:surname`](#d17e5896)   |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <attribute name="script"/>
   <attribute name="type"/>
   <choice>
      <element name="gndo:personalName" required="true"/>
      <anyOrder>
         <element name="gndo:forename" required="true"/>
         <element name="gndo:prefix"/>
         <element name="gndo:surname" required="true"/>
         <element name="gndo:counting"/>
         <text/>
      </anyOrder>
   </choice>
   <anyOrder>
      <element name="gndo:nameAddition" repeatable="true"/>
      <element name="gndo:epithetGenericNameTitleOrTerritory" repeatable="true"/>
   </anyOrder>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:preferredName[@type='original']">
      <sch:assert test="@script" role="error">(Missing script statement): It is required to explicitly provide script information via `@script` regarding the original form of the preferred name.</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5423"></a>
#### Vorzugsbenennung (Standard)
|     |     |
| --- | --- |
| **Name** | `gndo:preferredName`  |
| **See also** | [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName), [gndo:preferredNameForTheCorporateBody](https://d-nb.info/standards/elementset/gnd#preferredNameForTheCorporateBody), [gndo:preferredNameForTheWork](https://d-nb.info/standards/elementset/gnd#preferredNameForTheWork) |
| **Label** (de) | Vorzugsbenennung (Standard) |
| **Label** (en) | preferred name (default) |
| **Description** (de) | Eine bevorzugte Namensform, um die beschriebenen Entität zu bezeichnen. |
| **Description** (en) | A preferred name to denominate the documented Entity. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@xml:lang`](#d17e7157) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`place`](#d17e1265)  [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="elem.foaf.page"></a>
#### Webseite
|     |     |
| --- | --- |
| **Name** | `foaf:page`  |
| **See also** | [foaf:page](http://xmlns.com/foaf/0.1/#term_page) |
| **Label** (de) | Webseite |
| **Label** (en) | Website |
| **Description** (de) | URL eines Dokuments mit näheren Informationen über die beschriebene Entität (z.B. Homepage, Wikipedia Seite etc.). |
| **Description** (en) | URL of a document about that thing, eg. a Wikipedia page. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:label`](#d17e6963)[`@iso-date`](#attr.iso-date)   |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-date"/>
   <attribute name="gndo:label"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1648"></a>
#### Werk
|     |     |
| --- | --- |
| **Name** | `work`  |
| **See also** | [gndo:Work](https://d-nb.info/standards/elementset/gnd#Work) |
| **Label** (de) | Werk |
| **Label** (en) | Work |
| **Description** (de) | Ein Werksdatensatz zur potenziellen Ansetzung in der Gemeinsamen Normdatei (GND). |
| **Description** (en) | - |
| **Attributes** | [`@agency`](#attr.record.agency)  [`@enrich`](#d17e7628)  [`@gndo:type`](#d17e1737)  [`@gndo:uri`](#d17e6984)  [`@xml:id`](#d17e7648)   |
| **Contained by** | [`list`](#data-list) |
| **May contain** | [`<anyElement.narrow>`](#any-restricted)  [`dc:title`](#d17e3011)  [`dublicateGndIdentifier`](#dublicates)  [`gndo:abbreviatedName`](#d17e3272)  [`gndo:accordingWork`](#d17e3242)  [`gndo:author`](#d17e3422)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:contributor`](#d17e3635)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:dateOfPublication`](#d17e4177)  [`gndo:editor`](#d17e4281)  [`gndo:firstAuthor`](#d17e4491)  [`gndo:formOfWorkAndExpression`](#d17e4421)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e4843)  [`gndo:literarySource`](#d17e4888)  [`gndo:preferredName`](#d17e5423)  [`gndo:relatedWork`](#d17e5806)  [`gndo:relatesTo`](#d17e5757)  [`gndo:topic`](#d17e6005)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`img`](#elem.img)  [`owl:sameAs`](#elem.owl.sameAs)  [`ref`](#elem.ref)  [`revision`](#revision)[`skos:note`](#elem.skos.note)  [`source`](#elem.source)   |

***Content Model***  
```xml
<content>
   <attribute name="gndo:type"/>
   <attribute name="xml:id" required="true"/>
   <attribute name="gndo:uri"/>
   <attribute name="agency"/>
   <attribute name="enrich"/>
   <anyOrder>
      <element name="gndo:abbreviatedName" repeatable="true"/>
      <element name="gndo:accordingWork" repeatable="true"/>
      <element name="gndo:author" repeatable="true"/>
      <element name="gndo:broaderTerm" repeatable="true"/>
      <element name="gndo:contributor" repeatable="true"/>
      <element name="gndo:dateOfEstablishment"/>
      <element name="gndo:dateOfEstablishmentAndTermination"/>
      <element name="gndo:dateOfPublication"/>
      <element name="gndo:editor" repeatable="true"/>
      <element name="gndo:firstAuthor"/>
      <element name="gndo:formOfWorkAndExpression" repeatable="true"/>
      <element name="gndo:homepage" repeatable="true"/>
      <element name="gndo:literarySource" repeatable="true"/>
      <element name="gndo:preferredName"/>
      <element name="gndo:relatedWork" repeatable="true"/>
      <element name="gndo:topic" repeatable="true"/>
      <element name="gndo:variantName" repeatable="true"/>
      <element name="dc:title"/>
      <element name="gndo:biographicalOrHistoricalInformation" repeatable="true"/>
      <element name="dublicateGndIdentifier" repeatable="true"/>
      <element name="gndo:geographicAreaCode" repeatable="true"/>
      <element name="gndo:gndIdentifier" repeatable="true"/>
      <element name="gndo:gndSubjectCategory" repeatable="true"/>
      <element name="img" repeatable="true"/>
      <element name="gndo:languageCode" repeatable="true"/>
      <element name="gndo:relatesTo" repeatable="true"/>
      <element name="ref" repeatable="true"/>
      <element name="owl:sameAs" repeatable="true"/>
      <element name="skos:note" repeatable="true"/>
      <element name="source" repeatable="true"/>
      <anyElement type="narrow" repeatable="true"/>
      <text/>
   </anyOrder>
   <element name="revision"/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:work[not(@gndo:uri)]">
      <sch:assert test="gndo:preferredName" role="error">(Preferred Name missing): It is mandatory to provide a `gndo:preferredName` for
                        a "<sch:name/>" Entity if no `@gndo:uri` is provided!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="prop-periodOfActivity"></a>
#### Wirkungsdaten
|     |     |
| --- | --- |
| **Name** | `gndo:periodOfActivity`  |
| **Label** (de) | Wirkungsdaten |
| **Label** (en) | Period of activity |
| **Description** (de) | Zeitraum, innerhalb dessen die beschriebene Entität aktiv war. Wird nur dann erschlossen, wenn die Lebensdaten unbekannt sind. Wenn ein exaktes Wirkungsdatum erschlossen werden soll, dann wird das Datum als ISO 8601 YYYY-MM-DD in sowohl `@iso-from` als auch in `@iso-to` angegeben. |
| **Description** (en) | A person’s known period of activity |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@iso-from`](#attr.iso-from)  [`@iso-to`](#attr.iso-to)   |
| **Contained by** | [`person`](#person-record) |
| **May contain** | [`label`](#d17e6206)  [`note`](#add-note) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="iso-from"/>
   <attribute name="iso-to"/>
   <choice>
      <text/>
      <group>
         <element name="label" required="true"/>
         <element name="note" required="true"/>
      </group>
   </choice>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:periodOfActivity">
      <sch:assert test="@iso-from or @iso-to" role="error">(Point in time missing): It is mandatory to provide either a start- or
                        endpoint in time (or both) to encode a timespan like `<sch:name/>`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="person-record-placeOfActivity"></a>
#### Wirkungsort (Person)
|     |     |
| --- | --- |
| **Name** | `gndo:placeOfActivity`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#placeOfActivity](https://d-nb.info/standards/elementset/gnd#placeOfActivity) |
| **Label** (de) | Wirkungsort (Person) |
| **Label** (en) | Place of activity (Person) |
| **Description** (de) | Ein Wirkungsort, an dem die Person gewirkt oder gelebt (o.Ä.) hat. Über `@gndo:ref` kann ein Deskriptor (GND-URI) aus der GND erfasst werden. Steht kein GND-URI zur Verfügung erfolgt die Erfassung des Ortes als Freitext im Element. |
| **Description** (en) | A person’s or family’s place of activity |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@iso-date`](#attr.iso-date)  [`@iso-from`](#attr.iso-from)  [`@iso-to`](#attr.iso-to)[`@ref`](#d17e7662)   |
| **Contained by** | [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <choice>
      <attribute name="iso-date" required="true"/>
      <attribute name="iso-from"/>
      <attribute name="iso-to"/>
   </choice>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="gndo:placeOfActivity">
      <sch:assert test="@gndo:ref" role="warning">[WARN] (Missing GND-URI): It is recommended to provide a GND-URI for `<sch:name/>` via
                        `@gndo:ref`!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5924"></a>
#### Zeitweiser Name
|     |     |
| --- | --- |
| **Name** | `gndo:temporaryName`  |
| **See also** | [gndo:temporaryName](https://d-nb.info/standards/elementset/gnd#temporaryName), [gndo:temporaryNameOfTheConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#temporaryNameOfTheConferenceOrEvent), [gndo:temporaryNameOfTheCorporateBody](https://d-nb.info/standards/elementset/gnd#temporaryNameOfTheCorporateBody), [gndo:temporaryNameOfThePlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#temporaryNameOfThePlaceOrGeographicName) |
| **Label** (de) | Zeitweiser Name |
| **Label** (en) | Temporary name |
| **Description** (de) | Eine Namensform, die zeitweise zur Bezeichnung der dokumentierten Entität benutzt wurde. |
| **Description** (en) | A name used temporarily to denominate the entity encoded. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662)  [`@xml:lang`](#d17e7157) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`event`](#d17e320)  [`place`](#d17e1265) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <attribute name="xml:lang"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3242"></a>
#### Zugehöriges Werk
|     |     |
| --- | --- |
| **Name** | `gndo:accordingWork`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#accordingWork](https://d-nb.info/standards/elementset/gnd#accordingWork) |
| **Label** (de) | Zugehöriges Werk |
| **Label** (en) | According work |
| **Description** (de) | ... |
| **Description** (en) | ... |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`work`](#d17e1648) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3398"></a>
#### Zugehörigkeit
|     |     |
| --- | --- |
| **Name** | `gndo:affiliation`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#affiliation](https://d-nb.info/standards/elementset/gnd#affiliation) |
| **Label** (de) | Zugehörigkeit |
| **Description** (de) | Die Person oder Körperschaft ist zugehörig zu einer Körperschaft, oder ist mit einem Ort oder einem Event verbunden. |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`person`](#person-record) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e3608"></a>
#### Zählung
|     |     |
| --- | --- |
| **Name** | `gndo:counting`  |
| **See also** | [https://d-nb.info/standards/elementset/gnd#counting](https://d-nb.info/standards/elementset/gnd#counting) |
| **Label** (de) | Zählung |
| **Label** (en) | Counting |
| **Description** (de) | Eine Zählung als Namensbestandteil (z.B. in Ramses **II** oder Sethos **I**). |
| **Description** (en) | A Counting as Part of a proper name (eg. Ramesses **II** or Seti **II**). |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538) |
| **Contained by** | [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1553"></a>
#### embodimentOf
|     |     |
| --- | --- |
| **Name** | `embodimentOf`  |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`manifestation`](#d17e1518) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:embodimentOf/@ref">
      <sch:let name="id" value="data(.)"/>
      <sch:let name="target" value="root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::entity:expression]" role="error"> Referenced Entity "<sch:value-of select="$id"/>"
                                    must be an expression but is "<sch:value-of select="$target/name()"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6745"></a>
#### entityXML Store Metadaten
|     |     |
| --- | --- |
| **Name** | `store:store`  |
| **Label** (de) | entityXML Store Metadaten |
| **Label** (en) | entityXML Store metadata |
| **Description** (de) | Metadaten, die in einem entityXML Store automatisch erzeugt und zur Verwaltung des Datensatzes verwendet werden. |
| **Description** (en) | Metadata automatically generated by an entityXML Store to manage the dataset. |
| **Attributes** | [`@id`](#d17e6761)  [`@project`](#d17e6773)   |
| **Contained by** | [`entityXML`](#entity-xml) |
| **May contain** | [`store:workflow`](#d17e6785) |

***Content Model***  
```xml
<content>
   <attribute name="id"/>
   <attribute name="project"/>
   <element name="store:workflow"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6785"></a>
#### entityXML Store Workflow
|     |     |
| --- | --- |
| **Name** | `store:workflow`  |
| **Label** (de) | entityXML Store Workflow |
| **Label** (en) | entityXML Store workflow |
| **Description** (de) | Informationen zum entityXML Store Workflow des jeweiligen Informationsobjekts (Datensatz oder Record). |
| **Description** (en) | Information with regard to the entityXML Store Workflow of the Informationobject (dataset or record). |
| **Contained by** | [`store:store`](#d17e6745) |
| **May contain** | [`store:step`](#d17e6801) |

***Content Model***  
```xml
<content>
   <element name="store:step"/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6801"></a>
#### entityXML Store Workflow Schritt
|     |     |
| --- | --- |
| **Name** | `store:step`  |
| **Label** (de) | entityXML Store Workflow Schritt |
| **Label** (en) | entityXML Store workflow Step |
| **Description** (de) | Ein Schritt im Managementworkflow. |
| **Description** (en) | A step of the management workflow. |
| **Attributes** | [`@name`](#d17e6815)  [`@timestamp`](#d17e6817) |
| **Contained by** | [`store:workflow`](#d17e6785) |

***Content Model***  
```xml
<content>
   <attribute name="name" required="true"/>
   <attribute name="timestamp" required="true"/>
   <text/>
</content>
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e1479"></a>
#### realizationOf
|     |     |
| --- | --- |
| **Name** | `realizationOf`  |
| **Attributes** | [`@agency`](#d17e7177)  [`@enriched`](#d17e7538)  [`@gndo:ref`](#d17e7005)  [`@ref`](#d17e7662) |
| **Contained by** | [`expression`](#d17e1449) |

***Content Model***  
```xml
<content>
   <attribute name="agency"/>
   <attribute name="enriched"/>
   <attribute name="gndo:ref"/>
   <attribute name="ref"/>
   <text/>
</content>
```


***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="entity:realizationOf/@ref">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:let name="target" value="./root()//element()[@xml:id = $id]"/>
      <sch:assert test="$target[self::*:work]" role="error"> Referenced Entity "<sch:value-of select="$id"/>" must be a work
                                    but is "<sch:value-of select="$target/name()"/>"! </sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>


<a name="specs-attrs"></a>
### Attributspezifikationen

Derzeit umfasst das entityXML Schema **57** spezifizierte Attribute.

Die technische Spezifikation für Attribute umfasst folgende Eigenschaften: 

- **Name**: Der Attributsname bzw. XML-name, der das Attribut bezeichnet.
- **Datatype**: Angabe zum spezifizierten Datentyp des Attributs.
- **Label**: Ein oder mehrere Labels in Deutsch (und Englisch).
- **Description**: Eine oder mehrere Beschreibungen, die die Semantik des Attributs beschreiben.
- **Contained by**: Führt die Elemente auf, in denen das entsprechende Attrbut verwendet wird bzw. werden kann.
- **Predefined Values**: Falls spezifiziert, werden hier vordefinierte Werte angegeben.
- **Validation**: Falls spezifiziert, werden hier Validierungsregeln (z.B. Schematron Pattern) für das entsprechende Element angegeben.



<a name="d17e2331"></a>
#### ANY ATTRIBUTE
|     |     |
| --- | --- |
| **Name** | *Any name.*  |
| **Datatype** | string |
| **Label** (de) | ANY ATTRIBUTE |
| **Label** (en) | ANY ATTRIBUTE |
| **Description** (de) | Ein Attribut mit frei wählbarem **QName**. |
| **Description** (en) | - |
| **Contained by** | [`<anyElement.broad>`](#any-all)  [`<anyElement.narrow>`](#any-restricted) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.record.agency"></a>
#### Agenturanfrage
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string (*Schematron Pattern*) |
| **Label** (de) | Agenturanfrage |
| **Label** (en) | agency contact |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf den Eintrag durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Predefined Values***  (multiple choice)

 - **create**: (Eintrag in der GND neu anlegen) Der Eintrag soll als Normdatensatz neu in der GND angelegt werden.
 - **update**: (Normdatensatz mit Daten aus dem Eintrag erweitern) Der Normdatensatz soll durch neue Informationen angereichert werden.
 - **merge**: (Normdatensätze zusammenführen) Dubletten, die in dem Eintrag dokumentiert werden, sollen mit dem Normdatensatz, der durch den GND-URI des Eintrags (`@gndo:uri`) identifiziert ist, zusammengeführt werden.
 - **ignore**: (Eintrag ingonieren) Der Eintrag wird von der Agentur nicht bearbeitet.

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="@agency">
      <sch:report test=".='create' and parent::element()[@gndo:uri]" role="error">(Wrong agency mode): You can't choose agency-mode
                        "create" if the entity already exists within the GND-Authority File!</sch:report>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e7177"></a>
#### Aktion (Agentur)
|     |     |
| --- | --- |
| **Name** | `@agency`  |
| **Datatype** | string |
| **Label** (de) | Aktion (Agentur) |
| **Label** (en) | Task (Agentur) |
| **Description** (de) | Anfrage an die Agentur, welche Aktion in Hinblick auf eine Property durchgeführt werden soll. |
| **Description** (en) | - |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`dublicateGndIdentifier`](#dublicates)  [`embodimentOf`](#d17e1553)  [`foaf:page`](#elem.foaf.page)  [`gndo:abbreviatedName`](#d17e3272)  [`gndo:academicDegree`](#d17e3318)  [`gndo:accordingWork`](#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:affiliation`](#d17e3398)  [`gndo:author`](#d17e3422)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:contributor`](#d17e3635)  [`gndo:counting`](#d17e3608)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfConferenceOrEvent`](#d17e4073)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:dateOfPublication`](#d17e4177)  [`gndo:dateOfTermination`](#d17e4205)  [`gndo:editor`](#d17e4281)  [`gndo:exhibitor`](#d17e4336)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:fieldOfStudy`](#d17e4451)  [`gndo:firstAuthor`](#d17e4491)  [`gndo:formOfWorkAndExpression`](#d17e4421)  [`gndo:functionOrRole`](#d17e4550)  [`gndo:gender`](#d17e4581)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](#d17e4775)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e4843)  [`gndo:literarySource`](#d17e4888)  [`gndo:organizerOrHost`](#d17e4943)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:place`](#entity-place-standard)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e5119)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:playedInstrument`](#prop-playedInstrument)  [`gndo:precedingCorporateBody`](#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](#d17e5302)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:preferredName`](#d17e5423)  [`gndo:professionOrOccupation`](#d17e5488)  [`gndo:pseudonym`](#d17e5556)  [`gndo:publication`](#d17e5604)  [`gndo:relatedWork`](#d17e5806)  [`gndo:relatesTo`](#d17e5757)  [`gndo:succeedingCorporateBody`](#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](#d17e5866)  [`gndo:temporaryName`](#d17e5924)  [`gndo:titleOfNobility`](#d17e5969)  [`gndo:topic`](#d17e6005)  [`gndo:variantName`](#person-record-variantName)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`realizationOf`](#d17e1479)  [`skos:note`](#elem.skos.note)  [`source`](#elem.source)  [`wgs84:lat`](#d17e3192)  [`wgs84:long`](#d17e3205) |

***Predefined Values***  (multiple choice)

 - **add**: (Eigenschaft anlegen) Die Eigenschaft soll dem GND-Normdatensatz des Records hinzugefügt werden.
 - **ignore**: (Eigenschaft ingonieren) Die Eigenschaft wird von der Agentur nicht bearbeitet.
 - **remove**: (Eigenschaft entfernen) Die Eigenschaft soll aus dem GND-Normdatensatz entfernt werden.

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-from"></a>
#### Anfangsdatum
|     |     |
| --- | --- |
| **Name** | `@iso-from`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Anfangsdatum |
| **Label** (en) | Startdate |
| **Description** (de) | Ein Anfangsdatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | A startdate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfConferenceOrEvent`](#d17e4073)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:placeOfActivity`](#person-record-placeOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7538"></a>
#### Angereicherte Eigenschaft
|     |     |
| --- | --- |
| **Name** | `@enriched`  |
| **Datatype** | anyURI |
| **Label** (de) | Angereicherte Eigenschaft |
| **Label** (en) | Enriched Property |
| **Description** (de) | Diese Eigenschaft ist automatisch mithilfe des angegebenen externen Datendienstes angereichert wurden! |
| **Description** (en) | This Property was automatically added to the record using the documented Dataservice! |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`dublicateGndIdentifier`](#dublicates)  [`embodimentOf`](#d17e1553)  [`foaf:page`](#elem.foaf.page)  [`gndo:abbreviatedName`](#d17e3272)  [`gndo:academicDegree`](#d17e3318)  [`gndo:accordingWork`](#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:affiliation`](#d17e3398)  [`gndo:author`](#d17e3422)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:contributor`](#d17e3635)  [`gndo:counting`](#d17e3608)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfConferenceOrEvent`](#d17e4073)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:dateOfPublication`](#d17e4177)  [`gndo:dateOfTermination`](#d17e4205)  [`gndo:editor`](#d17e4281)  [`gndo:exhibitor`](#d17e4336)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:fieldOfStudy`](#d17e4451)  [`gndo:firstAuthor`](#d17e4491)  [`gndo:formOfWorkAndExpression`](#d17e4421)  [`gndo:functionOrRole`](#d17e4550)  [`gndo:gender`](#d17e4581)  [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:hierarchicalSuperiorOfPlaceOrGeographicName`](#d17e4775)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:languageCode`](#d17e4843)  [`gndo:literarySource`](#d17e4888)  [`gndo:organizerOrHost`](#d17e4943)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:place`](#entity-place-standard)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e5119)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:playedInstrument`](#prop-playedInstrument)  [`gndo:precedingCorporateBody`](#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](#d17e5302)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:preferredName`](#d17e5423)  [`gndo:professionOrOccupation`](#d17e5488)  [`gndo:pseudonym`](#d17e5556)  [`gndo:publication`](#d17e5604)  [`gndo:relatedWork`](#d17e5806)  [`gndo:relatesTo`](#d17e5757)  [`gndo:succeedingCorporateBody`](#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](#d17e5866)  [`gndo:temporaryName`](#d17e5924)  [`gndo:titleOfNobility`](#d17e5969)  [`gndo:topic`](#d17e6005)  [`gndo:variantName`](#person-record-variantName)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`owl:sameAs`](#elem.owl.sameAs)  [`realizationOf`](#d17e1479)  [`skos:note`](#elem.skos.note)  [`source`](#elem.source)  [`wgs84:lat`](#d17e3192)  [`wgs84:long`](#d17e3205) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6668"></a>
#### Anmerkungsart
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Anmerkungsart |
| **Description** (de) | Angabe zur Art der Anmerkung, sprich, ob es sich um eine interne Anmerkung handelt. |
| **Contained by** | [`skos:note`](#elem.skos.note) |

***Predefined Values***  

 - **internal**: (Interne Anmerkung)Eine Anmerkung, die ausschließlich für interne Zwecke dokumentiert wird.

---

<p style="margin-bottom:60px"></p>

<a name="d17e7628"></a>
#### Anreicherung
|     |     |
| --- | --- |
| **Name** | `@enrich`  |
| **Datatype** | boolean |
| **Label** (de) | Anreicherung |
| **Label** (en) | enrichment |
| **Description** (de) | Der Record soll mit Daten eines externen Datendienstes angereichert werden. Hierfür wird ein zusätzliches Konversionsscript benötigt, dass die Konversionsroutinen zur verfügung stellt, um den Record anzureichern! |
| **Description** (en) | The Record is marked to be enriched by a separate enrichment routine provided by an additional conversion script! |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

---

<p style="margin-bottom:60px"></p>

<a name="rev-status"></a>
#### Bearbeitungsphase
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsphase |
| **Label** (en) | Phase |
| **Description** (de) | Die Phase der Bearbeitung, in der sich die Datensammlung oder der Eintrag momentan befindet. |
| **Description** (en) | The phase of a records editing-lifecycle. |
| **Contained by** | [`revision`](#revision) |

***Predefined Values***  

 - **opened**: (Geöffnet, *default*) Die Informationsressource wurde angelegt und befindet sich derzeit in der Bearbeitung durch den Datenlieferanten.
 - **staged**: (Auf dem Prüfstand) Die Informationsressource befindet sich derzeit in der Kontrolle durch eine GND-Agentur: Es wird geprüft, ob die geltenden Qualitätskriterien erfüllt sind. Mögliche Rückfragen und Vorschläge zur Anpassungen werden durch die GND-Agentur dokumentiert und den Datenlieferanten mitgeteilt.
 - **closed**: (Abgeschlossen) Alle Arbeiten an der Informationsressource wurden durchgeführt. Sie gilt als abgeschlossen.

---

<p style="margin-bottom:60px"></p>

<a name="change-status"></a>
#### Bearbeitungsstadium
|     |     |
| --- | --- |
| **Name** | `@status`  |
| **Datatype** | string |
| **Label** (de) | Bearbeitungsstadium |
| **Description** (de) | Der momentane Bearbeitungsstatus der Informationsressource. |
| **Contained by** | [`change`](#d17e2624) |

***Predefined Values***  

 - **draft**: (Draft, *default*) Die Informationsressource befindet sich in der Bearbeitung.
 - **candidate**: (Kandidat) Die Bearbeitung ist soweit abgeschlossen. Die Informationsressource gilt nun als potenzieller Kandidat zur Übertragung an die GND und ist somit bereit zur Prüfung durch eine GND-Agentur.
 - **embargoed**: (Gesperrt) Die Informationsressource ist derzeit gesperrt. Sie erfüllt entweder nicht die erforderlichen Qualitätskriterien oder ist aus einem anderen Grund, der in dem Änderungseintrag vermerkt ist, gesperrt. Sie muss entsprechend überarbeitet werden.
 - **withdrawn**: (Zurückgezogen) Die Informationsressource wurde zurückgezogen. Sie wird in Hinblick auf eine Übertragung an die GND ignoriert.
 - **cleared**: (Überarbeitet) Die Informationsressource wurde in Hinblick auf die geltenden Qualitätskriterien überarbeitet und gilt nun als überarbeiteter Kandidat, der erneut geprüft wird.
 - **approved**: (Angenommen) Die Prüfung der Informationsressource ist abgeschlossen. Sie erfüllt alle erforderlichen Qualitätsstandards und ist bereit zur Übertragung an die GND. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **submitted**: (An GND geliefert) Die Daten der Informationsressource wurden an die GND zum Übertragen geliefert. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.
 - **published**: (Veröffentlicht) Die Daten der Informationsressource wurden in der GND veröffentlicht. Dieser Bearbeitungsschritt ist Mitarbeiter\*innen einer GND-Agentur vorbehalten.

---

<p style="margin-bottom:60px"></p>

<a name="d17e6145"></a>
#### Breite
|     |     |
| --- | --- |
| **Name** | `@width`  |
| **Datatype** | string |
| **Label** (de) | Breite |
| **Description** (de) | Breite des Bildes. |
| **Contained by** | [`img`](#elem.img) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6885"></a>
#### Certainty
|     |     |
| --- | --- |
| **Name** | `@cert`  |
| **Datatype** | string |
| **Label** (de) | Certainty |
| **Description** (de) | Angabe zum Status der getroffenen Aussage. `@cert` hat einen vorgegebenen, exklusiven Wertebereich ("low", "middle", "high"). |
| **Contained by** | [`dublicateGndIdentifier`](#dublicates)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfPublication`](#d17e4177)  [`gndo:dateOfTermination`](#d17e4205)  [`gndo:forename`](#d17e4522)  [`gndo:gndIdentifier`](#d17e4693)  [`gndo:surname`](#d17e5896) |

***Predefined Values***  

 - **low**: (unsicher) Die Aussage ist unsicher
 - **middle**: (wahrscheinlich) Die Aussage ist wahrscheinlich
 - **high**: (belegt) Die Aussage ist belegt

---

<p style="margin-bottom:60px"></p>

<a name="d17e5638"></a>
#### DNB Katalog
|     |     |
| --- | --- |
| **Name** | `@dnb:catalogue`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | DNB Katalog |
| **Description** (de) | Verknüpfung einer `gndo:publication` mit einem URL eines Eintrags im [Katalog der Deutschen Nationalbibliothek](https://portal.dnb.de/opac.htm). |
| **Contained by** | [`gndo:publication`](#d17e5604) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e7724"></a>
#### Datum
|     |     |
| --- | --- |
| **Name** | `@when`  |
| **Datatype** | date dateTime  |
| **Label** (de) | Datum |
| **Description** (de) | Angabe des Datums der Änderung. |
| **Contained by** | [`change`](#d17e2624)  [`mappingLabel`](#d17e6259) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-to"></a>
#### Enddatum
|     |     |
| --- | --- |
| **Name** | `@iso-to`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | Enddatum |
| **Label** (en) | Enddate |
| **Description** (de) | Ein Enddatum im ISO Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An enddate in ISO format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfConferenceOrEvent`](#d17e4073)  [`gndo:dateOfEstablishmentAndTermination`](#d17e4135)  [`gndo:periodOfActivity`](#prop-periodOfActivity)  [`gndo:placeOfActivity`](#person-record-placeOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="event-types"></a>
#### Eventtyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Eventtyp |
| **Label** (en) | Type of a Person |
| **Description** (de) | Typisierung des dokumentierten Events (z.B. als Veranstaltungsfolge). Wenn kein `@gndo:type` vergeben wird, gilt die Person als [gndo:ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent). |
| **Description** (en) | Spezifies the event encoded as eg. series of conference or event). If no `@gndo:type` is provided the default is [gndo:ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent). |
| **Contained by** | [`event`](#d17e320) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent**: (Kongressfolge oder Veranstaltungsfolge) [GNDO](https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent)

---

<p style="margin-bottom:60px"></p>

<a name="d17e5783"></a>
#### GND Beziehungstyp-Code
|     |     |
| --- | --- |
| **Name** | `@gndo:code`  |
| **Datatype** | string |
| **Label** (de) | GND Beziehungstyp-Code |
| **Label** (en) | GND Relation Typecode |
| **Description** (de) | Spezifiziert die Art der ausgewiesenen Beziehung über einen **GND-CODE** (vgl. hierzu [Vollständige Liste der GND-Codes für Beziehungen für das Feld 500](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://opus.k10plus.de/frontdoor/deliver/index/docId/417/file/K10plus_Tabelle_4-Codes_500.pdf&ved=2ahUKEwiHmvXt6vaHAxVp8wIHHavBORUQFnoECBoQAQ&usg=AOvVaw3Kh3sIAIamPs7ObESHL1ZF)) vom K10Pplus-Online-Publikations-Server. |
| **Description** (en) | Spezifies the type of the encoded relation using a **GND-CODE**, cf . [Vollständige Liste der GND-Codes für Beziehungen für das Feld 500](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://opus.k10plus.de/frontdoor/deliver/index/docId/417/file/K10plus_Tabelle_4-Codes_500.pdf&ved=2ahUKEwiHmvXt6vaHAxVp8wIHHavBORUQFnoECBoQAQ&usg=AOvVaw3Kh3sIAIamPs7ObESHL1ZF)) from the K10Pplus-Online-Publications-Server. |
| **Contained by** | [`gndo:relatesTo`](#d17e5757) |

***Predefined Values***  

 - **adre**: (Adressat) 
 - **anno**: (Annotator) 
 - **arch**: (Architekt) 
 - **arra**: (Arrangeur) 
 - **aust**: (Aussteller) 
 - **aut1**: (Verfasser, erster) 
 - **auta**: (Verfasser) 
 - **autf**: (Verfasser, fiktiver) 
 - **autg**: (Verfasser, zugeschriebener) 
 - **autw**: (Verfasser, zweifelhafter) 
 - **autz**: (Verfasser, zitierter) 
 - **bauh**: (Bauherr) 
 - **bear**: (Bearbeiter) 
 - **befr**: (Besitzer, früherer) 
 - **besi**: (Besitzer) 
 - **bete**: (Beteiligte) 
 - **beza**: (Bekanntschaft mit) 
 - **bezb**: (Beziehung beruflich) 
 - **bezf**: (Beziehung familiär) 
 - **bilh**: (Bildhauer) 
 - **bubi**: (Buchbinder) 
 - **chre**: (Choreograf) 
 - **comp**: (Compiler) 
 - **desi**: (Designer) 
 - **dich**: (Textdichter) 
 - **druc**: (Drucker) 
 - **erfi**: (Erfinder) 
 - **feie**: (Gefeierte oder dargestellte Person/Familie) 
 - **foto**: (Fotograf) 
 - **gest**: (Buchgestalter) 
 - **grav**: (Graveur, Stecher) 
 - **grue**: (Gründer) 
 - **hers**: (Hersteller) 
 - **hrsg**: (Herausgeber) 
 - **illu**: (Illustrator, Illuminator) 
 - **istm**: (Instrumentalmusiker) 
 - **kame**: (Verantwortlicher Kameramann) 
 - **kart**: (Kartograf) 
 - **kom1**: (Komponist, erster) 
 - **koma**: (Komponist) 
 - **komg**: (Komponist, zugeschriebener) 
 - **komm**: (Kommentator) 
 - **komw**: (Komponist, zweifelhafter) 
 - **komz**: (Komponist, zitierter) 
 - **kopi**: (Kopist) 
 - **korr**: (Korrespondenzpartner) 
 - **kue1**: (Künstler, erster) 
 - **kueg**: (Künstler, zugeschriebener) 
 - **kuen**: (Künstler) 
 - **kuew**: (Künstler, zweifelhafter) 
 - **kuez**: (Künstler, zitierter) 
 - **kura**: (Kurator) 
 - **leih**: (Leihgeber) 
 - **libr**: (Librettist) 
 - **lith**: (Lithograf) 
 - **malr**: (Maler) 
 - **mitg**: (Mitglied) 
 - **musi**: (Musiker) 
 - **nawi**: (Name, wirklicher) 
 - **obpa**: (Oberbegriff, partitiv) 
 - **pseu**: (Pseudonym) 
 - **radi**: (Radierer) 
 - **reda**: (Redakteur) 
 - **regi**: (Regisseur) 
 - **rela**: (Relation (allgemein)) 
 - **rest**: (Restaurator) 
 - **saen**: (Sänger) 
 - **saml**: (Sammler) 
 - **spon**: (Sponsor, Mäzen) 
 - **spre**: (Sprecher) 
 - **stif**: (Stifter) 
 - **them**: (Thema) 
 - **uebe**: (Übersetzer) 
 - **urhe**: (Urheber) 
 - **vbal**: (Verwandter Begriff (allgemein)) 
 - **verr**: (Veranlasser) 
 - **vfrd**: (Drehbuchautor) 
 - **widm**: (Widmungsempfänger) 

---

<p style="margin-bottom:60px"></p>

<a name="d17e4603"></a>
#### GND Gender Descriptor URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Gender Descriptor URI |
| **Description** (de) | Ein oder mehrere URIs von Termen aus dem GND Vokabular "Gender" (https://d-nb.info/standards/vocab/gnd/gender#). |
| **Contained by** | [`gndo:gender`](#d17e4581) |

***Predefined Values***  (multiple choice)

 - **https://d-nb.info/standards/vocab/gnd/gender#female**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#male**: *no description available*
 - **https://d-nb.info/standards/vocab/gnd/gender#notKnown**: *no description available*

---

<p style="margin-bottom:60px"></p>

<a name="d17e6949"></a>
#### GND Geographic Area Code Term
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | string |
| **Label** (de) | GND Geographic Area Code Term |
| **Description** (de) | URI eines Terms aus dem GND Vokabular *[Geographic Area Code](https://d-nb.info/standards/vocab/gnd/geographic-area-code)* |
| **Contained by** | [`gndo:geographicAreaCode`](#elem.gndo.geographicAreaCode) |

***Predefined Values***  

 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#NTHH**: (Neutral Zone (-1993))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA**: (Europe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AAAT**: (Austria (-12.11.1918))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AD**: (Andorra)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AL**: (Albania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT**: (Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-1**: (Burgenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-2**: (Carinthia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-3**: (Lower Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-4**: (Upper Austria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-5**: (Salzburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-6**: (Styria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-7**: (Tyrol)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-8**: (Vorarlberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AT-9**: (Vienna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-AX**: (Ǻland Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BA**: (Bosnia and Hercegovina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BE**: (Belgium)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BG**: (Bulgaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-BY**: (Belarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH**: (Switzerland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AG**: (Aargau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AI**: (Appenzell Innerrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-AR**: (Appenzell Ausserrhoden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BE**: (Bern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BL**: (Basel-Landschaft)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-BS**: (Basel-Stadt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-FR**: (Fribourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GE**: (Geneva)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GL**: (Glarus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-GR**: (Grisons)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-JU**: (Jura)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-LU**: (Luzern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NE**: (Neuchâtel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-NW**: (Nidwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-OW**: (Obwalden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SG**: (St. Gallen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SH**: (Schaffhausen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SO**: (Solothurn)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-SZ**: (Schwyz)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TG**: (Thurgau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-TI**: (Ticino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-UR**: (Uri)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VD**: (Vaud)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-VS**: (Valais)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZG**: (Zug)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CH-ZH**: (Zürich)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSHH**: (Czechoslovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CSXX**: (Serbia and Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CY**: (Cyprus)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-CZ**: (Czech Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DDDE**: (Germany East)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE**: (Germany)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BB**: (Brandenburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BE**: (Berlin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BW**: (Baden-Württemberg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-BY**: (Bavaria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HB**: (Bremen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HE**: (Hesse)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-HH**: (Hamburg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-MV**: (Mecklenburg-Vorpommern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NI**: (Lower Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-NW**: (North Rhine-Westphalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-RP**: (Rhineland-Palatinate)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SH**: (Schleswig-Holstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SL**: (Saarland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-SN**: (Saxony)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-ST**: (Saxony-Anhalt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DE-TH**: (Thuringia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DK**: (Denmark)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-DXDE**: (Germany (-1949))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-EE**: (Estonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ES**: (Spain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FI**: (Finland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-FR**: (France)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GB**: (Great Britain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GG**: (Guernsey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GI**: (Gibraltar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-GR**: (Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HR**: (Croatia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-HU**: (Hungary)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IE**: (Ireland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IM**: (Isle of Man)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IS**: (Iceland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT**: (Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-IT-32**: (Trentino-Alto Adige Italy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-JE**: (Jersey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LI**: (Liechtenstein)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LT**: (Lithuania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LU**: (Luxembourg)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-LV**: (Latvia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MC**: (Monaco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MD**: (Moldova)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-ME**: (Montenegro)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MK**: (North Macedonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-MT**: (Malta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NL**: (Netherlands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-NO**: (Norway)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PL**: (Poland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-PT**: (Portugal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-QV**: (Kosovo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RO**: (Romania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RS**: (Serbia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-RU**: (Russia (Federation))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SE**: (Sweden)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SI**: (Slovenia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SK**: (Slovakia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SM**: (San Marino)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-SUHH**: (Soviet Union)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-UA**: (Ukraine)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-VA**: (Vatican City)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XA-YUCS**: (Yugoslavia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB**: (Asia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AE**: (United Arab Emirates)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AF**: (Afghanistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AM**: (Armenia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-AZ**: (Azerbaijan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BD**: (Bangladesh)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BH**: (Bahrain)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BN**: (Brunei)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BT**: (Bhutan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-BUMM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN**: (China)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-CN-54**: (Tibet (China))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-GE**: (Georgia (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-HK**: (Hong Kong)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-ID**: (Indonesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IL**: (Israel)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IN**: (India)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IQ**: (Iraq)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-IR**: (Iran)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JO**: (Jordan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-JP**: (Japan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KG**: (Kyrgyzstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KH**: (Cambodia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KP**: (Korea (North))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KR**: (Korea (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KW**: (Kuwait)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-KZ**: (Kazakhstan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LA**: (Laos)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LB**: (Lebanon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-LK**: (Sri Lanka)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MM**: (Burma)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MN**: (Mongolia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MO**: (Macau (China : Special Administrative Region))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MV**: (Maldives)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-MY**: (Malaysia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-NP**: (Nepal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-OM**: (Oman)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PH**: (Philippines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-PK**: (Pakistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-QA**: (Qatar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SA**: (Saudi Arabia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SG**: (Singapore)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SKIN**: (Sikkim)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-SY**: (Syria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TH**: (Thailand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TJ**: (Tajikistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TM**: (Turkmenistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TPTL**: (East Timor)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TR**: (Turkey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-TW**: (Taiwan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-UZ**: (Uzbekistan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VDVN**: (Vietnam (South))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-VN**: (Vietnam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YDYE**: (Yemen (Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XB-YE**: (Yemen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC**: (Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AIDJ**: (French Afars and Issas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-AO**: (Angola)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BF**: (Burkina Faso)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BI**: (Burundi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BJ**: (Benin)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-BW**: (Botswana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CD**: (Congo (Democratic Republic))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CF**: (Central African Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CG**: (Congo (Brazzaville))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CI**: (Côte d'Ivoire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CM**: (Cameroon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-CV**: (Cape Verde)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DJ**: (Djibouti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DYBJ**: (Dahomey)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-DZ**: (Algeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EG**: (Egypt)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-EH**: (Western Sahara)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ER**: (Eritrea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ET**: (Ethiopia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GA**: (Gabon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GH**: (Ghana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GM**: (Gambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GN**: (Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GQ**: (Equatorial Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-GW**: (Guinea-Bissau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-HVBF**: (Upper Volta)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KE**: (Kenya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-KM**: (Comoros)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LR**: (Liberia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LS**: (Lesotho)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-LY**: (Libya)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MA**: (Morocco)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MG**: (Madagascar)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ML**: (Mali)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MR**: (Mauritania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MU**: (Mauritius)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MW**: (Malawi)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-MZ**: (Mozambique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NA**: (Namibia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NE**: (Niger)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-NG**: (Nigeria)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RHZW**: (Southern Rhodesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-RW**: (Rwanda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SC**: (Seychelles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SD**: (Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SL**: (Sierra Leone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SN**: (Senegal)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SO**: (Somalia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SS**: (South Sudan)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ST**: (Sao Tome and Principe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-SZ**: (Swaziland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TD**: (Chad)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TG**: (Togo)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TN**: (Tunisia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-TZ**: (Tanzania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-UG**: (Uganda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-YT**: (Mayotte)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZA**: (South Africa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZM**: (Zambia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZRCD**: (Zaire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XC-ZW**: (Zimbabwe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD**: (America)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AG**: (Antigua and Barbuda)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AI**: (Anguilla)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-ANHH**: (Netherlands Antilles)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AR**: (Argentina)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AS**: (American Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-AW**: (Aruba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BB**: (Barbados)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BL**: (Saint Barthélemy)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BM**: (Bermuda Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BO**: (Bolivia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BR**: (Brazil)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BQ**: (Bonaire, Sint Eustatius and Saba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BS**: (Bahamas)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-BZ**: (Belize)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CA**: (Canada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CL**: (Chile)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CO**: (Colombia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CR**: (Costa Rica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CU**: (Cuba)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-CW**: (Curaçao)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DM**: (Dominica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-DO**: (Dominican Republic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-EC**: (Ecuador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GD**: (Grenada)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GF**: (French Guiana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GP**: (Guadeloupe)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GT**: (Guatemala)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-GY**: (Guyana)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HN**: (Honduras)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-HT**: (Haiti)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-JM**: (Jamaica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KN**: (Saint Kitts-Nevis)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-KY**: (Cayman Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-LC**: (Saint Lucia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MF**: (Saint Martin, Northern)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MQ**: (Martinique)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MS**: (Montserrat)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-MX**: (Mexico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-NI**: (Nicaragua)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PA**: (Panama)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PE**: (Peru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PM**: (Saint Pierre and Miquelon)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PR**: (Puerto Rico)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PY**: (Paraguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-PZPA**: (Panama Canal Zone)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SR**: (Suriname)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SV**: (El Salvador)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-SX**: (Sint Maarten (Dutch part))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TC**: (Turks and Caicos Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-TT**: (Trinidad and Tobago)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-US**: (United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-UY**: (Uruguay)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VC**: (Saint Vincent and the Grenadines)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VE**: (Venezuela)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VG**: (British Virgin Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XD-VI**: (Virgin Islands of the United States)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE**: (Australia, Oceania)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-AU**: (Australia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CC**: (Cocos (Keeling) Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CK**: (Cook Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CTKI**: (Canton and Enderbury)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-CX**: (Christmas Island (Indian Ocean))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FJ**: (Fiji)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-FM**: (Micronesia (Federated States))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GEHH**: (Gilbert Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-GU**: (Guam)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-KI**: (Kiribati)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MH**: (Marshall Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-MP**: (Northern Mariana Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NC**: (New Caledonia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NF**: (Norfolk Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NHVU**: (New Hebrides)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NR**: (Nauru)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-NZ**: (New Zealand)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PCHH**: (Pacific Islands (Trust Territory))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PF**: (French Polynesia)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PG**: (Papua New Guinea)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-PW**: (Palau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-SB**: (Solomon Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TK**: (Tokelau)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TO**: (Tonga)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-TV**: (Tuvalu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-VU**: (Vanuatu)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WF**: (Wallis and Futuna)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XE-WS**: (Samoa)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XH**: (Arctic)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI**: (Antarctic Ocean/Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-AQ**: (Antarctica)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-BQAQ**: (British Antarctic Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-FQHH**: (Terres australes et antarctiques françaises)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-HM**: (Heard and McDonald Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XI-NQAQ**: (Queen Maud Land)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK**: (Atlantic Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-BV**: (Bouvet Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FK**: (Falkland Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-FO**: (Faroe Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GL**: (Greenland)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-GS**: (South Georgia and the South Sandwich Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SH**: (Saint Helena)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XK-SJ**: (Svalbard and Jan Mayen)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL**: (Indian Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-IO**: (British Indian Ocean Territory)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-RE**: (Réunion)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XL-TF**: (French Southern Territories (the))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM**: (Pacific Ocean)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-JTUM**: (Johnston Atoll)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-MIUM**: (Midway Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-NU**: (Niue)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PN**: (Pitcairn Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-PUUM**: (American Territory in the Pacific (-1986))
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-UM**: (United States Misc. Pacific Islands)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XM-WKUM**: (Wake Island)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XN**: (Outer Space)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XP**: (International Organizations)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XQ**: (World)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XR**: (Orient)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XS**: (Ancient Greece)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XT**: (Rome)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XU**: (Byzantine Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XV**: (Ottoman Empire)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XW**: (Palestinian Arabs)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XX**: (Arab Countries)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XY**: (Jews)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#XZ**: (Imaginary places)
 - **https://d-nb.info/standards/vocab/gnd/geographic-area-code#ZZ**: (Country unknown)

---

<p style="margin-bottom:60px"></p>

<a name="d17e7026"></a>
#### GND Term URI
|     |     |
| --- | --- |
| **Name** | `@gndo:term`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND Term URI |
| **Description** (de) | URI eines Terms entweder aus der **GND** (https://d-nb.info/gnd/) oder einem **GND Vokabular** (https://d-nb.info/standards/vocab/) |
| **Contained by** | [`gndo:functionOrRole`](#d17e4550)  [`gndo:gndSubjectCategory`](#d17e4734) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/standards/vocab/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e7005"></a>
#### GND-Referenz
|     |     |
| --- | --- |
| **Name** | `@gndo:ref`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-Referenz |
| **Label** (en) | GND-Reference |
| **Description** (de) | Referenz auf eine Entität in der GND, identifiziert durch einen GND-URI. |
| **Description** (en) | Reference to an entity in the GND identified by a GND-URI. |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`embodimentOf`](#d17e1553)  [`gndo:accordingWork`](#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:affiliation`](#d17e3398)  [`gndo:author`](#d17e3422)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:contributor`](#d17e3635)  [`gndo:editor`](#d17e4281)  [`gndo:exhibitor`](#d17e4336)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:fieldOfStudy`](#d17e4451)  [`gndo:firstAuthor`](#d17e4491)  [`gndo:formOfWorkAndExpression`](#d17e4421)  [`gndo:literarySource`](#d17e4888)  [`gndo:organizerOrHost`](#d17e4943)  [`gndo:place`](#entity-place-standard)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e5119)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:playedInstrument`](#prop-playedInstrument)  [`gndo:precedingCorporateBody`](#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](#d17e5302)  [`gndo:professionOrOccupation`](#d17e5488)  [`gndo:pseudonym`](#d17e5556)  [`gndo:publication`](#d17e5604)  [`gndo:relatedWork`](#d17e5806)  [`gndo:relatesTo`](#d17e5757)  [`gndo:succeedingCorporateBody`](#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](#d17e5866)  [`gndo:temporaryName`](#d17e5924)  [`gndo:titleOfNobility`](#d17e5969)  [`gndo:topic`](#d17e6005)  [`realizationOf`](#d17e1479)  [`term`](#d17e6829) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6984"></a>
#### GND-URI
|     |     |
| --- | --- |
| **Name** | `@gndo:uri`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | GND-URI |
| **Label** (en) | GND-URI |
| **Description** (de) | Angabe des GND-HTTP URIs der beschriebenen Entität in der GND, sofern vorhanden. |
| **Description** (en) | - |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(http|https)://d-nb.info/gnd/(.+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e268"></a>
#### GNDO-Typ der beschriebenen Entität
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | GNDO-Typ der beschriebenen Entität |
| **Description** (de) | URI eines Entitätstyps (Class) aus der [GNDO](https://d-nb.info/standards/elementset/gnd#). |
| **Contained by** | [`entity`](#entity-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CorporateBody**: (Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#CorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#Company)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent**: (Konferenz oder Veranstaltung) [GNDO](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent)
 - **https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent**: (Kongressfolge oder Veranstaltungsfolge) [GNDO](https://d-nb.info/standards/elementset/gnd#SeriesOfConferenceOrEvent)
 - **https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson**: (Individualisierte Person) [GNDO](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson)
 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#Gods)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#Spirits)
 - **https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName**: (Geografikum) [GNDO](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName)
 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#AdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#Country)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#FictivePlace)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#MemberState)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousTerritory)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#WayBorderOrLine)
 - **https://d-nb.info/standards/elementset/gnd#Work**: (Werk) [GNDO](https://d-nb.info/standards/elementset/gnd#Work)
 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#Collection)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectiveManuscript)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#Expression)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#Manuscript)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalWork)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6157"></a>
#### Höhe
|     |     |
| --- | --- |
| **Name** | `@height`  |
| **Datatype** | string |
| **Label** (de) | Höhe |
| **Description** (de) | Höhe des Bildes. |
| **Contained by** | [`img`](#elem.img) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7042"></a>
#### ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | ID |
| **Description** (de) | Eine interne ID zur Identifikation des entsprechenden Elements. |
| **Contained by** | [`list`](#data-list)  [`respStmt`](#respStmt) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6925"></a>
#### ISIL/Bibliothekssiegel
|     |     |
| --- | --- |
| **Name** | `@isil`  |
| **Datatype** | string (*RNG Pattern Matching*) |
| **Label** (de) | ISIL/Bibliothekssiegel |
| **Label** (de) | ISIL/Acronym for libraries |
| **Description** (de) | Eindeutiger Identifikator der Organisation als ISIL (International Standard Identifier for Libraries and Related Organisations). |
| **Description** (de) | ISIL of an Organisation (International Standard Identifier for Libraries and Related Organisations). |
| **Contained by** | [`agency`](#agency-stmt)  [`provider`](#data-provider) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">[A-Z]{1,4}-[a-zA-Z0-9\-/:]{1,11}</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-date"></a>
#### ISO-Datum
|     |     |
| --- | --- |
| **Name** | `@iso-date`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum |
| **Label** (en) | ISO-Date |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ |
| **Description** (en) | An ISO date in the Format YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`foaf:page`](#elem.foaf.page)  [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfPublication`](#d17e4177)  [`gndo:dateOfTermination`](#d17e4205)  [`gndo:homepage`](#elem.gndo.homepage)  [`gndo:placeOfActivity`](#person-record-placeOfActivity) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-notBefore"></a>
#### ISO-Datum (frühestes)
|     |     |
| --- | --- |
| **Name** | `@iso-notBefore`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum (frühestes) |
| **Label** (en) | ISO-Date (earliest) |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ als früheste mögliche Zeitangabe für ein Event. |
| **Description** (en) | The earliest possible date for the event in standard form YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfPublication`](#d17e4177)  [`gndo:dateOfTermination`](#d17e4205) |

---

<p style="margin-bottom:60px"></p>

<a name="attr.iso-notAfter"></a>
#### ISO-Datum (spätestes)
|     |     |
| --- | --- |
| **Name** | `@iso-notAfter`  |
| **Datatype** | date gYearMonth gYear  |
| **Label** (de) | ISO-Datum (spätestes) |
| **Label** (en) | ISO-Date (earliest) |
| **Description** (de) | Ein ISO Datum im Format JJJJ-MM-TT, JJJJ-MM oder JJJJ als späteste mögliche Zeitangabe für ein Event. |
| **Description** (en) | The latest possible date for the event in standard form YYYY-MM-DD, YYYY-MM or YYYY |
| **Contained by** | [`gndo:dateOfBirth`](#person-record-dateOfBirth)  [`gndo:dateOfDeath`](#person-record-dateOfDeath)  [`gndo:dateOfEstablishment`](#d17e4104)  [`gndo:dateOfPublication`](#d17e4177)  [`gndo:dateOfTermination`](#d17e4205) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6189"></a>
#### Identifier-Art
|     |     |
| --- | --- |
| **Name** | `@type`  |
| **Datatype** | string |
| **Label** (de) | Identifier-Art |
| **Description** (de) | Art des Identifiers |
| **Contained by** | [`idno`](#elem.idno) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e720"></a>
#### Körperschaftstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Körperschaftstyp |
| **Label** (en) | Type of a Corporate Body |
| **Description** (de) | Typisierung der dokumentierten Körperschaft (z.B. als fiktive Körperschaft, Firma usw). Wenn kein `@gndo:type` vergeben wird, gilt die Körperschaft als [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody). |
| **Description** (en) | Spezifies the Corporate Body encoded as eg. fictional corporate body, company etc.). If no `@gndo:type` is provided the default is [gndo:CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody). |
| **Contained by** | [`corporateBody`](#corporate-body-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Company**: (Firma) [GNDO](https://d-nb.info/standards/elementset/gnd#Company)
 - **https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody**: (Fiktive Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody**: (Musikalische Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody**: (Organ einer Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - **https://d-nb.info/standards/elementset/gnd#ProjectOrProgram**: (Projekt oder Programm) [GNDO](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit**: (Religiöse Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody**: (Religiöse Körperschaft) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6963"></a>
#### Label
|     |     |
| --- | --- |
| **Name** | `@gndo:label`  |
| **Datatype** | string |
| **Label** (de) | Label |
| **Label** (en) | Label |
| **Description** (de) | Angabe eines Labels, dass mit der entsprechenden Information assoziiert wird, z.B. zur Anzeigensteuerung. |
| **Description** (en) | - |
| **Contained by** | [`foaf:page`](#elem.foaf.page)  [`gndo:homepage`](#elem.gndo.homepage) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7241"></a>
#### Labeltyp (Mapping)
|     |     |
| --- | --- |
| **Name** | `@type`  |
| **Datatype** | string |
| **Label** (de) | Labeltyp (Mapping) |
| **Description** (de) | GND Entitätstyp, auf den das Label bezogen werden kann. |
| **Contained by** | [`mappingLabel`](#d17e6259)  [`term`](#d17e6829) |

***Predefined Values***  (multiple choice)

 - **CorporateBody**: ((CorporateBody)[https://d-nb.info/standards/elementset/gnd#CorporateBody](https://d-nb.info/standards/elementset/gnd#CorporateBody)
 - **ConferenceOrEvent**: ((ConferenceOrEvent)[https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent](https://d-nb.info/standards/elementset/gnd#ConferenceOrEvent)
 - **DifferentiatedPerson**: ((DifferentiatedPerson)[https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson)
 - **PlaceOrGeographicName**: ((PlaceOrGeographicName)[https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName)
 - **Work**: ((Work)[https://d-nb.info/standards/elementset/gnd#Work](https://d-nb.info/standards/elementset/gnd#Work)
 - **Company**: ((Company)[https://d-nb.info/standards/elementset/gnd#Company](https://d-nb.info/standards/elementset/gnd#Company)
 - **FictiveCorporateBody**: ((FictiveCorporateBody)[https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody](https://d-nb.info/standards/elementset/gnd#FictiveCorporateBody)
 - **MusicalCorporateBody**: ((MusicalCorporateBody)[https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody](https://d-nb.info/standards/elementset/gnd#MusicalCorporateBody)
 - **OrganOfCorporateBody**: ((OrganOfCorporateBody)[https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody](https://d-nb.info/standards/elementset/gnd#OrganOfCorporateBody)
 - **ProjectOrProgram**: ((ProjectOrProgram)[https://d-nb.info/standards/elementset/gnd#ProjectOrProgram](https://d-nb.info/standards/elementset/gnd#ProjectOrProgram)
 - **ReligiousAdministrativeUnit**: ((ReligiousAdministrativeUnit)[https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit](https://d-nb.info/standards/elementset/gnd#ReligiousAdministrativeUnit)
 - **ReligiousCorporateBody**: ((ReligiousCorporateBody)[https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody](https://d-nb.info/standards/elementset/gnd#ReligiousCorporateBody)
 - **CollectivePseudonym**: ((CollectivePseudonym)[https://d-nb.info/standards/elementset/gnd#CollectivePseudonym](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - **Gods**: ((Gods)[https://d-nb.info/standards/elementset/gnd#Gods](https://d-nb.info/standards/elementset/gnd#Gods)
 - **LiteraryOrLegendaryCharacter**: ((LiteraryOrLegendaryCharacter)[https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - **Pseudonym**: ((Pseudonym)[https://d-nb.info/standards/elementset/gnd#Pseudonym](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - **RoyalOrMemberOfARoyalHouse**: ((RoyalOrMemberOfARoyalHouse)[https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - **Spirits**: ((Spirits)[https://d-nb.info/standards/elementset/gnd#Spirits](https://d-nb.info/standards/elementset/gnd#Spirits)
 - **AdministrativeUnit**: ((AdministrativeUnit)[https://d-nb.info/standards/elementset/gnd#AdministrativeUnit](https://d-nb.info/standards/elementset/gnd#AdministrativeUnit)
 - **BuildingOrMemorial**: ((BuildingOrMemorial)[https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial](https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial)
 - **Country**: ((Country)[https://d-nb.info/standards/elementset/gnd#Country](https://d-nb.info/standards/elementset/gnd#Country)
 - **ExtraterrestrialTerritory**: ((ExtraterrestrialTerritory)[https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory](https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory)
 - **FictivePlace**: ((FictivePlace)[https://d-nb.info/standards/elementset/gnd#FictivePlace](https://d-nb.info/standards/elementset/gnd#FictivePlace)
 - **MemberState**: ((MemberState)[https://d-nb.info/standards/elementset/gnd#MemberState](https://d-nb.info/standards/elementset/gnd#MemberState)
 - **NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: ((NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)[https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit](https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)
 - **NaturalGeographicUnit**: ((NaturalGeographicUnit)[https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit](https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit)
 - **ReligiousTerritory**: ((ReligiousTerritory)[https://d-nb.info/standards/elementset/gnd#ReligiousTerritory](https://d-nb.info/standards/elementset/gnd#ReligiousTerritory)
 - **TerritorialCorporateBodyOrAdministrativeUnit**: ((TerritorialCorporateBodyOrAdministrativeUnit)[https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit](https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit)
 - **WayBorderOrLine**: ((WayBorderOrLine)[https://d-nb.info/standards/elementset/gnd#WayBorderOrLine](https://d-nb.info/standards/elementset/gnd#WayBorderOrLine)
 - **Collection**: ((Collection)[https://d-nb.info/standards/elementset/gnd#Collection](https://d-nb.info/standards/elementset/gnd#Collection)
 - **CollectiveManuscript**: ((CollectiveManuscript)[https://d-nb.info/standards/elementset/gnd#CollectiveManuscript](https://d-nb.info/standards/elementset/gnd#CollectiveManuscript)
 - **Expression**: ((Expression)[https://d-nb.info/standards/elementset/gnd#Expression](https://d-nb.info/standards/elementset/gnd#Expression)
 - **Manuscript**: ((Manuscript)[https://d-nb.info/standards/elementset/gnd#Manuscript](https://d-nb.info/standards/elementset/gnd#Manuscript)
 - **MusicalWork**: ((MusicalWork)[https://d-nb.info/standards/elementset/gnd#MusicalWork](https://d-nb.info/standards/elementset/gnd#MusicalWork)
 - **ProvenanceCharacteristic**: ((ProvenanceCharacteristic)[https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic](https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic)
 - **VersionOfAMusicalWork**: ((VersionOfAMusicalWork)[https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork](https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork)
 - **SubjectHeading**: ((SubjectHeading)[https://d-nb.info/standards/elementset/gnd#SubjectHeading](https://d-nb.info/standards/elementset/gnd#SubjectHeading)

---

<p style="margin-bottom:60px"></p>

<a name="d17e4864"></a>
#### Language Code
|     |     |
| --- | --- |
| **Name** | `@gndo:code`  |
| **Datatype** | string |
| **Label** (de) | Language Code |
| **Description** (de) | Ein Sprachcode aus dem [LOC ISO 693-2 Language Vocabulary](http://id.loc.gov/vocabulary/iso639-2/) |
| **Contained by** | [`gndo:languageCode`](#d17e4843) |

***Predefined Values***  

 - **aar**: (Afar): aar (ISO 639-3), aa (ISO 639-1); Afar [Afar]: Einzelsprache, Lebend
 - **abk**: (Abkhazian): abk (ISO 639-3), ab (ISO 639-1); Abchasisch [Abkhazian]: Einzelsprache, Lebend
 - **ace**: (Achinese): ace (ISO 639-3); Achinesisch [Achinese]: Einzelsprache, Lebend
 - **ach**: (Acoli): ach (ISO 639-3); Acholi [Acoli]: Einzelsprache, Lebend
 - **ada**: (Adangme): ada (ISO 639-3); Dangme [Adangme]: Einzelsprache, Lebend
 - **ady**: (Adyghe; Adygei): ady (ISO 639-3); Adygeisch [Adyghe; Adygei]: Einzelsprache, Lebend
 - **afa**: (Afro-Asiatic languages): afa (ISO 639-5); Afroasiatische Sprachen [Afro-Asiatic languages]: Sprachfamilie
 - **afh**: (Afrihili): afh (ISO 639-3); Afrihili [Afrihili]: Einzelsprache, Konstruiert
 - **afr**: (Afrikaans): afr (ISO 639-3), af (ISO 639-1); Afrikaans [Afrikaans]: Einzelsprache, Lebend
 - **ain**: (Ainu): ain (ISO 639-3); Ainu [Ainu]: Einzelsprache, Lebend
 - **aka**: (Akan): aka (ISO 639-3), ak (ISO 639-1); Akan [Akan]: Makrosprache, Lebend
 - **akk**: (Akkadian): akk (ISO 639-3); Akkadisch [Akkadian]: Einzelsprache, Alt
 - **ale**: (Aleut): ale (ISO 639-3); Aleutisch [Aleut]: Einzelsprache, Lebend
 - **alg**: (Algonquian languages): alg (ISO 639-5); Algonkin-Sprachen [Algonquian languages]: Sprachfamilie
 - **alt**: (Southern Altai): alt (ISO 639-3); Südaltaisch [Southern Altai]: Einzelsprache, Lebend
 - **amh**: (Amharic): amh (ISO 639-3), am (ISO 639-1); Amharisch [Amharic]: Einzelsprache, Lebend
 - **ang**: (English, Old (ca. 450–1100)): ang (ISO 639-3); Altenglisch [English, Old (ca. 450–1100)]: Einzelsprache, Historisch
 - **anp**: (Angika): anp (ISO 639-3); Angika [Angika]: Einzelsprache, Lebend
 - **apa**: (Apache languages): apa (ISO 639-5); Apache-Sprachen [Apache languages]: Sprachfamilie
 - **ara**: (Arabic): ara (ISO 639-3), ar (ISO 639-1); Arabisch [Arabic]: Makrosprache, Lebend
 - **arc**: (Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)): arc (ISO 639-3); Reichsaramäisch [Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)]: Einzelsprache, Alt
 - **arg**: (Aragonese): arg (ISO 639-3), an (ISO 639-1); Aragonesisch [Aragonese]: Einzelsprache, Lebend
 - **arn**: (Mapudungun; Mapuche): arn (ISO 639-3); Mapudungun [Mapudungun; Mapuche]: Einzelsprache, Lebend
 - **arp**: (Arapaho): arp (ISO 639-3); Arapaho [Arapaho]: Einzelsprache, Lebend
 - **art**: (Artificial languages): art (ISO 639-5); Konstruierte Sprachen [Artificial languages]: Sprachfamilie
 - **arw**: (Arawak): arw (ISO 639-3); Arawak [Arawak]: Einzelsprache, Lebend
 - **asm**: (Assamese): asm (ISO 639-3), as (ISO 639-1); Assamesisch [Assamese]: Einzelsprache, Lebend
 - **ast**: (Asturian; Bable; Leonese; Asturleonese): ast (ISO 639-3); Asturisch [Asturian; Bable; Leonese; Asturleonese]: Einzelsprache, Lebend
 - **ath**: (Athapascan languages): ath (ISO 639-5); Athapaskische Sprachen [Athapascan languages]: Sprachfamilie
 - **aus**: (Australian languages): aus (ISO 639-5); Australische Sprachen [Australian languages]: Sprachfamilie
 - **ava**: (Avaric): ava (ISO 639-3), av (ISO 639-1); Awarisch [Avaric]: Einzelsprache, Lebend
 - **ave**: (Avestan): ave (ISO 639-3), ae (ISO 639-1); Avestisch [Avestan]: Einzelsprache, Alt
 - **awa**: (Awadhi): awa (ISO 639-3); Awadhi [Awadhi]: Einzelsprache, Lebend
 - **aym**: (Aymara): aym (ISO 639-3), ay (ISO 639-1); Aymara [Aymara]: Makrosprache, Lebend
 - **aze**: (Azerbaijani): aze (ISO 639-3), az (ISO 639-1); Aserbaidschanisch [Azerbaijani]: Makrosprache, Lebend
 - **bad**: (Banda languages): bad (ISO 639-5); Banda-Sprachen [Banda languages]: Sprachfamilie
 - **bai**: (Bamileke languages): bai (ISO 639-5); Bamileke-Sprachen [Bamileke languages]: Sprachfamilie
 - **bak**: (Bashkir): bak (ISO 639-3), ba (ISO 639-1); Baschkirisch [Bashkir]: Einzelsprache, Lebend
 - **bal**: (Baluchi): bal (ISO 639-3); Belutschisch [Baluchi]: Makrosprache, Lebend
 - **bam**: (Bambara): bam (ISO 639-3), bm (ISO 639-1); Bambara [Bambara]: Einzelsprache, Lebend
 - **ban**: (Balinese): ban (ISO 639-3); Balinesisch [Balinese]: Einzelsprache, Lebend
 - **bas**: (Basa): bas (ISO 639-3); Bassa [Basa]: Einzelsprache, Lebend
 - **bat**: (Baltic languages): bat (ISO 639-5); Baltische Sprachen [Baltic languages]: Sprachfamilie
 - **bej**: (Beja; Bedawiyet): bej (ISO 639-3); Bedscha [Beja; Bedawiyet]: Einzelsprache, Lebend
 - **bel**: (Belarusian): bel (ISO 639-3), be (ISO 639-1); Belarussisch [Belarusian]: Einzelsprache, Lebend
 - **bem**: (Bemba): bem (ISO 639-3); Bemba [Bemba]: Einzelsprache, Lebend
 - **ben**: (Bengali): ben (ISO 639-3), bn (ISO 639-1); Bengalisch [Bengali]: Einzelsprache, Lebend
 - **ber**: (Berber languages): ber (ISO 639-5); Berbersprachen [Berber languages]: Sprachfamilie
 - **bho**: (Bhojpuri): bho (ISO 639-3); Bhojpuri [Bhojpuri]: Einzelsprache, Lebend
 - **bih**: (Bihari languages): bih (ISO 639-5), bh (ISO 639-1); Bihari [Bihari languages]: Sprachfamilie
 - **bik**: (Bikol): bik (ISO 639-3); Bikolano [Bikol]: Makrosprache, Lebend
 - **bin**: (Bini; Edo): bin (ISO 639-3); Edo [Bini; Edo]: Einzelsprache, Lebend
 - **bis**: (Bislama): bis (ISO 639-3), bi (ISO 639-1); Bislama [Bislama]: Einzelsprache, Lebend
 - **bla**: (Siksika): bla (ISO 639-3); Blackfoot [Siksika]: Einzelsprache, Lebend
 - **bnt**: (Bantu (Other)): bnt (ISO 639-5); Bantusprachen [Bantu (Other)]: Sprachfamilie
 - **bos**: (Bosnian): bos (ISO 639-3), bs (ISO 639-1); Bosnisch [Bosnian]: Einzelsprache, Lebend
 - **bra**: (Braj): bra (ISO 639-3); Braj-Bhakha [Braj]: Einzelsprache, Lebend
 - **bre**: (Breton): bre (ISO 639-3), br (ISO 639-1); Bretonisch [Breton]: Einzelsprache, Lebend
 - **btk**: (Batak languages): btk (ISO 639-5); Bataksprachen [Batak languages]: Sprachfamilie
 - **bua**: (Buriat): bua (ISO 639-3); Burjatisch [Buriat]: Makrosprache, Lebend
 - **bug**: (Buginese): bug (ISO 639-3); Buginesisch [Buginese]: Einzelsprache, Lebend
 - **bul**: (Bulgarian): bul (ISO 639-3), bg (ISO 639-1); Bulgarisch [Bulgarian]: Einzelsprache, Lebend
 - **byn**: (Blin; Bilin): byn (ISO 639-3); Blin [Blin; Bilin]: Einzelsprache, Lebend
 - **cad**: (Caddo): cad (ISO 639-3); Caddo [Caddo]: Einzelsprache, Lebend
 - **cai**: (Central American Indian languages): cai (ISO 639-5); Mesoamerikanische Sprachen [Central American Indian languages]: Sprachfamilie
 - **car**: (Galibi Carib): car (ISO 639-3); Karib [Galibi Carib]: Einzelsprache, Lebend
 - **cat**: (Catalan; Valencian): cat (ISO 639-3), ca (ISO 639-1); Katalanisch, Valencianisch [Catalan; Valencian]: Einzelsprache, Lebend
 - **cau**: (Caucasian languages): cau (ISO 639-5); Kaukasische Sprachen [Caucasian languages]: Sprachfamilie
 - **ceb**: (Cebuano): ceb (ISO 639-3); Cebuano [Cebuano]: Einzelsprache, Lebend
 - **cel**: (Celtic languages): cel (ISO 639-5); Keltische Sprachen [Celtic languages]: Sprachfamilie
 - **cha**: (Chamorro): cha (ISO 639-3), ch (ISO 639-1); Chamorro [Chamorro]: Einzelsprache, Lebend
 - **chb**: (Chibcha): chb (ISO 639-3); Chibcha [Chibcha]: Einzelsprache, Ausgestorben
 - **che**: (Chechen): che (ISO 639-3), ce (ISO 639-1); Tschetschenisch [Chechen]: Einzelsprache, Lebend
 - **chg**: (Chagatai): chg (ISO 639-3); Tschagataisch [Chagatai]: Einzelsprache, Ausgestorben
 - **chk**: (Chuukese): chk (ISO 639-3); Chuukesisch [Chuukese]: Einzelsprache, Lebend
 - **chm**: (Mari): chm (ISO 639-3); Mari [Mari]: Makrosprache, Lebend
 - **chn**: (Chinook jargon): chn (ISO 639-3); Chinook Wawa [Chinook jargon]: Einzelsprache, Lebend
 - **cho**: (Choctaw): cho (ISO 639-3); Choctaw [Choctaw]: Einzelsprache, Lebend
 - **chp**: (Chipewyan; Dene Suline): chp (ISO 639-3); Chipewyan [Chipewyan; Dene Suline]: Einzelsprache, Lebend
 - **chr**: (Cherokee): chr (ISO 639-3); Cherokee [Cherokee]: Einzelsprache, Lebend
 - **chu**: (Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic): chu (ISO 639-3), cu (ISO 639-1); Kirchenslawisch, Altkirchenslawisch [Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic]: Einzelsprache, Alt
 - **chv**: (Chuvash): chv (ISO 639-3), cv (ISO 639-1); Tschuwaschisch [Chuvash]: Einzelsprache, Lebend
 - **chy**: (Cheyenne): chy (ISO 639-3); Cheyenne [Cheyenne]: Einzelsprache, Lebend
 - **cmc**: (Chamic languages): cmc (ISO 639-5); Chamische Sprachen [Chamic languages]: Sprachfamilie
 - **cnr**: (Montenegrin): cnr (ISO 639-3); Montenegrinisch [Montenegrin]: Einzelsprache, Lebend
 - **cop**: (Coptic): cop (ISO 639-3); Koptisch [Coptic]: Einzelsprache, Ausgestorben
 - **cor**: (Cornish): cor (ISO 639-3), kw (ISO 639-1); Kornisch [Cornish]: Einzelsprache, Lebend
 - **cos**: (Corsican): cos (ISO 639-3), co (ISO 639-1); Korsisch [Corsican]: Einzelsprache, Lebend
 - **cpe**: (Creoles and pidgins, English based): cpe (ISO 639-5); Englisch-basierte Kreols und Pidgins [Creoles and pidgins, English based]: Sprachfamilie
 - **cpf**: (Creoles and pidgins, French-based): cpf (ISO 639-5); Französisch-basierte Kreols und Pidgins [Creoles and pidgins, French-based]: Sprachfamilie
 - **cpp**: (Creoles and pidgins, Portuguese-based): cpp (ISO 639-5); Portugiesisch-basierte Kreols und Pidgins [Creoles and pidgins, Portuguese-based]: Sprachfamilie
 - **cre**: (Cree): cre (ISO 639-3), cr (ISO 639-1); Cree [Cree]: Makrosprache, Lebend
 - **crh**: (Crimean Tatar; Crimean Turkish): crh (ISO 639-3); Krimtatarisch [Crimean Tatar; Crimean Turkish]: Einzelsprache, Lebend
 - **crp**: (Creoles and pidgins): crp (ISO 639-5); Kreol- und Pidginsprachen [Creoles and pidgins]: Sprachfamilie
 - **csb**: (Kashubian): csb (ISO 639-3); Kaschubisch [Kashubian]: Einzelsprache, Lebend
 - **cus**: (Cushitic languages): cus (ISO 639-5); Kuschitische Sprachen [Cushitic languages]: Sprachfamilie
 - **dak**: (Dakota): dak (ISO 639-3); Dakota [Dakota]: Einzelsprache, Lebend
 - **dan**: (Danish): dan (ISO 639-3), da (ISO 639-1); Dänisch [Danish]: Einzelsprache, Lebend
 - **dar**: (Dargwa): dar (ISO 639-3); Darginisch [Dargwa]: Einzelsprache, Lebend
 - **day**: (Land Dayak languages): day (ISO 639-5); Land-Dayak-Sprachen [Land Dayak languages]: Sprachfamilie
 - **del**: (Delaware): del (ISO 639-3); Delawarisch [Delaware]: Makrosprache, Lebend
 - **den**: (Slave (Athapascan)): den (ISO 639-3); Slavey [Slave (Athapascan)]: Makrosprache, Lebend
 - **dgr**: (Dogrib): dgr (ISO 639-3); Dogrib [Dogrib]: Einzelsprache, Lebend
 - **din**: (Dinka): din (ISO 639-3); Dinka [Dinka]: Makrosprache, Lebend
 - **div**: (Divehi; Dhivehi; Maldivian): div (ISO 639-3), dv (ISO 639-1); Dhivehi [Divehi; Dhivehi; Maldivian]: Einzelsprache, Lebend
 - **doi**: (Dogri): doi (ISO 639-3); Dogri [Dogri]: Makrosprache, Lebend
 - **dra**: (Dravidian languages): dra (ISO 639-5); Dravidische Sprachen [Dravidian languages]: Sprachfamilie
 - **dsb**: (Lower Sorbian): dsb (ISO 639-3); Niedersorbisch [Lower Sorbian]: Einzelsprache, Lebend
 - **dua**: (Duala): dua (ISO 639-3); Duala [Duala]: Einzelsprache, Lebend
 - **dum**: (Dutch, Middle (ca. 1050–1350)): dum (ISO 639-3); Mittelniederländisch [Dutch, Middle (ca. 1050–1350)]: Einzelsprache, Historisch
 - **dyu**: (Dyula): dyu (ISO 639-3); Dioula [Dyula]: Einzelsprache, Lebend
 - **dzo**: (Dzongkha): dzo (ISO 639-3), dz (ISO 639-1); Dzongkha [Dzongkha]: Einzelsprache, Lebend
 - **efi**: (Efik): efi (ISO 639-3); Efik [Efik]: Einzelsprache, Lebend
 - **egy**: (Egyptian (Ancient)): egy (ISO 639-3); Ägyptisch [Egyptian (Ancient)]: Einzelsprache, Alt
 - **eka**: (Ekajuk): eka (ISO 639-3); Ekajuk [Ekajuk]: Einzelsprache, Lebend
 - **elx**: (Elamite): elx (ISO 639-3); Elamisch [Elamite]: Einzelsprache, Alt
 - **eng**: (English): eng (ISO 639-3), en (ISO 639-1); Englisch [English]: Einzelsprache, Lebend
 - **enm**: (English, Middle (1100–1500)): enm (ISO 639-3); Mittelenglisch [English, Middle (1100–1500)]: Einzelsprache, Historisch
 - **epo**: (Esperanto): epo (ISO 639-3), eo (ISO 639-1); Esperanto [Esperanto]: Einzelsprache, Konstruiert
 - **est**: (Estonian): est (ISO 639-3), et (ISO 639-1); Estnisch [Estonian]: Makrosprache, Lebend
 - **ewe**: (Ewe): ewe (ISO 639-3), ee (ISO 639-1); Ewe [Ewe]: Einzelsprache, Lebend
 - **ewo**: (Ewondo): ewo (ISO 639-3); Ewondo [Ewondo]: Einzelsprache, Lebend
 - **fan**: (Fang): fan (ISO 639-3); Fang [Fang]: Einzelsprache, Lebend
 - **fao**: (Faroese): fao (ISO 639-3), fo (ISO 639-1); Färöisch [Faroese]: Einzelsprache, Lebend
 - **fat**: (Fanti): fat (ISO 639-3); Fante [Fanti]: Einzelsprache, Lebend
 - **fij**: (Fijian): fij (ISO 639-3), fj (ISO 639-1); Fidschi [Fijian]: Einzelsprache, Lebend
 - **fil**: (Filipino; Pilipino): fil (ISO 639-3); Filipino [Filipino; Pilipino]: Einzelsprache, Lebend
 - **fin**: (Finnish): fin (ISO 639-3), fi (ISO 639-1); Finnisch [Finnish]: Einzelsprache, Lebend
 - **fiu**: (Finno-Ugrian languages): fiu (ISO 639-5); Finno-ugrische Sprachen [Finno-Ugrian languages]: Sprachfamilie
 - **fon**: (Fon): fon (ISO 639-3); Fon [Fon]: Einzelsprache, Lebend
 - **frm**: (French, Middle (ca. 1400–1600)): frm (ISO 639-3); Mittelfranzösisch [French, Middle (ca. 1400–1600)]: Einzelsprache, Historisch
 - **fro**: (French, Old (842–ca. 1400)): fro (ISO 639-3); Altfranzösisch [French, Old (842–ca. 1400)]: Einzelsprache, Historisch
 - **frr**: (Northern Frisian): frr (ISO 639-3); Nordfriesisch [Northern Frisian]: Einzelsprache, Lebend
 - **frs**: (East Frisian Low Saxon): frs (ISO 639-3); Ostfriesisches Platt [East Frisian Low Saxon]: Einzelsprache, Lebend
 - **fry**: (Western Frisian): fry (ISO 639-3), fy (ISO 639-1); Westfriesisch [Western Frisian]: Einzelsprache, Lebend
 - **ful**: (Fulah): ful (ISO 639-3), ff (ISO 639-1); Fulfulde [Fulah]: Makrosprache, Lebend
 - **fur**: (Friulian): fur (ISO 639-3); Furlanisch [Friulian]: Einzelsprache, Lebend
 - **gaa**: (Einzelsprache): gaa (ISO 639-3), Ga (ISO 639-1); Ga [Einzelsprache]: Lebend, Gã 
 - **gay**: (Gayo): gay (ISO 639-3); Gayo [Gayo]: Einzelsprache, Lebend
 - **gba**: (Gbaya): gba (ISO 639-3); Gbaya-Sprachen [Gbaya]: Makrosprache, Lebend
 - **gem**: (Germanic languages): gem (ISO 639-5); Germanische Sprachen [Germanic languages]: Sprachfamilie
 - **gez**: (Geez): gez (ISO 639-3); Altäthiopisch [Geez]: Einzelsprache, Alt
 - **gil**: (Gilbertese): gil (ISO 639-3); Kiribatisch, Gilbertesisch [Gilbertese]: Einzelsprache, Lebend
 - **gla**: (Gaelic; Scottish Gaelic): gla (ISO 639-3), gd (ISO 639-1); Schottisch-gälisch [Gaelic; Scottish Gaelic]: Einzelsprache, Lebend
 - **gle**: (Irish): gle (ISO 639-3), ga (ISO 639-1); Irisch [Irish]: Einzelsprache, Lebend
 - **glg**: (Galician): glg (ISO 639-3), gl (ISO 639-1); Galicisch, Galegisch [Galician]: Einzelsprache, Lebend
 - **glv**: (Manx): glv (ISO 639-3), gv (ISO 639-1); Manx,Manx-Gälisch [Manx]: Einzelsprache, Lebend
 - **gmh**: (German, Middle High (ca. 1050–1500)): gmh (ISO 639-3); Mittelhochdeutsch [German, Middle High (ca. 1050–1500)]: Einzelsprache, Historisch
 - **goh**: (German, Old High (ca. 750–1050)): goh (ISO 639-3); Althochdeutsch [German, Old High (ca. 750–1050)]: Einzelsprache, Historisch
 - **gon**: (Gondi): gon (ISO 639-3); Gondi [Gondi]: Makrosprache, Lebend
 - **gor**: (Gorontalo): gor (ISO 639-3); Gorontalo [Gorontalo]: Einzelsprache, Lebend
 - **got**: (Gothic): got (ISO 639-3); Gotisch [Gothic]: Einzelsprache, Alt
 - **grb**: (Grebo): grb (ISO 639-3); Grebo [Grebo]: Makrosprache, Lebend
 - **grc**: (Greek, Ancient (bis 1453)): grc (ISO 639-3); Altgriechisch [Greek, Ancient (bis 1453)]: Einzelsprache, Historisch
 - **grn**: (Guarani): grn (ISO 639-3), gn (ISO 639-1); Guaraní [Guarani]: Makrosprache, Lebend
 - **gsw**: (Swiss German; Alemannic; Alsatian): gsw (ISO 639-3); Schweizerdeutsch [Swiss German; Alemannic; Alsatian]: Einzelsprache, Lebend
 - **guj**: (Gujarati): guj (ISO 639-3), gu (ISO 639-1); Gujarati [Gujarati]: Einzelsprache, Lebend
 - **gwi**: (Gwich'in): gwi (ISO 639-3); Gwich'in (Sprache) [Gwich'in]: Einzelsprache, Lebend
 - **hai**: (Haida): hai (ISO 639-3); Haida [Haida]: Makrosprache, Lebend
 - **hat**: (Haitian; Haitian Creole): hat (ISO 639-3), ht (ISO 639-1); Haitianisch-Kreolisch [Haitian; Haitian Creole]: Einzelsprache, Lebend
 - **hau**: (Hausa): hau (ISO 639-3), ha (ISO 639-1); Hausa [Hausa]: Einzelsprache, Lebend
 - **haw**: (Hawaiian): haw (ISO 639-3); Hawaiisch [Hawaiian]: Einzelsprache, Lebend
 - **heb**: (Hebrew): heb (ISO 639-3), he (ISO 639-1); Hebräisch [Hebrew]: Einzelsprache, Lebend
 - **her**: (Herero): her (ISO 639-3), hz (ISO 639-1); Otjiherero [Herero]: Einzelsprache, Lebend
 - **hil**: (Hiligaynon): hil (ISO 639-3); Hiligaynon [Hiligaynon]: Einzelsprache, Lebend
 - **him**: (Himachali languages; Western Pahari languages): him (ISO 639-5); West-Paharisprachen [Himachali languages; Western Pahari languages]: Sprachfamilie
 - **hin**: (Hindi): hin (ISO 639-3), hi (ISO 639-1); Hindi [Hindi]: Einzelsprache, Lebend
 - **hit**: (Hittite): hit (ISO 639-3); Hethitisch [Hittite]: Einzelsprache, Alt
 - **hmn**: (Hmong; Mong): hmn (ISO 639-3); Hmong-Sprache [Hmong; Mong]: Makrosprache, Lebend
 - **hmo**: (Hiri Motu): hmo (ISO 639-3), ho (ISO 639-1); Hiri Motu [Hiri Motu]: Einzelsprache, Lebend
 - **hrv**: (Croatian): hrv (ISO 639-3), hr (ISO 639-1); Kroatisch [Croatian]: Einzelsprache, Lebend
 - **hsb**: (Upper Sorbian): hsb (ISO 639-3); Obersorbisch [Upper Sorbian]: Einzelsprache, Lebend
 - **hun**: (Hungarian): hun (ISO 639-3), hu (ISO 639-1); Ungarisch [Hungarian]: Einzelsprache, Lebend
 - **hup**: (Hupa): hup (ISO 639-3); Hoopa [Hupa]: Einzelsprache, Lebend
 - **iba**: (Iban): iba (ISO 639-3); Iban [Iban]: Einzelsprache, Lebend
 - **ibo**: (Igbo): ibo (ISO 639-3), ig (ISO 639-1); Igbo [Igbo]: Einzelsprache, Lebend
 - **ido**: (Ido): ido (ISO 639-3), io (ISO 639-1); Ido [Ido]: Einzelsprache, Konstruiert
 - **iii**: (Sichuan Yi; Nuosu): iii (ISO 639-3), ii (ISO 639-1); Yi [Sichuan Yi; Nuosu]: Einzelsprache, Lebend
 - **ijo**: (Ijo languages): ijo (ISO 639-5); Ijo-Sprachen [Ijo languages]: Sprachfamilie
 - **iku**: (Inuktitut): iku (ISO 639-3), iu (ISO 639-1); Inuktitut [Inuktitut]: Makrosprache, Lebend
 - **ile**: (Interlingue; Occidental): ile (ISO 639-3), ie (ISO 639-1); Interlingue [Interlingue; Occidental]: Einzelsprache, Konstruiert
 - **ilo**: (Iloko): ilo (ISO 639-3); Ilokano [Iloko]: Einzelsprache, Lebend
 - **ina**: (Interlingua (International Auxiliary Language Association)): ina (ISO 639-3), ia (ISO 639-1); Interlingua [Interlingua (International Auxiliary Language Association)]: Einzelsprache, Konstruiert
 - **inc**: (Indic languages): inc (ISO 639-5); Indoarische Sprachen [Indic languages]: Sprachfamilie
 - **ind**: (Indonesian): ind (ISO 639-3), id (ISO 639-1); Indonesisch [Indonesian]: Einzelsprache, Lebend
 - **ine**: (Indo-European languages): ine (ISO 639-5); Indogermanische Sprachen [Indo-European languages]: Sprachfamilie
 - **inh**: (Ingush): inh (ISO 639-3); Inguschisch [Ingush]: Einzelsprache, Lebend
 - **ipk**: (Inupiaq): ipk (ISO 639-3), ik (ISO 639-1); Inupiaq [Inupiaq]: Makrosprache, Lebend
 - **ira**: (Iranian languages): ira (ISO 639-5); Iranische Sprachen [Iranian languages]: Sprachfamilie
 - **iro**: (Iroquoian languages): iro (ISO 639-5); Irokesische Sprachen [Iroquoian languages]: Sprachfamilie
 - **ita**: (Italian): ita (ISO 639-3), it (ISO 639-1); Italienisch [Italian]: Einzelsprache, Lebend
 - **jav**: (Javanese): jav (ISO 639-3), jv (ISO 639-1); Javanisch [Javanese]: Einzelsprache, Lebend
 - **jbo**: (Lojban): jbo (ISO 639-3); Lojban [Lojban]: Einzelsprache, Konstruiert
 - **jpn**: (Japanese): jpn (ISO 639-3), ja (ISO 639-1); Japanisch [Japanese]: Einzelsprache, Lebend
 - **jpr**: (Judeo-Persian): jpr (ISO 639-3); Judäo-Persisch [Judeo-Persian]: Einzelsprache, Lebend
 - **jrb**: (Judeo-Arabic): jrb (ISO 639-3); Judäo-Arabisch [Judeo-Arabic]: Makrosprache, Lebend
 - **kaa**: (Kara-Kalpak): kaa (ISO 639-3); Karakalpakisch [Kara-Kalpak]: Einzelsprache, Lebend
 - **kab**: (Kabyle): kab (ISO 639-3); Kabylisch [Kabyle]: Einzelsprache, Lebend
 - **kac**: (Kachin; Jingpho): kac (ISO 639-3); Jingpo [Kachin; Jingpho]: Einzelsprache, Lebend
 - **kal**: (Kalaallisut; Greenlandic): kal (ISO 639-3), kl (ISO 639-1); Grönländisch, Kalaallisut [Kalaallisut; Greenlandic]: Einzelsprache, Lebend
 - **kam**: (Kamba): kam (ISO 639-3); Kikamba [Kamba]: Einzelsprache, Lebend
 - **kan**: (Kannada): kan (ISO 639-3), kn (ISO 639-1); Kannada [Kannada]: Einzelsprache, Lebend
 - **kar**: (Karen languages): kar (ISO 639-5); Karenische Sprachen [Karen languages]: Sprachfamilie
 - **kas**: (Kashmiri): kas (ISO 639-3), ks (ISO 639-1); Kashmiri [Kashmiri]: Einzelsprache, Lebend
 - **kau**: (Kanuri): kau (ISO 639-3), kr (ISO 639-1); Kanuri [Kanuri]: Makrosprache, Lebend
 - **kaw**: (Kawi): kaw (ISO 639-3); Kawi, Altjavanisch [Kawi]: Einzelsprache, Alt
 - **kaz**: (Kazakh): kaz (ISO 639-3), kk (ISO 639-1); Kasachisch [Kazakh]: Einzelsprache, Lebend
 - **kbd**: (Kabardian): kbd (ISO 639-3); Kabardinisch, Ost-Tscherkessisch [Kabardian]: Einzelsprache, Lebend
 - **kha**: (Khasi): kha (ISO 639-3); Khasi [Khasi]: Einzelsprache, Lebend
 - **khi**: (Khoisan languages): khi (ISO 639-5); Khoisansprachen [Khoisan languages]: Sprachfamilie
 - **khm**: (Central Khmer): khm (ISO 639-3), km (ISO 639-1); Khmer [Central Khmer]: Einzelsprache, Lebend
 - **kho**: (Khotanese; Sakan): kho (ISO 639-3); Khotanesisch [Khotanese; Sakan]: Einzelsprache, Alt
 - **kik**: (Kikuyu; Gikuyu): kik (ISO 639-3), ki (ISO 639-1); Kikuyu [Kikuyu; Gikuyu]: Einzelsprache, Lebend
 - **kin**: (Kinyarwanda): kin (ISO 639-3), rw (ISO 639-1); Kinyarwanda, Ruandisch [Kinyarwanda]: Einzelsprache, Lebend
 - **kir**: (Kirghiz; Kyrgyz): kir (ISO 639-3), ky (ISO 639-1); Kirgisisch [Kirghiz; Kyrgyz]: Einzelsprache, Lebend
 - **kmb**: (Kimbundu): kmb (ISO 639-3); Kimbundu [Kimbundu]: Einzelsprache, Lebend
 - **kok**: (Konkani): kok (ISO 639-3); Konkani [Konkani]: Makrosprache, Lebend
 - **kom**: (Komi): kom (ISO 639-3), kv (ISO 639-1); Komi [Komi]: Makrosprache, Lebend
 - **kon**: (Kongo): kon (ISO 639-3), kg (ISO 639-1); Kikongo [Kongo]: Makrosprache, Lebend
 - **kor**: (Korean): kor (ISO 639-3), ko (ISO 639-1); Koreanisch [Korean]: Einzelsprache, Lebend
 - **kos**: (Kosraean): kos (ISO 639-3); Kosraeanisch [Kosraean]: Einzelsprache, Lebend
 - **kpe**: (Kpelle): kpe (ISO 639-3); Kpelle [Kpelle]: Makrosprache, Lebend
 - **krc**: (Karachay-Balkar): krc (ISO 639-3); Karatschai-balkarisch [Karachay-Balkar]: Einzelsprache, Lebend
 - **krl**: (Karelian): krl (ISO 639-3); Karelisch [Karelian]: Einzelsprache, Lebend
 - **kro**: (Kru languages): kro (ISO 639-5); Kru-Sprachen [Kru languages]: Sprachfamilie
 - **kru**: (Kurukh): kru (ISO 639-3); Kurukh [Kurukh]: Einzelsprache, Lebend
 - **kua**: (Kuanyama; Kwanyama): kua (ISO 639-3), kj (ISO 639-1); oshiKwanyama [Kuanyama; Kwanyama]: Einzelsprache, Lebend
 - **kum**: (Kumyk): kum (ISO 639-3); Kumykisch [Kumyk]: Einzelsprache, Lebend
 - **kur**: (Kurdish): kur (ISO 639-3), ku (ISO 639-1); Kurdisch [Kurdish]: Makrosprache, Lebend
 - **kut**: (Kutenai): kut (ISO 639-3); Kutanaha [Kutenai]: Einzelsprache, Lebend
 - **lad**: (Ladino): lad (ISO 639-3); Judenspanisch, Ladino, Sephardisch [Ladino]: Einzelsprache, Lebend
 - **lah**: (Lahnda): lah (ISO 639-3); Lahnda, Westpanjabi [Lahnda]: Makrosprache, Lebend
 - **lam**: (Lamba): lam (ISO 639-3); Lamba [Lamba]: Einzelsprache, Lebend
 - **lao**: (Lao): lao (ISO 639-3), lo (ISO 639-1); Laotisch [Lao]: Einzelsprache, Lebend
 - **lat**: (Latin): lat (ISO 639-3), la (ISO 639-1); Latein [Latin]: Einzelsprache, Alt
 - **lav**: (Latvian): lav (ISO 639-3), lv (ISO 639-1); Lettisch [Latvian]: Makrosprache, Lebend
 - **lez**: (Lezghian): lez (ISO 639-3); Lesgisch [Lezghian]: Einzelsprache, Lebend
 - **lim**: (Limburgan; Limburger; Limburgish): lim (ISO 639-3), li (ISO 639-1); Limburgisch, Südniederfränkisch [Limburgan; Limburger; Limburgish]: Einzelsprache, Lebend
 - **lin**: (Lingala): lin (ISO 639-3), ln (ISO 639-1); Lingála [Lingala]: Einzelsprache, Lebend
 - **lit**: (Lithuanian): lit (ISO 639-3), lt (ISO 639-1); Litauisch [Lithuanian]: Einzelsprache, Lebend
 - **lol**: (Mongo): lol (ISO 639-3); Lomongo [Mongo]: Einzelsprache, Lebend
 - **loz**: (Lozi): loz (ISO 639-3); Lozi [Lozi]: Einzelsprache, Lebend
 - **ltz**: (Luxembourgish; Letzeburgesch): ltz (ISO 639-3), lb (ISO 639-1); Luxemburgisch [Luxembourgish; Letzeburgesch]: Einzelsprache, Lebend
 - **lua**: (Luba-Lulua): lua (ISO 639-3); Tschiluba [Luba-Lulua]: Einzelsprache, Lebend
 - **lub**: (Luba-Katanga): lub (ISO 639-3), lu (ISO 639-1); Kiluba [Luba-Katanga]: Einzelsprache, Lebend
 - **lug**: (Ganda): lug (ISO 639-3), lg (ISO 639-1); Luganda [Ganda]: Einzelsprache, Lebend
 - **lui**: (Luiseno): lui (ISO 639-3); Luiseño [Luiseno]: Einzelsprache, Ausgestorben
 - **lun**: (Lunda): lun (ISO 639-3); Chilunda [Lunda]: Einzelsprache, Lebend
 - **luo**: (Luo (Kenya and Tanzania)): luo (ISO 639-3); Luo [Luo (Kenya and Tanzania)]: Einzelsprache, Lebend
 - **lus**: (Lushai): lus (ISO 639-3); Mizo, Lushai [Lushai]: Einzelsprache, Lebend
 - **mad**: (Madurese): mad (ISO 639-3); Maduresisch [Madurese]: Einzelsprache, Lebend
 - **mag**: (Magahi): mag (ISO 639-3); Magadhi [Magahi]: Einzelsprache, Lebend
 - **mah**: (Marshallese): mah (ISO 639-3), mh (ISO 639-1); Marshallesisch [Marshallese]: Einzelsprache, Lebend
 - **mai**: (Maithili): mai (ISO 639-3); Maithili [Maithili]: Einzelsprache, Lebend
 - **mak**: (Makasar): mak (ISO 639-3); Makassar [Makasar]: Einzelsprache, Lebend
 - **mal**: (Malayalam): mal (ISO 639-3), ml (ISO 639-1); Malayalam [Malayalam]: Einzelsprache, Lebend
 - **man**: (Mandingo): man (ISO 639-3); Manding [Mandingo]: Makrosprache, Lebend
 - **map**: (Austronesian languages): map (ISO 639-5); Austronesische Sprachen [Austronesian languages]: Sprachfamilie
 - **mar**: (Marathi): mar (ISO 639-3), mr (ISO 639-1); Marathi [Marathi]: Einzelsprache, Lebend
 - **mas**: (Masai): mas (ISO 639-3); Maa, Kimaasai [Masai]: Einzelsprache, Lebend
 - **mdf**: (Moksha): mdf (ISO 639-3); Mokschanisch [Moksha]: Einzelsprache, Lebend
 - **mdr**: (Mandar): mdr (ISO 639-3); Mandar [Mandar]: Einzelsprache, Lebend
 - **men**: (Mende): men (ISO 639-3); Mende [Mende]: Einzelsprache, Lebend
 - **mga**: (Irish, Middle (900–1200)): mga (ISO 639-3); Mittelirisch [Irish, Middle (900–1200)]: Einzelsprache, Historisch
 - **mic**: (Mi'kmaq; Micmac): mic (ISO 639-3); Míkmawísimk [Mi'kmaq; Micmac]: Einzelsprache, Lebend
 - **min**: (Minangkabau): min (ISO 639-3); Minangkabauisch [Minangkabau]: Einzelsprache, Lebend
 - **mis**: (Uncoded languages): mis (ISO 639-3); „Unkodiert“ [Uncoded languages]: Speziell
 - **mkh**: (Mon-Khmer languages): mkh (ISO 639-5); Mon-Khmer-Sprachen [Mon-Khmer languages]: Sprachfamilie
 - **mlg**: (Malagasy): mlg (ISO 639-3), mg (ISO 639-1); Malagasy, Malagassi [Malagasy]: Makrosprache, Lebend
 - **mlt**: (Maltese): mlt (ISO 639-3), mt (ISO 639-1); Maltesisch [Maltese]: Einzelsprache, Lebend
 - **mnc**: (Manchu): mnc (ISO 639-3); Mandschurisch [Manchu]: Einzelsprache, Lebend
 - **mni**: (Manipuri): mni (ISO 639-3); Meitei [Manipuri]: Einzelsprache, Lebend
 - **mno**: (Manobo languages): mno (ISO 639-5); Manobo-Sprachen [Manobo languages]: Sprachfamilie
 - **moh**: (Mohawk): moh (ISO 639-3); Mohawk [Mohawk]: Einzelsprache, Lebend
 - **mon**: (Mongolian): mon (ISO 639-3), mn (ISO 639-1); Mongolisch [Mongolian]: Makrosprache, Lebend
 - **mos**: (Mossi): mos (ISO 639-3); Mòoré [Mossi]: Einzelsprache, Lebend
 - **mul**: (Multiple languages): mul (ISO 639-3); „Mehrsprachig“ [Multiple languages]: Speziell
 - **mun**: (Munda languages): mun (ISO 639-5); Munda-Sprachen [Munda languages]: Sprachfamilie
 - **mus**: (Creek): mus (ISO 639-3); Muskogee-Sprachen [Creek]: Sprachfamilie
 - **mwl**: (Mirandese): mwl (ISO 639-3); Mirandés [Mirandese]: Einzelsprache, Lebend
 - **mwr**: (Marwari): mwr (ISO 639-3); Marwari [Marwari]: Makrosprache, Lebend
 - **myn**: (Mayan languages): myn (ISO 639-5); Maya-Sprachen [Mayan languages]: Sprachfamilie
 - **myv**: (Erzya): myv (ISO 639-3); Ersjanisch, Ersja-Mordwinisch [Erzya]: Einzelsprache, Lebend
 - **nah**: (Nahuatl languages): nah (ISO 639-5); Nahuatl [Nahuatl languages]: Sprachfamilie
 - **nai**: (North American Indian languages): nai (ISO 639-5); Nordamerikanische Sprachen [North American Indian languages]: Sprachfamilie
 - **nap**: (Neapolitan): nap (ISO 639-3); Neapolitanisch [Neapolitan]: Einzelsprache, Lebend
 - **nau**: (Nauru): nau (ISO 639-3), na (ISO 639-1); Nauruisch [Nauru]: Einzelsprache, Lebend
 - **nav**: (Navajo; Navaho): nav (ISO 639-3), nv (ISO 639-1); Navajo [Navajo; Navaho]: Einzelsprache, Lebend
 - **nbl**: (Ndebele, South; South Ndebele): nbl (ISO 639-3), nr (ISO 639-1); Süd-Ndebele [Ndebele, South; South Ndebele]: Einzelsprache, Lebend
 - **nde**: (Ndebele, North; North Ndebele): nde (ISO 639-3), nd (ISO 639-1); Nord-Ndebele [Ndebele, North; North Ndebele]: Einzelsprache, Lebend
 - **ndo**: (Ndonga): ndo (ISO 639-3), ng (ISO 639-1); Ndonga [Ndonga]: Einzelsprache, Lebend
 - **nds**: (Low German; Low Saxon; German, Low; Saxon, Low): nds (ISO 639-3); Niederdeutsch, Plattdeutsch [Low German; Low Saxon; German, Low; Saxon, Low]: Einzelsprache, Lebend
 - **nep**: (Nepali): nep (ISO 639-3), ne (ISO 639-1); Nepali [Nepali]: Makrosprache, Lebend
 - **new**: (Nepal Bhasa; Newari): new (ISO 639-3); Newari [Nepal Bhasa; Newari]: Einzelsprache, Lebend
 - **nia**: (Nias): nia (ISO 639-3); Nias [Nias]: Einzelsprache, Lebend
 - **nic**: (Niger-Kordofanian languages): nic (ISO 639-5); Niger-Kongo-Sprachen [Niger-Kordofanian languages]: Sprachfamilie
 - **niu**: (Niuean): niu (ISO 639-3); Niueanisch [Niuean]: Einzelsprache, Lebend
 - **nno**: (Norwegian Nynorsk; Nynorsk, Norwegian): nno (ISO 639-3), nn (ISO 639-1); Nynorsk [Norwegian Nynorsk; Nynorsk, Norwegian]: Einzelsprache, Lebend
 - **nob**: (Bokmål, Norwegian; Norwegian Bokmål): nob (ISO 639-3), nb (ISO 639-1); Bokmål [Bokmål, Norwegian; Norwegian Bokmål]: Einzelsprache, Lebend
 - **nog**: (Nogai): nog (ISO 639-3); Nogaisch [Nogai]: Einzelsprache, Lebend
 - **non**: (Norse, Old): non (ISO 639-3); Altnordisch [Norse, Old]: Einzelsprache, Historisch
 - **nor**: (Norwegian): nor (ISO 639-3), no (ISO 639-1); Norwegisch [Norwegian]: Makrosprache, Lebend
 - **nqo**: (N'Ko): nqo (ISO 639-3); N’Ko [N'Ko]: Einzelsprache, Lebend
 - **nso**: (Pedi; Sepedi; Northern Sotho): nso (ISO 639-3); Nord-Sotho [Pedi; Sepedi; Northern Sotho]: Einzelsprache, Lebend
 - **nub**: (Nubian languages): nub (ISO 639-5); Nubische Sprachen [Nubian languages]: Sprachfamilie
 - **nwc**: (Classical Newari; Old Newari; Classical Nepal Bhasa): nwc (ISO 639-3); Klassisches Newari [Classical Newari; Old Newari; Classical Nepal Bhasa]: Einzelsprache, Historisch
 - **nya**: (Chichewa; Chewa; Nyanja): nya (ISO 639-3), ny (ISO 639-1); Chichewa [Chichewa; Chewa; Nyanja]: Einzelsprache, Lebend
 - **nym**: (Nyamwezi): nym (ISO 639-3); Nyamwesi [Nyamwezi]: Einzelsprache, Lebend
 - **nyn**: (Nyankole): nyn (ISO 639-3); Runyankole, Runyankore [Nyankole]: Einzelsprache, Lebend
 - **nyo**: (Nyoro): nyo (ISO 639-3); Runyoro [Nyoro]: Einzelsprache, Lebend
 - **nzi**: (Nzima): nzi (ISO 639-3); Nzema [Nzima]: Einzelsprache, Lebend
 - **oci**: (Occitan (ab 1500); Provençal): oci (ISO 639-3), oc (ISO 639-1); Okzitanisch [Occitan (ab 1500); Provençal]: Einzelsprache, Lebend
 - **oji**: (Ojibwa): oji (ISO 639-3), oj (ISO 639-1); Ojibwe [Ojibwa]: Makrosprache, Lebend
 - **ori**: (Oriya): ori (ISO 639-3), or (ISO 639-1); Oriya [Oriya]: Makrosprache, Lebend
 - **orm**: (Oromo): orm (ISO 639-3), om (ISO 639-1); Oromo [Oromo]: Makrosprache, Lebend
 - **osa**: (Osage): osa (ISO 639-3); Osage [Osage]: Einzelsprache, Lebend
 - **oss**: (Ossetian; Ossetic): oss (ISO 639-3), os (ISO 639-1); Ossetisch [Ossetian; Ossetic]: Einzelsprache, Lebend
 - **ota**: (Turkish, Ottoman (1500–1928)): ota (ISO 639-3); Osmanisch, osmanisches Türkisch [Turkish, Ottoman (1500–1928)]: Einzelsprache, Historisch
 - **oto**: (Otomian languages): oto (ISO 639-5); Oto-Pame-Sprachen [Otomian languages]: Sprachfamilie
 - **paa**: (Papuan languages): paa (ISO 639-5); Papuasprachen [Papuan languages]: Sprachfamilie
 - **pag**: (Pangasinan): pag (ISO 639-3); Pangasinensisch [Pangasinan]: Einzelsprache, Lebend
 - **pal**: (Pahlavi): pal (ISO 639-3); Mittelpersisch, Pahlavi [Pahlavi]: Einzelsprache, Alt
 - **pam**: (Pampanga; Kapampangan): pam (ISO 639-3); Kapampangan [Pampanga; Kapampangan]: Einzelsprache, Lebend
 - **pan**: (Panjabi; Punjabi): pan (ISO 639-3), pa (ISO 639-1); Panjabi, Pandschabi [Panjabi; Punjabi]: Einzelsprache, Lebend
 - **pap**: (Papiamento): pap (ISO 639-3); Papiamentu [Papiamento]: Einzelsprache, Lebend
 - **pau**: (Palauan): pau (ISO 639-3); Palauisch [Palauan]: Einzelsprache, Lebend
 - **peo**: (Persian, Old (ca. 600–400 v. Chr.)): peo (ISO 639-3); Altpersisch [Persian, Old (ca. 600–400 v. Chr.)]: Einzelsprache, Historisch
 - **phi**: (Philippine languages): phi (ISO 639-5); Philippinische Sprachen [Philippine languages]: Sprachfamilie
 - **phn**: (Phoenician): phn (ISO 639-3); Phönizisch, Punisch [Phoenician]: Einzelsprache, Alt
 - **pli**: (Pali): pli (ISO 639-3), pi (ISO 639-1); Pali [Pali]: Einzelsprache, Alt
 - **pol**: (Polish): pol (ISO 639-3), pl (ISO 639-1); Polnisch [Polish]: Einzelsprache, Lebend
 - **pon**: (Pohnpeian): pon (ISO 639-3); Pohnpeanisch [Pohnpeian]: Einzelsprache, Lebend
 - **por**: (Portuguese): por (ISO 639-3), pt (ISO 639-1); Portugiesisch [Portuguese]: Einzelsprache, Lebend
 - **pra**: (Prakrit languages): pra (ISO 639-5); Prakrit [Prakrit languages]: Sprachfamilie
 - **pro**: (Provençal, Old (bis 1500); Old Occitan (bis 1500)): pro (ISO 639-3); Altokzitanisch, Altprovenzalisch [Provençal, Old (bis 1500); Old Occitan (bis 1500)]: Einzelsprache, Historisch
 - **pus**: (Pushto; Pashto): pus (ISO 639-3), ps (ISO 639-1); Paschtunisch [Pushto; Pashto]: Makrosprache, Lebend
 - **qaa-qtz**: Reserviert für lokale Nutzung, Reserved for local use
 - **que**: (Quechua): que (ISO 639-3), qwe (ISO 639-3), qu (ISO 639-1); Quechua [Quechua]: Makrosprache, Lebend
 - **raj**: (Rajasthani): raj (ISO 639-3); Rajasthani [Rajasthani]: Makrosprache, Lebend
 - **rap**: (Rapanui): rap (ISO 639-3); Rapanui [Rapanui]: Einzelsprache, Lebend
 - **rar**: (Rarotongan; Cook Islands Maori): rar (ISO 639-3); Rarotonganisch, Māori der Cookinseln [Rarotongan; Cook Islands Maori]: Einzelsprache, Lebend
 - **roa**: (Romance languages): roa (ISO 639-5); Romanische Sprachen [Romance languages]: Sprachfamilie
 - **roh**: (Romansh): roh (ISO 639-3), rm (ISO 639-1); Bündnerromanisch, Romanisch [Romansh]: Einzelsprache, Lebend
 - **rom**: (Romany): rom (ISO 639-3); Romani, Romanes [Romany]: Makrosprache, Lebend
 - **run**: (Rundi): run (ISO 639-3), rn (ISO 639-1); Kirundi [Rundi]: Einzelsprache, Lebend
 - **rup**: (Aromanian; Arumanian; Macedo-Romanian): rup (ISO 639-3); Aromunisch, Makedoromanisch [Aromanian; Arumanian; Macedo-Romanian]: Einzelsprache, Lebend
 - **rus**: (Russian): rus (ISO 639-3), ru (ISO 639-1); Russisch [Russian]: Einzelsprache, Lebend
 - **sad**: (Sandawe): sad (ISO 639-3); Sandawe [Sandawe]: Einzelsprache, Lebend
 - **sag**: (Sango): sag (ISO 639-3), sg (ISO 639-1); Sango [Sango]: Einzelsprache, Lebend
 - **sah**: (Yakut): sah (ISO 639-3); Jakutisch [Yakut]: Einzelsprache, Lebend
 - **sai**: (South American Indian (Other)): sai (ISO 639-5); Südamerikanische Sprachen [South American Indian (Other)]: Sprachfamilie
 - **sal**: (Salishan languages): sal (ISO 639-5); Salish-Sprachen [Salishan languages]: Sprachfamilie
 - **sam**: (Samaritan Aramaic): sam (ISO 639-3); Samaritanisch [Samaritan Aramaic]: Einzelsprache, Ausgestorben
 - **san**: (Sanskrit): san (ISO 639-3), sa (ISO 639-1); Sanskrit [Sanskrit]: Einzelsprache, Alt
 - **sas**: (Sasak): sas (ISO 639-3); Sasak [Sasak]: Einzelsprache, Lebend
 - **sat**: (Santali): sat (ISO 639-3); Santali [Santali]: Einzelsprache, Lebend
 - **scn**: (Sicilian): scn (ISO 639-3); Sizilianisch [Sicilian]: Einzelsprache, Lebend
 - **sco**: (Scots): sco (ISO 639-3); Scots [Scots]: Einzelsprache, Lebend
 - **sel**: (Selkup): sel (ISO 639-3); Selkupisch [Selkup]: Einzelsprache, Lebend
 - **sem**: (Semitic languages): sem (ISO 639-5); Semitische Sprachen [Semitic languages]: Sprachfamilie
 - **sga**: (Irish, Old (bis 900)): sga (ISO 639-3); Altirisch [Irish, Old (bis 900)]: Einzelsprache, Historisch
 - **sgn**: (Sign Languages): sgn (ISO 639-5); Gebärdensprache [Sign Languages]: Sprachfamilie
 - **shn**: (Shan): shn (ISO 639-3); Shan [Shan]: Einzelsprache, Lebend
 - **sid**: (Sidamo): sid (ISO 639-3); Sidama [Sidamo]: Einzelsprache, Lebend
 - **sin**: (Sinhala; Sinhalese): sin (ISO 639-3), si (ISO 639-1); Singhalesisch [Sinhala; Sinhalese]: Einzelsprache, Lebend
 - **sio**: (Siouan languages): sio (ISO 639-5); Sioux-Sprachen [Siouan languages]: Sprachfamilie
 - **sit**: (Sino-Tibetan languages): sit (ISO 639-5); Sinotibetische Sprachen [Sino-Tibetan languages]: Sprachfamilie
 - **sla**: (Slavic languages): sla (ISO 639-5); Slawische Sprachen [Slavic languages]: Sprachfamilie
 - **slv**: (Slovenian): slv (ISO 639-3), sl (ISO 639-1); Slowenisch [Slovenian]: Einzelsprache, Lebend
 - **sma**: (Southern Sami): sma (ISO 639-3); Südsamisch [Southern Sami]: Einzelsprache, Lebend
 - **sme**: (Northern Sami): sme (ISO 639-3), se (ISO 639-1); Nordsamisch [Northern Sami]: Einzelsprache, Lebend
 - **smi**: (Sami languages): smi (ISO 639-5); Samische Sprachen [Sami languages]: Sprachfamilie
 - **smj**: (Lule Sami): smj (ISO 639-3); Lulesamisch [Lule Sami]: Einzelsprache, Lebend
 - **smn**: (Inari Sami): smn (ISO 639-3); Inarisamisch [Inari Sami]: Einzelsprache, Lebend
 - **smo**: (Samoan): smo (ISO 639-3), sm (ISO 639-1); Samoanisch [Samoan]: Einzelsprache, Lebend
 - **sms**: (Skolt Sami): sms (ISO 639-3); Skoltsamisch [Skolt Sami]: Einzelsprache, Lebend
 - **sna**: (Shona): sna (ISO 639-3), sn (ISO 639-1); Shona [Shona]: Einzelsprache, Lebend
 - **snd**: (Sindhi): snd (ISO 639-3), sd (ISO 639-1); Sindhi [Sindhi]: Einzelsprache, Lebend
 - **snk**: (Soninke): snk (ISO 639-3); Soninke [Soninke]: Einzelsprache, Lebend
 - **sog**: (Sogdian): sog (ISO 639-3); Sogdisch [Sogdian]: Einzelsprache, Alt
 - **som**: (Somali): som (ISO 639-3), so (ISO 639-1); Somali [Somali]: Einzelsprache, Lebend
 - **son**: (Songhai languages): son (ISO 639-5); Songhai-Sprachen [Songhai languages]: Sprachfamilie
 - **sot**: (Sotho, Southern): sot (ISO 639-3), st (ISO 639-1); Sesotho, Süd-Sotho [Sotho, Southern]: Einzelsprache, Lebend
 - **spa**: (Spanish; Castilian): spa (ISO 639-3), es (ISO 639-1); Spanisch, Kastilisch [Spanish; Castilian]: Einzelsprache, Lebend
 - **srd**: (Sardinian): srd (ISO 639-3), sc (ISO 639-1); Sardisch [Sardinian]: Makrosprache, Lebend
 - **srn**: (Sranan Tongo): srn (ISO 639-3); Sranantongo [Sranan Tongo]: Einzelsprache, Lebend
 - **srp**: (Serbian): srp (ISO 639-3), sr (ISO 639-1); Serbisch [Serbian]: Einzelsprache, Lebend
 - **srr**: (Serer): srr (ISO 639-3); Serer [Serer]: Einzelsprache, Lebend
 - **ssa**: (Nilo-Saharan languages): ssa (ISO 639-5); Nilosaharanische Sprachen [Nilo-Saharan languages]: Sprachfamilie
 - **ssw**: (Swati): ssw (ISO 639-3), ss (ISO 639-1); Siswati [Swati]: Einzelsprache, Lebend
 - **suk**: (Sukuma): suk (ISO 639-3); Sukuma [Sukuma]: Einzelsprache, Lebend
 - **sun**: (Sundanese): sun (ISO 639-3), su (ISO 639-1); Sundanesisch [Sundanese]: Einzelsprache, Lebend
 - **sus**: (Susu): sus (ISO 639-3); Susu [Susu]: Einzelsprache, Lebend
 - **sux**: (Sumerian): sux (ISO 639-3); Sumerisch [Sumerian]: Einzelsprache, Alt
 - **swa**: (Swahili): swa (ISO 639-3), sw (ISO 639-1); Swahili [Swahili]: Makrosprache, Lebend
 - **swe**: (Swedish): swe (ISO 639-3), sv (ISO 639-1); Schwedisch [Swedish]: Einzelsprache, Lebend
 - **syc**: (Classical Syriac): syc (ISO 639-3); Syrisch [Classical Syriac]: Einzelsprache, Historisch
 - **syr**: (Syriac): syr (ISO 639-3); Nordost-Neuaramäisch [Syriac]: Makrosprache, Lebend
 - **tah**: (Tahitian): tah (ISO 639-3), ty (ISO 639-1); Tahitianisch, Tahitisch [Tahitian]: Einzelsprache, Lebend
 - **tai**: (Tai languages): tai (ISO 639-5); Tai-Sprachen [Tai languages]: Sprachfamilie
 - **tam**: (Tamil): tam (ISO 639-3), ta (ISO 639-1); Tamil [Tamil]: Einzelsprache, Lebend
 - **tat**: (Tatar): tat (ISO 639-3), tt (ISO 639-1); Tatarisch [Tatar]: Einzelsprache, Lebend
 - **tel**: (Telugu): tel (ISO 639-3), te (ISO 639-1); Telugu [Telugu]: Einzelsprache, Lebend
 - **tem**: (Timne): tem (ISO 639-3); Temne [Timne]: Einzelsprache, Lebend
 - **ter**: (Tereno): ter (ISO 639-3); Terena [Tereno]: Einzelsprache, Lebend
 - **tet**: (Tetum): tet (ISO 639-3); Tetum [Tetum]: Einzelsprache, Lebend
 - **tgk**: (Tajik): tgk (ISO 639-3), tg (ISO 639-1); Tadschikisch [Tajik]: Einzelsprache, Lebend
 - **tgl**: (Tagalog): tgl (ISO 639-3), tl (ISO 639-1); Tagalog [Tagalog]: Einzelsprache, Lebend
 - **tha**: (Thai): tha (ISO 639-3), th (ISO 639-1); Thai [Thai]: Einzelsprache, Lebend
 - **tig**: (Tigre): tig (ISO 639-3); Tigre [Tigre]: Einzelsprache, Lebend
 - **tir**: (Tigrinya): tir (ISO 639-3), ti (ISO 639-1); Tigrinya [Tigrinya]: Einzelsprache, Lebend
 - **tiv**: (Tiv): tiv (ISO 639-3); Tiv [Tiv]: Einzelsprache, Lebend
 - **tkl**: (Tokelau): tkl (ISO 639-3); Tokelauisch [Tokelau]: Einzelsprache, Lebend
 - **tlh**: (Klingon; tlhIngan-Hol): tlh (ISO 639-3); Klingonisch [Klingon; tlhIngan-Hol]: Einzelsprache, Konstruiert
 - **tli**: (Tlingit): tli (ISO 639-3); Tlingit [Tlingit]: Einzelsprache, Lebend
 - **tmh**: (Tamashek): tmh (ISO 639-3); Tuareg [Tamashek]: Makrosprache, Lebend
 - **tog**: (Tonga (Nyasa)): tog (ISO 639-3); ChiTonga [Tonga (Nyasa)]: Einzelsprache, Lebend
 - **ton**: (Tonga (Tonga Islands)): ton (ISO 639-3), to (ISO 639-1); Tongaisch [Tonga (Tonga Islands)]: Einzelsprache, Lebend
 - **tpi**: (Tok Pisin): tpi (ISO 639-3); Tok Pisin, Neuguinea-Pidgin [Tok Pisin]: Einzelsprache, Lebend
 - **tsi**: (Tsimshian): tsi (ISO 639-3); Tsimshian [Tsimshian]: Einzelsprache, Lebend
 - **tsn**: (Tswana): tsn (ISO 639-3), tn (ISO 639-1); Setswana [Tswana]: Einzelsprache, Lebend
 - **tso**: (Tsonga): tso (ISO 639-3), ts (ISO 639-1); Xitsonga [Tsonga]: Einzelsprache, Lebend
 - **tuk**: (Turkmen): tuk (ISO 639-3), tk (ISO 639-1); Turkmenisch [Turkmen]: Einzelsprache, Lebend
 - **tum**: (Tumbuka): tum (ISO 639-3); Tumbuka [Tumbuka]: Einzelsprache, Lebend
 - **tup**: (Tupi languages): tup (ISO 639-5); Tupí-Sprachen [Tupi languages]: Sprachfamilie
 - **tur**: (Turkish): tur (ISO 639-3), tr (ISO 639-1); Türkisch [Turkish]: Einzelsprache, Lebend
 - **tut**: (Altaic languages): tut (ISO 639-5); Altaische Sprachen [Altaic languages]: Sprachfamilie
 - **tvl**: (Tuvalu): tvl (ISO 639-3); Tuvaluisch [Tuvalu]: Einzelsprache, Lebend
 - **twi**: (Twi): twi (ISO 639-3), tw (ISO 639-1); Twi [Twi]: Einzelsprache, Lebend
 - **tyv**: (Tuvinian): tyv (ISO 639-3); Tuwinisch [Tuvinian]: Einzelsprache, Lebend
 - **udm**: (Udmurt): udm (ISO 639-3); Udmurtisch [Udmurt]: Einzelsprache, Lebend
 - **uga**: (Ugaritic): uga (ISO 639-3); Ugaritisch [Ugaritic]: Einzelsprache, Alt
 - **uig**: (Uighur; Uyghur): uig (ISO 639-3), ug (ISO 639-1); Uigurisch [Uighur; Uyghur]: Einzelsprache, Lebend
 - **ukr**: (Ukrainian): ukr (ISO 639-3), uk (ISO 639-1); Ukrainisch [Ukrainian]: Einzelsprache, Lebend
 - **umb**: (Umbundu): umb (ISO 639-3); Umbundu [Umbundu]: Einzelsprache, Lebend
 - **und**: (Undetermined): und (ISO 639-3); „Undefiniert“ [Undetermined]: Speziell
 - **urd**: (Urdu): urd (ISO 639-3), ur (ISO 639-1); Urdu [Urdu]: Einzelsprache, Lebend
 - **uzb**: (Uzbek): uzb (ISO 639-3), uz (ISO 639-1); Usbekisch [Uzbek]: Makrosprache, Lebend
 - **vai**: (Vai): vai (ISO 639-3); Vai [Vai]: Einzelsprache, Lebend
 - **ven**: (Venda): ven (ISO 639-3), ve (ISO 639-1); Tshivenda [Venda]: Einzelsprache, Lebend
 - **vie**: (Vietnamese): vie (ISO 639-3), vi (ISO 639-1); Vietnamesisch [Vietnamese]: Einzelsprache, Lebend
 - **vol**: (Volapük): vol (ISO 639-3), vo (ISO 639-1); Volapük [Volapük]: Einzelsprache, Konstruiert
 - **vot**: (Votic): vot (ISO 639-3); Wotisch [Votic]: Einzelsprache, Lebend
 - **wak**: (Wakashan languages): wak (ISO 639-5); Wakash-Sprachen [Wakashan languages]: Sprachfamilie
 - **wal**: (Walamo): wal (ISO 639-3); Wolaytta [Walamo]: Einzelsprache, Lebend
 - **war**: (Waray): war (ISO 639-3); Wáray-Wáray [Waray]: Einzelsprache, Lebend
 - **was**: (Washo): was (ISO 639-3); Washoe [Washo]: Einzelsprache, Lebend
 - **wen**: (Sorbian languages): wen (ISO 639-5); Sorbische Sprache [Sorbian languages]: Sprachfamilie
 - **wln**: (Walloon): wln (ISO 639-3), wa (ISO 639-1); Wallonisch [Walloon]: Einzelsprache, Lebend
 - **wol**: (Wolof): wol (ISO 639-3), wo (ISO 639-1); Wolof [Wolof]: Einzelsprache, Lebend
 - **xal**: (Kalmyk; Oirat): xal (ISO 639-3); Kalmückisch [Kalmyk; Oirat]: Einzelsprache, Lebend
 - **xho**: (Xhosa): xho (ISO 639-3), xh (ISO 639-1); isiXhosa [Xhosa]: Einzelsprache, Lebend
 - **yao**: (Yao): yao (ISO 639-3); Yao [Yao]: Einzelsprache, Lebend
 - **yap**: (Yapese): yap (ISO 639-3); Yapesisch [Yapese]: Einzelsprache, Lebend
 - **yid**: (Yiddish): yid (ISO 639-3), yi (ISO 639-1); Jiddisch [Yiddish]: Makrosprache, Lebend
 - **yor**: (Yoruba): yor (ISO 639-3), yo (ISO 639-1); Yoruba [Yoruba]: Einzelsprache, Lebend
 - **ypk**: (Yupik languages): ypk (ISO 639-5); Yupik-Sprachen [Yupik languages]: Sprachfamilie
 - **zap**: (Zapotec): zap (ISO 639-3); Zapotekisch [Zapotec]: Makrosprache, Lebend
 - **zbl**: (Blissymbols; Blissymbolics; Bliss): zbl (ISO 639-3); Bliss-Symbol [Blissymbols; Blissymbolics; Bliss]: Einzelsprache, Konstruiert
 - **zen**: (Zenaga): zen (ISO 639-3); Zenaga [Zenaga]: Einzelsprache, Lebend
 - **zgh**: (Standard Moroccan Tamazight): zgh (ISO 639-3); Marokkanisches Tamazight [Standard Moroccan Tamazight]: Einzelsprache, Lebend
 - **zha**: (Zhuang; Chuang): zha (ISO 639-3), za (ISO 639-1); Zhuang [Zhuang; Chuang]: Makrosprache, Lebend
 - **znd**: (Zande languages): znd (ISO 639-5); Zande-Sprachen [Zande languages]: Sprachfamilie
 - **zul**: (Zulu): zul (ISO 639-3), zu (ISO 639-1); isiZulu [Zulu]: Einzelsprache, Lebend
 - **zun**: (Zuni): zun (ISO 639-3); Zuñi [Zuni]: Einzelsprache, Lebend
 - **zxx**: (No linguistic content; Not applicable): zxx (ISO 639-3); „Kein sprachlicher Inhalt; Nicht anwendbar“ [No linguistic content; Not applicable]: Speziell
 - **zza**: (Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki): zza (ISO 639-3); Zazaisch [Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki]: Makrosprache, Lebend
 - **alb**: (Albanian): alb (ISO 639-2/B), sqi (ISO 639-3), sq (ISO 639-1); Albanisch [Albanian]: Makrosprache, Lebend
 - **arm**: (Armenian): arm (ISO 639-2/B), hye (ISO 639-3), hy (ISO 639-1); Armenisch [Armenian]: Einzelsprache, Lebend
 - **baq**: (Basque): baq (ISO 639-2/B), eus (ISO 639-3), eu (ISO 639-1); Baskisch [Basque]: Einzelsprache, Lebend
 - **bur**: (Burmese): bur (ISO 639-2/B), mya (ISO 639-3), my (ISO 639-1); Birmanisch [Burmese]: Einzelsprache, Lebend
 - **chi**: (Chinese): chi (ISO 639-2/B), zho (ISO 639-3), zh (ISO 639-1); Chinesisch [Chinese]: Makrosprache, Lebend
 - **cze**: (Czech): cze (ISO 639-2/B), ces (ISO 639-3), cs (ISO 639-1); Tschechisch [Czech]: Einzelsprache, Lebend
 - **dut**: (Dutch; Flemish): dut (ISO 639-2/B), nld (ISO 639-3), nl (ISO 639-1); Niederländisch, Belgisches Niederländisch [Dutch; Flemish]: Einzelsprache, Lebend
 - **fre**: (French): fre (ISO 639-2/B), fra (ISO 639-3), fr (ISO 639-1); Französisch [French]: Einzelsprache, Lebend
 - **geo**: (Georgian): geo (ISO 639-2/B), kat (ISO 639-3), ka (ISO 639-1); Georgisch [Georgian]: Einzelsprache, Lebend
 - **ger**: (German): ger (ISO 639-2/B), deu (ISO 639-3), de (ISO 639-1); Deutsch [German]: Einzelsprache, Lebend
 - **gre**: (Greek, Modern (ab 1453)): gre (ISO 639-2/B), ell (ISO 639-3), el (ISO 639-1); Griechisch [Greek, Modern (ab 1453)]: Einzelsprache, Lebend
 - **ice**: (Icelandic): ice (ISO 639-2/B), isl (ISO 639-3), is (ISO 639-1); Isländisch [Icelandic]: Einzelsprache, Lebend
 - **mac**: (Macedonian): mac (ISO 639-2/B), mkd (ISO 639-3), mk (ISO 639-1); Mazedonisch [Macedonian]: Einzelsprache, Lebend
 - **mao**: (Maori): mao (ISO 639-2/B), mri (ISO 639-3), mi (ISO 639-1); Maori [Maori]: Einzelsprache, Lebend
 - **may**: (Malay): may (ISO 639-2/B), msa (ISO 639-3), ms (ISO 639-1); Malaiisch [Malay]: Makrosprache, Lebend
 - **per**: (Persian): per (ISO 639-2/B), fas (ISO 639-3), fa (ISO 639-1); Persisch [Persian]: Makrosprache, Lebend
 - **rum**: (Romanian; Moldavian; Moldovan): rum (ISO 639-2/B), ron (ISO 639-3), ro (ISO 639-1); Rumänisch [Romanian; Moldavian; Moldovan]: Einzelsprache, Lebend
 - **slo**: (Slovak): slo (ISO 639-2/B), slk (ISO 639-3), sk (ISO 639-1); Slowakisch [Slovak]: Einzelsprache, Lebend
 - **tib**: (Tibetan): tib (ISO 639-2/B), bod (ISO 639-3), bo (ISO 639-1); Tibetisch [Tibetan]: Einzelsprache, Lebend
 - **wel**: (Welsh): wel (ISO 639-2/B), cym (ISO 639-3), cy (ISO 639-1); Walisisch [Welsh]: Einzelsprache, Lebend

---

<p style="margin-bottom:60px"></p>

<a name="d17e7229"></a>
#### Name (Mapping)
|     |     |
| --- | --- |
| **Name** | `@name`  |
| **Datatype** | string |
| **Label** (de) | Name (Mapping) |
| **Description** (de) | Name des Labels bzw. Terms |
| **Contained by** | [`mappingLabel`](#d17e6259) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e1371"></a>
#### Ortstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Ortstyp |
| **Label** (en) | Type of a Place |
| **Description** (de) | Typisierung des dokumentierten Orts (z.B. als fiktiver Ort, Gebäude oder Land). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName). |
| **Description** (en) | Spezifies the place encoded as eg. fictional Place, Building or Country). If no `@gndo:type` is provided the default is [gndo:PlaceOrGeographicName](https://d-nb.info/standards/elementset/gnd#PlaceOrGeographicName). |
| **Contained by** | [`place`](#d17e1265) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#AdministrativeUnit**: (Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#AdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial**: (Bauwerk oder Denkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#BuildingOrMemorial)
 - **https://d-nb.info/standards/elementset/gnd#Country**: (Land oder Staat) [GNDO](https://d-nb.info/standards/elementset/gnd#Country)
 - **https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory**: (Extraterrestrikum) [GNDO](https://d-nb.info/standards/elementset/gnd#ExtraterrestrialTerritory)
 - **https://d-nb.info/standards/elementset/gnd#FictivePlace**: (Fiktiver Ort) [GNDO](https://d-nb.info/standards/elementset/gnd#FictivePlace)
 - **https://d-nb.info/standards/elementset/gnd#MemberState**: (Gliedstaat) [GNDO](https://d-nb.info/standards/elementset/gnd#MemberState)
 - **https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit**: (Kleinräumiges Geografikum innerhalb eines Ortes) [GNDO](https://d-nb.info/standards/elementset/gnd#NameOfSmallGeographicUnitLyingWithinAnotherGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit**: (Natürlich geografische Einheit) [GNDO](https://d-nb.info/standards/elementset/gnd#NaturalGeographicUnit)
 - **https://d-nb.info/standards/elementset/gnd#ReligiousTerritory**: (Religiöses Territorium) [GNDO](https://d-nb.info/standards/elementset/gnd#ReligiousTerritory)
 - **https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit**: (Gebietskörperschaft oder Verwaltungseinheit) [GNDO](https://d-nb.info/standards/elementset/gnd#TerritorialCorporateBodyOrAdministrativeUnit)
 - **https://d-nb.info/standards/elementset/gnd#WayBorderOrLine**: (Weg, Grenze oder Linie) [GNDO](https://d-nb.info/standards/elementset/gnd#WayBorderOrLine)

---

<p style="margin-bottom:60px"></p>

<a name="person-types"></a>
#### Personentyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Personentyp |
| **Label** (en) | Type of a Person |
| **Description** (de) | Typisierung der dokumentierten Person (z.B. als fiktive Person, Gott oder royale Person). Wenn kein `@gndo:type` vergeben wird, gilt die Person als [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson). |
| **Description** (en) | Spezifies the person encoded as eg. fictional character, god oder royal). If no `@gndo:type` is provided the default is [gndo:DifferentiatedPerson](https://d-nb.info/standards/elementset/gnd#DifferentiatedPerson). |
| **Contained by** | [`person`](#person-record) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#CollectivePseudonym**: (Sammelpseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectivePseudonym)
 - **https://d-nb.info/standards/elementset/gnd#Gods**: (Götter) [GNDO](https://d-nb.info/standards/elementset/gnd#Gods)
 - **https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter**: (Literarische oder Sagengestalt) [GNDO](https://d-nb.info/standards/elementset/gnd#LiteraryOrLegendaryCharacter)
 - **https://d-nb.info/standards/elementset/gnd#Pseudonym**: (Pseudonym) [GNDO](https://d-nb.info/standards/elementset/gnd#Pseudonym)
 - **https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse**: (Regierender Fürst oder Mitglied eines regierenden Fürstenhauses) [GNDO](https://d-nb.info/standards/elementset/gnd#RoyalOrMemberOfARoyalHouse)
 - **https://d-nb.info/standards/elementset/gnd#Spirits**: (Geister) [GNDO](https://d-nb.info/standards/elementset/gnd#Spirits)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6773"></a>
#### Projekt ID
|     |     |
| --- | --- |
| **Name** | `@project`  |
| **Datatype** | string |
| **Label** (de) | Projekt ID |
| **Description** (de) | Identifiers einer Projekts, mit dem der Datensatz in einem entityXML Store assoziiert ist. |
| **Contained by** | [`store:store`](#d17e6745) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6476"></a>
#### Provider-ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | ID |
| **Label** (de) | Provider-ID |
| **Description** (de) | Der GND-Agentur Identifier des Datenproviders. Dieser Identifier wird in der Regel direkt von der GND-Agentur, bei der der Datenlieferant registriert ist, vergeben. |
| **Contained by** | [`provider`](#data-provider) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e3174"></a>
#### Quelle der Koordinaten
|     |     |
| --- | --- |
| **Name** | `@geo:source`  |
| **Datatype** | anyURI |
| **Label** (de) | Quelle der Koordinaten |
| **Label** (en) | Source of Coordinates |
| **Description** (de) | Ein URL zur Quelle der documentierten Koordinaten. |
| **Description** (en) | An URL pointing to the source of the coordinates encoded. |
| **Contained by** | [`geo:hasGeometry`](#d17e3150) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7648"></a>
#### Record ID
|     |     |
| --- | --- |
| **Name** | `@xml:id`  |
| **Datatype** | ID |
| **Label** (de) | Record ID |
| **Description** (de) | Angabe eines Identifiers zur Identifikation eines Entitäteneintrags innerhalb der Datensammlung. Die Angabe einer ID via `@xml:id` ist verpflichtend! |
| **Contained by** | [`corporateBody`](#corporate-body-record)  [`entity`](#entity-record)  [`event`](#d17e320)  [`expression`](#d17e1449)  [`manifestation`](#d17e1518)  [`person`](#person-record)  [`place`](#d17e1265)  [`subjectHeading`](#d17e1592)  [`work`](#d17e1648) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e7662"></a>
#### Referenz
|     |     |
| --- | --- |
| **Name** | `@ref`  |
| **Datatype** | anyURI (*Schematron Pattern*) |
| **Label** (de) | Referenz |
| **Description** (de) | Verweis auf die ID (`@xml:id`) eines Records in der selben entityXML Ressource oder auf einen HTTP-URI. |
| **Contained by** | [`bf:instanceOf`](#instance-of)  [`embodimentOf`](#d17e1553)  [`gndo:accordingWork`](#d17e3242)  [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:affiliation`](#d17e3398)  [`gndo:author`](#d17e3422)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:contributor`](#d17e3635)  [`gndo:editor`](#d17e4281)  [`gndo:exhibitor`](#d17e4336)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:fieldOfStudy`](#d17e4451)  [`gndo:firstAuthor`](#d17e4491)  [`gndo:formOfWorkAndExpression`](#d17e4421)  [`gndo:literarySource`](#d17e4888)  [`gndo:organizerOrHost`](#d17e4943)  [`gndo:place`](#entity-place-standard)  [`gndo:placeOfActivity`](#person-record-placeOfActivity)  [`gndo:placeOfBirth`](#person-record-placeOfBirth)  [`gndo:placeOfBusiness`](#d17e5119)  [`gndo:placeOfDeath`](#person-record-placeOfDeath)  [`gndo:playedInstrument`](#prop-playedInstrument)  [`gndo:precedingCorporateBody`](#d17e5335)  [`gndo:precedingPlaceOrGeographicName`](#d17e5302)  [`gndo:pseudonym`](#d17e5556)  [`gndo:publication`](#d17e5604)  [`gndo:relatedWork`](#d17e5806)  [`gndo:relatesTo`](#d17e5757)  [`gndo:succeedingCorporateBody`](#d17e5836)  [`gndo:succeedingPlaceOrGeographicName`](#d17e5866)  [`gndo:temporaryName`](#d17e5924)  [`gndo:titleOfNobility`](#d17e5969)  [`gndo:topic`](#d17e6005)  [`realizationOf`](#d17e1479) |

***Validation***  

```xml 
<sch:pattern xmlns:sch="http://purl.oclc.org/dsdl/schematron">
   <sch:rule context="@ref[starts-with(., '#')]" role="error">
      <sch:let name="id" value="substring-after(data(.), '#')"/>
      <sch:assert test="//element()[@xml:id = $id]">(Wrong ID): There is no ID "<sch:value-of select="$id"/>" in this resource! Your
                        reference is most likely wrong!</sch:assert>
   </sch:rule>
</sch:pattern>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e5652"></a>
#### Rolle der Entität (Publikation)
|     |     |
| --- | --- |
| **Name** | `@role`  |
| **Datatype** | string |
| **Label** (de) | Rolle der Entität (Publikation) |
| **Description** (de) | Hinweis darauf, welche Rolle die beschreibene Entität in Zusammenhang mit der Publikation spielt. |
| **Contained by** | [`gndo:publication`](#d17e5604) |

***Predefined Values***  

 - **author**: (Autor:in *default*) Die beschriebene Entität (Person) ist der/die Autor:in der Publikation.
 - **editor**: (Herausgeber:in) Die beschriebene Entität (Person) ist der/die Herausgeber:in der Publikation.
 - **about**: (About) Die Publikation handelt von der beschriebenen Entität.

---

<p style="margin-bottom:60px"></p>

<a name="d17e7703"></a>
#### Schrift
|     |     |
| --- | --- |
| **Name** | `@script`  |
| **Datatype** | string |
| **Label** (de) | Schrift |
| **Label** (en) | Script |
| **Description** (de) | Die Schrift, in der die Information angegeben ist wie in ISO 15924 definiert, vgl. http://www.unicode.org/iso15924/codelists.html. |
| **Description** (en) | The script, the information is given in as defined by ISO 15924, see: http://www.unicode.org/iso15924/codelists.html. |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName)  [`gndo:variantName`](#person-record-variantName)  [`gndo:variantName`](#elem.gndo.variantName.standard) |

***Predefined Values***  

 - **Adlm**: (Adlam) Scriptcode für Adlam (#166), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Afak**: (Afaka) Scriptcode für Afaka (#439), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Aghb**: (Caucasian Albanian) Scriptcode für Caucasian Albanian (#239), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ahom**: (Ahom, Tai Ahom) Scriptcode für Ahom, Tai Ahom (#338), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Arab**: (Arabic) Scriptcode für Arabic (#160), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Aran**: (Arabic (Nastaliq variant)) Scriptcode für Arabic (Nastaliq variant) (#161), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Armi**: (Imperial Aramaic) Scriptcode für Imperial Aramaic (#124), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Armn**: (Armenian) Scriptcode für Armenian (#230), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Avst**: (Avestan) Scriptcode für Avestan (#134), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bali**: (Balinese) Scriptcode für Balinese (#360), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bamu**: (Bamum) Scriptcode für Bamum (#435), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bass**: (Bassa Vah) Scriptcode für Bassa Vah (#259), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Batk**: (Batak) Scriptcode für Batak (#365), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Beng**: (Bengali (Bangla)) Scriptcode für Bengali (Bangla) (#325), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bhks**: (Bhaiksuki) Scriptcode für Bhaiksuki (#334), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Blis**: (Blissymbols) Scriptcode für Blissymbols (#550), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bopo**: (Bopomofo) Scriptcode für Bopomofo (#285), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Brah**: (Brahmi) Scriptcode für Brahmi (#300), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Brai**: (Braille) Scriptcode für Braille (#570), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Bugi**: (Buginese) Scriptcode für Buginese (#367), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Buhd**: (Buhid) Scriptcode für Buhid (#372), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cakm**: (Chakma) Scriptcode für Chakma (#349), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cans**: (Unified Canadian Aboriginal Syllabics) Scriptcode für Unified Canadian Aboriginal Syllabics (#440), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cari**: (Carian) Scriptcode für Carian (#201), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cham**: (Cham) Scriptcode für Cham (#358), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cher**: (Cherokee) Scriptcode für Cherokee (#445), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Chis**: (Chisoi) Scriptcode für Chisoi (#298), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Chrs**: (Chorasmian) Scriptcode für Chorasmian (#109), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cirt**: (Cirth) Scriptcode für Cirth (#291), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Copt**: (Coptic) Scriptcode für Coptic (#204), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cpmn**: (Cypro-Minoan) Scriptcode für Cypro-Minoan (#402), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cprt**: (Cypriot syllabary) Scriptcode für Cypriot syllabary (#403), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cyrl**: (Cyrillic) Scriptcode für Cyrillic (#220), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Cyrs**: (Cyrillic (Old Church Slavonic variant)) Scriptcode für Cyrillic (Old Church Slavonic variant) (#221), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Deva**: (Devanagari (Nagari)) Scriptcode für Devanagari (Nagari) (#315), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Diak**: (Dives Akuru) Scriptcode für Dives Akuru (#342), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Dogr**: (Dogra) Scriptcode für Dogra (#328), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Dsrt**: (Deseret (Mormon)) Scriptcode für Deseret (Mormon) (#250), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Dupl**: (Duployan shorthand, Duployan stenography) Scriptcode für Duployan shorthand, Duployan stenography (#755), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Egyd**: (Egyptian demotic) Scriptcode für Egyptian demotic (#070), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Egyh**: (Egyptian hieratic) Scriptcode für Egyptian hieratic (#060), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Egyp**: (Egyptian hieroglyphs) Scriptcode für Egyptian hieroglyphs (#050), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Elba**: (Elbasan) Scriptcode für Elbasan (#226), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Elym**: (Elymaic) Scriptcode für Elymaic (#128), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ethi**: (Ethiopic (Geʻez)) Scriptcode für Ethiopic (Geʻez) (#430), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gara**: (Garay) Scriptcode für Garay (#164), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Geok**: (Khutsuri (Asomtavruli and Nuskhuri)) Scriptcode für Khutsuri (Asomtavruli and Nuskhuri) (#241), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Geor**: (Georgian (Mkhedruli and Mtavruli)) Scriptcode für Georgian (Mkhedruli and Mtavruli) (#240), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Glag**: (Glagolitic) Scriptcode für Glagolitic (#225), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gong**: (Gunjala Gondi) Scriptcode für Gunjala Gondi (#312), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gonm**: (Masaram Gondi) Scriptcode für Masaram Gondi (#313), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Goth**: (Gothic) Scriptcode für Gothic (#206), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gran**: (Grantha) Scriptcode für Grantha (#343), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Grek**: (Greek) Scriptcode für Greek (#200), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gujr**: (Gujarati) Scriptcode für Gujarati (#320), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Gukh**: (Gurung Khema) Scriptcode für Gurung Khema (#397), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Guru**: (Gurmukhi) Scriptcode für Gurmukhi (#310), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hanb**: (Han with Bopomofo (alias for Han + Bopomofo)) Scriptcode für Han with Bopomofo (alias for Han + Bopomofo) (#503), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hang**: (Hangul (Hangŭl, Hangeul)) Scriptcode für Hangul (Hangŭl, Hangeul) (#286), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hani**: (Han (Hanzi, Kanji, Hanja)) Scriptcode für Han (Hanzi, Kanji, Hanja) (#500), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hano**: (Hanunoo (Hanunóo)) Scriptcode für Hanunoo (Hanunóo) (#371), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hans**: (Han (Simplified variant)) Scriptcode für Han (Simplified variant) (#501), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hant**: (Han (Traditional variant)) Scriptcode für Han (Traditional variant) (#502), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hatr**: (Hatran) Scriptcode für Hatran (#127), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hebr**: (Hebrew) Scriptcode für Hebrew (#125), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hira**: (Hiragana) Scriptcode für Hiragana (#410), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hluw**: (Anatolian Hieroglyphs (Luwian Hieroglyphs, Hittite Hieroglyphs)) Scriptcode für Anatolian Hieroglyphs (Luwian Hieroglyphs, Hittite Hieroglyphs) (#080), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hmng**: (Pahawh Hmong) Scriptcode für Pahawh Hmong (#450), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hmnp**: (Nyiakeng Puachue Hmong) Scriptcode für Nyiakeng Puachue Hmong (#451), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hrkt**: (Japanese syllabaries (alias for Hiragana + Katakana)) Scriptcode für Japanese syllabaries (alias for Hiragana + Katakana) (#412), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Hung**: (Old Hungarian (Hungarian Runic)) Scriptcode für Old Hungarian (Hungarian Runic) (#176), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Inds**: (Indus (Harappan)) Scriptcode für Indus (Harappan) (#610), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ital**: (Old Italic (Etruscan, Oscan, etc.)) Scriptcode für Old Italic (Etruscan, Oscan, etc.) (#210), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Jamo**: (Jamo (alias for Jamo subset of Hangul)) Scriptcode für Jamo (alias for Jamo subset of Hangul) (#284), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Java**: (Javanese) Scriptcode für Javanese (#361), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Jpan**: (Japanese (alias for Han + Hiragana + Katakana)) Scriptcode für Japanese (alias for Han + Hiragana + Katakana) (#413), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Jurc**: (Jurchen) Scriptcode für Jurchen (#510), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kali**: (Kayah Li) Scriptcode für Kayah Li (#357), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kana**: (Katakana) Scriptcode für Katakana (#411), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kawi**: (Kawi) Scriptcode für Kawi (#368), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Khar**: (Kharoshthi) Scriptcode für Kharoshthi (#305), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Khmr**: (Khmer) Scriptcode für Khmer (#355), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Khoj**: (Khojki) Scriptcode für Khojki (#322), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kitl**: (Khitan large script) Scriptcode für Khitan large script (#505), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kits**: (Khitan small script) Scriptcode für Khitan small script (#288), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Knda**: (Kannada) Scriptcode für Kannada (#345), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kore**: (Korean (alias for Hangul + Han)) Scriptcode für Korean (alias for Hangul + Han) (#287), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kpel**: (Kpelle) Scriptcode für Kpelle (#436), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Krai**: (Kirat Rai) Scriptcode für Kirat Rai (#396), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Kthi**: (Kaithi) Scriptcode für Kaithi (#317), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lana**: (Tai Tham (Lanna)) Scriptcode für Tai Tham (Lanna) (#351), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Laoo**: (Lao) Scriptcode für Lao (#356), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Latf**: (Latin (Fraktur variant)) Scriptcode für Latin (Fraktur variant) (#217), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Latg**: (Latin (Gaelic variant)) Scriptcode für Latin (Gaelic variant) (#216), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Latn**: (Latin) Scriptcode für Latin (#215), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Leke**: (Leke) Scriptcode für Leke (#364), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lepc**: (Lepcha (Róng)) Scriptcode für Lepcha (Róng) (#335), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Limb**: (Limbu) Scriptcode für Limbu (#336), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lina**: (Linear A) Scriptcode für Linear A (#400), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Linb**: (Linear B) Scriptcode für Linear B (#401), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lisu**: (Lisu (Fraser)) Scriptcode für Lisu (Fraser) (#399), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Loma**: (Loma) Scriptcode für Loma (#437), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lyci**: (Lycian) Scriptcode für Lycian (#202), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Lydi**: (Lydian) Scriptcode für Lydian (#116), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mahj**: (Mahajani) Scriptcode für Mahajani (#314), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Maka**: (Makasar) Scriptcode für Makasar (#366), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mand**: (Mandaic, Mandaean) Scriptcode für Mandaic, Mandaean (#140), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mani**: (Manichaean) Scriptcode für Manichaean (#139), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Marc**: (Marchen) Scriptcode für Marchen (#332), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Maya**: (Mayan hieroglyphs) Scriptcode für Mayan hieroglyphs (#090), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Medf**: (Medefaidrin (Oberi Okaime, Oberi Ɔkaimɛ)) Scriptcode für Medefaidrin (Oberi Okaime, Oberi Ɔkaimɛ) (#265), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mend**: (Mende Kikakui) Scriptcode für Mende Kikakui (#438), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Merc**: (Meroitic Cursive) Scriptcode für Meroitic Cursive (#101), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mero**: (Meroitic Hieroglyphs) Scriptcode für Meroitic Hieroglyphs (#100), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mlym**: (Malayalam) Scriptcode für Malayalam (#347), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Modi**: (Modi, Moḍī) Scriptcode für Modi, Moḍī (#324), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mong**: (Mongolian) Scriptcode für Mongolian (#145), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Moon**: (Moon (Moon code, Moon script, Moon type)) Scriptcode für Moon (Moon code, Moon script, Moon type) (#218), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mroo**: (Mro, Mru) Scriptcode für Mro, Mru (#264), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mtei**: (Meitei Mayek (Meithei, Meetei)) Scriptcode für Meitei Mayek (Meithei, Meetei) (#337), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mult**: (Multani) Scriptcode für Multani (#323), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Mymr**: (Myanmar (Burmese)) Scriptcode für Myanmar (Burmese) (#350), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nagm**: (Nag Mundari) Scriptcode für Nag Mundari (#295), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nand**: (Nandinagari) Scriptcode für Nandinagari (#311), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Narb**: (Old North Arabian (Ancient North Arabian)) Scriptcode für Old North Arabian (Ancient North Arabian) (#106), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nbat**: (Nabataean) Scriptcode für Nabataean (#159), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Newa**: (Newa, Newar, Newari, Nepāla lipi) Scriptcode für Newa, Newar, Newari, Nepāla lipi (#333), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nkdb**: (Naxi Dongba (na²¹ɕi³³ to³³ba²¹, Nakhi Tomba)) Scriptcode für Naxi Dongba (na²¹ɕi³³ to³³ba²¹, Nakhi Tomba) (#085), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nkgb**: (Naxi Geba (na²¹ɕi³³ gʌ²¹ba²¹, 'Na-'Khi ²Ggŏ-¹baw, Nakhi Geba)) Scriptcode für Naxi Geba (na²¹ɕi³³ gʌ²¹ba²¹, 'Na-'Khi ²Ggŏ-¹baw, Nakhi Geba) (#420), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nkoo**: (N’Ko) Scriptcode für N’Ko (#165), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Nshu**: (Nüshu) Scriptcode für Nüshu (#499), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ogam**: (Ogham) Scriptcode für Ogham (#212), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Olck**: (Ol Chiki (Ol Cemet’, Ol, Santali)) Scriptcode für Ol Chiki (Ol Cemet’, Ol, Santali) (#261), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Onao**: (Ol Onal) Scriptcode für Ol Onal (#296), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Orkh**: (Old Turkic, Orkhon Runic) Scriptcode für Old Turkic, Orkhon Runic (#175), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Orya**: (Oriya (Odia)) Scriptcode für Oriya (Odia) (#327), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Osge**: (Osage) Scriptcode für Osage (#219), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Osma**: (Osmanya) Scriptcode für Osmanya (#260), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ougr**: (Old Uyghur) Scriptcode für Old Uyghur (#143), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Palm**: (Palmyrene) Scriptcode für Palmyrene (#126), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Pauc**: (Pau Cin Hau) Scriptcode für Pau Cin Hau (#263), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Pcun**: (Proto-Cuneiform) Scriptcode für Proto-Cuneiform (#015), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Pelm**: (Proto-Elamite) Scriptcode für Proto-Elamite (#016), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Perm**: (Old Permic) Scriptcode für Old Permic (#227), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phag**: (Phags-pa) Scriptcode für Phags-pa (#331), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phli**: (Inscriptional Pahlavi) Scriptcode für Inscriptional Pahlavi (#131), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phlp**: (Psalter Pahlavi) Scriptcode für Psalter Pahlavi (#132), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phlv**: (Book Pahlavi) Scriptcode für Book Pahlavi (#133), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Phnx**: (Phoenician) Scriptcode für Phoenician (#115), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Plrd**: (Miao (Pollard)) Scriptcode für Miao (Pollard) (#282), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Piqd**: (Klingon (KLI pIqaD)) Scriptcode für Klingon (KLI pIqaD) (#293), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Prti**: (Inscriptional Parthian) Scriptcode für Inscriptional Parthian (#130), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Psin**: (Proto-Sinaitic) Scriptcode für Proto-Sinaitic (#103), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Qaaa**: (Reserved for private use (start)) Scriptcode für Reserved for private use (start) (#900), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Qabx**: (Reserved for private use (end)) Scriptcode für Reserved for private use (end) (#949), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ranj**: (Ranjana) Scriptcode für Ranjana (#303), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Rjng**: (Rejang (Redjang, Kaganga)) Scriptcode für Rejang (Redjang, Kaganga) (#363), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Rohg**: (Hanifi Rohingya) Scriptcode für Hanifi Rohingya (#167), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Roro**: (Rongorongo) Scriptcode für Rongorongo (#620), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Runr**: (Runic) Scriptcode für Runic (#211), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Samr**: (Samaritan) Scriptcode für Samaritan (#123), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sara**: (Sarati) Scriptcode für Sarati (#292), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sarb**: (Old South Arabian) Scriptcode für Old South Arabian (#105), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Saur**: (Saurashtra) Scriptcode für Saurashtra (#344), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sgnw**: (SignWriting) Scriptcode für SignWriting (#095), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Shaw**: (Shavian (Shaw)) Scriptcode für Shavian (Shaw) (#281), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Shrd**: (Sharada, Śāradā) Scriptcode für Sharada, Śāradā (#319), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Shui**: (Shuishu) Scriptcode für Shuishu (#530), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sidd**: (Siddham, Siddhaṃ, Siddhamātṛkā) Scriptcode für Siddham, Siddhaṃ, Siddhamātṛkā (#302), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sidt**: (Sidetic) Scriptcode für Sidetic (#180), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sind**: (Khudawadi, Sindhi) Scriptcode für Khudawadi, Sindhi (#318), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sinh**: (Sinhala) Scriptcode für Sinhala (#348), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sogd**: (Sogdian) Scriptcode für Sogdian (#141), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sogo**: (Old Sogdian) Scriptcode für Old Sogdian (#142), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sora**: (Sora Sompeng) Scriptcode für Sora Sompeng (#398), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Soyo**: (Soyombo) Scriptcode für Soyombo (#329), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sund**: (Sundanese) Scriptcode für Sundanese (#362), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sunu**: (Sunuwar) Scriptcode für Sunuwar (#274), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Sylo**: (Syloti Nagri) Scriptcode für Syloti Nagri (#316), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syrc**: (Syriac) Scriptcode für Syriac (#135), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syre**: (Syriac (Estrangelo variant)) Scriptcode für Syriac (Estrangelo variant) (#138), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syrj**: (Syriac (Western variant)) Scriptcode für Syriac (Western variant) (#137), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Syrn**: (Syriac (Eastern variant)) Scriptcode für Syriac (Eastern variant) (#136), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tagb**: (Tagbanwa) Scriptcode für Tagbanwa (#373), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Takr**: (Takri, Ṭākrī, Ṭāṅkrī) Scriptcode für Takri, Ṭākrī, Ṭāṅkrī (#321), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tale**: (Tai Le) Scriptcode für Tai Le (#353), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Talu**: (New Tai Lue) Scriptcode für New Tai Lue (#354), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Taml**: (Tamil) Scriptcode für Tamil (#346), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tang**: (Tangut) Scriptcode für Tangut (#520), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tavt**: (Tai Viet) Scriptcode für Tai Viet (#359), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tayo**: (Tai Yo) Scriptcode für Tai Yo (#380), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Telu**: (Telugu) Scriptcode für Telugu (#340), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Teng**: (Tengwar) Scriptcode für Tengwar (#290), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tfng**: (Tifinagh (Berber)) Scriptcode für Tifinagh (Berber) (#120), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tglg**: (Tagalog (Baybayin, Alibata)) Scriptcode für Tagalog (Baybayin, Alibata) (#370), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Thaa**: (Thaana) Scriptcode für Thaana (#170), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Thai**: (Thai) Scriptcode für Thai (#352), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tibt**: (Tibetan) Scriptcode für Tibetan (#330), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tirh**: (Tirhuta) Scriptcode für Tirhuta (#326), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tnsa**: (Tangsa) Scriptcode für Tangsa (#275), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Todr**: (Todhri) Scriptcode für Todhri (#229), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tols**: (Tolong Siki) Scriptcode für Tolong Siki (#299), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Toto**: (Toto) Scriptcode für Toto (#294), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Tutg**: (Tulu-Tigalari) Scriptcode für Tulu-Tigalari (#341), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Ugar**: (Ugaritic) Scriptcode für Ugaritic (#040), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Vaii**: (Vai) Scriptcode für Vai (#470), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Visp**: (Visible Speech) Scriptcode für Visible Speech (#280), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Vith**: (Vithkuqi) Scriptcode für Vithkuqi (#228), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Wara**: (Warang Citi (Varang Kshiti)) Scriptcode für Warang Citi (Varang Kshiti) (#262), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Wcho**: (Wancho) Scriptcode für Wancho (#283), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Wole**: (Woleai) Scriptcode für Woleai (#480), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Xpeo**: (Old Persian) Scriptcode für Old Persian (#030), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Xsux**: (Cuneiform, Sumero-Akkadian) Scriptcode für Cuneiform, Sumero-Akkadian (#020), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Yezi**: (Yezidi) Scriptcode für Yezidi (#192), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Yiii**: (Yi) Scriptcode für Yi (#460), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zanb**: (Zanabazar Square (Zanabazarin Dörböljin Useg, Xewtee Dörböljin Bicig, Horizontal Square Script)) Scriptcode für Zanabazar Square (Zanabazarin Dörböljin Useg, Xewtee Dörböljin Bicig, Horizontal Square Script) (#339), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zinh**: (Code for inherited script) Scriptcode für Code for inherited script (#994), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zmth**: (Mathematical notation) Scriptcode für Mathematical notation (#995), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zsye**: (Symbols (Emoji variant)) Scriptcode für Symbols (Emoji variant) (#993), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zsym**: (Symbols) Scriptcode für Symbols (#996), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zxxx**: (Code for unwritten documents) Scriptcode für Code for unwritten documents (#997), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zyyy**: (Code for undetermined script) Scriptcode für Code for undetermined script (#998), vgl. http://www.unicode.org/iso15924/codelists.html
 - **Zzzz**: (Code for uncoded script) Scriptcode für Code for uncoded script (#999), vgl. http://www.unicode.org/iso15924/codelists.html

---

<p style="margin-bottom:60px"></p>

<a name="d17e5520"></a>
#### Signifikanz
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Signifikanz |
| **Description** (de) | Angabe zur Signifikanz der Beschäftigung einer Person, d.h. ob es sich bei der angegebenen Beschäftigung um ihre hauptsächliche bzw. charakteristische Beschäfigung handelt. |
| **Contained by** | [`gndo:professionOrOccupation`](#d17e5488) |

***Predefined Values***  

 - **significant**: (charakteristisch) Der Beruf oder der Tätigkeitsbereich ist für die Person charakteristisch.

---

<p style="margin-bottom:60px"></p>

<a name="d17e7157"></a>
#### Sprache
|     |     |
| --- | --- |
| **Name** | `@xml:lang`  |
| **Datatype** | string |
| **Label** (de) | Sprache |
| **Label** (en) | Language |
| **Description** (de) | Die Sprache, in der die Information angegeben ist. |
| **Description** (en) | The language, the information is given in. |
| **Contained by** | [`dc:title`](#d17e3011)  [`gndo:abbreviatedName`](#d17e3272)  [`gndo:acquaintanceshipOrFriendship`](#d17e3345)  [`gndo:biographicalOrHistoricalInformation`](#d17e3463)  [`gndo:broaderTerm`](#d17e3502)  [`gndo:familialRelationship`](#d17e4367)  [`gndo:gndSubjectCategory`](#d17e4734)  [`gndo:preferredName`](#person-record-preferredName)  [`gndo:preferredName`](#d17e5423)  [`gndo:temporaryName`](#d17e5924)  [`gndo:titleOfNobility`](#d17e5969)  [`gndo:variantName`](#person-record-variantName)  [`gndo:variantName`](#elem.gndo.variantName.standard)  [`skos:note`](#elem.skos.note) |

***Predefined Values***  

 - **aar**: (Afar): aar (ISO 639-3), aa (ISO 639-1); Afar [Afar]: Einzelsprache, Lebend
 - **abk**: (Abkhazian): abk (ISO 639-3), ab (ISO 639-1); Abchasisch [Abkhazian]: Einzelsprache, Lebend
 - **ace**: (Achinese): ace (ISO 639-3); Achinesisch [Achinese]: Einzelsprache, Lebend
 - **ach**: (Acoli): ach (ISO 639-3); Acholi [Acoli]: Einzelsprache, Lebend
 - **ada**: (Adangme): ada (ISO 639-3); Dangme [Adangme]: Einzelsprache, Lebend
 - **ady**: (Adyghe; Adygei): ady (ISO 639-3); Adygeisch [Adyghe; Adygei]: Einzelsprache, Lebend
 - **afa**: (Afro-Asiatic languages): afa (ISO 639-5); Afroasiatische Sprachen [Afro-Asiatic languages]: Sprachfamilie
 - **afh**: (Afrihili): afh (ISO 639-3); Afrihili [Afrihili]: Einzelsprache, Konstruiert
 - **afr**: (Afrikaans): afr (ISO 639-3), af (ISO 639-1); Afrikaans [Afrikaans]: Einzelsprache, Lebend
 - **ain**: (Ainu): ain (ISO 639-3); Ainu [Ainu]: Einzelsprache, Lebend
 - **aka**: (Akan): aka (ISO 639-3), ak (ISO 639-1); Akan [Akan]: Makrosprache, Lebend
 - **akk**: (Akkadian): akk (ISO 639-3); Akkadisch [Akkadian]: Einzelsprache, Alt
 - **ale**: (Aleut): ale (ISO 639-3); Aleutisch [Aleut]: Einzelsprache, Lebend
 - **alg**: (Algonquian languages): alg (ISO 639-5); Algonkin-Sprachen [Algonquian languages]: Sprachfamilie
 - **alt**: (Southern Altai): alt (ISO 639-3); Südaltaisch [Southern Altai]: Einzelsprache, Lebend
 - **amh**: (Amharic): amh (ISO 639-3), am (ISO 639-1); Amharisch [Amharic]: Einzelsprache, Lebend
 - **ang**: (English, Old (ca. 450–1100)): ang (ISO 639-3); Altenglisch [English, Old (ca. 450–1100)]: Einzelsprache, Historisch
 - **anp**: (Angika): anp (ISO 639-3); Angika [Angika]: Einzelsprache, Lebend
 - **apa**: (Apache languages): apa (ISO 639-5); Apache-Sprachen [Apache languages]: Sprachfamilie
 - **ara**: (Arabic): ara (ISO 639-3), ar (ISO 639-1); Arabisch [Arabic]: Makrosprache, Lebend
 - **arc**: (Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)): arc (ISO 639-3); Reichsaramäisch [Official Aramaic (700–300 v. Chr.); Imperial Aramaic (700–300 v. Chr.)]: Einzelsprache, Alt
 - **arg**: (Aragonese): arg (ISO 639-3), an (ISO 639-1); Aragonesisch [Aragonese]: Einzelsprache, Lebend
 - **arn**: (Mapudungun; Mapuche): arn (ISO 639-3); Mapudungun [Mapudungun; Mapuche]: Einzelsprache, Lebend
 - **arp**: (Arapaho): arp (ISO 639-3); Arapaho [Arapaho]: Einzelsprache, Lebend
 - **art**: (Artificial languages): art (ISO 639-5); Konstruierte Sprachen [Artificial languages]: Sprachfamilie
 - **arw**: (Arawak): arw (ISO 639-3); Arawak [Arawak]: Einzelsprache, Lebend
 - **asm**: (Assamese): asm (ISO 639-3), as (ISO 639-1); Assamesisch [Assamese]: Einzelsprache, Lebend
 - **ast**: (Asturian; Bable; Leonese; Asturleonese): ast (ISO 639-3); Asturisch [Asturian; Bable; Leonese; Asturleonese]: Einzelsprache, Lebend
 - **ath**: (Athapascan languages): ath (ISO 639-5); Athapaskische Sprachen [Athapascan languages]: Sprachfamilie
 - **aus**: (Australian languages): aus (ISO 639-5); Australische Sprachen [Australian languages]: Sprachfamilie
 - **ava**: (Avaric): ava (ISO 639-3), av (ISO 639-1); Awarisch [Avaric]: Einzelsprache, Lebend
 - **ave**: (Avestan): ave (ISO 639-3), ae (ISO 639-1); Avestisch [Avestan]: Einzelsprache, Alt
 - **awa**: (Awadhi): awa (ISO 639-3); Awadhi [Awadhi]: Einzelsprache, Lebend
 - **aym**: (Aymara): aym (ISO 639-3), ay (ISO 639-1); Aymara [Aymara]: Makrosprache, Lebend
 - **aze**: (Azerbaijani): aze (ISO 639-3), az (ISO 639-1); Aserbaidschanisch [Azerbaijani]: Makrosprache, Lebend
 - **bad**: (Banda languages): bad (ISO 639-5); Banda-Sprachen [Banda languages]: Sprachfamilie
 - **bai**: (Bamileke languages): bai (ISO 639-5); Bamileke-Sprachen [Bamileke languages]: Sprachfamilie
 - **bak**: (Bashkir): bak (ISO 639-3), ba (ISO 639-1); Baschkirisch [Bashkir]: Einzelsprache, Lebend
 - **bal**: (Baluchi): bal (ISO 639-3); Belutschisch [Baluchi]: Makrosprache, Lebend
 - **bam**: (Bambara): bam (ISO 639-3), bm (ISO 639-1); Bambara [Bambara]: Einzelsprache, Lebend
 - **ban**: (Balinese): ban (ISO 639-3); Balinesisch [Balinese]: Einzelsprache, Lebend
 - **bas**: (Basa): bas (ISO 639-3); Bassa [Basa]: Einzelsprache, Lebend
 - **bat**: (Baltic languages): bat (ISO 639-5); Baltische Sprachen [Baltic languages]: Sprachfamilie
 - **bej**: (Beja; Bedawiyet): bej (ISO 639-3); Bedscha [Beja; Bedawiyet]: Einzelsprache, Lebend
 - **bel**: (Belarusian): bel (ISO 639-3), be (ISO 639-1); Belarussisch [Belarusian]: Einzelsprache, Lebend
 - **bem**: (Bemba): bem (ISO 639-3); Bemba [Bemba]: Einzelsprache, Lebend
 - **ben**: (Bengali): ben (ISO 639-3), bn (ISO 639-1); Bengalisch [Bengali]: Einzelsprache, Lebend
 - **ber**: (Berber languages): ber (ISO 639-5); Berbersprachen [Berber languages]: Sprachfamilie
 - **bho**: (Bhojpuri): bho (ISO 639-3); Bhojpuri [Bhojpuri]: Einzelsprache, Lebend
 - **bih**: (Bihari languages): bih (ISO 639-5), bh (ISO 639-1); Bihari [Bihari languages]: Sprachfamilie
 - **bik**: (Bikol): bik (ISO 639-3); Bikolano [Bikol]: Makrosprache, Lebend
 - **bin**: (Bini; Edo): bin (ISO 639-3); Edo [Bini; Edo]: Einzelsprache, Lebend
 - **bis**: (Bislama): bis (ISO 639-3), bi (ISO 639-1); Bislama [Bislama]: Einzelsprache, Lebend
 - **bla**: (Siksika): bla (ISO 639-3); Blackfoot [Siksika]: Einzelsprache, Lebend
 - **bnt**: (Bantu (Other)): bnt (ISO 639-5); Bantusprachen [Bantu (Other)]: Sprachfamilie
 - **bos**: (Bosnian): bos (ISO 639-3), bs (ISO 639-1); Bosnisch [Bosnian]: Einzelsprache, Lebend
 - **bra**: (Braj): bra (ISO 639-3); Braj-Bhakha [Braj]: Einzelsprache, Lebend
 - **bre**: (Breton): bre (ISO 639-3), br (ISO 639-1); Bretonisch [Breton]: Einzelsprache, Lebend
 - **btk**: (Batak languages): btk (ISO 639-5); Bataksprachen [Batak languages]: Sprachfamilie
 - **bua**: (Buriat): bua (ISO 639-3); Burjatisch [Buriat]: Makrosprache, Lebend
 - **bug**: (Buginese): bug (ISO 639-3); Buginesisch [Buginese]: Einzelsprache, Lebend
 - **bul**: (Bulgarian): bul (ISO 639-3), bg (ISO 639-1); Bulgarisch [Bulgarian]: Einzelsprache, Lebend
 - **byn**: (Blin; Bilin): byn (ISO 639-3); Blin [Blin; Bilin]: Einzelsprache, Lebend
 - **cad**: (Caddo): cad (ISO 639-3); Caddo [Caddo]: Einzelsprache, Lebend
 - **cai**: (Central American Indian languages): cai (ISO 639-5); Mesoamerikanische Sprachen [Central American Indian languages]: Sprachfamilie
 - **car**: (Galibi Carib): car (ISO 639-3); Karib [Galibi Carib]: Einzelsprache, Lebend
 - **cat**: (Catalan; Valencian): cat (ISO 639-3), ca (ISO 639-1); Katalanisch, Valencianisch [Catalan; Valencian]: Einzelsprache, Lebend
 - **cau**: (Caucasian languages): cau (ISO 639-5); Kaukasische Sprachen [Caucasian languages]: Sprachfamilie
 - **ceb**: (Cebuano): ceb (ISO 639-3); Cebuano [Cebuano]: Einzelsprache, Lebend
 - **cel**: (Celtic languages): cel (ISO 639-5); Keltische Sprachen [Celtic languages]: Sprachfamilie
 - **cha**: (Chamorro): cha (ISO 639-3), ch (ISO 639-1); Chamorro [Chamorro]: Einzelsprache, Lebend
 - **chb**: (Chibcha): chb (ISO 639-3); Chibcha [Chibcha]: Einzelsprache, Ausgestorben
 - **che**: (Chechen): che (ISO 639-3), ce (ISO 639-1); Tschetschenisch [Chechen]: Einzelsprache, Lebend
 - **chg**: (Chagatai): chg (ISO 639-3); Tschagataisch [Chagatai]: Einzelsprache, Ausgestorben
 - **chk**: (Chuukese): chk (ISO 639-3); Chuukesisch [Chuukese]: Einzelsprache, Lebend
 - **chm**: (Mari): chm (ISO 639-3); Mari [Mari]: Makrosprache, Lebend
 - **chn**: (Chinook jargon): chn (ISO 639-3); Chinook Wawa [Chinook jargon]: Einzelsprache, Lebend
 - **cho**: (Choctaw): cho (ISO 639-3); Choctaw [Choctaw]: Einzelsprache, Lebend
 - **chp**: (Chipewyan; Dene Suline): chp (ISO 639-3); Chipewyan [Chipewyan; Dene Suline]: Einzelsprache, Lebend
 - **chr**: (Cherokee): chr (ISO 639-3); Cherokee [Cherokee]: Einzelsprache, Lebend
 - **chu**: (Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic): chu (ISO 639-3), cu (ISO 639-1); Kirchenslawisch, Altkirchenslawisch [Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic]: Einzelsprache, Alt
 - **chv**: (Chuvash): chv (ISO 639-3), cv (ISO 639-1); Tschuwaschisch [Chuvash]: Einzelsprache, Lebend
 - **chy**: (Cheyenne): chy (ISO 639-3); Cheyenne [Cheyenne]: Einzelsprache, Lebend
 - **cmc**: (Chamic languages): cmc (ISO 639-5); Chamische Sprachen [Chamic languages]: Sprachfamilie
 - **cnr**: (Montenegrin): cnr (ISO 639-3); Montenegrinisch [Montenegrin]: Einzelsprache, Lebend
 - **cop**: (Coptic): cop (ISO 639-3); Koptisch [Coptic]: Einzelsprache, Ausgestorben
 - **cor**: (Cornish): cor (ISO 639-3), kw (ISO 639-1); Kornisch [Cornish]: Einzelsprache, Lebend
 - **cos**: (Corsican): cos (ISO 639-3), co (ISO 639-1); Korsisch [Corsican]: Einzelsprache, Lebend
 - **cpe**: (Creoles and pidgins, English based): cpe (ISO 639-5); Englisch-basierte Kreols und Pidgins [Creoles and pidgins, English based]: Sprachfamilie
 - **cpf**: (Creoles and pidgins, French-based): cpf (ISO 639-5); Französisch-basierte Kreols und Pidgins [Creoles and pidgins, French-based]: Sprachfamilie
 - **cpp**: (Creoles and pidgins, Portuguese-based): cpp (ISO 639-5); Portugiesisch-basierte Kreols und Pidgins [Creoles and pidgins, Portuguese-based]: Sprachfamilie
 - **cre**: (Cree): cre (ISO 639-3), cr (ISO 639-1); Cree [Cree]: Makrosprache, Lebend
 - **crh**: (Crimean Tatar; Crimean Turkish): crh (ISO 639-3); Krimtatarisch [Crimean Tatar; Crimean Turkish]: Einzelsprache, Lebend
 - **crp**: (Creoles and pidgins): crp (ISO 639-5); Kreol- und Pidginsprachen [Creoles and pidgins]: Sprachfamilie
 - **csb**: (Kashubian): csb (ISO 639-3); Kaschubisch [Kashubian]: Einzelsprache, Lebend
 - **cus**: (Cushitic languages): cus (ISO 639-5); Kuschitische Sprachen [Cushitic languages]: Sprachfamilie
 - **dak**: (Dakota): dak (ISO 639-3); Dakota [Dakota]: Einzelsprache, Lebend
 - **dan**: (Danish): dan (ISO 639-3), da (ISO 639-1); Dänisch [Danish]: Einzelsprache, Lebend
 - **dar**: (Dargwa): dar (ISO 639-3); Darginisch [Dargwa]: Einzelsprache, Lebend
 - **day**: (Land Dayak languages): day (ISO 639-5); Land-Dayak-Sprachen [Land Dayak languages]: Sprachfamilie
 - **del**: (Delaware): del (ISO 639-3); Delawarisch [Delaware]: Makrosprache, Lebend
 - **den**: (Slave (Athapascan)): den (ISO 639-3); Slavey [Slave (Athapascan)]: Makrosprache, Lebend
 - **dgr**: (Dogrib): dgr (ISO 639-3); Dogrib [Dogrib]: Einzelsprache, Lebend
 - **din**: (Dinka): din (ISO 639-3); Dinka [Dinka]: Makrosprache, Lebend
 - **div**: (Divehi; Dhivehi; Maldivian): div (ISO 639-3), dv (ISO 639-1); Dhivehi [Divehi; Dhivehi; Maldivian]: Einzelsprache, Lebend
 - **doi**: (Dogri): doi (ISO 639-3); Dogri [Dogri]: Makrosprache, Lebend
 - **dra**: (Dravidian languages): dra (ISO 639-5); Dravidische Sprachen [Dravidian languages]: Sprachfamilie
 - **dsb**: (Lower Sorbian): dsb (ISO 639-3); Niedersorbisch [Lower Sorbian]: Einzelsprache, Lebend
 - **dua**: (Duala): dua (ISO 639-3); Duala [Duala]: Einzelsprache, Lebend
 - **dum**: (Dutch, Middle (ca. 1050–1350)): dum (ISO 639-3); Mittelniederländisch [Dutch, Middle (ca. 1050–1350)]: Einzelsprache, Historisch
 - **dyu**: (Dyula): dyu (ISO 639-3); Dioula [Dyula]: Einzelsprache, Lebend
 - **dzo**: (Dzongkha): dzo (ISO 639-3), dz (ISO 639-1); Dzongkha [Dzongkha]: Einzelsprache, Lebend
 - **efi**: (Efik): efi (ISO 639-3); Efik [Efik]: Einzelsprache, Lebend
 - **egy**: (Egyptian (Ancient)): egy (ISO 639-3); Ägyptisch [Egyptian (Ancient)]: Einzelsprache, Alt
 - **eka**: (Ekajuk): eka (ISO 639-3); Ekajuk [Ekajuk]: Einzelsprache, Lebend
 - **elx**: (Elamite): elx (ISO 639-3); Elamisch [Elamite]: Einzelsprache, Alt
 - **eng**: (English): eng (ISO 639-3), en (ISO 639-1); Englisch [English]: Einzelsprache, Lebend
 - **enm**: (English, Middle (1100–1500)): enm (ISO 639-3); Mittelenglisch [English, Middle (1100–1500)]: Einzelsprache, Historisch
 - **epo**: (Esperanto): epo (ISO 639-3), eo (ISO 639-1); Esperanto [Esperanto]: Einzelsprache, Konstruiert
 - **est**: (Estonian): est (ISO 639-3), et (ISO 639-1); Estnisch [Estonian]: Makrosprache, Lebend
 - **ewe**: (Ewe): ewe (ISO 639-3), ee (ISO 639-1); Ewe [Ewe]: Einzelsprache, Lebend
 - **ewo**: (Ewondo): ewo (ISO 639-3); Ewondo [Ewondo]: Einzelsprache, Lebend
 - **fan**: (Fang): fan (ISO 639-3); Fang [Fang]: Einzelsprache, Lebend
 - **fao**: (Faroese): fao (ISO 639-3), fo (ISO 639-1); Färöisch [Faroese]: Einzelsprache, Lebend
 - **fat**: (Fanti): fat (ISO 639-3); Fante [Fanti]: Einzelsprache, Lebend
 - **fij**: (Fijian): fij (ISO 639-3), fj (ISO 639-1); Fidschi [Fijian]: Einzelsprache, Lebend
 - **fil**: (Filipino; Pilipino): fil (ISO 639-3); Filipino [Filipino; Pilipino]: Einzelsprache, Lebend
 - **fin**: (Finnish): fin (ISO 639-3), fi (ISO 639-1); Finnisch [Finnish]: Einzelsprache, Lebend
 - **fiu**: (Finno-Ugrian languages): fiu (ISO 639-5); Finno-ugrische Sprachen [Finno-Ugrian languages]: Sprachfamilie
 - **fon**: (Fon): fon (ISO 639-3); Fon [Fon]: Einzelsprache, Lebend
 - **frm**: (French, Middle (ca. 1400–1600)): frm (ISO 639-3); Mittelfranzösisch [French, Middle (ca. 1400–1600)]: Einzelsprache, Historisch
 - **fro**: (French, Old (842–ca. 1400)): fro (ISO 639-3); Altfranzösisch [French, Old (842–ca. 1400)]: Einzelsprache, Historisch
 - **frr**: (Northern Frisian): frr (ISO 639-3); Nordfriesisch [Northern Frisian]: Einzelsprache, Lebend
 - **frs**: (East Frisian Low Saxon): frs (ISO 639-3); Ostfriesisches Platt [East Frisian Low Saxon]: Einzelsprache, Lebend
 - **fry**: (Western Frisian): fry (ISO 639-3), fy (ISO 639-1); Westfriesisch [Western Frisian]: Einzelsprache, Lebend
 - **ful**: (Fulah): ful (ISO 639-3), ff (ISO 639-1); Fulfulde [Fulah]: Makrosprache, Lebend
 - **fur**: (Friulian): fur (ISO 639-3); Furlanisch [Friulian]: Einzelsprache, Lebend
 - **gaa**: (Einzelsprache): gaa (ISO 639-3), Ga (ISO 639-1); Ga [Einzelsprache]: Lebend, Gã 
 - **gay**: (Gayo): gay (ISO 639-3); Gayo [Gayo]: Einzelsprache, Lebend
 - **gba**: (Gbaya): gba (ISO 639-3); Gbaya-Sprachen [Gbaya]: Makrosprache, Lebend
 - **gem**: (Germanic languages): gem (ISO 639-5); Germanische Sprachen [Germanic languages]: Sprachfamilie
 - **gez**: (Geez): gez (ISO 639-3); Altäthiopisch [Geez]: Einzelsprache, Alt
 - **gil**: (Gilbertese): gil (ISO 639-3); Kiribatisch, Gilbertesisch [Gilbertese]: Einzelsprache, Lebend
 - **gla**: (Gaelic; Scottish Gaelic): gla (ISO 639-3), gd (ISO 639-1); Schottisch-gälisch [Gaelic; Scottish Gaelic]: Einzelsprache, Lebend
 - **gle**: (Irish): gle (ISO 639-3), ga (ISO 639-1); Irisch [Irish]: Einzelsprache, Lebend
 - **glg**: (Galician): glg (ISO 639-3), gl (ISO 639-1); Galicisch, Galegisch [Galician]: Einzelsprache, Lebend
 - **glv**: (Manx): glv (ISO 639-3), gv (ISO 639-1); Manx,Manx-Gälisch [Manx]: Einzelsprache, Lebend
 - **gmh**: (German, Middle High (ca. 1050–1500)): gmh (ISO 639-3); Mittelhochdeutsch [German, Middle High (ca. 1050–1500)]: Einzelsprache, Historisch
 - **goh**: (German, Old High (ca. 750–1050)): goh (ISO 639-3); Althochdeutsch [German, Old High (ca. 750–1050)]: Einzelsprache, Historisch
 - **gon**: (Gondi): gon (ISO 639-3); Gondi [Gondi]: Makrosprache, Lebend
 - **gor**: (Gorontalo): gor (ISO 639-3); Gorontalo [Gorontalo]: Einzelsprache, Lebend
 - **got**: (Gothic): got (ISO 639-3); Gotisch [Gothic]: Einzelsprache, Alt
 - **grb**: (Grebo): grb (ISO 639-3); Grebo [Grebo]: Makrosprache, Lebend
 - **grc**: (Greek, Ancient (bis 1453)): grc (ISO 639-3); Altgriechisch [Greek, Ancient (bis 1453)]: Einzelsprache, Historisch
 - **grn**: (Guarani): grn (ISO 639-3), gn (ISO 639-1); Guaraní [Guarani]: Makrosprache, Lebend
 - **gsw**: (Swiss German; Alemannic; Alsatian): gsw (ISO 639-3); Schweizerdeutsch [Swiss German; Alemannic; Alsatian]: Einzelsprache, Lebend
 - **guj**: (Gujarati): guj (ISO 639-3), gu (ISO 639-1); Gujarati [Gujarati]: Einzelsprache, Lebend
 - **gwi**: (Gwich'in): gwi (ISO 639-3); Gwich'in (Sprache) [Gwich'in]: Einzelsprache, Lebend
 - **hai**: (Haida): hai (ISO 639-3); Haida [Haida]: Makrosprache, Lebend
 - **hat**: (Haitian; Haitian Creole): hat (ISO 639-3), ht (ISO 639-1); Haitianisch-Kreolisch [Haitian; Haitian Creole]: Einzelsprache, Lebend
 - **hau**: (Hausa): hau (ISO 639-3), ha (ISO 639-1); Hausa [Hausa]: Einzelsprache, Lebend
 - **haw**: (Hawaiian): haw (ISO 639-3); Hawaiisch [Hawaiian]: Einzelsprache, Lebend
 - **heb**: (Hebrew): heb (ISO 639-3), he (ISO 639-1); Hebräisch [Hebrew]: Einzelsprache, Lebend
 - **her**: (Herero): her (ISO 639-3), hz (ISO 639-1); Otjiherero [Herero]: Einzelsprache, Lebend
 - **hil**: (Hiligaynon): hil (ISO 639-3); Hiligaynon [Hiligaynon]: Einzelsprache, Lebend
 - **him**: (Himachali languages; Western Pahari languages): him (ISO 639-5); West-Paharisprachen [Himachali languages; Western Pahari languages]: Sprachfamilie
 - **hin**: (Hindi): hin (ISO 639-3), hi (ISO 639-1); Hindi [Hindi]: Einzelsprache, Lebend
 - **hit**: (Hittite): hit (ISO 639-3); Hethitisch [Hittite]: Einzelsprache, Alt
 - **hmn**: (Hmong; Mong): hmn (ISO 639-3); Hmong-Sprache [Hmong; Mong]: Makrosprache, Lebend
 - **hmo**: (Hiri Motu): hmo (ISO 639-3), ho (ISO 639-1); Hiri Motu [Hiri Motu]: Einzelsprache, Lebend
 - **hrv**: (Croatian): hrv (ISO 639-3), hr (ISO 639-1); Kroatisch [Croatian]: Einzelsprache, Lebend
 - **hsb**: (Upper Sorbian): hsb (ISO 639-3); Obersorbisch [Upper Sorbian]: Einzelsprache, Lebend
 - **hun**: (Hungarian): hun (ISO 639-3), hu (ISO 639-1); Ungarisch [Hungarian]: Einzelsprache, Lebend
 - **hup**: (Hupa): hup (ISO 639-3); Hoopa [Hupa]: Einzelsprache, Lebend
 - **iba**: (Iban): iba (ISO 639-3); Iban [Iban]: Einzelsprache, Lebend
 - **ibo**: (Igbo): ibo (ISO 639-3), ig (ISO 639-1); Igbo [Igbo]: Einzelsprache, Lebend
 - **ido**: (Ido): ido (ISO 639-3), io (ISO 639-1); Ido [Ido]: Einzelsprache, Konstruiert
 - **iii**: (Sichuan Yi; Nuosu): iii (ISO 639-3), ii (ISO 639-1); Yi [Sichuan Yi; Nuosu]: Einzelsprache, Lebend
 - **ijo**: (Ijo languages): ijo (ISO 639-5); Ijo-Sprachen [Ijo languages]: Sprachfamilie
 - **iku**: (Inuktitut): iku (ISO 639-3), iu (ISO 639-1); Inuktitut [Inuktitut]: Makrosprache, Lebend
 - **ile**: (Interlingue; Occidental): ile (ISO 639-3), ie (ISO 639-1); Interlingue [Interlingue; Occidental]: Einzelsprache, Konstruiert
 - **ilo**: (Iloko): ilo (ISO 639-3); Ilokano [Iloko]: Einzelsprache, Lebend
 - **ina**: (Interlingua (International Auxiliary Language Association)): ina (ISO 639-3), ia (ISO 639-1); Interlingua [Interlingua (International Auxiliary Language Association)]: Einzelsprache, Konstruiert
 - **inc**: (Indic languages): inc (ISO 639-5); Indoarische Sprachen [Indic languages]: Sprachfamilie
 - **ind**: (Indonesian): ind (ISO 639-3), id (ISO 639-1); Indonesisch [Indonesian]: Einzelsprache, Lebend
 - **ine**: (Indo-European languages): ine (ISO 639-5); Indogermanische Sprachen [Indo-European languages]: Sprachfamilie
 - **inh**: (Ingush): inh (ISO 639-3); Inguschisch [Ingush]: Einzelsprache, Lebend
 - **ipk**: (Inupiaq): ipk (ISO 639-3), ik (ISO 639-1); Inupiaq [Inupiaq]: Makrosprache, Lebend
 - **ira**: (Iranian languages): ira (ISO 639-5); Iranische Sprachen [Iranian languages]: Sprachfamilie
 - **iro**: (Iroquoian languages): iro (ISO 639-5); Irokesische Sprachen [Iroquoian languages]: Sprachfamilie
 - **ita**: (Italian): ita (ISO 639-3), it (ISO 639-1); Italienisch [Italian]: Einzelsprache, Lebend
 - **jav**: (Javanese): jav (ISO 639-3), jv (ISO 639-1); Javanisch [Javanese]: Einzelsprache, Lebend
 - **jbo**: (Lojban): jbo (ISO 639-3); Lojban [Lojban]: Einzelsprache, Konstruiert
 - **jpn**: (Japanese): jpn (ISO 639-3), ja (ISO 639-1); Japanisch [Japanese]: Einzelsprache, Lebend
 - **jpr**: (Judeo-Persian): jpr (ISO 639-3); Judäo-Persisch [Judeo-Persian]: Einzelsprache, Lebend
 - **jrb**: (Judeo-Arabic): jrb (ISO 639-3); Judäo-Arabisch [Judeo-Arabic]: Makrosprache, Lebend
 - **kaa**: (Kara-Kalpak): kaa (ISO 639-3); Karakalpakisch [Kara-Kalpak]: Einzelsprache, Lebend
 - **kab**: (Kabyle): kab (ISO 639-3); Kabylisch [Kabyle]: Einzelsprache, Lebend
 - **kac**: (Kachin; Jingpho): kac (ISO 639-3); Jingpo [Kachin; Jingpho]: Einzelsprache, Lebend
 - **kal**: (Kalaallisut; Greenlandic): kal (ISO 639-3), kl (ISO 639-1); Grönländisch, Kalaallisut [Kalaallisut; Greenlandic]: Einzelsprache, Lebend
 - **kam**: (Kamba): kam (ISO 639-3); Kikamba [Kamba]: Einzelsprache, Lebend
 - **kan**: (Kannada): kan (ISO 639-3), kn (ISO 639-1); Kannada [Kannada]: Einzelsprache, Lebend
 - **kar**: (Karen languages): kar (ISO 639-5); Karenische Sprachen [Karen languages]: Sprachfamilie
 - **kas**: (Kashmiri): kas (ISO 639-3), ks (ISO 639-1); Kashmiri [Kashmiri]: Einzelsprache, Lebend
 - **kau**: (Kanuri): kau (ISO 639-3), kr (ISO 639-1); Kanuri [Kanuri]: Makrosprache, Lebend
 - **kaw**: (Kawi): kaw (ISO 639-3); Kawi, Altjavanisch [Kawi]: Einzelsprache, Alt
 - **kaz**: (Kazakh): kaz (ISO 639-3), kk (ISO 639-1); Kasachisch [Kazakh]: Einzelsprache, Lebend
 - **kbd**: (Kabardian): kbd (ISO 639-3); Kabardinisch, Ost-Tscherkessisch [Kabardian]: Einzelsprache, Lebend
 - **kha**: (Khasi): kha (ISO 639-3); Khasi [Khasi]: Einzelsprache, Lebend
 - **khi**: (Khoisan languages): khi (ISO 639-5); Khoisansprachen [Khoisan languages]: Sprachfamilie
 - **khm**: (Central Khmer): khm (ISO 639-3), km (ISO 639-1); Khmer [Central Khmer]: Einzelsprache, Lebend
 - **kho**: (Khotanese; Sakan): kho (ISO 639-3); Khotanesisch [Khotanese; Sakan]: Einzelsprache, Alt
 - **kik**: (Kikuyu; Gikuyu): kik (ISO 639-3), ki (ISO 639-1); Kikuyu [Kikuyu; Gikuyu]: Einzelsprache, Lebend
 - **kin**: (Kinyarwanda): kin (ISO 639-3), rw (ISO 639-1); Kinyarwanda, Ruandisch [Kinyarwanda]: Einzelsprache, Lebend
 - **kir**: (Kirghiz; Kyrgyz): kir (ISO 639-3), ky (ISO 639-1); Kirgisisch [Kirghiz; Kyrgyz]: Einzelsprache, Lebend
 - **kmb**: (Kimbundu): kmb (ISO 639-3); Kimbundu [Kimbundu]: Einzelsprache, Lebend
 - **kok**: (Konkani): kok (ISO 639-3); Konkani [Konkani]: Makrosprache, Lebend
 - **kom**: (Komi): kom (ISO 639-3), kv (ISO 639-1); Komi [Komi]: Makrosprache, Lebend
 - **kon**: (Kongo): kon (ISO 639-3), kg (ISO 639-1); Kikongo [Kongo]: Makrosprache, Lebend
 - **kor**: (Korean): kor (ISO 639-3), ko (ISO 639-1); Koreanisch [Korean]: Einzelsprache, Lebend
 - **kos**: (Kosraean): kos (ISO 639-3); Kosraeanisch [Kosraean]: Einzelsprache, Lebend
 - **kpe**: (Kpelle): kpe (ISO 639-3); Kpelle [Kpelle]: Makrosprache, Lebend
 - **krc**: (Karachay-Balkar): krc (ISO 639-3); Karatschai-balkarisch [Karachay-Balkar]: Einzelsprache, Lebend
 - **krl**: (Karelian): krl (ISO 639-3); Karelisch [Karelian]: Einzelsprache, Lebend
 - **kro**: (Kru languages): kro (ISO 639-5); Kru-Sprachen [Kru languages]: Sprachfamilie
 - **kru**: (Kurukh): kru (ISO 639-3); Kurukh [Kurukh]: Einzelsprache, Lebend
 - **kua**: (Kuanyama; Kwanyama): kua (ISO 639-3), kj (ISO 639-1); oshiKwanyama [Kuanyama; Kwanyama]: Einzelsprache, Lebend
 - **kum**: (Kumyk): kum (ISO 639-3); Kumykisch [Kumyk]: Einzelsprache, Lebend
 - **kur**: (Kurdish): kur (ISO 639-3), ku (ISO 639-1); Kurdisch [Kurdish]: Makrosprache, Lebend
 - **kut**: (Kutenai): kut (ISO 639-3); Kutanaha [Kutenai]: Einzelsprache, Lebend
 - **lad**: (Ladino): lad (ISO 639-3); Judenspanisch, Ladino, Sephardisch [Ladino]: Einzelsprache, Lebend
 - **lah**: (Lahnda): lah (ISO 639-3); Lahnda, Westpanjabi [Lahnda]: Makrosprache, Lebend
 - **lam**: (Lamba): lam (ISO 639-3); Lamba [Lamba]: Einzelsprache, Lebend
 - **lao**: (Lao): lao (ISO 639-3), lo (ISO 639-1); Laotisch [Lao]: Einzelsprache, Lebend
 - **lat**: (Latin): lat (ISO 639-3), la (ISO 639-1); Latein [Latin]: Einzelsprache, Alt
 - **lav**: (Latvian): lav (ISO 639-3), lv (ISO 639-1); Lettisch [Latvian]: Makrosprache, Lebend
 - **lez**: (Lezghian): lez (ISO 639-3); Lesgisch [Lezghian]: Einzelsprache, Lebend
 - **lim**: (Limburgan; Limburger; Limburgish): lim (ISO 639-3), li (ISO 639-1); Limburgisch, Südniederfränkisch [Limburgan; Limburger; Limburgish]: Einzelsprache, Lebend
 - **lin**: (Lingala): lin (ISO 639-3), ln (ISO 639-1); Lingála [Lingala]: Einzelsprache, Lebend
 - **lit**: (Lithuanian): lit (ISO 639-3), lt (ISO 639-1); Litauisch [Lithuanian]: Einzelsprache, Lebend
 - **lol**: (Mongo): lol (ISO 639-3); Lomongo [Mongo]: Einzelsprache, Lebend
 - **loz**: (Lozi): loz (ISO 639-3); Lozi [Lozi]: Einzelsprache, Lebend
 - **ltz**: (Luxembourgish; Letzeburgesch): ltz (ISO 639-3), lb (ISO 639-1); Luxemburgisch [Luxembourgish; Letzeburgesch]: Einzelsprache, Lebend
 - **lua**: (Luba-Lulua): lua (ISO 639-3); Tschiluba [Luba-Lulua]: Einzelsprache, Lebend
 - **lub**: (Luba-Katanga): lub (ISO 639-3), lu (ISO 639-1); Kiluba [Luba-Katanga]: Einzelsprache, Lebend
 - **lug**: (Ganda): lug (ISO 639-3), lg (ISO 639-1); Luganda [Ganda]: Einzelsprache, Lebend
 - **lui**: (Luiseno): lui (ISO 639-3); Luiseño [Luiseno]: Einzelsprache, Ausgestorben
 - **lun**: (Lunda): lun (ISO 639-3); Chilunda [Lunda]: Einzelsprache, Lebend
 - **luo**: (Luo (Kenya and Tanzania)): luo (ISO 639-3); Luo [Luo (Kenya and Tanzania)]: Einzelsprache, Lebend
 - **lus**: (Lushai): lus (ISO 639-3); Mizo, Lushai [Lushai]: Einzelsprache, Lebend
 - **mad**: (Madurese): mad (ISO 639-3); Maduresisch [Madurese]: Einzelsprache, Lebend
 - **mag**: (Magahi): mag (ISO 639-3); Magadhi [Magahi]: Einzelsprache, Lebend
 - **mah**: (Marshallese): mah (ISO 639-3), mh (ISO 639-1); Marshallesisch [Marshallese]: Einzelsprache, Lebend
 - **mai**: (Maithili): mai (ISO 639-3); Maithili [Maithili]: Einzelsprache, Lebend
 - **mak**: (Makasar): mak (ISO 639-3); Makassar [Makasar]: Einzelsprache, Lebend
 - **mal**: (Malayalam): mal (ISO 639-3), ml (ISO 639-1); Malayalam [Malayalam]: Einzelsprache, Lebend
 - **man**: (Mandingo): man (ISO 639-3); Manding [Mandingo]: Makrosprache, Lebend
 - **map**: (Austronesian languages): map (ISO 639-5); Austronesische Sprachen [Austronesian languages]: Sprachfamilie
 - **mar**: (Marathi): mar (ISO 639-3), mr (ISO 639-1); Marathi [Marathi]: Einzelsprache, Lebend
 - **mas**: (Masai): mas (ISO 639-3); Maa, Kimaasai [Masai]: Einzelsprache, Lebend
 - **mdf**: (Moksha): mdf (ISO 639-3); Mokschanisch [Moksha]: Einzelsprache, Lebend
 - **mdr**: (Mandar): mdr (ISO 639-3); Mandar [Mandar]: Einzelsprache, Lebend
 - **men**: (Mende): men (ISO 639-3); Mende [Mende]: Einzelsprache, Lebend
 - **mga**: (Irish, Middle (900–1200)): mga (ISO 639-3); Mittelirisch [Irish, Middle (900–1200)]: Einzelsprache, Historisch
 - **mic**: (Mi'kmaq; Micmac): mic (ISO 639-3); Míkmawísimk [Mi'kmaq; Micmac]: Einzelsprache, Lebend
 - **min**: (Minangkabau): min (ISO 639-3); Minangkabauisch [Minangkabau]: Einzelsprache, Lebend
 - **mis**: (Uncoded languages): mis (ISO 639-3); „Unkodiert“ [Uncoded languages]: Speziell
 - **mkh**: (Mon-Khmer languages): mkh (ISO 639-5); Mon-Khmer-Sprachen [Mon-Khmer languages]: Sprachfamilie
 - **mlg**: (Malagasy): mlg (ISO 639-3), mg (ISO 639-1); Malagasy, Malagassi [Malagasy]: Makrosprache, Lebend
 - **mlt**: (Maltese): mlt (ISO 639-3), mt (ISO 639-1); Maltesisch [Maltese]: Einzelsprache, Lebend
 - **mnc**: (Manchu): mnc (ISO 639-3); Mandschurisch [Manchu]: Einzelsprache, Lebend
 - **mni**: (Manipuri): mni (ISO 639-3); Meitei [Manipuri]: Einzelsprache, Lebend
 - **mno**: (Manobo languages): mno (ISO 639-5); Manobo-Sprachen [Manobo languages]: Sprachfamilie
 - **moh**: (Mohawk): moh (ISO 639-3); Mohawk [Mohawk]: Einzelsprache, Lebend
 - **mon**: (Mongolian): mon (ISO 639-3), mn (ISO 639-1); Mongolisch [Mongolian]: Makrosprache, Lebend
 - **mos**: (Mossi): mos (ISO 639-3); Mòoré [Mossi]: Einzelsprache, Lebend
 - **mul**: (Multiple languages): mul (ISO 639-3); „Mehrsprachig“ [Multiple languages]: Speziell
 - **mun**: (Munda languages): mun (ISO 639-5); Munda-Sprachen [Munda languages]: Sprachfamilie
 - **mus**: (Creek): mus (ISO 639-3); Muskogee-Sprachen [Creek]: Sprachfamilie
 - **mwl**: (Mirandese): mwl (ISO 639-3); Mirandés [Mirandese]: Einzelsprache, Lebend
 - **mwr**: (Marwari): mwr (ISO 639-3); Marwari [Marwari]: Makrosprache, Lebend
 - **myn**: (Mayan languages): myn (ISO 639-5); Maya-Sprachen [Mayan languages]: Sprachfamilie
 - **myv**: (Erzya): myv (ISO 639-3); Ersjanisch, Ersja-Mordwinisch [Erzya]: Einzelsprache, Lebend
 - **nah**: (Nahuatl languages): nah (ISO 639-5); Nahuatl [Nahuatl languages]: Sprachfamilie
 - **nai**: (North American Indian languages): nai (ISO 639-5); Nordamerikanische Sprachen [North American Indian languages]: Sprachfamilie
 - **nap**: (Neapolitan): nap (ISO 639-3); Neapolitanisch [Neapolitan]: Einzelsprache, Lebend
 - **nau**: (Nauru): nau (ISO 639-3), na (ISO 639-1); Nauruisch [Nauru]: Einzelsprache, Lebend
 - **nav**: (Navajo; Navaho): nav (ISO 639-3), nv (ISO 639-1); Navajo [Navajo; Navaho]: Einzelsprache, Lebend
 - **nbl**: (Ndebele, South; South Ndebele): nbl (ISO 639-3), nr (ISO 639-1); Süd-Ndebele [Ndebele, South; South Ndebele]: Einzelsprache, Lebend
 - **nde**: (Ndebele, North; North Ndebele): nde (ISO 639-3), nd (ISO 639-1); Nord-Ndebele [Ndebele, North; North Ndebele]: Einzelsprache, Lebend
 - **ndo**: (Ndonga): ndo (ISO 639-3), ng (ISO 639-1); Ndonga [Ndonga]: Einzelsprache, Lebend
 - **nds**: (Low German; Low Saxon; German, Low; Saxon, Low): nds (ISO 639-3); Niederdeutsch, Plattdeutsch [Low German; Low Saxon; German, Low; Saxon, Low]: Einzelsprache, Lebend
 - **nep**: (Nepali): nep (ISO 639-3), ne (ISO 639-1); Nepali [Nepali]: Makrosprache, Lebend
 - **new**: (Nepal Bhasa; Newari): new (ISO 639-3); Newari [Nepal Bhasa; Newari]: Einzelsprache, Lebend
 - **nia**: (Nias): nia (ISO 639-3); Nias [Nias]: Einzelsprache, Lebend
 - **nic**: (Niger-Kordofanian languages): nic (ISO 639-5); Niger-Kongo-Sprachen [Niger-Kordofanian languages]: Sprachfamilie
 - **niu**: (Niuean): niu (ISO 639-3); Niueanisch [Niuean]: Einzelsprache, Lebend
 - **nno**: (Norwegian Nynorsk; Nynorsk, Norwegian): nno (ISO 639-3), nn (ISO 639-1); Nynorsk [Norwegian Nynorsk; Nynorsk, Norwegian]: Einzelsprache, Lebend
 - **nob**: (Bokmål, Norwegian; Norwegian Bokmål): nob (ISO 639-3), nb (ISO 639-1); Bokmål [Bokmål, Norwegian; Norwegian Bokmål]: Einzelsprache, Lebend
 - **nog**: (Nogai): nog (ISO 639-3); Nogaisch [Nogai]: Einzelsprache, Lebend
 - **non**: (Norse, Old): non (ISO 639-3); Altnordisch [Norse, Old]: Einzelsprache, Historisch
 - **nor**: (Norwegian): nor (ISO 639-3), no (ISO 639-1); Norwegisch [Norwegian]: Makrosprache, Lebend
 - **nqo**: (N'Ko): nqo (ISO 639-3); N’Ko [N'Ko]: Einzelsprache, Lebend
 - **nso**: (Pedi; Sepedi; Northern Sotho): nso (ISO 639-3); Nord-Sotho [Pedi; Sepedi; Northern Sotho]: Einzelsprache, Lebend
 - **nub**: (Nubian languages): nub (ISO 639-5); Nubische Sprachen [Nubian languages]: Sprachfamilie
 - **nwc**: (Classical Newari; Old Newari; Classical Nepal Bhasa): nwc (ISO 639-3); Klassisches Newari [Classical Newari; Old Newari; Classical Nepal Bhasa]: Einzelsprache, Historisch
 - **nya**: (Chichewa; Chewa; Nyanja): nya (ISO 639-3), ny (ISO 639-1); Chichewa [Chichewa; Chewa; Nyanja]: Einzelsprache, Lebend
 - **nym**: (Nyamwezi): nym (ISO 639-3); Nyamwesi [Nyamwezi]: Einzelsprache, Lebend
 - **nyn**: (Nyankole): nyn (ISO 639-3); Runyankole, Runyankore [Nyankole]: Einzelsprache, Lebend
 - **nyo**: (Nyoro): nyo (ISO 639-3); Runyoro [Nyoro]: Einzelsprache, Lebend
 - **nzi**: (Nzima): nzi (ISO 639-3); Nzema [Nzima]: Einzelsprache, Lebend
 - **oci**: (Occitan (ab 1500); Provençal): oci (ISO 639-3), oc (ISO 639-1); Okzitanisch [Occitan (ab 1500); Provençal]: Einzelsprache, Lebend
 - **oji**: (Ojibwa): oji (ISO 639-3), oj (ISO 639-1); Ojibwe [Ojibwa]: Makrosprache, Lebend
 - **ori**: (Oriya): ori (ISO 639-3), or (ISO 639-1); Oriya [Oriya]: Makrosprache, Lebend
 - **orm**: (Oromo): orm (ISO 639-3), om (ISO 639-1); Oromo [Oromo]: Makrosprache, Lebend
 - **osa**: (Osage): osa (ISO 639-3); Osage [Osage]: Einzelsprache, Lebend
 - **oss**: (Ossetian; Ossetic): oss (ISO 639-3), os (ISO 639-1); Ossetisch [Ossetian; Ossetic]: Einzelsprache, Lebend
 - **ota**: (Turkish, Ottoman (1500–1928)): ota (ISO 639-3); Osmanisch, osmanisches Türkisch [Turkish, Ottoman (1500–1928)]: Einzelsprache, Historisch
 - **oto**: (Otomian languages): oto (ISO 639-5); Oto-Pame-Sprachen [Otomian languages]: Sprachfamilie
 - **paa**: (Papuan languages): paa (ISO 639-5); Papuasprachen [Papuan languages]: Sprachfamilie
 - **pag**: (Pangasinan): pag (ISO 639-3); Pangasinensisch [Pangasinan]: Einzelsprache, Lebend
 - **pal**: (Pahlavi): pal (ISO 639-3); Mittelpersisch, Pahlavi [Pahlavi]: Einzelsprache, Alt
 - **pam**: (Pampanga; Kapampangan): pam (ISO 639-3); Kapampangan [Pampanga; Kapampangan]: Einzelsprache, Lebend
 - **pan**: (Panjabi; Punjabi): pan (ISO 639-3), pa (ISO 639-1); Panjabi, Pandschabi [Panjabi; Punjabi]: Einzelsprache, Lebend
 - **pap**: (Papiamento): pap (ISO 639-3); Papiamentu [Papiamento]: Einzelsprache, Lebend
 - **pau**: (Palauan): pau (ISO 639-3); Palauisch [Palauan]: Einzelsprache, Lebend
 - **peo**: (Persian, Old (ca. 600–400 v. Chr.)): peo (ISO 639-3); Altpersisch [Persian, Old (ca. 600–400 v. Chr.)]: Einzelsprache, Historisch
 - **phi**: (Philippine languages): phi (ISO 639-5); Philippinische Sprachen [Philippine languages]: Sprachfamilie
 - **phn**: (Phoenician): phn (ISO 639-3); Phönizisch, Punisch [Phoenician]: Einzelsprache, Alt
 - **pli**: (Pali): pli (ISO 639-3), pi (ISO 639-1); Pali [Pali]: Einzelsprache, Alt
 - **pol**: (Polish): pol (ISO 639-3), pl (ISO 639-1); Polnisch [Polish]: Einzelsprache, Lebend
 - **pon**: (Pohnpeian): pon (ISO 639-3); Pohnpeanisch [Pohnpeian]: Einzelsprache, Lebend
 - **por**: (Portuguese): por (ISO 639-3), pt (ISO 639-1); Portugiesisch [Portuguese]: Einzelsprache, Lebend
 - **pra**: (Prakrit languages): pra (ISO 639-5); Prakrit [Prakrit languages]: Sprachfamilie
 - **pro**: (Provençal, Old (bis 1500); Old Occitan (bis 1500)): pro (ISO 639-3); Altokzitanisch, Altprovenzalisch [Provençal, Old (bis 1500); Old Occitan (bis 1500)]: Einzelsprache, Historisch
 - **pus**: (Pushto; Pashto): pus (ISO 639-3), ps (ISO 639-1); Paschtunisch [Pushto; Pashto]: Makrosprache, Lebend
 - **qaa-qtz**: Reserviert für lokale Nutzung, Reserved for local use
 - **que**: (Quechua): que (ISO 639-3), qwe (ISO 639-3), qu (ISO 639-1); Quechua [Quechua]: Makrosprache, Lebend
 - **raj**: (Rajasthani): raj (ISO 639-3); Rajasthani [Rajasthani]: Makrosprache, Lebend
 - **rap**: (Rapanui): rap (ISO 639-3); Rapanui [Rapanui]: Einzelsprache, Lebend
 - **rar**: (Rarotongan; Cook Islands Maori): rar (ISO 639-3); Rarotonganisch, Māori der Cookinseln [Rarotongan; Cook Islands Maori]: Einzelsprache, Lebend
 - **roa**: (Romance languages): roa (ISO 639-5); Romanische Sprachen [Romance languages]: Sprachfamilie
 - **roh**: (Romansh): roh (ISO 639-3), rm (ISO 639-1); Bündnerromanisch, Romanisch [Romansh]: Einzelsprache, Lebend
 - **rom**: (Romany): rom (ISO 639-3); Romani, Romanes [Romany]: Makrosprache, Lebend
 - **run**: (Rundi): run (ISO 639-3), rn (ISO 639-1); Kirundi [Rundi]: Einzelsprache, Lebend
 - **rup**: (Aromanian; Arumanian; Macedo-Romanian): rup (ISO 639-3); Aromunisch, Makedoromanisch [Aromanian; Arumanian; Macedo-Romanian]: Einzelsprache, Lebend
 - **rus**: (Russian): rus (ISO 639-3), ru (ISO 639-1); Russisch [Russian]: Einzelsprache, Lebend
 - **sad**: (Sandawe): sad (ISO 639-3); Sandawe [Sandawe]: Einzelsprache, Lebend
 - **sag**: (Sango): sag (ISO 639-3), sg (ISO 639-1); Sango [Sango]: Einzelsprache, Lebend
 - **sah**: (Yakut): sah (ISO 639-3); Jakutisch [Yakut]: Einzelsprache, Lebend
 - **sai**: (South American Indian (Other)): sai (ISO 639-5); Südamerikanische Sprachen [South American Indian (Other)]: Sprachfamilie
 - **sal**: (Salishan languages): sal (ISO 639-5); Salish-Sprachen [Salishan languages]: Sprachfamilie
 - **sam**: (Samaritan Aramaic): sam (ISO 639-3); Samaritanisch [Samaritan Aramaic]: Einzelsprache, Ausgestorben
 - **san**: (Sanskrit): san (ISO 639-3), sa (ISO 639-1); Sanskrit [Sanskrit]: Einzelsprache, Alt
 - **sas**: (Sasak): sas (ISO 639-3); Sasak [Sasak]: Einzelsprache, Lebend
 - **sat**: (Santali): sat (ISO 639-3); Santali [Santali]: Einzelsprache, Lebend
 - **scn**: (Sicilian): scn (ISO 639-3); Sizilianisch [Sicilian]: Einzelsprache, Lebend
 - **sco**: (Scots): sco (ISO 639-3); Scots [Scots]: Einzelsprache, Lebend
 - **sel**: (Selkup): sel (ISO 639-3); Selkupisch [Selkup]: Einzelsprache, Lebend
 - **sem**: (Semitic languages): sem (ISO 639-5); Semitische Sprachen [Semitic languages]: Sprachfamilie
 - **sga**: (Irish, Old (bis 900)): sga (ISO 639-3); Altirisch [Irish, Old (bis 900)]: Einzelsprache, Historisch
 - **sgn**: (Sign Languages): sgn (ISO 639-5); Gebärdensprache [Sign Languages]: Sprachfamilie
 - **shn**: (Shan): shn (ISO 639-3); Shan [Shan]: Einzelsprache, Lebend
 - **sid**: (Sidamo): sid (ISO 639-3); Sidama [Sidamo]: Einzelsprache, Lebend
 - **sin**: (Sinhala; Sinhalese): sin (ISO 639-3), si (ISO 639-1); Singhalesisch [Sinhala; Sinhalese]: Einzelsprache, Lebend
 - **sio**: (Siouan languages): sio (ISO 639-5); Sioux-Sprachen [Siouan languages]: Sprachfamilie
 - **sit**: (Sino-Tibetan languages): sit (ISO 639-5); Sinotibetische Sprachen [Sino-Tibetan languages]: Sprachfamilie
 - **sla**: (Slavic languages): sla (ISO 639-5); Slawische Sprachen [Slavic languages]: Sprachfamilie
 - **slv**: (Slovenian): slv (ISO 639-3), sl (ISO 639-1); Slowenisch [Slovenian]: Einzelsprache, Lebend
 - **sma**: (Southern Sami): sma (ISO 639-3); Südsamisch [Southern Sami]: Einzelsprache, Lebend
 - **sme**: (Northern Sami): sme (ISO 639-3), se (ISO 639-1); Nordsamisch [Northern Sami]: Einzelsprache, Lebend
 - **smi**: (Sami languages): smi (ISO 639-5); Samische Sprachen [Sami languages]: Sprachfamilie
 - **smj**: (Lule Sami): smj (ISO 639-3); Lulesamisch [Lule Sami]: Einzelsprache, Lebend
 - **smn**: (Inari Sami): smn (ISO 639-3); Inarisamisch [Inari Sami]: Einzelsprache, Lebend
 - **smo**: (Samoan): smo (ISO 639-3), sm (ISO 639-1); Samoanisch [Samoan]: Einzelsprache, Lebend
 - **sms**: (Skolt Sami): sms (ISO 639-3); Skoltsamisch [Skolt Sami]: Einzelsprache, Lebend
 - **sna**: (Shona): sna (ISO 639-3), sn (ISO 639-1); Shona [Shona]: Einzelsprache, Lebend
 - **snd**: (Sindhi): snd (ISO 639-3), sd (ISO 639-1); Sindhi [Sindhi]: Einzelsprache, Lebend
 - **snk**: (Soninke): snk (ISO 639-3); Soninke [Soninke]: Einzelsprache, Lebend
 - **sog**: (Sogdian): sog (ISO 639-3); Sogdisch [Sogdian]: Einzelsprache, Alt
 - **som**: (Somali): som (ISO 639-3), so (ISO 639-1); Somali [Somali]: Einzelsprache, Lebend
 - **son**: (Songhai languages): son (ISO 639-5); Songhai-Sprachen [Songhai languages]: Sprachfamilie
 - **sot**: (Sotho, Southern): sot (ISO 639-3), st (ISO 639-1); Sesotho, Süd-Sotho [Sotho, Southern]: Einzelsprache, Lebend
 - **spa**: (Spanish; Castilian): spa (ISO 639-3), es (ISO 639-1); Spanisch, Kastilisch [Spanish; Castilian]: Einzelsprache, Lebend
 - **srd**: (Sardinian): srd (ISO 639-3), sc (ISO 639-1); Sardisch [Sardinian]: Makrosprache, Lebend
 - **srn**: (Sranan Tongo): srn (ISO 639-3); Sranantongo [Sranan Tongo]: Einzelsprache, Lebend
 - **srp**: (Serbian): srp (ISO 639-3), sr (ISO 639-1); Serbisch [Serbian]: Einzelsprache, Lebend
 - **srr**: (Serer): srr (ISO 639-3); Serer [Serer]: Einzelsprache, Lebend
 - **ssa**: (Nilo-Saharan languages): ssa (ISO 639-5); Nilosaharanische Sprachen [Nilo-Saharan languages]: Sprachfamilie
 - **ssw**: (Swati): ssw (ISO 639-3), ss (ISO 639-1); Siswati [Swati]: Einzelsprache, Lebend
 - **suk**: (Sukuma): suk (ISO 639-3); Sukuma [Sukuma]: Einzelsprache, Lebend
 - **sun**: (Sundanese): sun (ISO 639-3), su (ISO 639-1); Sundanesisch [Sundanese]: Einzelsprache, Lebend
 - **sus**: (Susu): sus (ISO 639-3); Susu [Susu]: Einzelsprache, Lebend
 - **sux**: (Sumerian): sux (ISO 639-3); Sumerisch [Sumerian]: Einzelsprache, Alt
 - **swa**: (Swahili): swa (ISO 639-3), sw (ISO 639-1); Swahili [Swahili]: Makrosprache, Lebend
 - **swe**: (Swedish): swe (ISO 639-3), sv (ISO 639-1); Schwedisch [Swedish]: Einzelsprache, Lebend
 - **syc**: (Classical Syriac): syc (ISO 639-3); Syrisch [Classical Syriac]: Einzelsprache, Historisch
 - **syr**: (Syriac): syr (ISO 639-3); Nordost-Neuaramäisch [Syriac]: Makrosprache, Lebend
 - **tah**: (Tahitian): tah (ISO 639-3), ty (ISO 639-1); Tahitianisch, Tahitisch [Tahitian]: Einzelsprache, Lebend
 - **tai**: (Tai languages): tai (ISO 639-5); Tai-Sprachen [Tai languages]: Sprachfamilie
 - **tam**: (Tamil): tam (ISO 639-3), ta (ISO 639-1); Tamil [Tamil]: Einzelsprache, Lebend
 - **tat**: (Tatar): tat (ISO 639-3), tt (ISO 639-1); Tatarisch [Tatar]: Einzelsprache, Lebend
 - **tel**: (Telugu): tel (ISO 639-3), te (ISO 639-1); Telugu [Telugu]: Einzelsprache, Lebend
 - **tem**: (Timne): tem (ISO 639-3); Temne [Timne]: Einzelsprache, Lebend
 - **ter**: (Tereno): ter (ISO 639-3); Terena [Tereno]: Einzelsprache, Lebend
 - **tet**: (Tetum): tet (ISO 639-3); Tetum [Tetum]: Einzelsprache, Lebend
 - **tgk**: (Tajik): tgk (ISO 639-3), tg (ISO 639-1); Tadschikisch [Tajik]: Einzelsprache, Lebend
 - **tgl**: (Tagalog): tgl (ISO 639-3), tl (ISO 639-1); Tagalog [Tagalog]: Einzelsprache, Lebend
 - **tha**: (Thai): tha (ISO 639-3), th (ISO 639-1); Thai [Thai]: Einzelsprache, Lebend
 - **tig**: (Tigre): tig (ISO 639-3); Tigre [Tigre]: Einzelsprache, Lebend
 - **tir**: (Tigrinya): tir (ISO 639-3), ti (ISO 639-1); Tigrinya [Tigrinya]: Einzelsprache, Lebend
 - **tiv**: (Tiv): tiv (ISO 639-3); Tiv [Tiv]: Einzelsprache, Lebend
 - **tkl**: (Tokelau): tkl (ISO 639-3); Tokelauisch [Tokelau]: Einzelsprache, Lebend
 - **tlh**: (Klingon; tlhIngan-Hol): tlh (ISO 639-3); Klingonisch [Klingon; tlhIngan-Hol]: Einzelsprache, Konstruiert
 - **tli**: (Tlingit): tli (ISO 639-3); Tlingit [Tlingit]: Einzelsprache, Lebend
 - **tmh**: (Tamashek): tmh (ISO 639-3); Tuareg [Tamashek]: Makrosprache, Lebend
 - **tog**: (Tonga (Nyasa)): tog (ISO 639-3); ChiTonga [Tonga (Nyasa)]: Einzelsprache, Lebend
 - **ton**: (Tonga (Tonga Islands)): ton (ISO 639-3), to (ISO 639-1); Tongaisch [Tonga (Tonga Islands)]: Einzelsprache, Lebend
 - **tpi**: (Tok Pisin): tpi (ISO 639-3); Tok Pisin, Neuguinea-Pidgin [Tok Pisin]: Einzelsprache, Lebend
 - **tsi**: (Tsimshian): tsi (ISO 639-3); Tsimshian [Tsimshian]: Einzelsprache, Lebend
 - **tsn**: (Tswana): tsn (ISO 639-3), tn (ISO 639-1); Setswana [Tswana]: Einzelsprache, Lebend
 - **tso**: (Tsonga): tso (ISO 639-3), ts (ISO 639-1); Xitsonga [Tsonga]: Einzelsprache, Lebend
 - **tuk**: (Turkmen): tuk (ISO 639-3), tk (ISO 639-1); Turkmenisch [Turkmen]: Einzelsprache, Lebend
 - **tum**: (Tumbuka): tum (ISO 639-3); Tumbuka [Tumbuka]: Einzelsprache, Lebend
 - **tup**: (Tupi languages): tup (ISO 639-5); Tupí-Sprachen [Tupi languages]: Sprachfamilie
 - **tur**: (Turkish): tur (ISO 639-3), tr (ISO 639-1); Türkisch [Turkish]: Einzelsprache, Lebend
 - **tut**: (Altaic languages): tut (ISO 639-5); Altaische Sprachen [Altaic languages]: Sprachfamilie
 - **tvl**: (Tuvalu): tvl (ISO 639-3); Tuvaluisch [Tuvalu]: Einzelsprache, Lebend
 - **twi**: (Twi): twi (ISO 639-3), tw (ISO 639-1); Twi [Twi]: Einzelsprache, Lebend
 - **tyv**: (Tuvinian): tyv (ISO 639-3); Tuwinisch [Tuvinian]: Einzelsprache, Lebend
 - **udm**: (Udmurt): udm (ISO 639-3); Udmurtisch [Udmurt]: Einzelsprache, Lebend
 - **uga**: (Ugaritic): uga (ISO 639-3); Ugaritisch [Ugaritic]: Einzelsprache, Alt
 - **uig**: (Uighur; Uyghur): uig (ISO 639-3), ug (ISO 639-1); Uigurisch [Uighur; Uyghur]: Einzelsprache, Lebend
 - **ukr**: (Ukrainian): ukr (ISO 639-3), uk (ISO 639-1); Ukrainisch [Ukrainian]: Einzelsprache, Lebend
 - **umb**: (Umbundu): umb (ISO 639-3); Umbundu [Umbundu]: Einzelsprache, Lebend
 - **und**: (Undetermined): und (ISO 639-3); „Undefiniert“ [Undetermined]: Speziell
 - **urd**: (Urdu): urd (ISO 639-3), ur (ISO 639-1); Urdu [Urdu]: Einzelsprache, Lebend
 - **uzb**: (Uzbek): uzb (ISO 639-3), uz (ISO 639-1); Usbekisch [Uzbek]: Makrosprache, Lebend
 - **vai**: (Vai): vai (ISO 639-3); Vai [Vai]: Einzelsprache, Lebend
 - **ven**: (Venda): ven (ISO 639-3), ve (ISO 639-1); Tshivenda [Venda]: Einzelsprache, Lebend
 - **vie**: (Vietnamese): vie (ISO 639-3), vi (ISO 639-1); Vietnamesisch [Vietnamese]: Einzelsprache, Lebend
 - **vol**: (Volapük): vol (ISO 639-3), vo (ISO 639-1); Volapük [Volapük]: Einzelsprache, Konstruiert
 - **vot**: (Votic): vot (ISO 639-3); Wotisch [Votic]: Einzelsprache, Lebend
 - **wak**: (Wakashan languages): wak (ISO 639-5); Wakash-Sprachen [Wakashan languages]: Sprachfamilie
 - **wal**: (Walamo): wal (ISO 639-3); Wolaytta [Walamo]: Einzelsprache, Lebend
 - **war**: (Waray): war (ISO 639-3); Wáray-Wáray [Waray]: Einzelsprache, Lebend
 - **was**: (Washo): was (ISO 639-3); Washoe [Washo]: Einzelsprache, Lebend
 - **wen**: (Sorbian languages): wen (ISO 639-5); Sorbische Sprache [Sorbian languages]: Sprachfamilie
 - **wln**: (Walloon): wln (ISO 639-3), wa (ISO 639-1); Wallonisch [Walloon]: Einzelsprache, Lebend
 - **wol**: (Wolof): wol (ISO 639-3), wo (ISO 639-1); Wolof [Wolof]: Einzelsprache, Lebend
 - **xal**: (Kalmyk; Oirat): xal (ISO 639-3); Kalmückisch [Kalmyk; Oirat]: Einzelsprache, Lebend
 - **xho**: (Xhosa): xho (ISO 639-3), xh (ISO 639-1); isiXhosa [Xhosa]: Einzelsprache, Lebend
 - **yao**: (Yao): yao (ISO 639-3); Yao [Yao]: Einzelsprache, Lebend
 - **yap**: (Yapese): yap (ISO 639-3); Yapesisch [Yapese]: Einzelsprache, Lebend
 - **yid**: (Yiddish): yid (ISO 639-3), yi (ISO 639-1); Jiddisch [Yiddish]: Makrosprache, Lebend
 - **yor**: (Yoruba): yor (ISO 639-3), yo (ISO 639-1); Yoruba [Yoruba]: Einzelsprache, Lebend
 - **ypk**: (Yupik languages): ypk (ISO 639-5); Yupik-Sprachen [Yupik languages]: Sprachfamilie
 - **zap**: (Zapotec): zap (ISO 639-3); Zapotekisch [Zapotec]: Makrosprache, Lebend
 - **zbl**: (Blissymbols; Blissymbolics; Bliss): zbl (ISO 639-3); Bliss-Symbol [Blissymbols; Blissymbolics; Bliss]: Einzelsprache, Konstruiert
 - **zen**: (Zenaga): zen (ISO 639-3); Zenaga [Zenaga]: Einzelsprache, Lebend
 - **zgh**: (Standard Moroccan Tamazight): zgh (ISO 639-3); Marokkanisches Tamazight [Standard Moroccan Tamazight]: Einzelsprache, Lebend
 - **zha**: (Zhuang; Chuang): zha (ISO 639-3), za (ISO 639-1); Zhuang [Zhuang; Chuang]: Makrosprache, Lebend
 - **znd**: (Zande languages): znd (ISO 639-5); Zande-Sprachen [Zande languages]: Sprachfamilie
 - **zul**: (Zulu): zul (ISO 639-3), zu (ISO 639-1); isiZulu [Zulu]: Einzelsprache, Lebend
 - **zun**: (Zuni): zun (ISO 639-3); Zuñi [Zuni]: Einzelsprache, Lebend
 - **zxx**: (No linguistic content; Not applicable): zxx (ISO 639-3); „Kein sprachlicher Inhalt; Nicht anwendbar“ [No linguistic content; Not applicable]: Speziell
 - **zza**: (Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki): zza (ISO 639-3); Zazaisch [Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki]: Makrosprache, Lebend
 - **bod**: (Tibetan [Terminology Code]): tib (ISO 639-2/B), bod (ISO 639-3), bo (ISO 639-1); Tibetisch [Tibetan]: Einzelsprache, Lebend
 - **ces**: (Czech [Terminology Code]): cze (ISO 639-2/B), ces (ISO 639-3), cs (ISO 639-1); Tschechisch [Czech]: Einzelsprache, Lebend
 - **cym**: (Welsh [Terminology Code]): wel (ISO 639-2/B), cym (ISO 639-3), cy (ISO 639-1); Walisisch [Welsh]: Einzelsprache, Lebend
 - **deu**: (German [Terminology Code]): ger (ISO 639-2/B), deu (ISO 639-3), de (ISO 639-1); Deutsch [German]: Einzelsprache, Lebend
 - **ell**: (Greek, Modern (ab 1453) [Terminology Code]): gre (ISO 639-2/B), ell (ISO 639-3), el (ISO 639-1); Griechisch [Greek, Modern (ab 1453)]: Einzelsprache, Lebend
 - **eus**: (Basque [Terminology Code]): baq (ISO 639-2/B), eus (ISO 639-3), eu (ISO 639-1); Baskisch [Basque]: Einzelsprache, Lebend
 - **fas**: (Persian [Terminology Code]): per (ISO 639-2/B), fas (ISO 639-3), fa (ISO 639-1); Persisch [Persian]: Makrosprache, Lebend
 - **fra**: (French [Terminology Code]): fre (ISO 639-2/B), fra (ISO 639-3), fr (ISO 639-1); Französisch [French]: Einzelsprache, Lebend
 - **hye**: (Armenian [Terminology Code]): arm (ISO 639-2/B), hye (ISO 639-3), hy (ISO 639-1); Armenisch [Armenian]: Einzelsprache, Lebend
 - **isl**: (Icelandic [Terminology Code]): ice (ISO 639-2/B), isl (ISO 639-3), is (ISO 639-1); Isländisch [Icelandic]: Einzelsprache, Lebend
 - **kat**: (Georgian [Terminology Code]): geo (ISO 639-2/B), kat (ISO 639-3), ka (ISO 639-1); Georgisch [Georgian]: Einzelsprache, Lebend
 - **mkd**: (Macedonian [Terminology Code]): mac (ISO 639-2/B), mkd (ISO 639-3), mk (ISO 639-1); Mazedonisch [Macedonian]: Einzelsprache, Lebend
 - **mri**: (Maori [Terminology Code]): mao (ISO 639-2/B), mri (ISO 639-3), mi (ISO 639-1); Maori [Maori]: Einzelsprache, Lebend
 - **msa**: (Malay [Terminology Code]): may (ISO 639-2/B), msa (ISO 639-3), ms (ISO 639-1); Malaiisch [Malay]: Makrosprache, Lebend
 - **mya**: (Burmese [Terminology Code]): bur (ISO 639-2/B), mya (ISO 639-3), my (ISO 639-1); Birmanisch [Burmese]: Einzelsprache, Lebend
 - **nld**: (Dutch; Flemish [Terminology Code]): dut (ISO 639-2/B), nld (ISO 639-3), nl (ISO 639-1); Niederländisch, Belgisches Niederländisch [Dutch; Flemish]: Einzelsprache, Lebend
 - **ron**: (Romanian; Moldavian; Moldovan [Terminology Code]): rum (ISO 639-2/B), ron (ISO 639-3), ro (ISO 639-1); Rumänisch [Romanian; Moldavian; Moldovan]: Einzelsprache, Lebend
 - **slk**: (Slovak [Terminology Code]): slo (ISO 639-2/B), slk (ISO 639-3), sk (ISO 639-1); Slowakisch [Slovak]: Einzelsprache, Lebend
 - **sqi**: (Albanian [Terminology Code]): alb (ISO 639-2/B), sqi (ISO 639-3), sq (ISO 639-1); Albanisch [Albanian]: Makrosprache, Lebend
 - **zho**: (Chinese [Terminology Code]): chi (ISO 639-2/B), zho (ISO 639-3), zh (ISO 639-1); Chinesisch [Chinese]: Makrosprache, Lebend

---

<p style="margin-bottom:60px"></p>

<a name="d17e7691"></a>
#### Term
|     |     |
| --- | --- |
| **Name** | `@term`  |
| **Datatype** | string |
| **Label** (de) | Term |
| **Description** (de) | - |
| **Contained by** |  |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6132"></a>
#### URL
|     |     |
| --- | --- |
| **Name** | `@src`  |
| **Datatype** | anyURI |
| **Label** (de) | URL |
| **Description** (de) | URL bzw. Pfad zum Bild. |
| **Contained by** | [`img`](#elem.img) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6713"></a>
#### URL
|     |     |
| --- | --- |
| **Name** | `@url`  |
| **Datatype** | string |
| **Label** (de) | URL |
| **Description** (de) | URL zu einer Quelle, falls vorhanden. |
| **Contained by** | [`source`](#elem.source) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e2640"></a>
#### Verantwortliche Person
|     |     |
| --- | --- |
| **Name** | `@who`  |
| **Datatype** | IDREF |
| **Label** (de) | Verantwortliche Person |
| **Description** (de) | Die ID einer Person, die für die Änderung verantwortlich zeichnet. |
| **Contained by** | [`change`](#d17e2624) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e5396"></a>
#### Vorzugsbenennung in originalschriftlicher Form
|     |     |
| --- | --- |
| **Name** | `@type`  |
| **Datatype** | string |
| **Label** () | Vorzugsbenennung in originalschriftlicher Form |
| **Description** () | Die angegebene Vorzugsbenennung entspricht der originalschriftlicher Form. |
| **Contained by** | [`gndo:preferredName`](#person-record-preferredName) |

***Predefined Values***  

 - **original**: *no description available*

---

<p style="margin-bottom:60px"></p>

<a name="d17e1737"></a>
#### Werkstyp
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Label** (de) | Werkstyp |
| **Label** (en) | Type of a Work |
| **Description** (de) | Typisierung des dokumentierten Werks (z.B. als Sammlung, Werk der Musik etc.). Wenn kein `@gndo:type` vergeben wird, gilt der Ort als [gndo:Work](https://d-nb.info/standards/elementset/gnd#Work). |
| **Description** (en) | Spezifies the work encoded as eg. collection, musical work etc. If no `@gndo:type` is provided the default is [gndo:Work](https://d-nb.info/standards/elementset/gnd#Work). |
| **Contained by** | [`work`](#d17e1648) |

***Predefined Values***  

 - **https://d-nb.info/standards/elementset/gnd#Collection**: (Sammlung) [GNDO](https://d-nb.info/standards/elementset/gnd#Collection)
 - **https://d-nb.info/standards/elementset/gnd#CollectiveManuscript**: (Sammelhandschrift) [GNDO](https://d-nb.info/standards/elementset/gnd#CollectiveManuscript)
 - **https://d-nb.info/standards/elementset/gnd#Expression**: (Expression) [GNDO](https://d-nb.info/standards/elementset/gnd#Expression)
 - **https://d-nb.info/standards/elementset/gnd#Manuscript**: (Schriftdenkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#Manuscript)
 - **https://d-nb.info/standards/elementset/gnd#MusicalWork**: (Werk der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#MusicalWork)
 - **https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic**: (Provenienzmerkmal) [GNDO](https://d-nb.info/standards/elementset/gnd#ProvenanceCharacteristic)
 - **https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork**: (Fassung eines Werks der Musik) [GNDO](https://d-nb.info/standards/elementset/gnd#VersionOfAMusicalWork)

---

<p style="margin-bottom:60px"></p>

<a name="d17e6517"></a>
#### Zieladresse (Hyperlink)
|     |     |
| --- | --- |
| **Name** | `@target`  |
| **Datatype** | anyURI (*RNG Pattern Matching*) |
| **Label** (de) | Zieladresse (Hyperlink) |
| **Description** (de) | Zieladresse eines Hyperlinks, repräsentiert durch einen URL nach HTTP oder HTTPS Schema. |
| **Contained by** | [`ref`](#elem.ref) |

***Validation***  

```xml 
<param xmlns="http://relaxng.org/ns/structure/1.0" name="pattern">(https|http)://(\S+)</param>
 
```


---

<p style="margin-bottom:60px"></p>

<a name="d17e6761"></a>
#### entityXML Store ID
|     |     |
| --- | --- |
| **Name** | `@id`  |
| **Datatype** | string |
| **Label** (de) | entityXML Store ID |
| **Description** (de) | Identifier des Datensatzes in einem entityXML Store. |
| **Contained by** | [`store:store`](#d17e6745) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e3656"></a>
#### gndo:role
|     |     |
| --- | --- |
| **Name** | `@gndo:role`  |
| **Datatype** | string |
| **Contained by** | [`gndo:contributor`](#d17e3635) |

***Predefined Values***  

 - **gndo:accreditedAuthor**: (Zugeschriebener Verfasser) An author, artist, etc., relating him/her to a resource for which there is or once was substantial authority for designating that person as author, creator, etc. of the work. Same as [https://d-nb.info/standards/elementset/gnd#accreditedAuthor](https://d-nb.info/standards/elementset/gnd#accreditedAuthor).
 - **gndo:accreditedComposer**: (Zugeschriebener Komponist) An author, artist, etc., relating him/her to a resource for which there is or once was substantial authority for designating that person as author, creator, etc. of the work .Same as [https://d-nb.info/standards/elementset/gnd#accreditedComposer](https://d-nb.info/standards/elementset/gnd#accreditedComposer)
 - **gndo:addressee**: (Adressat) A person, family, or organization to whom the correspondence in a work is addressed. Same as [https://d-nb.info/standards/elementset/gnd#addressee](https://d-nb.info/standards/elementset/gnd#addressee)
 - **gndo:annotator**: (Annotator) A person who makes manuscript annotations on an item. Same as [https://d-nb.info/standards/elementset/gnd#annotator](https://d-nb.info/standards/elementset/gnd#annotator)
 - **gndo:arranger**: (Arrangeur) A person, family, or organization contributing to a musical work by rewriting the composition for a medium of performance different from that for which the work was originally intended, or modifying the work for the same medium of performance, etc., such that the musical substance of the original composition remains essentially unchanged. For extensive modification that effectively results in the creation of a new musical work, see composer. Same as [https://d-nb.info/standards/elementset/gnd#arranger](https://d-nb.info/standards/elementset/gnd#arranger)
 - **gndo:author**: (Verfasser) A person, family, or organization responsible for creating a work that is primarily textual in content, regardless of media type (e.g., printed text, spoken word, electronic text, tactile text) or genre (e.g., poems, novels, screenplays, blogs). Use also for persons, etc., creating a new work by paraphrasing, rewriting, or adapting works by another creator such that the modification has substantially changed the nature and content of the original or changed the medium of expression.Same as [https://d-nb.info/standards/elementset/gnd#author](https://d-nb.info/standards/elementset/gnd#author)
 - **gndo:bookbinder**: (Buchbinder) A person who binds an item. Same as [https://d-nb.info/standards/elementset/gnd#bookbinder](https://d-nb.info/standards/elementset/gnd#bookbinder)
 - **gndo:bookdesigner**: (Buchgestalter) A person or organization involved in manufacturing a manifestation by being responsible for the entire graphic design of a book, including arrangement of type and illustration, choice of materials, and process used. Same as [https://d-nb.info/standards/elementset/gnd#bookdesigner](https://d-nb.info/standards/elementset/gnd#bookdesigner)
 - **gndo:cartographer**: (Kartograf) A person, family, or organization responsible for creating a map, atlas, globe, or other cartographic work. Same as [https://d-nb.info/standards/elementset/gnd#cartographer](https://d-nb.info/standards/elementset/gnd#cartographer)
 - **gndo:choreographer**: (Choreograf) A person responsible for creating or contributing to a work of movement. Same as [https://d-nb.info/standards/elementset/gnd#choreographer](https://d-nb.info/standards/elementset/gnd#choreographer).
 - **gndo:citedAuthor**: (Zitierter Verfasser) A person or organization whose work is largely quoted or extracted in works to which he or she did not contribute directly. Such quotations are found particularly in exhibition catalogs, collections of photographs, etc. Same as [https://d-nb.info/standards/elementset/gnd#citedAuthor](https://d-nb.info/standards/elementset/gnd#citedAuthor).
 - **gndo:citedComposer**: (Zitierter Komponist) A person or organization whose work is largely quoted or extracted in works to which he or she did not contribute directly. Such quotations are found particularly in exhibition catalogs, collections of photographs, etc. Same as [https://d-nb.info/standards/elementset/gnd#citedComposer](https://d-nb.info/standards/elementset/gnd#citedComposer).
 - **gndo:compiler**: (Kompilator) A person, family, or organization responsible for creating a new work (e.g., a bibliography, a directory) through the act of compilation, e.g., selecting, arranging, aggregating, and editing data, information, etc. Same as [https://d-nb.info/standards/elementset/gnd#compiler](https://d-nb.info/standards/elementset/gnd#compiler).
 - **gndo:composer**: (Komponist) A person, family, or organization responsible for creating or contributing to a musical resource by adding music to a work that originally lacked it or supplements it. Same as [https://d-nb.info/standards/elementset/gnd#composer](https://d-nb.info/standards/elementset/gnd#composer).
 - **gndo:conferrer**: (Leihgeber) A person or organization permitting the temporary use of a book, manuscript, etc., such as for photocopying or microfilming. Same as [https://d-nb.info/standards/elementset/gnd#conferrer](https://d-nb.info/standards/elementset/gnd#conferrer).
 - **gndo:copist**: (Kopist) A person or family who is known as scribe or copyist. Same as [https://d-nb.info/standards/elementset/gnd#copist](https://d-nb.info/standards/elementset/gnd#copist). Same as [https://d-nb.info/standards/elementset/gnd#copist](https://d-nb.info/standards/elementset/gnd#copist)
 - **gndo:creator**: (Urheber) A person or organization performing the work, i.e., the name of a person or organization associated with the intellectual content of the work. This category does not include the publisher or personal affiliation, or sponsor except where it is also the corporate author. Same as [https://d-nb.info/standards/elementset/gnd#creator](https://d-nb.info/standards/elementset/gnd#creator).
 - **gndo:designer**: (Designer) A person, family, or organization responsible for creating a design for an object. Same as [https://d-nb.info/standards/elementset/gnd#designer](https://d-nb.info/standards/elementset/gnd#designer).
 - **gndo:director**: (Regisseur) A person responsible for the general management and supervision of a filmed performance, a radio or television program, etc. Same as [https://d-nb.info/standards/elementset/gnd#director](https://d-nb.info/standards/elementset/gnd#director).
 - **gndo:directorOfPhotography**: (Verantwortlicher Kameramann) A person in charge of photographing a motion picture, who plans the technical aspets of lighting and photographing of scenes, and often assists the director in the choice of angles, camera setups, and lighting moods. He or she may also supervise the further processing of filmed material up to the completion of the work print. Cinematographer is also referred to as director of photography. Do not confuse with videographer. Same as [https://d-nb.info/standards/elementset/gnd#directorOfPhotography](https://d-nb.info/standards/elementset/gnd#directorOfPhotography).
 - **gndo:doubtfulAuthor**: (Angezweifelter Verfasser) A person or organization to which authorship has been dubiously or incorrectly ascribed. Same as [https://d-nb.info/standards/elementset/gnd#doubtfulAuthor](https://d-nb.info/standards/elementset/gnd#doubtfulAuthor).
 - **gndo:doubtfulComposer**: (Angezweifelter Komponist) A person or organization to which authorship has been dubiously or incorrectly ascribed. Same as [https://d-nb.info/standards/elementset/gnd#doubtfulComposer](https://d-nb.info/standards/elementset/gnd#doubtfulComposer).
 - **gndo:editor**: (Herausgeber) A person, family, or organization contributing to a resource by revising or elucidating the content, e.g., adding an introduction, notes, or other critical matter. An editor may also prepare a resource for production, publication, or distribution. For major revisions, adaptations, etc., that substantially change the nature and content of the original work, resulting in a new work, see author. Same as [https://d-nb.info/standards/elementset/gnd#editor](https://d-nb.info/standards/elementset/gnd#editor).
 - **gndo:engraver**: (Graveur) A person or organization who cuts letters, figures, etc. on a surface, such as a wooden or metal plate used for printing. Same as [https://d-nb.info/standards/elementset/gnd#engraver](https://d-nb.info/standards/elementset/gnd#engraver).
 - **gndo:etcher**: (Radierer) A person or organization who produces text or images for printing by subjecting metal, glass, or some other surface to acid or the corrosive action of some other substance. Same as [https://d-nb.info/standards/elementset/gnd#etcher](https://d-nb.info/standards/elementset/gnd#etcher).
 - **gndo:fictitiousAuthor**: (Fiktiver Verfasser) A fictitious person, family, or corporate body ascertained to be the author. Same as [https://d-nb.info/standards/elementset/gnd#fictitiousAuthor](https://d-nb.info/standards/elementset/gnd#fictitiousAuthor).
 - **gndo:firstAuthor**: (Erste Verfasserschaft) A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators. Same as [https://d-nb.info/standards/elementset/gnd#firstAuthor](https://d-nb.info/standards/elementset/gnd#firstAuthor).
 - **gndo:firstComposer**: (Erster Komponist) A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators. Same as [https://d-nb.info/standards/elementset/gnd#firstComposer](https://d-nb.info/standards/elementset/gnd#firstComposer).
 - **gndo:illustratorOrIlluminator**: (Illustrator oder Illuminator) A person, family, or organization contributing to a resource by supplementing the primary content with drawings, diagrams, photographs, etc. If the work is primarily the artistic content created by this entity, use artist or photographer. Same as [https://d-nb.info/standards/elementset/gnd#illustratorOrIlluminator](https://d-nb.info/standards/elementset/gnd#illustratorOrIlluminator).
 - **gndo:instrumentalist**: (Instrumentalmusiker) A performer contributing to a resource by playing a musical instrument. Same as [https://d-nb.info/standards/elementset/gnd#instrumentalist](https://d-nb.info/standards/elementset/gnd#instrumentalist).
 - **gndo:librettist**: (Librettist) An author of a libretto of an opera or other stage work, or an oratorio. Same as [https://d-nb.info/standards/elementset/gnd#librettist](https://d-nb.info/standards/elementset/gnd#librettist).
 - **gndo:lithographer**: (Litograf) A person or organization who prepares the stone or plate for lithographic printing, including a graphic artist creating a design directly on the surface from which printing will be done. Same as [https://d-nb.info/standards/elementset/gnd#lithographer](https://d-nb.info/standards/elementset/gnd#lithographer).
 - **gndo:narrator**: (Sprecher) A performer contributing to a resource by reading or speaking in order to give an account of an act, occurrence, course of events, etc. Same as [https://d-nb.info/standards/elementset/gnd#narrator](https://d-nb.info/standards/elementset/gnd#narrator).
 - **gndo:painter**: (Maler) A person or family who paints. Same as [https://d-nb.info/standards/elementset/gnd#painter](https://d-nb.info/standards/elementset/gnd#painter).
 - **gndo:photographer**: (Fotograf) A person, family, or organization responsible for creating a photographic work. Same as [https://d-nb.info/standards/elementset/gnd#photographer](https://d-nb.info/standards/elementset/gnd#photographer).
 - **gndo:poet**: (Dichter) An author of the words of a non-dramatic musical work (e.g. the text of a song), except for oratorios. Same as [https://d-nb.info/standards/elementset/gnd#poet](https://d-nb.info/standards/elementset/gnd#poet).
 - **gndo:printer**: (Drucker) A person, family, or organization involved in manufacturing a manifestation of printed text, notated music, etc., from type or plates, such as a book, newspaper, magazine, broadside, score, etc. Same as [https://d-nb.info/standards/elementset/gnd#printer](https://d-nb.info/standards/elementset/gnd#printer).
 - **gndo:revisor**: (Bearbeiter) A person or organization who 1) reworks a musical composition, usually for a different medium, or 2) rewrites novels or stories for motion pictures or other audiovisual medium. Same as [https://d-nb.info/standards/elementset/gnd#revisor](https://d-nb.info/standards/elementset/gnd#revisor).
 - **gndo:screenwriter**: (Drehbuchautor) An author of a screenplay, script, or scene. Same as [https://d-nb.info/standards/elementset/gnd#screenwriter](https://d-nb.info/standards/elementset/gnd#screenwriter).
 - **gndo:singer**: (Sänger) A performer contributing to a resource by using his/her/their voice, with or without instrumental accompaniment, to produce music. A singer's performance may or may not include actual words. Same as [https://d-nb.info/standards/elementset/gnd#singer](https://d-nb.info/standards/elementset/gnd#singer).
 - **gndo:subeditor**: (Redakteur) A person or organization who writes or develops the framework for an item without being intellectually responsible for its content. Same as [https://d-nb.info/standards/elementset/gnd#subeditor](https://d-nb.info/standards/elementset/gnd#subeditor).
 - **gndo:writerOfAddedCommentary**: (Kommentator (schriftlich)) A person or organization responsible for the commentary or explanatory notes about a text. For the writer of manuscript annotations in a printed book, use Annotator. Same as [https://d-nb.info/standards/elementset/gnd#writerOfAddedCommentary](https://d-nb.info/standards/elementset/gnd#writerOfAddedCommentary)

---

<p style="margin-bottom:60px"></p>

<a name="d17e3541"></a>
#### gndo:type
|     |     |
| --- | --- |
| **Name** | `@gndo:type`  |
| **Datatype** | string |
| **Contained by** | [`gndo:broaderTerm`](#d17e3502) |

***Predefined Values***  

 - **general**: (Oberbegriff allgemein) Broader term . Same as [https://d-nb.info/standards/elementset/gnd#broaderTermGeneral](https://d-nb.info/standards/elementset/gnd#broaderTermGeneral).
 - **generic**: (Oberbegriff generisch) The generic relation is a semantic relation between two concepts where the intension of one of the concepts includes that of the other concepts and at least one additional delimiting characteristic is added. Same as [https://d-nb.info/standards/elementset/gnd#broaderTermGeneric](https://d-nb.info/standards/elementset/gnd#broaderTermGeneric).
 - **instantial**: (Oberbegriff instantiell) The instance relationship links a general concept such as a class of things or events, and an individual instance oft hat class, which is often represented by a proper name. Same as [https://d-nb.info/standards/elementset/gnd#broaderTermInstantial](https://d-nb.info/standards/elementset/gnd#broaderTermInstantial).
 - **partitive**: (Oberbegriff partitiv) The hierarchical whole-part relationship covers a limited range of situations in which a part of an entity or system belongs uniquely to a particular possessing whole. When applied to persons, this is the relation between a single person (particularly gods) and hierarchically broader groups of gods and mythologic entities. Same as [https://d-nb.info/standards/elementset/gnd#broaderTermPartitive](https://d-nb.info/standards/elementset/gnd#broaderTermPartitive).
 - **with-more-than-one-element**: (Oberbegriff mehrgliedrig) Broader term (with more than one element). Same as [https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement](https://d-nb.info/standards/elementset/gnd#broaderTermWithMoreThanOneElement).

---

<p style="margin-bottom:60px"></p>

<a name="d17e6815"></a>
#### name
|     |     |
| --- | --- |
| **Name** | `@name`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](#d17e6801) |

---

<p style="margin-bottom:60px"></p>

<a name="d17e6817"></a>
#### timestamp
|     |     |
| --- | --- |
| **Name** | `@timestamp`  |
| **Datatype** | string |
| **Contained by** | [`store:step`](#d17e6801) |

---

<p style="margin-bottom:60px"></p>


<a name="serialisation"></a>
## Serialisierungen

Daten in entityXML lassen sich in verschiedene XML Stile aber auch andere Datenformate serialisieren:

<a name="docs_d14e2885"></a>
### XML


- **entityXML → entityXML (strict)**: [`entityxml.strict.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml.strict.xsl): Konversion, die alle *custom namespaces* aus einer entityXML Ressource entfernt. Sie ist fester Bestandteil des Ingestworkflows von Daten in die GND durch die Text+ GND Agentur.
- **entityXML → MARC-XML**: [`entityxml2marcxml.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/marcxml/entityxml2marcxml.xsl): Konversion, die entityXML Einträge in MARC-XML konvertiert. Sie ist fester Bestandteil des Ingestworkflows von Daten in die GND durch die Text+ GND Agentur.

<a name="docs_d14e2908"></a>
### JSON


- **entityXML → JSON**: [`entityxml2json.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2json.xsl): JSON Serialisierung einer entityXML Ressource.
- **entityXML → GND-Toolbox JSON**: [`entityxml2toolbox-json.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2toolbox-json.xsl): JSON Serialisierung einer entityXML Ressource, optimiert für die Verarbeitung durch die GND-Toolbox.

<a name="docs_d14e2928"></a>
### Markdown


- **entityXML → Markdown**: [`entityxml2md.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2md.xsl): Markdown Liste mit einzelnen Einträgen aus einer entityXML Ressource

<a name="docs_d14e2943"></a>
### CSV


- **entityXML → CSV**: [`entityxml2csv.xsl`](https://gitlab.gwdg.de/entities/entityxml/-/blob/master/scripts/xslt/entityxml2csv.xsl) (Proof of Concept): CSV Liste aller Einträge einer entityXML Ressource. *nota bene*: Diese Konversion steckt noch in den Kinderschuhen.

<a name="revision-history"></a>
## Revisionen



##0.6.5+ BETA (Nightly) (2024-10-22)  

- Added Validation on `gndo:geographicAreaCode` and updated Documentation.
- Added `@cert` to `gndo:forename` and `gndo:surname`.
- Added `gndo:topic`, `gndo:dateOfConferenceOrEvent`, `gndo:organizerOrHost` and `gndo:place` on `event` entity.
- Added `gndo:dateOfConferenceOrEvent` on `event` entity.


##0.6.5 BETA (Nightly) (2024-08-06)  

- Added `gndo:playedInstrument` on `person` entity.
- Added Languagecode vocabulary for `@xml:lang`.
- Added `@script` to `gndo:variantName`.
- Added `@script` and `@type='original'` to Persons `gndo:preferredName` which makes it possible to encode an additional preferred Name in original script.
- Added `gndo:familialRelationship` and `gndo:acquaintanceshipOrFriendship` to Person.
- Removed `gndo:relatesTo/gndo:type` and added `gndo:relatesTo/gndo:code` incl. list of GND relation-type-codes.
- Made `gndo:professionOrOccupation` capable of documenting a simple Literal without GND-URI reference. 
- Changed Contentmodell for `source`: (1) removed `ref` since there is already `@url`; (2) added `title` and `note` to Contentmodel.


##0.6.0 BETA (2024-07-24)  

- Added `@isil` to `provider`.
- Added first elements to markup "Provenienzmerkmale" using `work`.


##0.5.3 ALPHA (2024-03-01)  

- Added `mapping` structure.


##0.5.2 ALPHA (2024-02-26)  

- Added `gndo:dateOfEstablishmentAndTermination` to events, places and works. The property now may contain text.
- Added `ref` to `source` content model.
- Added `img` property. Now it is possible to document images for every entity record.


##0.5.1 ALPHA (2023-02-08)  

- Preparations for the BETA Version. New Attributes (`@iso-notBefore`, `@iso-notAfter`) added to Date Elements.
- GND-Toolbox Konversion added to serialize entityXML Data to optimised Toolbox JSON.
- separated record types.
- revised `revision` description model.
- cleaned `gndo:publication` and added simple bibliographic model.
- changed record-id to `@xml:id`
- added `bf:instanceOf` to `manifestation` entity.
- changed `gndo:page` to `gndo:homepage` (cf. v0.5.0 > v0.5.1 update script `/scripts/xslt/update/050-051.xsl`)
- added `gndo:succeedingPlaceOrGeographicName`, `gndo:temporaryName`, `gndo:titleOfNobility`, `gndo:abbreviatedName`, `gndo:fieldOfStudy` and `gndo:abbreviatedName`.
- added `gndo:relatesTo` property to all records (which has no direct equivalent in the GNDO but may be helpfull to map out relations between entities anyway).
- added `event` record which more or less translates to `gndo:ConferenceOrEvent` but is not fully moddled yet.
- changed `gndo:dateOfActivity` to `gndo:periodOfActivity` (cf. v0.5.0 > v0.5.1 update script `/scripts/xslt/update/050-051.xsl`)
- refined Validation on `gndo:publication` concerning `@ref`, `@gndo:ref`, `@dnb:catalogue`


##0.5.0 ALPHA (2023-01-25)  

- **First published Version 0.5.0** (ALPHA) is now open for testing.
- Renamed former `project` element to `provider` and placed in `entityXML/collection`
- Changes on the Content Model of `entity`: ANY ELEMENT concept finetuned.
- Added `@url` to `source`
- Added `gndo:affiliation` to `person` and `corporateBody`
- Added `@dnb:catalogue` to `gndo:publication`
- Added `gndo:pseudonym` to `person`.
- Added `@gndo:type` to `person` to diferentiate subclasses like gods, fictional charcters etc.
- Modified `@ref` to either point to an internal ID or to an external URI.
- Added `@gndo:ref` to `gndo:professionOrOccupation` to make `@gndo:term` more clearer.


##0.3.2 ALPHA (Unpublished) (2022-11-18)  

- Cleaned Structure and introduced models
- Added Support for other schemes (like LIDO) using `rng:element/rng:anyName` concept.


##0.3.1 ALPHA (Unpublished) (2022-11-11)  

- Added manifestation and expression record.
- Added Property Class with agency features.


##0.3.0 ALPHA (Unpublished) (2022-10-05)  
Added store-namespace and schema.


##0.2.1 (Unpublished) (2022-07-06)  

- Added `work` Entity and provided MARC mapping.
- Added `@id` to `list` element.


##0.2 (Unpublished) (2022-05-10)  
Added GND-Reference abstract class.


##0.1.4 (Unpublished) (2022-04-21)  
Simplified Entity lists. Now every Record needs an ID (either `@id` or `@gndo:uri`).


##0.1.3 (Unpublished) (2022-04-20)  
Better revision description; entityXML now supports arbitrary elements within entity markup.


##0.1.2 (Unpublished) (2022-04-19)  
Structural Changes concerning project and collection container.


##0.1.1 (Unpublished) (2022-03-31)  
Reduced Content: Integrating just persons and places for now!


##0.1 (Unpublished) (2022-03-29)  
Created RELAX-NG Schema.

